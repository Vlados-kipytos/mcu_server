import asyncio
from typing import Any

from aiohttp import web

from app.api.v1.subapp import setup_api
from app.core import cmdline
from app.core import installtools
from app.core import licenses
from app.core import logger
from app.core import xmltools
from mcu.postgresql.client import Manager as PostgreSqlClientManager
import event_processor.ipc.event_processor as event_processor

async def init_app(config) -> web.Application:
    app = web.Application()
    app.on_cleanup.append(close_database)
    app[PostgreSqlClientManager.get_class_name()] = PostgreSqlClientManager(
        host=config['db']['host'],
        port=config['db']['port'],
        user=config['db']['username'],
        password=config['db']['password'],
        database=config['db']['dbname']
    )

    setup_api(app)
    return app


def main(args: Any = None) -> None:
    """TODO: Почему бы не использовать argparse? https://docs.python.org/3.5/library/argparse.html"""
    cmd_line_params = cmdline.parse_command_line()
    if not cmd_line_params["success"]:
        print(cmd_line_params["error"])
        return

    if "--config" in cmd_line_params["params"]:
        xmltools.load_config(cmd_line_params["params"]["--config"])

    config = xmltools.get_config()

    if "--port" in cmd_line_params["params"]:
        config["webapi"]["port"] = cmd_line_params["params"]["--port"]

    lg = logger.init_logger("web_api.log")

    # dbtools.set_optimize_connection(True)
    licenses.set_first_license_upd(True)
    installtools.set_installing(False)

    #EXPERIMENT:
    if "--join-evt-proc" in cmd_line_params["params"]:
        if cmd_line_params["params"]["--join-evt-proc"] == "true":
            event_processor.run_event_processor_in_web_api()

    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(init_app(config))
    web.run_app(app, host='127.0.0.1', port=config["webapi"]["port"], access_log=lg)


async def close_database(app: web.Application):
    pool: PostgreSqlClientManager = app[PostgreSqlClientManager.get_class_name()]
    await pool.close()

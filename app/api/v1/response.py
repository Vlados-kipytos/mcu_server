import os
import os.path

import app.core.data as data
import app.core.features as features
import app.core.calls_settings as calls_settings
import app.core.customlayouttypes as customlayouttypes
import app.core.layoutcfgtools as layoutcfgtools
import app.core.networksettings as networksettings
import app.core.profile_settings as profile_settings
from app.core.errors import CustomLayoutTypeSlotsIntersectError
import app.core.tools as tools
from app.core.errors import FeatureRestrictedByLicenseError
from app.core.errors import TooManySlotRoles
from app.core.responses import *
from .errors import BadFormatFieldError
from .errors import NoRequiredError
from .errors import ValueNotInLimitsError


def export_contact_entity(contact: ContactEntity) -> dict:
    unquoted_file = tools.unquote_address(contact.file)
    parsed_file_uri = tools.parse_address(unquoted_file)
    return {
        'id': contact.contact_id,
        'name': contact.name,
        'display_name': contact.display_name,
        'sip': tools.unquote_address(contact.sip),
        'h323': tools.unquote_address(contact.h323),
        'rtsp': tools.unquote_address(contact.rtsp),
        'file': parsed_file_uri["path"],
        'loop': contact.loop,
        'email': contact.email,
        'bitrate': contact.bitrate,
        'groups': contact.groups,
        'realaddr': contact.realaddr,
        'autoadded': contact.autoadded,
        'created_at': contact.created_at,
        'allowed_audio_codecs': contact.audio_codecs.get_codecs_list(),
        'allowed_video_codecs': contact.video_codecs.get_codecs_list()
        #'connection_id': contact.conn_id
    }


def export_contact_list(contact_list: ContactListResponse) -> dict:
    return {
        'contacts': list(map(export_contact_entity, contact_list.contacts)),
        'size': contact_list.full_list_size
    }


def export_id_entity(id: int) -> dict:
    return {"id": id}


def export_auto_start_mode_entity(_auto_start: AutoStartConferenceModeEntity, _out_json: dict):
    if "schedule" not in _out_json:
        _out_json["schedule"] = {}

    _out_json["schedule"]["period"] = []
    for time_range in _auto_start.time_range_list:
        _out_json["schedule"]["period"].append({ "start_time": time_range.start_time, "end_time": time_range.end_time })

    _out_json["schedule"]["mode"] = _auto_start.auto_start_mode

    _out_json["schedule"]["week_days"] = []
    for week_day in range(7):
        mask = 1 << week_day
        if mask & _auto_start.auto_start_week_day:
            week_day_name = data.get_weekday(week_day)
            _out_json["schedule"]["week_days"].append(week_day_name)

    _out_json["schedule"]["month_days"] = []
    month_day = 1
    while month_day <= 31:
        mask = 1 << month_day
        if mask & _auto_start.auto_start_month_day:
            _out_json["schedule"]["month_days"].append(month_day)
        month_day += 1

    _out_json["schedule"]["month_week_days"] = { "first": [], "last": [] }
    for week_day in range(7):
        mask = 1 << week_day
        week_day_name = data.get_weekday(week_day)
        if mask & _auto_start.first_week_day:
            _out_json["schedule"]["month_week_days"]["first"].append(week_day_name)
        if mask & _auto_start.last_week_day:
            _out_json["schedule"]["month_week_days"]["last"].append(week_day_name)


def import_auto_start_mode_entity(_data: dict) -> AutoStartConferenceModeEntity:
    if "schedule" not in _data:
        return AutoStartConferenceModeEntity(None, None, None, None, None, None)

    time_range_list = []
    if "period" in _data["schedule"] and type(_data["schedule"]["period"]) == list:
        for time_range_json in _data["schedule"]["period"]:
            time_range_list.append( TimeRangeEntity(time_range_json["start_time"], time_range_json["end_time"]) )

    if "mode" not in _data["schedule"]:
        raise NoRequiredError("mode")
    start_mode = _data["schedule"]["mode"]

    if start_mode != "off" and len(time_range_list) == 0:
        raise NoRequiredError("period")

    if "week_days" not in _data["schedule"]:
        auto_start_week_day = None
    elif _data["schedule"]["week_days"] == None:
        auto_start_week_day = None
    else:
        auto_start_week_day = 0
        for week_day_name in _data["schedule"]["week_days"]:
            week_day = data.get_weekday_index(week_day_name)
            auto_start_week_day |= 1 << week_day

    if "month_days" not in _data["schedule"]:
        auto_start_month_day = None
    elif _data["schedule"]["month_days"] == None:
        auto_start_month_day = None
    else:
        auto_start_month_day = 0
        for month_day in _data["schedule"]["month_days"]:
            auto_start_month_day |= 1 << month_day

    if "month_week_days" not in _data["schedule"]:
        first_week_day = None
        last_week_day = None
    else:
        if "first" not in _data["schedule"]["month_week_days"]:
            first_week_day = None
        else:
            first_week_day = 0
            for week_day_name in _data["schedule"]["month_week_days"]["first"]:
                week_day = data.get_weekday_index(week_day_name)
                first_week_day |= 1 << week_day

        if "last" not in _data["schedule"]["month_week_days"]:
            last_week_day = None
        else:
            last_week_day = 0
            for week_day_name in _data["schedule"]["month_week_days"]["last"]:
                week_day = data.get_weekday_index(week_day_name)
                last_week_day |= 1 << week_day

    return AutoStartConferenceModeEntity(time_range_list, start_mode, auto_start_week_day,
                                         auto_start_month_day, first_week_day, last_week_day)


def export_repeate_conf_mode_entity(_repeate_mode: RepeateConferenceModeEntity, _out_json: dict):
    if "schedule" not in _out_json:
        _out_json["schedule"] = {}
    _out_json["schedule"]["repeat_stop_enabled"] = _repeate_mode.repeate_mode != "off"
    if "repeat_stop" not in _out_json["schedule"]:
        _out_json["schedule"]["repeat_stop"] = {}
    _out_json["schedule"]["repeat_stop"]["mode"] = _repeate_mode.repeate_mode
    _out_json["schedule"]["repeat_stop"]["counter"] = _repeate_mode.repeate_count
    _out_json["schedule"]["repeat_stop"]["date"] = _repeate_mode.repeate_last_time


def import_repeate_conf_mode_entity(_data: dict) -> RepeateConferenceModeEntity:
    if "schedule" not in _data:
        return RepeateConferenceModeEntity(None, None, None)

    if "repeat_stop_enabled" not in _data["schedule"]:
        return RepeateConferenceModeEntity(None, None, None)

    if _data["schedule"]["repeat_stop_enabled"]:
        if "repeat_stop" not in _data["schedule"]:
            raise NoRequiredError("repeat_stop")

        if "mode" not in _data["schedule"]["repeat_stop"]:
            raise NoRequiredError("mode")
        repeate_mode = _data["schedule"]["repeat_stop"]["mode"]

        if "counter" not in _data["schedule"]["repeat_stop"]:
            repeate_count = None
        else:
            repeate_count = _data["schedule"]["repeat_stop"]["counter"]

        if "date" not in _data["schedule"]["repeat_stop"]:
            repeate_last_time = None
        else:
            repeate_last_time = _data["schedule"]["repeat_stop"]["date"]
    else:
        repeate_mode = "off"
        repeate_count = None
        repeate_last_time = None

    return RepeateConferenceModeEntity(repeate_mode, repeate_count, repeate_last_time)


def export_autostop_conf_entity(_auto_stop: AutoStopConferenceEntity, _out_json: dict):
    _out_json["auto_stop_enabled"] = _auto_stop.stop_mode != "off"
    if "auto_stop" not in _out_json:
        _out_json["auto_stop"] = {}
    _out_json["auto_stop"]["mode"] = _auto_stop.stop_mode
    _out_json["auto_stop"]["duration"] = _auto_stop.stop_duration
    _out_json["auto_stop"]["date"] = _auto_stop.stop_abs_time


def import_autostop_conf_entity(_data: dict) -> AutoStopConferenceEntity:
    if "auto_stop_enabled" not in _data:
        return AutoStopConferenceEntity(None, None, None)

    if _data["auto_stop_enabled"]:
        if "auto_stop" not in _data:
            raise NoRequiredError("auto_stop")

        if "mode" not in _data["auto_stop"]:
            raise NoRequiredError("mode")
        stop_mode = _data["auto_stop"]["mode"]

        if "duration" not in _data["auto_stop"]:
            stop_duration = None
        else:
            stop_duration = _data["auto_stop"]["duration"]

        if "date" not in _data["auto_stop"]:
            stop_abs_time = None
        else:
            stop_abs_time = _data["auto_stop"]["date"]
    else:
        stop_mode = "off"
        stop_duration = None
        stop_abs_time = None

    return AutoStopConferenceEntity(stop_mode, stop_duration, stop_abs_time)


def export_stop_time_table_entity(_stop_time_table: StopTimeTableEntity, _out_json: dict):
    if "schedule" not in _out_json:
        _out_json["schedule"] = {}
    _out_json["schedule"]["suspend_enabled"] = _stop_time_table.stop_time_table_mode != "off"
    if "suspend" not in _out_json["schedule"]:
        _out_json["schedule"]["suspend"] = {}
    _out_json["schedule"]["suspend"]["mode"] = _stop_time_table.stop_time_table_mode
    _out_json["schedule"]["suspend"]["from"] = _stop_time_table.stop_time_table_begin
    _out_json["schedule"]["suspend"]["to"] = _stop_time_table.stop_time_table_end


def import_stop_time_table_entity(_data: dict) -> StopTimeTableEntity:
    if "schedule" not in _data:
        return StopTimeTableEntity(None, None, None)

    if "suspend_enabled" not in _data["schedule"]:
        return StopTimeTableEntity(None, None, None)

    if _data["schedule"]["suspend_enabled"]:
        if "suspend" not in _data["schedule"]:
            raise NoRequiredError("suspend")

        if "mode" not in _data["schedule"]["suspend"]:
            raise NoRequiredError("mode")
        stop_time_table_mode = _data["schedule"]["suspend"]["mode"]

        if "from" not in _data["schedule"]["suspend"]:
            stop_time_table_begin = None
        else:
            stop_time_table_begin = _data["schedule"]["suspend"]["from"]

        if "to" not in _data["schedule"]["suspend"]:
            stop_time_table_end = None
        else:
            stop_time_table_end = _data["schedule"]["suspend"]["to"]
    else:
        stop_time_table_mode = "off"
        stop_time_table_begin = None
        stop_time_table_end = None

    return StopTimeTableEntity(stop_time_table_mode, stop_time_table_begin, stop_time_table_end)


def export_conference_entity(conference: ConferenceEntity) -> dict:
    conf_json = {
        'id': conference.confid,
        'name': conference.name,
        'infinity': conference.infinity,
        'start_time': 3*3600, #conference.start_time, # legacy
        'last_start_time': conference.last_start_time,
        'end_time': 3*3600, #conference.end_time, # legacy
        'duration': conference.duration,
        'created_at': conference.created_at,
        'max_participants': conference.max_participants,
        'usepin': conference.usepin,
        'pin': conference.pin,
        'autocall': conference.autocall,
        'rtmp': conference.rtmp,
        'record_participant': conference.record_participant,
        'state': conference.state,
        'is_recording': conference.is_recording,
        'layout_mode': conference.layout_mode,
        'participants_count': conference.participants_cnt,
        'audio': conference.audio,
        'video': conference.video
    }
    export_auto_start_mode_entity(conference.auto_start, conf_json)
    export_repeate_conf_mode_entity(conference.repeate, conf_json)
    export_stop_time_table_entity(conference.stop_time_table, conf_json)
    export_autostop_conf_entity(conference.auto_stop, conf_json)
    return conf_json


def export_conferences_list(conf_list: ConferencesListResponse) -> dict:
    return {
        'conferences': list(map(export_conference_entity, conf_list.conferences)),
        'size': conf_list.full_list_size
    }


def export_participant_entity(participant: ParticipantEntity) -> dict:
    contact_dict = export_contact_entity(participant.contact)
    return {
        "id": participant.contactid,
        "contact": contact_dict,
        "use_audio": participant.useaudio,
        "use_video": participant.usevideo,
        "call_ab": participant.callab,
        "volume": participant.volume,
        "use_audio_tx": participant.use_audio_tx,
        "volume_tx": participant.volume_tx,
        "use_video_tx": participant.use_video_tx,
        "use_pres": participant.use_pres,
        "render_mode": participant.render_mode,
        "call_protocol": participant.call_protocol,
        "noise_suppressor": participant.noise_suppressor,
        "auto_gain_ctrl": participant.auto_gain_ctrl
    }


def export_participants_list(participants_list: ConferenceParticipantsListResponse) -> dict:
    return {
        'participants': list(map(export_participant_entity, participants_list.participants))
    }


def export_deleted_participant(response: DeleteConferenceParticipantResponse) -> dict:
    return {
        "confid": response.confid,
        "contactid": response.contactid
    }


def export_session_participant_entity(participant: SessionParticipantEntity) -> dict:
    return {
        "address": participant.id,
        "id": participant.contact_id,
        "name": participant.name,
        "display_name": participant.dispname,
        "online": participant.online,
        "video_en": participant.video_en,
        "audio_en": participant.audio_en,
        "speaker": participant.speaker,
        "sound_vol": participant.sound_vol,
        "audio_call": participant.audio_call,
        "online_other_conf": participant.online_other_conf,
        "global_layout": participant.global_layout,
        "lyt_reg": participant.lyt_reg,
        "status": participant.status,
        "detail_status": participant.detail_status,
        "video_en_tx": participant.video_en_tx,
        "audio_en_tx": participant.audio_en_tx,
        "sound_vol_tx": participant.sound_vol_tx,
        "audio_power": participant.audio_power,
        "has_crown": participant.has_crown,
        "send_presentation": participant.send_presentation,
        "is_outgoing": participant.is_outgoing,
        "full_bitrate": participant.full_bitrate,
        "user_agent": participant.user_agent,
        "connection_id": participant.connection_id,
        "render_mode": participant.render_mode,
        "noise_suppressor": participant.noise_suppressor,
        "auto_gain_ctrl": participant.auto_gain_ctrl
    }


def export_disconnected_session_participant_entity(participant: DisconnectedSessionParticipantEntity) -> dict:
    return {
        "id": participant.contact_id,
        "name": participant.name,
        "display_name": participant.dispname,
        "address": participant.realuri,
        "autoadded": participant.autoadded
    }


def export_session_participants_list(participants_list: SessionParticipantsListResponse) -> dict:
    return {
        'participants': list(map(export_session_participant_entity, participants_list.participants)),
        'connecting': list(map(export_session_participant_entity, participants_list.connecting_participants)),
        'disconnected': list(map(export_disconnected_session_participant_entity,participants_list.disconnected_ab_list))
    }


def export_session_participant_addr_entity(addr: SessionParticipantAddressEntity) -> dict:
    return {
        "conference_id": addr.conference_id,
        "participant_id": addr.participant_uri
    }


def export_online_layout_abonent_entity(lyt_ab: OnlineLayoutAbonentEntity) -> dict:
    return {
        "position": lyt_ab.position,
        "participant_id": lyt_ab.participant_id,
        "display_name": lyt_ab.name,
        "in_layout": lyt_ab.in_layout,
        "role": layoutcfgtools.role_id_to_name(lyt_ab.role_id),
        "connection_id": lyt_ab.connection_id,
        "render_mode": lyt_ab.render_mode
    }


def export_online_layout_abonent_list(lyt_ab_list: list) -> list:
    return list(map(export_online_layout_abonent_entity, lyt_ab_list))


def import_online_layout_abonent_entity(ab_json: dict) -> OnlineLayoutAbonentEntity:
    if "position" not in ab_json:
        raise NoRequiredError("position")
    if "participant_id" not in ab_json:
        raise NoRequiredError("participant_id")
    if "role" not in ab_json:
        # TODO:raise NoRequiredError("role")
        role_id = layoutcfgtools.DEFAULT_EMPTY_ROLE_ID
    else:
        role_id = layoutcfgtools.role_name_to_id(ab_json["role"])
    if "connection_id" in ab_json:
        conn_id = ab_json["connection_id"]
        if conn_id is None:
            conn_id = -1
    else:
        conn_id = -1
    if "render_mode" not in ab_json:
        render_mode = "AUTO"
    else:
        render_mode = ab_json["render_mode"]
    return OnlineLayoutAbonentEntity(ab_json["position"], ab_json["participant_id"], "", True, role_id,
                                     conn_id, render_mode)


def import_online_layout_abonent_list(lyt_ab_list_json: list) -> list:
    result_list = []
    for ab_json in lyt_ab_list_json:
        result_list.append(import_online_layout_abonent_entity(ab_json))

    features_list = features.get_features_list()

    # validation of abonents list
    active_role_count = 0
    presentation_role_count = 0
    for ab in result_list:
        if ab.role_id == layoutcfgtools.ACTIVE_ROLE_ID:
            active_role_count += 1
            if not features.get_feature(features_list, "VAD"):
                raise FeatureRestrictedByLicenseError("VAD")
        if ab.role_id == layoutcfgtools.PRESENTATION_ROLE_ID:
            presentation_role_count += 1

    #if active_role_count > 1:
    #    raise TooManySlotRoles(layoutcfgtools.ACTIVE_ROLE_NAME)

    if presentation_role_count > 1:
        raise TooManySlotRoles(layoutcfgtools.PRESENTATION_ROLE_NAME)

    return result_list


def export_online_layout_response(resp) -> dict:
    result = {
        "name": resp.layout_name,
        "type": resp.layout_type,
        "auto": resp.auto,
        "display_name": resp.display_name.strip(),
        "template": resp.template,
        "grid": {
            "cells_count": resp.cells_count,
            "type": resp.schema_type,
            "slots": export_online_layout_abonent_list(resp.participant_positions)
        }
    }
    if type(resp) == IndividualOnlineLayoutResponse:
        result["participant_id"] = resp.participant_id
        result["participant_disp_name"] = resp.owner_disp_name
        result["connection_id"] = resp.connection_id
    return result


def export_online_layouts_list(response: OnlineLayoutsListResponse) -> dict:
    list_json = []
    for lyt in response.layouts_list:
        list_json.append(export_online_layout_response(lyt))
    return {"layouts": list_json}


def export_delete_online_layout_response(del_resp: DeleteOnlineLayoutResponse) -> dict:
    return {
        "conference_id": del_resp.confid,
        "layout_name": del_resp.layout_name
    }


def export_layout_type_entity(layout_type: LayoutTypeEntity):
    return {
        "lyt_type": layout_type.lyt_type,
        "name": layout_type.name,
        "picture": layout_type.picture
    }


def export_layout_row_entity(layout_row: LayoutRowEntity):
    result = {"ab_num": layout_row.ab_num, "types_list": []}
    for lyt_type in layout_row.types_list:
        result["types_list"].append(export_layout_type_entity(lyt_type))
    return result


def export_layout_types_list(types_list: GetAllLayoutsTypesResponse) -> dict:
    return {
        'layout_types': list(map(export_layout_row_entity, types_list.layout_types_list))
    }


def export_layout_abonent_position_entity(layout_ab_item: LayoutAbonentPositionEntity):
    return {
        "position": layout_ab_item.wndnum,
        "contact_id": layout_ab_item.abid,
        "priority": data.get_priority_name(layout_ab_item.priority),
        "role": layoutcfgtools.role_id_to_name(layout_ab_item.role_id),
        "render_mode": layout_ab_item.render_mode
    }


def export_sheduled_layout_entity(layout):
    result = {
        "id": layout.layoutid,
        "type": layout.layout_type,
        "auto": layout.auto,
        "display_name": layout.display_name.strip(),
        "template": layout.template,
        "grid": {
            "cells_count": layout.cells_count,
            "type": layout.schema_type,
            "slots": []
        }
    }
    for wnd in layout.wndlist:
        result["grid"]["slots"].append(export_layout_abonent_position_entity(wnd))
    if layout.layout_type == "individual":
        result["participant_id"] = layout.participant_id
    return result


def export_sheduled_layouts_list(lyt_list) -> dict:
    return {
        'layouts': list(map(export_sheduled_layout_entity, lyt_list))
    }


def import_layout_abonent_position_entity(lyt_ab_pos_json):
    if "position" not in lyt_ab_pos_json:
        raise NoRequiredError("position")
    if "contact_id" not in lyt_ab_pos_json:
        raise NoRequiredError("contact_id")
    if "priority" not in lyt_ab_pos_json:
        raise NoRequiredError("priority")
    if "role" not in lyt_ab_pos_json:
        # TODO:raise NoRequiredError("role")
        role_id = layoutcfgtools.DEFAULT_EMPTY_ROLE_ID
    else:
        role_id = layoutcfgtools.role_name_to_id(lyt_ab_pos_json["role"])
    if "render_mode" not in lyt_ab_pos_json:
        render_mode = "AUTO"
    else:
        render_mode = lyt_ab_pos_json["render_mode"]
    return LayoutAbonentPositionEntity(lyt_ab_pos_json["position"], lyt_ab_pos_json["contact_id"],
                                       data.get_priority_by_name(lyt_ab_pos_json["priority"]), role_id, render_mode)


def import_scheduled_layout_abonent_list(lyt_ab_list_json: list) -> list:
    result_list = []
    for ab_json in lyt_ab_list_json:
        result_list.append(import_layout_abonent_position_entity(ab_json))

    features_list = features.get_features_list()

    # validation of abonents list
    active_role_count = 0
    presentation_role_count = 0
    for ab in result_list:
        if ab.role_id == layoutcfgtools.ACTIVE_ROLE_ID:
            active_role_count += 1
            if not features.get_feature(features_list, "VAD"):
                raise FeatureRestrictedByLicenseError("VAD")
        if ab.role_id == layoutcfgtools.PRESENTATION_ROLE_ID:
            presentation_role_count += 1

    #if active_role_count > 1:
    #    raise TooManySlotRoles(layoutcfgtools.ACTIVE_ROLE_NAME)

    if presentation_role_count > 1:
        raise TooManySlotRoles(layoutcfgtools.PRESENTATION_ROLE_NAME)

    return result_list


def export_delete_scheduled_layout_response(del_resp: DeleteSheduledLayoutResponse) -> dict:
    return {"layout_id": del_resp.layout_id}


def export_group_entity(group: GroupEntity) -> dict:
    return {
        'id': group.group_id,
        'name': group.name,
        'abonents_count': group.ab_cnt
    }


def export_groups_list(groups_list) -> dict:
    return {
        'groups': list(map(export_group_entity, groups_list))
    }


def export_del_contact_from_group_response(del_response: DeleteContactFromGroupResponse) -> dict:
    return {
        "group_id": del_response.group_id,
        "contact_id": del_response.contactid
    }


def export_registration_result_entity(reg_result: RegistrationResultEntity) -> dict:
    return {
        'registered': reg_result.registered,
        'error_code': reg_result.err_code,
        'error_message': reg_result.err_msg
    }


def export_license_entity(license: LicenseEntity) -> dict:
    return {
        'state': license.state,
        'type': license.type,
        'first_expired': license.exp_first,
        'last_expired': license.exp_last,
        'count': license.count,
        'serial_id': license.serial,
        'features': license.features,
        'max_participants': license.max_participants,
        'max_active_participants': license.max_active_participants,
        'max_layouts': license.max_layouts,
        'demo': license.demo,
        'last_update_failed': license.last_upd_fail,
        'domain': license.domain,
        'license_params': license.license_params
    }


def export_calls_entity(calls: CallsEntity) -> dict:
    return {
        'remote_camera_control': calls.remote_camera_control,
        'enable_sip': calls.enable_sip,
        'enable_h323': calls.enable_h323,
        'preferred_protocol': calls.preferred_protocol,
        'mcu_display_name': calls.mcu_display_name,
        'call_sequence': calls.call_sequence,
        'external_ip': calls.external_ip,
        'audio_call': calls.audio_call,
        'enable_noise_suppressor': calls.enable_noise_suppressor,
        'enable_auto_gain_ctrl': calls.enable_auto_gain_ctrl
    }


def export_sip_entity(sip: SipEntity) -> dict:
    parsed_turn_addr = calls_settings.parse_turn_address(sip.turn_addr)
    return {
        "register": sip.register,
        "server": sip.server,
        "username": sip.username,
        "password": sip.password,
        "transport_protocol": sip.transport_protocol,
        "registration": sip.registration,
        "bfcp_transport": sip.sip_bfcp,
        "use_ice": sip.use_ice,
        "turn_username": parsed_turn_addr["username"],
        "turn_password": parsed_turn_addr["password"],
        "turn_hostname": parsed_turn_addr["hostport"],
        "sip_port": sip.sip_port,
        "bfcp_port_min": sip.bfcp_port_min,
        "bfcp_port_max": sip.bfcp_port_max
    }


def export_h323_entity(h323: H323SettingsEntity) -> dict:
    return {
        "use_gatekeeper": h323.use_gatekeeper,
        "id_number": h323.id_num,
        "e164_ext": h323.e164_extension,
        "server": h323.server,
        "zone": h323.zone,
        "registration": h323.registration,
        "h46017": h323.h46017,
        "h46018": h323.h46018,
        "h46019": h323.h46019,
        "h323_port": h323.h323_port,
        "h245_port_min": h323.h245_port_min,
        "h245_port_max": h323.h245_port_max
    }


def export_video_entity(video: VideoSettingsEntity) -> dict:
    return {
        "video_storage_size": video.video_storage_size,
        "stream1_priority": video.stream1_priority,
        "stream2_priority": video.stream2_priority,
        "content_wide_band": video.content_wide_band,
        "adapt_bitrate": video.adapt_bitrate,
        "stream_control": video.stream_control,
        "packet_recovery": video.packet_recovery,
        'background': video.background,
        'rolltimeout': video.rolltimeout
    }


def export_ldap_entity(ldap: LDAPSettingsEntity) -> dict:
    return {
        "server": ldap.server,
        "port": ldap.port,
        "distinct_name": ldap.distinct_name,
        "filter": ldap.filter
    }


def export_email_entity(email: EmailSettingsEntity) -> dict:
    return {
        "smtp_server": email.smtp_server,
        "username": email.username,
        "password": email.password
    }


def export_db_entity(db: DBSettingsEntity) -> dict:
    return {
        "backup_place": db.backup_place,
        "shared_dir_url": db.shared_dir_url,
        "username": db.username,
        "password": db.password,
        "backup_type": db.backup_type,
        "weekday": db.weekday,
        "hour": db.hour,
        "minute": db.minute
    }


def export_cpu_entity(cpu: InstanceCPUResponse) -> dict:
    res = {"count_of_cores": cpu.count}

    cores = []
    for core in cpu.cores:
        core_res = {
            "processes_of_user": core.user,
            "system_using": core.system,
            "idle": core.idle
        }
        cores.append(core_res)

    res["loading"] = cores
    return res


def export_network_entity(net: InstanceNetResponse) -> dict:
    res = {"count_of_interfaces": net.count}

    interfaces = []
    for interface in net.interfaces:
        net_res = {
            "name": interface.name,
            "rx": interface.rx,
            "tx": interface.tx
        }
        interfaces.append(net_res)

    res["interfaces"] = interfaces
    return res


def export_memory_entity(memory: InstanceMemoryResponse) -> dict:
    return {
        "all": memory.memory.all,
        "using": memory.memory.using,
        "free": memory.memory.free
    }


def export_free_space_entity(sp: InstanceFreeSpaceResponse) -> dict:
    res = {"count_of_devices": sp.count}

    devices = []
    for device in sp.devices:
        dev_res = {
            "name": device.name,
            "free": device.free,
            "total": device.total
        }
        devices.append(dev_res)

    res["spaces"] = devices
    return res


def export_string_record_entity(record: StringRecordEntity) -> dict:
    return {"id": record.id, "name": record.name}


def export_all_resolutions_list(res_list: AllResolutionsListResponse) -> dict:
    return {
        'resolutions': list(map(export_string_record_entity, res_list.resolutions))
    }


def export_layout_wnd_coord_entity(layout_wnd: LayoutWindowCoordEntity) -> dict:
    return {
        "position": layout_wnd.position,
        "x": layout_wnd.x,
        "y": layout_wnd.y,
        "width": layout_wnd.width,
        "height": layout_wnd.height,
        "z_order": layout_wnd.z_order
    }


def export_layout_wnd_coord_list(layout_wnd_list: CalculateLayoutResponse) -> dict:
    return {
        'grid': list(map(export_layout_wnd_coord_entity, layout_wnd_list.wnd_list))
    }


def export_session_participant_control_response(session_participant: SessionParticipantControlResponse) -> dict:
    return {
        "conference_id": session_participant.conference_id,
        "participant_id": session_participant.participant_id
    }


def export_session_group_operation_response(response: SessionGroupOperationResponse) -> dict:
    return {
        'conference_id': response.conference_id,
        'processed_obj_count': response.processed_obj_count,
        'failed_obj_count': response.failed_obj_count,
        'all_obj_count': response.processed_obj_count + response.failed_obj_count
    }


def export_session_control_response(resp: SessionControlResponse) -> dict:
    return {
        "conference_id": resp.conference_id
    }


def export_move_to_conf_response(resp: MoveToOtherConfResponse) -> dict:
    return {
        "conf_from": resp.conf_from,
        "conf_to": resp.conf_to,
        "participant_uri": resp.abonent_uri
    }


def export_waiting_abonent_entity(abonent: WaitAbonentEntity) -> dict:
    return {
        "address": abonent.address,
        "status": abonent.status,
        "connection_id": abonent.conn_id,
        "name": abonent.name
    }


def export_waiting_abonents_list(ab_list: WaitingAbonentsListResponse) -> dict:
    return {
        'participants': list(map(export_waiting_abonent_entity, ab_list.abonents))
    }


def export_recording_status_response(response: GetRecordingStatusResponse) -> dict:
    result = {"status": response.status, "urls": []}
    for urldata in response.urls:
        result["urls"].append(urldata.type)
    return result


def export_file_entity(fe: FileEntity) -> dict:
    return {
        "file_name": fe.filename,
        "file_size": fe.size,
        "create_time": fe.create_time_sec
    }


def export_files_list(files_list: GetFilesListResponse) -> dict:
    return {
        'files': list(map(export_file_entity, files_list.flist))
    }


def export_handling_file_response(response: HandlingFileResponse) -> dict:
    return {
        "filename": response.filename
    }


def export_import_abook_response(response: ImportAddressBookResponse) -> dict:
    return {
        "filename": response.filename,
        "imported_contacts": response.imported,
        "failed_contacts": response.failed,
        "all_contacts": response.all
    }


def export_switch_presentation_response(response: SwitchPresentationResponse) -> dict:
    return {
        "filename": response.filename
    }


def export_presentation_status_response(response: GetPresentationStatusResponse) -> dict:
    return {
        "status": {"switched_on": response.switched_on, "filename": response.filename}
    }


def export_stop_incoming_presentation_response(response: StopIncomingPresentationResponse) -> dict:
    return {
        "conference_id": response.conference_id
    }


def export_conf_session_participant_entity(record: ConfSessionParticipantEntity) -> dict:
    return {
        "conf_id": record.conf_id,
        "contact_id": record.contact_id,
        "address": record.address,
        "name": record.name,
        "connection_id": record.conn_id
    }


def export_all_conf_participants_list(response: AllConfSessionParticipantsResponse) -> dict:
    return {
        'participants': list(map(export_conf_session_participant_entity, response.participants_list))
    }


def export_server_time(response: ServerTimeResponse) -> dict:
    return {
        'time': response.time_sec
    }


def export_mcu_params_response(response: MCUParamsResponse) -> dict:
    return {
        'server_name': response.server_name,
        'version': response.version,
        'time': response.time_sec,
        'diagnostics': response.diagnostics,
        'engine_connected': response.engine_connected,
        'db_connected': response.db_connected,
        'edit_network': response.edit_network
    }


def export_preview_state_response(response: GetPreviewStateResponse) -> dict:
    return {
        'state': response.state
    }


def export_start_util_response(response: StartUtilResponse) -> dict:
    return {
        "util_type": response.util_type
    }


def export_util_response(response: UtilResponse) -> dict:
    return {
        "output": response.output,
        "error": response.error,
        "running": response.running
    }


def export_set_crown_response(response: SetCrownResponse) -> dict:
    return {
        "conference_id": response.confid,
        "participant_id": response.contactid
    }


def export_delete_crown_response(response: DeleteCrownResponse) -> dict:
    return {
        "conference_id": response.confid
    }


def export_rtp_entity(rtp: RTPEntity) -> dict:
    ip_presedence_audio = profile_settings.dscp_to_ip_presedence(rtp.dscp_audio)
    ip_presedence_video = profile_settings.dscp_to_ip_presedence(rtp.dscp_video)
    ip_presedence_other = profile_settings.dscp_to_ip_presedence(rtp.dscp_other)
    return {
        'rtpportmin': rtp.rtpportmin,
        'rtpportmax': rtp.rtpportmax,
        'mtu': rtp.mtu,
        'dscp_audio': rtp.dscp_audio,
        'dscp_video': rtp.dscp_video,
        'dscp_other': rtp.dscp_other,
        'ip_presedence_audio': ip_presedence_audio,
        'ip_presedence_video': ip_presedence_video,
        'ip_presedence_other': ip_presedence_other
    }


def export_ip_mask_entity(obj: IPAddressMaskEntity) -> dict:
    return {
        "ipaddress": obj.ipaddr,
        "netmask": obj.netmask
    }


def export_ip_mask_list(ip_mask_list: list) -> list:
    """convert list of objects IPAddressMaskEntity to list of dictionary { 'ipaddress': <ip>, 'netmask': <mask> }"""
    return list(map(export_ip_mask_entity, ip_mask_list))


def import_ip_mask_entity(ip_mask_dict: dict) -> IPAddressMaskEntity:
    if "ipaddress" not in ip_mask_dict:
        raise NoRequiredError("ipaddress")

    if "netmask" not in ip_mask_dict:
        raise NoRequiredError("netmask")

    res = networksettings.check_ip_mask(ip_mask_dict["ipaddress"], ip_mask_dict["netmask"])
    if res == networksettings.BAD_IP_ADDRESS:
        raise BadFormatFieldError("ipaddress", ip_mask_dict["ipaddress"], "num.num.num.num")
    elif res == networksettings.BAD_NETWORK_MASK:
        raise BadFormatFieldError("netmask", ip_mask_dict["netmask"], "num.num.num.num")
    elif res == networksettings.OTHER_ERROR:
        raise BadFormatFieldError("ipaddress/netmask",
                                  "%s/%s" % (ip_mask_dict["ipaddress"], ip_mask_dict["netmask"]),
                                  "num.num.num.num")

    return IPAddressMaskEntity(ip_mask_dict["ipaddress"], ip_mask_dict["netmask"])


def import_ip_mask_list(ip_mask_dict_list: list) -> list:
    """convert list of dictionary { 'ipaddress': <ip>, 'netmask': <mask> } to list of objects IPAddressMaskEntity"""
    return list(map(import_ip_mask_entity, ip_mask_dict_list))


def export_network_settings_entity(obj: NetworkSettingsEntity) -> dict:
    result = {
        "interface": obj.ifname,
        "mac": obj.mac,
        "use_dhcp": obj.use_dhcp,
        "gateway": obj.gateway,
        "nameservers": obj.nameservers,
        "ready": obj.ready
    }
    result["ipaddr_list"] = export_ip_mask_list(obj.ipaddr_list)
    return result


def import_network_settings_entity(iface: str, settings: dict) -> NetworkSettingsEntity:
    # check fields:
    if "use_dhcp" not in settings:
        raise NoRequiredError("use_dhcp")

    if settings["use_dhcp"]:
        ip_list = []
        gateway = ""
        dns_list = []
    else:
        fields_list = ["ipaddr_list", "gateway", "nameservers"]
        for field in fields_list:
            if field not in settings:
                raise NoRequiredError(field)
        ip_list = settings["ipaddr_list"]
        if len(ip_list) == 0:
            raise NoRequiredError("ipaddr_list")
        gateway = settings["gateway"]
        dns_list = settings["nameservers"]

    for dns in dns_list:
        if not networksettings.is_valid_ip(dns):
            raise BadFormatFieldError("nameservers", dns, "[ 'num.num.num.num', ... ]")

    ipmask_obj_list = import_ip_mask_list(ip_list)
    return NetworkSettingsEntity(iface, "00:00:00:00:00:00", settings["use_dhcp"], ipmask_obj_list, gateway, dns_list,
                                 True, "", "")


def export_network_settings_list(net_settings_list: list) -> list:
    return list(map(export_network_settings_entity, net_settings_list))


def export_can_edit_network_settings_response(response: CanChangeNetworkSettingsResponse):
    return {"can_edit": response.can_edit}


def export_vengine_log_entity(obj: GetVEngineLogResponse) -> dict:
    return {"venginelog": obj.log}


def export_vengine_command_response(response: VEngineCommandResponse) -> dict:
    return {"sent_command": response.command}


def export_mcu_references_response(response: MCUReferencesResponse) -> dict:
    return {"references": response.references}


def export_clean_abook_response(response: CleanAddressBookResponse) -> dict:
    return { "success": response.success }


def export_layout_type_slot_entity(slot: CustomLayoutTypeSlotEntity) -> dict:
    return {
        "x": slot.x,
        "y": slot.y,
        "width": slot.width,
        "height": slot.height
    }


def export_layout_type_slots_list(slots_list: list) -> list:
    return list(map(export_layout_type_slot_entity, slots_list))


def export_custom_layout_type(custom_type: CustomLayoutTypeEntity) -> dict:
    result = {
        "schema_type": custom_type.layout_type,
        "width": custom_type.width,
        "height": custom_type.height,
        "centered_row": custom_type.centered_row,
        "selected_slots": export_layout_type_slots_list(custom_type.selected_slots)
    }
    result["max_slots_count"] = customlayouttypes.get_custom_layout_max_slots_count(custom_type)
    return result


def export_custom_layout_types_list(custom_types_list: list) -> list:
    return list(map(export_custom_layout_type, custom_types_list))


def import_layout_type_slot_entity(slot_json: dict) -> CustomLayoutTypeSlotEntity:
    if "x" not in slot_json:
        raise NoRequiredError("x")
    if "y" not in slot_json:
        raise NoRequiredError("y")
    if "width" not in slot_json:
        raise NoRequiredError("width")
    if "height" not in slot_json:
        raise NoRequiredError("height")

    slot = CustomLayoutTypeSlotEntity(slot_json["x"], slot_json["y"], slot_json["width"], slot_json["height"])
    return slot


def import_layout_type_slots_list(full_width: int, full_height: int, slots_list_json: list) -> list:
    result_list = []
    for slot_json in slots_list_json:
        result_list.append(import_layout_type_slot_entity(slot_json))

    for slot in result_list:
        if slot.x < 0 or slot.x >= full_width:
            raise ValueNotInLimitsError("slot.x", str(slot.x), "[0 .. %d]" % (full_width))
        if slot.y < 0 or slot.y >= full_height:
            raise ValueNotInLimitsError("slot.y", str(slot.y), "[0 .. %d]" % (full_height))
        if slot.width <= 0 or slot.width > full_width:
            raise ValueNotInLimitsError("slot.width", str(slot.width), "[0 .. %d]" % (full_width))
        if slot.height <= 0 or slot.height > full_height:
            raise ValueNotInLimitsError("slot.x", str(slot.height), "[0 .. %d]" % (full_height))

    slots_count = len(result_list)
    i = 0
    while i < slots_count-1:
        j = i + 1
        while j < slots_count:
            slot_i = result_list[i]
            slot_j = result_list[j]
            if customlayouttypes.is_rectangles_intersected(slot_i.x, slot_i.y, slot_i.width, slot_i.height,
                                                           slot_j.x, slot_j.y, slot_j.width, slot_j.height):
                raise CustomLayoutTypeSlotsIntersectError("slot")
            j += 1
        i += 1

    return result_list

def export_delete_custom_layout_type(response: DeleteCustomLayoutTypeResponse) -> dict:
    return { "schema_type": response.layout_type }


def export_restart_command_response(response: RestartCommandResponse) -> dict:
    return {"success": response.success}


def export_install_deb_response(response: InstallDebResponse) -> dict:
    return {"filename": response.filename}


def export_install_deb_state_response(response: GetInstallDebStateResponse) -> dict:
    return {"state": response.state}


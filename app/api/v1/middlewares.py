import logging
import sys
import time
import os
import os.path

import jwt
from aiohttp import web

from app.api.v1.apierrors import response_error
from app.api.v1.errors import AuthorizationError
from mcu.postgresql.schemas.oauth import get_access_token

shared_key = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsuSfXS5pRgzQRDf/3oyu
H2jdpM7aVgTBZxRXlZdt5A5Ossxeg7oI/Vsv2Cb+2cTR7GD+O7mbKceYgxUejIAU
YBwyIZt7Y5bKN49CB2mUBgkO74eZzfFR0rU6HWoK4WZOkvvfDXlGMwvQ972LTFoe
ly8yVHQ+r5Fkb/aJF9yNZOrnbuqA18kA+lEJYtGzt/N71roDfSgveKnxEv+2vOOc
C/TdK86NhSXzk/5LSjRRQyUxW9DGvdWx+zQ6HdqAwYL9eWmlQmT3ojq6+SbVI2Ux
M8pVeAARUdy06bLVS6HtfFTaeOpS+l0L7eWbmaziJsZCgsMdSIU/j4uwmgwNsatm
YwIDAQAB
-----END PUBLIC KEY-----"""


def _json_401_response() -> web.Response:
    return web.json_response(status=401, data={'error': {
        'code': 401,
        'message': '401 Unauthorized'
    }})


def load_shared_key():
    global shared_key

    if "TCMCU_WEB_JWT_PUB" not in os.environ:
        logging.error("load_shared_key(): environment variable TCMCU_WEB_JWT_PUB is not found")
        return False

    filename = os.environ["TCMCU_WEB_JWT_PUB"]
    if not os.path.exists(filename):
        logging.error("load_shared_key(): file %s with public key is not found" % (filename))
        return False

    try:
        f = open(filename, "rb")
        loaded_key = f.read()
        f.close()

        loaded_key_str = loaded_key.decode("UTF-8")
        shared_key = loaded_key_str
    except:
        logging.error("load_shared_key() error loading authorization key: " + str(sys.exc_info()))
        return False

    return True


@web.middleware
async def authorization_middleware(request: web.Request, handler) -> web.Response:
    token = request.headers.get('authorization')
    #logging.info("authorization_middleware(): token: " + str(token))

    if token is None:
        return _json_401_response()

    token = token.replace('Bearer', '').strip()

    if token is "":
        return _json_401_response()

    try:
        data = jwt.decode(token, key=shared_key, algorithms=['RS256'], verify=False)
        #logging.info("authorization_middleware(): decoded token data: " + str(data))
    except jwt.InvalidTokenError:
        return _json_401_response()

    expire_at = data['exp']

    if time.time() > expire_at:
        return _json_401_response()

    try:
        params: get_access_token.Params = get_access_token.Params(access_token_id=data['jti'])
        select: get_access_token.Select = request.app[get_access_token.Select.get_class_name()]
        result: get_access_token.Result = await select.handle(params)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)

    if result is None:
        return _json_401_response()

    response: web.Response = await handler(request)
    return response


async def authorize_request(request: web.Request, token_in_url=False):
    global shared_key

    if token_in_url:
        token = str(request.query["token"])
    else:
        token = request.headers.get('authorization')
    logging.info("authorization_request(): token: " + str(token))

    if token is None:
        raise AuthorizationError("token is not found")

    token = token.replace('Bearer', '').strip()

    if token is "":
        raise AuthorizationError("token is empty")

    try:
        data = jwt.decode(token, key=shared_key, algorithms=['RS256'], verify=False)
        #logging.info("authorization_request(): decoded token data: " + str(data))
    except jwt.InvalidTokenError:
        raise AuthorizationError("decode token error")

    expire_at = data['exp']

    if time.time() > expire_at:
        raise AuthorizationError("token is expired")

    try:
        params: get_access_token.Params = get_access_token.Params(access_token_id=data['jti'])
        select: get_access_token.Select = request.app[get_access_token.Select.get_class_name()]
        result: get_access_token.Result = await select.handle(params)
    except Exception as ex:
        logging.error("authorization_request(): get token from db error: " + str(sys.exc_info()))
        raise AuthorizationError("get token from database error")

    if result is None:
        raise AuthorizationError("token is not found in database ")


async def handle_404(request: web.Request, response: web.Response = None) -> web.Response:
    if response is None:
        return web.json_response(status=404, data={'error': {
            'code': 404,
            'message': '404 Not Found'
        }})

    if not response.body:
        return web.json_response(status=404, data={'error': {
            'code': 404,
            'message': '404 Not Found'
        }})

    return response


async def handle_500(request: web.Request, response: web.Response = None) -> web.Response:
    if response is None:
        return web.json_response(status=500, data={'error': {
            'code': 500,
            'message': '500 Internal Server Error'
        }})

    if not response.body:
        return web.json_response(status=500, data={'error': {
            'code': 500,
            'message': '500 Internal Server Error'
        }})

    return response


def create_error_middleware(overrides):
    @web.middleware
    async def error_middleware(request, handler) -> web.Response:

        try:
            response = await handler(request)

            override = overrides.get(response.status)
            if override:
                return await override(request, response)

            return response

        except web.HTTPException as ex:
            override = overrides.get(ex.status)
            if override:
                return await override(request, None)

            raise

    return error_middleware


def setup_middlewares(app: web.Application) -> None:
    error_middleware = create_error_middleware({
        404: handle_404,
        500: handle_500
    })
    app.middlewares.append(error_middleware)
    #app.middlewares.append(authorization_middleware)
    load_shared_key()

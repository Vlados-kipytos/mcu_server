import sys

from aiohttp import MultipartReader
from aiohttp import hdrs
from aiohttp import web

from app.api.v1.errors import *
from app.api.v1.middlewares import authorize_request
from app.core.data import AUDIO_CALLS_LIST
from app.core.data import AUTO_GAIN_CTRL_NAMES
from app.core.data import AUTO_START_MODES_LIST
from app.core.data import AUTO_STOP_MODES_LIST
from app.core.data import BFCP_TRANSPORT_LIST
from app.core.data import CALL_PROTOCOLS_LIST
from app.core.data import LIST_UTILS
from app.core.data import NOISE_SUPPRESSOR_NAMES
from app.core.data import POWER_COMMANDS_LIST
from app.core.data import RENDER_PARTICIPANT_NAMES
from app.core.data import REPEATE_MODES_LIST
from app.core.data import STOP_TIME_TABLE_LIST
from app.core.data import get_background_names
from app.core.data import get_fecc_commands_list
from app.core.handlers import *
from app.core.requests import *
from .apierrors import generate_error_no_body_json
from .apierrors import response_error
from .response import *

import app.core.calls_settings as calls_settings
import app.core.profile_settings as profile_settings


def bool_field(name, data, is_req, default):
    str_name = str(name)
    if str_name in data:
        false_values = ['0', 'false', 'False']
        true_values = ['1', 'true', 'True']
        if str(data[str_name]) in true_values:
            return True
        elif str(data[str_name]) in false_values:
            return False
        else:
            raise ValueNotInLimitsError(str_name, data[str_name],
                                        " ['0', 'false', 'False', '1', 'true', 'True']")
    else:
        if is_req:
            raise NoRequiredError(str_name)
        else:
            return default


def string_field(name, data, down, up, is_req, default):
    str_name = str(name)
    if str_name in data:
        if data[str_name] is None:
            return None
        res = str(data[str_name])
        if len(res) < down or len(res) > up:
            raise BadStringLengthError(str_name, res, down, up)
    else:
        if is_req:
            raise NoRequiredError(str_name)
        else:
            res = default

    return res


def int_field(name, data, down, up, is_req, default):
    str_name = str(name)
    if str_name in data:
        if data[str_name] is None:
            return None
        if str(data[str_name]).isdigit():
            res = int(str(data[str_name]))
        else:
            raise BadIntInRequestError(str_name, data[str_name])
        if (up >= down) and (res > up or res < down) and (res != 0):
            raise ValueNotInLimitsError(str_name, data[str_name], "[%d .. %d]" % (down, up))
    else:
        if is_req:
            raise NoRequiredError(str_name)
        else:
            res = default

    return res


def enum_field(name, data, enum, is_req, default):
    str_name = str(name)
    if str_name in data:
        corr_values = enum
        if data[str_name] in corr_values:
            return data[str_name]
        else:
            raise ValueNotInLimitsError(str_name, data[str_name], str(enum))
    else:
        if is_req:
            raise NoRequiredError(str_name)
        else:
            return default


async def create_contact(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()

    try:
        await authorize_request(request)

        data = await request.json()

        ab_name = string_field("name", data, 1, 64, True, "")
        dispname = string_field("display_name", data, 0, 256, False, "")
        ab_sip_addr = string_field("sip", data, 0, 64, False, "")
        ab_h323_addr = string_field("h323", data, 0, 64, False, "")
        ab_email = string_field("email", data, 0, 64, False, "")
        ab_bitrate = int_field("bitrate", data, 64, 4096, False, 0)
        ab_rtsp = string_field("rtsp", data, 0, 80, False, "")
        ab_file = string_field("file", data, 0, 80, False, "")
        ab_loop = bool_field("loop", data, False, False)
        ab_realaddr = string_field("realaddr", data, 0, 80, False, "")
        ab_autoadded = bool_field("autoadded", data, False, False)

        if ab_sip_addr == "" and ab_h323_addr == "" and ab_rtsp == "" and ab_file == "":
            raise AllContactAddressesEmptyError()

        features_list = features.get_features_list()

        if "allowed_audio_codecs" in data and type(data["allowed_audio_codecs"]) == list:
            audio_codecs = AudioCodecsEntity(False)
            audio_codecs.init_by_list(data["allowed_audio_codecs"])
        else:
            audio_codecs = AudioCodecsEntity(True)
            audio_codecs.init_by_supported_fields(features_list)

        if "allowed_video_codecs" in data and type(data["allowed_video_codecs"]) == list:
            video_codecs = VideoCodecsEntity(False)
            video_codecs.init_by_list(data["allowed_video_codecs"])
        else:
            video_codecs = VideoCodecsEntity(True)
            video_codecs.init_by_supported_fields(features_list)

        core_request = CreateContactRequest(ab_name, dispname, ab_sip_addr, ab_h323_addr, ab_rtsp, ab_email, ab_bitrate,
                                            ab_realaddr, ab_autoadded, -1, audio_codecs, video_codecs, ab_file, ab_loop)

        core_handler = request.app[CreateContactHandler.__name__]

        core_response = await core_handler.handle(core_request)

        result = {"contact": export_contact_entity(core_response.contact)}

        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_contact_list(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        page = int_field("page", request.query, 0, 5000, False, 0)
        per_page = int_field("per_page", request.query, 1, 200, False, 60)
        if per_page == 0:
            raise ValueNotInLimitsError("per_page", "0", "[1 .. 200]")

        corr_values = ["id", "name", "sip", "h323", "rtsp", "email", "bitrate"]
        sort_by = enum_field("sort_by", request.query, corr_values, False, "display_name")
        corr_values = ['asc', 'desc']
        order_by = enum_field("order_by", request.query, corr_values, False, "asc")

        name = string_field("name", request.query, 0, 64, False, "")
        email = string_field("email", request.query, 0, 64, False, "")
        display_name = string_field("display_name", request.query, 0, 64, False, "")

        sip = string_field("sip", request.query, 0, 64, False, "")
        h323 = string_field("h323", request.query, 0, 64, False, "")
        rtsp = string_field("rtsp", request.query, 0, 64, False, "")
        autoadded = bool_field("autoadded", request.query, False, None)

        core_request = GetContactListRequest(page, per_page, sort_by, order_by, name, email, display_name, sip, h323,
                                             rtsp, autoadded)

        core_handler = request.app[GetContactListHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_contact_list(core_response))
    except Exception as e:
        logging.error(str(sys.exc_info()))
        return response_error(e)


async def get_contact_by_id(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        ab_id = int_field("contact_id", request.match_info, 0, -1, True, 0)

        core_request = GetContactRequest(ab_id)

        core_handler = request.app[GetContactHandler.__name__]

        core_response = await core_handler.handle(core_request)

        result = {"contact": export_contact_entity(core_response.contact)}

        return web.json_response(result)
    except Exception as e:
        logging.error(str(sys.exc_info()))
        return response_error(e)


async def edit_contact_by_id(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()

    try:
        await authorize_request(request)

        data = await request.json()

        ab_id = int_field("contact_id", request.match_info, 0, -1, True, 0)

        ab_name = string_field("name", data, 1, 64, True, None)
        dispname = string_field("display_name", data, 0, 256, False, None)
        ab_sip_addr = string_field("sip", data, 0, 64, False, None)
        ab_h323_addr = string_field("h323", data, 0, 64, False, None)
        ab_email = string_field("email", data, 0, 64, False, None)
        ab_bitrate = int_field("bitrate", data, 64, 4096, False, None)
        ab_rtsp = string_field("rtsp", data, 0, 80, False, None)
        ab_file = string_field("file", data, 0, 80, False, None)
        ab_loop = bool_field("loop", data, False, None)
        ab_realaddr = string_field("realaddr", data, 0, 80, False, None)
        ab_autoadded = bool_field("autoadded", data, False, None)

        if "allowed_audio_codecs" in data and type(data["allowed_audio_codecs"]) == list:
            audio_codecs = AudioCodecsEntity(False)
            audio_codecs.init_by_list(data["allowed_audio_codecs"])
        else:
            audio_codecs = None

        if "allowed_video_codecs" in data and type(data["allowed_video_codecs"]) == list:
            video_codecs = VideoCodecsEntity(False)
            video_codecs.init_by_list(data["allowed_video_codecs"])
        else:
            video_codecs = None

        contact = ContactEntity(ab_id, ab_name, dispname, ab_sip_addr, ab_h323_addr, ab_rtsp, ab_email, ab_bitrate, [],
                                ab_realaddr, ab_autoadded, None, None, audio_codecs, video_codecs, ab_file, ab_loop)
        core_request = ChangeContactRequest(contact)

        core_handler = request.app[ChangeContactHandler.__name__]

        core_response = await core_handler.handle(core_request)

        result = {"contact": export_contact_entity(core_response.contact)}

        return web.json_response(result)
    except Exception as e:
        logging.error(str(sys.exc_info()))
        return response_error(e)


async def delete_contact_by_id(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        ab_id = int_field("contact_id", request.match_info, 0, -1, True, 0)

        core_request = DeleteContactRequest(ab_id)

        core_handler = request.app[DeleteContactHandler.__name__]

        core_response = await core_handler.handle(core_request)

        result = export_id_entity(core_response.contactid)

        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_conference_by_id(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = GetConferenceRequest(conf_id)

        core_handler = request.app[GetConferenceHandler.__name__]

        core_response = await core_handler.handle(core_request)

        result = {"conference": export_conference_entity(core_response.conf)}

        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_conferences_list(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        page = int_field("page", request.query, 0, 5000, False, 0)
        per_page = int_field("per_page", request.query, 1, 200, False, 60)
        if per_page == 0:
            raise ValueNotInLimitsError("per_page", "0", "[1 .. 200]")

        corr_values = ['id', 'name', 'start_time', 'end_time', 'created_at']
        sort_by = enum_field("sort_by", request.query, corr_values, False, "created_at")

        corr_values = ['asc', 'desc']
        order_by = enum_field("order_by", request.query, corr_values, False, "asc")

        name = string_field("name", request.query, 0, 64, False, None)

        corr_values = ['inactive', 'start', 'active', 'closing']
        state = enum_field("state", request.query, corr_values, False, None)

        recording = bool_field("recording", request.query, False, None)

        core_request = GetConferencesListRequest(page, per_page, sort_by, order_by, name, state, recording)

        core_handler = request.app[GetConferencesListHandler.__name__]

        core_response = await core_handler.handle(core_request)

        result = export_conferences_list(core_response)

        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def create_conference(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()

    try:
        await authorize_request(request)

        data = await request.json()

        conf_name = string_field("name", data, 1, 64, True, None)
        usepin = bool_field("usepin", data, False, None)
        if usepin:
            pin = string_field("pin", data, 2, 10, usepin, None)
        else:
            pin = ""
        infinity = bool_field("infinity", data, False, None)
        #start_time = int_field("start_time", data, 0, -1, False, None)
        #end_time = int_field("end_time", data, 0, -1, False, None)

        auto_start = import_auto_start_mode_entity(data)
        repeate = import_repeate_conf_mode_entity(data)
        stop_time_table = import_stop_time_table_entity(data)
        auto_stop = import_autostop_conf_entity(data)

        max_participants = int_field("max_participants", data, 0, -1, False, None)
        autocall = bool_field("autocall", data, False, None)
        rtmp = string_field("rtmp", data, 0, 500, False, None)
        record_participant = int_field("record_participant", data, 0, -1, False, None)

        #if not infinity:
        #    if "start_time" not in data:
        #        raise NoRequiredError("start_time")
        #    if "end_time" not in data:
        #        raise NoRequiredError("end_time")

        if "audio" in data:
            if ("allowed_codecs" in data["audio"]) and (type(data["audio"]["allowed_codecs"]) == list):
                audio = data["audio"]
                audio["muteaudio"] = bool_field("muteaudio", data["audio"], False, None)
                audio["noise_suppressor"] = enum_field("noise_suppressor", data["audio"], NOISE_SUPPRESSOR_NAMES, False,
                                                       None)
                audio["auto_gain_ctrl"] = enum_field("auto_gain_ctrl", data["audio"], AUTO_GAIN_CTRL_NAMES, False, None)
            else:
                raise NoRequiredError("audio[allowed_codecs]")
        else:
            audio = None

        if "video" in data:
            video = {}
            video["max_bitrate"] = int_field("max_bitrate", data["video"], 0, 8192 * 8192, True, None)
            video["max_resolution"] = int_field("max_resolution", data["video"], 0, 8192 * 8192, True, None)
            if ("allowed_codecs" not in data["video"]) or type(data["video"]["allowed_codecs"]) != list:
                raise NoRequiredError("video[allowed_codecs]")
            video["allowed_codecs"] = data["video"]["allowed_codecs"]
            corr_values = RENDER_PARTICIPANT_NAMES
            video["show_full_participant"] = enum_field("show_full_participant", data["video"], corr_values, True, None)
            video["rec_at_start"] = bool_field("rec_at_start", data["video"], False, None)
            video["rec_autostop"] = bool_field("rec_autostop", data["video"], False, None)
            video["show_aud_pwr"] = bool_field("show_aud_pwr", data["video"], False, None)
            video["show_names"] = bool_field("show_names", data["video"], False, None)
            video["rec_to_file"] = bool_field("rec_to_file", data["video"], False, None)
            video["use_pres"] = bool_field("use_pres", data["video"], False, None)
            bk_names = get_background_names()
            video["background"] = enum_field("background", data["video"], bk_names, False, None)
            video["mutevideo"] = bool_field("mutevideo", data["video"], False, None)
        else:
            video = None

        core_request = CreateConferenceRequest(conf_name, usepin, pin, infinity,
                                               auto_start, repeate, stop_time_table, auto_stop,
                                               max_participants,
                                               autocall, rtmp, record_participant, audio, video)

        core_handler = request.app[CreateConferenceHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_conference_entity(core_response.conf))

    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def delete_conference_by_id(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = DeleteConferenceRequest(conf_id)

        core_handler = request.app[DeleteConferenceHandler.__name__]

        core_response = await core_handler.handle(core_request)

        result = export_id_entity(core_response.confid)

        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def edit_conference_by_id(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()

    try:
        await authorize_request(request)

        data = await request.json()

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        conf_name = string_field("name", data, 1, 64, False, None)
        usepin = bool_field("usepin", data, False, None)
        if usepin:
            pin = string_field("pin", data, 2, 10, usepin, None)
        else:
            pin = None
        infinity = bool_field("infinity", data, False, None)
        #start_time = int_field("start_time", data, 0, -1, False, None)
        #end_time = int_field("end_time", data, 0, -1, False, None)

        auto_start = import_auto_start_mode_entity(data)
        repeate = import_repeate_conf_mode_entity(data)
        stop_time_table = import_stop_time_table_entity(data)
        auto_stop = import_autostop_conf_entity(data)

        max_participants = int_field("max_participants", data, 0, -1, False, None)
        autocall = bool_field("autocall", data, False, None)
        rtmp = string_field("rtmp", data, 0, 500, False, None)
        record_participant = int_field("record_participant", data, 0, -1, False, None)

        corr_values = ['common', 'aspeaker']
        layout_mode = enum_field("layout_mode", data, corr_values, False, None)

        if "audio" in data:
            if ("allowed_codecs" in data["audio"]) and (type(data["audio"]["allowed_codecs"]) == list):
                audio = data["audio"]
                audio["muteaudio"] = bool_field("muteaudio", data["audio"], False, None)
                audio["noise_suppressor"] = enum_field("noise_suppressor", data["audio"], NOISE_SUPPRESSOR_NAMES, False,
                                                       None)
                audio["auto_gain_ctrl"] = enum_field("auto_gain_ctrl", data["audio"], AUTO_GAIN_CTRL_NAMES, False, None)
            else:
                raise BadFormatFieldError("audio", "", "{'allowed_codecs': [codecs]}")
        else:
            audio = None

        if "video" in data:
            video = {}
            video["max_bitrate"] = int_field("max_bitrate", data["video"], 0, 8192 * 8192, False, None)
            video["max_resolution"] = int_field("max_resolution", data["video"], 0, 8192 * 8192, False, None)
            if "allowed_codecs" in data["video"]:
                if type(data["video"]["allowed_codecs"]) != list:
                    raise BadFormatFieldError("video['allowed_codecs']", "", "[codecs]")
                else:
                    video["allowed_codecs"] = data["video"]["allowed_codecs"]
            else:
                video["allowed_codecs"] = None
            corr_values = RENDER_PARTICIPANT_NAMES
            video["show_full_participant"] = enum_field("show_full_participant", data["video"], corr_values, False,
                                                        None)
            video["rec_at_start"] = bool_field("rec_at_start", data["video"], False, None)
            video["rec_autostop"] = bool_field("rec_autostop", data["video"], False, None)
            video["show_aud_pwr"] = bool_field("show_aud_pwr", data["video"], False, None)
            video["show_names"] = bool_field("show_names", data["video"], False, None)
            video["rec_to_file"] = bool_field("rec_to_file", data["video"], False, None)
            video["use_pres"] = bool_field("use_pres", data["video"], False, None)
            bk_names = get_background_names()
            video["background"] = enum_field("background", data["video"], bk_names, False, None)
            video["mutevideo"] = bool_field("mutevideo", data["video"], False, None)
        else:
            video = None

        conf = ConferenceEntity(conf_id, conf_name, infinity, 0,
                                auto_start, repeate, stop_time_table, auto_stop,
                                0, "", max_participants,
                                usepin, pin, autocall, rtmp, record_participant, "inactive", False, layout_mode, 0,
                                audio,
                                video)
        core_request = ChangeConferenceRequest(conf)

        core_handler = request.app[ChangeConferenceHandler.__name__]

        core_response = await core_handler.handle(core_request)

        result = export_conference_entity(core_response.conf)

        return web.json_response(result)

    except Exception as e:
        logging.error(str(sys.exc_info()))
        return response_error(e)


async def get_conference_participants_list(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = GetConferenceParticipantsListRequest(conf_id)

        core_handler = request.app[GetConferenceParticipantsListHandler.__name__]

        core_response = await core_handler.handle(core_request)

        result = export_participants_list(core_response)

        return web.json_response(result)

    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def add_participant_to_conference(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()

        contact_id = int_field("contact_id", data, 0, -1, True, 0)
        use_audio = bool_field("use_audio", data, False, None)
        use_video = bool_field("use_video", data, False, None)
        call_ab = bool_field("call_ab", data, False, None)
        volume = int_field("volume", data, -400, 200, False, 0)
        use_audio_tx = bool_field("use_audio_tx", data, False, None)
        volume_tx = int_field("volume_tx", data, -400, 200, False, 0)
        use_video_tx = bool_field("use_video_tx", data, False, True)
        use_pres = bool_field("use_pres", data, False, True)
        render_mode = enum_field("render_mode", data, RENDER_PARTICIPANT_NAMES, False, "AUTO")
        call_protocol = enum_field("call_protocol", data, CALL_PROTOCOLS_LIST, False, "auto")
        noise_suppressor = enum_field("noise_suppressor", data, NOISE_SUPPRESSOR_NAMES, False, "DEFAULT")
        auto_gain_ctrl = enum_field("auto_gain_ctrl", data, AUTO_GAIN_CTRL_NAMES, False, "DEFAULT")

        core_request = CreateConferenceParticipantRequest(conf_id, contact_id, use_audio, use_video, call_ab, volume,
                                                          use_audio_tx, volume_tx, use_video_tx, use_pres, render_mode,
                                                          call_protocol, noise_suppressor, auto_gain_ctrl)

        core_handler = request.app[CreateConferenceParticipantHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response({"participant": export_participant_entity(core_response.participant)})

    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_conference_participant_by_id(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        contact_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        core_request = GetConferenceParticipantRequest(conf_id, contact_id)

        core_handler = request.app[GetConferenceParticipantHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response({"participant": export_participant_entity(core_response.participant)})

    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def change_conference_participant(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        contact_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        data = await request.json()

        use_audio = bool_field("use_audio", data, False, None)
        use_video = bool_field("use_video", data, False, None)
        call_ab = bool_field("call_ab", data, False, None)
        volume = int_field("volume", data, -400, 200, False, 0)
        use_audio_tx = bool_field("use_audio_tx", data, False, None)
        volume_tx = int_field("volume_tx", data, -400, 200, False, 0)
        use_video_tx = bool_field("use_video_tx", data, False, None)
        use_pres = bool_field("use_pres", data, False, None)
        render_mode = enum_field("render_mode", data, RENDER_PARTICIPANT_NAMES, False, "AUTO")
        call_protocol = enum_field("call_protocol", data, CALL_PROTOCOLS_LIST, False, "auto")
        noise_suppressor = enum_field("noise_suppressor", data, NOISE_SUPPRESSOR_NAMES, False, "DEFAULT")
        auto_gain_ctrl = enum_field("auto_gain_ctrl", data, AUTO_GAIN_CTRL_NAMES, False, "DEFAULT")

        core_request = ChangeConferenceParticipantRequest(conf_id, contact_id, use_audio, use_video, call_ab, volume,
                                                          use_audio_tx, volume_tx, use_video_tx, use_pres, render_mode,
                                                          call_protocol, noise_suppressor, auto_gain_ctrl)

        core_handler = request.app[ChangeConferenceParticipantHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response({"participant": export_participant_entity(core_response.participant)})

    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def delete_conference_participant(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        contact_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        core_request = DeleteConferenceParticipantRequest(conf_id, contact_id)

        core_handler = request.app[DeleteConferenceParticipantHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_deleted_participant(core_response))

    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def start_conference_session(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = StartConferenceSessionRequest(conf_id)

        core_handler = request.app[StartConferenceSessionHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_id_entity(core_response.confid))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def stop_conference_session(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = StopConferenceSessionRequest(conf_id)

        core_handler = request.app[StopConferenceSessionHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_id_entity(core_response.confid))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_session_participants_list(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = GetConferenceSessionParticipantsListRequest(conf_id)

        core_handler = request.app[GetConferenceSessionParticipantsListHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participants_list(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def invite_participants(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()

        call_id = string_field("call_id", data, 1, 80, True, None)

        core_request = InviteSessionParticipantRequest(SessionParticipantAddressEntity(conf_id, call_id))

        core_handler = request.app[InviteSessionParticipantHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participant_addr_entity(core_response.session_addr))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def invite_participant_by_id(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        core_request = InviteConferenceParticipantRequest(ConferenceParticipantAddressEntity(conf_id, participant_id))
        core_handler = request.app[InviteConferenceParticipantHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participant_control_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_session_participant(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        if request.can_read_body:
            data = await request.json()
            use_conn_id = bool_field("use_connection_id", data, False, False)
        else:
            use_conn_id = False

        core_request = GetConferenceSessionParticipantRequest(conf_id, participant_id, use_conn_id)

        core_handler = request.app[GetConferenceSessionParticipantHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response({"participant": export_session_participant_entity(core_response.participant)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def delete_participant(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        if request.can_read_body:
            data = await request.json()
            use_conn_id = bool_field("use_connection_id", data, False, False)
        else:
            use_conn_id = False

        core_request = HangupConferenceParticipantRequest(ConferenceParticipantAddressEntity(conf_id, participant_id), use_conn_id)
        core_handler = request.app[HangupConferenceParticipantHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participant_control_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_all_layout_types(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = GetAllLayoutsTypesRequest()

        core_handler = request.app[GetAllLayoutsTypesHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_layout_types_list(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_online_layout(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        layout_name = string_field("layout_name", request.match_info, 1, 32, True, "")

        core_request = GetOnlineLayoutRequest(conf_id, layout_name)
        core_handler = request.app[GetOnlineLayoutHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response({"layout": export_online_layout_response(core_response)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_online_layouts_list(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = GetOnlineLayoutsListRequest(conf_id)
        core_handler = request.app[GetOnlineLayoutsListHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_online_layouts_list(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def create_online_layout(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        auto = bool_field("auto", data, False, False)

        corr_values = ["individual", "active_speaker"]
        layout_type = enum_field("type", data, corr_values, False, "individual")

        if layout_type == "individual":
            participant_id = int_field("participant_id", data, 0, -1, True, 0)
            connection_id = int_field("connection_id", data, 0, -1, False, -1)
        else:
            participant_id = 0
            connection_id = -1

        if "grid" not in data:
            raise NoRequiredError("grid")

        if ("type" in data["grid"]) and (type(data["grid"]["type"]) == int):
            cells_count = int_field("cells_count", data["grid"], 0, -1, True, 0)
            schema_type = int_field("type", data["grid"], 0, -1, True, 0)
        else:
            raise NoRequiredError("'grid'.'type'")

        if ("slots" in data["grid"]) and (type(data["grid"]["slots"]) == list):
            participant_positions = import_online_layout_abonent_list(data["grid"]["slots"])
        else:
            raise NoRequiredError("'grid'.'slots'")

        core_request = CreateOnlineLayoutRequest(conf_id, participant_id, cells_count, schema_type,
                                                 participant_positions, auto, layout_type, connection_id, "", False)
        core_handler = request.app[CreateOnlineLayoutHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response({"layout": export_online_layout_response(core_response)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def delete_online_layout(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        layout_name = string_field("layout_name", request.match_info, 1, 32, True, "")

        core_request = DeleteOnlineLayoutRequest(conf_id, layout_name)
        core_handler = request.app[DeleteOnlineLayoutHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_delete_online_layout_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def change_online_layout(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        layout_name = string_field("layout_name", request.match_info, 1, 32, True, "")

        auto = bool_field("auto", data, False, False)

        if "grid" not in data:
            raise NoRequiredError("grid")

        if ("type" in data["grid"]) and (type(data["grid"]["type"]) == int):
            cells_count = int_field("cells_count", data["grid"], 0, -1, True, 0)
            schema_type = int_field("type", data["grid"], 0, -1, True, 0)
        else:
            raise NoRequiredError("'grid'.'type'")

        if ("slots" in data["grid"]) and (type(data["grid"]["slots"]) == list):
            participant_positions = import_online_layout_abonent_list(data["grid"]["slots"])
        else:
            raise NoRequiredError("'grid'.'slots'")

        core_request = ChangeOnlineLayoutRequest(conf_id, layout_name, cells_count, schema_type, participant_positions,
                                                 auto)
        core_handler = request.app[ChangeOnlineLayoutHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response({"layout": export_online_layout_response(core_response)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def create_online_layout_from_template(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        layout_template_id = int_field("layout_template_id", data, 0, -1, True, 0)

        corr_values = ["individual", "active_speaker", "global"]
        layout_type = enum_field("type", data, corr_values, True, "individual")

        if layout_type == "individual":
            participant_id = int_field("participant_id", data, 0, -1, True, 0)
            connection_id = int_field("connection_id", data, 0, -1, False, -1)
        else:
            participant_id = 0
            connection_id = -1

        core_request = CreateLayoutFromTemplateRequest(conf_id, layout_template_id, layout_type, participant_id,
                                                       connection_id)
        core_handler = request.app[CreateLayoutFromTemplateHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response({"layout": export_online_layout_response(core_response)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def create_template_from_online_layout(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        conf_id = int_field("conference_id", data, 0, -1, True, 0)
        layout_name = string_field("layout_name", data, 1, 64, True, "")
        display_name = string_field("display_name", data, 1, 64, False, "")

        core_request = CreateTemplateFromOnlineLayoutRequest(conf_id, layout_name, display_name)
        core_handler = request.app[CreateTemplateFromOnlineLayoutHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response({"layout": export_sheduled_layout_entity(core_response)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_scheduled_layout_by_id_ex(request: web.Request, template: bool) -> web.Response:
    try:
        await authorize_request(request)

        if template:
            conf_id = 0
        else:
            conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        layout_id = int_field("layout_id", request.match_info, 0, -1, True, 0)

        core_request = GetSheduledLayoutRequest(conf_id, layout_id)
        core_handler = request.app[GetSheduledLayoutHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response({"layout": export_sheduled_layout_entity(core_response)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_scheduled_layouts_list_ex(request: web.Request, template: bool) -> web.Response:
    try:
        await authorize_request(request)

        if template:
            conf_id = 0
        else:
            conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = GetSheduledLayoutsListRequest(conf_id, template)
        core_handler = request.app[GetSheduledLayoutsListHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_sheduled_layouts_list(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def create_scheduled_layout_ex(request: web.Request, template: bool) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()

    try:
        await authorize_request(request)

        if template:
            conf_id = 0
        else:
            conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()

        all_layout_types = ["global", "individual", "active_speaker"]
        layout_type = enum_field("type", data, all_layout_types, True, "global")

        auto = bool_field("auto", data, True, False)

        display_name = string_field("display_name", data, 0, 64, False, "")

        if "grid" not in data:
            raise NoRequiredError("grid")

        if ("type" in data["grid"]) and (type(data["grid"]["type"]) == int):
            cells_count = int_field("cells_count", data["grid"], 0, -1, True, 0)
            schema_type = int_field("type", data["grid"], 0, -1, True, 0)
        else:
            raise NoRequiredError("'grid'.'type'")

        if ("slots" in data["grid"]) and (type(data["grid"]["slots"]) == list):
            participant_positions = import_scheduled_layout_abonent_list(data["grid"]["slots"])
        else:
            raise NoRequiredError("'grid'.'slots'")

        if layout_type == "individual":
            owner_id = int_field("participant_id", data, 0, -1, True, 0)
            core_request = CreateIndividualLayoutRequest(conf_id, auto, cells_count, schema_type, participant_positions,
                                                         owner_id, display_name, template)
        elif layout_type == "global":
            core_request = CreateGlobalLayoutRequest(conf_id, auto, cells_count, schema_type, participant_positions,
                                                     display_name, template)
        elif layout_type == "active_speaker":
            core_request = CreateAspeakerLayoutRequest(conf_id, auto, cells_count, schema_type, participant_positions,
                                                       display_name, template)
        else:
            raise BadFormatFieldError("type", layout_type, "'global', 'individual' or 'active_speaker'")

        core_handler = request.app[CreateLayoutHandler.__name__]
        core_response = await core_handler.handle(core_request)
        result = {"layout": export_sheduled_layout_entity(core_response)}

        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def update_scheduled_layout_ex(request: web.Request, template: bool) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()

    try:
        await authorize_request(request)

        if template:
            conf_id = 0
        else:
            conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        layout_id = int_field("layout_id", request.match_info, 0, -1, True, 0)

        data = await request.json()

        auto = bool_field("auto", data, True, False)
        display_name = string_field("display_name", data, 0, 64, False, None)

        if "grid" not in data:
            raise NoRequiredError("grid")

        if ("type" in data["grid"]) and (type(data["grid"]["type"]) == int):
            cells_count = int_field("cells_count", data["grid"], 0, -1, True, 0)
            schema_type = int_field("type", data["grid"], 0, -1, True, 0)
        else:
            raise NoRequiredError("'grid'.'type'")

        if ("slots" in data["grid"]) and (type(data["grid"]["slots"]) == list):
            participant_positions = import_scheduled_layout_abonent_list(data["grid"]["slots"])
        else:
            raise NoRequiredError("'grid'.'slots'")

        core_request = ChangeSheduledLayoutRequest(layout_id, auto, cells_count, schema_type, participant_positions,
                                                   display_name)

        core_handler = request.app[ChangeLayoutHandler.__name__]
        core_response = await core_handler.handle(core_request)
        result = {"layout": export_sheduled_layout_entity(core_response)}

        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def delete_scheduled_layout_ex(request: web.Request, template: bool) -> web.Response:
    try:
        await authorize_request(request)

        if template:
            conf_id = 0
        else:
            conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        layout_id = int_field("layout_id", request.match_info, 0, -1, True, 0)

        core_request = DeleteSheduledLayoutRequest(conf_id, layout_id)
        core_handler = request.app[DeleteLayoutHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_delete_scheduled_layout_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_scheduled_layout_by_id(request: web.Request) -> web.Response:
    return await get_scheduled_layout_by_id_ex(request, False)


async def get_scheduled_layout_template_by_id(request: web.Request) -> web.Response:
    return await get_scheduled_layout_by_id_ex(request, True)


async def get_scheduled_layouts_list(request: web.Request) -> web.Response:
    return await get_scheduled_layouts_list_ex(request, False)


async def get_scheduled_layouts_templates_list(request: web.Request) -> web.Response:
    return await get_scheduled_layouts_list_ex(request, True)


async def create_scheduled_layout(request: web.Request) -> web.Response:
    return await create_scheduled_layout_ex(request, False)


async def create_scheduled_layout_template(request: web.Request) -> web.Response:
    return await create_scheduled_layout_ex(request, True)


async def update_scheduled_layout(request: web.Request) -> web.Response:
    return await update_scheduled_layout_ex(request, False)


async def update_scheduled_layout_template(request: web.Request) -> web.Response:
    return await update_scheduled_layout_ex(request, True)


async def delete_scheduled_layout(request: web.Request) -> web.Response:
    return await delete_scheduled_layout_ex(request, False)


async def delete_scheduled_layout_template(request: web.Request) -> web.Response:
    return await delete_scheduled_layout_ex(request, True)


async def create_custom_layout_type(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        data = await request.json()

        width = int_field("width", data, 1, 0x7fffffff, True, 1)
        height = int_field("height", data, 1, 0x7fffffff, True, 1)
        centered_row = int_field("centered_row", data, 0, height, False, None)

        slots_list = []
        if "selected_slots" in data:
            slots_list = import_layout_type_slots_list(width, height, data["selected_slots"])

        core_request = CreateCustomLayoutTypeRequest(width, height, centered_row, slots_list)
        core_handler = request.app[CreateCustomLayoutTypeHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response({"custom_schema_type": export_custom_layout_type(core_response.type)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def change_custom_layout_type(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        data = await request.json()

        schema_type = int_field("schema_type", request.match_info, 1, 0x7fffffff, True, 0)

        width = int_field("width", data, 1, 0x7fffffff, True, 1)
        height = int_field("height", data, 1, 0x7fffffff, True, 1)
        centered_row = int_field("centered_row", data, 0, height, False, None)

        slots_list = []
        if "selected_slots" in data:
            slots_list = import_layout_type_slots_list(width, height, data["selected_slots"])

        layout_type_entity = CustomLayoutTypeEntity(schema_type, width, height, centered_row, slots_list)
        core_request = ChangeCustomLayoutTypeRequest(layout_type_entity)
        core_handler = request.app[ChangeCustomLayoutTypeHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response({"custom_schema_type": export_custom_layout_type(core_response.type)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def delete_custom_layout_type(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        schema_type = int_field("schema_type", request.match_info, 1, 0x7fffffff, True, 0)

        core_request = DeleteCustomLayoutTypeRequest(schema_type)
        core_handler = request.app[DeleteCustomLayoutTypeHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_delete_custom_layout_type(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_custom_layout_type(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        schema_type = int_field("schema_type", request.match_info, 1, 0x7fffffff, True, 0)

        core_request = GetCustomLayoutTypeRequest(schema_type)
        core_handler = request.app[GetCustomLayoutTypeHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response({"custom_schema_type": export_custom_layout_type(core_response.type)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_all_custom_layout_types(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = GetAllCustomLayoutTypesRequest()
        core_handler = request.app[GetAllCustomLayoutTypesHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response({"custom_schema_types": export_custom_layout_types_list(core_response.types_list)})
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_groups_list(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        page = int_field("page", request.query, 0, 5000, False, 0)
        per_page = int_field("per_page", request.query, 1, 200, False, 60)
        if per_page == 0:
            raise ValueNotInLimitsError("per_page", "0", "[1 .. 200]")

        name = string_field("name", request.query, 0, 64, False, "")

        core_request = GetGroupsListRequest(page, per_page, name)
        core_handler = request.app[GetGroupsListHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_groups_list(core_response.groups))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_group_by_id(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        gr_id = int_field("group_id", request.match_info, 0, -1, True, 0)

        core_request = GetGroupRequest(gr_id)
        core_handler = request.app[GetGroupHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"group": export_group_entity(core_response.group)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def create_new_group(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()
        gr_name = string_field("name", data, 1, 32, True, "")

        core_request = CreateGroupRequest(gr_name)
        core_handler = request.app[CreateGroupHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"group": export_group_entity(core_response.group)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def update_group(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()
        gr_id = int_field("group_id", request.match_info, 0, -1, True, 0)
        gr_name = string_field("name", data, 1, 32, True, None)

        group = GroupEntity(gr_id, gr_name, 0)
        core_request = ChangeGroupRequest(group)
        core_handler = request.app[ChangeGroupHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"group": export_group_entity(core_response.group)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def delete_group_by_id(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        gr_id = int_field("group_id", request.match_info, 0, -1, True, 0)

        core_request = DeleteGroupRequest(gr_id)
        core_handler = request.app[DeleteGroupHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_id_entity(core_response.group_id)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_group_contacts_list(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        gr_id = int_field("group_id", request.match_info, 0, -1, True, 0)

        page = int_field("page", request.query, 0, 5000, False, 0)
        per_page = int_field("per_page", request.query, 1, 200, False, 60)
        if per_page == 0:
            raise ValueNotInLimitsError("per_page", "0", "[1 .. 200]")

        corr_values = ["id", "name", "sip", "h323", "rtsp", "email", "bitrate"]
        sort_by = enum_field("sort_by", request.query, corr_values, False, "id")
        corr_values = ['asc', 'desc']
        order_by = enum_field("order_by", request.query, corr_values, False, "asc")

        name = string_field("name", request.query, 0, 64, False, "")
        email = string_field("email", request.query, 0, 64, False, "")
        display_name = string_field("display_name", request.query, 0, 64, False, "")

        sip = string_field("sip", request.query, 0, 64, False, "")
        h323 = string_field("h323", request.query, 0, 64, False, "")
        rtsp = string_field("rtsp", request.query, 0, 64, False, "")
        autoadded = bool_field("autoadded", request.query, False, None)

        core_request = GetGroupContactListRequest(gr_id, page, per_page, sort_by, order_by, name, email, display_name,
                                                  sip, h323, rtsp, autoadded)
        core_handler = request.app[GetContactsOfGroupHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_contact_list(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def add_contact_to_group(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        gr_id = int_field("group_id", request.match_info, 0, -1, True, 0)

        data = await request.json()

        if ("contact_id_list" in data) and type(data["contact_id_list"]) == list:
            ab_id = data["contact_id_list"]
        else:
            raise NoRequiredError("contact_id_list")

        core_request = AddContactToGroupRequest(gr_id, ab_id)
        core_handler = request.app[AddContactToGroupHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"group": export_group_entity(core_response.group)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_group_contact(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        gr_id = int_field("group_id", request.match_info, 0, -1, True, 0)
        ab_id = int_field("contact_id", request.match_info, 0, -1, True, 0)

        core_request = GetContactOfGroupRequest(gr_id, ab_id)
        core_handler = request.app[GetContactGroupHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"contact": export_contact_entity(core_response.contact)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def delete_group_contact(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        gr_id = int_field("group_id", request.match_info, 0, -1, True, 0)
        ab_id = int_field("contact_id", request.match_info, 0, -1, True, 0)

        core_request = DeleteContactFromGroupRequest(gr_id, ab_id)
        core_handler = request.app[DeleteContactFromGroupHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_del_contact_from_group_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_license(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = GetLicenseRequest()
        core_handler = request.app[GetLicenseHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"license": export_license_entity(core_response.license)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


# TODO: DELETE
async def registration(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        reader = MultipartReader.from_response(request)

        type = None
        file = None
        serial = None
        server_name = None
        request_as_file = None

        while True:

            part = await reader.next()

            if part is None:
                break

            if part.headers[hdrs.CONTENT_DISPOSITION].find('name="type"') != -1:
                metadata = await part.read(decode=False)

                data_new = {"field": metadata.decode("utf-8")}
                enum = ["online", "offline"]
                type = enum_field("field", data_new, enum, True, None)
                continue

            if (type == "offline") and (part.filename is not None):
                metadata = await part.read(decode=False)
                file = metadata.decode("utf-8")
                continue

            if (type == "online") and part.headers[hdrs.CONTENT_DISPOSITION].find('name="serial"') != -1:
                metadata = await part.read(decode=False)
                serial = metadata.decode("utf-8")
                if (len(serial) < 1) or (len(serial) > 64):
                    raise BadStringLengthError("serial", serial, 1, 64)
                continue

            if (type == "online") and part.headers[hdrs.CONTENT_DISPOSITION].find('name="server_name"') != -1:
                metadata = await part.read(decode=False)
                server_name = metadata.decode("utf-8")
                if (len(server_name) < 1) or (len(server_name) > 64):
                    raise BadStringLengthError("server_name", server_name, 1, 64)

            if (type == "online") and part.headers[hdrs.CONTENT_DISPOSITION].find('name="request_as_file"') != -1:
                metadata = await part.read(decode=False)
                data_new = {"field": metadata.decode("utf-8")}
                request_as_file = bool_field("field", data_new, False, None)

        if type == "online":
            logging.debug(
                "serial = " + serial + " server = " + server_name + " request_as_file = " + str(request_as_file))
            core_request = OnlineRegistrationRequest(serial, server_name, request_as_file)
            core_handler = request.app[OnlineRegistrationHandler.__name__]
        elif type == "offline":
            core_request = OfflineRegistrationRequest(file)
            core_handler = request.app[OfflineRegistrationHandler.__name__]
        else:
            raise BadTypeRegistration(type)

        core_response = await core_handler.handle(core_request)

        result = export_registration_result_entity(core_response.result)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def registration_online(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        serial = string_field("serial", data, 0, 64, True, "")
        server_name = string_field("server_name", data, 0, 64, True, "")

        logging.debug("registration_online(): serial = " + serial + " server = " + server_name)

        core_request = OnlineRegistrationRequest(serial, server_name, True)
        core_handler = request.app[OnlineRegistrationHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_registration_result_entity(core_response.result)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def registration_offline(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        serial = string_field("serial", data, 0, 64, True, "")
        server_name = string_field("server_name", data, 0, 64, True, "")

        logging.debug("registration_offine(): serial = " + serial + " server = " + server_name)

        # generate CSR-file
        core_request = OfflineRegistrationRequest(serial, server_name)
        core_handler = request.app[OfflineRegistrationHandler.__name__]
        core_response = await core_handler.handle(core_request)

        # return CSR-file dump (base64 encoded)
        return web.Response(body=core_response.csr_data, status=200)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def upload_license(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        reader = MultipartReader.from_response(request)

        filedata = None
        filedata_str = ""
        filename = ""

        while True:
            part = await reader.next()
            if part is None:
                break

            if part.filename is not None:
                filedata = await part.read(decode=False)
                filedata_str = filedata.decode("UTF-8")
                filename = part.filename
                continue

        if filename == "":
            raise MCUFileNotFoundError("<upload-file>", "file_name")

        if filedata == None or filedata_str == "":
            raise FileIsEmptyError(filename)

        core_request = UploadLicenseRequest(filedata_str)

        core_handler = request.app[UploadLicenseHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_registration_result_entity(core_response.result)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_call_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[InstanceCallsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"callsettings": export_calls_entity(core_response.calls)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_call_settings(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        fecc = bool_field("remote_camera_control", data, False, None)
        prots = ["sip", "h323"]
        protpref = enum_field("preferred_protocol", data, prots, False, None)
        usesip = bool_field("enable_sip", data, False, None)
        useh323 = bool_field("enable_h323", data, False, None)
        disp_name = string_field("mcu_display_name", data, 0, 32, False, None)
        call_seq_list = ["addressbook", "gatekeeper"]
        call_sequence = enum_field("call_sequence", data, call_seq_list, False, None)
        external_ip = string_field("external_ip", data, 0, 32, False, None)

        if external_ip != None and external_ip != "" and (not tools.is_valid_ip(external_ip)):
            raise BadFormatFieldError("external_ip", external_ip, "num.num.num.num")

        audio_call = enum_field("audio_call", data, AUDIO_CALLS_LIST, False, None)

        enable_noise_suppressor = bool_field("enable_noise_suppressor", data, False, None)
        enable_auto_gain_ctrl = bool_field("enable_auto_gain_ctrl", data, False, None)

        core_request = InstanceCallsRequest(CallsEntity(fecc, usesip, useh323, protpref, disp_name, call_sequence,
                                                        external_ip, audio_call, enable_noise_suppressor,
                                                        enable_auto_gain_ctrl))
        core_handler = request.app[InstanceSetCallsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"callsettings": export_calls_entity(core_response.calls)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_sip_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[InstanceSipHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"sipsettings": export_sip_entity(core_response.sip)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_sip_settings(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        register = bool_field("register", data, False, None)
        server = string_field("server", data, 0, 64, False, None)
        username = string_field("username", data, 0, 64, False, None)
        password = string_field("password", data, 0, 64, False, None)
        prots = ["auto", "udp", "tcp", "tls"]
        transport_protocol = enum_field("transport_protocol", data, prots, False, None)
        bfcp_transport = enum_field("bfcp_transport", data, BFCP_TRANSPORT_LIST, False, None)
        use_ice = bool_field("use_ice", data, False, None)

        turn_username = string_field("turn_username", data, 0, 80, False, None)
        turn_password = string_field("turn_password", data, 0, 80, False, None)
        turn_hostname = string_field("turn_hostname", data, 0, 80, False, None)

        b_all_turn_fields = (turn_username is not None) and (turn_password is not None) and (turn_hostname is not None)
        b_no_turn_fields = (turn_username is None) and (turn_password is None) and (turn_hostname is None)
        if (not b_all_turn_fields) and (not b_no_turn_fields): # set all TURN server fields or nothing
            if turn_username is None:
                raise NoRequiredError("turn_username")
            if turn_password is None:
                raise NoRequiredError("turn_password")
            if turn_hostname is None:
                raise NoRequiredError("turn_hostname")

        turn_addr = calls_settings.join_turn_address(turn_username, turn_password, turn_hostname)

        sip_port = int_field("sip_port", data, 1, 65535, False, None)
        bfcp_port_min = int_field("bfcp_port_min", data, 1, 65535, False, None)
        bfcp_port_max = int_field("bfcp_port_max", data, 1, 65535, False, None)

        if (bfcp_port_min is not None) and (bfcp_port_max is not None) and bfcp_port_min >= bfcp_port_max:
            raise MinGreaterMaxError(bfcp_port_min, bfcp_port_max, ["bfcp_port_min", "bfcp_port_max"])

        core_request = InstanceSipRequest( SipEntity(register, server, username, password,
        transport_protocol, False, bfcp_transport, use_ice, turn_addr,
        sip_port, bfcp_port_min, bfcp_port_max))

        core_handler = request.app[InstanceSetSipHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"sipsettings": export_sip_entity(core_response.sip)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_h323_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[GetH323SettingsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"h323settings": export_h323_entity(core_response.h323)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_h323_settings(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        use_gatekeeper = bool_field("use_gatekeeper", data, False, None)
        id_number = string_field("id_number", data, 0, 64, False, None)
        e164_ext = string_field("e164_ext", data, 0, 64, False, None)
        server = string_field("server", data, 0, 80, False, None)
        zone = string_field("zone", data, 0, 64, False, None)
        h46017 = bool_field("h46017", data, False, None)
        h46018 = bool_field("h46018", data, False, None)
        h46019 = bool_field("h46019", data, False, None)
        h323_port = int_field("h323_port", data, 1, 65535, False, None)
        h245_port_min = int_field("h245_port_min", data, 1, 65535, False, None)
        h245_port_max = int_field("h245_port_max", data, 1, 65535, False, None)

        if (h245_port_min is not None) and (h245_port_max is not None) and h245_port_min >= h245_port_max:
            raise MinGreaterMaxError(h245_port_min, h245_port_max, ["h245_port_min", "h245_port_max"])

        settings = H323SettingsEntity(use_gatekeeper, id_number, e164_ext, server, zone, False,
                                      h46017, h46018, h46019, h323_port, h245_port_min, h245_port_max)
        core_request = SetH323SettingsRequest(settings)
        core_handler = request.app[SetH323SettingsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"h323settings": export_h323_entity(core_response.h323)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_video_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[InstanceVideoHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"videosettings": export_video_entity(core_response.video)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_video_settings(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        video_storage_size = int_field("video_storage_size", data, 0, -1, False, None)
        prots = ["default", "balanced", "resolution", "rate", "compliant"]
        stream1_priority = enum_field("stream1_priority", data, prots, False, None)
        stream2_priority = enum_field("stream2_priority", data, prots, False, None)
        prots = ["auto", "20", "30", "40", "50", "60", "70"]
        content_wide_band = enum_field("content_wide_band", data, prots, False, None)

        adapt_bitrate = bool_field("adapt_bitrate", data, False, None)
        stream_control = bool_field("stream_control", data, False, None)
        packet_recovery = bool_field("packet_recovery", data, False, None)

        bk_names = get_background_names()
        background = enum_field("background", data, bk_names, False, None)
        rolltimeout = int_field("rolltimeout", data, 500, 20000, False, None)

        core_request = InstanceVideoRequest(
            VideoSettingsEntity(video_storage_size, stream1_priority, stream2_priority, content_wide_band,
                                adapt_bitrate, stream_control, packet_recovery, background, rolltimeout))
        core_handler = request.app[InstanceSetVideoHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"videosettings": export_video_entity(core_response.video)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_ldap_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[InstanceLDAPHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"ldapsettings": export_ldap_entity(core_response.ldapsettings),
                  "is_connected": bool(core_response.is_connected)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_ldap_settings(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        server = string_field("server", data, 0, 64, False, None)
        port = int_field("port", data, 0, -1, False, None)
        distinct_name = string_field("distinct_name", data, 0, 64, False, None)
        filter = string_field("filter", data, 0, 64, False, None)

        core_request = InstanceLDAPRequest(LDAPSettingsEntity(server, port, distinct_name, filter))
        core_handler = request.app[InstanceSetLDAPHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"ldapsettings": export_ldap_entity(core_response.ldapsettings),
                  "is_connected": bool(core_response.is_connected)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_email_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[InstanceEmailHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"emailsettings": export_email_entity(core_response.email)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_email_settings(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        smtp_server = string_field("smtp_server", data, 0, 64, False, None)
        username = string_field("username", data, 0, 64, False, None)
        password = string_field("password", data, 0, 64, False, None)

        core_request = InstanceEmailRequest(EmailSettingsEntity(smtp_server, username, password))
        core_handler = request.app[InstanceSetEmailHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"emailsettings": export_email_entity(core_response.email)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_db_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[InstanceDBHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"backup": export_db_entity(core_response.db)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_db_settings(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        values = ["local", "remote"]
        backup_place = enum_field("backup_place", data, values, True, None)
        shared_dir_url = string_field("shared_dir_url", data, 0, 64, backup_place == "remote", None)
        username = string_field("username", data, 0, 64, backup_place == "remote", None)
        password = string_field("password", data, 0, 64, backup_place == "remote", None)

        values = ["manual", "timer"]
        backup_type = enum_field("backup_type", data, values, True, None)
        values = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
        weekday = enum_field("weekday", data, values, backup_type == "timer", None)
        hour = int_field("hour", data, 0, 23, backup_type == "timer", None)
        minute = int_field("minute", data, 0, 59, backup_type == "timer", None)

        core_request = InstanceDBRequest(DBSettingsEntity(backup_place, shared_dir_url, username, password, backup_type,
                                                          weekday, hour, minute))
        core_handler = request.app[InstanceSetDBHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"backup": export_db_entity(core_response.db)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_cpu_stat(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[InstanceCPUHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"cpu": export_cpu_entity(core_response)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_mem_stat(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[InstanceMemoryHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"memory": export_memory_entity(core_response)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_net_stat(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[InstanceNetHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"network": export_network_entity(core_response)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_free_space(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[InstanceFreeSpaceHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_free_space_entity(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_all_resolutions(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = GetAllResolutionsListRequest()

        core_handler = request.app[GetAllResolutionsListHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_all_resolutions_list(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def calculate_layout(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        cells_count = int_field("cells_count", data, 0, -1, True, 0)
        width = int_field("width", data, 0, -1, True, 0)
        height = int_field("height", data, 0, -1, True, 0)
        pip = bool_field("pip", data, False, False)
        schema_type = int_field("type", data, 0, -1, True, 0)

        core_request = CalculateLayoutRequest(cells_count, width, height, pip, schema_type)

        core_handler = request.app[LayoutCalculatorHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_layout_wnd_coord_list(core_response)

        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def switch_microphone_of_participant(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        dir_values = ["RX", "TX"]
        direction = enum_field("direction", data, dir_values, False, "RX")
        turn_on = bool_field("turn_on", data, True, True)
        use_conn_id = bool_field("use_connection_id", data, False, False)

        core_request = SwitchMicrophoneRequest(conf_id, participant_id, turn_on, direction, use_conn_id)
        core_handler = request.app[SwitchMicrophoneHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participant_control_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def switch_audio_of_group_participants(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        dir_values = ["RX", "TX"]
        direction = enum_field("direction", data, dir_values, False, "RX")
        turn_on = bool_field("turn_on", data, True, True)

        if "connection_id_list" in data:
            conn_id_list = data["connection_id_list"]
        else:
            conn_id_list = []

        core_request = SwitchAudioToGroupAbonentsRequest(conf_id, conn_id_list, turn_on, direction)
        core_handler = request.app[SwitchAudioToGroupAbonentsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_group_operation_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_speaker_of_participant(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        dir_values = ["RX", "TX"]
        direction = enum_field("direction", data, dir_values, False, "RX")
        volume = 0
        if "volume" in data and type(data["volume"]) == int:
            volume = data["volume"]
        MIN_VOL = -400
        MAX_VOL = 200
        if volume > MAX_VOL or volume < MIN_VOL:
            raise ValueNotInLimitsError("volume", str(volume), "[%d .. %d]" % (MIN_VOL, MAX_VOL))
        use_conn_id = bool_field("use_connection_id", data, False, False)

        core_request = TuneSpeakerVolumeRequest(conf_id, participant_id, volume, direction, use_conn_id)
        core_handler = request.app[TuneSpeakerVolumeHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participant_control_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def switch_video_of_participant(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        dir_values = ["RX", "TX"]
        direction = enum_field("direction", data, dir_values, False, "RX")
        turn_on = bool_field("turn_on", data, True, True)
        use_conn_id = bool_field("use_connection_id", data, False, False)

        core_request = SwitchVideoRequest(conf_id, participant_id, turn_on, direction, use_conn_id)
        core_handler = request.app[SwitchVideoHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participant_control_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_preview_mode(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        turn_on = bool_field("turn_on", data, True, True)
        use_conn_id = bool_field("use_connection_id", data, False, False)
        if turn_on:
            corr_values = ["first", "second", "camera"]
            stream = enum_field("stream", data, corr_values, True, "first")
            core_request = TurnOnPreviewModeRequest(conf_id, participant_id, stream, use_conn_id)
        else:
            core_request = TurnOffPreviewModeRequest(conf_id, participant_id, use_conn_id)

        core_handler = request.app[SwitchPreviewModeHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participant_control_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_preview_image(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        core_request = GetPreviewImageRequest(conf_id, participant_id)

        core_handler = request.app[GetPreviewImageHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.Response(body=core_response.image, status=200)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_preview_state(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = GetPreviewStateRequest()
        core_handler = request.app[GetPreviewStateHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_preview_state_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def move_participants_camera(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        correct_commands = get_fecc_commands_list()
        camera_cmd = enum_field("command", data, correct_commands, True, "")
        use_conn_id = bool_field("use_connection_id", data, False, False)

        core_request = CameraControlRequest(conf_id, participant_id, camera_cmd, use_conn_id)

        core_handler = request.app[CameraControlHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participant_control_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def move_participant_to_other_conf(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id_from = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        conf_id_to = int_field("conference_id", data, 0, -1, True, 0)
        use_conn_id = bool_field("use_connection_id", data, False, False)

        if use_conn_id:
            conn_id = participant_id
            participant_uri = onlinetools.get_realaddr_by_conn_id(conn_id, [])
        else:
            participant_uri = addrbook.get_realaddr_by_ab_id(participant_id)
            conn_id = onlinetools.get_conn_id_by_realaddr(participant_uri, [])
        if participant_uri == "":
            raise SessionAbonentNotFoundError(str(participant_id), "participant_id")

        core_request = MoveParticipantToOtherConfRequest(conf_id_from, conf_id_to, participant_uri, conn_id)
        core_handler = request.app[MoveParticipantToOtherConfHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_move_to_conf_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def send_message_to_conf(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        message = string_field("message", data, 0, 256, True, "")

        core_request = SendMessageToConfRequest(conf_id, message)
        core_handler = request.app[SendMessageToConfHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_control_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def rename_participant(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conference_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        participant_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        new_dispname = string_field("dispname", data, 0, 64, True, "")
        use_conn_id = bool_field("use_connection_id", data, False, False)

        core_request = RenameParticipantRequest(conference_id, participant_id, new_dispname, use_conn_id)
        core_handler = request.app[RenameParticipantHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participant_control_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_waiting_participants_list(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = GetWaitingAbonentsRequest()

        core_handler = request.app[GetWaitingAbonentsListHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_waiting_abonents_list(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def move_waiting_abonent_to_conf(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        conference_id_to = int_field("conference_id", data, 0, -1, True, 0)

        if "connection_id" in data:
            conn_id = int_field("connection_id", data, 0, -1, True, -1)
            participant_uri = onlinetools.get_realaddr_by_conn_id(conn_id, [])
        else:
            participant_uri = string_field("participant_id", data, 0, 64, True, "")
            conn_id = onlinetools.get_conn_id_by_realaddr(participant_uri, [])

        core_request = MoveParticipantToOtherConfRequest(0, conference_id_to, participant_uri, conn_id)
        core_handler = request.app[MoveParticipantToOtherConfHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_move_to_conf_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def hangup_waiting_abonent(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        participant_uri = string_field("participant_id", request.match_info, 0, 64, True, "")

        core_request = HangupSessionParticipantRequest(SessionParticipantAddressEntity(0, participant_uri))
        core_handler = request.app[HangupSessionParticipantHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_participant_addr_entity(core_response.session_addr))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def switch_video_record(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        turn_on = bool_field("turn_on", data, True, True)

        corr_values = ["file", "rtmp"]
        url_type = enum_field("url_type", data, corr_values, False, "file")

        core_request = SwitchVideoRecordRequest(conf_id, turn_on, url_type)
        core_handler = request.app[SwitchVideoRecordHandler.__name__]

        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_control_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_video_recording_status(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = GetRecordingStatusRequest(conf_id)
        core_handler = request.app[GetVideoRecordingStatusHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_recording_status_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_files_list(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        corr_values = files.get_file_types_list()
        file_type = enum_field("file_type", request.match_info, corr_values, True, "videorecord")

        core_request = GetFilesListRequest(file_type)

        core_handler = request.app[GetFilesListHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_files_list(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_file(request: web.Request) -> web.Response:
    try:
        corr_values = files.get_file_types_list()
        file_type = enum_field("file_type", request.match_info, corr_values, True, "videorecord")
        file_name = string_field("file_name", request.match_info, 0, 64, True, "")

        token = string_field("token", request.query, 0, 8192, False, None)
        if token is not None:
            await authorize_request(request, True) # get token from URL of request
        else:
            await authorize_request(request) # get token from headers of request

        core_request = GetFileRequest(file_type, file_name)

        # OLD:
        # core_handler = request.app[GetFileHandler.__name__]
        # core_response = await core_handler.handle(core_request)
        # return web.Response(body=core_response.data, status=200)

        # NEW:
        return web.Response(body=files.get_file_by_chunks(request=core_request), status=200)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def upload_file_OLD(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        corr_values = files.get_file_types_list()
        file_type = enum_field("file_type", request.match_info, corr_values, True, "videorecord")

        reader = MultipartReader.from_response(request)

        filedata = None
        filename = ""

        while True:
            part = await reader.next()
            if part is None:
                break

            if part.filename is not None:
                filedata = await part.read(decode=False)
                filename = part.filename
                continue

        if filename == "":
            raise MCUFileNotFoundError("<upload-file>", "file_name")

        if filedata == None:
            raise FileIsEmptyError(filename)

        core_request = UploadFileRequest(file_type, filename, filedata)

        core_handler = request.app[UploadFileHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_handling_file_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def upload_file(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        corr_values = files.get_file_types_list()
        file_type = enum_field("file_type", request.match_info, corr_values, True, "videorecord")

        reader = MultipartReader.from_response(request)

        body_part_reader = None
        filename = ""

        while True:
            part = await reader.next()
            if part is None:
                break

            if part.filename is not None:
                body_part_reader = part
                filename = part.filename
                break

        if filename == "":
            raise MCUFileNotFoundError("<upload-file>", "file_name")

        if body_part_reader == None:
            raise FileIsEmptyError(filename)

        core_request = UploadFileByChunksRequest(file_type, filename, body_part_reader)

        core_handler = request.app[UploadFileByChunksHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_handling_file_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def delete_file(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        corr_values = files.get_file_types_list()
        file_type = enum_field("file_type", request.match_info, corr_values, True, "videorecord")
        file_name = string_field("file_name", request.match_info, 0, 64, True, "")

        core_request = DeleteFileRequest(file_type, file_name)

        core_handler = request.app[DeleteFileHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_handling_file_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def switch_presentation(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        if "mode" not in data:
            raise NoRequiredError("mode")

        turn_on = bool_field("turn_on", data["mode"], True, True)
        if turn_on:
            filename = string_field("filename", data["mode"], 0, 64, True, "")
        else:
            filename = ""
        core_request = SwitchPresentationRequest(conf_id, turn_on, filename)

        core_handler = request.app[SwitchPresentationHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_switch_presentation_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_presentation_status(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = GetPresentationStatusRequest(conf_id)
        core_handler = request.app[GetPresentationStatusHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_presentation_status_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def stop_incoming_presentation(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = StopIncomingPresentationRequest(conf_id)
        core_handler = request.app[StopIncomingPresentationHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_stop_incoming_presentation_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def export_abook_to_file(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        corr_values = ["filecsv", "filexml"]
        abook_type = enum_field("abook_type", request.match_info, corr_values, True, "filecsv")

        core_request = ExportAbookToFileRequest(abook_type)

        core_handler = request.app[ExportAbookToFileHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_handling_file_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def import_abook_from_file(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        corr_values = ["filecsv", "filexml"]
        abook_type = enum_field("abook_type", request.match_info, corr_values, True, "filecsv")

        data = await request.json()
        file_name = string_field("file_name", data, 0, 64, True, "")

        core_request = ImportAbookFromFileRequest(abook_type, file_name)

        core_handler = request.app[ImportAbookFromFileHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_import_abook_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def export_db_to_file(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = SaveDatabaseToFile()

        core_handler = request.app[SaveDatabaseHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_handling_file_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def import_db_from_file(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()
        file_name = string_field("file_name", data, 0, 64, True, "")

        core_request = RestoreDatabaseFromFile(file_name)

        core_handler = request.app[RestoreDatabaseHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_handling_file_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_all_sessions_partic_list(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = AllConfSessionParticipantsRequest()
        core_handler = request.app[GetAllConfSessionParticipantsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_all_conf_participants_list(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_server_time(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = GetServerTimeRequest()
        core_handler = request.app[GetServerTimeHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_server_time(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_mcu_params(request: web.Request) -> web.Response:
    try:
        core_request = GetMCUParamsRequest()
        core_handler = request.app[GetMCUParamsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_mcu_params_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def start_util(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        util_type = enum_field("util_type", request.match_info, LIST_UTILS, True, "ping")

        data = await request.json()
        argument = string_field("argument", data, 0, 64, True, "")

        core_request = StartUtilRequest(util_type, argument)
        core_handler = request.app[StartUtilHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_start_util_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def stop_util(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        util_type = enum_field("util_type", request.match_info, LIST_UTILS, True, "ping")

        core_request = StopUtilRequest(util_type)
        core_handler = request.app[StopUtilHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_start_util_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_util_output(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        util_type = enum_field("util_type", request.match_info, LIST_UTILS, True, "ping")

        core_request = GetUtilOutputRequest(util_type)
        core_handler = request.app[GetUtilOutputHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_util_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_crown(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)
        contact_id = int_field("participant_id", request.match_info, 0, -1, True, 0)

        if request.can_read_body:
            data = await request.json()
            use_conn_id = bool_field("use_connection_id", data, False, False)
        else:
            use_conn_id = False

        core_request = SetCrownRequest(conf_id, contact_id, use_conn_id)
        core_handler = request.app[SetCrownHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_set_crown_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def delete_crown(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        core_request = DeleteCrownRequest(conf_id)
        core_handler = request.app[DeleteCrownHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_delete_crown_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_rtp_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = EmptyRequest()
        core_handler = request.app[GetRTPSettingsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"rtpsettings": export_rtp_entity(core_response.rtp)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_rtp_settings(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        rtpportmin = int_field("rtpportmin", data, 1024, 65534, False, None)
        if rtpportmin % 2 != 0:
            raise NotParityParamError("rtpportmin", rtpportmin)

        rtpportmax = int_field("rtpportmax", data, 1024, 65534, False, None)
        if rtpportmax % 2 != 0:
            raise NotParityParamError("rtpportmax", rtpportmax)

        if rtpportmin >= rtpportmax:
            raise MinGreaterMaxError(rtpportmin, rtpportmax, ["rtpportmin", "rtpportmax"])

        mtu = int_field("mtu", data, 1280, 1500, False, None)

        use_ip_presedence = bool_field("use_ip_presedence", data, False, False)
        if use_ip_presedence:
            ip_presedence_audio = int_field("ip_presedence_audio", data, 0, 7, False, None)
            dscp_audio = profile_settings.ip_presedence_to_dscp(ip_presedence_audio)

            ip_presedence_video = int_field("ip_presedence_video", data, 0, 7, False, None)
            dscp_video = profile_settings.ip_presedence_to_dscp(ip_presedence_video)

            ip_presedence_other = int_field("ip_presedence_other", data, 0, 7, False, None)
            dscp_other = profile_settings.ip_presedence_to_dscp(ip_presedence_other)
        else:
            dscp_audio = int_field("dscp_audio", data, 0, 63, False, None)
            dscp_video = int_field("dscp_video", data, 0, 63, False, None)
            dscp_other = int_field("dscp_other", data, 0, 63, False, None)

        core_request = SetRTPSettingsRequest(RTPEntity(rtpportmin, rtpportmax, mtu, dscp_audio, dscp_video, dscp_other))
        core_handler = request.app[SetRTPSettingsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"rtpsettings": export_rtp_entity(core_response.rtp)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_all_network_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = GetAllNetworkSettingsRequest()
        core_handler = request.app[GetAllNetworkSettingsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"allnetsettings": export_network_settings_list(core_response.settings_list)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_network_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        iface = string_field("iface", request.match_info, 0, 64, True, "")

        core_request = GetNetworkSettingsRequest(iface)
        core_handler = request.app[GetNetworkSettingsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"netsettings": export_network_settings_entity(core_response.settings)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def set_network_settings(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        iface = string_field("iface", request.match_info, 0, 64, True, "")

        req_entity = import_network_settings_entity(iface, data)

        core_request = ChangeNetworkSettingsRequest(req_entity)
        core_handler = request.app[ChangeNetworkSettingsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = {"netsettings": export_network_settings_entity(core_response.settings)}
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def can_change_network_settings(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = CanChangeNetworkSettingsRequest()
        core_handler = request.app[CanChangeNetworkSettingsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_can_edit_network_settings_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_video_engine_log(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        time_stamp = int_field("timestamp", request.query, 0, -1, False, None)
        id = int_field("id", request.query, 0, -1, False, None)

        core_request = GetVEngineLogRequest(time_stamp, id)
        core_handler = request.app[GetVEngineLogHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_vengine_log_entity(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def execute_video_engine_command(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        command = string_field("command", data, 0, 256, True, "")

        core_request = ExecuteVEngineCommandRequest(command)
        core_handler = request.app[ExecuteVEngineCommandHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_vengine_command_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_mcu_references(request: web.Request) -> web.Response:
    try:
        language = string_field("language", request.query, 0, 64, False, "en")

        core_request = GetMCUReferencesRequest(language)
        core_handler = request.app[GetMCUReferencesHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_mcu_references_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def echo(request: web.Request) -> web.Response:
    return web.json_response(status=200, data={'echo': True})


async def clean_address_book(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = CleanAddressBookRequest()
        core_handler = request.app[CleanAddressBookHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_clean_abook_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def execute_power_command(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()

        command = enum_field("command", data, POWER_COMMANDS_LIST, True, "restart_api")

        core_request = ExecuteRestartCommandRequest(command)
        core_handler = request.app[PowerManagementHandler.__name__]
        core_response = await core_handler.handle(core_request)

        result = export_restart_command_response(core_response)
        return web.json_response(result)
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def install_deb_package(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        data = await request.json()
        filename = string_field("filename", data, 0, 128, True, "")

        core_request = InstallDebRequest(filename)
        core_handler = request.app[InstallDebHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_install_deb_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def get_install_deb_state(request: web.Request) -> web.Response:
    try:
        await authorize_request(request)

        core_request = GetInstallDebStateRequest()
        core_handler = request.app[GetInstallDebStateHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_install_deb_state_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def switch_render_mode_of_group_participants(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()

        render_mode = enum_field("render_mode", data, RENDER_PARTICIPANT_NAMES, True, "AUTO")

        if "connection_id_list" in data:
            conn_id_list = data["connection_id_list"]
        else:
            conn_id_list = []

        core_request = SwitchRenderModeToGroupAbonentsRequest(conf_id, conn_id_list, render_mode)
        core_handler = request.app[SwitchRenderModeToGroupAbonentsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_group_operation_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def hangup_participants_group(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()

        if "connection_id_list" in data:
            conn_id_list = data["connection_id_list"]
        else:
            conn_id_list = []

        core_request = HangupGroupAbonentsRequest(conf_id, conn_id_list)
        core_handler = request.app[HangupGroupAbonentsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_group_operation_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def switch_noise_suppressor(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()

        noise_suppressor = enum_field("noise_suppressor", data, NOISE_SUPPRESSOR_NAMES, True, "DEFAULT")

        if "connection_id_list" in data:
            conn_id_list = data["connection_id_list"]
        else:
            conn_id_list = []

        core_request = SwitchNoiseSuppresorToGroupAbonentsRequest(conf_id, conn_id_list, noise_suppressor)
        core_handler = request.app[SwitchNoiseSuppresorToGroupAbonentsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_group_operation_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def switch_auto_gain_ctrl(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()

        auto_gain_ctrl = enum_field("auto_gain_ctrl", data, AUTO_GAIN_CTRL_NAMES, True, "DEFAULT")

        if "connection_id_list" in data:
            conn_id_list = data["connection_id_list"]
        else:
            conn_id_list = []

        core_request = SwitchAGCToGroupAbonentsRequest(conf_id, conn_id_list, auto_gain_ctrl)
        core_handler = request.app[SwitchAGCToGroupAbonentsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_group_operation_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


async def switch_video_of_group_participants(request: web.Request) -> web.Response:
    if request.can_read_body is False:
        return generate_error_no_body_json()
    try:
        await authorize_request(request)

        conf_id = int_field("conference_id", request.match_info, 0, -1, True, 0)

        data = await request.json()
        dir_values = ["RX", "TX"]
        direction = enum_field("direction", data, dir_values, False, "RX")
        turn_on = bool_field("turn_on", data, True, True)

        if "connection_id_list" in data:
            conn_id_list = data["connection_id_list"]
        else:
            conn_id_list = []

        core_request = SwitchVideoToGroupAbonentsRequest(conf_id, conn_id_list, turn_on, direction)
        core_handler = request.app[SwitchVideoToGroupAbonentsHandler.__name__]
        core_response = await core_handler.handle(core_request)

        return web.json_response(export_session_group_operation_response(core_response))
    except Exception as ex:
        logging.error(str(sys.exc_info()))
        return response_error(ex)


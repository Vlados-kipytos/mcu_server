from aiohttp import web

from app.core.handlers import *
from mcu.postgresql.client import Manager as PostgreSqlClientManager
from mcu.postgresql.schemas.oauth import get_access_token
from .middlewares import setup_middlewares
from .routes import setup_routes


def setup_api(app: web.Application) -> None:
    subapp = web.Application()

    subapp[PostgreSqlClientManager.get_class_name()] = app[PostgreSqlClientManager.get_class_name()]

    get_access_token.Select.setup(subapp)  # Установка хендлера в приложении

    subapp[GetContactListHandler.__name__] = GetContactListHandler()
    subapp[GetContactHandler.__name__] = GetContactHandler()
    subapp[CreateContactHandler.__name__] = CreateContactHandler()
    subapp[ChangeContactHandler.__name__] = ChangeContactHandler()
    subapp[DeleteContactHandler.__name__] = DeleteContactHandler()
    subapp[CleanAddressBookHandler.__name__] = CleanAddressBookHandler()

    subapp[ExportAbookToFileHandler.__name__] = ExportAbookToFileHandler()
    subapp[ImportAbookFromFileHandler.__name__] = ImportAbookFromFileHandler()
    subapp[SaveDatabaseHandler.__name__] = SaveDatabaseHandler()
    subapp[RestoreDatabaseHandler.__name__] = RestoreDatabaseHandler()

    subapp[GetConferenceHandler.__name__] = GetConferenceHandler()
    subapp[GetConferencesListHandler.__name__] = GetConferencesListHandler()
    subapp[CreateConferenceHandler.__name__] = CreateConferenceHandler()
    subapp[DeleteConferenceHandler.__name__] = DeleteConferenceHandler()
    subapp[ChangeConferenceHandler.__name__] = ChangeConferenceHandler()
    subapp[GetAllResolutionsListHandler.__name__] = GetAllResolutionsListHandler()

    subapp[GetConferenceParticipantHandler.__name__] = GetConferenceParticipantHandler()
    subapp[GetConferenceParticipantsListHandler.__name__] = GetConferenceParticipantsListHandler()
    subapp[CreateConferenceParticipantHandler.__name__] = CreateConferenceParticipantHandler()
    subapp[DeleteConferenceParticipantHandler.__name__] = DeleteConferenceParticipantHandler()
    subapp[ChangeConferenceParticipantHandler.__name__] = ChangeConferenceParticipantHandler()

    subapp[StartConferenceSessionHandler.__name__] = StartConferenceSessionHandler()
    subapp[StopConferenceSessionHandler.__name__] = StopConferenceSessionHandler()

    subapp[GetConferenceSessionParticipantHandler.__name__] = GetConferenceSessionParticipantHandler()
    subapp[GetConferenceSessionParticipantsListHandler.__name__] = GetConferenceSessionParticipantsListHandler()
    subapp[InviteSessionParticipantHandler.__name__] = InviteSessionParticipantHandler()
    subapp[InviteConferenceParticipantHandler.__name__] = InviteConferenceParticipantHandler()
    subapp[HangupSessionParticipantHandler.__name__] = HangupSessionParticipantHandler()
    subapp[HangupConferenceParticipantHandler.__name__] = HangupConferenceParticipantHandler()
    subapp[GetAllConfSessionParticipantsHandler.__name__] = GetAllConfSessionParticipantsHandler()
    subapp[HangupGroupAbonentsHandler.__name__] = HangupGroupAbonentsHandler()

    subapp[GetAllLayoutsTypesHandler.__name__] = GetAllLayoutsTypesHandler()
    subapp[GetSheduledLayoutHandler.__name__] = GetSheduledLayoutHandler()
    subapp[GetSheduledLayoutsListHandler.__name__] = GetSheduledLayoutsListHandler()
    subapp[CreateLayoutHandler.__name__] = CreateLayoutHandler()
    subapp[ChangeLayoutHandler.__name__] = ChangeLayoutHandler()
    subapp[DeleteLayoutHandler.__name__] = DeleteLayoutHandler()

    subapp[CreateCustomLayoutTypeHandler.__name__] = CreateCustomLayoutTypeHandler()
    subapp[ChangeCustomLayoutTypeHandler.__name__] = ChangeCustomLayoutTypeHandler()
    subapp[DeleteCustomLayoutTypeHandler.__name__] = DeleteCustomLayoutTypeHandler()
    subapp[GetCustomLayoutTypeHandler.__name__] = GetCustomLayoutTypeHandler()
    subapp[GetAllCustomLayoutTypesHandler.__name__] = GetAllCustomLayoutTypesHandler()

    subapp[GetOnlineLayoutHandler.__name__] = GetOnlineLayoutHandler()
    subapp[GetOnlineLayoutsListHandler.__name__] = GetOnlineLayoutsListHandler()
    subapp[CreateOnlineLayoutHandler.__name__] = CreateOnlineLayoutHandler()
    subapp[DeleteOnlineLayoutHandler.__name__] = DeleteOnlineLayoutHandler()
    subapp[ChangeOnlineLayoutHandler.__name__] = ChangeOnlineLayoutHandler()
    subapp[CreateLayoutFromTemplateHandler.__name__] = CreateLayoutFromTemplateHandler()
    subapp[CreateTemplateFromOnlineLayoutHandler.__name__] = CreateTemplateFromOnlineLayoutHandler()

    subapp[GetGroupsListHandler.__name__] = GetGroupsListHandler()
    subapp[GetGroupHandler.__name__] = GetGroupHandler()
    subapp[CreateGroupHandler.__name__] = CreateGroupHandler()
    subapp[ChangeGroupHandler.__name__] = ChangeGroupHandler()
    subapp[DeleteGroupHandler.__name__] = DeleteGroupHandler()

    subapp[GetContactsOfGroupHandler.__name__] = GetContactsOfGroupHandler()
    subapp[AddContactToGroupHandler.__name__] = AddContactToGroupHandler()
    subapp[GetContactGroupHandler.__name__] = GetContactGroupHandler()
    subapp[DeleteContactFromGroupHandler.__name__] = DeleteContactFromGroupHandler()

    subapp[GetLicenseHandler.__name__] = GetLicenseHandler()
    subapp[OnlineRegistrationHandler.__name__] = OnlineRegistrationHandler()
    subapp[OfflineRegistrationHandler.__name__] = OfflineRegistrationHandler()
    subapp[UploadLicenseHandler.__name__] = UploadLicenseHandler()

    subapp[InstanceCallsHandler.__name__] = InstanceCallsHandler()
    subapp[InstanceSetCallsHandler.__name__] = InstanceSetCallsHandler()

    subapp[InstanceSipHandler.__name__] = InstanceSipHandler()
    subapp[InstanceSetSipHandler.__name__] = InstanceSetSipHandler()
    subapp[GetH323SettingsHandler.__name__] = GetH323SettingsHandler()
    subapp[SetH323SettingsHandler.__name__] = SetH323SettingsHandler()
    subapp[GetRTPSettingsHandler.__name__] = GetRTPSettingsHandler()
    subapp[SetRTPSettingsHandler.__name__] = SetRTPSettingsHandler()

    subapp[InstanceVideoHandler.__name__] = InstanceVideoHandler()
    subapp[InstanceSetVideoHandler.__name__] = InstanceSetVideoHandler()

    subapp[InstanceLDAPHandler.__name__] = InstanceLDAPHandler()
    subapp[InstanceSetLDAPHandler.__name__] = InstanceSetLDAPHandler()

    subapp[InstanceEmailHandler.__name__] = InstanceEmailHandler()
    subapp[InstanceSetEmailHandler.__name__] = InstanceSetEmailHandler()

    subapp[InstanceDBHandler.__name__] = InstanceDBHandler()
    subapp[InstanceSetDBHandler.__name__] = InstanceSetDBHandler()

    subapp[GetAllNetworkSettingsHandler.__name__] = GetAllNetworkSettingsHandler()
    subapp[GetNetworkSettingsHandler.__name__] = GetNetworkSettingsHandler()
    subapp[ChangeNetworkSettingsHandler.__name__] = ChangeNetworkSettingsHandler()
    subapp[CanChangeNetworkSettingsHandler.__name__] = CanChangeNetworkSettingsHandler()

    subapp[InstanceCPUHandler.__name__] = InstanceCPUHandler()
    subapp[InstanceMemoryHandler.__name__] = InstanceMemoryHandler()
    subapp[InstanceNetHandler.__name__] = InstanceNetHandler()
    subapp[InstanceFreeSpaceHandler.__name__] = InstanceFreeSpaceHandler()
    subapp[GetServerTimeHandler.__name__] = GetServerTimeHandler()
    subapp[GetMCUParamsHandler.__name__] = GetMCUParamsHandler()
    subapp[GetVEngineLogHandler.__name__] = GetVEngineLogHandler()
    subapp[ExecuteVEngineCommandHandler.__name__] = ExecuteVEngineCommandHandler()
    subapp[GetMCUReferencesHandler.__name__] = GetMCUReferencesHandler()

    subapp[LayoutCalculatorHandler.__name__] = LayoutCalculatorHandler()

    subapp[SwitchMicrophoneHandler.__name__] = SwitchMicrophoneHandler()
    subapp[SwitchAudioToGroupAbonentsHandler.__name__] = SwitchAudioToGroupAbonentsHandler()
    subapp[TuneSpeakerVolumeHandler.__name__] = TuneSpeakerVolumeHandler()
    subapp[SwitchVideoHandler.__name__] = SwitchVideoHandler()
    subapp[SwitchPreviewModeHandler.__name__] = SwitchPreviewModeHandler()
    subapp[GetPreviewImageHandler.__name__] = GetPreviewImageHandler()
    subapp[GetPreviewStateHandler.__name__] = GetPreviewStateHandler()
    subapp[CameraControlHandler.__name__] = CameraControlHandler()
    subapp[RenameParticipantHandler.__name__] = RenameParticipantHandler()
    subapp[SendMessageToConfHandler.__name__] = SendMessageToConfHandler()
    subapp[MoveParticipantToOtherConfHandler.__name__] = MoveParticipantToOtherConfHandler()
    subapp[GetWaitingAbonentsListHandler.__name__] = GetWaitingAbonentsListHandler()
    subapp[SwitchVideoRecordHandler.__name__] = SwitchVideoRecordHandler()
    subapp[GetVideoRecordingStatusHandler.__name__] = GetVideoRecordingStatusHandler()
    subapp[SwitchPresentationHandler.__name__] = SwitchPresentationHandler()
    subapp[GetPresentationStatusHandler.__name__] = GetPresentationStatusHandler()
    subapp[StopIncomingPresentationHandler.__name__] = StopIncomingPresentationHandler()
    subapp[SwitchRenderModeToGroupAbonentsHandler.__name__] = SwitchRenderModeToGroupAbonentsHandler()
    subapp[SwitchNoiseSuppresorToGroupAbonentsHandler.__name__] = SwitchNoiseSuppresorToGroupAbonentsHandler()
    subapp[SwitchAGCToGroupAbonentsHandler.__name__] = SwitchAGCToGroupAbonentsHandler()
    subapp[SwitchVideoToGroupAbonentsHandler.__name__] = SwitchVideoToGroupAbonentsHandler()

    subapp[GetFilesListHandler.__name__] = GetFilesListHandler()
    subapp[GetFileHandler.__name__] = GetFileHandler()
    subapp[UploadFileHandler.__name__] = UploadFileHandler()
    subapp[UploadFileByChunksHandler.__name__] = UploadFileByChunksHandler()
    subapp[DeleteFileHandler.__name__] = DeleteFileHandler()

    subapp[StartUtilHandler.__name__] = StartUtilHandler()
    subapp[StopUtilHandler.__name__] = StopUtilHandler()
    subapp[GetUtilOutputHandler.__name__] = GetUtilOutputHandler()

    subapp[PowerManagementHandler.__name__] = PowerManagementHandler()
    subapp[InstallDebHandler.__name__] = InstallDebHandler()
    subapp[GetInstallDebStateHandler.__name__] = GetInstallDebStateHandler()

    subapp[SetCrownHandler.__name__] = SetCrownHandler()
    subapp[DeleteCrownHandler.__name__] = DeleteCrownHandler()

    setup_routes(subapp)

    setup_middlewares(subapp)

    app.add_subapp('/api/v1/', subapp)

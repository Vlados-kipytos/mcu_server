# /usr/bin/python3
from aiohttp import web

from app.api.v1.errors import *
from app.core.errors import *
import app.core.tools as tools


def generate_error_json(id_err: str, message: str, code: int, details: list = None):
    if details != None and type(details) == list:
        det_list = details
    else:
        det_list = []
    data_json = {"error": {"id": "%s" % id_err, "message": "%s" % message, "status_code": code, "details": det_list}}
    return web.json_response(status=code, data=data_json)


def response_error_bad_page_size(ex: BadPageSizeError):
    return generate_error_json("bad.page.size", "bad page size %d" % (ex.per_page), 400,
                               ex.get_details_list())


def response_error_bad_start_index(ex: BadStartIndexError):
    return generate_error_json("bad.start.index", "bad start index %d" % (ex.start_index), 400,
                               ex.get_details_list())


def response_error_bad_end_index(ex: BadEndIndexError):
    return generate_error_json("bad.end.index", "bad end index %d" % (ex.end_index), 400,
                               ex.get_details_list())


def response_error_contact_not_found(ex: ContactNotFoundError):
    return generate_error_json("contact.not.found", "address book contact %d not found" % (ex.contact_id),
                               404, ex.get_details_list())


def response_error_duplicate_abonent_found(value: str, field: str, message: str, details: list):
    return generate_error_json("duplicate.%s" % field, message, 400, details)


def response_all_addresses_empty(ex: AllContactAddressesEmptyError):
    return generate_error_json("all.addr.empty", ex.get_error_message(), 400)


def response_error_gen_id(ex: GenerateContactIDError):
    return generate_error_json("cannot.gen.id", "Impossible to create abonent ID", 500, ex.get_details_list())


def response_mod_contact_error(mod: str, msg: str, details: list):
    return generate_error_json("%s.contact.error" % mod, "Impossible to %s contact: %s" % (mod, msg), 500, details)


def response_config_not_found(s_: str, ex):
    return generate_error_json("%s.config.error" % s_, "%sconfiguration not found" % s_, 500, ex.get_details_list())


def response_update_audio_config_error(ex: UpdateAudioConfigError):
    return generate_error_json("update.audio.config.error", ex.get_error_message(), 500, ex.get_details_list())


def response_update_video_config_error(ex: UpdateVideoConfigError):
    return generate_error_json("update.video.config.error", ex.get_error_message(), 500, ex.get_details_list())


def response_online_state_error(ex: GetOnlineStateError):
    return generate_error_json("online.state.error", "cannot get online state from engine", 500, ex.get_details_list())


def response_bad_layout_name(layout_name: str, ex: BadLayoutNameError):
    return generate_error_json("bad.layout.name", "bad layout name: %s" % (layout_name), 400, ex.get_details_list())


def response_no_layout_owner(ex: NoLayoutOwnerError):
    return generate_error_json("no.layout.owner", ex.get_error_message(), 400, ex.get_details_list())


def response_active_conference_not_found(ex: ActiveConferenceNotFoundError):
    return generate_error_json("active.conference.not.found", "cannot find session for conference id: %d" % (ex.confid),
                               404, ex.get_details_list())


def response_online_layout_not_found(ex: OnlineLayoutNotFoundError):
    return generate_error_json("online.layout.not.found",
                               "cannot find layout %s in active session" % (ex.layout_name),
                               404,
                               ex.get_details_list())


def response_too_many_roles(ex: TooManySlotRoles):
    return generate_error_json("too.many.roles.error", ex.get_error_message(), 400, ex.get_details_list())


def response_delete_global_layout(ex: DeleteGlobalLayoutError):
    return generate_error_json("delete.global.layout", ex.get_error_message(), 400, ex.get_details_list())


def response_error_conf_not_found(ex: ConferenceNotFoundError):
    return generate_error_json("conference.not.found", "conference %d not found" % (ex.confid), 404,
                               ex.get_details_list())


def response_error_bad_int_in_request(ex: BadIntInRequestError):
    return generate_error_json("bad.field",
                               "Incorrect field '%s': %s. Need non-negative number" % (ex.field, ex.bad_int),
                               400,
                               ex.get_details_list())


def response_limits_error(ex: ValueNotInLimitsError):
    return generate_error_json("limits.error",
                               "Incorrect field '%s': %s. Use value in %s." % (ex.field, ex.value, ex.limits),
                               400,
                               ex.get_details_list())


def response_string_limits_error(ex: BadStringLengthError):
    return generate_error_json("string.limits.error",
                               "Length of field '%s' ('%s', len = %d) is out of range (%s)." % (
                               ex.field, ex.string, ex.len, ex.lim), 400,
                               ex.get_details_list())


def response_no_required_error(ex: NoRequiredError):
    return generate_error_json("no.required", "Required field '%s' not found" % ex.field, 400,
                               ex.get_details_list())


def response_bad_field_error(ex: BadFormatFieldError):
    return generate_error_json("bad.field.%s" % ex.field,
                               "Field '%s' has bad value '%s', must be %s" % (ex.field, ex.value, ex.must_be),
                               400,
                               ex.get_details_list())


def response_util_execute_error(ex: UtiliteExecutionError):
    return generate_error_json("util.execute.error", ex.get_error_message(), 400, ex.get_details_list())


def response_error_default():
    return generate_error_json("internal.server.error", "internal server error", 500)


def generate_error_no_body_json():
    details_list = [ tools.get_detail_error_item("no.body.error", "request without body", "request", None) ]
    return generate_error_json("empty.request.error", "request without body", 400, details_list)


def response_layout_not_found_error(ex: LayoutNotFoundError):
    return generate_error_json("layout.not.found", ex.get_error_message(), 404,
                               ex.get_details_list())


def response_old_layout_format_error(ex: OldLayoutFormatError):
    return generate_error_json("old.layout.format", ex.get_error_message(), 500, ex.get_details_list())


def response_bad_schema_type_error(ex: BadSchemaTypeError):
    return generate_error_json("bad.schema.type", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_empty_layout_slots_list_error(ex: EmptyLayoutSlotsListError):
    return generate_error_json("empty.layout.slots.list", ex.get_error_message(), 400, ex.get_details_list())


def response_empty_layout_owner_error(ex: EmptyLayoutOwnerError):
    return generate_error_json("empty.layout.owner", ex.get_error_message(), 400, ex.get_details_list())


def response_layout_of_abonent_exist_error(ex: LayoutOfAbonentAlreadyExistError):
    return generate_error_json("layout.already.exist", ex.get_error_message(), 400, ex.get_details_list())


def response_gen_layout_id_error(ex: GenerateLayoutIDError):
    return generate_error_json("gen.layout.id.error", ex.get_error_message(), 500, ex.get_details_list())


def response_create_layout_error(ex: CreateLayoutError):
    return generate_error_json("create.layout.error", ex.get_error_message(), 500, ex.get_details_list())


def response_add_layout_to_conference_error(ex: AddLayoutToConferenceError):
    return generate_error_json("add.layout.to.conf.err", ex.get_error_message(), 500, ex.get_details_list())


def response_add_abonents_to_layout_error(ex: AddAbonentsToLayoutError):
    return generate_error_json("add.abonents.to.layout.err", ex.get_error_message(), 500, ex.get_details_list())


def response_bad_abonents_number_error(ex: BadAbonentsNumberError):
    return generate_error_json("bad.abonents.number", ex.get_error_message(), 400, ex.get_details_list())


def response_delete_layout_abonents_error(ex: DeleteLayoutAbonentsError):
    return generate_error_json("del.layout.abonents", ex.get_error_message(), 500, ex.get_details_list())


def response_update_layout_error(ex: UpdateLayoutError):
    return generate_error_json("update.layout.error", ex.get_error_message(), 500, ex.get_details_list())


def response_del_shed_layout_from_conf_error(ex: DeleteScheduledLayoutFromConferenceError):
    return generate_error_json("del.layout.from.conf.err", ex.get_error_message(), 500, ex.get_details_list())


def response_delete_shed_layout_error(ex: DeleteLayoutError):
    return generate_error_json("del.scheduled.layout.error", ex.get_error_message(), 500, ex.get_details_list())


def response_get_conf_participant_error(ex: GetConferenceParticipantError):
    return generate_error_json("get.conf.participant.error", ex.get_error_message(), 500, ex.get_details_list())


def response_create_conf_participant_error(ex: CreateConferenceParticipantError):
    return generate_error_json("create.conf.participant.error", ex.get_error_message(), 500, ex.get_details_list())


def response_conf_partic_not_found_error(ex: ConferenceParticipantNotFoundError):
    return generate_error_json("conf.participant.not.found.error", ex.get_error_message(), 404,
                               ex.get_details_list())


def response_delete_conf_participant_error(ex: DeleteConferenceParticipantError):
    return generate_error_json("delete.conf.participant.error", ex.get_error_message(), 500, ex.get_details_list())


def response_delete_layout_conf_participant_error(ex: DeleteLayoutFromConferenceError):
    return generate_error_json("delete.layout.conf.participant.error", ex.get_error_message(), 500,
                               ex.get_details_list())


def response_change_conf_participant_error(ex: ChangeConferenceParticipantError):
    return generate_error_json("change.conf.participant.error", ex.get_error_message(), 500,
                               ex.get_details_list())


def response_session_abonent_not_found_error(ex: SessionAbonentNotFoundError):
    return generate_error_json("session.abonent.not.found.error", ex.get_error_message(), 404,
                               ex.get_details_list())


def response_already_in_session_error(ex: AlreadyInSessionError):
    return generate_error_json("already.in.session.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_send_cmd_to_engine_error(ex: SendCmdToEngineError):
    return generate_error_json("send.cmd.to.engine.error", ex.get_error_message(), 500, ex.get_details_list())


def response_bad_conference_status_error(ex: BadConferenceStatusError):
    return generate_error_json("bad.conference.status.error", ex.get_error_message(), 500, ex.get_details_list())


def response_conference_locked_error(ex: ConferenceLockedError):
    return generate_error_json("conference.locked.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_conference_already_launched_error(ex: ConferenceAlreadyLaunchedError):
    return generate_error_json("conference.already.launched.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_conference_already_stopped_error(ex: ConferenceAlreadyStoppedError):
    return generate_error_json("conference.already.stopped.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_too_many_layouts_error(ex: TooManyLayoutsError):
    return generate_error_json("too.many.layouts.error", ex.get_error_message(), 400, ex.get_details_list())


def response_switch_conf_state_error(ex: SwitchConferenceStateError):
    return generate_error_json("switch.conference.state.error", ex.get_error_message(), 500, ex.get_details_list())


def response_set_layout_mode_error(ex: SetLayoutModeError):
    return generate_error_json("set.layout.mode.error", ex.get_error_message(), 500, ex.get_details_list())


def response_group_not_found_error(ex: GroupNotFoundError):
    return generate_error_json("group.not.found.error", ex.get_error_message(), 404, ex.get_details_list())


def response_duplicate_group_name_error(ex: DuplicateGroupNameError):
    return generate_error_json("duplicate.group.name.error", ex.get_error_message(), 400, ex.get_details_list())


def response_gen_group_id_error(ex: GenerateGroupIDError):
    return generate_error_json("gen.group.id.error", ex.get_error_message(), 500, ex.get_details_list())


def response_create_new_group_error(ex: CreateNewGroupError):
    return generate_error_json("create.new.group.error", ex.get_error_message(), 500, ex.get_details_list())


def response_update_group_error(ex: UpdateGroupError):
    return generate_error_json("update.group.error", ex.get_error_message(), 500, ex.get_details_list())


def response_del_group_abonents_error_error(ex: DeleteGroupAbonentsError):
    return generate_error_json("del.group.abonents.error", ex.get_error_message(), 500, ex.get_details_list())


def response_del_group_error(ex: DeleteGroupError):
    return generate_error_json("del.group.error", ex.get_error_message(), 500, ex.get_details_list())


def response_already_in_group_error(ex: AlreadyInGroup):
    return generate_error_json("already.in.group.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_add_contact_to_group_error(ex: AddContactToGroupError):
    return generate_error_json("add.contact.to.group.error", ex.get_error_message(), 500, ex.get_details_list())


def response_contact_not_in_group_error(ex: ContactNotInGroup):
    return generate_error_json("contact.not.in.group.error", ex.get_error_message(), 404,
                               ex.get_details_list())


def response_del_contact_from_group_error(ex: ErrorDelContactFromGroup):
    return generate_error_json("del.contact.from.group.error", ex.get_error_message(), 500, ex.get_details_list())


def response_bad_type_reg_error(ex: BadTypeRegistration):
    return generate_error_json("bad.type.reg.error", ex.get_error_message(), 400, ex.get_details_list())


def response_license_error(ex):
    return generate_error_json("bad.license.file.error", ex.get_error_message(), 400, ex.get_details_list())


def online_registration_error(ex: OnlineRegistrationError):
    return generate_error_json("online.registration.error.%d" % (ex.err_code),
                               ex.get_error_message(),
                               ex.http_code,
                               ex.get_details_list())


def response_parse_layout_coord_error(ex: ParseLayoutCoordError):
    return generate_error_json("parse.layout.coord.error", ex.get_error_message(), 500, ex.get_details_list())


def response_no_field_in_calc_layout_error(ex: NoFieldInCalculatedLayoutError):
    return generate_error_json("no.field.in.calculated.layout.error", ex.get_error_message(), 500,
                               ex.get_details_list())


def response_no_free_disk_space(ex: NoFreeDiskSpaceError):
    return generate_error_json("no.free.disk.space.error", ex.get_error_message(), 500, ex.get_details_list())


def response_video_storage_overflow(ex: VideoStorageOverflowError):
    return generate_error_json("video.storage.overflow.error", ex.get_error_message(), 500, ex.get_details_list())


def response_dir_not_found(ex: DirectoryNotFoundError):
    return generate_error_json("dir.not.found.error", ex.get_error_message(), 404,
                               ex.get_details_list())


def response_file_not_found(ex: MCUFileNotFoundError):
    return generate_error_json("file.not.found.error", ex.get_error_message(), 404,
                               ex.get_details_list())


def response_handling_file_error(ex: HandlingFileError):
    return generate_error_json("handling.file.error", ex.get_error_message(), 500, ex.get_details_list())


def response_bad_file_extension(ex: BadFileExtensionError):
    return generate_error_json("file.extension.error", ex.get_error_message(), 500,
                               ex.get_details_list())


def response_file_is_empty_error(ex: FileIsEmptyError):
    return generate_error_json("file.is.empty.error", ex.get_error_message(), 500, ex.get_details_list())


def response_setting_restricted_error(ex: SettingIsRestrictedByLicense):
    return generate_error_json("setting.is.restricted.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_max_active_participants(ex: MaxActiveParticipantsError):
    return generate_error_json("max.active.participants.error", ex.get_error_message(), 400, ex.get_details_list())


def response_create_export_file_error(ex: CreateExportFileError):
    return generate_error_json("create.export.file.error", ex.get_error_message(), 500, ex.get_details_list())


def response_file_io_error(ex: FileIOError):
    return generate_error_json("file.io.error", ex.get_error_message(), 500, ex.get_details_list())


def response_export_abook_error(ex: ExportAddressBookError):
    return generate_error_json("export.abook.error", ex.get_error_message(), 500, ex.get_details_list())


def response_unknown_abook_type_error(ex: UnknownAddressBookTypeError):
    return generate_error_json("unknown.abook.type.error", ex.get_error_message(), 400, ex.get_details_list())


def response_parse_file_error(ex: ParseFileError):
    return generate_error_json("parse.file.error", ex.get_error_message(), 500, ex.get_details_list())


def response_mount_net_dir_error(ex: MountNetworkDirectoryError):
    return generate_error_json("mount.net.dir.error", ex.get_error_message(), 500, ex.get_details_list())


def response_address_format_error(ex: InvalidAddressFormatError):
    return generate_error_json("address.format.error", ex.get_error_message(), 400, ex.get_details_list())


def response_pin_not_unique_error(ex: PinCodeNotUniqueError):
    return generate_error_json("pin.code.not.unique.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_max_conf_participants_error(ex: MaxConferenceParticipantsError):
    return generate_error_json("max.conference.participants.error", ex.get_error_message(), 400, ex.get_details_list())


def response_bad_call_protocol_error(ex: BadCallProtocolError):
    return generate_error_json("bad.call.protocol.error", ex.get_error_message(), 400, ex.get_details_list())


def response_getting_database_error(ex: GetDatabaseDataError):
    return generate_error_json("get.data.from.database.error", ex.get_error_message(), 500, ex.get_details_list())


def response_abonent_in_launched_conf_error(ex: AbonentInLaunchedConference):
    return generate_error_json("abonent.in.launched.conf.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_addr_in_launched_conf_error(ex: AddressIsUsedInLaunchedConference):
    return generate_error_json("address.in.launched.conf.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_recording_already_switched(ex: RecordingAlreadySwitchedOnError):
    return generate_error_json("recording.already.switched.on.error", ex.get_error_message(), 400,ex.get_details_list())


def response_record_empty_layout_error(ex: RecordEmptyLayoutError):
    return generate_error_json("record.empty.layout.error", ex.get_error_message(), 400, ex.get_details_list())


def response_pres_file_in_session_error(ex: PresFileIsUsedInLaunchedConference):
    return generate_error_json("presentation.file.in.launched.conference.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_get_settings_error(ex: GetSettingsError):
    return generate_error_json("get.settings.error", ex.get_error_message(), 500, ex.get_details_list())


def response_not_parity_error(ex: NotParityParamError):
    return generate_error_json("not.parity.parameter.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_min_greater_max_error(ex: MinGreaterMaxError):
    return generate_error_json("min.greater.max.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_authorizetion_error(ex: AuthorizationError):
    return generate_error_json("authorization.error", ex.get_error_message(), 401, ex.get_details_list())


def response_delete_active_conf_error(ex: DeleteActiveConfereneceError):
    return generate_error_json("delete.active.conf.error", "you cannot delete active conference", 400,
                               ex.get_details_list())


def response_network_interface_not_found_error(ex: NetworkInterfaceNotFoundError):
    return generate_error_json("network.interface.not.found.error", ex.get_error_message(), 404,
                               ex.get_details_list())


def response_network_manager_error(ex: NetworkManagerError):
    return generate_error_json("network.manager.error", ex.get_error_message(), 500, ex.get_details_list())


def response_network_manager_not_installed_error(ex: NetworkManagerNotInstalledError):
    return generate_error_json("network.manager.not.installed.error", ex.get_error_message(), 500,ex.get_details_list())


def response_sent_video_to_rtsp_error(ex: SendVideoToRTSPError):
    return generate_error_json("send.video.to.rtsp.error", ex.get_error_message(), 400, ex.get_details_list())


def feature_restricted_by_license_error(ex: FeatureRestrictedByLicenseError):
    return generate_error_json("feature.restricted.by.license.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_export_db_error(ex: ExportDatabaseError):
    return generate_error_json("export.database.error", ex.get_error_message(), 500, ex.get_details_list())


def response_import_db_error(ex: ImportDatabaseError):
    return generate_error_json("import.database.error", ex.get_error_message(), 500, ex.get_details_list())


def response_bad_layout_type(ex: BadLayoutTypeError):
    return generate_error_json("bad.layout.type.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def response_del_aspeaker_layout_error(ex: DeleteAspeakerLayoutError):
    return generate_error_json("delete.aspeaker.layout.error", ex.get_error_message(), 400, ex.get_details_list())


def response_concurrent_layouts_error(ex: ConcurrentLayoutsError):
    return generate_error_json("concurrent.layouts.error", ex.get_error_message(), 400, ex.get_details_list())


def response_not_compatible_settigs_error(ex: NotCompatibleSettingsError):
    return generate_error_json("not.compatible.settings.error", ex.get_error_message(), 400, ex.get_details_list())


def response_custom_layout_type_not_found_error(ex: CustomLayoutTypeNotFoundError):
    return generate_error_json("custom.layout.type.not.found", ex.get_error_message(), 400, ex.get_details_list())


def response_custom_layout_type_create_error(ex: CustomLayoutTypeCreateError):
    return generate_error_json("custom.layout.type.create.error", ex.get_error_message(), 400, ex.get_details_list())


def response_custom_layout_type_delete_error(ex: CustomLayoutTypeDeleteError):
    return generate_error_json("custom.layout.type.delete.error", ex.get_error_message(), 400, ex.get_details_list())


def response_custom_layout_type_change_error(ex: CustomLayoutTypeChangeError):
    return generate_error_json("custom.layout.type.change.error", ex.get_error_message(), 400, ex.get_details_list())


def responses_custom_layout_type_slots_intersect_error(ex: CustomLayoutTypeSlotsIntersectError):
    return generate_error_json("custom.layout.type.slots.intersect.error", ex.get_error_message(), 400,
                               ex.get_details_list())


def responses_custom_layout_type_is_used_error(ex: CustomLayoutTypeIsUsedError):
    return generate_error_json("custom.layout.type.is.used.error", ex.get_error_message(), 400, ex.get_details_list())


def responses_create_conference_config_error(ex: CreateConferenceConfigurationError):
    return generate_error_json("create.conference.config.error", ex.get_error_message(), 500, ex.get_details_list())


def responses_create_conference_error(ex: CreateConfereneceError):
    return generate_error_json("create.conference.error", ex.get_error_message(), 500, ex.get_details_list())


def responses_delete_conference_error(ex: DeleteConfereneceError):
    return generate_error_json("delete.conference.error", ex.get_error_message(), 500, ex.get_details_list())


def responses_change_conference_error(ex: ChangeConfereneceError):
    return generate_error_json("delete.conference.error", ex.get_error_message(), 500, ex.get_details_list())


def response_complex_error(ex: ComplexError):
    return generate_error_json("complex.error", ex.get_error_message(), ex.http_code, ex.get_details_list())


def response_power_management_error(ex: PowerManagementError):
    return generate_error_json("power.management.error", ex.get_error_message(), 500, ex.get_details_list())


def response_install_deb_error(ex: InstallDebError):
    return generate_error_json("install.deb.error", ex.get_error_message(), 500, ex.get_details_list())


def response_error(ex: Exception):
    # core
    if type(ex) == BadPageSizeError:
        return response_error_bad_page_size(ex)

    if type(ex) == BadStartIndexError:
        return response_error_bad_start_index(ex)

    if type(ex) == BadEndIndexError:
        return response_error_bad_end_index(ex)

    if type(ex) == ContactNotFoundError:
        return response_error_contact_not_found(ex)

    if type(ex) == DuplicateSipAddressError:
        return response_error_duplicate_abonent_found(ex.address, "sip", ex.get_error_message(), ex.get_details_list())

    if type(ex) == DuplicateH323AddressError:
        return response_error_duplicate_abonent_found(ex.address, "h323", ex.get_error_message(), ex.get_details_list())

    if type(ex) == DuplicateRTSPAddressError:
        return response_error_duplicate_abonent_found(ex.address, "rtsp", ex.get_error_message(), ex.get_details_list())

    if type(ex) == DuplicateFileUriError:
        return response_error_duplicate_abonent_found(ex.address, "file", ex.get_error_message(), ex.get_details_list())

    if type(ex) == DuplicateContactNameError:
        return response_error_duplicate_abonent_found(ex.name, "name", ex.get_error_message(), ex.get_details_list())

    if type(ex) == AllContactAddressesEmptyError:
        return response_all_addresses_empty(ex)

    if type(ex) == GenerateContactIDError:
        return response_error_gen_id(ex)

    if type(ex) == AddContactError:
        return response_mod_contact_error("add", ex.contactname, ex.get_details_list())

    if type(ex) == ChangeContactError:
        return response_mod_contact_error("change", "", ex.get_details_list())

    if type(ex) == DeleteContactError:
        return response_mod_contact_error("delete", ex.message, ex.get_details_list())

    if type(ex) == ConferenceNotFoundError:
        return response_error_conf_not_found(ex)

    if type(ex) == AudioConfigurationNotFoundError:
        return response_config_not_found("audio", ex)

    if type(ex) == VideoConfigurationNotFoundError:
        return response_config_not_found("video", ex)

    if type(ex) == UpdateAudioConfigError:
        return response_update_audio_config_error(ex)

    if type(ex) == UpdateVideoConfigError:
        return response_update_video_config_error(ex)

    if type(ex) == GetOnlineStateError:
        return response_online_state_error(ex)

    if type(ex) == BadLayoutNameError:
        return response_bad_layout_name(ex.layout_name, ex)

    if type(ex) == NoLayoutOwnerError:
        return response_no_layout_owner(ex)

    if type(ex) == ActiveConferenceNotFoundError:
        return response_active_conference_not_found(ex)

    if type(ex) == OnlineLayoutNotFoundError:
        return response_online_layout_not_found(ex)

    if type(ex) == TooManySlotRoles:
        return response_too_many_roles(ex)

    if type(ex) == DeleteGlobalLayoutError:
        return response_delete_global_layout(ex)

    if type(ex) == LayoutNotFoundError:
        return response_layout_not_found_error(ex)

    if type(ex) == OldLayoutFormatError:
        return response_old_layout_format_error(ex)

    if type(ex) == BadSchemaTypeError:
        return response_bad_schema_type_error(ex)

    if type(ex) == EmptyLayoutSlotsListError:
        return response_empty_layout_slots_list_error(ex)

    if type(ex) == EmptyLayoutOwnerError:
        return response_empty_layout_owner_error(ex)

    if type(ex) == LayoutOfAbonentAlreadyExistError:
        return response_layout_of_abonent_exist_error(ex)

    if type(ex) == GenerateLayoutIDError:
        return response_gen_layout_id_error(ex)

    if type(ex) == CreateLayoutError:
        return response_create_layout_error(ex)

    if type(ex) == AddLayoutToConferenceError:
        return response_add_layout_to_conference_error(ex)

    if type(ex) == AddAbonentsToLayoutError:
        return response_add_abonents_to_layout_error(ex)

    if type(ex) == BadAbonentsNumberError:
        return response_bad_abonents_number_error(ex)

    if type(ex) == DeleteLayoutAbonentsError:
        return response_delete_layout_abonents_error(ex)

    if type(ex) == UpdateLayoutError:
        return response_update_layout_error(ex)

    if type(ex) == DeleteScheduledLayoutFromConferenceError:
        return response_del_shed_layout_from_conf_error(ex)

    if type(ex) == DeleteLayoutError:
        return response_delete_shed_layout_error(ex)

    if type(ex) == GetConferenceParticipantError:
        return response_get_conf_participant_error(ex)

    if type(ex) == CreateConferenceParticipantError:
        return response_create_conf_participant_error(ex)

    if type(ex) == ConferenceParticipantNotFoundError:
        return response_conf_partic_not_found_error(ex)

    if type(ex) == DeleteConferenceParticipantError:
        return response_delete_conf_participant_error(ex)

    if type(ex) == DeleteLayoutFromConferenceError:
        return response_delete_layout_conf_participant_error(ex)

    if type(ex) == ChangeConferenceParticipantError:
        return response_change_conf_participant_error(ex)

    if type(ex) == SessionAbonentNotFoundError:
        return response_session_abonent_not_found_error(ex)

    if type(ex) == AlreadyInSessionError:
        return response_already_in_session_error(ex)

    if type(ex) == SendCmdToEngineError:
        return response_send_cmd_to_engine_error(ex)

    if type(ex) == BadConferenceStatusError:
        return response_bad_conference_status_error(ex)

    if type(ex) == ConferenceLockedError:
        return response_conference_locked_error(ex)

    if type(ex) == ConferenceAlreadyLaunchedError:
        return response_conference_already_launched_error(ex)

    if type(ex) == ConferenceAlreadyStoppedError:
        return response_conference_already_stopped_error(ex)

    if type(ex) == TooManyLayoutsError:
        return response_too_many_layouts_error(ex)

    if type(ex) == SwitchConferenceStateError:
        return response_switch_conf_state_error(ex)

    if type(ex) == SetLayoutModeError:
        return response_set_layout_mode_error(ex)

    if type(ex) == GroupNotFoundError:
        return response_group_not_found_error(ex)

    if type(ex) == DuplicateGroupNameError:
        return response_duplicate_group_name_error(ex)

    if type(ex) == GenerateGroupIDError:
        return response_gen_group_id_error(ex)

    if type(ex) == CreateNewGroupError:
        return response_create_new_group_error(ex)

    if type(ex) == UpdateGroupError:
        return response_update_group_error(ex)

    if type(ex) == DeleteGroupAbonentsError:
        return response_del_group_abonents_error_error(ex)

    if type(ex) == DeleteGroupError:
        return response_del_group_error(ex)

    if type(ex) == AlreadyInGroup:
        return response_already_in_group_error(ex)

    if type(ex) == AddContactToGroupError:
        return response_add_contact_to_group_error(ex)

    if type(ex) == ContactNotInGroup:
        return response_contact_not_in_group_error(ex)

    if type(ex) == ErrorDelContactFromGroup:
        return response_del_contact_from_group_error(ex)

    if type(ex) == SaveTemporaryLicenseError:
        return response_license_error(ex)

    if type(ex) == SplitGroupLicensesError:
        return response_license_error(ex)

    if type(ex) == SaveZippedLicensesError:
        return response_license_error(ex)

    if type(ex) == OnlineRegistrationError:
        return online_registration_error(ex)

    if type(ex) == ParseLayoutCoordError:
        return response_parse_layout_coord_error(ex)

    if type(ex) == NoFieldInCalculatedLayoutError:
        return response_no_field_in_calc_layout_error(ex)

    if type(ex) == NoFreeDiskSpaceError:
        return response_no_free_disk_space(ex)

    if type(ex) == VideoStorageOverflowError:
        return response_video_storage_overflow(ex)

    if type(ex) == DirectoryNotFoundError:
        return response_dir_not_found(ex)

    if type(ex) == MCUFileNotFoundError:
        return response_file_not_found(ex)

    if type(ex) == HandlingFileError:
        return response_handling_file_error(ex)

    if type(ex) == BadFileExtensionError:
        return response_bad_file_extension(ex)

    if type(ex) == FileIsEmptyError:
        return response_file_is_empty_error(ex)

    if type(ex) == SettingIsRestrictedByLicense:
        return response_setting_restricted_error(ex)

    if type(ex) == MaxActiveParticipantsError:
        return response_max_active_participants(ex)

    if type(ex) == CreateExportFileError:
        return response_create_export_file_error(ex)

    if type(ex) == FileIOError:
        return response_file_io_error(ex)

    if type(ex) == ExportAddressBookError:
        return response_export_abook_error(ex)

    if type(ex) == UnknownAddressBookTypeError:
        return response_unknown_abook_type_error(ex)

    if type(ex) == ParseFileError:
        return response_parse_file_error(ex)

    if type(ex) == MountNetworkDirectoryError:
        return response_mount_net_dir_error(ex)

    if type(ex) == InvalidAddressFormatError:
        return response_address_format_error(ex)

    if type(ex) == PinCodeNotUniqueError:
        return response_pin_not_unique_error(ex)

    if type(ex) == MaxConferenceParticipantsError:
        return response_max_conf_participants_error(ex)

    if type(ex) == BadCallProtocolError:
        return response_bad_call_protocol_error(ex)

    if type(ex) == GetDatabaseDataError:
        return response_getting_database_error(ex)

    if type(ex) == UtiliteExecutionError:
        return response_util_execute_error(ex)

    if type(ex) == AbonentInLaunchedConference:
        return response_abonent_in_launched_conf_error(ex)

    if type(ex) == AddressIsUsedInLaunchedConference:
        return response_addr_in_launched_conf_error(ex)

    if type(ex) == RecordingAlreadySwitchedOnError:
        return response_recording_already_switched(ex)

    if type(ex) == RecordEmptyLayoutError:
        return response_record_empty_layout_error(ex)

    if type(ex) == PresFileIsUsedInLaunchedConference:
        return response_pres_file_in_session_error(ex)

    if type(ex) == GetSettingsError:
        return response_get_settings_error(ex)

    if type(ex) == DeleteActiveConfereneceError:
        return response_delete_active_conf_error(ex)

    if type(ex) == NetworkInterfaceNotFoundError:
        return response_network_interface_not_found_error(ex)

    if type(ex) == NetworkManagerError:
        return response_network_manager_error(ex)

    if type(ex) == NetworkManagerNotInstalledError:
        return response_network_manager_not_installed_error(ex)

    if type(ex) == SendVideoToRTSPError:
        return response_sent_video_to_rtsp_error(ex)

    if type(ex) == FeatureRestrictedByLicenseError:
        return feature_restricted_by_license_error(ex)

    if type(ex) == ExportDatabaseError:
        return response_export_db_error(ex)

    if type(ex) == ImportDatabaseError:
        return response_import_db_error(ex)

    if type(ex) == BadLayoutTypeError:
        return response_bad_layout_type(ex)

    if type(ex) == DeleteAspeakerLayoutError:
        return response_del_aspeaker_layout_error(ex)

    if type(ex) == ConcurrentLayoutsError:
        return response_concurrent_layouts_error(ex)

    if type(ex) == NotCompatibleSettingsError:
        return response_not_compatible_settigs_error(ex)

    if type(ex) == CustomLayoutTypeNotFoundError:
        return response_custom_layout_type_not_found_error(ex)

    if type(ex) == CustomLayoutTypeCreateError:
        return response_custom_layout_type_create_error(ex)

    if type(ex) == CustomLayoutTypeDeleteError:
        return response_custom_layout_type_delete_error(ex)

    if type(ex) == CustomLayoutTypeChangeError:
        return response_custom_layout_type_change_error(ex)

    if type(ex) == CustomLayoutTypeSlotsIntersectError:
        return responses_custom_layout_type_slots_intersect_error(ex)

    if type(ex) == CustomLayoutTypeIsUsedError:
        return responses_custom_layout_type_is_used_error(ex)

    if type(ex) == CreateConferenceConfigurationError:
        return responses_create_conference_config_error(ex)

    if type(ex) == CreateConfereneceError:
        return responses_create_conference_error(ex)

    if type(ex) == DeleteConfereneceError:
        return responses_delete_conference_error(ex)

    if type(ex) == ChangeConfereneceError:
        return responses_change_conference_error(ex)

    if type(ex) == ComplexError or type(ex) == InvalidAbookContactParamsError or \
        type(ex) == InvalidConferenceParamsError or type(ex) == InvalidConferenceParticipantParamsError or \
        type(ex) == InvalidScheduledLayoutParamsError or type(ex) == InvalidOnlineLayoutParamsError:
        return response_complex_error(ex)

    if type(ex) == PowerManagementError:
        return response_power_management_error(ex)

    if type(ex) == InstallDebError:
        return response_install_deb_error(ex)

    # api
    if type(ex) == BadIntInRequestError:
        return response_error_bad_int_in_request(ex)

    if type(ex) == ValueNotInLimitsError:
        return response_limits_error(ex)

    if type(ex) == BadStringLengthError:
        return response_string_limits_error(ex)

    if type(ex) == NoRequiredError:
        return response_no_required_error(ex)

    if type(ex) == BadFormatFieldError:
        return response_bad_field_error(ex)

    if type(ex) == BadTypeRegistration:
        return response_bad_type_reg_error(ex)

    if type(ex) == NotParityParamError:
        return response_not_parity_error(ex)

    if type(ex) == MinGreaterMaxError:
        return response_min_greater_max_error(ex)

    if type(ex) == AuthorizationError:
        return response_authorizetion_error(ex)

    return response_error_default()

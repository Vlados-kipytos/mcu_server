#!/usr/bin/python3

import app.core.tools as tools


class BadIntInRequestError(Exception):
    def __init__(self, field: str, bad_int: str):
        self._field = field
        self._bad_int = bad_int

    @property
    def field(self) -> str:
        return self._field

    @property
    def bad_int(self) -> str:
        return self._bad_int

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.integer.field", "Bad integer field", self._field, None) ]


class ValueNotInLimitsError(Exception):
    def __init__(self, field: str, value: str, limits: str):
        self._field = field
        self._value = value
        self._limits = limits

    @property
    def field(self) -> str:
        return self._field

    @property
    def value(self) -> str:
        return self._value

    @property
    def limits(self) -> str:
        return self._limits

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("value.out.of.boundce", "Value out of boundce", self._field, None) ]


class BadStringLengthError(Exception):
    def __init__(self, field: str, string: str, down: int, up: int):
        self._field = field
        self._string = string
        self._down = down
        self._up = up

    @property
    def field(self) -> str:
        return self._field

    @property
    def string(self) -> str:
        return self._string

    @property
    def len(self) -> int:
        return len(self._string)

    @property
    def lim(self) -> str:
        if self._down == 0:
            return "less than %d" % (self._up)
        if self._up == 0:
            return "more than %d" % (self._down)
        return "%d .. %d" % (self._down, self._up)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.string.field.length", "Bad string field length", self._field, None) ]


class NoRequiredError(Exception):
    def __init__(self, field: str):
        self._field = field

    @property
    def field(self) -> str:
        return self._field

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("no.required.field", "Required field is not found", self._field, None) ]


class BadFormatFieldError(Exception):
    def __init__(self, field: str, _value: str, must_be: str):
        self._field = field
        self._value = _value
        self._must_be = must_be

    @property
    def field(self) -> str:
        return self._field

    @property
    def value(self) -> str:
        return self._value

    @property
    def must_be(self) -> str:
        return self._must_be

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.format.field",
                                             "Bad format of field '%s'. Must be '%s'" % (self._field, self._must_be),
                                             self._field, None) ]


class AllContactAddressesEmptyError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "All contact addresses are empty. Set at lest one address."


class BadTypeRegistration(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "Bad type of registration. Use 'online' or 'offline'"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.registration.type.error", self.get_error_message(), "type", None) ]


class NotParityParamError(Exception):
    def __init__(self, field_name: str, value: int):
        self._field_name = field_name
        self._value = value

    @property
    def field_name(self) -> str:
        return self._field_name

    @property
    def value(self) -> int:
        return self._value

    def get_error_message(self):
        return "Parameter '%s' is not parity (value=%d)." % (self._field_name, self._value)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("no.parity.field", "Field is not parity", self._field_name, None) ]


class MinGreaterMaxError(Exception):
    def __init__(self, min_value: int, max_value: int, field_names: list):
        self._min_value = min_value
        self._max_value = max_value
        self._field_names = field_names

    @property
    def min_value(self) -> int:
        return self._min_value

    @property
    def max_value(self) -> int:
        return self._max_value

    @property
    def field_names(self) -> list:
        return self._field_names

    def get_error_message(self):
        return "Min value %d >= max value %d." % (self._min_value, self._max_value)

    def get_details_list(self) -> list:
        details = []
        for field in self._field_names:
            item = tools.get_detail_error_item("min.greater.max.field", "Min field is greater max field", field, None)
            details.append(item)
        return details


class AuthorizationError(Exception):
    def __init__(self, message: str):
        self._message = message

    @property
    def message(self) -> str:
        return self._message

    def get_error_message(self):
        return self._message

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("authorization.error", self.get_error_message(), "token", None) ]



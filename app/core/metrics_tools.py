#!/usr/bin/python3

import time

import psutil

from app.core.entities import CoreEntity
from app.core.entities import FreeSpaceEntity
from app.core.entities import MemoryDashboardEntity
from app.core.entities import NetInterfaceEntity


def get_memory_usage():
    mem = psutil.virtual_memory()
    mb = 1024 * 1024
    all = int(mem.total / mb)
    using = int(mem.used / mb)
    free = int(mem.free / mb)

    return MemoryDashboardEntity(all, using, free)


def get_cpu_loading():
    cpu_list = []

    perc = psutil.cpu_times_percent(1, True)

    if len(perc) == 0:
        return cpu_list  # error

    for core in perc:
        cpu_list.append(CoreEntity(core.user, core.system, core.idle))

    return cpu_list


def get_net_metrics():
    old_time = time.time()
    net_state = psutil.net_io_counters(True)
    ifaces = psutil.net_if_addrs()

    old_values = []

    # calculate received/sent bytes for each interface exclude "lo"
    for name, val in ifaces.items():
        if name == "lo":
            continue
        stat = net_state[name]
        old_values.append({"interface": name, "rx": stat.bytes_recv, "tx": stat.bytes_sent})

    time.sleep(1)

    diff = time.time() - old_time

    net_state = psutil.net_io_counters(True)
    ifaces = psutil.net_if_addrs()

    res = []

    # calculate received/sent bytes for each interface exclude "lo"
    for name, val in ifaces.items():
        if name == "lo":
            continue
        stat = net_state[name]
        for old in old_values:
            if old["interface"] == name:
                interface_name = name
                interface_rx = int(((stat.bytes_recv - old["rx"]) * 8) / (diff * 1024))
                interface_tx = int(((stat.bytes_sent - old["tx"]) * 8) / (diff * 1024))
                res.append(NetInterfaceEntity(interface_name, interface_rx, interface_tx))

    return res


def get_free_space():
    MEGABYTE = 2 ** 20

    res = []

    for dp in psutil.disk_partitions():
        try:
            du = psutil.disk_usage(dp.mountpoint)
        except Exception:
            continue
        res.append(FreeSpaceEntity(dp.device, int(du.free / MEGABYTE), int(du.total / MEGABYTE)))

    return res

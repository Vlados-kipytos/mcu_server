#!/usr/bin/python3

import logging
import os
import os.path
import sys
import time

import app.core.conftools as conftools
import app.core.constants as constants
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.features as features
import app.core.layoutbackup as layoutbackup
import app.core.nettools as nettools
import app.core.onlinetools as onlinetools
import app.core.profile_settings as profile_settings
import app.core.tools as tools
import app.core.xmltools as xmltools
from app.core.entities import RecordingURLEntity
from app.core.errors import ConferenceAlreadyStoppedError
from app.core.errors import NoFreeDiskSpaceError
from app.core.errors import RecordEmptyLayoutError
from app.core.errors import RecordingAlreadySwitchedOnError
from app.core.errors import SettingIsRestrictedByLicense
from app.core.errors import VideoStorageOverflowError
from app.core.requests import SwitchVideoRecordRequest
from app.core.responses import GetRecordingStatusResponse
from app.core.responses import SessionControlResponse




def get_video_file_name():
    """This function returns unique file name for video files."""
    timestr = tools.get_unique_time_stamp_str()
    ctrl_params = xmltools.get_config()
    SLASH = os.path.sep
    fname = ctrl_params["dirs"]["videodir"] + SLASH + "vile_" + timestr + ".mkv"
    return (fname)


def is_autostop_record(confid: int) -> bool:
    autostop_rows = dbtools.select(
        "SELECT rec_autostop FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            confid))
    if len(autostop_rows) != 0:
        return autostop_rows[0][0]
    else:
        return False


def is_write_to_file(confid: int) -> bool:
    write_rows = dbtools.select(
        "SELECT rec_to_file FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            confid))
    if len(write_rows) != 0:
        return write_rows[0][0]
    else:
        return False


def is_video_storage_overflow() -> bool:
    """This function check overflow of video storage. If overflow, then return True."""

    maxvideosz = profile_settings.get_video_storage_size()
    if maxvideosz == 0:  # unlimited video storage
        return False

    ctrl_params = xmltools.get_config()
    cur_video_size = tools.get_dir_size(ctrl_params["dirs"]["videodir"], "mkv")
    if cur_video_size > maxvideosz:
        return True
    else:
        return False


def can_write_video() -> bool:
    if tools.get_free_disk_space() < 100 or is_video_storage_overflow():
        return False

    return True


def is_record_disabled_by_license():
    features_list = features.get_features_list()
    record_enabled = features.get_feature(features_list, "RECORD")
    return not record_enabled


def get_video_recording_status(confid: int) -> GetRecordingStatusResponse:
    """Return video recording status for conference 'confid'."""

    connstr, confstr = onlinetools.get_online_state()
    conf_json = onlinetools.extract_conference(confstr, confid)

    status = "off"
    if "record_status" in conf_json:
        status = conf_json["record_status"]

    url_list = []
    if "recording" in conf_json:
        for urldata in conf_json["recording"]:
            url_list.append(RecordingURLEntity(urldata["type"], urldata["url"]))

    return GetRecordingStatusResponse(status, url_list)


def is_url_recording(recording_status: GetRecordingStatusResponse, url_type: str) -> bool:
    """Check recording status: if exist recording with specified URL type, then return True."""
    for url_data in recording_status.urls:
        if url_data.type == url_type:
            return True
    return False


def get_all_conf_record_state() -> dict:
    """This function returns dictionary: obj[active confid] = { state of recording }."""

    conf_rows = dbtools.select("""SELECT conferences.id, conferences.rtmp_ab_id, configurations.rec_at_start, configurations.rec_autostop 
        FROM legacy.conferences, legacy.configurations WHERE conferences.configid=configurations.id AND conferences.state=%d""" % (
        data.CONF_STATE_ACTIVE))

    connstr, confstr = onlinetools.get_online_state()
    conf_list_json = onlinetools.extract_conf_list(confstr)

    all_conf_dict = {}
    for row in conf_rows:
        confid = row[0]
        rtmp_ab_id = row[1]
        autostart = tools.int_to_bool(row[2])
        autostop = tools.int_to_bool(row[3])

        layout_id = layoutbackup.get_layout_id_by_owner(confid, rtmp_ab_id)
        lid = tools.get_layout_name_by_id(confid, layout_id)

        cid = tools.conference_name_by_id(confid)

        recording = False
        record_layout_empty = True
        for conf_json in conf_list_json:
            if conf_json["name"] == cid:
                if "record_status" in conf_json:
                    recording = conf_json["record_status"] == "in_progress"
                for lyt in conf_json["layout_list"]:
                    if lyt["name"] == lid:
                        record_layout_empty = len(lyt["rx"]["connections"]) == 0
                        break
                break

        all_conf_dict[confid] = {"autostart": autostart, "autostop": autostop, "recording": recording,
                                 "record_layout_empty": record_layout_empty}

    return all_conf_dict


def stop_all_recording():
    cmd_list = []
    conf_rows = dbtools.select("SELECT id FROM legacy.conferences WHERE state=%d OR state=%d" % (
        data.CONF_STATE_ACTIVE, data.CONF_STATE_START))
    for conf in conf_rows:
        id = conf[0]
        cid = tools.conference_name_by_id(id)
        cmd_list.append("REC STOP " + cid)
    if len(cmd_list) != 0:
        nettools.exec_cmd_list_s(cmd_list)


def set_video_record_settings(confid: int, cmd_list: list):
    """Add to 'cmd_list' commands to configure video recording."""
    cid = tools.conference_name_by_id(confid)

    # b_autostop = is_autostop_record(confid)
    # cmd_list.append("REC AUTOSTART ON %s" % (cid))
    # cmd_list.append("REC AUTOSTOP %s %s" % (tools.num_to_onoff(b_autostop), cid))

    config_rows = dbtools.select(
        "SELECT width, height, fps FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            confid))
    if len(config_rows) != 0:
        width = config_rows[0][0]
        height = config_rows[0][1]
        fps = config_rows[0][2]
        # logging.error("width=%d , height = %d , fps=%d" % (width, height, fps))
        bitrate = int(width * height * fps * 0.125) / 1024  # 0.125 bit of bitrate per pixel
        cmd_list.append("REC BITRATE %d" % (bitrate))
        cmd_list.append("REC BITRATE %s %d" % (cid, bitrate))

    conf_rows = dbtools.select("SELECT name FROM legacy.conferences WHERE id=%d" % (confid))
    if len(conf_rows) != 0:
        conf_name = conf_rows[0][0].strip()
        title = "videorecord of conference: '%s'" % (conf_name)
        cmd_list.append("CONFERENCE TITLE %s %s" % (cid, title))


def switch_video_record_manually(request: SwitchVideoRecordRequest):
    """Switch on/off video recording manually (switch one recording URL only)."""

    if request.turn_on:
        if is_record_disabled_by_license():
            raise SettingIsRestrictedByLicense("videorecord")

    conf_status = conftools.get_conference_status(request.conference_id)
    if not conf_status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    video_conf_rows = dbtools.select(
        "SELECT rtmp_ab_id, rtmp_url FROM legacy.conferences WHERE id=%s" % (request.conference_id))
    if len(video_conf_rows) != 0:
        rtmp_ab_id = video_conf_rows[0][0]  # id of owner of recorded layout
        rtmp_url = video_conf_rows[0][1]  # rtmp url to send video
        rtmp_url = rtmp_url.strip()
    else:
        rtmp_ab_id = 0
        rtmp_url = ""

    # get recorded layout name (global layout by default)
    # layout = onlinetools.get_layout_of_abonent(confid, rtmp_ab_id)
    layout_id = layoutbackup.get_layout_id_by_owner(request.conference_id, rtmp_ab_id)
    layout = tools.get_layout_name_by_id(request.conference_id, layout_id)
    cid = tools.conference_name_by_id(request.conference_id)

    recording_status = get_video_recording_status(request.conference_id)

    cmd_list = []

    if request.turn_on:
        # if 'autostart' ON, then not recording empty layout
        rec_state_dict = get_all_conf_record_state()
        if request.conference_id in rec_state_dict:
            state = rec_state_dict[request.conference_id]
            if state["autostart"] and state["record_layout_empty"]:
                raise RecordEmptyLayoutError()

        if is_url_recording(recording_status, request.url_type):
            raise RecordingAlreadySwitchedOnError(request.url_type)

        set_video_record_settings(request.conference_id, cmd_list)

        if len(recording_status.urls) == 0:
            cmd_list.append("REC ON " + cid + " " + layout)

        if request.url_type == "rtmp" and rtmp_url != "":
            cmd_list.append("REC LOWLATENCY ON %s" % (cid))
            cmd_list.append("REC START " + cid + " " + rtmp_url)

        if request.url_type == "file":
            if tools.get_free_disk_space() == 0:
                raise NoFreeDiskSpaceError()

            if is_video_storage_overflow():
                raise VideoStorageOverflowError()

            cmd_list.append("REC LOWLATENCY OFF %s" % (cid))
            cmd_list.append("REC START " + cid + " " + get_video_file_name())
    else:
        if is_url_recording(recording_status, request.url_type):
            recording_url = ""
            for urldata in recording_status.urls:
                if urldata.type == request.url_type:
                    recording_url = urldata.url
                    break
            if recording_url != "":
                cmd_list.append("REC STOP " + cid + " " + recording_url)
                if len(recording_status.urls) == 1:  # stop last recording stream
                    cmd_list.append("REC OFF " + cid)
        else:
            logging.error("switch_video_record_manually(): no recording with type '%s'" % (request.url_type))

    nettools.exec_cmd_list_s(cmd_list)

    return SessionControlResponse(request.conference_id)


def switch_video_record_by_flags(confid: int, turn_on: bool) -> SessionControlResponse:
    """This function switch video record for conference 'confid'."""

    if turn_on:
        if is_record_disabled_by_license():
            raise SettingIsRestrictedByLicense("videorecord")

    status = conftools.get_conference_status(confid)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    video_conf_rows = dbtools.select("SELECT rtmp_ab_id, rtmp_url FROM legacy.conferences WHERE id=%s" % (confid))
    if len(video_conf_rows) != 0:
        rtmp_ab_id = video_conf_rows[0][0]  # id of owner of recorded layout
        rtmp_url = video_conf_rows[0][1]  # rtmp url to send video
        rtmp_url = rtmp_url.strip()
    else:
        rtmp_ab_id = 0
        rtmp_url = ""

    # get recorded layout name (global layout by default)
    # layout = onlinetools.get_layout_of_abonent(confid, rtmp_ab_id)
    layout_id = layoutbackup.get_layout_id_by_owner(confid, rtmp_ab_id)
    layout = tools.get_layout_name_by_id(confid, layout_id)
    cid = tools.conference_name_by_id(confid)

    cmd_list = []

    if turn_on:
        set_video_record_settings(confid, cmd_list)

        b_trans_to_rtmp = rtmp_url != ""
        b_write_to_file = is_write_to_file(confid)

        cmd_list.append("REC LOWLATENCY %s %s" % (tools.num_to_onoff(b_trans_to_rtmp), cid))

        cmd_list.append("REC ON " + cid + " " + layout)

        # translate to specified URL (if need)
        if b_trans_to_rtmp:
            cmd_list.append("REC START " + cid + " " + rtmp_url)

        # write to file (if need)
        if b_write_to_file:
            if tools.get_free_disk_space() == 0:
                raise NoFreeDiskSpaceError()

            if is_video_storage_overflow():
                raise VideoStorageOverflowError()

            cmd_list.append("REC START " + cid + " " + get_video_file_name())
    else:
        cmd_list.append("REC STOP " + cid)
        cmd_list.append("REC OFF " + cid)

    nettools.exec_cmd_list_s(cmd_list)

    return SessionControlResponse(confid)


def is_record_at_start_conf(confid: int) -> bool:
    """This function return flag 'start video record at begin of conference'."""
    rec_rows = dbtools.select(
        "SELECT rec_at_start FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            confid))
    if len(rec_rows) != 0:
        return rec_rows[0][0] != 0
    else:
        return False


def switch_video_record2(confid: int, turn_on: bool):
    try:
        switch_video_record_by_flags(confid, turn_on)
    except:
        logging.error("switch_video_record2(): " + str(sys.exc_info()))

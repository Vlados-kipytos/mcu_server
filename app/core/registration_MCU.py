#!/usr/bin/python3

import logging
import platform
import sys
import xml.etree.ElementTree as eltree

import requests

import app.core.cert_functions as cert_func
import app.core.sysinfotools as sysinfotools
import app.core.tools as tools


def parse_client_data_file(data_file_name: str):
    """Load from file 'data_file_name' client params."""

    result = {"result_code": cert_func.SUCCESS, "domain": "", "serial": ""}

    # parse xml with client data
    try:
        tree = eltree.parse(data_file_name)
        root = tree.getroot()

        result["domain"] = root.find("domain").text
        result["serial"] = root.find("serial").text
    except IOError:
        cert_func.log_msg("parse_client_data_file() IOError at reading client data: " + str(sys.exc_info()))
        result["result_code"] = cert_func.NO_CLIENTDATA
    except Exception:
        cert_func.log_msg("parse_client_data_file() Exception at reading client data: " + str(sys.exc_info()))
        result["result_code"] = cert_func.ERROR_CLIENTDATA

    return result


def parse_sys_data_file(hwinfo_file_name: str):
    """Load from file 'hwinfo_file_name' system information."""

    result = {"result_code": cert_func.SUCCESS, "hdd_id": "", "mac_address": "", "bios_id": ""}

    # parse sysdata.xml
    try:
        tree = eltree.parse(hwinfo_file_name)
        root = tree.getroot()

        result["hdd_id"] = root.find("hdd").attrib['id']
        result["mac_address"] = root.find("mac").attrib['id']
        result["bios_id"] = root.find("smb_uuid").attrib['id']
    except IOError:
        cert_func.log_msg(
            "parse_sys_data_file() IOError at reading sysdata: " + str(hwinfo_file_name) + " " + str(sys.exc_info()))
        result["result_code"] = cert_func.NO_SYSDATA
    except Exception:
        cert_func.log_msg("parse_sys_data_file() Exception at reading sysdata: " + str(sys.exc_info()))
        result["result_code"] = cert_func.ERROR_SYSDATA

    return result


def build_csr_file(data_file, hwinfo_file, private_key_file, csr_file):
    """Build CSR-file and save it to 'csr_file'."""

    # parse xml with client data
    client_data = parse_client_data_file(data_file)
    if client_data["result_code"] != cert_func.SUCCESS:
        cert_func.log_msg("build_csr_file() Error at parsing client data")
        return client_data["result_code"]

    # parse sysdata.xml
    sys_data = parse_sys_data_file(hwinfo_file)
    if sys_data["result_code"] != cert_func.SUCCESS:
        cert_func.log_msg("build_csr_file() Error at parsing sys data")
        return sys_data["result_code"]

    # generate CSR
    try:
        # generate private key
        key = cert_func.generate_key()
        cert_func.write_key(private_key_file, key)

        # get version
        all_vers = sysinfotools.get_all_versions()
        if "installer" in all_vers:
            version = all_vers["installer"]
        else:
            version = sysinfotools.DEFAULT_VERSION

        # get cpu name
        if platform.system() == "Linux":
            with open('/proc/cpuinfo') as f:
                for line in f:
                    # Ignore the blank line separating the information between
                    # details about two processing units
                    if line.strip():
                        if line.rstrip('\n').startswith('model name'):
                            cpu_info = line.rstrip('\n').split(':')[1].strip()
        else:
            # import platform
            cpu_info = platform.processor()

        # get os name
        os_info = cert_func.get_python_os_info()

        # generating CSR with parse parameters
        csr_raw = cert_func.generate_csr(client_data["domain"], key, client_data["serial"], sys_data["mac_address"],
                                         sys_data["bios_id"], version, os_info, cpu_info)
        csr = cert_func.csr_to_str(csr_raw)
        cert_func.write_csr(csr_file, csr_raw)

    except Exception:
        cert_func.log_msg("build_csr_file() error at generating CSR: " + str(sys.exc_info()))
        return cert_func.ERROR_CSR

    return cert_func.SUCCESS


"""
Parameters:
    scripts_params_file - file with URL's for requests
    data_file           - file with client data, for generating CSR (domain, country, organization, serial, etc)
    hwinfo_file         - file with hardware info
    private_key_file    - file for save private key [OUT]
    csr_file            - file for save generating CSR [OUT]
    cert_file           - file for save getting certificate [OUT]
"""


def registration_MCU(scripts_params_file, data_file, hwinfo_file, private_key_file, csr_file, cert_file, ca_cert_fname):
    # load URL for this request

    logging.debug("registration_MCU(): try to read URL from scripts params file %s" % (scripts_params_file))

    try:
        tree = eltree.parse(scripts_params_file)
        root = tree.getroot()

        # url for POST-request
        url_for_req = root.find("url_reg").text
    except IOError:
        cert_func.log_msg("registration_MCU() IOError at reading URL: " + str(sys.exc_info()))
        return cert_func.build_error_dict(cert_func.NO_SCRIPT_PARAMS)
    except Exception:
        cert_func.log_msg("registration_MCU() Exception at reading URL: " + str(sys.exc_info()))
        return cert_func.build_error_dict(cert_func.ERROR_SCRIPT_PARAMS)

    logging.debug("registration_MCU(): try to build csr-file %s" % (csr_file))

    # generate CSR
    build_csr_res = build_csr_file(data_file, hwinfo_file, private_key_file, csr_file)
    if build_csr_res != cert_func.SUCCESS:
        cert_func.log_msg("registration_MCU() Error at building CSR-file")
        return cert_func.build_error_dict(build_csr_res)

    logging.debug("registration_MCU(): create tmp CA certificate file")

    if not tools.save_text_to_file(ca_cert_fname, cert_func.CA_CRT):
        cert_func.log_msg("update_cert(): cannot save CA certificate to file")
        return cert_func.build_error_dict(cert_func.ERROR_CREATE_CA_CERT_FILE)

    logging.debug("registration_MCU(): make request to URL=%s to get certificate..." % (url_for_req))

    # POST-request to server
    try:
        with open(csr_file, 'rb') as f:
            r = requests.post(url_for_req, files={'csr': f}, verify=ca_cert_fname)

        if (r.status_code != 200):
            cert_func.log_msg("registration_MCU() bad http ansver code or redirect: " + str(r.status_code))
            cert_func.log_msg("registration_MCU() ansver: " + str(r.text.encode('utf-8')))
            tools.delete_file(ca_cert_fname)  # delete tmp file
            return cert_func.build_error_dict_ex(cert_func.GET_CERT_HTTP_ERROR, r.text)

    except Exception:
        cert_func.log_msg("registration_MCU() error at connection: " + str(sys.exc_info()))
        tools.delete_file(ca_cert_fname)  # delete tmp file
        return cert_func.build_error_dict(cert_func.ERROR_CONNECTION)

    tools.delete_file(ca_cert_fname)  # delete tmp file

    logging.debug("registration_MCU(): try to save certificate to file %s..." % (cert_file))

    # handle response
    try:
        with open(cert_file, "wb") as f:
            f.write(r.content)
    except:
        cert_func.log_msg(
            "registration_MCU() error at getting certificate and write it to file: " + str(sys.exc_info()))
        return cert_func.build_error_dict(cert_func.ERROR_SAVE_CERT)

    return cert_func.build_error_dict(cert_func.SUCCESS)

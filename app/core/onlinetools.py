#!/usr/bin/python3

import logging
import sys

import app.core.addrbook as addrbook
import app.core.constants as constants
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.layoutbackup as layoutbackup
import app.core.nettools as nettools
import app.core.sysinfotools as sysinfotools
import app.core.tools as tools
from app.core.entities import ConfSessionParticipantEntity
from app.core.entities import DisconnectedSessionParticipantEntity
from app.core.entities import SessionParticipantEntity
from app.core.entities import WaitAbonentEntity
from app.core.responses import GetPreviewStateResponse


def get_online_state():
    # get online state from buffer (in database)
    online_data_rows = dbtools.load_table("legacy.onlinedata", "connections, conferences", "")
    if len(online_data_rows) != 0:
        connstr = online_data_rows[0][0]
        confstr = online_data_rows[0][1]
    else:
        connstr = '{ "connection_list": [] }'
        confstr = '{ "conference_list": [] }'

    return (connstr, confstr)


def get_config_from_engine():
    config_json = {}

    config_str = nettools.get_url_data_s("config.json", True)
    if config_str != "":
        true = True  # define constants "true"/"false"
        false = False
        try:
            config_json = eval(config_str)  # convert string to python expression (json format)
        except:
            return ({})

    return config_json


def is_server_disconnected():
    """This function checks vtsrv state: if this script cannot connect to vtsrv and vtsrv is not in restarting, then return 1"""

    sysinfo_str = nettools.get_url_data_s("sysinfo.json", True)

    # confid = 0
    # conf_rows = dbtools.select( "SELECT confid FROM legacy.sheduledconference" )
    # if len(conf_rows) != 0:
    #    confid = conf_rows[0][0]

    if sysinfo_str == "":  # cannot connect to vtsrt to get 'sysinfo.json' and vtsrv is not restarting
        return (1)
    else:
        return (0)


#def is_abonent_registered_in_lyt(confid, abid):
#    """If abonentr 'abid' of conference 'confud' is registered (showed) in some layout of confernce, then return 1, else return 0."""
#    confid = int(confid)
#    abid = int(abid)
#
#    if abid <= 0:
#        return (0)
#
#    ab_rows = dbtools.select(
#        "SELECT abid FROM legacy.layoutconfigs WHERE abid=%d AND layout_id IN (SELECT id FROM legacy.layoutconferences WHERE confid=%d)" % (
#            abid, confid))
#    if len(ab_rows) != 0:
#        return (1)
#    else:
#        return (0)


def extract_conn_list(connstr):
    """This function extracts list of connections getted by http request 'http get /connections.json'
    Input params:
    connstr - string with 'http get' responce (see function get_url_data())
    Return value:
    list of dictionaries (each dictionary describes one connection)
    at error returns empty list"""
    connlist = []  # default connections list
    if connstr != "":
        true = True  # define constants "true"/"false"  (they is present in input expression "connstr")
        false = False
        conndict = {}
        try:
            conndict = eval(connstr)  # convert string to python expression (json format)
        except:
            logging.error("extract_conn_list(): exception " + str(sys.exc_info()))
            return ([])
        if "connection_list" not in conndict:
            return ([])
        connlist = conndict["connection_list"]  # extract connetions list from
    return connlist


def extract_conference(confstr, confid):
    """This function extracts conference 'confid' getted by http request 'http get /conferences.json'
    Input params:
    confstr - string with 'http get' responce (see function get_url_data())
    Return value:
    conference with id 'confid'
    at error returns empty dictionary"""
    conflist = []  # default conferences list
    if confstr == "":
        return ({})
    true = True  # define constants "true"/"false"  (they is present in input expression "connstr")
    false = False
    allconfdict = {}
    cid = tools.conference_name_by_id(confid)
    try:
        allconfdict = eval(confstr)  # convert string to python expression (json format)
    except:
        logging.error("extract_conference(): exception " + str(sys.exc_info()))
        return ({})
    if "conference_list" not in allconfdict:
        return ({})
    conflist = allconfdict["conference_list"]  # extract conferences list
    for conf in conflist:
        if conf["name"] == cid:  # select conference with specified name/id
            return (conf)
    return ({})


def extract_speaker_value(conf_json, speaker_uri):
    """This function extracts speaker type of all abonents of conference 'conf_json'.
    conf_json - conference (result of function extract_conference())
    speker_uri - uri of speaker
    Return value: 1 - if speaker is on, 0 - if speaker is off."""
    if speaker_uri == "":
        return (0)
    if "layout_list" not in conf_json:
        return (0)
    layout_list = conf_json["layout_list"]
    for layout in layout_list:
        for conn in layout["rx"]["connections"]:
            if conn["uri"] == speaker_uri and (conn["rendering"]["representation"] == "selected" or conn["rendering"][
                "representation"] == "exclusive"):
                return (1)
    return (0)


def is_translated_file(conn: dict) -> bool:
    """Check: is connection 'conn' presentation file?"""
    muted = ("muted" in conn["in"]["video"][1]) and conn["in"]["video"][1]["muted"] != 0
    width = conn["in"]["video"][1]["width"]
    height = conn["in"]["video"][1]["height"]
    vcodec = conn["in"]["video"][1]["format"]
    # vbitrate = conn["in"]["video"][1]["requested_bitrate"]
    if (not muted) and width != 0 and height != 0 and vcodec != "":
        return True
    else:
        return False


def is_translated_file2(conn_list: list, conn_id: int) -> bool:
    """find in 'conn_list' connection with 'conn_id' and check: is connection 'conn_id' presentation file?"""
    for conn in conn_list:
        cur_uri = conn["contact_uri"]
        cur_conn_id = conn["id"]
        if cur_conn_id == conn_id and tools.is_presentation_file(cur_uri) and is_translated_file(conn):
                return True
    return False


def extract_abonents_list(conf_json, use_translation, connstr):
    """This function extracts all abonents of conference 'conf_json'.
    conf_json - conference (result of function extract_conference())
    use_translation - flag (1 - add translations to result list, 0 - don't add)
    Return value: list of conferences. At error returns empty list."""
    conn_list = extract_conn_list(connstr)
    result = []
    if "layout_list" not in conf_json:
        return ([])
    layout_list = conf_json["layout_list"]
    for layout in layout_list:
        for conn in layout["tx"]["connections"]:
            cur_conn_id = conn["id"]
            if use_translation == 0 and is_translated_file2(conn_list, cur_conn_id):
                continue
            found = 0
            for testconn in result:
                if testconn["id"] == conn["id"]:
                    found = 1
                    break
            if not found:
                result.append(conn)
    return (result)


def get_presentation_uri(_conf_abonent_list, _connstr):
    """This function find presentation 'abonent' in list of abonents '_conf_abonent_list' of conference.
    NOTE: _conf_abonent_list = extract_abonents_list(conf_json, 1, constr) !!!
    If presentation of conference found, then return presentation filename, else return empty string."""
    conn_list = extract_conn_list(_connstr)
    for cur_ab in _conf_abonent_list:
        cur_uri = cur_ab["uri"]
        cur_conn_id = cur_ab["id"]
        if is_translated_file2(conn_list, cur_conn_id):
            return cur_uri

    return ""


def call_state_to_gui_state(call_state):
    """This function converts video engine's abonent state to internal code."""
    if call_state == "confirmed":
        return (data.AB_ST_ESTABLISHED)
    elif call_state == "setup":
        return (data.AB_ST_IN_PROGRESS)
    else:
        return (data.AB_ST_INACTIVE)


def conn_to_enable_video_status(conn):
    """This function returns 1 if video is enabled, else returns 0."""
    muted = ("muted" in conn["in"]["video"][0]) and conn["in"]["video"][0]["muted"] != 0
    if conn["in"]["video"][0]["width"] != 0 and conn["in"]["video"][0]["height"] != 0 and (not muted):
        return (1)
    else:
        return (0)


def conn_to_enable_audio_status(conn):
    """This function returns 1 if audio is enabled, else returns 0."""
    muted = ("muted" in conn["in"]["audio"]) and conn["in"]["audio"]["muted"] != 0
    if conn["in"]["audio"]["bitrate"] != 0 and (not muted):
        return (1)
    else:
        return (0)


def conn_to_audio_power(conn):
    """This function returns 1 if audio is enabled, else returns 0."""
    if "power_sB" in conn["in"]["audio"]:
        return conn["in"]["audio"]["power_sB"]
    else:
        return (0)


def conn_to_enable_audio_status_out(conn):
    """This function returns 1 if audio is enabled, else returns 0."""
    muted = ("muted" in conn["out"]["audio"]) and conn["out"]["audio"]["muted"] != 0
    if conn["out"]["audio"]["bitrate"] != 0 and (not muted):
        return (1)
    else:
        return (0)


def conn_to_enable_input_video(conn):
    """This function returns 1 if input video is enabled, else returns 0."""
    muted = ("muted" in conn["out"]["video"][0]) and conn["out"]["video"][0]["muted"] != 0
    if conn["out"]["video"][0]["width"] != 0 and conn["out"]["video"][0]["height"] != 0 and (not muted):
        return (1)
    else:
        return (0)


def conn_to_enable_input_audio(conn):
    """This function returns 1 if input audio is enabled, else returns 0."""
    muted = ("muted" in conn["out"]["audio"]) and conn["out"]["audio"]["muted"] != 0
    if conn["out"]["audio"]["bitrate"] != 0 and (not muted):
        return (1)
    else:
        return (0)


def is_audio_call(conn):
    if conn_to_enable_video_status(conn) or conn_to_enable_input_video(conn):
        return (0)
    elif conn_to_enable_audio_status(conn) or conn_to_enable_input_audio(conn):
        return (1)
    else:
        return (0)


def conn_to_str(conn, b_in):
    """Converts connection 'conn' to string  (list of connections was extracted by function extract_conn_list()).
    conn - connection
    b_in - flag: "out" for output stream, "in" for input stream"""

    if is_audio_call(conn):
        acodec = conn[b_in]["audio"]["codec"]
        abitrate = conn[b_in]["audio"]["bitrate"]
        lossrate_a = conn[b_in]["audio"]["lossrate"] / 10.0
        if b_in == "in":
            str = "Audio call" + "<br>\n"
            str += "Rx: %s, %dk, LR %.1f%%" % (acodec, abitrate, lossrate_a)
        else:
            str = "Tx: %s, %dk" % (acodec, abitrate)
        return (str)

    width = conn[b_in]["video"][0]["width"]
    height = conn[b_in]["video"][0]["height"]
    vcodec = conn[b_in]["video"][0]["format"]
    vbitrate = conn[b_in]["video"][0]["bitrate"]
    frame_rate = conn[b_in]["video"][0]["frame_rate"]
    lossrate_v = conn[b_in]["video"][0]["lossrate"] / 10.0
    acodec = conn[b_in]["audio"]["codec"]
    abitrate = conn[b_in]["audio"]["bitrate"]
    lossrate_a = conn[b_in]["audio"]["lossrate"] / 10.0
    if b_in == "in":
        str = "Rx: %dx%d, %s, %dk, FPS %d, LR %.1f%%, %s, %dk, LR %.1f%%" % (
        width, height, vcodec, vbitrate, frame_rate, lossrate_v, acodec, abitrate, lossrate_a)
    else:
        str = "Tx: %dx%d, %s, %dk, FPS %d, %s, %dk" % (width, height, vcodec, vbitrate, frame_rate, acodec, abitrate)
    return (str)


def is_second_chenel_work(conn, b_in):
    width = conn[b_in]["video"][1]["width"]
    height = conn[b_in]["video"][1]["height"]
    # vbitrate = conn[b_in]["video"][1]["bitrate"]
    vcodec = conn[b_in]["video"][1]["format"]
    muted = ("muted" in conn[b_in]["video"][1]) and conn[b_in]["video"][1]["muted"] != 0
    if width != 0 and height != 0 and vcodec != "" and (not muted):
        return (1)
    else:
        return (0)


def second_chanel_to_str(conn, b_in):
    """Converts second video chanel of connection 'conn' to string  (list of connections was extracted by function extract_conn_list()).
    conn - connection
    b_in - flag: "out" for output stream, "in" for input stream"""
    width = conn[b_in]["video"][1]["width"]
    height = conn[b_in]["video"][1]["height"]
    vcodec = conn[b_in]["video"][1]["format"]
    vbitrate = conn[b_in]["video"][1]["bitrate"]
    frame_rate = conn[b_in]["video"][1]["frame_rate"]
    lossrate_v = conn[b_in]["video"][1]["lossrate"] / 10.0
    if b_in == "in":
        str = "Rx: %dx%d, %s, %dk, FPS %d, LR %.1f%%" % (width, height, vcodec, vbitrate, frame_rate, lossrate_v)
    else:
        str = "Tx: %dx%d, %s, %dk, FPS %d, LR %.1f%%" % (width, height, vcodec, vbitrate, frame_rate, lossrate_v)
    return (str)


def is_abonent_send_presentation(conn):
    muted = ("muted" in conn["in"]["video"][1]) and conn["in"]["video"][1]["muted"] != 0
    width = conn["in"]["video"][1]["width"]
    height = conn["in"]["video"][1]["height"]
    vcodec = conn["in"]["video"][1]["format"]
    vbitrate = conn["in"]["video"][1]["requested_bitrate"]
    if (not muted) and width != 0 and height != 0 and vcodec != "" and vbitrate != 0:
        return True
    else:
        return False


def get_noise_suppressor(conn: dict) -> bool:
    if "noise_suppressor" in conn["in"]["audio"]:
        return conn["in"]["audio"]["noise_suppressor"]
    else:
        return False


def get_auto_gain_control(conn: dict) -> bool:
    if "agc" in conn["in"]["audio"]:
        return conn["in"]["audio"]["agc"]
    else:
        return False


def conn_to_html_status(conn):
    """This function converts connection 'conn' to status of connection."""
    rx_str = conn_to_str(conn, "in")
    tx_str = conn_to_str(conn, "out")
    result = "Common stream" + ":<br>\n" + rx_str + "<br>\n" + tx_str + "<br>\n"

    if is_second_chenel_work(conn, "in") or is_second_chenel_work(conn, "out"):
        rx2_str = second_chanel_to_str(conn, "in")
        tx2_str = second_chanel_to_str(conn, "out")
        result += "Content" + ":<br>\n" + rx2_str + "<br>\n" + tx2_str + "<br>\n"
    return (result)


def conn_to_dict_status(conn):
    """This function converts connection 'conn' to status of connection."""
    result = {"first": {"rx": None, "tx": None}, "second": {"rx": None, "tx": None}}

    rx_str = conn_to_str(conn, "in")
    tx_str = conn_to_str(conn, "out")
    result["first"]["rx"] = rx_str
    result["first"]["tx"] = tx_str

    if is_second_chenel_work(conn, "in") or is_second_chenel_work(conn, "out"):
        rx2_str = second_chanel_to_str(conn, "in")
        tx2_str = second_chanel_to_str(conn, "out")
        result["second"]["rx"] = rx2_str
        result["second"]["tx"] = tx2_str
    return (result)



def conn_to_detail_video(conn: dict, b_in: str, s_ind: int) -> dict:
    """Converts connection 'conn' to dictionary (list of connections was extracted by function extract_conn_list()).
    conn - connection
    b_in - flag: "out" for output stream, "in" for input stream
    s_ind - index of video stream: 0 - first stream, 1 - second stream (content)"""
    result = {
        "protocol": conn[b_in]["video"][s_ind]["format"],
        "requested_bitrate": conn[b_in]["video"][s_ind]["requested_bitrate"],
        "resolution": "%dx%d" % (conn[b_in]["video"][s_ind]["width"], conn[b_in]["video"][s_ind]["height"]),
        "frame_rate": conn[b_in]["video"][s_ind]["frame_rate"],
        "bitrate": conn[b_in]["video"][s_ind]["bitrate"],
        "packets": conn[b_in]["video"][s_ind]["packets"],
        "lost_packets": conn[b_in]["video"][s_ind]["lost_packets"],
        "lossrate": conn[b_in]["video"][s_ind]["lossrate"] / 10.0,
        "jitter": conn[b_in]["video"][s_ind]["jitter"]
    }
    return result


def conn_to_detail_audio(conn: dict, b_in: str) -> dict:
    """Converts connection 'conn' to dictionary (list of connections was extracted by function extract_conn_list()).
    conn - connection
    b_in - flag: "out" for output stream, "in" for input stream"""
    result = {
        "protocol": conn[b_in]["audio"]["codec"],
        "bitrate": conn[b_in]["audio"]["bitrate"],
        "packets": conn[b_in]["audio"]["packets"],
        "lost_packets": conn[b_in]["audio"]["lost_packets"],
        "lossrate": conn[b_in]["audio"]["lossrate"] / 10.0,
        "jitter": conn[b_in]["audio"]["jitter"]
    }
    return result


def conn_to_detail_status(conn) -> dict:
    """This function converts connection 'conn' to status of connection."""
    status = {
        "video":
            {
                "rx": conn_to_detail_video(conn, "in", 0),
                "tx": conn_to_detail_video(conn, "out", 0)
            },
        "audio":
            {
                "rx": conn_to_detail_audio(conn, "in"),
                "tx": conn_to_detail_audio(conn, "out")
            },
        "content":
            {
                "rx": conn_to_detail_video(conn, "in", 1),
                "tx": conn_to_detail_video(conn, "out", 1)
            }
    }
    return status


def conn_to_requested_full_bitrate(conn: dict) -> int:
    """Return full requested bitrate for connection 'conn'."""
    #OLD: return conn["out"]["video"][0]["requested_bitrate"] + conn["out"]["audio"]["bitrate"]
    return conn["bitrate"]


#def extract_layout_from_conference(_conf_json, _aburi):
#    """This function get layout of abonent _aburi in conference _conf_json"""
#    # get edited layout
#    if "layout_list" not in _conf_json:
#        return ({})
#    layout_json = {}
#    for lyt in _conf_json["layout_list"]:
#        if ("name" in lyt) and lyt["name"] == "PRESENTATION":
#            continue
#        for ab in lyt["tx"]["connections"]:
#            if tools.is_addr_equivalent(ab["uri"], _aburi):
#                return (lyt)
#    return ({})  # error


def extract_layout_by_conn_id(_conf_json, _conn_id):
    """This function get layout of abonent _conn_id in conference _conf_json"""
    # get edited layout
    if "layout_list" not in _conf_json:
        return ({})
    layout_json = {}
    for lyt in _conf_json["layout_list"]:
        if ("name" in lyt) and lyt["name"] == "PRESENTATION":
            continue
        for ab in lyt["tx"]["connections"]:
            if ab["id"] == _conn_id:
                return (lyt)
    return ({})  # error


def extract_layout_by_name(_conf_json, _lid):
    """This function get layout _lid in conference _conf_json"""
    # get edited layout
    if "layout_list" not in _conf_json:
        return ({})
    for lyt in _conf_json["layout_list"]:
        if lyt["name"] == _lid:
            return (lyt)
    return ({})  # error


#def get_layout_of_abonent(confid, ab_id):
#    """This function returns layout of abonent ab_id in conference confid (or global layout name by default)."""
#    confid = int(confid)
#    ab_id = int(ab_id)
#
#    default_layout_name = ""
#
#    # get default layout name (global layout name)
#    glb_lyt_id = layoutbackup.get_global_layout_id(confid)
#    if glb_lyt_id != 0:
#        default_layout_name = tools.get_layout_name_by_id(confid, glb_lyt_id)
#
#    ab_uri = addrbook.get_realaddr_by_ab_id(ab_id)
#
#    connstr, confstr = get_online_state()
#    conf_json = extract_conference(confstr, confid)
#    if conf_json == {}:
#        return default_layout_name
#
#    layout_json = extract_layout_from_conference(conf_json, ab_uri)
#    if layout_json == {} or ("name" not in layout_json):
#        return default_layout_name
#
#    return layout_json["name"]


#def get_aburi_from_layout_position(layout_json: dict, position: int) -> str:
#    """This function find abonent in 'position' of layout 'layout_json'.
#    NOTE: argument position = 1, 2, 3, ... but layoyt's position = 0, 1, 2, ..."""
#    if layout_json == {}:
#        return ""  # error
#    if "rx" not in layout_json:
#        return ""  # error
#    if "connections" not in layout_json["rx"]:
#        return ""  # error
#
#    for ab in layout_json["rx"]["connections"]:
#        if "position" not in ab["rendering"]:
#            continue
#        if position == ab["rendering"]["position"] + 1:
#            return ab["uri"]
#
#    return ""  # position is not found


def get_conn_id_from_layout_position(layout_json: dict, position: int) -> int:
    """This function find abonent in 'position' of layout 'layout_json'.
    NOTE: argument position = 1, 2, 3, ... but layoyt's position = 0, 1, 2, ..."""
    if layout_json == {}:
        return -1  # error
    if "rx" not in layout_json:
        return -1  # error
    if "connections" not in layout_json["rx"]:
        return -1  # error

    for ab in layout_json["rx"]["connections"]:
        if "position" not in ab["rendering"]:
            continue
        if position == ab["rendering"]["position"] + 1:
            return ab["id"]

    return -1  # position is not found


def get_engine_render_mode(_engine_render_mode_name: str) -> int:
    """This function convert engine render mode 'fill'/'full'/'auto' to render mode index (see data.RENDER_PARTICIPANT_...)"""
    if _engine_render_mode_name == "fill":
        return data.RENDER_PARTICIPANT_FILL
    elif _engine_render_mode_name == "full":
        return data.RENDER_PARTICIPANT_FULL
    elif _engine_render_mode_name == "auto":
        return data.RENDER_PARTICIPANT_AUTO
    else:
        return -1 # error


def get_render_mode_ind_from_map_pos(layout_json: dict, position: int) -> int:
    """This function get 'render mode' from layout slot in 'position'.
    ( 1 <= position <= Nslots ). Layout must have map."""
    if "map" not in layout_json:
        return -1 # error

    pos_ind = position - 1
    if pos_ind < 0 or pos_ind >= len(layout_json["map"]):
        return -1 # error

    peer_params = layout_json["map"][pos_ind]
    if "method" not in peer_params:
        return -1 # error

    render_mode_str = peer_params["method"]
    return get_engine_render_mode(render_mode_str)


def get_render_mode_of_connection(conn: dict) -> int:
    render_method_str = conn["in"]["video"][0]["render_method"]
    return get_engine_render_mode(render_method_str)


def extract_conf_list(confstr):
    """This function extracts conferences list getted by http request 'http get /conferences.json'
    Input params:
    confstr - string with 'http get' responce (see function get_url_data())
    Return value:
    conference with id 'confid'
    at error returns empty list"""
    conflist = []  # default conferences list
    if confstr == "":
        return ([])
    true = True  # define constants "true"/"false"  (they is present in input expression "connstr")
    false = False
    allconflist = {}
    try:
        allconflist = eval(confstr)  # convert string to python expression (json format)
    except:
        logging.error("extract_conf_list(): exception " + str(sys.exc_info()))
        return ([])
    if "conference_list" not in allconflist:
        return ([])
    else:
        return (allconflist["conference_list"])


def extract_all_active_abonents(conf_list):
    """This function extracts all active abonents (who send video to MCU) of all conferences.
    conf_list - list of conferences (result of function extract_conf_list())
    Return value: list of abonents. At error returns empty list."""
    result = []
    for conf in conf_list:
        if "layout_list" not in conf:
            continue
        for layout in conf["layout_list"]:
            for conn in layout["rx"]["connections"]:
                if conn["rendering"]["width"] == 0 or conn["rendering"]["height"] == 0:
                    continue
                found = 0
                for testconn in result:
                    if testconn["id"] == conn["id"]:
                        found = 1
                        break
                if not found:
                    result.append(conn)
    return (result)


def get_realaddr_by_conn_id(conn_id: int, conn_list: list):
    """This function find abonent uri by it's connection id."""

    if conn_list == None or conn_list == []:
        connstr, confstr = get_online_state()
        conn_list = extract_conn_list(connstr)

    for conn in conn_list:
        if conn["id"] == conn_id:
            return conn["contact_uri"]

    return ""


def get_conn_id_by_realaddr(aburi: str, conn_list: list):
    """This function find abonent uri by it's connection id."""

    if conn_list == None or conn_list == []:
        connstr, confstr = get_online_state()
        conn_list = extract_conn_list(connstr)

    for conn in conn_list:
        if conn["contact_uri"] == aburi:
            return conn["id"]

    return -1


def get_crown_layout(confid: int, conf_json: dict):
    """This function returns 'crown' layout of conference 'confid'."""

    if conf_json == {}:
        connstr, confstr = get_online_state()

        conf_json = extract_conference(confstr, confid)
        if conf_json == {}:
            return {}  # conference/session is not found

    # get crown layout
    crown_layout_name = tools.confid_to_crown_layout_name(confid)
    crown_layout_json = extract_layout_by_name(conf_json, crown_layout_name)
    if crown_layout_json == {}:
        return {}  # crown layout is not found

    return crown_layout_json


def get_crown_participant_from_layout(crown_layout_json: dict) -> int:
    """This function returns participant id of conference confid, who is in big window of crown layout. At error returns 0."""

    if crown_layout_json == {}:
        # logging.error("get_crown_participant() ERROR: crown layout is not found")
        return 0  # crown layout is not found

    # get big participant of layout (if exist)
    ab_conn_id = -1

    # OLD: if video of crown participant is disabled, then cannot find "rendering" field
    # for ab in crown_layout_json["rx"]["connections"]:
    #    if "rendering" not in ab:
    #        continue
    #    if "position" not in ab["rendering"]:
    #        continue
    #    #logging.error("get_crown_participant(): position: %d , uri: %s" % (ab["rendering"]["position"], ab["uri"]))
    #    if ab["rendering"]["position"] == 0: # big slot of crown layout
    #        ab_uri = ab["uri"]
    #        break

    # NEW:
    if "map" in crown_layout_json:
        if len(crown_layout_json["map"]) != 0:
            first_slot = crown_layout_json["map"][0]
            if "call_id" in first_slot:
                ab_conn_id = first_slot["call_id"]

    if ab_conn_id == -1:
        # logging.error("get_crown_participant() ERROR: crown participant is not found")
        return 0  # crown participant is not found

    abid = addrbook.get_ab_id_by_conn_id(ab_conn_id)
    return abid


def get_crown_participant(confid: int, conf_json: dict):
    """This function returns participant id of conference confid, who is in big window of crown layout. At error returns 0."""

    crown_layout_json = get_crown_layout(confid, conf_json)
    abid = get_crown_participant_from_layout(crown_layout_json)

    return abid


def get_show_names_cmd_list(confid: int, show_names: bool, cmd_list: list):
    """This function fill commands list to switch layout names of conference 'confid'."""

    connstr, confstr = get_online_state()

    conf_json = extract_conference(confstr, confid)
    if conf_json == {}:
        return # conference/session is not found

    if "layout_list" not in conf_json:
        return # no layouts in conference/session

    cid = tools.conference_name_by_id(confid)

    for layout in conf_json["layout_list"]:
        layout_name = layout["name"]
        if layout_name == "PRESENTATION" or layout_name == "DEFAULT":
            continue
        cmd = "LAYOUT DN %s %s %s" % (tools.num_to_onoff(show_names), cid, layout_name)
        cmd_list.append(cmd)


def get_show_audio_power_cmd_list(confid: int, show_audio_power: bool, cmd_list: list):
    """Update param 'show audio power' for all layouts of conference."""
    connstr, confstr = get_online_state()

    conf_json = extract_conference(confstr, confid)
    if conf_json == {}:
        return # conference/session is not found

    if "layout_list" not in conf_json:
        return # no layouts in conference/session

    cid = tools.conference_name_by_id(confid)

    for layout in conf_json["layout_list"]:
        layout_name = layout["name"]
        if layout_name == "PRESENTATION" or layout_name == "DEFAULT":
            continue
        cmd_list.append("LAYOUT LOUDNESS %s %s %s" % (tools.num_to_onoff(show_audio_power), cid, layout_name))


def get_layout_background_cmd_list(confid: int, background_ind: int, cmd_list: list):
    """Update param 'layout background' for all layouts of conference."""
    connstr, confstr = get_online_state()

    conf_json = extract_conference(confstr, confid)
    if conf_json == {}:
        return # conference/session is not found

    if "layout_list" not in conf_json:
        return # no layouts in conference/session

    cid = tools.conference_name_by_id(confid)

    background_base_cmd = data.get_background(background_ind)["cmd2"]

    for layout in conf_json["layout_list"]:
        layout_name = layout["name"]
        if layout_name == "PRESENTATION" or layout_name == "DEFAULT":
            continue
        cmd_list.append(background_base_cmd + " " + cid + " " + layout_name)


def get_render_fully_cmd_list(confid: int, render_fully: int, cmd_list: list):
    """Update param 'render fully' for all abonents of conference."""
    connstr, confstr = get_online_state()

    conf_json = extract_conference(confstr, confid)
    if conf_json == {}:
        return # conference/session is not found

    render_fully_name = data.get_render_participant_name(render_fully)

    conf_ab_list = extract_abonents_list(conf_json, 1, connstr)

    for ab in conf_ab_list:
        conn_id = ab["id"]
        conn_id_str = "#%d" % (conn_id)
        cmd_list.append("SET RENDER " + render_fully_name + " " + conn_id_str)


def get_resolution_cmd_list(confid: int, width: int, height: int, cmd_list: list):
    """Update resolution 'width x height' for all layouts of conference."""
    connstr, confstr = get_online_state()

    conf_json = extract_conference(confstr, confid)
    if conf_json == {}:
        return # conference/session is not found

    if "layout_list" not in conf_json:
        return # no layouts in conference/session

    cid = tools.conference_name_by_id(confid)

    for layout in conf_json["layout_list"]:
        layout_name = layout["name"]
        if layout_name == "PRESENTATION" or layout_name == "DEFAULT":
            continue
        cmd_list.append("LAYOUT FRAME SIZE " + cid + " " + layout_name + " " + str(width) + " " + str(height))


def get_abonent_display_name(_conf_list: list, _conn_id: int) -> str:
    """This function get from engine display name of abonent '_conn_id'."""
    if _conf_list == None or len(_conf_list) == 0:
        confstr = nettools.get_url_data_s("conferences.json", True)
        _conf_list = extract_conf_list(confstr)

    for conf in _conf_list:
        for layout in conf["layout_list"]:
            for conn in layout["tx"]["connections"]:
                if conn["id"] == _conn_id:
                    return conn["display_name"]

    return ""


def is_maximum_loading_ex(_max_visible: int, _conf_list: list):
    """This function checks loading of VCS-server. If loading is critical returns 1 else returns 0."""
    if _conf_list == None or len(_conf_list) == 0:
        confstr = nettools.get_url_data_s("conferences.json", True)
        _conf_list = extract_conf_list(confstr)

    ab_list = extract_all_active_abonents(_conf_list)

    if len(ab_list) >= _max_visible:
        logging.error("is_maximum_loading_ex(): visible abonents: %d" % (len(ab_list)))
        return (1)
    else:
        return (0)


def is_maximum_loading():
    max_ab = sysinfotools.get_abonents_restriction()
    confstr = nettools.get_url_data_s("conferences.json", True)
    conf_list = extract_conf_list(confstr)
    return is_maximum_loading_ex(max_ab["max_visible"], conf_list)


def is_max_abonents():
    "This function checks count of abonents connected to server. If it count is maximum, returns 1 else returns 0."
    connstr, confstr = get_online_state()
    conn_list = extract_conn_list(connstr)

    max_ab = sysinfotools.get_abonents_restriction()

    if len(conn_list) > max_ab["max_calls"]:
        logging.error("is_max_abonents(): connected abonents: %d" % (len(conn_list)))
        return (1)
    else:
        return (0)


def is_video_muted(conf_json, ab_conn_id):
    """This function returns flag 'video of abonent ab_uri muted' (no layout with abonents ab_uri in conference conf_json)."""
    muted = 1
    for lyt in conf_json["layout_list"]:
        for conn in lyt["rx"]["connections"]:
            if conn["id"] == ab_conn_id:
                muted = 0
                break

    return (muted)


def is_video_enabled(conf_json, ab_conn_id):
    vmuted = is_video_muted(conf_json, ab_conn_id)
    if vmuted != 0:
        return (0)
    else:
        return (1)


def get_active_abonents_json(confid, connstr, confstr, filter_abid, use_conn_id: bool, ab_names_dict: dict):
    """This function builds list of abonents connected to conference 'confid'."""
    confid = int(confid)

    # get global layout name
    glb_lyt_id = layoutbackup.get_global_layout_id(confid, False)
    glb_lid = tools.get_layout_name_by_id(confid, glb_lyt_id)

    # get 'dynamic' (real) list of abonents from VCS-server
    real_ab_list = extract_conn_list(connstr)

    # get 'dynamic' list of abonents in conference confid (abonents in this list may not be in database)
    conf_list = extract_conference(confstr, confid)

    if "active_speaker" in conf_list:
        speaker_uri = conf_list["active_speaker"]
    else:
        speaker_uri = ""
    speaker_val = tools.int_to_bool(extract_speaker_value(conf_list, speaker_uri))

    conf_conn_list = extract_abonents_list(conf_list, 1, connstr)

    if ab_names_dict == {}:
        ab_names_dict = addrbook.get_all_abonent_names_dict()

    crown_participant_id = get_crown_participant(confid, {})

    ab_list = []
    for cur_ab in conf_conn_list:
        cur_uri = cur_ab["uri"]
        cur_conn_id = cur_ab["id"]
        if is_translated_file2(real_ab_list, cur_conn_id): # skip files of second stream (show it in separated table)
            continue

        cur_contact_id = addrbook.get_ab_id_by_conn_id(cur_conn_id)
        if cur_contact_id == 0:
            logging.error("get_active_abonents_json(): abonent '%s' has zero contact id (disconnetced)" % (cur_uri))
            continue

        if filter_abid != None:
            if use_conn_id:
                if cur_conn_id != filter_abid:
                    continue
            else:
                if filter_abid != 0 and filter_abid != cur_contact_id:
                    continue

        for j in range(len(real_ab_list)):
            real_ab_uri = real_ab_list[j]["contact_uri"]
            if real_ab_list[j]["id"] == cur_conn_id:
                id = real_ab_uri
                contact_id = cur_contact_id

                if real_ab_list[j]["is_outgoing"] and (contact_id in ab_names_dict):
                    name = ab_names_dict[contact_id]["name"]
                    dispname = ab_names_dict[contact_id]["display_name"]
                else:
                    name = real_ab_list[j]["name"]
                    dispname = ""

                sipaddr = ""
                h323addr = ""
                extaddr = ""
                parsed_addr = tools.parse_address(real_ab_uri)
                if parsed_addr["protocol"] == "sip":
                    sipaddr = real_ab_uri
                if parsed_addr["protocol"] == "h323":
                    h323addr = real_ab_uri
                if parsed_addr["protocol"] == "rtsp":
                    extaddr = real_ab_uri

                online = call_state_to_gui_state(real_ab_list[j]["call_state"])

                status = conn_to_dict_status(real_ab_list[j])
                detail_status = conn_to_detail_status(real_ab_list[j])

                video_en = tools.int_to_bool(conn_to_enable_video_status(real_ab_list[j]))
                audio_en = tools.int_to_bool(conn_to_enable_audio_status(real_ab_list[j]))
                video_en_tx = tools.int_to_bool(conn_to_enable_input_video(real_ab_list[j]))
                audio_en_tx = tools.int_to_bool(conn_to_enable_audio_status_out(real_ab_list[j]))
                sound_vol_tx = int(real_ab_list[j]["out"]["audio"]["gain_sB"])
                audio_power = conn_to_audio_power(real_ab_list[j])

                audio_call = tools.int_to_bool(is_audio_call(real_ab_list[j]))

                if tools.is_addr_equivalent(real_ab_uri, speaker_uri):
                    speaker = speaker_val
                else:
                    speaker = False

                sound_vol = int(real_ab_list[j]["in"]["audio"]["gain_sB"])

                lyt = extract_layout_by_conn_id(conf_list, cur_conn_id)
                if ("name" in lyt) and lyt["name"] == glb_lid:
                    global_layout = True
                else:
                    global_layout = False
                lyt_reg = False

                b_has_crown = crown_participant_id != 0 and crown_participant_id == contact_id
                send_pres = is_abonent_send_presentation(real_ab_list[j])

                is_outgoing = real_ab_list[j]["is_outgoing"]
                full_bitrate = conn_to_requested_full_bitrate(real_ab_list[j])
                user_agent = real_ab_list[j]["user_agent"]
                connection_id = cur_conn_id
                render_mode = data.get_render_participant_name(get_render_mode_of_connection(real_ab_list[j]))
                noise_suppressor = get_noise_suppressor(real_ab_list[j])
                auto_gain_ctrl = get_auto_gain_control(real_ab_list[j])

                ab_list.append(
                    SessionParticipantEntity(id, contact_id, name, dispname, sipaddr, h323addr, extaddr, online,
                                             video_en,
                                             audio_en, speaker, sound_vol, audio_call, False, global_layout,
                                             lyt_reg, status, detail_status, True, real_ab_uri,
                                             video_en_tx, audio_en_tx, sound_vol_tx, audio_power, b_has_crown,
                                             send_pres, is_outgoing, full_bitrate, user_agent, connection_id,
                                             render_mode, noise_suppressor, auto_gain_ctrl)
                )
                break

    return ab_list


def get_connecting_participants(confid: int, connstr: str, ab_names_dict: dict) -> list:
    """This function returns list of connectiong participants"""

    # get 'dynamic' (real) list of abonents from VCS-server
    real_ab_list = extract_conn_list(connstr)

    if ab_names_dict == {}:
        ab_names_dict = addrbook.get_all_abonent_names_dict()

    ab_list = []
    ab_rows = dbtools.select(
        "SELECT abid, sipaddr, h323addr, extaddr FROM legacy.outgoingcalls WHERE confid=%d" % (confid))
    for ab in ab_rows:
        contact_id = ab[0]
        sipaddr = ab[1]
        h323addr = ab[2]
        extaddr = ab[3]

        found_sip = ""
        found_h323 = ""
        found_rtsp = ""
        real_ab_uri = ""
        j = 0
        while j < len(real_ab_list): # strict compare addresses
            real_ab_uri = real_ab_list[j]["contact_uri"]
            if real_ab_list[j]["call_state"] == "confirmed":
                j += 1
                continue
            if tools.is_addr_equivalent(real_ab_uri, sipaddr, True):
                found_sip = real_ab_uri
                break
            if tools.is_addr_equivalent(real_ab_uri, h323addr, True):
                found_h323 = real_ab_uri
                break
            if tools.is_addr_equivalent(real_ab_uri, extaddr, True):
                found_rtsp = real_ab_uri
                break
            j += 1
        if j >= len(real_ab_list): # if strict address is not found
            j = 0
            while j < len(real_ab_list): # not strict compare addresses
                real_ab_uri = real_ab_list[j]["contact_uri"]
                if real_ab_list[j]["call_state"] == "confirmed":
                    j += 1
                    continue
                if tools.is_addr_equivalent(real_ab_uri, sipaddr):
                    found_sip = real_ab_uri
                    break
                if tools.is_addr_equivalent(real_ab_uri, h323addr):
                    found_h323 = real_ab_uri
                    break
                if tools.is_addr_equivalent(real_ab_uri, extaddr):
                    found_rtsp = real_ab_uri
                    break
                j += 1
        if j >= len(real_ab_list): # if not strict address is not found
            continue

        #OLD: contact_id = addrbook.get_abonent_id(real_ab_uri)

        if real_ab_list[j]["is_outgoing"] and (contact_id in ab_names_dict):
            name = ab_names_dict[contact_id]["name"]
            dispname = ab_names_dict[contact_id]["display_name"]
        else:
            name = real_ab_list[j]["name"]
            dispname = ""

        online = call_state_to_gui_state(real_ab_list[j]["call_state"])
        if online == data.AB_ST_ESTABLISHED:
            continue

        status = conn_to_dict_status(real_ab_list[j])
        detail_status = conn_to_detail_status(real_ab_list[j])

        video_en = False
        audio_en = tools.int_to_bool(conn_to_enable_audio_status(real_ab_list[j]))
        video_en_tx = tools.int_to_bool(conn_to_enable_input_video(real_ab_list[j]))
        audio_en_tx = tools.int_to_bool(conn_to_enable_audio_status_out(real_ab_list[j]))
        sound_vol_tx = int(real_ab_list[j]["out"]["audio"]["gain_sB"])
        audio_power = conn_to_audio_power(real_ab_list[j])

        audio_call = tools.int_to_bool(is_audio_call(real_ab_list[j]))

        speaker = False

        sound_vol = int(real_ab_list[j]["in"]["audio"]["gain_sB"])

        global_layout = True
        lyt_reg = False

        is_outgoing = True
        full_bitrate = conn_to_requested_full_bitrate(real_ab_list[j])
        user_agent = real_ab_list[j]["user_agent"]
        connection_id = real_ab_list[j]["id"]
        render_mode = data.get_render_participant_name(get_render_mode_of_connection(real_ab_list[j]))
        noise_suppressor = get_noise_suppressor(real_ab_list[j])
        auto_gain_ctrl = get_auto_gain_control(real_ab_list[j])

        ab_list.append(
            SessionParticipantEntity(real_ab_uri, contact_id, name, dispname, found_sip, found_h323, found_rtsp, online,
                                     video_en, audio_en, speaker, sound_vol, audio_call, False, global_layout,
                                     lyt_reg, status, detail_status, True, real_ab_uri,
                                     video_en_tx, audio_en_tx, sound_vol_tx, audio_power, False, False,
                                     is_outgoing, full_bitrate, user_agent, connection_id, render_mode,
                                     noise_suppressor, auto_gain_ctrl)
        )

    return ab_list


def get_disconnected_abonents(_confid: int, _conn_ab_list: list, _ab_names_dict: dict) -> list:
    """This function return list of abonents is disconnected during session.
    _confid - conference id
    _conn_ab_list - list of SessionParticipantEntity getted by call get_active_abonents_json()
     _ab_names_dict - dictionary of abonents from address book"""

    if _ab_names_dict == {}:
        _ab_names_dict = addrbook.get_all_abonent_names_dict()

    # get last start time and state of conference _confid
    conf_rows = dbtools.select("SELECT last_start_time, state FROM legacy.conferences WHERE id=%d AND deleted_at is null" %
                               (_confid))
    if len(conf_rows) == 0:
        logging.error("get_disconnected_abonents() conference %d is not found" % (_confid))
        return []
    last_start_time = str(conf_rows[0][0])
    conf_state = conf_rows[0][1]
    if conf_state != data.CONF_STATE_ACTIVE:
        logging.error("get_disconnected_abonents() conference %d is stopped" % (_confid))
        return []

    # get history of calls of current conference
    disconnected_ab_list = []
    ab_rows = dbtools.select("""SELECT DISTINCT connections.abonent_id, connections.real_uri, abonents.autoadded 
    FROM legacy.connections, legacy.abonents 
    WHERE connections.abonent_id=abonents.id AND connections.conference_id=%d AND 
    connections.stop_at is not null AND connections.stop_at>'%s'""" % (_confid, last_start_time))
    for ab in ab_rows:
        contact_id = ab[0]
        real_uri = ab[1]
        autoadded = tools.int_to_bool(ab[2])

        if contact_id in _ab_names_dict:
            name = _ab_names_dict[contact_id]["name"]
            dispname = _ab_names_dict[contact_id]["display_name"]
        else:
            name = ""
            dispname = ""

        # find abonent in current connected abonents
        found = False
        for conn_ab in _conn_ab_list:
            if conn_ab.contact_id == contact_id:
                found = True
                break
        if found: # abonent is connected again
            continue # skip it

        disconnected_ab_list.append(DisconnectedSessionParticipantEntity(contact_id,name,dispname,real_uri,autoadded))

    return disconnected_ab_list


def get_free_abonents_list(_connstr, _confstr):
    """This function returns list of abonents not added to conferences."""

    # get 'dynamic' (real) list of abonents from VCS-server
    real_ab_list = extract_conn_list(_connstr)

    # get 'dynamic' list of conferences
    conf_list = extract_conf_list(_confstr)

    # build list of abonents no in conferences
    ab_list = []
    for ab in real_ab_list:
        if ab["is_outgoing"]:  # skip outgoings calls
            continue
        ab_uri = ab["contact_uri"]
        ab_conn_id = ab["id"]
        conffound = 0
        for conf in conf_list:
            layfound = 0
            for layout in conf["layout_list"]:
                txfound = 0
                for conn in layout["tx"]["connections"]:
                    if conn["id"] == ab_conn_id:
                        txfound = 1
                        break
                if txfound:
                    layfound = 1
                    break
                rxfound = 0
                for conn in layout["rx"]["connections"]:
                    if conn["id"] == ab_conn_id:
                        rxfound = 1
                        break
                if rxfound:
                    layfound = 1
                    break
            if layfound:
                conffound = 1
                break
        if conffound == 0:
            ab_list.append(ab)

    return (ab_list)


def get_waiting_abonents_json():
    # get online state from buffer (in database)
    connstr, confstr = get_online_state()

    free_ab_list = get_free_abonents_list(connstr, confstr)

    wait_ab_list = []
    for ab in free_ab_list:
        status = conn_to_dict_status(ab)
        wait_ab_list.append(WaitAbonentEntity(ab["contact_uri"], status, ab["id"], ab["name"]))

    return wait_ab_list


def is_call_outgoing(conn_list: list, aburi: str) -> bool:
    for connection in conn_list:
        if connection["contact_uri"] == aburi:
            b_outgoung = connection["is_outgoing"]
            return b_outgoung
    return True


def is_conn_id_outgoing(conn_list: list, conn_id: int) -> bool:
    for connection in conn_list:
        if connection["id"] == conn_id:
            b_outgoung = connection["is_outgoing"]
            return b_outgoung
    return True


def get_all_conf_abonents_list(_connlist, _confstr):
    """This function returns list of all abonents of all conferences."""

    conn_list = extract_conn_list(_connlist)

    # get 'dynamic' list of conferences
    conf_list = extract_conf_list(_confstr)

    ab_names_dict = addrbook.get_all_abonent_names_dict()

    # build list of abonents no in conferences
    ab_list = []
    for conf in conf_list:
        cid = conf["name"]

        conf_id = 0
        prefix = "conference"
        prefix_len = len(prefix)
        if len(cid) > prefix_len:
            conf_id = int(cid[prefix_len:])

        for layout in conf["layout_list"]:
            for conn in layout["tx"]["connections"]:
                cur_ab_uri = conn["uri"]
                cur_name = conn["display_name"]
                cur_conn_id = conn["id"]

                # skip files of second stream (show it in separated table)
                if is_translated_file2(conn_list, cur_conn_id):
                    continue

                ab_found = False
                for ab in ab_list:
                    if ab.conn_id == cur_conn_id:
                        ab_found = True
                        break
                if not ab_found:
                    ab_id = addrbook.get_ab_id_by_conn_id(cur_conn_id)

                    b_outgoung = is_conn_id_outgoing(conn_list, cur_conn_id)
                    if b_outgoung and (ab_id in ab_names_dict):
                        cur_name = ab_names_dict[ab_id]["name"]

                    ab_list.append(ConfSessionParticipantEntity(conf_id, ab_id, cur_ab_uri, cur_name, cur_conn_id))

    return ab_list


def get_preview_state() -> GetPreviewStateResponse:
    """This function returns state of preview ("off","rx","tx")."""
    preview_state = "off"

    connstr, confstr = get_online_state()

    conn_list = extract_conn_list(connstr)
    for conn in conn_list:
        video_list = conn["in"]["video"]
        found_in_video = False
        for v in video_list:
            if "preview" in v:
                if v["preview"]:
                    preview_state = "rx"
                    found_in_video = True
                    break
        if found_in_video:
            break

    conf_list = extract_conf_list(confstr)
    for conf in conf_list:
        found = False
        for layout in conf["layout_list"]:
            if "preview" in layout:
                if layout["preview"]:
                    preview_state = "tx"
                    found = True
                    break
        if found:
            break

    return GetPreviewStateResponse(preview_state)

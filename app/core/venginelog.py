#!/usr/bin/python3

import logging

import app.core.constants as constants
import app.core.data as data
import app.core.nettools as nettools
from app.core.requests import ExecuteVEngineCommandRequest
from app.core.requests import GetVEngineLogRequest
from app.core.responses import GetVEngineLogResponse
from app.core.responses import VEngineCommandResponse


def get_vengine_log(request: GetVEngineLogRequest) -> GetVEngineLogResponse:
    """This function returns video engine log contains records with id >= request.id and timestamp >= request.timestamp"""

    log_json = {}

    if request.id != None:
        if request.timestamp != None:
            url_str = "log.json?timestamp=%d&id=%d" % (request.timestamp, request.id)
        else:
            url_str = "log.json?id=%d" % (request.id)
    else:
        if request.timestamp != None:
            url_str = "log.json?timestamp=%d" % (request.timestamp)
        else:
            url_str = "log.json"

    logging.error("get_vengine_log(): GET " + url_str)

    log_str = nettools.get_url_data_s(url_str, True)
    if log_str != "":
        true = True  # define constants "true"/"false"
        false = False
        try:
            log_json = eval(log_str)  # convert string to python expression (json format)
        except:
            return GetVEngineLogResponse({})

    for i in range(len(log_json["log"])):
        log_json["log"][i]["class"] = data.get_log_level_str(log_json["log"][i]["class"])

    return GetVEngineLogResponse(log_json)


def execute_vengine_command(request: ExecuteVEngineCommandRequest) -> VEngineCommandResponse:
    cmd_list = [request.command]
    nettools.exec_cmd_list_s(cmd_list)
    return VEngineCommandResponse(request.command)

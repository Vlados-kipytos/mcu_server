#!/usr/bin/python3

import logging
import sys
import xml.etree.ElementTree as eltree

import certifi
import urllib3

import app.core.cert_functions as cert_func
import app.core.tools as tools

"""
Parameters:
    scripts_params_file - file with URL's for requests
    cert_file           - file for save getting certificate [IN, OUT]
    private_key_file    - file with private key 
"""


def update_cert(scripts_params_file, cert_file, private_key_file, ca_cert_fname):
    logging.debug("update_cert(): try to get URL from scripts params file %s" % (scripts_params_file))

    # load data for this function
    try:
        tree = eltree.parse(scripts_params_file)
        root = tree.getroot()

        # url for POST-request
        url_for_req = root.find("url_upd").text
    except IOError:
        cert_func.log_msg("update_cert() IOError at reading URL:" + str(sys.exc_info()))
        return cert_func.build_error_dict(cert_func.NO_SCRIPT_PARAMS)
    except Exception:
        cert_func.log_msg("update_cert() Exception at reading URL:" + str(sys.exc_info()))
        return cert_func.build_error_dict(cert_func.ERROR_SCRIPT_PARAMS)

    logging.debug("update_cert(): create tmp CA certificate file")

    if not tools.save_text_to_file(ca_cert_fname, cert_func.CA_CRT):
        cert_func.log_msg("update_cert(): cannot save CA certificate to file")
        return cert_func.build_error_dict(cert_func.ERROR_CREATE_CA_CERT_FILE)

    logging.debug("update_cert(): try to update certificate (make request to URL=%s)" % (url_for_req))

    # POST-request to server
    try:
        http = urllib3.PoolManager(cert_file=cert_file,
                                   cert_reqs='CERT_REQUIRED',
                                   key_file=private_key_file,
                                   key_password=b"APM1JDVgT8WDGOWBgQv6EIhvxl4vDYvU",
                                   ca_certs = ca_cert_fname)
                                   #OLD:ca_certs=certifi.where())

        r = http.request('POST', url_for_req, fields={})

        if r.status != 200:
            cert_func.log_msg("update_cert() bad httd ansver code: " + str(r.status))
            err_txt = r.data.decode("utf-8")
            cert_func.log_msg("update_cert() ansver: " + str(err_txt))
            tools.delete_file(ca_cert_fname)  # delete tmp file
            return cert_func.build_error_dict_ex(cert_func.UPD_CERT_HTTP_ERROR, err_txt)

    except Exception:
        cert_func.log_msg("update_cert() error at connection:" + str(sys.exc_info()))
        tools.delete_file(ca_cert_fname)  # delete tmp file
        return cert_func.build_error_dict(cert_func.ERROR_CONNECTION)

    tools.delete_file(ca_cert_fname)  # delete tmp file

    logging.debug("update_cert(): save updated certificate to file %s" % (cert_file))

    # handle response
    try:
        with open(cert_file, "wb") as f:
            f.write(r.data)
    except:
        cert_func.log_msg("update_cert() error at getting certificate and write it to file:" + str(sys.exc_info()))
        return cert_func.build_error_dict(cert_func.ERROR_SAVE_CERT)

    return cert_func.build_error_dict(cert_func.SUCCESS)

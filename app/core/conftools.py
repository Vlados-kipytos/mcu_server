#!/usr/bin/python3

import logging
import sys
import time
import calendar

import app.core.constants as constants
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.features as features
import app.core.layoutcfgtools as layoutcfgtools
import app.core.licenses as licenses
import app.core.onlinelayouttools as onlinelayouttools
import app.core.pipetools as pipetools
import app.core.sysinfotools as sysinfotools
import app.core.tools as tools
from .entities import AudioCodecsEntity
from .entities import AutoStartConferenceModeEntity
from .entities import AutoStopConferenceEntity
from .entities import RepeateConferenceModeEntity
from .entities import StopTimeTableEntity
from .entities import ConferenceEntity
from .entities import StringRecordEntity
from .entities import TimeRangeEntity
from .entities import VideoCodecsEntity
from .errors import BadConferenceStatusError
from .errors import BadEndIndexError
from .errors import BadPageSizeError
from .errors import BadStartIndexError
from .errors import ConferenceAlreadyLaunchedError
from .errors import ConferenceAlreadyStoppedError
from .errors import ConferenceLockedError
from .errors import ConferenceNotFoundError
from .errors import FeatureRestrictedByLicenseError
from .errors import SwitchConferenceStateError
from .errors import TooManyLayoutsError
from .requests import GetConferencesListRequest
from .responses import AllResolutionsListResponse


def get_conf_bitrate(confid):
    """This function returns bitrate of conference"""
    confid = int(confid)

    config_rows = dbtools.load_table("legacy.configurations", "bitrate",
                                     "id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (confid))
    if len(config_rows) != 0:
        bitrate = config_rows[0][0]
    else:
        bitrate = 1600  # default value

    return (bitrate)


def get_conf_time_ranges(confid: int) -> list:
    """Return list of entities TimeRangeEntity for specified conference confid."""
    time_range_list = []

    time_range_rows = dbtools.select("""SELECT start_time, stop_time FROM legacy.confautostart WHERE conference_id=%d"""
                                     % (confid))
    for time_range in time_range_rows:
        start_time = tools.db_date_time_to_json(str(time_range[0]))
        end_time = tools.db_date_time_to_json(str(time_range[1]))
        time_range_list.append( TimeRangeEntity(start_time, end_time) )

    return time_range_list


def get_all_time_ranges() -> dict:
    """Return dict[confid] = [ TimeRangeEntity(), TimeRangeEntity(), ... ] """
    time_range_dict = {}

    time_range_rows = dbtools.select("""SELECT conference_id, start_time, stop_time FROM legacy.confautostart 
    WHERE conference_id IN (SELECT id FROM legacy.conferences WHERE deleted_at is null)""")
    for time_range in time_range_rows:
        confid = time_range[0]
        start_time = tools.db_date_time_to_json(str(time_range[1]))
        end_time = tools.db_date_time_to_json(str(time_range[2]))
        if confid not in time_range_dict:
            time_range_dict[confid] = []
        time_range_dict[confid].append( TimeRangeEntity(start_time, end_time) )

    return time_range_dict


def add_time_ranges_to_conference(confid: int, time_range_list: list) -> bool:
    if len(time_range_list) == 0:
        return True

    sql_request = "INSERT INTO legacy.confautostart (conference_id, start_time, stop_time) VALUES "

    time_range_list_str = ""
    for i in range(len(time_range_list)):
        start_time = tools.json_date_time_to_db(time_range_list[i].start_time)
        end_time = tools.json_date_time_to_db(time_range_list[i].end_time)
        record_str = "(%d, '%s', '%s')" % (confid, start_time, end_time)
        time_range_list_str += record_str
        if i != len(time_range_list) - 1:
            time_range_list_str += ","

    sql_request += time_range_list_str
    res = dbtools.make_sql_request(sql_request)
    if res == 0:
        logging.error("add_time_ranges_to_conference(): db error " + dbtools.get_last_error())
        return False

    return True


def delete_time_ranges_from_conference(confid: int) -> bool:
    res = dbtools.make_sql_request("DELETE FROM legacy.confautostart WHERE conference_id=%d" % (confid))
    if res == 0:
        logging.error("delete_time_ranges_from_conference(): db error " + dbtools.get_last_error())
        return False

    return True


def get_conferences(request: GetConferencesListRequest, confid: int, confstr: str):
    """This function returns list of conferences or list with one conference confid (if confid specified).
    Returns conference list and count of conferences, getted by filter."""

    cur_time_sec = time.time()

    page = request.page
    per_page = request.per_page
    db_sort_field = request.get_db_sort_field()
    order_by = request.order_by
    filter_name = request.name
    filter_state = request.state
    filter_recording = request.recording

    if per_page == 0:
        raise BadPageSizeError(per_page, "per_page")

    # get online state from buffer (in database)
    if confstr == None or confstr == "":
        online_data_rows = dbtools.load_table("legacy.onlinedata", "conferences", "")
        if len(online_data_rows) != 0:
            confstr = online_data_rows[0][0]
        else:
            confstr = '{ "conference_list": [] }'

    time_range_dict = get_all_time_ranges()

    one_conf_only = ""
    if confid != None and confid != 0:
        one_conf_only = " AND conferences.id=%d " % (confid)
    else:
        one_conf_only = " AND deleted_at is null "

    # get conferences and its configurations
    conf_rows = dbtools.select("""SELECT conferences.id, conferences.name, conferences.starttime, conferences.endtime, 
        conferences.maxab, conferences.infinity, conferences.pin, conferences.usepin, conferences.autocall, 
        conferences.rtmp_ab_id, conferences.rtmp_url, conferences.state,
        configurations.aac, configurations.g711, configurations.g726, configurations.g728, configurations.g722, 
        configurations.g7221_24, configurations.g7221_32, configurations.g7221c_24, configurations.g7221c_32, 
        configurations.g7221c_48, configurations.g729, configurations.g7231, configurations.g719, configurations.muteaudio,
        configurations.noise_suppressor, configurations.auto_gain_ctrl,
        configurations.bitrate, configurations.h261, configurations.h263, configurations.h264, configurations.h264high, 
        configurations.h265, configurations.width, 
        configurations.height, configurations.fps, configurations.render_fully, configurations.rec_at_start, 
        configurations.rec_autostop, configurations.show_aud_pwr, configurations.show_names, configurations.rec_to_file,
        configurations.use_pres, configurations.background, configurations.mutevideo, configurations.aspeaker, 
        conferences.last_start_time,
        conferences.start_mode, conferences.start_week_day, conferences.start_month_day, 
        conferences.start_wk_first, conferences.start_wk_last,
        conferences.repeate_mode, conferences.repeate_count, conferences.repeate_last_time,
        conferences.stop_time_table_mode, conferences.stop_time_table_begin, conferences.stop_time_table_end,
        conferences.stop_mode, conferences.stop_duration, conferences.stop_abs_time
        FROM legacy.conferences, legacy.configurations WHERE conferences.configid=configurations.id %s ORDER BY conferences.%s %s""" %
                               (one_conf_only, db_sort_field, order_by))

    if confid != None and confid != 0 and len(conf_rows) == 0:
        raise ConferenceNotFoundError(confid, "conference_id")

    conf_list = []
    for conf in conf_rows:
        id = conf[0]
        confname = conf[1].strip()
        #start_dt = tools.db_date_time_to_json(str(conf[2]))
        #end_dt = tools.db_date_time_to_json(str(conf[3]))
        maxab = conf[4]
        b_infinity = tools.int_to_bool(conf[5])
        pin = conf[6].strip()
        b_usepin = tools.int_to_bool(conf[7])
        b_autocall = tools.int_to_bool(conf[8])
        rtmp_ab_id = conf[9]
        rtmp_url = conf[10]
        state = conf[11]
        if state >= 0 and state < len(data.CONF_STATE_NAMES):
            state_str = data.CONF_STATE_NAMES[state]["name"]
        else:
            state_str = "unknown"  # error

        acodec_list = []
        if conf[12]:
            acodec_list.append("aac")
        if conf[13]:
            acodec_list.append("g711")
        if conf[14]:
            acodec_list.append("g726")
        if conf[15]:
            acodec_list.append("g728")
        if conf[16]:
            acodec_list.append("g722")
        if conf[17]:
            acodec_list.append("g722_1_24")
        if conf[18]:
            acodec_list.append("g722_1_32")
        if conf[19]:
            acodec_list.append("g722_1c_24")
        if conf[20]:
            acodec_list.append("g722_1c_32")
        if conf[21]:
            acodec_list.append("g722_1c_48")
        if conf[22]:
            acodec_list.append("g729")
        if conf[23]:
            acodec_list.append("g723_1")
        if conf[24]:
            acodec_list.append("g719")
        muteaudio = tools.int_to_bool(conf[25])
        noise_suppressor  = data.get_noise_suppressor_name(conf[26])
        auto_gain_ctrl = data.get_auto_gain_ctrl_name(conf[27])
        audio_cfg = {"allowed_codecs": acodec_list, "muteaudio": muteaudio, "noise_suppressor": noise_suppressor,
                     "auto_gain_ctrl": auto_gain_ctrl}

        bitrate = conf[28]
        vcodecs_list = []
        if conf[29]:
            vcodecs_list.append("h261")
        if conf[30]:
            vcodecs_list.append("h263")
        if conf[31]:
            vcodecs_list.append("h264")
        if conf[32]:
            vcodecs_list.append("h264_high")
        if conf[33]:
            vcodecs_list.append("h265")
        width = conf[34]
        height = conf[35]
        fps = conf[36]
        resolution = data.find_resolution_index(width, height, fps)
        render_fully = data.get_render_participant_name(conf[37])
        rec_at_start = tools.int_to_bool(conf[38])
        rec_autostop = tools.int_to_bool(conf[39])
        show_aud_pwr = tools.int_to_bool(conf[40])
        show_names = tools.int_to_bool(conf[41])
        rec_to_file = tools.int_to_bool(conf[42])
        use_pres = tools.int_to_bool(conf[43])
        background_name = data.get_background(conf[44])["name"]
        mutevideo = tools.int_to_bool(conf[45])

        video_cfg = {"max_bitrate": bitrate, "allowed_codecs": vcodecs_list, "max_resolution": resolution,
                     "show_full_participant": render_fully, "rec_at_start": rec_at_start, "rec_autostop": rec_autostop,
                     "show_aud_pwr": show_aud_pwr, "show_names": show_names, "rec_to_file": rec_to_file,
                     "use_pres": use_pres, "background": background_name, "mutevideo": mutevideo}

        aspeaker_num = conf[46]
        last_start_time = tools.db_date_time_to_json(str(conf[47]))

        if state == data.CONF_STATE_ACTIVE:
            duration = cur_time_sec - last_start_time
        else:
            duration = 0

        conf_json = tools.extract_conference(confstr, id)
        if "recording" in conf_json:
            rec_flag = True
        else:
            rec_flag = False

        layout_mode = layoutcfgtools.aspeaker_to_layout_mode(aspeaker_num)

        auto_start_mode = data.get_auto_start_mode(conf[48])
        auto_start_week_day = conf[49]
        auto_start_month_day = conf[50]
        auto_start_wk_first = conf[51]
        auto_start_wk_last = conf[52]
        time_range_list = []
        if id in time_range_dict:
            time_range_list = time_range_dict[id]
        auto_start = AutoStartConferenceModeEntity(time_range_list, auto_start_mode,
                                                   auto_start_week_day, auto_start_month_day, auto_start_wk_first,
                                                   auto_start_wk_last)

        repeate_mode = data.get_repeate_mode(conf[53])
        repeate_count = conf[54]
        repeate_last_time = tools.db_date_time_to_json(str(conf[55]))
        repeate = RepeateConferenceModeEntity(repeate_mode, repeate_count, repeate_last_time)

        stop_time_table_mode = data.get_stop_time_table(conf[56])
        stop_time_table_begin = tools.db_date_time_to_json(str(conf[57]))
        stop_time_table_end = tools.db_date_time_to_json(str(conf[58]))
        stop_time_table = StopTimeTableEntity(stop_time_table_mode, stop_time_table_begin, stop_time_table_end)

        stop_mode = data.get_auto_stop_mode(conf[59])
        stop_duration = conf[60]
        stop_abs_time = tools.db_date_time_to_json(str(conf[61]))
        auto_stop = AutoStopConferenceEntity(stop_mode, stop_duration, stop_abs_time)

        conf_obj = ConferenceEntity(id, confname, b_infinity, last_start_time,
                                    auto_start, repeate, stop_time_table, auto_stop,
                                    duration, "", maxab,
                                    b_usepin, pin, b_autocall, rtmp_url, rtmp_ab_id, state_str, rec_flag, layout_mode,
                                    0, audio_cfg, video_cfg)
        conf_list.append(conf_obj)

    one_conf_only2 = ""
    if confid != None and confid != 0:
        one_conf_only2 = " WHERE confid=%d " % (confid)

    # get participants count dictionary
    partic_cnt_dict = {}
    partic_cnt_rows = dbtools.select(
        "SELECT DISTINCT confid, count(confid) FROM legacy.confabonents %s GROUP BY confid" % (one_conf_only2))
    for partic_cnt in partic_cnt_rows:
        conf_id = partic_cnt[0]
        cnt = partic_cnt[1]
        partic_cnt_dict[conf_id] = cnt

    # set participants count to all conferences
    for conf in conf_list:
        if conf.confid in partic_cnt_dict:
            conf.participants_cnt = partic_cnt_dict[conf.confid]
        else:
            conf.participants_cnt = 0

    # filter list of conferences
    filtered_list = []
    for conf in conf_list:
        if filter_name != None and filter_name != "" and (not tools.is_param_match(conf.name, filter_name)):
            continue
        if filter_state != None and filter_state != "" and conf.state != filter_state:
            continue
        if filter_recording != None and conf.is_recording != filter_recording:
            continue
        filtered_list.append(conf)

    conf_cnt = len(filtered_list)
    if conf_cnt == 0:
        return [], 0

    start_index = page * per_page
    if start_index < 0 or start_index >= conf_cnt:
        raise BadStartIndexError(start_index, "page")

    end_index = start_index + per_page - 1
    if end_index < 0:
        raise BadEndIndexError(end_index, "page")
    if end_index >= conf_cnt:
        end_index = conf_cnt - 1

    # get selected page of conferences
    out_list = []
    index = start_index
    while index <= end_index:
        out_list.append(filtered_list[index])
        index += 1

    return out_list, conf_cnt


def get_cur_conf_participants(_confid: int) -> int:
    partic_cnt_rows = dbtools.select("SELECT count(confid) FROM legacy.confabonents WHERE confid=%d" % (_confid))
    if len(partic_cnt_rows) != 0:
        return partic_cnt_rows[0][0]
    else:
        return 0


def get_max_conf_participants(_confid: int) -> int:
    max_participants_rows = dbtools.select("SELECT maxab FROM legacy.conferences WHERE id=%d" % (_confid))
    if len(max_participants_rows) != 0:
        return max_participants_rows[0][0]
    else:
        return 0


def can_add_participants(_confid: int) -> bool:
    """If you can add to conferece planned participant then return True."""
    partic_cnt = get_cur_conf_participants(_confid)
    max_participants = get_max_conf_participants(_confid)

    if partic_cnt < max_participants:
        return True
    else:
        return False


def is_pin_exist(_pin_code: str, _conf_id: int) -> bool:
    pin_rows = dbtools.select("SELECT pin FROM legacy.conferences WHERE pin='%s' and id<>%d and deleted_at is null" %
                              (_pin_code, _conf_id))
    if len(pin_rows) != 0:
        return True
    else:
        return False


def get_presentation_cmd(confid: int, use_pres: bool, cmd_list: list):
    cid = tools.conference_name_by_id(confid)
    if use_pres:
        cmd_list.append("PRESENTATION CHAIRCONTROL ACCEPT " + cid)
    else:
        cmd_list.append("PRESENTATION CHAIRCONTROL REJECT " + cid)


def send_restart(_width, _height, _fps, _bitr_in):
    _width = int(_width)
    _height = int(_height)
    _fps = int(_fps)
    _bitr_in = int(_bitr_in)

    # restart VCS-server
    if sys.platform != "win32":
        res_index = data.find_resolution_index(_width, _height, _fps)

        if res_index >= 0 and res_index < len(data.RESOLUTIONS_LIST):
            disable_str = data.get_disable_resol_str(res_index)
        else:
            disable_str = ""

        vsize = "-s %sx%s -r %s" % (_width, _height, _fps)

        if len(disable_str) != 0:
            vsize += " " + disable_str

        # TODO: set language of vtsrv engine
        # lang = strings.get_current_language()
        # if len(lang) != 0:
        #    vsize += " --language %s" % (lang)

        pipetools.send_msg("RESTART " + vsize)


def send_restart2(_confid):
    _confid = int(_confid)
    frm = tools.get_frame_params(_confid)

    bitr_in_rows = dbtools.select("SELECT bitr_in FROM legacy.commonconfig")
    if len(bitr_in_rows) != 0:
        bitr_in = bitr_in_rows[0][0]
    else:
        bitr_in = 1600

    send_restart(frm["width"], frm["height"], frm["fps"], bitr_in)


def get_conference_status(confid):
    """This function returns status of conference.
    Returns flags:
    locked - If False, you can manipulate conference. If True, manipulations disabled.
    launched - If True, you can stop conferece. If False, you can start conference."""

    confid = int(confid)

    result = {"locked": True, "launched": False, "error": ""}

    # get conference name and state
    conf_rows = dbtools.load_table("legacy.conferences", "state, starttime, endtime, infinity", "id=%d" % (confid))
    if len(conf_rows) == 0:
        result["error"] = "cannot get conference from db"
        return (result)
    conf_state = conf_rows[0][0]
    starttime = conf_rows[0][1]
    endtime = conf_rows[0][2]
    infinity = conf_rows[0][3]

    startsec = tools.db_date_time_to_json(str(starttime))
    endsec = tools.db_date_time_to_json(str(endtime))

    curtime = int(time.time())

    # if conference started by timer and out of time range, set flag "lock 'start/stop' button"
    if infinity == 0 and (curtime < startsec or curtime > endsec):
        timer_conf_locked = 1
    else:
        timer_conf_locked = 0

    if conf_state == data.CONF_STATE_START or conf_state == data.CONF_STATE_CLOSING_VILE or timer_conf_locked != 0:
        locked = True
    else:
        locked = False

    if conf_state == data.CONF_STATE_PRE_INACTIVE or conf_state == data.CONF_STATE_POST_INACTIVE:
        launched = False
    else:
        launched = True

    result["locked"] = locked
    result["launched"] = launched
    return (result)


def is_conf_deleted(confid: int) -> bool:
    # check existing of conference
    conf_rows = dbtools.select("SELECT id FROM legacy.conferences WHERE id=%d AND deleted_at is not null" % (confid))
    if len(conf_rows) != 0:
        return True
    else:
        return False


def start_conference(confid):
    confid = int(confid)

    logging.error("start_conference(): START CONFERENCE")

    if is_conf_deleted(confid):
        raise ConferenceNotFoundError(confid, "conference_id")

    status = get_conference_status(confid)
    if status["error"] != "":
        raise BadConferenceStatusError(status["error"])

    if status["locked"]:
        raise ConferenceLockedError("conference_id")

    if status["launched"]:
        raise ConferenceAlreadyLaunchedError("conference_id")

    license = licenses.get_license()
    if license.state != "valid":
        raise FeatureRestrictedByLicenseError("launch conference")

    if license.demo:
        count_rows = dbtools.select("SELECT count(*) FROM legacy.conferences WHERE state<>%d AND state<>%d" %
                                    (data.CONF_STATE_PRE_INACTIVE, data.CONF_STATE_POST_INACTIVE))
        count = count_rows[0][0]
        if count >= constants.MAX_DEMO_CONFERENCES:
            raise FeatureRestrictedByLicenseError("conference count")

    # check connection to video engine (vtsrv) and to database
    sysinfotools.check_connection_to_backend_components()

    # check layouts limit
    lyt_cnt_rows = dbtools.select("SELECT count(*) FROM legacy.layoutconferences WHERE confid=%s" % (confid))
    conf_lyt_cnt = lyt_cnt_rows[0][0]
    online_lyt_cnt = onlinelayouttools.get_online_layouts_count2()
    lax_layouts_cnt = sysinfotools.get_abonents_restriction()["max_layouts"]
    if conf_lyt_cnt + online_lyt_cnt > lax_layouts_cnt:
        raise TooManyLayoutsError()

    request = "UPDATE legacy.conferences SET state=%d WHERE id=%s" % (data.CONF_STATE_START, confid)
    update_res = dbtools.make_sql_request(request)

    if update_res == 0:
        raise SwitchConferenceStateError()

    return (confid)


def stop_conference(confid):
    confid = int(confid)

    if is_conf_deleted(confid):
        raise ConferenceNotFoundError(confid, "conference_id")

    status = get_conference_status(confid)
    if status["error"] != "":
        raise BadConferenceStatusError(status["error"])

    if status["locked"]:
        raise ConferenceLockedError("conference_id")

    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    request = "UPDATE legacy.conferences SET state=%d WHERE id=%s" % (data.CONF_STATE_CLOSING_VILE, confid)
    res = dbtools.make_sql_request(request)
    if res == 0:
        raise SwitchConferenceStateError()

    return (confid)


def get_all_resolutions_list():
    features_list = features.get_features_list()

    res_list = []
    for i in range(len(data.RESOLUTIONS_LIST)):
        resolution = data.get_frame_resolution(i)
        resolution_supported = features.get_feature(features_list, resolution["feature"])
        if resolution_supported:
            res_list.append(StringRecordEntity(i, resolution["hint"]))

    return (AllResolutionsListResponse(res_list))


def is_auto_start_time_range(_curtimesec: int, _time_range_list: list, _auto_start_mode: int) -> bool:
    """This function check: is _curtimesec in range [_startsec; _endsec]."""
    if _auto_start_mode == data.AUTO_START_MODE_ONCE:
        #logging.info("is_auto_start_time_range(): _auto_start_mode == data.AUTO_START_MODE_OFF, return " + str(result))
        for time_range in _time_range_list:
            start_dt = time_range.start_time
            end_dt = time_range.end_time
            if _curtimesec >= start_dt and _curtimesec <= end_dt and _curtimesec <= start_dt + 60:
                return True
        return False
    elif _auto_start_mode != data.AUTO_START_MODE_OFF:
        #logging.info("is_auto_start_time_range(): _auto_start_mode != data.AUTO_START_MODE_OFF, return " + str(result))
        cur_time_tup = time.gmtime(_curtimesec)
        cur_hour = cur_time_tup[3]
        cur_minute = cur_time_tup[4]

        for time_range in _time_range_list:
            start_dt = time_range.start_time
            end_dt = time_range.end_time

            start_time_tup = time.gmtime(start_dt)
            start_hour = start_time_tup[3]
            start_minute = start_time_tup[4]
            if _curtimesec>=start_dt and _curtimesec<=end_dt and cur_hour==start_hour and cur_minute==start_minute:
                return True

        return False
    else:
        return False # don't autostart


def is_auto_start_hour_minute(_curtimesec: int, _time_range_list: list) -> bool:
    """This function check: is _curtimesec time equal auto start time (see hour:minute from _startsec)."""
    cur_time_tup = time.gmtime(_curtimesec)
    cur_hour = cur_time_tup[3]
    cur_minute = cur_time_tup[4]

    for time_range in _time_range_list:
        start_dt = time_range.start_time
        end_dt = time_range.end_time

        start_time_tup = time.gmtime(start_dt)
        start_hour = start_time_tup[3]
        start_minute = start_time_tup[4]
        if _curtimesec>=start_dt and _curtimesec<=end_dt and cur_hour == start_hour and cur_minute == start_minute:
            return True

    #logging.info("is_auto_start_hour_minute(): return " + str(result))
    return False


def is_auto_stop_hour_minute(_curtimesec: int, _time_range_list: list) -> bool:
    """This function check: is _curtimesec time equal auto stop time (see hour:minute from _startsec)."""
    cur_time_tup = time.gmtime(_curtimesec)
    cur_hour = cur_time_tup[3]
    cur_minute = cur_time_tup[4]

    for time_range in _time_range_list:
        start_dt = time_range.start_time
        end_dt = time_range.end_time
        last_minute_dt = end_dt - 60

        end_time_tup = time.gmtime(last_minute_dt)
        end_hour = end_time_tup[3]
        end_minute = end_time_tup[4]
        if _curtimesec>=start_dt and _curtimesec<=end_dt and cur_hour == end_hour and cur_minute == end_minute:
            return True

    # logging.info("is_auto_stop_hour_minute(): return " + str(result))
    return False


def is_auto_start_period(_curtimesec: int, _auto_start_mode: int, _auto_start_week_day: int,
                         _auto_start_month_day: int, _start_week_first: int, _start_week_last: int) -> bool:
    """This function check period of start of conference (daily/weekly/monthly)."""
    cur_time_tup = time.gmtime(_curtimesec)

    cur_week_day = cur_time_tup[6]
    cur_week_day_mask = 1 << cur_week_day

    cur_month_day = cur_time_tup[2]
    cur_month_day_mask = 1 << cur_month_day

    cur_year = cur_time_tup[0]
    cur_month = cur_time_tup[1]
    wd, month_len = calendar.monthrange(cur_year, cur_month)

    if _auto_start_mode == data.AUTO_START_MODE_OFF:
        #logging.info("is_auto_start_period(): _auto_start_mode == data.AUTO_START_MODE_OFF, return False")
        return False
    elif _auto_start_mode == data.AUTO_START_MODE_DAILY: # any day
        #logging.info("is_auto_start_period(): _auto_start_mode == data.AUTO_START_MODE_DAILY, return True")
        return True
    elif _auto_start_mode == data.AUTO_START_MODE_WEEKLY: # concrete week day
        #logging.info("is_auto_start_period(): _auto_start_mode == data.AUTO_START_MODE_WEEKLY, return " + str(result))
        return (cur_week_day_mask & _auto_start_week_day) != 0
    elif _auto_start_mode == data.AUTO_START_MODE_MONTHLY:
        #logging.info("is_auto_start_period(): _auto_start_mode == data.AUTO_START_MODE_MONTHLY")
        if cur_month_day <= 7 and _start_week_first != 0 and (cur_week_day_mask & _start_week_first) != 0: # day of first week
            return True
        if  cur_month_day + 7 > month_len and _start_week_last != 0 and (cur_week_day_mask & _start_week_last) != 0: # day of last week
            return True
        if _auto_start_month_day != 0 and (cur_month_day_mask & _auto_start_month_day) != 0: # concrete month day
            return True
        return False # error or not coincidence of time
    else:
        #logging.info("is_auto_start_period(): _auto_start_mode == %d (error), return False" % (_auto_start_mode))
        return False # error


def is_repeate_auto_start(_curtimesec: int, _repeate_mode: int, _repeate_count: int, _repeate_last_time: int) -> bool:
    """This function check repaate limit of autostart of conference."""
    if _repeate_mode == data.REPEATE_MODE_INFINITY: # repeate autostart infinity times
        #logging.info("is_repeate_auto_start() _repeate_mode == data.REPEATE_MODE_INFINITY, return True")
        return True
    elif _repeate_mode == data.REPEATE_MODE_COUNTER: # repeate autostart _repeate_count times
        result = _repeate_count > 0
        #logging.info("is_repeate_auto_start() _repeate_mode == data.REPEATE_MODE_COUNTER, return " + str(result))
        return result
    elif _repeate_mode == data.REPEATE_MODE_STOP_DATE: # repeate autostart to moment _repeate_last_time
        result = _curtimesec < _repeate_last_time
        #logging.info("is_repeate_auto_start() _repeate_mode == data.REPEATE_MODE_STOP_DATE, return " + str(result))
        return result
    else:
        #logging.info("is_repeate_auto_start() _repeate_mode == %d, return False" % (_repeate_mode))
        return False # error / off


def is_time_table_stopped(_curtimesec: int, _stop_time_table_mode: int, _stop_time_table_begin: int,
                          _stop_time_table_end: int) -> bool:
    """This function checks: is time table stopped?"""
    if _stop_time_table_mode != data.STOP_TIME_TABLE_OFF:
        result = _curtimesec >= _stop_time_table_begin and _curtimesec <= _stop_time_table_end
        #logging.info("is_time_table_stopped() _stop_time_table_mode != data.STOP_TIME_TABLE_OFF, return " + str(result))
        return result

    #logging.info("is_time_table_stopped() _stop_time_table_mode == data.STOP_TIME_TABLE_OFF, return False")
    return False # don't use "stop time table"


def is_auto_stop(_curtimesec: int, _last_start_time: int, _stop_mode: int, _stop_duration: int,
                 _stop_abs_time: int) -> bool:
    """This function check: is conference must be stopped automatically?"""
    if _stop_mode == data.AUTO_STOP_MODE_OFF:
        #logging.info("is_auto_stop() _stop_mode == data.AUTO_STOP_MODE_OFF, return False")
        return False
    elif _stop_mode == data.AUTO_STOP_MODE_DURATION: # stop conference after specified duration (in seconds)
        result = _curtimesec > _last_start_time + _stop_duration and _curtimesec < _last_start_time + _stop_duration+60
        #logging.info("is_auto_stop() _stop_mode == data.AUTO_STOP_MODE_DURATION, return " + str(result))
        return result
    elif _stop_mode == data.AUTO_STOP_MODE_STOPTIME: # stop conference at specified time
        result = _curtimesec > _stop_abs_time and _curtimesec < _stop_abs_time + 60
        #logging.info("is_auto_stop() _stop_mode == data.AUTO_STOP_MODE_STOPTIME, return " + str(result))
        return result
    else:
        #logging.info("is_auto_stop() _stop_mode == %d, return False" % (_stop_mode))
        return False # error


def is_end_of_time_range(_curtimesec: int, _time_range_list: list) -> bool:
    """this function returns True if some time range is ended."""
    for time_range in _time_range_list:
        end_dt = time_range.end_time
        if _curtimesec >= end_dt and _curtimesec <= end_dt + 60:
            return True
    return False


def get_conf_audio_codecs(confid: int) -> AudioCodecsEntity:
    """Get audio codecs settings for specified conference 'confid'."""
    audio_codecs = AudioCodecsEntity(True)

    configs = dbtools.load_table(
        "legacy.configurations",
        """id, aac, g711, g726, g728, g722, g7221_24, g7221_32, g7221c_24, g7221c_32, g7221c_48, g729, g7231, g719""",
        "id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (confid)
    )
    if len(configs) != 0:
        audio_codecs.init_codecs(
            tools.int_to_bool(configs[0][1]), # aac
            tools.int_to_bool(configs[0][2]), # g711
            tools.int_to_bool(configs[0][3]), # g726
            tools.int_to_bool(configs[0][4]), # g728
            tools.int_to_bool(configs[0][5]), # g722
            tools.int_to_bool(configs[0][6]), # g722_1_24
            tools.int_to_bool(configs[0][7]), # g722_1_32
            tools.int_to_bool(configs[0][8]), # g722_1c_24
            tools.int_to_bool(configs[0][9]), # g722_1c_32
            tools.int_to_bool(configs[0][10]), # g722_1c_48
            tools.int_to_bool(configs[0][11]), # g729
            tools.int_to_bool(configs[0][12]), # g723_1
            tools.int_to_bool(configs[0][13]) # g719
        )

    return audio_codecs


def get_audio_codecs_cmd_list(confid: int, config_commands: list):
    """Build commands list to configure all audio codecs of conference confid."""
    audio_codecs = get_conf_audio_codecs(confid)
    cid = tools.conference_name_by_id(confid)
    cmd_list = audio_codecs.get_conference_cmd_list(cid)
    config_commands += cmd_list


def get_conf_video_codecs(confid: int) -> VideoCodecsEntity:
    """Get video codecs settings for specified conference 'confid'."""
    video_codecs = VideoCodecsEntity(True)

    configs = dbtools.load_table(
        "legacy.configurations",
        """h261, h263, h264, h264high, h265""",
        "id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (confid)
    )
    if len(configs) != 0:
        video_codecs.init_codecs(
            tools.int_to_bool(configs[0][0]), # h261
            tools.int_to_bool(configs[0][1]), # h263
            tools.int_to_bool(configs[0][2]), # h264
            tools.int_to_bool(configs[0][3]), # h264high
            tools.int_to_bool(configs[0][4]) # h265
        )

    return video_codecs


def get_video_codecs_cmd_list(confid: int, config_commands: list):
    """Build commands list to configure all video codecs of conference confid."""
    video_codecs = get_conf_video_codecs(confid)
    cid = tools.conference_name_by_id(confid)
    cmd_list = video_codecs.get_conference_cmd_list(cid)
    config_commands += cmd_list








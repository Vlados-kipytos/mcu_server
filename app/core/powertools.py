#!/usr/bin/python3

import logging
import sys

import app.core.tools as tools


def restart_all_api():
    if sys.platform == "win32":
        logging.error("reatart_all_api(): windows version is not implemented")
        return False

    out, err = tools.exec_cmd([ "systemctl", "restart", "tcmcu-php-fpm.service" ], False)
    if len(err) != 0:
        return False

    out, err = tools.exec_cmd([ "systemctl", "restart", "tcmcu.target" ], False)
    if len(err) != 0:
        return False

    out, err = tools.exec_cmd([ "systemctl", "restart", "tcmcu-web.service" ], False)
    if len(err) != 0:
        return False

    out, err = tools.exec_cmd([ "systemctl", "restart", "tcmcu-web-api.target" ], False)
    if len(err) != 0:
        return False

    return True


def restart_os():
    if sys.platform == "win32":
        logging.error("reatart_os(): windows version is not implemented")
        return False

    out, err = tools.exec_cmd([ "systemctl", "reboot" ], False)
    if len(err) != 0:
        return False

    return True


def power_off():
    if sys.platform == "win32":
        logging.error("power_off(): windows version is not implemented")
        return False

    out, err = tools.exec_cmd([ "systemctl", "poweroff" ], False)
    if len(err) != 0:
        return False

    return True




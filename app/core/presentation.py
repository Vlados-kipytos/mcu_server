#!/usr/bin/python3

import logging
import time
import urllib

import app.core.callabtools as callabtools
import app.core.conftools as conftools
import app.core.constants as constants
import app.core.nettools as nettools
import app.core.onlinetools as onlinetools
import app.core.tools as tools
import app.core.xmltools as xmltools
from .errors import ConferenceAlreadyStoppedError
from .errors import PresFileIsUsedInLaunchedConference
from .errors import SessionAbonentNotFoundError
from .requests import GetPresentationStatusRequest
from .requests import StopIncomingPresentationRequest
from .requests import SwitchPresentationRequest
from .responses import GetPresentationStatusResponse
from .responses import StopIncomingPresentationResponse
from .responses import SwitchPresentationResponse


def get_current_presentation_filename(confid):
    """This function return current media file name (presentation) in launched conference 'confid'.
    If it is not in conference, then return empty string.
    NOTE: This function return filename AS IS (in quoted encoding for internal use in engine)"""
    confid = int(confid)

    connstr, confstr = onlinetools.get_online_state()

    conf_json = onlinetools.extract_conference(confstr, confid)

    conf_conn_list = onlinetools.extract_abonents_list(conf_json, 1, connstr)

    presentation_uri = onlinetools.get_presentation_uri(conf_conn_list, connstr)
    return presentation_uri


def is_file_sent(filename: str) -> bool:
    """If file 'filename' is sent as presentation, then return True.
    'filename' must be full name of file in quoted encoding."""

    connstr, confstr = onlinetools.get_online_state()

    conn_list = onlinetools.extract_conn_list(connstr)

    for conn in conn_list:
        if conn["contact_uri"] == filename:
            return True

    return False


def wait_presentation_state(_confid: int, _switched: bool):
    """If _switched==True function waits for presentation switch on. Else waits for presentation switch off."""
    MAX_STEPS = 20
    STEP_DURATION = 0.5
    for step in range(MAX_STEPS):
        time.sleep(STEP_DURATION)
        pres_fname = get_current_presentation_filename(_confid)
        if _switched and len(pres_fname) != 0:  # check: is presentation on
            logging.error("wait_presentation_state() wait 'on' ok")
            return True
        elif (not _switched) and len(pres_fname) == 0:  # check: is presentation off
            logging.error("wait_presentation_state() wait 'off' ok")
            return True

    switched_num = tools.bool_to_int(_switched)
    logging.error("wait_presentation_state() wait for _switched=%d presentation: TIMEOUT" % (switched_num))
    return False


def switch_presentation(request: SwitchPresentationRequest) -> SwitchPresentationResponse:
    """NOTE:
    1) request.filename must be 'normal' filename (not in quoted encoding).
    2) request.filename must be short file name (without path) of file placed in 'presentation' directory."""

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    cur_pres_fname = get_current_presentation_filename(request.conference_id)

    if request.turn_on:
        # check current conference: is some presentation showed?
        if len(cur_pres_fname) != 0:
            commands_list = ["HANGUP PEER " + cur_pres_fname]
            nettools.exec_cmd_list_s(commands_list)
            wait_presentation_state(request.conference_id, False)

        ctrl_params = xmltools.get_config()
        long_fname = ctrl_params["dirs"]["presentation"] + "/" + request.filename

        conv_fname = tools.invert_back_slash(long_fname)
        full_fname = "file:2@" + urllib.parse.quote(conv_fname)  # set quoted encoding (to internal use in engine)

        # check all conferences: is file request.filename showed as presentation?
        if is_file_sent(full_fname):
            raise PresFileIsUsedInLaunchedConference(request.filename)

        callabtools.set_outgoing_call(request.conference_id, 0, full_fname, full_fname, full_fname, full_fname)
        commands_list = ["DIAL " + full_fname]
        nettools.exec_cmd_list_s(commands_list)
        wait_presentation_state(request.conference_id, True)
    else:
        if len(cur_pres_fname) == 0:
            raise SessionAbonentNotFoundError(request.filename, "filename")

        commands_list = ["HANGUP PEER " + cur_pres_fname]
        nettools.exec_cmd_list_s(commands_list)
        wait_presentation_state(request.conference_id, False)

    return SwitchPresentationResponse(request.filename)


def get_presentation_status(request: GetPresentationStatusRequest) -> GetPresentationStatusResponse:
    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    fname = get_current_presentation_filename(request.conference_id)
    fname = urllib.parse.unquote(fname)  # get from engine unquoted filename (to show in API)
    if len(fname) != 0:
        switched_on = True
    else:
        switched_on = False

    short_fname = tools.get_short_file_name(fname)

    response = GetPresentationStatusResponse(switched_on, short_fname)
    return response


def stop_incoming_presentation(request: StopIncomingPresentationRequest) -> StopIncomingPresentationResponse:
    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    cid = tools.conference_name_by_id(request.conference_id)
    commands_list = ["PRESENTATION STOP " + cid]
    nettools.exec_cmd_list_s(commands_list)

    return StopIncomingPresentationResponse(request.conference_id)

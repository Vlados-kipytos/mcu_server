#!/usr/bin/python3


import csv
import logging
import os
import xml.dom.minidom

import app.core.addrbook as addrbook
import app.core.features as features
import app.core.tools as tools
from .entities import AudioCodecsEntity
from .entities import ContactEntity
from .entities import VideoCodecsEntity
from .errors import MCUFileNotFoundError
from .errors import ParseFileError
from .requests import CreateContactRequest
from .requests import CreateGroupRequest
from .requests import GetContactListRequest
from .responses import ImportAddressBookResponse


def unpack_list_from_str(buf_str):
    lst = []

    parse_word = 0
    cur_word = ""

    for i in range(len(buf_str)):
        letter = buf_str[i]
        if parse_word:
            if letter == '"':
                lst.append(cur_word)
                cur_word = ""
                parse_word = 0
            else:
                cur_word += letter
        else:
            if letter == '"':
                parse_word = 1

    # tail
    if cur_word != "":
        lst.append(cur_word)

    return (lst)


def import_addrbook_from_csv(filename) -> ImportAddressBookResponse:
    """This function imports address book from csv-file 'filename'."""

    # get old address book
    request = GetContactListRequest(0, 1000000000, "name", "asc", "", "", "", "", "", "", None)
    response = addrbook.get_contacts_list(request)
    old_contacts_list = response.contacts

    all_groups_dict = addrbook.get_all_groups_dict()
    all_group_abonents = addrbook.get_all_group_abonents()
    features_list = features.get_features_list()

    csvfile = open(filename, encoding='utf-8')
    if csvfile == None:
        raise MCUFileNotFoundError(filename, "file_name")

    rdr = csv.reader(csvfile, delimiter=';', quotechar='"')

    # read csv-file line by line
    imported = 0
    failed = 0
    for row in rdr:
        line_len = len(row)
        if line_len == 0:
            continue

        if line_len == 1: # 1 column in row: empty group
            group_name = row[0]
            if group_name not in all_groups_dict:
                request = CreateGroupRequest(group_name)
                new_group = addrbook.add_new_group_fast(request)
                gr_id = new_group.group_id
                all_groups_dict[group_name] = gr_id
                if gr_id not in all_group_abonents:
                    all_group_abonents[gr_id] = {}
            continue

        if line_len != 7:
            logging.error("import_addrbook_from_csv(): bad columns count")
            failed += 1
            continue  # bad row format

        # 7 column in row: contact
        name = row[0]
        sipaddr = row[1].strip()
        h323addr = row[2].strip()
        email = row[3]
        bitrate = int(row[4])
        extaddr = row[5].strip()
        gr_list_str = row[6]

        sipaddr = tools.quote_address(sipaddr)
        h323addr = tools.quote_address(h323addr)
        extaddr = tools.quote_address(extaddr)

        if len(sipaddr) != 0 and (not tools.is_correct_sip_address(sipaddr)):
            logging.error("import_addrbook_from_csv(): bad sip address: " + sipaddr)
            failed += 1
            continue
        if len(h323addr) != 0 and (not tools.is_correct_h323_address(h323addr)):
            logging.error("import_addrbook_from_csv(): bad h323 address: " + h323addr)
            failed += 1
            continue
        if len(extaddr) != 0 and (not tools.is_correct_rtsp_address(extaddr)):
            logging.error("import_addrbook_from_csv(): bad rtsp address: " + extaddr)
            failed += 1
            continue

        imported += 1

        found_id = 0
        audio_codecs = AudioCodecsEntity(True)
        audio_codecs.init_by_supported_fields(features_list)
        video_codecs = VideoCodecsEntity(True)
        video_codecs.init_by_supported_fields(features_list)
        found_contact = ContactEntity(0, "", "", "", "", "", "", 0, [], "", False, 0, -1, audio_codecs, video_codecs,"",
                                      False)
        for ab in old_contacts_list:
            if (sipaddr != "" and tools.is_addr_equivalent(ab.sip, sipaddr, True)) or \
                    (h323addr != "" and tools.is_addr_equivalent(ab.h323, h323addr, True)) or \
                    (extaddr != "" and tools.is_addr_equivalent(ab.rtsp, extaddr, True)):
                found_id = ab.contact_id
                found_contact = ab
                break

        if found_id != 0:
            found_contact.name = name
            found_contact.sip = sipaddr
            found_contact.h323 = h323addr
            found_contact.rtsp = extaddr
            found_contact.email = email
            found_contact.bitrate = bitrate
            found_contact.autoadded = False
            addrbook.update_contact_fast(found_contact)
        else:
            request = CreateContactRequest(name, "", sipaddr, h323addr, extaddr, email, bitrate, "", False, -1,
                                           audio_codecs, video_codecs, "", False)
            found_id = addrbook.create_new_contact_fast(request)

        # add current abonent to its groups
        gr_list = unpack_list_from_str(gr_list_str)
        for gr_name in gr_list:
            if gr_name in all_groups_dict:
                gr_id = all_groups_dict[gr_name]
            else:
                request = CreateGroupRequest(gr_name)
                new_group = addrbook.add_new_group_fast(request)
                gr_id = new_group.group_id
                all_groups_dict[gr_name] = gr_id

            if gr_id not in all_group_abonents:
                all_group_abonents[gr_id] = {}
            if found_id not in all_group_abonents[gr_id]:
                addrbook.add_contact_to_group_fast(found_id, gr_id)
                all_group_abonents[gr_id][found_id] = True
        # OLD: addrbook.add_abonent_to_groups(found_id, gr_list)

    csvfile.close()

    short_fname = tools.get_short_file_name(filename)
    return ImportAddressBookResponse(short_fname, imported, failed, imported + failed)


def import_addrbook_from_xml(filename) -> ImportAddressBookResponse:
    """This function imports address book from xml-file 'filename'."""

    # get old address book
    request = GetContactListRequest(0, 1000000000, "name", "asc", "", "", "", "", "", "", None)
    response = addrbook.get_contacts_list(request)
    old_contacts_list = response.contacts

    features_list = features.get_features_list()

    if not os.path.exists(filename):
        raise MCUFileNotFoundError(filename, "file_name")

    nodeList = []
    try:
        xmldoc = xml.dom.minidom.parse(filename)

        nodeList = xmldoc.getElementsByTagName("address")
        if len(nodeList) == 0:
            return (1)  # empty address book
    except:
        raise ParseFileError(filename)

    # get addresses of abonents (list of nodes "<address>")
    imported = 0
    failed = 0
    for node in nodeList:
        cur_name = ""
        cur_sip = ""
        cur_h323 = ""
        cur_email = ""
        cur_speed = "0"

        # parse node "<address>"
        attr = node.attributes
        for key, val in attr.items():
            if key == "name":
                cur_name = val
            if key == "sip-uri":
                cur_sip = val
            if key == "h323-uri":
                cur_h323 = val
            if key == "e-mail":
                cur_email = val
            if key == "speed":
                cur_speed = val

        cur_sip = tools.quote_address(cur_sip)
        cur_h323 = tools.quote_address(cur_h323)

        if len(cur_sip) != 0 and (not tools.is_correct_sip_address(cur_sip)):
            logging.error("import_addrbook_from_xml(): bad sip address: " + cur_sip)
            failed += 1
            continue
        if len(cur_h323) != 0 and (not tools.is_correct_h323_address(cur_h323)):
            logging.error("import_addrbook_from_xml(): bad h323 address: " + cur_h323)
            failed += 1
            continue

        found_id = 0
        audio_codecs = AudioCodecsEntity(True)
        audio_codecs.init_by_supported_fields(features_list)
        video_codecs = VideoCodecsEntity(True)
        video_codecs.init_by_supported_fields(features_list)
        found_contact = ContactEntity(0, "", "", "", "", "", "", 0, [], "", False, 0, -1, audio_codecs, video_codecs,"",
                                      False)
        for ab in old_contacts_list:
            if (cur_sip != "" and tools.is_addr_equivalent(ab.sip, cur_sip, True)) or \
                    (cur_h323 != "" and tools.is_addr_equivalent(ab.h323, cur_h323, True)):
                found_id = ab.contact_id
                found_contact = ab
                break

        if found_id != 0:
            found_contact.name = cur_name
            found_contact.sip = cur_sip
            found_contact.h323 = cur_h323
            found_contact.email = cur_email
            found_contact.bitrate = int(cur_speed)
            found_contact.autoadded = False
            addrbook.update_contact_fast(found_contact)
        else:
            if (len(cur_sip) == 0 and len(cur_h323) == 0) or len(cur_name) == 0:
                logging.error("import_addrbook_from_xml(): error: empty contact name/address")
                failed += 1
                continue
            request = CreateContactRequest(cur_name, "", cur_sip, cur_h323, "", cur_email, int(cur_speed), "", False, -1,
                                           audio_codecs, video_codecs, "", False)
            addrbook.create_new_contact_fast(request)

        imported += 1

    short_fname = tools.get_short_file_name(filename)
    return ImportAddressBookResponse(short_fname, imported, failed, imported + failed)

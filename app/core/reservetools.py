#!/usr/bin/python3

import logging
import os
import sys
import time

import app.core.files as files
import app.core.pipetools as pipetools
import app.core.profile_settings as profile_settings
import app.core.tools as tools
import app.core.xmltools as xmltools
import event_processor.ipc.on_event_tools as on_event_tools
from .errors import ExportDatabaseError
from .errors import ImportDatabaseError
from .errors import MCUFileNotFoundError
from .responses import HandlingFileResponse


def get_reserve_file_name():
    """This function returns unique file name for reserve copy of database."""
    timestamp_str = tools.get_unique_time_stamp_str()
    directory = files.get_directory("dbcopy")
    fname = directory + files.SLASH + timestamp_str + ".dump"
    return (fname)


def is_reserve_time():
    """If current time is hour of reserve DB, then return 1 else return 0."""
    reserve_db_settings = profile_settings.get_db_settings()
    if reserve_db_settings.weekday < 0:  # manual reserve - don't reserve by timer
        return (0)

    # get current time
    timetup = time.gmtime(time.time())
    wday = timetup[6]
    hour = timetup[3]
    minute = timetup[4]

    # make saving at specified hour
    if wday == reserve_db_settings.weekday and reserve_db_settings.hour == hour and reserve_db_settings.minute == minute:
        return (1)
    else:
        return (0)


# OLD:
def reserve_db() -> HandlingFileResponse:
    """This function save database to file."""
    cfg = xmltools.get_config()

    full_fname = get_reserve_file_name()
    if sys.platform == "win32":
        cmd = "pg_dump -Fc --host=%s --port=%d --username=%s --no-password %s >%s" % (
            cfg["db"]["host"], cfg["db"]["port"], cfg["db"]["username"], cfg["db"]["dbname"], full_fname)
    else:
        cmd = "PGPASSWORD=%s pg_dump -Fc --host=%s --port=%d --username=%s %s >%s" % (
            cfg["db"]["password"], cfg["db"]["host"], cfg["db"]["port"], cfg["db"]["username"], cfg["db"]["dbname"],
            full_fname)

    os.system(cmd)

    return HandlingFileResponse(tools.get_short_file_name(full_fname))


def reserve_db2() -> HandlingFileResponse:
    """This function save database to file."""
    cfg = xmltools.get_config()

    full_fname = get_reserve_file_name()

    on_event_tools.stop_all_conferences()

    if sys.platform == "win32":
        cmd_str = "pg_dump -Fc --host=%s --port=%d --username=%s --no-password --file=%s %s" % (
               cfg["db"]["host"], cfg["db"]["port"], cfg["db"]["username"], full_fname, cfg["db"]["dbname"])
        out, err = tools.exec_cmd2(cmd_str, False)
    else:
        cmd_str = "pg_dump -Fc --port=%d --username=postgres --file=%s %s" % \
                      (cfg["db"]["port"], full_fname, cfg["db"]["dbname"])
        out, err = tools.exec_cmd2(cmd_str, False)

    logging.error("reserve_db() command: " + cmd_str)
    logging.error("reserve_db() stdout: ")
    logging.error(out)
    logging.error("reserve_db() stderr: ")
    logging.error(err)

    if len(err) != 0:
        raise ExportDatabaseError()

    return HandlingFileResponse(tools.get_short_file_name(full_fname))


def get_sql_fname():
    """returns file name with sql request to stop database connecions"""
    cfg = xmltools.get_config()
    return cfg["dirs"]["shareddir"] + files.SLASH + "stop_dbconn.sql"


def save_stop_conn_sql_to_file(_fname: str):
    """save to file sql request to stop database connections"""
    cfg = xmltools.get_config()
    dbname = cfg["db"]["dbname"]

    sql = """REVOKE CONNECT ON DATABASE %s FROM public;
    SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname='%s';""" % \
    (dbname, dbname)

    sql_bytes = sql.encode("UTF-8")

    try:
        f = open(_fname, "wb")
        f.write(sql_bytes)
        f.close()
    except:
        logging.error("save_stop_conn_sql(): exception: " + str(sys.exc_info()))


def stop_db_conn():
    """make sql request to stop database connections"""
    fname = get_sql_fname()
    save_stop_conn_sql_to_file(fname)

    cfg = xmltools.get_config()
    dbport = cfg["db"]["port"]

    cmd_str = "psql -p %d --username=postgres --file=%s" % (dbport, fname)
    out, err = tools.exec_cmd2(cmd_str, False)


def restore_db(fname: str) -> HandlingFileResponse:
    """This function restore database from file. 'fname' - short file name (without directory)."""
    cfg = xmltools.get_config()

    directory = files.get_directory("dbcopy")
    full_fname = directory + files.SLASH + fname

    if not os.path.exists(full_fname):
        raise MCUFileNotFoundError(full_fname, "file_name")

    on_event_tools.stop_all_conferences()

    if sys.platform == "win32":
        cmd_str = "pg_restore --clean --host=%s --port=%d --username=%s --no-password --dbname=%s %s" % (
            cfg["db"]["host"], cfg["db"]["port"], cfg["db"]["username"], cfg["db"]["dbname"], full_fname)
        out, err = tools.exec_cmd2(cmd_str, False)
    else:
        stop_db_conn()
        cmd_str = "pg_restore --create --clean --port=%d --username=postgres --dbname=postgres %s" % \
                         (cfg["db"]["port"], full_fname)
        out, err = tools.exec_cmd2(cmd_str, False)

    logging.error("restore_db() command: " + cmd_str)
    logging.error("restore_db() stdout: ")
    logging.error(out)
    logging.error("restore_db() stderr: ")
    logging.error(err)

    pipetools.send_msg("RECONNECTDB")
    pipetools.send_msg("RESTART")

    if len(err) != 0:
        raise ImportDatabaseError()

    return HandlingFileResponse(fname)

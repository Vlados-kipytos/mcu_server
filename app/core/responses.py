from app.core.entities import *


class ContactListResponse:

    def __init__(self, contacts: list, full_list_size: int):
        self._contacts = contacts
        self._full_list_size = full_list_size

    @property
    def contacts(self) -> list:
        return self._contacts

    @property
    def full_list_size(self) -> int:
        return self._full_list_size


class ContactResponse:

    def __init__(self, contact: ContactEntity):
        self._contact = contact

    @property
    def contact(self) -> ContactEntity:
        return self._contact


class DeleteContactResponse:
    def __init__(self, contactid: int):
        self._contactid = contactid

    @property
    def contactid(self) -> int:
        return self._contactid


class ConferencesListResponse:
    def __init__(self, conferences: list, full_list_size: int):
        self._conferences = conferences
        self._full_list_size = full_list_size

    @property
    def conferences(self) -> list:
        return self._conferences

    @property
    def full_list_size(self) -> int:
        return self._full_list_size


class ConferenceResponse:
    def __init__(self, conf: ConferenceEntity):
        self._conf = conf

    @property
    def conf(self) -> ConferenceEntity:
        return self._conf


class DeleteConferenceResponse:
    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class ConferenceParticipantResponse:

    def __init__(self, participant: ParticipantEntity):
        self._participant = participant

    @property
    def participant(self) -> ParticipantEntity:
        return self._participant


class ConferenceParticipantsListResponse:

    def __init__(self, participants: list):
        self._participants = participants

    @property
    def participants(self) -> list:
        return self._participants


class DeleteConferenceParticipantResponse:
    def __init__(self, confid: int, contactid: int):
        self._confid = confid
        self._contactid = contactid

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def contactid(self) -> int:
        return self._contactid


class StartConferenceSessionResponse:
    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class StopConferenceSessionResponse:
    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class SessionParticipantResponse:

    def __init__(self, participant: SessionParticipantEntity):
        self._participant = participant

    @property
    def participant(self) -> SessionParticipantEntity:
        return self._participant


class SessionParticipantsListResponse:

    def __init__(self, participants: list, connecting_participants: list, disconnected_ab_list: list):
        self._participants = participants
        self._connecting_participants = connecting_participants
        self._disconnected_ab_list = disconnected_ab_list

    @property
    def participants(self) -> list:
        return self._participants

    @property
    def connecting_participants(self) -> list:
        return self._connecting_participants

    @property
    def disconnected_ab_list(self) -> list:
        return self._disconnected_ab_list


class InviteSessionParticipantResponse:

    def __init__(self, session_addr: SessionParticipantAddressEntity):
        self._session_addr = session_addr

    @property
    def session_addr(self) -> SessionParticipantAddressEntity:
        return self._session_addr


class HangupSessionParticipantResponse:

    def __init__(self, session_addr: SessionParticipantAddressEntity):
        self._session_addr = session_addr

    @property
    def session_addr(self) -> SessionParticipantAddressEntity:
        return self._session_addr


class GetAllLayoutsTypesResponse:

    def __init__(self, layout_types_list: list):
        self._layout_types_list = layout_types_list

    @property
    def layout_types_list(self) -> list:
        return self._layout_types_list


class GlobalOnlineLayoutResponse:

    def __init__(self, layout_name: str, layout_type: str, cells_count: int, schema_type: int,
                 participant_positions: list, auto: bool, display_name: str, template: bool):
        self._layout_name = layout_name
        self._layout_type = layout_type
        self._cells_count = cells_count
        self._schema_type = schema_type
        self._participant_positions = participant_positions
        self._auto = auto
        self._display_name = display_name
        self._template = template

    @property
    def layout_name(self) -> str:
        return self._layout_name

    @property
    def layout_type(self) -> str:
        return self._layout_type

    @property
    def cells_count(self) -> int:
        return self._cells_count

    @property
    def schema_type(self) -> int:
        return self._schema_type

    # list of OnlineLayoutAbonentEntity
    @property
    def participant_positions(self) -> list:
        return self._participant_positions

    @property
    def auto(self) -> bool:
        return self._auto

    @property
    def display_name(self) -> str:
        return self._display_name

    @property
    def template(self) -> bool:
        return self._template


class IndividualOnlineLayoutResponse(GlobalOnlineLayoutResponse):

    def __init__(self, layout_name: str, layout_type: str, cells_count: int, schema_type: int,
                 participant_positions: list, participant_id: int, owner_disp_name: str, auto: bool,
                 connection_id: int, display_name: str, template: bool):
        GlobalOnlineLayoutResponse.__init__(self, layout_name, layout_type, cells_count, schema_type,
                                            participant_positions, auto, display_name, template)
        self._participant_id = participant_id
        self._owner_disp_name = owner_disp_name
        self._connection_id = connection_id

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def owner_disp_name(self) -> str:
        return self._owner_disp_name

    @property
    def connection_id(self) -> int:
        return self._connection_id


class OnlineLayoutsListResponse:

    def __init__(self, layouts_list: list):
        self._layouts_list = layouts_list

    @property
    def layouts_list(self) -> list:
        return self._layouts_list


class DeleteOnlineLayoutResponse:

    def __init__(self, confid: int, layout_name: str):
        self._confid = confid
        self._layout_name = layout_name

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def layout_name(self) -> str:
        return self._layout_name


class SheduledLayoutGlobalResponse(SheduledLayoutEntity):

    def __init__(self, layoutid: int, auto: bool, cells_count: int, schema_type: int, wndlist: list,
                 display_name: str, template: bool):
        SheduledLayoutEntity.__init__(self, layoutid, "global", auto, cells_count, schema_type, wndlist,
                                      display_name, template)


class SheduledLayoutIndividualResponse(SheduledLayoutEntity):

    def __init__(self, layoutid: int, auto: bool, cells_count: int, schema_type: int, wndlist: list,
                 participant_id: int, display_name: str, template: bool):
        SheduledLayoutEntity.__init__(self, layoutid, "individual", auto, cells_count, schema_type, wndlist,
                                      display_name, template)
        self._participant_id = participant_id

    @property
    def participant_id(self) -> int:
        return self._participant_id


class SheduledLayoutAspeakerResponse(SheduledLayoutEntity):

    def __init__(self, layoutid: int, auto: bool, cells_count: int, schema_type: int, wndlist: list,
                 display_name: str, template: bool):
        SheduledLayoutEntity.__init__(self, layoutid, "active_speaker", auto, cells_count, schema_type, wndlist,
                                      display_name, template)


class DeleteSheduledLayoutResponse:

    def __init__(self, layout_id: int):
        self._layout_id = layout_id

    @property
    def layout_id(self) -> int:
        return self._layout_id


class GroupsListResponse:

    def __init__(self, groups: list):
        self._groups = groups

    @property
    def groups(self) -> list:
        return self._groups


class GroupResponse:

    def __init__(self, group: GroupEntity):
        self._group = group

    @property
    def group(self) -> GroupEntity:
        return self._group


class DeleteGroupResponse:
    def __init__(self, group_id: int):
        self._group_id = group_id

    @property
    def group_id(self) -> int:
        return self._group_id


class DeleteContactFromGroupResponse:
    def __init__(self, group_id: int, contactid: int):
        self._group_id = group_id
        self._contactid = contactid

    @property
    def group_id(self) -> int:
        return self._group_id

    @property
    def contactid(self) -> int:
        return self._contactid


class RegistrationResponse:

    def __init__(self, result: RegistrationResultEntity):
        self._result = result

    @property
    def result(self) -> RegistrationResultEntity:
        return self._result


class OfflineRegistrationResponse:

    def __init__(self, csr_data: bytes):
        self._csr_data = csr_data

    @property
    def csr_data(self) -> bytes:
        return self._csr_data


class GetLicenseResponse:

    def __init__(self, license: LicenseEntity):
        self._license = license

    @property
    def license(self) -> LicenseEntity:
        return self._license


class InstanceCallsResponse:

    def __init__(self, calls: CallsEntity):
        self._calls = calls

    @property
    def calls(self) -> CallsEntity:
        return self._calls


class InstanceSipResponse:

    def __init__(self, sip: SipEntity):
        self._sip = sip

    @property
    def sip(self) -> SipEntity:
        return self._sip


class GetH323SettingsResponse:

    def __init__(self, h323: H323SettingsEntity):
        self._h323 = h323

    @property
    def h323(self) -> H323SettingsEntity:
        return self._h323


class InstanceVideoResponse:

    def __init__(self, video: VideoSettingsEntity):
        self._video = video

    @property
    def video(self) -> VideoSettingsEntity:
        return self._video


class InstanceLDAPResponse:

    def __init__(self, ldapsettings: LDAPSettingsEntity, is_connected: bool):
        self._ldapsettings = ldapsettings
        self._is_connected = is_connected

    @property
    def ldapsettings(self) -> LDAPSettingsEntity:
        return self._ldapsettings

    @property
    def is_connected(self) -> bool:
        return self._is_connected


class InstanceEmailResponse:

    def __init__(self, email: EmailSettingsEntity):
        self._email = email

    @property
    def email(self) -> EmailSettingsEntity:
        return self._email


class InstanceDBResponse:

    def __init__(self, db: DBSettingsEntity):
        self._db = db

    @property
    def db(self) -> DBSettingsEntity:
        return self._db


class InstanceMemoryResponse:

    def __init__(self, memory: MemoryDashboardEntity):
        self._memory = memory

    @property
    def memory(self) -> MemoryDashboardEntity:
        return self._memory


class InstanceCPUResponse:

    def __init__(self, cores: list):
        self._cores = cores
        self._count = len(cores)

    @property
    def cores(self) -> list:
        return self._cores

    @property
    def count(self) -> int:
        return self._count


class InstanceNetResponse:

    def __init__(self, interfaces: list):
        self._interfaces = interfaces
        self._count = len(interfaces)

    @property
    def interfaces(self) -> list:
        return self._interfaces

    @property
    def count(self) -> int:
        return self._count


class InstanceFreeSpaceResponse:

    def __init__(self, devices: list):
        self._devices = devices
        self._count = len(devices)

    @property
    def devices(self) -> list:
        return self._devices

    @property
    def count(self) -> int:
        return self._count


class AllResolutionsListResponse:

    def __init__(self, resolutions: list):
        self._resolutions = resolutions

    @property
    def resolutions(self) -> list:
        return self._resolutions


class CalculateLayoutResponse:
    def __init__(self, wnd_list: list):
        self._wnd_list = wnd_list

    # list of LayoutWindowCoordEntity
    @property
    def wnd_list(self) -> list:
        return self._wnd_list


class SessionParticipantControlResponse:
    def __init__(self, conference_id: int, participant_id: int):
        self._conference_id = conference_id
        self._participant_id = participant_id

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def participant_id(self) -> int:
        return self._participant_id


class SessionGroupOperationResponse:
    def __init__(self, conference_id: int, processed_obj_count: int, failed_obj_count: int):
        self._conference_id = conference_id
        self._processed_obj_count = processed_obj_count
        self._failed_obj_count = failed_obj_count

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def processed_obj_count(self) -> int:
        return self._processed_obj_count

    @property
    def failed_obj_count(self) -> int:
        return self._failed_obj_count


class GetPreviewImageResponse:
    def __init__(self, image: bytes):
        self._image = image

    @property
    def image(self) -> bytes:
        return self._image


class SessionControlResponse:
    def __init__(self, conference_id: int):
        self._conference_id = conference_id

    @property
    def conference_id(self) -> int:
        return self._conference_id


class MoveToOtherConfResponse:
    def __init__(self, conf_from: int, conf_to: int, abonent_uri: str):
        self._conf_from = conf_from
        self._conf_to = conf_to
        self._abonent_uri = abonent_uri

    @property
    def conf_from(self) -> int:
        return self._conf_from

    @property
    def conf_to(self) -> int:
        return self._conf_to

    @property
    def abonent_uri(self) -> str:
        return self._abonent_uri


class WaitingAbonentsListResponse:
    def __init__(self, abonents: list):
        self._abonents = abonents

    # list of WaitAbonentEntity
    @property
    def abonents(self) -> list:
        return self._abonents


class GetRecordingStatusResponse:
    def __init__(self, status: str, urls: list):
        self._status = status
        self._urls = urls

    @property
    def status(self) -> str:
        return self._status

    # list of RecordingURLEntity
    @property
    def urls(self) -> list:
        return self._urls


class GetFilesListResponse:
    def __init__(self, flist: list):
        self._flist = flist

    # list of FileEntity
    @property
    def flist(self) -> list:
        return self._flist


class GetFileResponse:
    def __init__(self, data: bytes):
        self._data = data

    @property
    def data(self) -> bytes:
        return self._data


class HandlingFileResponse:
    def __init__(self, filename: str):
        self._filename = filename

    @property
    def filename(self) -> str:
        return self._filename


class ImportAddressBookResponse:
    def __init__(self, filename: str, imported: int, failed: int, all: int):
        self._filename = filename
        self._imported = imported
        self._failed = failed
        self._all = all

    @property
    def filename(self) -> str:
        return self._filename

    @property
    def imported(self) -> int:
        return self._imported

    @property
    def failed(self) -> int:
        return self._failed

    @property
    def all(self) -> int:
        return self._all


class SwitchPresentationResponse:
    def __init__(self, filename: str):
        self._filename = filename

    # short file name of image, stored in special directory
    @property
    def filename(self) -> str:
        return self._filename


class GetPresentationStatusResponse:
    def __init__(self, switched_on: bool, filename: str):
        self._switched_on = switched_on
        self._filename = filename

    @property
    def switched_on(self) -> bool:
        return self._switched_on

    # short file name of image, stored in special directory
    @property
    def filename(self) -> str:
        return self._filename


class StopIncomingPresentationResponse:
    def __init__(self, conference_id: int):
        self._conference_id = conference_id

    @property
    def conference_id(self) -> int:
        return self._conference_id


class AllConfSessionParticipantsResponse:
    def __init__(self, participants_list: list):
        self._participants_list = participants_list

    # list of ConfSessionParticipantEntity
    @property
    def participants_list(self) -> list:
        return self._participants_list


class ServerTimeResponse:
    def __init__(self, time_sec: int):
        self._time_sec = time_sec

    @property
    def time_sec(self) -> int:
        return self._time_sec


class MCUParamsResponse:
    def __init__(self, server_name: str, version: dict, time_sec: int, diagnostics: dict, engine_connected: bool,
                 db_connected: bool, edit_network: bool):
        self._server_name = server_name
        self._version = version
        self._time_sec = time_sec
        self._diagnostics = diagnostics
        self._engine_connected = engine_connected
        self._db_connected = db_connected
        self._edit_network = edit_network

    @property
    def server_name(self) -> str:
        return self._server_name

    @property
    def version(self) -> dict:
        return self._version

    @property
    def time_sec(self) -> int:
        return self._time_sec

    @property
    def diagnostics(self) -> dict:
        return self._diagnostics

    @property
    def engine_connected(self) -> bool:
        return self._engine_connected

    @property
    def db_connected(self) -> bool:
        return self._db_connected

    @property
    def edit_network(self) -> bool:
        return self._edit_network


class GetPreviewStateResponse:
    def __init__(self, state: str):
        self._state = state

    @property
    def state(self) -> str:
        return self._state


class StartUtilResponse:
    def __init__(self, util_type: str):
        self._util_type = util_type

    @property
    def util_type(self) -> str:
        return self._util_type


class UtilResponse:
    def __init__(self, output: str, error: str, running: bool):
        self._output = output
        self._error = error
        self._running = running

    @property
    def output(self) -> str:
        return self._output

    @property
    def error(self) -> str:
        return self._error

    @property
    def running(self) -> bool:
        return self._running


class SetCrownResponse:
    def __init__(self, confid: int, contactid: int):
        self._confid = confid
        self._contactid = contactid

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def contactid(self) -> int:
        return self._contactid


class DeleteCrownResponse:
    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class RTPSettingsResponse:

    def __init__(self, rtp: RTPEntity):
        self._rtp = rtp

    @property
    def rtp(self) -> RTPEntity:
        return self._rtp


class AllNetworkSettingsResponse:
    def __init__(self, settings_list: list):
        self._settings_list = settings_list

    # list of NetworkSettingsEntity
    @property
    def settings_list(self) -> list:
        return self._settings_list


class NetworkSettingsResponse:
    def __init__(self, settings: NetworkSettingsEntity):
        self._settings = settings

    @property
    def settings(self) -> NetworkSettingsEntity:
        return self._settings


class CanChangeNetworkSettingsResponse:
    def __init__(self, can_edit: bool):
        self._can_edit = can_edit

    @property
    def can_edit(self) -> bool:
        return self._can_edit


class GetVEngineLogResponse:
    def __init__(self, log: dict):
        self._log = log

    # log.json form vengine (vtsrv)
    @property
    def log(self) -> dict:
        return self._log


class VEngineCommandResponse:
    def __init__(self, command: str):
        self._command = command

    @property
    def command(self) -> str:
        return self._command


class MCUReferencesResponse:
    def __init__(self, references: dict):
        self._references = references

    @property
    def references(self) -> dict:
        return self._references


class CleanAddressBookResponse:
    def __init__(self, success: bool):
        self._success = success

    @property
    def success(self) -> bool:
        return self._success


class CustomLayoutTypeResponse:
    def __init__(self, type: CustomLayoutTypeEntity):
        self._type = type

    @property
    def type(self) -> CustomLayoutTypeEntity:
        return self._type


class AllCustomLayoutTypesResponse:
    def __init__(self, types_list: list):
        self._types_list = types_list

    @property
    def types_list(self) -> list:
        return self._types_list


class DeleteCustomLayoutTypeResponse:
    def __init__(self, layout_type: int):
        self._layout_type = layout_type

    @property
    def layout_type(self) -> int:
        return self._layout_type


class RestartCommandResponse:

    def __init__(self, success: bool):
        self._success = success

    @property
    def success(self) -> bool:
        return self._success


class InstallDebResponse:
    def __init__(self, filename: str):
        self._filename = filename

    @property
    def filename(self) -> str:
        return self._filename


class GetInstallDebStateResponse:
    def __init__(self, state: str):
        self._state = state

    @property
    def state(self) -> str:
        return self._state



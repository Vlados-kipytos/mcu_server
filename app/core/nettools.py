#!/usr/bin/python3

# """Network tools"""


import hashlib
import http
import http.client
import logging
import os
import random
import socket
import sys
import urllib

import app.core.constants as constants
import app.core.dbtools as dbtools
import app.core.tools as tools
import app.core.xmltools as xmltools


class UnixHTTPConnection(http.client.HTTPConnection, object):

    def __init__(self, unix_socket_url, timeout=15):
        super(UnixHTTPConnection, self).__init__('localhost', timeout=timeout)
        self.unix_socket_url = unix_socket_url
        self.timeout = timeout
        self.sock = None

    def __del__(self):
        if self.sock:
            # logging.error("class UnixHTTPConnection: __del__: close socket")
            self.sock.close()

    def connect(self):
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.settimeout(self.timeout)
        socket_path = urllib.parse.unquote(urllib.parse.urlparse(self.unix_socket_url).netloc)
        sock.connect(socket_path)
        self.sock = sock


def _create_connection(host, portnum, timeout=15):
    if sys.platform != "win32":
        p_url = urllib.parse.urlparse(host)
        scheme = p_url.scheme
        if scheme and scheme == "unix" and p_url.netloc:
            return UnixHTTPConnection(host, timeout=timeout)

    return http.client.HTTPConnection(host, portnum, timeout=timeout)


def file_to_path(fname):
    """This function raturns path where 'fname' is located"""
    length = len(fname)
    if length == 0:
        return ("")
    i = length - 1
    while i >= 0:
        if fname[i] == '/' or fname[i] == '\\':
            return (fname[:i])
        i = i - 1
    return ("")


def exec_cmd_list(cmd_list):
    """This function execute vitahd-commands from list 'cmd_list' by http protocol (send POST request with each command).
    Input params:
    ipstr    - string with ip-address of vitahd-terminal
    portnum  - number of port on which works web interface of terminal
    cmd_list - list of strings (commands)
    Return Value:
    1 - success
    0 - error"""
    cfg = xmltools.get_config()
    ipstr = cfg["vile"]["ip"]
    portnum = cfg["vile"]["port"]

    if len(cmd_list) == 0:  # if commands list is empty, do none
        return 1
    try:
        conn = _create_connection(ipstr, portnum)  # make http GET request to get session id
        conn.request("GET", "/")
        resp = conn.getresponse()
        resphdrs = resp.getheaders()
        sessionid = ""  # extract session id from http-header "Set-Cookie"
        for hdr in resphdrs:
            if hdr[0] == "set-cookie":
                sessionid = hdr[1]
        respbody = resp.read()
        headersdict = {}
        headersdict["Host"] = ipstr + ":" + str(portnum)
        headersdict["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0"
        headersdict["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
        headersdict["Accept-Language"] = "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3"
        headersdict["Accept-Encoding"] = "identity" #"gzip, deflate"
        headersdict["Content-Type"] = "text/plain;charset=UTF-8"
        # headersdict["Connection"]      = "keep-alive"
        headersdict["Cookie"] = sessionid

        all_cmd_str = ""
        for i in range(len(cmd_list)):
            all_cmd_str += cmd_list[i] + "\n"

        # send http POST for each vitahd command
        conn.request("POST", "/cmd", all_cmd_str.encode("UTF-8"), headersdict)
        resp = conn.getresponse()
        resphdrs = resp.getheaders()
        respbody = resp.read()
        conn.close()
    except http.client.HTTPException as httpex:
        logging.error("function exec_cmd_list(): http exception:" + str(httpex))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        conn.close()
        return 0
    except OSError as ex:
        logging.error(
            "function exec_cmd_list(): OSError exception: errno=" + str(ex.errno) + " , errortext=" + str(ex.strerror))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        conn.close()
        return 0
    except:
        logging.error("function exec_cmd_list(): unknown exception:" + str(sys.exc_info()))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        conn.close()
        return 0
    return 1


def get_url_data(filename):
    """This function get data from vitahd terminal by http protocol (send GET request).
    Input params:
    ipstr    - string with ip-address of vitahd-terminal
    portnum  - number of port on which works web interface of terminal
    filename - string with name of resource (for example "connections.json")
    Return Value:
    string with getted data (at error return empty string)"""
    cfg = xmltools.get_config()
    ipstr = cfg["vile"]["ip"]
    portnum = cfg["vile"]["port"]

    data = ""
    try:
        conn = _create_connection(ipstr, portnum)
        conn.request("GET", "/" + filename)
        resp = conn.getresponse()
        resphdrs = resp.getheaders()
        data = resp.read().decode("UTF-8", "ignore")
        conn.close()
    except http.client.HTTPException as httpex:
        logging.error("function get_url_data(): http exception:" + str(httpex))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        conn.close()
        return data
    except OSError as ex:
        logging.error(
            "function get_url_data(): OSError exception: errno=" + str(ex.errno) + " , errortext=" + str(ex.strerror))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        conn.close()
        return data
    except:
        logging.error("function get_url_data(): unknown exception:" + str(sys.exc_info()))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        conn.close()
        return data
    return data


def hash1(username, realm, password):
    """This function calculates 'H(A1)' value for http autorization (see RFC 2069).
    username - name of user
    raealm   - 'area' (example: "support@integrit.com")
    password - password of user
    Return value: MD5 hash string"""
    hash = hashlib.md5(username + ":" + realm + ":" + password)
    return hash.hexdigest()


def hash2(http_method, uri):
    """This function calculates 'H(A2)' value for http autorization (see RFC 2069).
    http_method - GET or POST
    uri         - requested URI
    Return value: MD5 hash string"""
    # uri_digest     = hashlib.md5(uri)
    # uri_digest_str = uri_digest.hexdigest()
    # hash = hashlib.md5(http_method + ":" + uri_digest_str)
    hash = hashlib.md5(http_method + ":" + uri)
    return hash.hexdigest()


def resp_digest_rfc2069(ha1, nonce, ha2):
    """This function calculates response digest value for http authorizetion (see RFC 2069).
    ha1   - MD5( <user> : <realm> : <passw> )
    nonce - string (see RFC 2069)
    ha2   - MD5( <http_ethod> : <uri> )
    Return value: MD5 hash string"""
    hash = hashlib.md5(ha1 + ":" + nonce + ":" + ha2)
    return hash.hexdigest()


def resp_digest_rfc2617(ha1, nonce, nonceCnt, clientNonce, qop, ha2):
    """This function calculates response digest value for http authorizetion (see RFC 2617).
    ha1         - MD5( <user> : <realm> : <passw> )
    nonce       - string (see RFC 2069)
    nonceCnt    - count of nonce (number 1)
    clientNonce - ?
    qop         - quality of protect ("auth" or "auth-int")
    ha2         - MD5( <http_ethod> : <uri> )
    Return value: MD5 hash string"""
    nonceCntStr = "%08x" % nonceCnt
    hash = hashlib.md5(ha1 + ":" + nonce + ":" + nonceCntStr + ":" + clientNonce + ":" + qop + ":" + ha2)
    return hash.hexdigest()


def get_url_data_s(filename, decode):
    """This function get data from vitahd terminal by http protocol (send GET request).
    Input params:
    ipstr    - string with ip-address of vitahd-terminal
    portnum  - number of port on which works web interface of terminal
    login    - user name
    password - password
    filename - string with name of resource (for example "connections.json")
    decode - flag of decoding result from bytes to unicode string
    Return Value:
    string with getted data (at error return empty string)"""
    cfg = xmltools.get_config()
    ipstr = cfg["vile"]["ip"]
    portnum = cfg["vile"]["port"]
    login = cfg["vile"]["login"]
    password = cfg["vile"]["password"]

    if decode:
        default_data = ""
    else:
        default_data = b""

    data = default_data
    try:
        uri = "/" + filename

        headersdict = {}
        headersdict["Host"] = ipstr + ":" + str(portnum)
        headersdict["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0"
        headersdict["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
        headersdict["Accept-Language"] = "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3"
        headersdict["Accept-Encoding"] = "identity" #"gzip, deflate"
        # headersdict["Connection"]      = "keep-alive"
        headersdict["Upgrade-Insecure-Requests"] = "1"

        conn = _create_connection(ipstr, portnum)
        conn.request("GET", uri, None, headersdict)
        resp = conn.getresponse()
        if decode:
            data = resp.read().decode("UTF-8", "ignore")
        else:
            data = resp.read()
        if resp.status == 200:
            conn.close()
            return data
        if resp.status != 401:
            logging.warning("function get_url_data_s(): unexpected http status code:" + str(
                resp.status) + " , response reason: " + str(resp.reason) + " , get file: " + filename)
            conn.close()
            return default_data
        authhdr = resp.getheader("WWW-Authenticate", "")
        if authhdr == "":
            logging.warning("function get_url_data_s(): no 'WWW-Authenticate' header in response")
            conn.close()
            return default_data

        realm = tools.extract_value_by_name(authhdr, "realm")
        qop = tools.extract_value_by_name(authhdr, "qop")
        nonce = tools.extract_value_by_name(authhdr, "nonce")
        opaque = tools.extract_value_by_name(authhdr, "opaque")
        cnonceNum = int(random.random() * 0x7fffffff)
        cnonce = "%0x" % cnonceNum
        ha1 = hash1(login, realm, password)
        ha2 = hash2("GET", uri)
        response = resp_digest_rfc2617(ha1, nonce, 1, cnonce, qop, ha2)
        authHdrStr = """Digest username="%s", realm="%s", nonce="%s", uri="%s", response="%s", opaque="%s", qop=%s, nc=00000001, cnonce="%s" """ % (
        login, realm, nonce, uri, response, opaque, qop, cnonce)

        resphdrs = resp.getheaders()
        sessionid = ""
        for hdr in resphdrs:
            if hdr[0] == "set-cookie" or hdr[0] == "Set-Cookie":
                sessionid = hdr[1]

        headersdict["Authorization"] = authHdrStr
        headersdict["Cookie"] = sessionid

        conn.request("GET", uri, None, headersdict)
        resp = conn.getresponse()
        if decode:
            data = resp.read().decode("UTF-8", "ignore")
        else:
            data = resp.read()
        conn.close()
    except http.client.HTTPException as httpex:
        logging.error("function get_url_data_s(): http exception:" + str(httpex) + " , get file: " + filename)
        logging.error("http.args: " + str(httpex.args))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        conn.close()
        return data
    except OSError as ex:
        logging.error(
            "function get_url_data_s(): OSError exception: errno=" + str(ex.errno) + " , errortext=" + str(ex.strerror))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        conn.close()
        return data
    except:
        logging.error("function get_url_data_s(): unknown exception:" + str(sys.exc_info()))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        conn.close()
        return data
    return data


def exec_cmd_list_s(cmdlist):
    """This function executes commands list 'cmdlist' (send 'POST /cmd' request).
    Input params:
    ipstr    - string with ip-address of vile
    portnum  - number of port on which works web interface of vile
    login    - user name
    password - password
    cmdlist  - list of commands
    Return Value:
    1 at success, 0 at error"""
    if len(cmdlist) == 0:
        return (1)
    if constants.USE_CMD_LOGS:
        for cmd in cmdlist:
            logging.debug(cmd)

    cfg = xmltools.get_config()
    ipstr = cfg["vile"]["ip"]
    portnum = cfg["vile"]["port"]
    login = cfg["vile"]["login"]
    password = cfg["vile"]["password"]

    data = ""
    auth = 0
    cur_cmd = ""

    try:
        starturi = "/"
        uri = "/cmd"
        headersdict = {}
        # OLD:
        headersdict["Host"] = ipstr + ":" + str(portnum)
        headersdict["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0"
        headersdict["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
        headersdict["Accept-Language"] = "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3"
        headersdict["Accept-Encoding"] = "identity" #"gzip, deflate"
        # headersdict["Connection"]     = "keep-alive"
        headersdict["Upgrade-Insecure-Requests"] = "1"
        headersdict["Content-Type"] = "text/plain; charset=utf-8"

        # NEW:
        # headersdict["Host"]            = ipstr + ":" + str(portnum)
        # headersdict["User-Agent"]      = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0"
        # headersdict["Accept"]          = "*/*"
        # headersdict["Connection"]      = "Close"
        # headersdict["Content-Type"]    = "application/x-www-form-urlencoded"
        # headersdict["Upgrade-Insecure-Requests"] = "1"

        conn = _create_connection(ipstr, portnum)
        # conn.request("GET", starturi, None, headersdict)
        cur_cmd = cmdlist[0]
        conn.request("POST", uri, cur_cmd.encode("UTF-8"), headersdict)
        resp = conn.getresponse()
        data = resp.read()
        # logging.error("exec_cmd_list_s(): resp.status=%d" % (resp.status))

        if resp.status != 200 and resp.status != 204 and resp.status != 401:
            conn.close()
            # logging.error("exec_cmd_list_s(): connection closed")
            return 0
        if resp.status == 401:
            auth = 1

        resphdrs = resp.getheaders()
        sessionid = ""
        for hdr in resphdrs:
            if hdr[0] == "set-cookie" or hdr[0] == "Set-Cookie":
                sessionid = hdr[1]
        if sessionid == "":
            conn.close()
            # logging.error("exec_cmd_list_s() sessionid='' : connection closed")
            return 0

        if auth == 1:
            authhdr = resp.getheader("WWW-Authenticate", "")
            realm = tools.extract_value_by_name(authhdr, "realm")
            qop = tools.extract_value_by_name(authhdr, "qop")
            nonce = tools.extract_value_by_name(authhdr, "nonce")
            opaque = tools.extract_value_by_name(authhdr, "opaque")
            # TMP:
            cnonceNum = int(random.random() * 0x7fffffff)
            cnonce = "%0x" % cnonceNum
            nonceCnt = 1
            ha1 = hash1(login, realm, password)
            ha2 = hash2("GET", starturi)
            response = resp_digest_rfc2617(ha1, nonce, nonceCnt, cnonce, qop, ha2)
            authHdrStr = """Digest username="%s", realm="%s", nonce="%s", uri="%s", response="%s", opaque="%s", qop=%s, nc=%08x, cnonce="%s" """ % (
            login, realm, nonce, starturi, response, opaque, qop, nonceCnt, cnonce)
            headersdict["Authorization"] = authHdrStr
            headersdict["Cookie"] = sessionid
            conn.request("GET", starturi, None, headersdict)
            resp = conn.getresponse()
            data = resp.read()
            # logging.error("exec_cmd_list_s(): resp.status=%d" % (resp.status))

        c = 1
        all_cmd_str = ""
        while c < len(cmdlist):
            all_cmd_str += cmdlist[c] + "\n"
            c = c + 1

        if len(all_cmd_str) != 0:
            if auth == 1:
                cnonceNum = int(random.random() * 0x7fffffff)
                cnonce = "%0x" % cnonceNum
                nonceCnt = c + 2
                ha1 = hash1(login, realm, password)
                ha2 = hash2("GET", uri)
                response = resp_digest_rfc2617(ha1, nonce, nonceCnt, cnonce, qop, ha2)
                authHdrStr = """Digest username="%s", realm="%s", nonce="%s", uri="%s", response="%s", opaque="%s", qop=%s, nc=%08x, cnonce="%s" """ % (
                login, realm, nonce, uri, response, opaque, qop, nonceCnt, cnonce)
                headersdict["Authorization"] = authHdrStr
                headersdict["Cookie"] = sessionid
                conn.request("POST", uri, all_cmd_str.encode("UTF-8"), headersdict)
                resp = conn.getresponse()
                data = resp.read()
                # logging.error("exec_cmd_list_s(): resp.status=%d" % (resp.status))
            else:
                headersdict["Cookie"] = sessionid
                conn.request("POST", uri, all_cmd_str.encode("UTF-8"), headersdict)
                resp = conn.getresponse()
                data = resp.read()
                # logging.error("exec_cmd_list_s(): resp.status=%d" % (resp.status))
        conn.close()
        # logging.error("exec_cmd_list_s(): connection closed")
    except http.client.HTTPException as httpex:
        logging.error("function exec_cmd_list_s(): http exception:" + str(httpex))
        logging.error("http.args: " + str(httpex.args))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        logging.error("(try to execute command " + cur_cmd + ")")
        logging.error("cmd_list:" + str(cmdlist) + ")")
        conn.close()
        # logging.error("exec_cmd_list_s(): connection closed")
        return 0
    except OSError as ex:
        logging.error(
            "function get_url_data_s(): OSError exception: errno=" + str(ex.errno) + " , errortext=" + str(ex.strerror))
        logging.error("exception: " + str(ex))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        logging.error("(try to execute command " + cur_cmd + ")")
        logging.error("cmd_list:" + str(cmdlist) + ")")
        conn.close()
        # logging.error("exec_cmd_list_s(): connection closed")
        return 0
    except:
        logging.error("function exec_cmd_list_s(): unknown exception:" + str(sys.exc_info()))
        logging.error("(try to connect to ip=" + ipstr + ", port=" + str(portnum) + ")")
        logging.error("(try to execute command " + cur_cmd + ")")
        logging.error("cmd_list:" + str(cmdlist) + ")")
        conn.close()
        # logging.error("exec_cmd_list_s(): connection closed")
        return 0
    return 1


def get_my_ip():
    if sys.platform == "win32":
        host = socket.gethostname()
        (mainhostname, aliaslist, iplist) = socket.gethostbyname_ex(host)
        if len(iplist) != 0:
            return (iplist[0])
        else:
            return ("")  # error
    else:

        # OLD
        # ipv4 = os.popen("ip addr show eth0").read().split("inet ")[1].split("/")[0]

        try:
            ipv4 = os.popen("ip route").read().split("src ")[1].split()[0]
        except Exception:
            logging.error("get_my_ip: can't ip route")

        return (ipv4)


def update_online_state():
    """This function update in database state of conferences and its abonents."""
    try:
        connstr = get_url_data_s("connections.json", True)
        confstr = get_url_data_s("conferences.json", True)
        configstr = get_url_data_s("config.json", True)

        # NOTE: strings with "'" is cause of syntax error
        connstr = connstr.replace("'", "''")
        confstr = confstr.replace("'", "''")
        configstr = configstr.replace("'", "''")

        online_data_rows = dbtools.load_table("legacy.onlinedata", "id, connections, conferences, config", "")
        if len(online_data_rows) != 0:
            id = online_data_rows[0][0]
            dbtools.make_sql_request(
                "UPDATE legacy.onlinedata SET connections='%s', conferences='%s', config='%s' WHERE id=%d" % (
                    connstr, confstr, configstr, id))
        else:
            id = 1
            dbtools.make_sql_request(
                "INSERT INTO legacy.onlinedata (id, connections, conferences, config) VALUES (%d, '%s', '%s', '%s')" % (
                    id, connstr, confstr, configstr))

        return (connstr)
    except:
        logging.error("function update_online_state(): unknown exception:" + str(sys.exc_info()))
        return ("")

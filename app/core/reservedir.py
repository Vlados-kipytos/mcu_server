#!/usr/bin/python3

import logging
import os
import sys

import app.core.profile_settings as profile_settings
import app.core.tools as tools
import app.core.xmltools as xmltools


def get_net_reserve_disk():
    if sys.platform == "win32":
        return "n:"
    else:
        return "/"


def get_net_reserve_folder():
    """directory to which mount network directory"""
    if sys.platform == "win32":
        return get_net_reserve_disk() + "\\"
    else:
        return tools.get_engine_dir() + "/net"


def is_use_net_folder():
    reserve_db_settings = profile_settings.get_db_settings()
    if reserve_db_settings.backup_place == "remote":
        return True
    else:
        return False


def get_credentials_filename():
    config = xmltools.get_config()
    fname = config["dirs"]["shareddir"] + "/cred.txt"
    return fname


def create_credentials_file(_fname: str, _username: str, _passwd: str):
    """This function creates credentials file with login/passwd for access to network directory."""

    text = "username=%s\npassword=%s\n" % (_username, _passwd)
    data = text.encode("UTF-8")

    f = open(_fname, "wb")
    f.write(data)
    f.close()


def mount_net_dir():
    reserve_db_settings = profile_settings.get_db_settings()
    if reserve_db_settings.backup_place == "local":
        return

    if sys.platform == "win32":
        cmd = """net use %s %s /user:%s %s""" % \
              (get_net_reserve_disk(), reserve_db_settings.shared_dir_url, reserve_db_settings.username,
               reserve_db_settings.password)
        out, err = tools.exec_cmd2(cmd, False)

        logging.error("net use command: " + cmd)
        logging.error("net use stdout: ")
        logging.error(out)
        logging.error("net use stderr: ")
        logging.error(err)
    else:
        if not os.path.exists(get_net_reserve_folder()):
            cmd = "mkdir %s" % (get_net_reserve_folder())
            os.system(cmd)

        cred_fname = get_credentials_filename()
        create_credentials_file(cred_fname, reserve_db_settings.username, reserve_db_settings.password)

        # OLD:
        # cmd = """mount %s %s -o username="%s",password="%s",file_mode=0666,dir_mode=0777""" % \
        #      (reserve_db_settings.shared_dir_url, get_net_reserve_folder(), reserve_db_settings.username, reserve_db_settings.password)

        # NOTE: you need install pack "cifs-utils" first
        cmd = """mount -v -t cifs %s %s -o credentials=%s,file_mode=0666,dir_mode=0777""" % \
              (reserve_db_settings.shared_dir_url, get_net_reserve_folder(), cred_fname)
        out, err = tools.exec_cmd2(cmd, False)

        logging.error("mount command: " + cmd)
        logging.error("mount stdout: ")
        logging.error(out)
        logging.error("mount stderr: ")
        logging.error(err)

        cmd = "rm -f %s" % (cred_fname)
        out, err = tools.exec_cmd2(cmd, False)


def umount_net_dir():
    if sys.platform == "win32":
        cmd = """net use %s /delete""" % (get_net_reserve_disk())
        out, err = tools.exec_cmd2(cmd, False)
    else:
        cmd = """umount %s""" % (get_net_reserve_folder())
        out, err = tools.exec_cmd2(cmd, False)

    logging.error("umount/net use command: " + cmd)
    logging.error("umount/net use stdout: ")
    logging.error(out)
    logging.error("umount/net use stderr: ")
    logging.error(err)


def is_network_dir_mounted():
    if sys.platform == "win32":
        cmd = "WMIC LogicalDisk Get Name"
        out, err = tools.exec_cmd2(cmd, False)

        logging.error("check mount command: " + cmd)
        logging.error("check mount stdout: ")
        logging.error(out)
        logging.error("check mount stderr: ")
        logging.error(err)

        stdout_str = out.decode("UTF-8")

        net_disk_found = False
        stdout_lines = stdout_str.splitlines()
        for line in stdout_lines:
            line_lower = line.lower()
            if line_lower.find(get_net_reserve_disk()) != -1:
                net_disk_found = True
                break

        return net_disk_found
    else:
        cmd = "mount -l"
        out, err = tools.exec_cmd2(cmd, False)
        stdout_str = out.decode("UTF-8")

        logging.error("check mount command: " + cmd)
        logging.error("check mount stdout: ")
        logging.error(out)
        logging.error("check mount stderr: ")
        logging.error(err)

        mount_lines = stdout_str.splitlines()
        net_dir_found = False
        for line in mount_lines:
            if line.find(get_net_reserve_folder()) != -1:
                net_dir_found = True
                break

        return net_dir_found

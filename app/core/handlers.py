import logging
import time

import app.core.abook_export as abook_export
import app.core.abook_import as abook_import
import app.core.addrbook as addrbook
import app.core.callabtools as callabtools
import app.core.calls_settings as calls
import app.core.conftools as conftools
import app.core.constants as constants
import app.core.customlayouttypes as customlayouttypes
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.features as features
import app.core.files as files
import app.core.installtools as installtools
import app.core.layoutbackup as layoutbackup
import app.core.layoutcfgtools as layoutcfgtools
import app.core.layoutcrown as layoutcrown
import app.core.licenses as licenses
import app.core.layouttemplates as layouttemplates
import app.core.metrics_tools as metrics
import app.core.nettools as nettools
import app.core.networksettings as networksettings
import app.core.onlinelayouttools as onlinelayouttools
import app.core.onlinetools as onlinetools
import app.core.pipetools as pipetools
import app.core.powertools as powertools
import app.core.presentation as presentation
import app.core.profile_settings as profile
import app.core.reservedir as reservedir
import app.core.reservetools as reservetools
import app.core.sessionpartctrl as sessionpartctrl
import app.core.sysinfotools as sysinfotools
import app.core.tools as tools
import app.core.utils as utils
import app.core.venginelog as venginelog
import app.core.videorecord as videorecord
from .errors import *
from .requests import *
from .responses import *


def get_audio_config_json(confid):
    """This function returns JSON document with audio configuration of conference confid."""
    confid = int(confid)

    config_rows = dbtools.select(
        """SELECT aac, g711, g726, g728, g722, g7221_24, g7221_32, g7221c_24, g7221c_32, g7221c_48, g729, g7231, g719,
        muteaudio, noise_suppressor, auto_gain_ctrl 
        FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)""" % (
            confid))
    if len(config_rows) != 0:
        audio_codecs = AudioCodecsEntity(True)
        audio_codecs.init_codecs(
            tools.int_to_bool(config_rows[0][0]), # aac
            tools.int_to_bool(config_rows[0][1]), # g711
            tools.int_to_bool(config_rows[0][2]), # g726
            tools.int_to_bool(config_rows[0][3]), # g728
            tools.int_to_bool(config_rows[0][4]), # g722
            tools.int_to_bool(config_rows[0][5]), # g7221_24
            tools.int_to_bool(config_rows[0][6]), # g7221_32
            tools.int_to_bool(config_rows[0][7]), # g7221c_24
            tools.int_to_bool(config_rows[0][8]), # g7221c_32
            tools.int_to_bool(config_rows[0][9]), # g7221c_48
            tools.int_to_bool(config_rows[0][10]), # g729
            tools.int_to_bool(config_rows[0][11]), # g7231
            tools.int_to_bool(config_rows[0][12]) # g719
        )
        muteaudio = tools.int_to_bool(config_rows[0][13])
        noise_suppressor = data.get_noise_suppressor_name(config_rows[0][14])
        auto_gain_ctrl = data.get_auto_gain_ctrl_name(config_rows[0][15])
    else:
        # TODO: logging.error("get_audio_config_json() db error:" + dbtools.get_last_error())
        raise AudioConfigurationNotFoundError()

    acodec_list = audio_codecs.get_codecs_list()

    result = {"allowed_codecs": acodec_list, "muteaudio": muteaudio, "noise_suppressor": noise_suppressor,
              "auto_gain_ctrl": auto_gain_ctrl}

    return (result)


def validate_audio_config(confid, _cfg_json):
    confid = int(confid)

    #if confid != 0:
    #    status = conftools.get_conference_status(confid)
    #    if status["launched"] and _cfg_json != None:
    #        raise ConferenceAlreadyLaunchedError("conference_id")

    if _cfg_json != None and ("allowed_codecs" in _cfg_json) and _cfg_json["allowed_codecs"] != None:
        acodec_list = _cfg_json["allowed_codecs"]
        features_list = features.get_features_list()
        for codec_name in acodec_list:
            features.validate_conf_setting(features_list, codec_name)


def set_audio_config_json(confid, _cfg_json):
    """This function update audio configuration of conference confid."""
    confid = int(confid)

    if _cfg_json == None:
        res = get_audio_config_json(confid)
        return (res)

    old_cfg = get_audio_config_json(confid)

    status = conftools.get_conference_status(confid)
    #if status["launched"] and _cfg_json != None:
    #    raise ConferenceAlreadyLaunchedError("conference_id")

    acodec_list = _cfg_json["allowed_codecs"]

    audio_codecs = AudioCodecsEntity(False)
    audio_codecs.init_by_list(acodec_list)

    aac = tools.bool_to_int(audio_codecs.aac)
    g711 = tools.bool_to_int(audio_codecs.g711)
    g726 = tools.bool_to_int(audio_codecs.g726)
    g728 = tools.bool_to_int(audio_codecs.g728)
    g722 = tools.bool_to_int(audio_codecs.g722)
    g722_1_24 = tools.bool_to_int(audio_codecs.g722_1_24)
    g722_1_32 = tools.bool_to_int(audio_codecs.g722_1_32)
    g722_1c_24 = tools.bool_to_int(audio_codecs.g722_1c_24)
    g722_1c_32 = tools.bool_to_int(audio_codecs.g722_1c_32)
    g722_1c_48 = tools.bool_to_int(audio_codecs.g722_1c_48)
    g729 = tools.bool_to_int(audio_codecs.g729)
    g723_1 = tools.bool_to_int(audio_codecs.g723_1)
    g719 = tools.bool_to_int(audio_codecs.g719)

    if _cfg_json["muteaudio"] != None:
        muteaudio = tools.bool_to_int(_cfg_json["muteaudio"])
    else:
        muteaudio = tools.bool_to_int(old_cfg["muteaudio"])

    if _cfg_json["noise_suppressor"] != None:
        noise_suppressor = data.get_noise_suppressor_ind(_cfg_json["noise_suppressor"])
    else:
        noise_suppressor = data.get_noise_suppressor_ind(old_cfg["noise_suppressor"])

    if _cfg_json["auto_gain_ctrl"] != None:
        auto_gain_ctrl = data.get_auto_gain_ctrl_ind(_cfg_json["auto_gain_ctrl"])
    else:
        auto_gain_ctrl = data.get_auto_gain_ctrl_ind(old_cfg["auto_gain_ctrl"])

    request = """UPDATE legacy.configurations SET aac=%d, g711=%d, g726=%d, g728=%d, g722=%d, g7221_24=%d, g7221_32=%d, 
    g7221c_24=%d, g7221c_32=%d, g7221c_48=%d, g729=%d, g7231=%d, g719=%d, muteaudio=%d, noise_suppressor=%d, 
    auto_gain_ctrl=%d  
    WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)""" % (
        aac, g711, g726, g728, g722, g722_1_24, g722_1_32, g722_1c_24, g722_1c_32, g722_1c_48, g729, g723_1, g719,
        muteaudio, noise_suppressor, auto_gain_ctrl, confid)
    result = dbtools.make_sql_request(request)
    if result == 0:
        logging.error("set_audio_config_json() db error" + dbtools.get_last_error())
        raise UpdateAudioConfigError()

    cmd_list = []

    # update changed audio codecs of launched conference
    if status["launched"] and _cfg_json != None:
        conftools.get_audio_codecs_cmd_list(confid, cmd_list)
        cid = tools.conference_name_by_id(confid)
        cmd_list.append(data.get_noise_suppressor_conf_cmd(noise_suppressor, cid))
        cmd_list.append(data.get_auto_gain_ctrl_conf_cmd(auto_gain_ctrl, cid))

    if len(cmd_list) != 0:
        nettools.exec_cmd_list_s(cmd_list)

    res = get_audio_config_json(confid)
    return (res)


def get_video_config_json(confid):
    """This function returns JSON document with video configuration of conference confid."""
    confid = int(confid)

    config_rows = dbtools.select("""SELECT bitrate, h261, h263, h264, h264high, h265, width, height, fps, render_fully, 
        rec_at_start, rec_autostop, show_aud_pwr, show_names, rec_to_file, use_pres, background, mutevideo 
        FROM legacy.configurations WHERE 
        id IN (SELECT configid FROM legacy.conferences WHERE id=%d)""" % (confid))

    if len(config_rows) != 0:
        bitrate = config_rows[0][0]
        video_codecs = VideoCodecsEntity(True)
        video_codecs.init_codecs(
            tools.int_to_bool(config_rows[0][1]), # h261
            tools.int_to_bool(config_rows[0][2]), # h263
            tools.int_to_bool(config_rows[0][3]), # h264
            tools.int_to_bool(config_rows[0][4]), # h264high
            tools.int_to_bool(config_rows[0][5]) # h265
        )
        width = config_rows[0][6]
        height = config_rows[0][7]
        fps = config_rows[0][8]
        resolution = data.find_resolution_index(width, height, fps)
        render_fully = data.get_render_participant_name(config_rows[0][9])
        rec_at_start = tools.int_to_bool(config_rows[0][10])
        rec_autostop = tools.int_to_bool(config_rows[0][11])
        show_aud_pwr = tools.int_to_bool(config_rows[0][12])
        show_names = tools.int_to_bool(config_rows[0][13])
        rec_to_file = tools.int_to_bool(config_rows[0][14])
        use_pres = tools.int_to_bool(config_rows[0][15])
        background_ind = config_rows[0][16]
        mutevideo = tools.int_to_bool(config_rows[0][17])
    else:
        logging.error("get_video_config_json() db error:" + dbtools.get_last_error())
        raise VideoConfigurationNotFoundError()

    vcodecs_list = video_codecs.get_codecs_list()

    background_name = data.get_background(background_ind)["name"]

    result = {"max_bitrate": bitrate, "allowed_codecs": vcodecs_list, "max_resolution": resolution,
              "show_full_participant": render_fully, "rec_at_start": rec_at_start, "rec_autostop": rec_autostop,
              "show_aud_pwr": show_aud_pwr, "show_names": show_names, "rec_to_file": rec_to_file, "use_pres": use_pres,
              "background": background_name, "mutevideo": mutevideo}

    return (result)


def validate_video_config_json(confid, _cfg_json):
    confid = int(confid)

    if _cfg_json == None:
        return

    if confid != 0:
        status = conftools.get_conference_status(confid)
        b_launched = status["launched"]
    else:
        b_launched = False

    # don't edit video codecs in launched conference
    #if b_launched and ("allowed_codecs" in _cfg_json) and _cfg_json["allowed_codecs"] != None:
    #    raise ConferenceAlreadyLaunchedError("conference_id")

    features_list = features.get_features_list()

    if ("allowed_codecs" in _cfg_json) and _cfg_json["allowed_codecs"] != None:
        vcodecs_list = _cfg_json["allowed_codecs"]
        for codec_name in vcodecs_list:
            features.validate_conf_setting(features_list, codec_name)

    if _cfg_json["use_pres"] != None:
        use_pres = tools.bool_to_int(_cfg_json["use_pres"])
        if use_pres:
            features.validate_conf_setting(features_list, "use_pres")

    # check editing video settings if conference is launched
    b_edit_important_field = _cfg_json["max_bitrate"] != None or \
                             _cfg_json["rec_at_start"] != None or \
                             _cfg_json["rec_autostop"] != None or \
                             _cfg_json["rec_to_file"] != None
    if b_edit_important_field and b_launched:
        raise ConferenceAlreadyLaunchedError("conference_id")



def set_video_config_json(confid, _cfg_json):
    """This function updates video configuration of conference confid."""
    confid = int(confid)

    #features_list = features.get_features_list()

    old_cfg = get_video_config_json(confid)

    if _cfg_json == None:
        return (old_cfg)

    if _cfg_json["max_bitrate"] != None:
        bitrate = _cfg_json["max_bitrate"]
    else:
        bitrate = old_cfg["max_bitrate"]

    # don't edit video codecs in launched conference
    status = conftools.get_conference_status(confid)
    #if status["launched"] and ("allowed_codecs" in _cfg_json) and _cfg_json["allowed_codecs"] != None:
    #    raise ConferenceAlreadyLaunchedError("conference_id")

    if "allowed_codecs" in _cfg_json and _cfg_json["allowed_codecs"] != None:
        vcodecs_list = _cfg_json["allowed_codecs"]
    else:
        vcodecs_list = old_cfg["allowed_codecs"]

    video_codecs = VideoCodecsEntity(False)
    video_codecs.init_by_list(vcodecs_list)

    h261 = tools.bool_to_int(video_codecs.h261)
    h263 = tools.bool_to_int(video_codecs.h263)
    h264 = tools.bool_to_int(video_codecs.h264)
    h264_high = tools.bool_to_int(video_codecs.h264_high)
    h265 = tools.bool_to_int(video_codecs.h265)

    if _cfg_json["max_resolution"] != None:
        res_ind = _cfg_json["max_resolution"]
    else:
        res_ind = old_cfg["max_resolution"]
    resolution = data.get_frame_resolution(res_ind)
    width = resolution["width"]
    height = resolution["height"]
    fps = resolution["fps"]

    if _cfg_json["show_full_participant"] != None:
        show_full = data.get_render_participant_index(_cfg_json["show_full_participant"])
    else:
        show_full = data.get_render_participant_index(old_cfg["show_full_participant"])

    if _cfg_json["rec_at_start"] != None:
        rec_at_start = tools.bool_to_int(_cfg_json["rec_at_start"])
    else:
        rec_at_start = tools.bool_to_int(old_cfg["rec_at_start"])

    if _cfg_json["rec_autostop"] != None:
        rec_autostop = tools.bool_to_int(_cfg_json["rec_autostop"])
    else:
        rec_autostop = tools.bool_to_int(old_cfg["rec_autostop"])

    if _cfg_json["show_aud_pwr"] != None:
        show_aud_pwr = tools.bool_to_int(_cfg_json["show_aud_pwr"])
    else:
        show_aud_pwr = tools.bool_to_int(old_cfg["show_aud_pwr"])

    if _cfg_json["show_names"] != None:
        show_names = tools.bool_to_int(_cfg_json["show_names"])
    else:
        show_names = tools.bool_to_int(old_cfg["show_names"])

    if _cfg_json["rec_to_file"] != None:
        rec_to_file = tools.bool_to_int(_cfg_json["rec_to_file"])
    else:
        rec_to_file = tools.bool_to_int(old_cfg["rec_to_file"])

    if _cfg_json["use_pres"] != None:
        use_pres = tools.bool_to_int(_cfg_json["use_pres"])
        #if use_pres:
        #    features.validate_conf_setting(features_list, "use_pres")
    else:
        use_pres = tools.bool_to_int(old_cfg["use_pres"])

    if _cfg_json["background"] != None:
        background_ind = data.get_background_ind_by_name(_cfg_json["background"])
    else:
        background_ind = data.get_background_ind_by_name(old_cfg["background"])

    if _cfg_json["mutevideo"] != None:
        mutevideo = _cfg_json["mutevideo"]
    else:
        mutevideo = old_cfg["mutevideo"]

    # check editing video settings if conference is launched
    #b_edit_important_field = _cfg_json["max_bitrate"] != None or _cfg_json["max_resolution"] != None or \
    #                         _cfg_json["show_full_participant"] != None or _cfg_json["rec_at_start"] != None or \
    #                         _cfg_json["rec_autostop"] != None or _cfg_json["show_aud_pwr"] != None or \
    #                         _cfg_json["show_names"] != None or _cfg_json["rec_to_file"] != None
    #if b_edit_important_field and status["launched"]:
    #    raise ConferenceAlreadyLaunchedError("conference_id")

    request = """UPDATE legacy.configurations SET bitrate=%d, h261=%d, h263=%d, h264=%d, h264high=%d, h265=%d, width=%d, height=%d, fps=%d, 
                 render_fully=%d, rec_at_start=%d, rec_autostop=%d, show_aud_pwr=%d, show_names=%d, rec_to_file=%d, use_pres=%d,
                 background=%d, mutevideo=%d 
                 WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)""" % (
        bitrate, h261, h263, h264, h264_high, h265, width, height, fps,
        show_full, rec_at_start, rec_autostop, show_aud_pwr, show_names, rec_to_file, use_pres, background_ind,
        mutevideo, confid)
    result = dbtools.make_sql_request(request)
    if result == 0:
        logging.error("set_video_config_json() db error" + dbtools.get_last_error())
        raise UpdateVideoConfigError()

    cmd_list = []

    if status["launched"]:
        # update changed video codecs of launched conference
        if ("allowed_codecs" in _cfg_json) and _cfg_json["allowed_codecs"] != None:
            conftools.get_video_codecs_cmd_list(confid, cmd_list)

        if _cfg_json["use_pres"] != None:
            conftools.get_presentation_cmd(confid, use_pres, cmd_list)

        if _cfg_json["show_names"] != None:
            onlinetools.get_show_names_cmd_list(confid, _cfg_json["show_names"], cmd_list)

        if _cfg_json["show_full_participant"] != None:
            onlinetools.get_render_fully_cmd_list(confid, show_full, cmd_list)

        if _cfg_json["show_aud_pwr"] != None:
            onlinetools.get_show_audio_power_cmd_list(confid, _cfg_json["show_aud_pwr"], cmd_list)

        if _cfg_json["background"] != None:
            onlinetools.get_layout_background_cmd_list(confid, background_ind, cmd_list)

        if _cfg_json["max_resolution"] != None:
            onlinetools.get_resolution_cmd_list(confid, width, height, cmd_list)

    if len(cmd_list) != 0:
        nettools.exec_cmd_list_s(cmd_list)

    res = get_video_config_json(confid)
    return (res)


def create_new_configuration():
    configid = dbtools.generate_unique_fld("legacy.configurations", "id")
    result = dbtools.make_sql_request("""
        INSERT INTO legacy.configurations
               (id, name, aac, g711, g726, g728, g722, g7221_24, g7221_32, g7221c_24, g7221c_32, g7221c_48, g729, g7231, g719,
                muteaudio, noise_suppressor, auto_gain_ctrl,
                bitrate, h261, h263, h264, h264high, h265, ratectrl, adaptrate, fec, width, height, fps, 
                contenttype, aspeaker, asp2lyt, render_fully, rec_at_start, rec_autostop, show_aud_pwr, show_names, rec_to_file, use_pres,
                background, mutevideo)
        VALUES (%d, 'config1', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                0, 2, 2,
                1600, 1, 1, 1, 1, 1, 0, 0, 0, 1920, 1080, 30, 
                0, 0, 0, 0, 0, 0, 1, 0, 1, 1,
                0, 0);
        """ % configid)
    if result == 0:
        return (-1)
    return (configid)


class GetContactListHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetContactListRequest) -> ContactListResponse:
        if request.per_page <= 0 or request.per_page > 200:
            raise BadPageSizeError(request.per_page, "per_page")
        result = addrbook.get_contacts_list(request)
        return result


class GetContactHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetContactRequest) -> ContactResponse:
        result = addrbook.get_contact_by_id(request)
        return result


class CreateContactHandler:

    def __init__(self):
        pass

    async def handle(self, request: CreateContactRequest) -> ContactResponse:
        result = addrbook.create_new_contact(request)
        return result


class ChangeContactHandler:

    def __init__(self):
        pass

    async def handle(self, request: ChangeContactRequest) -> ContactResponse:
        result = addrbook.update_contact(request)
        return result


class DeleteContactHandler:

    def __init__(self):
        pass

    async def handle(self, request: DeleteContactRequest) -> DeleteContactResponse:

        abid = request.contactid

        # get created contact (check existing of contact)
        req = GetContactRequest(abid)
        del_contact = addrbook.get_contact_by_id(req)

        if addrbook.is_abonent_connected(abid):
            raise AlreadyInSessionError("contact_id")

        # if abonent planned in launched conferences (may be not connected to MCU)
        conf_rows = dbtools.select(
            "SELECT id FROM legacy.conferences WHERE state=%d AND id IN (SELECT confid FROM legacy.confabonents WHERE abid=%d)" % (
                data.CONF_STATE_ACTIVE, abid))
        if len(conf_rows) != 0:
            raise AbonentInLaunchedConference(abid, "contact_id")

        res_err = addrbook.delete_abonent(abid)
        if res_err != "":
            raise DeleteContactError(res_err)

        result = DeleteContactResponse(abid)
        return result


class GetConferenceHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetConferenceRequest) -> ConferenceResponse:
        conf_list_request = GetConferencesListRequest(0, 1, "created_at", "asc", "", "", None)
        conf_entity_list, size = conftools.get_conferences(conf_list_request, request.confid, request.confstr)
        return ConferenceResponse(conf_entity_list[0])


class GetConferencesListHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetConferencesListRequest) -> ConferencesListResponse:
        conf_entity_list, size = conftools.get_conferences(request, 0, "")
        return ConferencesListResponse(conf_entity_list, size)


class CreateConferenceHandler:
    def __init__(self):
        pass

    async def handle(self, request: CreateConferenceRequest) -> ConferenceResponse:

        pin_str = request.pin.strip()
        if request.usepin and conftools.is_pin_exist(pin_str, 0):
            raise PinCodeNotUniqueError(pin_str, "pin")

        if request.audio != None:
            validate_audio_config(0, request.audio)

        if request.video != None:
            validate_video_config_json(0, request.video)

        db_name = tools.str_to_dbstr(request.name.strip())

        infinity_num = tools.bool_to_int(request.infinity)
        usepin_num = tools.bool_to_int(request.usepin)
        autocall_num = tools.bool_to_int(request.autocall)

        # convert datetime from format 'YYYY-MM-DDThh:mm:ssZ'
        start_dt = tools.json_date_time_to_db(0)
        end_dt = tools.json_date_time_to_db(0)
        last_start_dt = tools.json_date_time_to_db(0)

        auto_start_mode = data.get_auto_start_mode_index(request.auto_start.auto_start_mode)
        auto_start_week_day = request.auto_start.auto_start_week_day
        auto_start_month_day = request.auto_start.auto_start_month_day
        auto_start_week_first = request.auto_start.first_week_day
        auto_start_week_last = request.auto_start.last_week_day
        time_range_list = request.auto_start.time_range_list

        repeate_mode = data.get_repeate_mode_index(request.repeate.repeate_mode)
        repeate_count = request.repeate.repeate_count
        repeate_last_time = tools.json_date_time_to_db(request.repeate.repeate_last_time)

        stop_time_table_mode = data.get_stop_time_table_index(request.stop_time_table.stop_time_table_mode)
        stop_time_table_begin = tools.json_date_time_to_db(request.stop_time_table.stop_time_table_begin)
        stop_time_table_end = tools.json_date_time_to_db(request.stop_time_table.stop_time_table_end)

        stop_mode = data.get_auto_stop_mode_index(request.auto_stop.stop_mode)
        stop_duration = request.auto_stop.stop_duration
        stop_abs_time = tools.json_date_time_to_db(request.auto_stop.stop_abs_time)

        configid = create_new_configuration()
        if configid <= 0:
            raise CreateConferenceConfigurationError()

        confid = dbtools.generate_unique_fld("legacy.conferences", "id")

        # tmp_pin_num    = confid % 10000
        # unique_pin_str = "%04d" % (tmp_pin_num)

        req_conf = """INSERT INTO legacy.conferences (id, name, starttime, endtime, last_start_time, state, 
                                               vmid, invite, enabled, maxab, pin, 
                                               usepin, infinity, fst_ab, width, height, 
                                               templ, configid, autocall, callcount, rtmp_ab_id, 
                                               rtmp_url,
                                               start_mode, start_week_day, start_month_day, start_wk_first, start_wk_last,
                                               repeate_mode, repeate_count, repeate_last_time,
                                               stop_time_table_mode, stop_time_table_begin, stop_time_table_end,
                                               stop_mode, stop_duration, stop_abs_time)
                                       VALUES (%d, '%s', '%s', '%s', '%s', 0, 
                                               0, 0, 1, %d, '%s', 
                                               %d, %d, '', %d, %d, 
                                               0, %d, %d, %d, %d, 
                                              '%s',
                                              %d, %d, %d, %d, %d,   %d, %d, '%s',    %d, '%s', '%s',    %d, %d, '%s');
                                        """ % (
            confid, db_name, start_dt, end_dt, last_start_dt, request.max_participants, pin_str, usepin_num,
            infinity_num, 1920, 1080, configid, autocall_num, constants.MAX_AUTOCALLS, request.record_participant,
            request.rtmp,
            auto_start_mode, auto_start_week_day, auto_start_month_day, auto_start_week_first, auto_start_week_last,
            repeate_mode, repeate_count, repeate_last_time,
            stop_time_table_mode, stop_time_table_begin, stop_time_table_end,
            stop_mode, stop_duration, stop_abs_time
        )

        res = dbtools.make_sql_request(req_conf)
        if res == 0:
            logging.error("cannot create new conference: %s " % (dbtools.get_last_error()))
            raise CreateConfereneceError()

        if request.audio != None:
            set_audio_config_json(confid, request.audio)

        if request.video != None:
            set_video_config_json(confid, request.video)

        conftools.add_time_ranges_to_conference(confid, time_range_list)

        # get created conference
        req = GetConferenceRequest(confid, "")
        handler = GetConferenceHandler()
        resp = await handler.handle(req)

        return (resp)


def delete_conference(confid):
    """This function deletes conference 'confid'.
    At error returns error message, else empty string."""
    confid = int(confid)

    if confid != 0:
        res2 = dbtools.make_sql_request("DELETE from legacy.outgoingcalls WHERE confid=%d" % (confid))
        if res2 == 0:
            raise DeleteConfereneceError("Error: cannot remove binding outgoingcalls and conferences")

        # remove abonents from conference
        res2 = dbtools.make_sql_request("DELETE from legacy.confabonents WHERE confid=%d" % (confid))
        if res2 == 0:
            raise DeleteConfereneceError("Error: cannot remove abonents from deleting conference")

        # remove binding backup of layout and conference
        res2 = dbtools.make_sql_request("""DELETE FROM legacy.layoutbackup WHERE layout_id IN 
                                          (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d)""" % (confid))
        if res2 == 0:
            raise DeleteConfereneceError("Error: cannot clear layoutbackup table for deleting conference")

        res2 = dbtools.make_sql_request(
            "DELETE FROM legacy.layoutsprmbackup WHERE id IN (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d)" % (
                confid))
        if res2 == 0:
            raise DeleteConfereneceError("Error: cannot clear layoutsprmbackup table for deleting conference")

        res2 = dbtools.make_sql_request("DELETE FROM legacy.layoutconfbackup WHERE confid=%d" % (confid))
        if res2 == 0:
            raise DeleteConfereneceError("Error: cannot clear layoutconfbackup table for deleting conference")

        lyt_id_list_str = layoutcfgtools.get_layouts_id_list_str(confid)

        # remove binding layout and conference
        res2 = dbtools.make_sql_request(
            "DELETE FROM legacy.layoutconfigs WHERE layout_id IN (SELECT id FROM legacy.layoutconferences WHERE confid=%d)" % (
                confid))
        if res2 == 0:
            raise DeleteConfereneceError("Error: cannot clear layoutconfigs table for deleting conference")

        res2 = dbtools.make_sql_request("DELETE from legacy.layoutconferences WHERE confid=%d" % (confid))
        if res2 == 0:
            raise DeleteConfereneceError("Error: cannot clear layoutconferences table for deleting conference")

        if len(lyt_id_list_str) != 0:
            res2 = dbtools.make_sql_request("DELETE FROM legacy.layouts WHERE id IN %s" % (lyt_id_list_str))
            if res2 == 0:
                raise DeleteConfereneceError("Error: cannot clear layouts table for deleting conference")

        # remove binding configuration and conference
        res4 = dbtools.make_sql_request(
            """DELETE FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)""" % (
                confid))
        if res4 == 0:
            raise DeleteConfereneceError("Error: cannot delete binding configuration and conference.")

        #OLD:
        #res3 = dbtools.make_sql_request("""DELETE FROM legacy.conferences WHERE id=%d""" % (confid))
        #if res3 == 0:
        #    raise DeleteConfereneceError("Error: cannot delete conference.")

        dt_now_str = tools.json_date_time_to_db(time.time())
        res3 = dbtools.make_sql_request("UPDATE legacy.conferences SET deleted_at='%s' WHERE id=%d" %
                                        (dt_now_str, confid))
        if res3 == 0:
            raise DeleteConfereneceError("Error: cannot mark conference as deleted.")


class DeleteConferenceHandler:
    def __init__(self):
        pass

    async def handle(self, request: DeleteConferenceRequest) -> DeleteConferenceResponse:

        confid = request.confid

        state_rows = dbtools.select("SELECT state FROM legacy.conferences WHERE id=%d and deleted_at is null" %
                                    (confid))
        if len(state_rows) != 0:
            state = state_rows[0][0]
            if state != data.CONF_STATE_PRE_INACTIVE and state != data.CONF_STATE_POST_INACTIVE:
                raise DeleteActiveConfereneceError("conference_id")
        else:
            raise ConferenceNotFoundError(request.confid, "conference_id")

        delete_conference(confid)

        return (DeleteConferenceResponse(confid))


class ChangeConferenceHandler:
    def __init__(self):
        pass

    async def handle(self, request: ChangeConferenceRequest) -> ConferenceResponse:
        id = request.conference.confid

        if request.conference.audio is not None:
            validate_audio_config(id, request.conference.audio)

        if request.conference.video is not None:
            validate_video_config_json(id, request.conference.video)

        # get old conference (for update)
        req = GetConferenceRequest(id, "")
        handler = GetConferenceHandler()
        old_conf = await handler.handle(req)

        if request.conference.name is not None:
            name = request.conference.name.strip()
        else:
            name = old_conf.conf.name.strip()

        if request.conference.usepin is not None:
            usepin = request.conference.usepin
        else:
            usepin = old_conf.conf.usepin

        if request.conference.pin is not None:
            pin = request.conference.pin.strip()
            if usepin and conftools.is_pin_exist(pin, id):
                raise PinCodeNotUniqueError(pin, "pin")
        else:
            pin = old_conf.conf.pin.strip()

        if request.conference.infinity is not None:
            infinity = request.conference.infinity
        else:
            infinity = old_conf.conf.infinity

        if request.conference.auto_start.time_range_list is not None:
            time_range_list = request.conference.auto_start.time_range_list
        else:
            time_range_list = old_conf.conf.auto_start.time_range_list

        if request.conference.auto_start.auto_start_mode is not None:
            auto_start_mode = data.get_auto_start_mode_index(request.conference.auto_start.auto_start_mode)
        else:
            auto_start_mode = data.get_auto_start_mode_index(old_conf.conf.auto_start.auto_start_mode)

        if request.conference.auto_start.auto_start_week_day is not None:
            auto_start_week_day = request.conference.auto_start.auto_start_week_day
        else:
            auto_start_week_day = old_conf.conf.auto_start.auto_start_week_day

        if request.conference.auto_start.auto_start_month_day is not None:
            auto_start_month_day = request.conference.auto_start.auto_start_month_day
        else:
            auto_start_month_day = old_conf.conf.auto_start.auto_start_month_day

        if request.conference.auto_start.first_week_day is not None:
            first_week_day = request.conference.auto_start.first_week_day
        else:
            first_week_day = old_conf.conf.auto_start.first_week_day

        if request.conference.auto_start.last_week_day is not None:
            last_week_day = request.conference.auto_start.last_week_day
        else:
            last_week_day = old_conf.conf.auto_start.last_week_day

        if request.conference.repeate.repeate_mode is not None:
            repeate_mode = data.get_repeate_mode_index(request.conference.repeate.repeate_mode)
        else:
            repeate_mode = data.get_repeate_mode_index(old_conf.conf.repeate.repeate_mode)

        if request.conference.repeate.repeate_count is not None:
            repeate_count = request.conference.repeate.repeate_count
        else:
            repeate_count = old_conf.conf.repeate.repeate_count

        if request.conference.repeate.repeate_last_time is not None:
            repeate_last_time = tools.json_date_time_to_db(request.conference.repeate.repeate_last_time)
        else:
            repeate_last_time = tools.json_date_time_to_db(old_conf.conf.repeate.repeate_last_time)

        if request.conference.stop_time_table.stop_time_table_mode is not None:
            stop_time_table_mode = data.get_stop_time_table_index(request.conference.stop_time_table.stop_time_table_mode)
        else:
            stop_time_table_mode = data.get_stop_time_table_index(old_conf.conf.stop_time_table.stop_time_table_mode)

        if request.conference.stop_time_table.stop_time_table_begin is not None:
            stop_time_table_begin = tools.json_date_time_to_db(request.conference.stop_time_table.stop_time_table_begin)
        else:
            stop_time_table_begin = tools.json_date_time_to_db(old_conf.conf.stop_time_table.stop_time_table_begin)

        if request.conference.stop_time_table.stop_time_table_end is not None:
            stop_time_table_end = tools.json_date_time_to_db(request.conference.stop_time_table.stop_time_table_end)
        else:
            stop_time_table_end = tools.json_date_time_to_db(old_conf.conf.stop_time_table.stop_time_table_end)

        if request.conference.auto_stop.stop_mode is not None:
            stop_mode = data.get_auto_stop_mode_index(request.conference.auto_stop.stop_mode)
        else:
            stop_mode = data.get_auto_stop_mode_index(old_conf.conf.auto_stop.stop_mode)

        if request.conference.auto_stop.stop_duration is not None:
            stop_duration = request.conference.auto_stop.stop_duration
        else:
            stop_duration = old_conf.conf.auto_stop.stop_duration

        if request.conference.auto_stop.stop_abs_time is not None:
            stop_abs_time = tools.json_date_time_to_db(request.conference.auto_stop.stop_abs_time)
        else:
            stop_abs_time = tools.json_date_time_to_db(old_conf.conf.auto_stop.stop_abs_time)

        if request.conference.max_participants is not None:
            max_abonents = request.conference.max_participants
        else:
            max_abonents = old_conf.conf.max_participants

        if request.conference.autocall is not None:
            autocall = request.conference.autocall
        else:
            autocall = old_conf.conf.autocall

        if request.conference.rtmp is not None:
            rtmp = request.conference.rtmp
        else:
            rtmp = old_conf.conf.rtmp

        if request.conference.record_participant is not None:
            record_abid = request.conference.record_participant
        else:
            record_abid = old_conf.conf.record_participant

        infinity_num = tools.bool_to_int(infinity)
        usepin_num = tools.bool_to_int(usepin)
        autocall_num = tools.bool_to_int(autocall)

        # convert datetime from format 'YYYY-MM-DDThh:mm:ssZ'
        start_dt = tools.json_date_time_to_db(0)
        end_dt = tools.json_date_time_to_db(0)

        # check editing critical params of conference if it is launched
        change_important_field = autocall != old_conf.conf.autocall or \
                                 rtmp != old_conf.conf.rtmp or record_abid != old_conf.conf.record_participant
        status = conftools.get_conference_status(id)
        if change_important_field and status["launched"]:
            raise ConferenceAlreadyLaunchedError("conference_id")

        if status["launched"]:
            if request.conference.layout_mode is not None:
                raise ConferenceAlreadyLaunchedError("conference_id")

        # update conference
        db_name = tools.str_to_dbstr(name)
        sql = """UPDATE legacy.conferences SET name='%s', starttime='%s', endtime='%s', maxab=%d, pin='%s', 
                     usepin=%d, infinity=%d, autocall=%d, callcount=%d, rtmp_ab_id=%d, 
                     rtmp_url='%s',
                     start_mode=%d, start_week_day=%d, start_month_day=%d, start_wk_first=%d, start_wk_last=%d,
                     repeate_mode=%d, repeate_count=%d, repeate_last_time='%s',
                     stop_time_table_mode=%d, stop_time_table_begin='%s', stop_time_table_end='%s',
                     stop_mode=%d, stop_duration=%d, stop_abs_time='%s'
                     WHERE id=%d
                  """ % (db_name, start_dt, end_dt, max_abonents, pin,
                         usepin_num, infinity_num, autocall_num, constants.MAX_AUTOCALLS, record_abid,
                         rtmp,
                         auto_start_mode, auto_start_week_day, auto_start_month_day, first_week_day, last_week_day,
                         repeate_mode, repeate_count, repeate_last_time,
                         stop_time_table_mode, stop_time_table_begin, stop_time_table_end,
                         stop_mode, stop_duration, stop_abs_time,
                         id)

        res = dbtools.make_sql_request(sql)
        if res == 0:
            logging.error("cannot update conference: %s " % (dbtools.get_last_error()))
            raise ChangeConfereneceError()

        # update layout mode if need
        layoutcfgtools.switch_layout_mode(id, request.conference.layout_mode)

        # update audio params
        if request.conference.audio is not None:
            set_audio_config_json(id, request.conference.audio)

        # update video params
        if request.conference.video is not None:
            set_video_config_json(id, request.conference.video)

        conftools.delete_time_ranges_from_conference(id)
        conftools.add_time_ranges_to_conference(id, time_range_list)

        # get updated conference
        req = GetConferenceRequest(id, "")
        handler = GetConferenceHandler()
        resp = await handler.handle(req)
        return (resp)


class GetConferenceParticipantHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetConferenceParticipantRequest) -> ConferenceParticipantResponse:
        # check existing of conference
        if conftools.is_conf_deleted(request.confid):
            raise ConferenceNotFoundError(request.confid, "conference_id")

        confab_rows = dbtools.select("""SELECT useaudio, usevideo, callab, sndvol, useaudio_tx, sndvol_tx, usevideo_tx, 
        use_pres, render_mode, protocol, noise_suppressor, auto_gain_ctrl 
        FROM legacy.confabonents WHERE confid=%d AND abid=%d""" % (request.confid, request.contactid))
        if len(confab_rows) != 0:
            useaudio = confab_rows[0][0]
            usevideo = confab_rows[0][1]
            callab = confab_rows[0][2]
            volume = confab_rows[0][3]
            use_audio_tx = confab_rows[0][4]
            volume_tx = confab_rows[0][5]
            use_video_tx = confab_rows[0][6]
            use_pres = confab_rows[0][7]
            render_mode = data.get_render_participant_name(confab_rows[0][8])
            call_protocol = data.get_call_protocol_name(confab_rows[0][9])
            noise_suppressor = data.get_noise_suppressor_name(confab_rows[0][10])
            auto_gain_ctrl = data.get_auto_gain_ctrl_name(confab_rows[0][11])

            b_useaudio = tools.int_to_bool(useaudio)
            b_usevideo = tools.int_to_bool(usevideo)
            b_callab = tools.int_to_bool(callab)
            b_use_audio_tx = tools.int_to_bool(use_audio_tx)
            b_use_video_tx = tools.int_to_bool(use_video_tx)
            b_use_pres = tools.int_to_bool(use_pres)

            req = GetContactRequest(request.contactid)
            handler = GetContactHandler()
            contact_result = await handler.handle(req)

            part_entity = ParticipantEntity(request.contactid, b_useaudio, b_usevideo, b_callab, volume, b_use_audio_tx,
                                            volume_tx, b_use_video_tx, b_use_pres, contact_result.contact, render_mode,
                                            call_protocol, noise_suppressor, auto_gain_ctrl)

            response = ConferenceParticipantResponse(part_entity)

            return (response)
        else:
            raise GetConferenceParticipantError()


class GetConferenceParticipantsListHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetConferenceParticipantsListRequest) -> ConferenceParticipantsListResponse:
        # check existing of conference
        if conftools.is_conf_deleted(request.confid):
            raise ConferenceNotFoundError(request.confid, "conference_id")

        ab_list = []
        confab_rows = dbtools.select("""SELECT abid, useaudio, usevideo, callab, sndvol, useaudio_tx, sndvol_tx, 
        usevideo_tx, use_pres, render_mode, protocol, noise_suppressor, auto_gain_ctrl
        FROM legacy.confabonents WHERE confid=%d ORDER BY abid""" % (request.confid))
        for i in range(len(confab_rows)):
            abid = confab_rows[i][0]
            useaudio = confab_rows[i][1]
            usevideo = confab_rows[i][2]
            callab = confab_rows[i][3]
            volume = confab_rows[i][4]
            use_audio_tx = confab_rows[i][5]
            volume_tx = confab_rows[i][6]
            use_video_tx = confab_rows[i][7]
            use_pres = confab_rows[i][8]
            render_mode = data.get_render_participant_name(confab_rows[i][9])
            call_protocol = data.get_call_protocol_name(confab_rows[i][10])
            noise_suppressor = data.get_noise_suppressor_name(confab_rows[i][11])
            auto_gain_ctrl = data.get_auto_gain_ctrl_name(confab_rows[i][12])

            b_useaudio = tools.int_to_bool(useaudio)
            b_usevideo = tools.int_to_bool(usevideo)
            b_callab = tools.int_to_bool(callab)
            b_use_audio_tx = tools.int_to_bool(use_audio_tx)
            b_use_video_tx = tools.int_to_bool(use_video_tx)
            b_use_pres = tools.int_to_bool(use_pres)

            req = GetContactRequest(abid)
            handler = GetContactHandler()
            contact_result = await handler.handle(req)

            part_entity = ParticipantEntity(abid, b_useaudio, b_usevideo, b_callab, volume, b_use_audio_tx, volume_tx,
                                            b_use_video_tx, b_use_pres, contact_result.contact, render_mode,
                                            call_protocol, noise_suppressor, auto_gain_ctrl)
            ab_list.append(part_entity)

        response = ConferenceParticipantsListResponse(ab_list)
        return (response)


class CreateConferenceParticipantHandler:

    def __init__(self):
        pass

    async def handle(self, request: CreateConferenceParticipantRequest) -> ConferenceParticipantResponse:
        # check existing of conference
        if conftools.is_conf_deleted(request.confid):
            raise ConferenceNotFoundError(request.confid, "conference_id")

        confid = request.confid
        id = request.contactid
        use_audio = request.use_audio
        use_video = request.use_video
        call_ab = request.call_ab
        volume = request.volume
        use_audio_tx = request.use_audio_tx
        volume_tx = request.volume_tx
        use_video_tx = request.use_video_tx
        use_pres = request.use_pres

        if not conftools.can_add_participants(confid):
            raise MaxConferenceParticipantsError()

        use_audio_num = tools.bool_to_int(use_audio)
        use_video_num = tools.bool_to_int(use_video)
        use_audio_tx_num = tools.bool_to_int(use_audio_tx)
        call_ab_num = tools.bool_to_int(call_ab)
        use_video_tx_num = tools.bool_to_int(use_video_tx)
        use_pres_num = tools.bool_to_int(use_pres)
        render_mode_ind = data.get_render_participant_index(request.render_mode)
        call_protocol_ind = data.get_call_protocol_ind(request.call_protocol)
        noise_suppressor_ind = data.get_noise_suppressor_ind(request.noise_suppressor)
        auto_gain_ctrl = data.get_auto_gain_ctrl_ind(request.auto_gain_ctrl)

        # check call protocol
        ab = addrbook.get_abonent_params(request.contactid)
        if ab["success"]:
            ok = data.is_protocol_correspond_any_addr(call_protocol_ind, ab["sip"], ab["h323"], ab["rtsp"], ab["file"])
            if not ok:
                raise BadCallProtocolError()

        sql = """INSERT INTO legacy.confabonents (confid, abid, useaudio, usevideo, callab, sndvol, useaudio_tx, sndvol_tx, 
            usevideo_tx, use_pres, render_mode, protocol, noise_suppressor, auto_gain_ctrl) 
            VALUES (%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)""" % (
            confid, id, use_audio_num, use_video_num, call_ab_num, volume, use_audio_tx_num, volume_tx,
            use_video_tx_num, use_pres_num, render_mode_ind, call_protocol_ind, noise_suppressor_ind, auto_gain_ctrl)
        result = dbtools.make_sql_request(sql)
        if result != 0:
            req = GetConferenceParticipantRequest(confid, id)
            handler = GetConferenceParticipantHandler()
            partic_result = await handler.handle(req)
            return (partic_result)
        else:
            logging.error("error at adding abonent to conference: " + dbtools.get_last_error())
            raise CreateConferenceParticipantError()


class DeleteConferenceParticipantHandler:

    def __init__(self):
        pass

    async def handle(self, request: DeleteConferenceParticipantRequest) -> DeleteConferenceParticipantResponse:
        confid = request.confid
        abid = request.contactid

        # check existing of conference
        if conftools.is_conf_deleted(request.confid):
            raise ConferenceNotFoundError(request.confid, "conference_id")

        # check existing of participant in conference
        conf_part_rows = dbtools.select(
            "SELECT abid FROM legacy.confabonents WHERE confid=%d and abid=%d" % (confid, abid))
        if len(conf_part_rows) == 0:
            raise ConferenceParticipantNotFoundError(confid, abid, "participant_id")

        sql = "DELETE FROM legacy.confabonents WHERE confid=%d and abid=%d" % (confid, abid)
        result = dbtools.make_sql_request(sql)
        if result == 0:
            logging.error("error at deleting abonent from conference: " + dbtools.get_last_error())
            raise DeleteConferenceParticipantError()

        layout_id = layoutcfgtools.get_individual_layout_id(confid, abid, False)
        if layout_id != 0:
            # OLD: dbtools.make_sql_request("DELETE FROM layoutabonents WHERE layout_id=%d" % (layout_id))
            dbtools.make_sql_request("DELETE FROM legacy.layoutconfigs WHERE layout_id=%d" % (layout_id))
            dbtools.make_sql_request("DELETE FROM legacy.layoutconferences WHERE id=%s" % (layout_id))
            dbtools.make_sql_request("DELETE FROM legacy.layouts WHERE id=%d" % (layout_id))

            db_err = dbtools.get_last_error()
            if db_err != "":
                logging.error("error at deleting abonent from conference: " + db_err)
                raise DeleteLayoutFromConferenceError()

        sql_request = "UPDATE legacy.layoutconfigs SET abid=0, role=%d WHERE abid=%d AND layout_id IN (SELECT id FROM legacy.layoutconferences WHERE confid=%d)" % (
            layoutcfgtools.AUTO_ROLE_ID, abid, request.confid)
        result = dbtools.make_sql_request(sql_request)
        if result == 0:
            logging.error("error at deleting abonent layouts of conference: " + dbtools.get_last_error())
            raise DeleteLayoutFromConferenceError()

        # reset recording participant if delete it from conference
        dbtools.make_sql_request("UPDATE legacy.conferences SET rtmp_ab_id=0 WHERE id=%d AND rtmp_ab_id=%d" % (
            request.confid, request.contactid))

        response = DeleteConferenceParticipantResponse(confid, abid)
        return (response)


class ChangeConferenceParticipantHandler:

    def __init__(self):
        pass

    async def handle(self, request: ChangeConferenceParticipantRequest) -> ConferenceParticipantResponse:
        pass
        confid = request.confid
        abid = request.contactid

        # get old participant params
        req = GetConferenceParticipantRequest(confid, abid)
        handler = GetConferenceParticipantHandler()
        partic_response = await handler.handle(req)
        old_partic_entity = partic_response.participant

        if request.use_audio != None:
            use_audio = request.use_audio
        else:
            use_audio = old_partic_entity.useaudio

        if request.use_video != None:
            use_video = request.use_video
        else:
            use_video = old_partic_entity.usevideo

        if request.call_ab != None:
            call_ab = request.call_ab
        else:
            call_ab = old_partic_entity.callab

        if request.volume != None:
            volume = request.volume
        else:
            volume = old_partic_entity.volume

        if request.use_audio_tx != None:
            use_audio_tx = request.use_audio_tx
        else:
            use_audio_tx = old_partic_entity.use_audio_tx

        if request.volume_tx != None:
            volume_tx = request.volume_tx
        else:
            volume_tx = old_partic_entity.volume_tx

        if request.use_video_tx != None:
            use_video_tx = request.use_video_tx
        else:
            use_video_tx = old_partic_entity.use_video_tx

        if request.use_pres != None:
            use_pres = request.use_pres
        else:
            use_pres = old_partic_entity.use_pres

        if request.render_mode != None:
            render_mode_ind = data.get_render_participant_index(request.render_mode)
        else:
            render_mode_ind = data.get_render_participant_index(old_partic_entity.render_mode)

        if request.call_protocol != None:
            call_protocol_ind = data.get_call_protocol_ind(request.call_protocol)
        else:
            call_protocol_ind = data.get_call_protocol_ind(old_partic_entity.call_protocol)

        if request.noise_suppressor != None:
            noise_suppressor_ind = data.get_noise_suppressor_ind(request.noise_suppressor)
        else:
            noise_suppressor_ind = data.get_noise_suppressor_ind(old_partic_entity.noise_suppressor)

        if request.auto_gain_ctrl != None:
            auto_gain_ctrl_ind = data.get_auto_gain_ctrl_ind(request.auto_gain_ctrl)
        else:
            auto_gain_ctrl_ind = data.get_auto_gain_ctrl_ind(old_partic_entity.auto_gain_ctrl)

        # check call protocol
        ab = addrbook.get_abonent_params(request.contactid)
        if ab["success"]:
            ok = data.is_protocol_correspond_any_addr(call_protocol_ind, ab["sip"], ab["h323"], ab["rtsp"], ab["file"])
            if not ok:
                raise BadCallProtocolError()

        use_audio_num = tools.bool_to_int(use_audio)
        use_video_num = tools.bool_to_int(use_video)
        call_ab_num = tools.bool_to_int(call_ab)
        use_audio_tx_num = tools.bool_to_int(use_audio_tx)
        use_video_tx_num = tools.bool_to_int(use_video_tx)
        use_pres_num = tools.bool_to_int(use_pres)

        sql = """UPDATE legacy.confabonents SET useaudio=%d, usevideo=%d, callab=%d, sndvol=%d, useaudio_tx=%d, 
        sndvol_tx=%d, usevideo_tx=%d, use_pres=%d, render_mode=%d, protocol=%d, noise_suppressor=%d, auto_gain_ctrl=%d
        WHERE confid=%d AND abid=%d""" % (
            use_audio_num, use_video_num, call_ab_num, volume, use_audio_tx_num, volume_tx, use_video_tx_num,
            use_pres_num, render_mode_ind, call_protocol_ind, noise_suppressor_ind, auto_gain_ctrl_ind, confid, abid)
        result = dbtools.make_sql_request(sql)
        if result != 0:
            # get updated participant params
            req = GetConferenceParticipantRequest(confid, abid)
            handler = GetConferenceParticipantHandler()
            partic_response = await handler.handle(req)
            return (partic_response)
        else:
            logging.error("error at updating abonent in conference: " + dbtools.get_last_error())
            raise ChangeConferenceParticipantError()


class GetConferenceSessionParticipantsListHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetConferenceSessionParticipantsListRequest) -> SessionParticipantsListResponse:
        # get online state from buffer (in database)
        connstr, confstr = onlinetools.get_online_state()

        ab_names_dict = addrbook.get_all_abonent_names_dict()

        # get abonents list
        ab_list = onlinetools.get_active_abonents_json(request.confid, connstr, confstr, None, False, ab_names_dict)

        # connecting abonents list
        conn_ab_list = onlinetools.get_connecting_participants(request.confid, connstr, ab_names_dict)

        # disconnected abonents list
        disconnected_ab_list = onlinetools.get_disconnected_abonents(request.confid, ab_list, ab_names_dict)

        return (SessionParticipantsListResponse(ab_list, conn_ab_list, disconnected_ab_list))


class GetConferenceSessionParticipantHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetConferenceSessionParticipantRequest) -> SessionParticipantResponse:
        # get online state from buffer (in database)
        connstr, confstr = onlinetools.get_online_state()

        # get abonents list
        ab_list = onlinetools.get_active_abonents_json(request.confid, connstr, confstr,
                                                       request.participant_id, request.use_conn_id, {})
        if len(ab_list) == 0:
            raise SessionAbonentNotFoundError(str(request.participant_id), "participant_id")

        return (SessionParticipantResponse(ab_list[0]))


class StartConferenceSessionHandler:

    def __init__(self):
        pass

    async def handle(self, request: StartConferenceSessionRequest) -> StartConferenceSessionResponse:
        started_confid = conftools.start_conference(request.confid)
        return (StartConferenceSessionResponse(started_confid))


class StopConferenceSessionHandler:

    def __init__(self):
        pass

    async def handle(self, request: StopConferenceSessionRequest) -> StopConferenceSessionResponse:
        stopped_confid = conftools.stop_conference(request.confid)
        return (StopConferenceSessionResponse(stopped_confid))


class InviteSessionParticipantHandler:

    def __init__(self):
        pass

    async def handle(self, request: InviteSessionParticipantRequest) -> InviteSessionParticipantResponse:
        callabtools.call_abonent_by_uri(request.session_addr.conference_id, request.session_addr.participant_uri)

        addr = SessionParticipantAddressEntity(request.session_addr.conference_id, request.session_addr.participant_uri)

        return (InviteSessionParticipantResponse(addr))


class InviteConferenceParticipantHandler:

    def __init__(self):
        pass

    async def handle(self, request: InviteConferenceParticipantRequest) -> SessionParticipantControlResponse:
        callabtools.call_abonent_by_id(request.conference_addr.conference_id, request.conference_addr.participant_id)
        return SessionParticipantControlResponse(request.conference_addr.conference_id,
                                                 request.conference_addr.participant_id)


class HangupSessionParticipantHandler:

    def __init__(self):
        pass

    async def handle(self, request: HangupSessionParticipantRequest) -> HangupSessionParticipantResponse:
        callabtools.hangup_abonent(request.session_addr.conference_id, request.session_addr.participant_uri)

        addr = SessionParticipantAddressEntity(request.session_addr.conference_id, request.session_addr.participant_uri)

        return (HangupSessionParticipantResponse(addr))


class HangupConferenceParticipantHandler:

    def __init__(self):
        pass

    async def handle(self, request: HangupConferenceParticipantRequest) -> SessionParticipantControlResponse:
        if request.use_connection_id:
            callabtools.hangup_abonent_by_conn_id(request.conference_addr.conference_id,
                                                  request.conference_addr.participant_id)
        else:
            callabtools.hangup_abonent_by_id(request.conference_addr.conference_id,
                                             request.conference_addr.participant_id)
        return SessionParticipantControlResponse(request.conference_addr.conference_id,
                                                 request.conference_addr.participant_id)


class GetOnlineLayoutHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetOnlineLayoutRequest):
        response = onlinelayouttools.get_online_layout_by_name(request.confid, request.layout_name, "", "")
        return response


class GetOnlineLayoutsListHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetOnlineLayoutsListRequest) -> OnlineLayoutsListResponse:
        layouts_list = onlinelayouttools.get_online_layouts_list(request.confid)
        return OnlineLayoutsListResponse(layouts_list)


class CreateOnlineLayoutHandler:

    def __init__(self):
        pass

    async def handle(self, request: CreateOnlineLayoutRequest) -> IndividualOnlineLayoutResponse:
        return onlinelayouttools.create_online_layout_ex(request)


class DeleteOnlineLayoutHandler:

    def __init__(self):
        pass

    async def handle(self, request: DeleteOnlineLayoutRequest) -> DeleteOnlineLayoutResponse:
        response = onlinelayouttools.reset_to_global_layout(request.confid, request.layout_name)
        return response


class ChangeOnlineLayoutHandler:

    def __init__(self):
        pass

    async def handle(self, request: ChangeOnlineLayoutRequest):
        return onlinelayouttools.edit_online_layout(request)


class CreateLayoutFromTemplateHandler:

    def __init__(self):
        pass

    async def handle(self, request: CreateLayoutFromTemplateRequest) -> IndividualOnlineLayoutResponse:
        return layouttemplates.create_layout_from_template(request)


class CreateTemplateFromOnlineLayoutHandler:

    def __init__(self):
        pass

    async def handle(self, request: CreateTemplateFromOnlineLayoutRequest):
        response = layouttemplates.create_template_from_online_layout(request)
        return response


class GetAllLayoutsTypesHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetAllLayoutsTypesRequest) -> GetAllLayoutsTypesResponse:
        types_response = layoutcfgtools.get_layout_types()
        return types_response


class GetSheduledLayoutHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetSheduledLayoutRequest):
        response = layoutcfgtools.get_layout_by_id(request.layout_id)
        return response


class GetSheduledLayoutsListHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetSheduledLayoutsListRequest):
        response = layoutcfgtools.get_layouts_list(request.confid, request.template)
        return response


class CreateLayoutHandler:

    def __init__(self):
        pass

    async def handle(self, request):
        response = layoutcfgtools.create_layout(request)
        return response


class ChangeLayoutHandler:

    def __init__(self):
        pass

    async def handle(self, request: ChangeSheduledLayoutRequest):
        response = layoutcfgtools.update_layout(request)
        return response


class DeleteLayoutHandler:

    def __init__(self):
        pass

    async def handle(self, request: DeleteSheduledLayoutRequest) -> DeleteSheduledLayoutResponse:
        response = layoutcfgtools.delete_layout(request.layout_id)
        return response


class GetGroupHandler:
    def __init__(self):
        pass

    async def handle(self, request: GetGroupRequest) -> GroupResponse:
        response = addrbook.get_group_by_id(request.id)
        return GroupResponse(response)


class GetGroupsListHandler:
    def __init__(self):
        pass

    async def handle(self, request: GetGroupsListRequest) -> GroupsListResponse:
        response = addrbook.get_groups_list(request)
        return GroupsListResponse(response)


class CreateGroupHandler:
    def __init__(self):
        pass

    async def handle(self, request: CreateGroupRequest) -> GroupResponse:
        response = addrbook.add_new_group(request)
        return GroupResponse(response)


class ChangeGroupHandler:
    def __init__(self):
        pass

    async def handle(self, request: ChangeGroupRequest) -> GroupResponse:
        response = addrbook.update_group(request)
        return GroupResponse(response)


class DeleteGroupHandler:
    def __init__(self):
        pass

    async def handle(self, request: DeleteGroupRequest) -> DeleteGroupResponse:
        del_group_id = addrbook.delete_group(request.id)
        return DeleteGroupResponse(del_group_id)


class GetContactsOfGroupHandler:
    def __init__(self):
        pass

    async def handle(self, request: GetGroupContactListRequest) -> ContactListResponse:
        response = addrbook.get_contacts_of_group(request)
        return response


class AddContactToGroupHandler:
    def __init__(self):
        pass

    async def handle(self, request: AddContactToGroupRequest) -> GroupResponse:
        response = addrbook.add_contacts_list_to_group(request.contact_id_list, request.group_id)
        return GroupResponse(response)


class GetContactGroupHandler:
    def __init__(self):
        pass

    async def handle(self, request: GetContactOfGroupRequest) -> ContactResponse:
        req = GetContactRequest(request.contact_id)
        handler = GetContactHandler()
        result = await handler.handle(req)
        return result


class DeleteContactFromGroupHandler:
    def __init__(self):
        pass

    async def handle(self, request: DeleteContactFromGroupRequest) -> DeleteContactFromGroupResponse:
        response = addrbook.del_contact_from_group(request.contact_id, request.group_id)
        return response


# generate CSR-file (1-st phase of offline registration)
class OfflineRegistrationHandler:

    def __init__(self):
        pass

    async def handle(self, request: OfflineRegistrationRequest) -> OfflineRegistrationResponse:
        csr_data_base64 = licenses.build_csr_file(request.serial, request.server_name)
        return OfflineRegistrationResponse(csr_data_base64)


# registration by network
class OnlineRegistrationHandler:

    def __init__(self):
        pass

    async def handle(self, request: OnlineRegistrationRequest) -> RegistrationResponse:
        result = licenses.online_registration(request.serial, request.server_name, request.request_as_file)

        res = RegistrationResultEntity(result["ok"], result["code"], "")

        return RegistrationResponse(res)


# upload lisense to MCU (1-nd phase of offline registration)
class UploadLicenseHandler:

    def __init__(self):
        pass

    async def handle(self, request: UploadLicenseRequest) -> RegistrationResponse:
        licenses.save_licenses_to_files(request.licenses_str)

        res = RegistrationResultEntity(True, 0, "success")
        return RegistrationResponse(res)


# get license params
class GetLicenseHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetLicenseRequest) -> GetLicenseResponse:
        res = licenses.get_license()

        return GetLicenseResponse(res)


class InstanceCallsHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> InstanceCallsResponse:
        res = calls.get_calls_settings()

        return InstanceCallsResponse(res)


class InstanceSetCallsHandler:

    def __init__(self):
        pass

    async def handle(self, request: InstanceCallsRequest) -> InstanceCallsResponse:
        sysinfotools.check_connection_to_backend_components()
        calls.set_calls_settings(request.calls)
        req = EmptyRequest()
        handler = InstanceCallsHandler()
        res = await handler.handle(req)

        return res


class InstanceSipHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> InstanceSipResponse:
        res = calls.get_sip_settings()

        return InstanceSipResponse(res)


class InstanceSetSipHandler:

    def __init__(self):
        pass

    async def handle(self, request: InstanceSipRequest) -> InstanceSipResponse:
        sysinfotools.check_connection_to_backend_components()
        calls.set_sip_settings(request.sip)
        req = EmptyRequest()
        handler = InstanceSipHandler()
        res = await handler.handle(req)

        return res


class GetH323SettingsHandler:
    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> GetH323SettingsResponse:
        res = calls.get_h323_settings()
        return GetH323SettingsResponse(res)


class SetH323SettingsHandler:
    def __init__(self):
        pass

    async def handle(self, request: SetH323SettingsRequest) -> GetH323SettingsResponse:
        sysinfotools.check_connection_to_backend_components()
        calls.set_h323_settings(request.h323)
        req = EmptyRequest()
        handler = GetH323SettingsHandler()
        res = await handler.handle(req)
        return res


class InstanceVideoHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> InstanceVideoResponse:
        res = profile.get_video_settings()

        return InstanceVideoResponse(res)


class InstanceSetVideoHandler:

    def __init__(self):
        pass

    async def handle(self, request: InstanceVideoRequest) -> InstanceVideoResponse:
        profile.set_video_settings(request.video)
        req = EmptyRequest()
        handler = InstanceVideoHandler()
        res = await handler.handle(req)

        return res


class InstanceLDAPHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> InstanceLDAPResponse:
        res = profile.get_ldap_settings()
        is_connected = profile.is_ldap_server_online()

        return InstanceLDAPResponse(res, is_connected)


class InstanceSetLDAPHandler:

    def __init__(self):
        pass

    async def handle(self, request: InstanceLDAPRequest) -> InstanceLDAPResponse:
        profile.set_ldap_settings(request.ldap)
        req = EmptyRequest()
        handler = InstanceLDAPHandler()
        res = await handler.handle(req)

        return res


class InstanceEmailHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> InstanceEmailResponse:
        res = profile.get_email_settings()
        is_connected = profile.is_ldap_server_online()

        return InstanceEmailResponse(res)


class InstanceSetEmailHandler:

    def __init__(self):
        pass

    async def handle(self, request: InstanceEmailRequest) -> InstanceEmailResponse:
        profile.set_email_settings(request.email)
        req = EmptyRequest()
        handler = InstanceEmailHandler()
        res = await handler.handle(req)

        return res


class InstanceDBHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> InstanceDBResponse:
        res = profile.get_db_settings()

        return InstanceDBResponse(res)


class InstanceSetDBHandler:

    def __init__(self):
        pass

    async def handle(self, request: InstanceDBRequest) -> InstanceDBResponse:
        if reservedir.is_use_net_folder():
            reservedir.umount_net_dir()

        profile.set_db_settings(request.db)
        req = EmptyRequest()
        handler = InstanceDBHandler()
        res = await handler.handle(req)

        if reservedir.is_use_net_folder():
            reservedir.mount_net_dir()

        return res


class InstanceCPUHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> InstanceCPUResponse:
        res = metrics.get_cpu_loading()

        return InstanceCPUResponse(res)


class InstanceMemoryHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> InstanceMemoryResponse:
        res = metrics.get_memory_usage()

        return InstanceMemoryResponse(res)


class InstanceNetHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> InstanceNetResponse:
        res = metrics.get_net_metrics()

        return InstanceNetResponse(res)


class InstanceFreeSpaceHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> InstanceFreeSpaceResponse:
        res = metrics.get_free_space()

        return InstanceFreeSpaceResponse(res)


class GetAllResolutionsListHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetAllResolutionsListRequest) -> AllResolutionsListResponse:
        res = conftools.get_all_resolutions_list()
        return res


class LayoutCalculatorHandler:

    def __init__(self):
        pass

    async def handle(self, request: CalculateLayoutRequest) -> CalculateLayoutResponse:
        if customlayouttypes.is_custom_layout_type(request.schema_type):
            custom_layout_types_list = customlayouttypes.get_all_custom_layout_types()
            result = customlayouttypes.calc_custom_layout_type_coord2(request.cells_count, request.width,
                                                                      request.height, request.schema_type,
                                                                      custom_layout_types_list)
        else:
            result = layoutcfgtools.get_layout_wnd_coord(request)
        return result


class SwitchMicrophoneHandler:

    def __init__(self):
        pass

    async def handle(self, request: SwitchMicrophoneRequest) -> SessionParticipantControlResponse:
        result = sessionpartctrl.switch_microphone(request)
        return result


class SwitchAudioToGroupAbonentsHandler:

    def __init__(self):
        pass

    async def handle(self, request: SwitchAudioToGroupAbonentsRequest) -> SessionGroupOperationResponse:
        result = sessionpartctrl.switch_group_audio(request)
        return result


class TuneSpeakerVolumeHandler:

    def __init__(self):
        pass

    async def handle(self, request: TuneSpeakerVolumeRequest) -> SessionParticipantControlResponse:
        result = sessionpartctrl.set_speaker_volume(request)
        return result


class SwitchVideoHandler:

    def __init__(self):
        pass

    async def handle(self, request: SwitchVideoRequest) -> SessionParticipantControlResponse:
        result = sessionpartctrl.switch_video(request)
        return result


class SwitchPreviewModeHandler:

    def __init__(self):
        pass

    async def handle(self, request: BaseSwitchPreviewModeRequest) -> SessionParticipantControlResponse:
        result = sessionpartctrl.set_preview_mode(request)
        return result


class GetPreviewImageHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetPreviewImageRequest) -> GetPreviewImageResponse:
        result = sessionpartctrl.get_preview_image(request)
        return result


class CameraControlHandler:

    def __init__(self):
        pass

    async def handle(self, request: CameraControlRequest) -> SessionParticipantControlResponse:
        result = sessionpartctrl.send_cmd_to_camera(request)
        return result


class RenameParticipantHandler:

    def __init__(self):
        pass

    async def handle(self, request: RenameParticipantRequest) -> SessionParticipantControlResponse:
        result = sessionpartctrl.rename_participant(request.conference_id, request.participant_id,
                                                    request.dispname, request.use_conn_id)
        return result


class SendMessageToConfHandler:

    def __init__(self):
        pass

    async def handle(self, request: SendMessageToConfRequest) -> SessionControlResponse:
        result = sessionpartctrl.send_message_to_conference(request.conference_id, request.message)
        return result


class MoveParticipantToOtherConfHandler:

    def __init__(self):
        pass

    async def handle(self, request: MoveParticipantToOtherConfRequest) -> MoveToOtherConfResponse:
        result = callabtools.move_participant_to_other_conference(request.conf_from, request.conf_to,
                                                                request.abonent_uri, request.conn_id)
        return result


class GetWaitingAbonentsListHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetWaitingAbonentsRequest) -> WaitingAbonentsListResponse:
        ab_list = onlinetools.get_waiting_abonents_json()
        return WaitingAbonentsListResponse(ab_list)


class SwitchVideoRecordHandler:

    def __init__(self):
        pass

    async def handle(self, request: SwitchVideoRecordRequest) -> SessionControlResponse:
        result = videorecord.switch_video_record_manually(request)
        return result


class GetVideoRecordingStatusHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetRecordingStatusRequest) -> GetRecordingStatusResponse:
        result = videorecord.get_video_recording_status(request.conference_id)
        return result


class GetFilesListHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetFilesListRequest) -> GetFilesListResponse:
        result = files.get_files_list(request)
        return result


class GetFileHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetFileRequest) -> GetFileResponse:
        result = files.get_file(request)
        return result


class UploadFileHandler:

    def __init__(self):
        pass

    async def handle(self, request: UploadFileRequest) -> HandlingFileResponse:
        result = files.upload_file(request)
        return result


class UploadFileByChunksHandler:

    def __init__(self):
        pass

    async def handle(self, request: UploadFileByChunksRequest) -> HandlingFileResponse:
        result = await files.upload_file_by_chunks(request)
        return result


class DeleteFileHandler:

    def __init__(self):
        pass

    async def handle(self, request: DeleteFileRequest) -> HandlingFileResponse:
        result = files.delete_file(request)
        return result


class SwitchPresentationHandler:

    def __init__(self):
        pass

    async def handle(self, request: SwitchPresentationRequest) -> SwitchPresentationResponse:
        result = presentation.switch_presentation(request)
        return result


class GetPresentationStatusHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetPresentationStatusRequest) -> GetPresentationStatusResponse:
        result = presentation.get_presentation_status(request)
        return result


class StopIncomingPresentationHandler:

    def __init__(self):
        pass

    async def handle(self, request: StopIncomingPresentationRequest) -> StopIncomingPresentationResponse:
        result = presentation.stop_incoming_presentation(request)
        return result


class ExportAbookToFileHandler:

    def __init__(self):
        pass

    async def handle(self, request: ExportAbookToFileRequest) -> HandlingFileResponse:
        if request.abook_type == "filecsv":
            fname = abook_export.get_csv_export_file_name()
            response = abook_export.export_addrbook_to_csv(fname)
        elif request.abook_type == "filexml":
            fname = abook_export.get_abook_export_file_name()
            response = abook_export.export_addrbook_to_xml(fname)
        else:
            raise UnknownAddressBookTypeError(request.abook_type)
        return response


class ImportAbookFromFileHandler:

    def __init__(self):
        pass

    async def handle(self, request: ImportAbookFromFileRequest) -> ImportAddressBookResponse:
        if request.abook_type == "filecsv":
            directory = files.get_directory("addrbookcsv")
            fname = directory + files.SLASH + request.short_filename
            result = abook_import.import_addrbook_from_csv(fname)
        elif request.abook_type == "filexml":
            directory = files.get_directory("addrbookxml")
            fname = directory + files.SLASH + request.short_filename
            result = abook_import.import_addrbook_from_xml(fname)
        else:
            raise UnknownAddressBookTypeError(request.abook_type)
        return result


class SaveDatabaseHandler:

    def __init__(self):
        pass

    async def handle(self, request: SaveDatabaseToFile) -> HandlingFileResponse:
        if reservedir.is_use_net_folder():
            if not reservedir.is_network_dir_mounted():
                reservedir.mount_net_dir()
            if not reservedir.is_network_dir_mounted():
                raise MountNetworkDirectoryError()

        result = reservetools.reserve_db2()
        return result


class RestoreDatabaseHandler:

    def __init__(self):
        pass

    async def handle(self, request: RestoreDatabaseFromFile) -> HandlingFileResponse:
        if reservedir.is_use_net_folder():
            if not reservedir.is_network_dir_mounted():
                reservedir.mount_net_dir()
            if not reservedir.is_network_dir_mounted():
                raise MountNetworkDirectoryError()

        result = reservetools.restore_db(request.short_filename)
        return result


class GetAllConfSessionParticipantsHandler:

    def __init__(self):
        pass

    async def handle(self, request: AllConfSessionParticipantsRequest) -> AllConfSessionParticipantsResponse:
        connstr, confstr = onlinetools.get_online_state()
        ab_list = onlinetools.get_all_conf_abonents_list(connstr, confstr)
        return AllConfSessionParticipantsResponse(ab_list)


class GetServerTimeHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetServerTimeRequest) -> ServerTimeResponse:
        time_sec = int(time.time())
        return ServerTimeResponse(time_sec)


class GetMCUParamsHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetMCUParamsRequest) -> MCUParamsResponse:
        calls_sett = calls.get_calls_settings()
        versions = sysinfotools.get_all_versions()
        time_sec = int(time.time())
        diagnostics = sysinfotools.get_diagnostics_info()
        b_engine_connected = diagnostics != {}
        b_db_connected = sysinfotools.is_db_connected()
        b_edit_network = networksettings.can_edit_network_settings()
        return MCUParamsResponse(calls_sett.mcu_display_name, versions, time_sec, diagnostics, b_engine_connected,
                                 b_db_connected, b_edit_network)


class GetPreviewStateHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetPreviewStateRequest) -> GetPreviewStateResponse:
        result = onlinetools.get_preview_state()
        return result


class StartUtilHandler:

    def __init__(self):
        pass

    async def handle(self, request: StartUtilRequest) -> StartUtilResponse:
        result = utils.start_utilite(request)
        return result


class StopUtilHandler:

    def __init__(self):
        pass

    async def handle(self, request: StopUtilRequest) -> StartUtilResponse:
        result = utils.stop_utilite(request, True)
        return result


class GetUtilOutputHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetUtilOutputRequest) -> UtilResponse:
        result = utils.get_utilite_output(request)
        return result


class SetCrownHandler:

    def __init__(self):
        pass

    async def handle(self, request: SetCrownRequest) -> SetCrownResponse:
        response = layoutcrown.set_crown_to_participant(request)
        return response


class DeleteCrownHandler:

    def __init__(self):
        pass

    async def handle(self, request: DeleteCrownRequest) -> DeleteCrownResponse:
        response = layoutcrown.delete_crown_from_participant(request, {})
        return response


class GetRTPSettingsHandler:

    def __init__(self):
        pass

    async def handle(self, request: EmptyRequest) -> RTPSettingsResponse:
        res = profile.get_rtp_settings()
        return RTPSettingsResponse(res)


class SetRTPSettingsHandler:

    def __init__(self):
        pass

    async def handle(self, request: SetRTPSettingsRequest) -> RTPSettingsResponse:
        sysinfotools.check_connection_to_backend_components()
        profile.set_rtp_settings(request.rtp)
        req = EmptyRequest()
        handler = GetRTPSettingsHandler()
        res = await handler.handle(req)
        return res


class GetAllNetworkSettingsHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetAllNetworkSettingsRequest) -> AllNetworkSettingsResponse:
        settings_list = networksettings.get_all_network_settings()
        return AllNetworkSettingsResponse(settings_list)


class GetNetworkSettingsHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetNetworkSettingsRequest) -> NetworkSettingsResponse:
        settings = networksettings.get_network_settings(request)
        return NetworkSettingsResponse(settings)


class ChangeNetworkSettingsHandler:

    def __init__(self):
        pass

    async def handle(self, request: ChangeNetworkSettingsRequest) -> NetworkSettingsResponse:
        # don't edit network settings if some conference is launched
        if tools.is_some_conf_launched():
            raise ConferenceAlreadyLaunchedError("some_conference")

        settings = networksettings.set_network_settings(request)

        # restart event_processor and vtsrv after change ip-address
        pipetools.send_msg("RESTART")
        return NetworkSettingsResponse(settings)


class CanChangeNetworkSettingsHandler:

    def __init__(self):
        pass

    async def handle(self, request: CanChangeNetworkSettingsRequest) -> CanChangeNetworkSettingsResponse:
        b_edit = networksettings.can_edit_network_settings()
        return CanChangeNetworkSettingsResponse(b_edit)


class GetVEngineLogHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetVEngineLogRequest) -> GetVEngineLogResponse:
        log_obj = venginelog.get_vengine_log(request)
        return log_obj


class ExecuteVEngineCommandHandler:

    def __init__(self):
        pass

    async def handle(self, request: ExecuteVEngineCommandRequest) -> VEngineCommandResponse:
        response = venginelog.execute_vengine_command(request)
        return response


class GetMCUReferencesHandler:
    def __init__(self):
        pass

    async def handle(self, request: GetMCUReferencesRequest) -> MCUReferencesResponse:
        response = sysinfotools.get_mcu_references(request)
        return response


class CleanAddressBookHandler:
    def __init__(self):
        pass

    async def handle(self, request: CleanAddressBookRequest) -> CleanAddressBookResponse:
        success = addrbook.clean_address_book(request)
        return CleanAddressBookResponse(success)


class CreateCustomLayoutTypeHandler:
    def __init__(self):
        pass

    async def handle(self, request: CreateCustomLayoutTypeRequest) -> CustomLayoutTypeResponse:
        result = customlayouttypes.create_new_custom_layout_type(request)
        return result


class ChangeCustomLayoutTypeHandler:
    def __init__(self):
        pass

    async def handle(self, request: ChangeCustomLayoutTypeRequest) -> CustomLayoutTypeResponse:
        result = customlayouttypes.change_custom_layout_type(request)
        return result


class DeleteCustomLayoutTypeHandler:
    def __init__(self):
        pass

    async def handle(self, request: DeleteCustomLayoutTypeRequest) -> DeleteCustomLayoutTypeResponse:
        customlayouttypes.delete_custom_layout_type(request.layout_type)
        return DeleteCustomLayoutTypeResponse(request.layout_type)


class GetCustomLayoutTypeHandler:
    def __init__(self):
        pass

    async def handle(self, request: GetCustomLayoutTypeRequest) -> CustomLayoutTypeResponse:
        result = customlayouttypes.get_custom_layout_type(request.layout_type)
        return result


class GetAllCustomLayoutTypesHandler:
    def __init__(self):
        pass

    async def handle(self, request: GetAllCustomLayoutTypesRequest) -> AllCustomLayoutTypesResponse:
        types_list = customlayouttypes.get_all_custom_layout_types()
        return AllCustomLayoutTypesResponse(types_list)


class PowerManagementHandler:

    def __init__(self):
        pass

    async def handle(self, request: ExecuteRestartCommandRequest) -> RestartCommandResponse:
        if request.command == "restart_api":
            result = powertools.restart_all_api()
        elif request.command == "restart_os":
            result = powertools.restart_os()
        elif request.command == "power_off":
            result = powertools.power_off()
        else:
            result = False
        if not result:
            raise PowerManagementError("")
        return (RestartCommandResponse(result))


class InstallDebHandler:

    def __init__(self):
        pass

    async def handle(self, request: InstallDebRequest) -> InstallDebResponse:
        installtools.install_deb(request.filename)
        return InstallDebResponse(request.filename)


class GetInstallDebStateHandler:

    def __init__(self):
        pass

    async def handle(self, request: GetInstallDebStateRequest) -> GetInstallDebStateResponse:
        state_num = installtools.is_installing()
        state_str = data.get_install_state(state_num)
        return GetInstallDebStateResponse(state_str)


class SwitchRenderModeToGroupAbonentsHandler:
    def __init__(self):
        pass

    async def handle(self, request: SwitchRenderModeToGroupAbonentsRequest) -> SessionGroupOperationResponse:
        result = sessionpartctrl.switch_group_render_mode(request)
        return result


class HangupGroupAbonentsHandler:

    def __init__(self):
        pass

    async def handle(self, request: HangupGroupAbonentsRequest) -> SessionGroupOperationResponse:
        result = callabtools.hangup_group_abonents(request)
        return result


class SwitchNoiseSuppresorToGroupAbonentsHandler:

    def __init__(self):
        pass

    async def handle(self, request: SwitchNoiseSuppresorToGroupAbonentsRequest) -> SessionGroupOperationResponse:
        result = sessionpartctrl.switch_noise_suppressor(request)
        return result


class SwitchAGCToGroupAbonentsHandler:

    def __init__(self):
        pass

    async def handle(self, request: SwitchAGCToGroupAbonentsRequest) -> SessionGroupOperationResponse:
        result = sessionpartctrl.switch_auto_gain_ctrl(request)
        return result


class SwitchVideoToGroupAbonentsHandler:

    def __init__(self):
        pass

    async def handle(self, request: SwitchVideoToGroupAbonentsRequest) -> SessionGroupOperationResponse:
        result = sessionpartctrl.switch_group_video(request)
        return result



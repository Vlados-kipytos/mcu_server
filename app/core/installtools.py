#!/usr/bin/python3


import logging
import sys
import os

import app.core.data as data
import app.core.dbtools as dbtools
import app.core.files as files
import app.core.powertools as powertools
import app.core.tools as tools
import app.core.xmltools as xmltools

from app.core.errors import InstallDebError


def is_installing() -> int:
    """Return state of installation process"""
    inst_rows = dbtools.select("SELECT installing FROM legacy.commonconfig")
    if len(inst_rows) != 0:
        if inst_rows[0][0] != 0:
            return data.INSTALL_STATE_IN_PROGRESS
        else:
            return data.INSTALL_STATE_STOPPED
    else:
        return data.INSTALL_STATE_UNKNOWN


def set_installing(b_installing: bool) -> bool:
    """Set state of installation process (set flag value only)"""
    installing_num = tools.bool_to_int(b_installing)
    res = dbtools.make_sql_request("UPDATE legacy.commonconfig SET installing=%d" % (installing_num))
    return res != 0


def install_deb(_deb_fname: str):
    """This function install deb package with short file name _deb_fname."""
    if sys.platform == "win32":
        logging.error("install_deb(): windows version is not implemented")
        return

    set_installing(True)

    cfg = xmltools.get_config()
    deb_dir = cfg["dirs"]["updatemcu"]
    full_deb_fname = deb_dir + files.SLASH + _deb_fname

    out_log_fname = deb_dir + files.SLASH + "install_out.txt"
    err_log_fname = deb_dir + files.SLASH + "install_err.txt"

    # command "apt install full-deb-file-name" must install new deb package with all dependencies
    code = 0
    code = os.system("apt -y --allow-downgrades install %s 1>%s 2>%s" % (full_deb_fname, out_log_fname, err_log_fname))
    code >>= 8  # get major byte

    if code != 0:
        set_installing(False)
        logging.error("install_deb() error at installing new deb package, code=%d" % (code))
        raise InstallDebError(code)

    # restart api after install new .deb package
    success = powertools.restart_all_api()
    if not success:
        set_installing(False)
        logging.error("install_deb() error at restart services after install new deb package")
        raise InstallDebError(-1)


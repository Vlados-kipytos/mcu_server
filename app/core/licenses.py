#!/usr/bin/python

import base64
import logging
import os
import os.path
import sys
import shutil
import time
import zipfile

import app.core.cert_functions as CF
import app.core.check_cert as check_cert
import app.core.constants as constants
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.errors as errors
import app.core.getting_license as getting_license
import app.core.nettools as  nettools
import app.core.pipetools as pipetools
import app.core.registration_MCU as registration_MCU
import app.core.sysinfotools as sysinfotools
import app.core.tools as tools
import app.core.update_cert as update_cert
import app.core.xmltools as xmltools
from app.core.entities import LicenseEntity


SLASH = os.path.sep

MAX_LIC_UPD_FAILS = 3


def get_scripts_params_fname():
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]
    return dir + SLASH + "scripts_params.xml"


def get_client_data_fname():
    """This function returns file name with client data (serial, domain) for online registration."""
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]
    return dir + SLASH + "clientdata.xml"


def get_client_data_offline_fname():
    """This function returns file name with client data (serial, domain) for offline registration."""
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]
    return dir + SLASH + "clientdata_offline.xml"


def get_client_data_valid_fname():
    """This function returns file name with valid client data (serial, domain).
    If offline/online registration success, then copy offline/online client data file to "valid" client data file.
    Valid client data appropriate getted online/offline license."""
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]
    return dir + SLASH + "clientdata_valid.xml"


def get_hwinfo_fname():
    if sys.platform == "win32":
        return tools.get_engine_dir() + SLASH + "install" + SLASH + constants.HW_INFO_XML_FNAME
    else:
        cfg = xmltools.get_config()
        dir = cfg["dirs"]["shareddir"]
        return dir + SLASH + constants.HW_INFO_XML_FNAME


def get_private_key_fname():
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]
    return dir + SLASH + "private.key"


def get_csr_fname():
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]
    return dir + SLASH + "clientdata.csr"


def get_cert_fname():
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]
    return dir + SLASH + "certificate.crt"

def get_tmp_ca_cert_fname():
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]
    return dir + SLASH + "trueconf_ca.crt"


def get_buffer_fname():
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]
    return dir + SLASH + "buffer.tmp"


def get_tmp_license_fname():
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]
    return dir + SLASH + "license.xml"


def exp_date_to_dict(_exp_date_str):
    """This function convert date string 'yyyymmdd' to dictionary."""
    _exp_date_str = str(_exp_date_str)

    result = {"year": 2000, "month": 1, "day": 1}

    if len(_exp_date_str) != 8:
        return (result)  # error

    year_str = _exp_date_str[0:4]
    month_str = _exp_date_str[4:6]
    day_str = _exp_date_str[6:8]

    result["year"] = int(year_str)
    result["month"] = int(month_str)
    result["day"] = int(day_str)

    return (result)


def exp_date_db_str(_exp_date_str):
    """This function converts expiration date to database string"""

    date_dict = exp_date_to_dict(_exp_date_str)

    db_str = "%4d-%02d-%02d 00:00:00" % (date_dict["year"], date_dict["month"], date_dict["day"])

    return (db_str)


def is_date_expired(_exp_date_str):
    """This function return True if license expiration date is expired."""
    date_db_str = exp_date_db_str(_exp_date_str)
    exp_date_sec = tools.loacal_timestr_to_sec(date_db_str)
    if time.time() > exp_date_sec:
        return True
    else:
        return False


def update_license_state_in_db(_lic_state_num, _lic_type_str, _exp_first_str, _exp_last_str, _lic_cnt, _lic_serial):
    """This function update state of license in database"""

    if _lic_cnt == "":
        _lic_cnt = 0

    _lic_state_num = int(_lic_state_num)
    _lic_type_str = str(_lic_type_str)
    _exp_first_str = str(_exp_first_str)
    _exp_last_str = str(_exp_last_str)
    _lic_cnt = int(_lic_cnt)
    _lic_serial = str(_lic_serial)

    lic_type_num = data.license_type_str_to_index(_lic_type_str)
    if lic_type_num < 0:
        lic_type_num = data.LIC_TYPE_INSTANTANEOUS  # default value

    exp_first_db_str = exp_date_db_str(_exp_first_str)
    exp_last_db_str = exp_date_db_str(_exp_last_str)

    res = dbtools.make_sql_request(
        "UPDATE legacy.commonconfig SET lic_stat=%d, lic_type=%d, exp_first='%s', exp_last='%s', lic_cnt=%d, lic_serial='%s'" % (
            _lic_state_num, lic_type_num, exp_first_db_str, exp_last_db_str, _lic_cnt, _lic_serial))

    if res == 0:
        logging.error("update_license_state_in_db(): db error: " + dbtools.get_last_error())


def del_certificate_file():
    """This function deletes crt-file of certificate."""

    cert_fname = get_cert_fname()

    if sys.platform == "win32":
        cmd = 'del /Q "%s"' % (cert_fname)
        os.system(cmd)
    else:
        out, err = tools.exec_cmd(["rm", "-f", "-v", cert_fname], False)
        if err != b"":
            logging.error("cannot delete certificate file: ")
            logging.error(err)


def del_all_licenses():
    lic_dir = tools.get_license_dir()
    files = lic_dir + SLASH + "license*.xml"
    if sys.platform == "win32":
        cmd = 'del /Q "%s"' % (files)
    else:
        cmd = 'rm -f %s' % (files)
    logging.error("del_all_licenses(): " + cmd)
    os.system(cmd)


def split_group_licenses(_group_fname):
    """This function read file _group_fname with some concatenated licenses and save each license to separated file."""

    # read source file with some licenses
    data = ""
    try:
        f = open(_group_fname, "rb")
        data = f.read().decode("UTF-8")
        f.close()
    except:
        logging.error("split_group_licenses() error: " + str(sys.exc_info()))
        return (0)  # error

    if len(data) == 0:
        logging.error("split_group_licenses() error: license is empty")
        return (0)  # error

    # find first license (begin tag)
    start_ind = data.find("<?xml")
    if start_ind == -1:
        logging.error("split_group_licenses() error: no license")
        return (0)  # error

    saved_lic_cnt = 0

    while 1:
        end_ind = data.find("<?xml", start_ind + len("<?xml"))
        if end_ind != -1:
            cur_license = data[start_ind:end_ind]
            cur_fname = tools.get_license_unique_fname2()
            fout = open(cur_fname, "wb")
            fout.write(cur_license.encode("UTF-8"))
            fout.close()
            saved_lic_cnt += 1
            time.sleep(0.02)
            start_ind = end_ind
        else:
            cur_license = data[start_ind:]
            cur_fname = tools.get_license_unique_fname2()
            fout = open(cur_fname, "wb")
            fout.write(cur_license.encode("UTF-8"))
            fout.close()
            saved_lic_cnt += 1
            break

    if saved_lic_cnt > 0:
        return (1)
    else:
        logging.error("split_group_licenses() error: no licenses saved")
        return (0)


def is_raw_license(_fname):
    """This function checks format of license file _fname. If file _fname is just text file then returns 1, else returns 0."""

    f = open(_fname, "rb")
    data = f.read()
    f.close()

    data_str = data.decode("UTF-8")

    xml_index = data_str.find("<?xml")
    if xml_index >= 0:
        return (1)
    else:
        return (0)


def save_zipped_licenses(_zip_fname):
    """This function extract licenses and certificate form zip archive"""

    buffer_fname = get_buffer_fname()

    try:
        # read source file
        fsrc = open(_zip_fname, "rb")
        encoded_data = fsrc.read()
        fsrc.close()

        # decode source data
        decoded_data = base64.b64decode(encoded_data)

        # save decoded data
        fdst = open(buffer_fname, "wb")
        fdst.write(decoded_data)
        fdst.close()
    except:
        logging.error("save_zipped_licenses(): error at decode zip file")
        return (0)

    if not zipfile.is_zipfile(buffer_fname):
        logging.error("save_zipped_licenses(): bad format of zip file")
        return (0)

    zf = zipfile.ZipFile(buffer_fname, "r", zipfile.ZIP_DEFLATED)

    zipped_files_list = zf.namelist()
    for fname in zipped_files_list:
        logging.error("extract file %s..." % (fname))

        data = zf.read(fname)

        ext = tools.extract_extension(fname)
        if ext == "xml":
            dst_fname = tools.get_license_unique_fname2()
            time.sleep(0.02)
        elif ext == "crt":
            dst_fname = get_cert_fname()
        else:
            logging.error("unknown extension of file %s..." % (fname))
            continue

        f = open(dst_fname, "wb")
        f.write(data)
        f.close()

    zf.close()

    return (1)


def get_license_type() -> int:
    """This function returns type of license. (see constants data. LIC_TYPE_INSTANTANEOUS, ...)"""

    lic_type = data.LIC_TYPE_INSTANTANEOUS
    lic_rows = dbtools.select("SELECT lic_type FROM legacy.commonconfig")
    if len(lic_rows) != 0:
        lic_type = lic_rows[0][0]

    return lic_type


def get_license_upd_cnt() -> int:
    lic_rows = dbtools.select("SELECT lic_upd_cnt FROM legacy.commonconfig")
    if len(lic_rows) != 0:
        return lic_rows[0][0]
    else:
        return 0


def set_license_upd_cnt(upd_cnt: int):
    dbtools.make_sql_request("UPDATE legacy.commonconfig SET lic_upd_cnt=%d" % (upd_cnt))


def get_license_upd_time() -> int:
    lic_rows = dbtools.select("SELECT lic_upd_time FROM legacy.commonconfig")
    if len(lic_rows) != 0:
        lic_upd_time = tools.db_date_time_to_json(str(lic_rows[0][0]))
        return int(lic_upd_time)
    else:
        return 0


def set_license_upd_time(upd_time: int):
    upd_time = int(upd_time)
    upd_time_str = tools.json_date_time_to_db(upd_time)
    dbtools.make_sql_request("UPDATE legacy.commonconfig SET lic_upd_time='%s'" % (upd_time_str))


def is_first_upd_license() -> bool:
    lic_rows = dbtools.select("SELECT fst_lic_upd FROM legacy.commonconfig")
    if len(lic_rows) != 0:
        return tools.int_to_bool( lic_rows[0][0] )
    else:
        return False


def set_first_license_upd(first_upd: bool):
    first_upd_num = tools.bool_to_int(first_upd)
    dbtools.make_sql_request("UPDATE legacy.commonconfig SET fst_lic_upd=%d" % (first_upd_num))


def get_license_state():
    """This function returns current license state (see constants data.LIC_STAT...)"""

    license_state = data.LIC_STAT_UNDEFINED
    license_rows = dbtools.select("SELECT lic_stat FROM legacy.commonconfig")
    if len(license_rows) != 0:
        license_state = license_rows[0][0]

    return license_state


# OFFLINE REGIATRATION (SAVE LICENSE FILE)
def save_licenses_to_files(_license_str):
    tmp_license_fname = get_tmp_license_fname()

    try:
        # save block of licenses to temporery file
        f = open(tmp_license_fname, "wb")
        f.write(_license_str.encode("UTF-8"))
        f.close()
    except:
        logging.error("save_licenses_to_files(): cannot save licenses to temporary file: " + str(sys.exc_info()))
        raise errors.SaveTemporaryLicenseError()

    # check format of license file (raw license or encoded zip)
    if is_raw_license(tmp_license_fname):
        # split group of licenses "tmp_license_fname" to separated files and save it with uniques names
        result = split_group_licenses(tmp_license_fname)
        if not result:
            raise errors.SplitGroupLicensesError()
    else:
        # unpack licenses from zip-archive
        result = save_zipped_licenses(tmp_license_fname)
        if not result:
            raise errors.SaveZippedLicensesError()

    license_state = get_license_state()
    logging.error("save_licenses_to_files(): license_state=%d" % (license_state))

    # make copy of valid client data
    client_data_fname = get_client_data_offline_fname()
    if os.path.exists(client_data_fname):
        shutil.copy(client_data_fname, get_client_data_valid_fname())

    # restart vtsrv engine to activate licenses
    pipetools.send_msg("RESTART")

    return (1)


def get_license_files_list():
    """This function builds list (table) of videofiles."""

    license_dir = tools.get_license_dir()
    #logging.info("get_license_files_list(): license_dir=" + license_dir)

    flist = tools.get_files_list(license_dir)
    #logging.info("get_license_files_list(): flist=" + str(flist))

    # fill files table
    filtered_list = []
    for file in flist:
        # skip directories
        if file["dir"]:
            #logging.info("get_license_files_list(): ignore directory " + str(file["dir"]))
            continue

        # skip files with outside extensions
        sname = file["shortname"]
        cur_ext = tools.get_file_extension(sname)
        if cur_ext != "xml":
            #logging.info("get_license_files_list(): ignore not .xml file " + str(sname))
            continue

        # skip files without prefix "license"
        prefix = "license"
        prefix_len = len(prefix)
        if len(sname) < prefix_len:
            #logging.info("get_license_files_list(1): no prefix 'license' in fname " + str(sname))
            continue
        if sname[:prefix_len] != prefix:
            #logging.info("get_license_files_list(2): no prefix 'license' in fname " + str(sname))
            continue

        fullname = file["fullname"]
        size = tools.get_file_size(fullname)
        #logging.info("get_license_files_list() add file with fullname=" + str(fullname))

        filtered_list.append({"shortname": sname, "fullname": fullname, "size": size})

    return (filtered_list)


def update_license_from_server(_forse_reg):
    """This function updates license from licenses server"""

    scripts_params_fname = get_scripts_params_fname()
    client_data_fname = get_client_data_fname()
    hwinfo_fname = get_hwinfo_fname()
    private_key_fname = get_private_key_fname()
    csr_fname = get_csr_fname()
    cert_fname = get_cert_fname()
    ca_cert_fname = get_tmp_ca_cert_fname()

    set_first_license_upd(False)

    if (not os.path.exists(cert_fname)) or _forse_reg:
        res = registration_MCU.registration_MCU(scripts_params_fname,
                                                client_data_fname,
                                                hwinfo_fname,
                                                private_key_fname,
                                                csr_fname,
                                                cert_fname,
                                                ca_cert_fname)
        if not res["ok"]:
            logging.error("update_license_from_server(): cannot create certificate")
            return res
    else:
        valid = check_cert.check_cert(cert_fname)  # check for expiration of certificate
        if valid != CF.CORRECT_CERT:
            res = update_cert.update_cert(scripts_params_fname, cert_fname, private_key_fname,
                                          ca_cert_fname)
            if not res["ok"]:
                logging.error("update_license_from_server(): cannot update certificate")
                return res

    # update license from server
    license_file = tools.get_license_unique_fname2()
    res = getting_license.getting_licence(scripts_params_fname, cert_fname, private_key_fname, license_file,
                                          ca_cert_fname)
    if not res["ok"]:
        logging.error("update_license_from_server(): cannot update license")
        return res

    # drop counter of failed downloads of license if it is successfully downloaded
    if res["ok"]:
        set_license_upd_cnt(0)

    return res


def smart_upd_lic_from_srv(_forse_reg):
    """This function try to download licenses from server. If license is downloaded, then delete all old licenses."""

    # get license files list
    flist = get_license_files_list()
    logging.info("smart_upd_lic_from_srv(): flist=" + str(flist))

    result = update_license_from_server(_forse_reg)
    logging.info("smart_upd_lic_from_srv(): get license result: " + str(result))

    # if download new license, delete old licenses
    if result["ok"]:
        logging.info("smart_upd_lic_from_srv(): result=ok")
        for f in flist:
            logging.error("smart_upd_lic_from_srv(): DELETE OLD LICENSE " + f["fullname"])
            if sys.platform == "win32":
                cmd = 'del /Q "%s"' % (f["fullname"])
            else:
                cmd = 'rm -f "%s"' % (f["fullname"])
            os.system(cmd)

    return (result)


def create_scripts_params_file(_serial):
    """This function creates file script_params.xml"""

    _serial = _serial

    pwd_index = _serial.find("-")
    if pwd_index >= 0:
        procuct_id = _serial[0:pwd_index]
    else:
        procuct_id = ""

    data = """<?xml version="1.0" encoding="UTF-8"?>
    <data>
      <url_reg>https://reg.trueconf.com/mcu/v1/cert</url_reg>
      <url_upd>https://reg.trueconf.com/mcu/v1/cert</url_upd>
      <url_get_licence>https://reg.trueconf.com/mcu/v1/licenses</url_get_licence>
    </data>
    """

    scripts_params_fname = get_scripts_params_fname()

    f = open(scripts_params_fname, "wb")
    f.write(data.encode("UTF-8"))
    f.close()


def create_clientdata_file(_serial, _domain, _fname):
    """This function creates file clientdata.xml"""

    _serial = _serial
    _domain = _domain

    data = """<?xml version="1.0" encoding="UTF-8"?>
    <clientdata>
      <serial>%s</serial>
      <domain>%s</domain>
      <country>RU</country>
      <state>Moscow</state>
      <city>Moscow</city>
      <org>CBGB</org>
      <org_unit>Network Operations</org_unit>
    </clientdata>
    """ % (_serial, _domain)

    f = open(_fname, "wb")
    f.write(data.encode("UTF-8"))
    f.close()


def build_csr_file(_serial: str, _server_name: str) -> bytes:
    """this function create script params file, client data file, CSR-file and returns CSR-file as base64 bytes """

    create_scripts_params_file(_serial)

    client_data_offline_fname = get_client_data_offline_fname()
    hwinfo_fname = get_hwinfo_fname()
    private_key_fname = get_private_key_fname()
    csr_fname = get_csr_fname()

    create_clientdata_file(_serial, _server_name, client_data_offline_fname)

    res = registration_MCU.build_csr_file(client_data_offline_fname, hwinfo_fname, private_key_fname, csr_fname)
    if res != CF.SUCCESS:
        logging.error("build_csr_file(): cannot create CSR file")
        return b""

    f = open(csr_fname, "rb")
    csr_data = f.read()
    f.close()

    csr_data_str = base64.b64encode(csr_data)
    return csr_data_str


# ONLINE REGISTRATION
def online_registration(_serial, _server_name, _force_reg):
    """This function register MCU in license server."""

    # force recreate certificate
    if _force_reg:
        del_certificate_file()

    client_data_online_fname = get_client_data_fname()

    create_scripts_params_file(_serial)
    create_clientdata_file(_serial, _server_name, client_data_online_fname)

    license_state = get_license_state()
    logging.error("online_registration(): license_state=%d" % (license_state))

    logging.debug("GET LICENSE IN WEB API:")
    result = smart_upd_lic_from_srv(_force_reg)
    if not result["ok"]:
        details_list = result["details"]
        http_code = result["http_code"]
        raise errors.OnlineRegistrationError(result["code"], details_list, http_code)
    else:
        # make copy of valid client data
        if os.path.exists(client_data_online_fname):
            shutil.copy(client_data_online_fname, get_client_data_valid_fname())

        pipetools.send_msg("RESTART")

    return result


def send_event_to_crm(lic_type: str, exp_first: str, exp_last: str, licenses_cnt: str,
                      max_abonents: int, max_visible: int, demo: bool):
    """Send event to license server."""
    cert_fname = get_cert_fname()
    private_key_fname = get_private_key_fname()
    json_data = {
        "type": "license_reload",
        "content":
            {
                "license_type": lic_type,
                "first_expire_data": exp_first,
                "last_expire_data": exp_last,
                "licenses_count": licenses_cnt,
                "call_limit": str(max_abonents),
                "active_limit": str(max_visible),
                "unrestricted": str(demo)
            }
    }
    ca_cert_fname = get_tmp_ca_cert_fname()

    getting_license.send_event_to_crm(cert_fname, private_key_fname, json_data, ca_cert_fname)


def get_license_json() -> dict:
    """this function get sysinfo.json file form server and return it as dictionnary."""
    license_json = {}

    license_str = nettools.get_url_data_s("license.json", True)
    if license_str != "":
        true = True  # define constants "true"/"false"
        false = False
        try:
            license_json = eval(license_str)  # convert string to python expression (json format)
        except:
            return {}

    return license_json


def get_demo_flag():
    license_json = get_license_json()
    if "license" not in license_json:
        logging.error("get_demo_flag(): no field 'license'")
        return False

    if "server" not in license_json["license"]:
        logging.error("get_demo_flag(): no field 'server'")
        return False

    if "unrestricted" not in license_json["license"]["server"]:
        logging.error("get_demo_flag(): no field 'unrestricted'")
        return False

    return license_json["license"]["server"]["unrestricted"]


def get_license():
    """This function get license's parameters"""

    license = dbtools.select("""SELECT lic_stat, lic_type, exp_first, exp_last, lic_cnt, lic_serial, 
                                features, lic_upd_cnt FROM legacy.commonconfig""")

    db_state = license[0][0]
    state = data.get_license_state(db_state)

    db_type = license[0][1]
    type = data.get_license_type(db_type)

    exp_first = tools.db_date_time_to_json(str(license[0][2]))
    exp_last = tools.db_date_time_to_json(str(license[0][3]))
    count = license[0][4]
    serial = license[0][5].split("-")[0]
    features = license[0][6].split()

    lic_upd_cnt = license[0][7]
    last_upd_fail = lic_upd_cnt > 0 and db_state == data.LIC_STAT_VALID and db_type != data.LIC_TYPE_PERMANENT

    if "DEMO" in features:
        demo = True
    else:
        demo = False

    max_participants = 0
    max_active_participants = 0
    max_layouts = 0
    restriction_json = sysinfotools.get_abonents_restriction()
    if restriction_json != {}:
        max_participants = restriction_json["max_calls"]
        max_active_participants = restriction_json["max_visible"]
        max_layouts = restriction_json["max_layouts"]
        logging.info("get_license(): max_calls=%d , max_visible=%d, max_layouts=%d" %
                     (max_participants, max_active_participants, max_layouts))

    client_data_valid_fname = get_client_data_valid_fname()
    cd_res = registration_MCU.parse_client_data_file(client_data_valid_fname)
    if cd_res["result_code"] == CF.SUCCESS:
        domain = cd_res["domain"]
    else:
        domain = ""

    lic_json = get_license_json()
    if lic_json != {} and ("license" in lic_json):
        lic_prm = lic_json["license"]
    else:
        lic_prm = {}

    return LicenseEntity(state, type, exp_first, exp_last, count, serial, features, max_participants,
                         max_active_participants, max_layouts, demo, last_upd_fail, domain, lic_prm)

#!/usr/bin/python3

import datetime
import json
import logging
import os
import os.path
import random
import re
import shutil
import stat
import subprocess
import sys
import time
import traceback
import urllib
import zipfile
import ipaddress

import app.core.data as data
import app.core.dbtools as dbtools
import app.core.xmltools as xmltools

SLASH = os.path.sep

# if sys.platform != "win32":
#  import statvfs


def loacal_timestr_to_sec(timestr):
    """Converts local date-time string 'yyyy-mm-dd hh:mm:ss' to seconds count after begin of age."""
    timetup = time.strptime(timestr, "%Y-%m-%d %H:%M:%S")
    timesec = int(time.mktime(timetup))
    return timesec


def sec_to_timestr(timesec):
    """Converts seconds after begin of age to date-time string 'yyyy-mm-dd hh:mm:ss'."""
    timetup = time.gmtime(int(timesec))
    timestr = time.strftime("%Y-%m-%d %H:%M:%S", timetup)
    return timestr


# def sec_to_guistr(timesec):
#    """Converts seconds after begin of age to date-time string 'dd.mm.yyyy hh:mm:ss'."""
#    timetup = time.gmtime(int(timesec))
#    timestr = time.strftime("%d.%m.%Y %H:%M:%S", timetup)
#    return timestr


def duration_sec_to_guistr(duration_sec):
    duration_sec = int(duration_sec)
    sec = duration_sec % 60
    min_left = (duration_sec - sec) / 60
    min = min_left % 60
    hour = (min_left - min) / 60
    gui_str = "%d:%02d:%02d" % (hour, min, sec)
    return (gui_str)


def timestr_to_timedict(timestr):
    """Converts date-time string 'yyyy-mm-dd hh:mm:ss' to dictionary with date/time."""
    timetup = time.strptime(timestr, "%Y-%m-%d %H:%M:%S")
    timedict = {"year": timetup[0], "month": timetup[1], "day": timetup[2], "hour": timetup[3], "minute": timetup[4],
                "second": timetup[5]}
    return timedict


# def timedict_to_timestr(timedict):
#    """Converts dictionary with params of date/time to date-time string 'yyyy-mm-dd hh:mm:ss'."""
#    timestr = "%04d-%02d-%02d %02d:%02d:%02d" % (
#    timedict["year"], timedict["month"], timedict["day"], timedict["hour"], timedict["minute"], timedict["second"])
#    return timestr


# def get_current_time():
#    """This function returns current local time as dictionary of records"""
#    timetup = time.gmtime(time.time())
#    timedict = {"year": timetup[0], "month": timetup[1], "day": timetup[2], "hour": timetup[3], "minute": timetup[4],
#                "second": timetup[5]}
#    return (timedict)


def get_time_stamp_str():
    curtimesec = time.time()
    timetup = time.gmtime(curtimesec)
    timestr = time.strftime("%Y%m%d_%H%M%S", timetup)
    fract = time.clock()
    fract = fract - int(fract)
    fract = int(100 * fract)
    timestr += ".%02d" % (fract)
    return (timestr)


g_files_counter = 1


def get_unique_time_stamp_str():
    """This function returns unique file name for video files."""
    global g_files_counter

    timestr = get_time_stamp_str()
    cnt_str = "_%d" % (g_files_counter)
    g_files_counter += 1
    return timestr + cnt_str


def parse_ru_dt(_dt_str):
    """This function parse datetime string in format: 'dd.mm.yyyy hh:mm'. Returns dictionary with datetime."""
    result = {"year": 0, "month": 0, "day": 0, "hour": 0, "minute": 0, "second": 0}

    dt_list = _dt_str.split(" ")
    if len(dt_list) != 2:
        return (result)  # error

    d = dt_list[0]
    t = dt_list[1]

    d_list = d.split(".")
    if len(d_list) != 3:
        return (result)  # error

    t_list = t.split(":")
    if len(t_list) != 2:
        return (result)  # error

    result["day"] = int(d_list[0])
    result["month"] = int(d_list[1])
    result["year"] = int(d_list[2])
    result["hour"] = int(t_list[0])
    result["minute"] = int(t_list[1])

    return (result)


def timedict_to_ru_str(_dt_dict):
    """This function convert datetime dictionary to GUI string in russian style."""
    timestr = "%02d.%02d.%04d %02d:%02d" % (
        _dt_dict["day"], _dt_dict["month"], _dt_dict["year"], _dt_dict["hour"], _dt_dict["minute"])
    return timestr


def parse_en_dt(_dt_str):
    """This function parse datetime string in format: 'mm/dd/yyyy hh:mm SS'. Where suffix 'SS' must be 'AM' or 'PM'. Returns dictionary with datetime."""
    result = {"year": 0, "month": 0, "day": 0, "hour": 0, "minute": 0, "second": 0}

    dt_list = _dt_str.split(" ")
    if len(dt_list) != 3:
        return (result)  # error

    d = dt_list[0]
    t = dt_list[1]
    s = dt_list[2]

    d_list = d.split("/")
    if len(d_list) != 3:
        return (result)  # error

    t_list = t.split(":")
    if len(t_list) != 2:
        return (result)  # error

    result["month"] = int(d_list[0])
    result["day"] = int(d_list[1])
    result["year"] = int(d_list[2])
    result["hour"] = int(t_list[0])
    result["minute"] = int(t_list[1])

    if s == "PM":
        result["hour"] = (result["hour"] + 12) % 24

    return (result)


def timedict_to_en_str(_dt_dict):
    """This function convert datetime dictionary to GUI string in english style."""
    if _dt_dict["hour"] <= 12:
        suffix = "AM"
    else:
        _dt_dict["hour"] -= 12
        suffix = "PM"
    timestr = "%02d/%02d/%04d %02d:%02d %s" % (
        _dt_dict["month"], _dt_dict["day"], _dt_dict["year"], _dt_dict["hour"], _dt_dict["minute"], suffix)
    return timestr


def db_date_time_to_json(_db_date_time):
    """This function converts datetime string from format 'YYYY-MM-DD hh:mm:ss' to format 'YYYY-MM-DDThh:mm:ssZ'."""
    # OLD: return timestr_to_sec(_db_date_time)
    time_tup = time.strptime(_db_date_time, "%Y-%m-%d %H:%M:%S")
    dt = datetime.datetime(time_tup[0], time_tup[1], time_tup[2], time_tup[3], time_tup[4], time_tup[5],
                           tzinfo=datetime.timezone.utc)
    return int(dt.timestamp())


def json_date_time_to_db(_json_date_time):
    """This function converts datetime string from format 'YYYY-MM-DDThh:mm:ssZ' to format 'YYYY-MM-DD hh:mm:ss'."""
    return sec_to_timestr(_json_date_time)


def datetime_tz_to_json(_datetime: datetime) -> int:
    """This function converts datetime string from format 'YYYY-MM-DD hh:mm:ss TZ' to timestamp."""
    return int(_datetime.timestamp())


def db_date_time_tz_to_json(_db_date_time: str) -> int:
    """This function converts datetime string from format 'YYYY-MM-DD hh:mm:ss TZ' to timestamp."""
    # OLD: return timestr_to_sec(_db_date_time)
    time_tup = time.strptime(_db_date_time, "%Y-%m-%d %H:%M:%S%z")
    dt = datetime.datetime(time_tup[0], time_tup[1], time_tup[2], time_tup[3], time_tup[4], time_tup[5])
    #tzinfo=datetime.timezone.utc)
    return int(dt.timestamp())


def json_date_time_to_db_tz(_json_date_time: int) -> str:
    """This function converts timestamp to datetime string in format 'YYYY-MM-DD hh:mm:ss TZ'."""
    time_tuple = time.localtime(_json_date_time)
    dtstr = time.strftime("%Y-%m-%d %H:%M:%S%z", time_tuple)
    return dtstr


def extract_value_by_name(_str, _name):
    """This funcrion find in 'str' record <name>=<value>.
    If record found, return <value> else return empty string."""
    ind = _str.find(_name)
    if ind == -1:
        return ""
    startind = ind + len(_name)
    if _str[startind] != "=" or _str[startind + 1] != '"':
        return ""
    startind = startind + 2
    endind = startind
    while _str[endind] != '"':
        endind = endind + 1
    return _str[startind:endind]


def num_to_onoff(num):
    """This function converts boolean value 'num' (0/1) to 'ON'/'OFF'."""
    if num != 0:
        return "ON"
    else:
        return "OFF"


def bool_to_auto_off(flag: bool) -> str:
    """This function converts boolean value 'flag' (True/False) to 'AUTO'/'OFF'."""
    if flag:
        return "AUTO"
    else:
        return "OFF"


def num_to_defaultoff(num):
    """This function converts boolean value 'num' (0/1) to 'DEFAULT'/'OFF'."""
    if num != 0:
        return "DEFAULT"
    else:
        return "OFF"


def num_to_sign(num):
    """This function converts boolean value 'num' ("true"/"false") to '+'/'-'."""
    if num == "true":
        return "+"
    else:
        return "-"


def bool_str_to_int(_bool_str):
    if _bool_str == "True" or _bool_str == "true":
        return (1)
    if _bool_str == "False" or _bool_str == "False":
        return (0)
    return (0)


def bool_to_int(_bool):
    if _bool:
        return (1)
    else:
        return (0)


def int_to_bool(_num):
    if _num:
        return (True)
    else:
        return (False)


def genarate_random_string():
    """This function generates rendom string contains hex digits"""
    rndnum = random.random() * 0xFFFFFFFF
    rndstr = "%08X" % rndnum
    return rndstr


def extract_extension(fname):
    """This function extracts and returns extension of file 'fname'."""
    if len(fname) <= 1:
        return ""
    ind = len(fname) - 1
    while ind >= 0:
        if fname[ind] == ".":
            startind = ind + 1
            return fname[startind:]
        ind = ind - 1
    return ""


def zip_files(aname, flist):
    """This function create archve file 'aname' and add to it files from list 'flist'.
    aname - name of created archive file
    flist - list of file names, added to archive"""
    try:
        zf = zipfile.ZipFile(aname, 'w', zipfile.ZIP_DEFLATED)
        for fname in flist:
            zf.write(fname)
        zf.close()
    except:
        logging.error("function zip_files(): exception:" + str(sys.exc_info()))


# !!! NOTE: '_path' MUST BE IN ENCODING UTF-8 !!!
def is_dir(_path):
    """This function return boolean flag of file/path '_path'.
    If '_path' is directory then return True else False."""
    try:
        params = os.stat(_path)
        mode = params[stat.ST_MODE]
        b_dir = stat.S_ISDIR(mode)
        return b_dir
    except:
        return (1)


def get_file_size(_filename):
    """This function returns size of specified file '_filename' in bytes."""
    params = os.stat(_filename)
    size = params[stat.ST_SIZE]
    return size


def get_file_create_time(_filename) -> int:
    """This function returns creating time of specified file '_filename'."""
    params = os.stat(_filename)
    t_sec = params[stat.ST_CTIME]
    return t_sec


def get_files_list(_path):
    """This function returns list of files for directory '_path'. (variable '_path' must be in encoding 'UTF-8')
    Each list's item has format { "shortname": <string>, "fullname": <string>, "dir": <boolean> }."""

    flist = []
    try:
        tmp_files_list = os.listdir(_path)

        for fname in tmp_files_list:
            fullname = _path + SLASH + fname
            b_dir = is_dir(fullname)
            fsize = get_file_size(fullname)
            ftime = get_file_create_time(fullname)
            flist.append({"shortname": fname, "fullname": fullname, "dir": b_dir, "size": fsize, "create_time": ftime})

        # sort files list
        cnt = len(flist)
        i = 0
        while i < cnt - 1:
            j = i + 1
            while j < cnt:
                if flist[i]["shortname"] > flist[j]["shortname"]:
                    tmp = flist[i]
                    flist[i] = flist[j]
                    flist[j] = tmp
                j += 1
            i += 1

        return flist
    except:
        logging.error("function get_files_list(): exception:" + str(sys.exc_info()))
        return []  # error


def get_prot_pref_str(pref):
    """This function converts protocol preference to string format"""
    if pref != 0:
        return ("H323")
    else:
        return ("SIP")


def get_file_extension(filename):
    """This function extracts extension of file 'filename'."""
    name_len = len(filename)
    i = name_len - 1
    extension = ""
    while i >= 0:
        if filename[i] == ".":
            return (filename[i + 1:])
        i = i - 1
    return ("")  # no dots in filename - extension is empty


def invert_back_slash(str):
    """This function changes back slashes '\' in string 'str' to normal slashes '/'. Returns converted string."""
    conv_str = ""
    for i in range(len(str)):
        if str[i] == "\\":
            conv_str += "/"
        else:
            conv_str += str[i]
    return (conv_str)


def get_short_file_name(fname):
    """This function raturns short filename by full filename"""
    length = len(fname)
    if length == 0:
        return ("")
    i = length - 1
    while i >= 0:
        if (fname[i] == '/' or fname[i] == '\\') and len(fname) >= i + 2:
            return (fname[i + 1:])
        i = i - 1
    return (fname)


# def get_filling_file_system():
#  """This function returns percent of used blocks on disk"""
#  if sys.platform == "win32":
#    return(50)
#  else:
#    st = os.statvfs(".")
#    total_blocks      = st[statvfs.F_BLOCKS]
#    total_free_blocks = st[statvfs.F_BFREE]
#    used_blocks       = total_blocks - total_free_blocks 
#    return(used_blocks * 100 / total_blocks)


# OLD:
# def get_free_disk_space():
#  """This function returns free disk space in megabytes."""
#  if sys.platform == "win32":
#    return(1000)
#  else:
#    st = os.statvfs(".")
#    fundamental_block_size = st[statvfs.F_FRSIZE]
#    available_blocks       = st[statvfs.F_BAVAIL]
#    free_size              = fundamental_block_size * available_blocks / (1024*1024)
#    return(free_size)

def get_free_disk_space():
    """This function returns free disk space in megabytes."""
    if sys.platform == "win32":
        path = "c:\\"
    else:
        path = "/"
    disk = shutil.disk_usage(path)
    free_size = disk[2] / (1024 * 1024)
    return free_size


def get_dir_size(path, ext):
    """This function find files 'path/*.ext' , and calculates its size in megabytes.
    path   - directory name in whith find files
    ext    - extension of processed files"""
    fullsize = 0

    flist = os.listdir(path)
    for fname in flist:
        if fname == "." or fname == "..":
            continue
        curext = extract_extension(fname)
        if curext != ext:
            continue

        full_fname = path + "/" + fname

        if is_dir(full_fname):
            continue

        params = os.stat(full_fname)
        cur_size = params[stat.ST_SIZE]
        fullsize += cur_size / (1024.0 * 1024.0)
    return (fullsize)


def get_dir_size2(_dir):
    """This function calculates size of files in directory '_dir' (in megabytes)."""

    if sys.platform == "win32":
        return (1)  # dummy

    out, err = exec_cmd(["du", "-b", _dir], False)

    words = out.split()
    if len(words) == 0:
        return (0)

    size_in_bytes = int(words[0])
    size_in_megabytes = size_in_bytes / (1024.0 * 1024.0)
    return (size_in_megabytes)


def make_links_to_video(_path_to_files, _path_to_links, _ext):
    """This function makes in directory _path_to_links links to files _path_to_files/*._ext"""
    # DP rude fix!!!
    # flist = os.listdir(_path)
    flist = os.listdir(_path_to_files)  # is it correct ???

    for fname in flist:
        if fname == "." or fname == "..":
            continue
        curext = extract_extension(fname)
        if curext != _ext:
            continue

        full_fname = _path_to_files + "/" + fname
        full_lname = _path_to_links + "/" + fname

        if is_dir(full_fname):
            continue

        if sys.platform == "win32":  # windows
            cmd = "mklink %s %s" % (full_lname, full_fname)
        else:  # linux
            cmd = "ln -s %s %s" % (full_fname, full_lname)

        os.system(cmd)


def exec_cmd(cmd_list, useshell):
    """This function execute OS command 'cmd_list' and returns pair (stdout, stderr).
    'cmd_list' is lid as: [ "program", "arg1", "arg2", ... ].
    useshell = True/False"""

    # if sys.platform == "win32":
    #    sep = " "
    #    cmd = sep.join(cmd_list)
    #    os.system(cmd)
    #    return (("", ""))

    res = {}
    try:
        res = subprocess.Popen(cmd_list, shell=useshell, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except:
        return ("", str(sys.exc_info()))

    streams = res.communicate()

    if len(streams) != 2:
        result = ("", "error: bad streams count")
        return (result)  # error

    # if stderr is not empty
    if len(streams[1]) != 0:
        logging.error("exec_cmd(): " + str(cmd_list))
        logging.error(streams[1])

    return ((streams[0], streams[1]))


def exec_cmd2(cmd, useshell):
    """Execute command 'cmd'. Arguments must be separated by spaces. Each argument must not include spaces."""
    cmd_list = cmd.split(" ")
    res = exec_cmd(cmd_list, useshell)
    return (res)


def get_free_disk_space2():
    """This function returns free disk space in megabytes."""
    if sys.platform == "win32":
        return (1000)  # dummy

    # get sizes of file system "/"
    out, err = exec_cmd(["df", "/"], False)

    # split output table to lines
    out_lines = out.splitlines()
    if len(out_lines) != 2:
        return (0)  # error

    # get sizes (words) from line 2 (with data)
    words = out_lines[1].split()
    if len(words) < 4:
        return (0)  # error

    # extract available size
    size_kb = int(words[3])
    size_mb = size_kb / 1024.0
    return (size_mb)


def get_video_dir_size():
    """This function calculates size of video storage directory in megabytes."""

    if sys.platform == "win32":
        return (1)  # dummy

    ctrl_params = xmltools.get_config()
    out, err = exec_cmd(["du", "-b", ctrl_params["dirs"]["videodir"]], False)

    words = out.split()
    if len(words) == 0:
        return (0)

    size_in_bytes = int(words[0])
    size_in_megabytes = size_in_bytes / (1024.0 * 1024.0)
    return (size_in_megabytes)


def get_error_json(_http_code, _err_name, _err_str):
    _http_code = int(_http_code)
    _err_name = str(_err_name)
    _err_str = str(_err_str)
    return ({"error": {"id": _err_name, "message": _err_str, "status_code": _http_code}})


def json_to_str(_json_data):
    json_str = ""
    try:
        json_str = json.dumps(_json_data)
    except:
        return (""" "error": "json_to_str() exception: %s" """ % (str(sys.exc_info())))
    return (json_str)


def str_to_json(_str):
    """Note: before call this funection you must replace '"' to '\\"' for all string fields"""
    data_json = {}
    try:
        data_json = json.loads(_str)
    except:
        return ({"error": "str_to_json() exception: %s" % (str(sys.exc_info()))})
    return (data_json)


def copy_dict_fields(_src_dict, _dst_dict):
    """This function creates in dictionary _dst_dict fields of dictionary _src_dict."""

    for key, val in _src_dict.items():
        _dst_dict[key] = val


def is_presentation_file(_url: str) -> bool:
    prefix = "file:2@"
    pref_len = len(prefix)
    if pref_len > len(_url):  # no prefix in string
        return False
    else: # may be prefix in string
        return _url[:pref_len] == prefix


def parse_address(addr):
    """This function parse address 'addr' to 'protocol', 'username' and 'host+port'."""
    result = {"disp_name": "", "protocol": "", "username": "", "hostport": "", "host": "", "port": "", "path": ""}

    if addr == None or addr == "":
        return result

    # parse display name (if exist)
    addr = addr.strip()
    if len(addr) == 0:
        return (result)  # error
    if addr[0] == '"':
        addr = addr[1:]
        end_disp_name_ind = addr.find('"')
        if end_disp_name_ind == -1:
            return (result)  # error
        result["disp_name"] = addr[:end_disp_name_ind]
        addr = addr[end_disp_name_ind + 1:]

    # delete brackets if its exist
    addr = addr.strip()
    if len(addr) < 2: # uri with display name but without address
        return (result)  # error
    if addr[0] == "<" and addr[-1] == ">":
        addr = addr[1:-1]

    end_prot_ind = addr.find(":")
    if end_prot_ind == -1:
        result["host"] = addr
        return (result)  # error
    result["protocol"] = addr[0:end_prot_ind]
    addr_len = len(addr)

    # skip "//" after "protocol:"
    if addr_len > end_prot_ind + 3 and addr[end_prot_ind + 1] == "/" and addr[end_prot_ind + 2] == "/":
        end_prot_ind += 2

    if result["protocol"] == "media":
        if addr_len > end_prot_ind + 3 and addr[end_prot_ind + 1].isdigit() and addr[end_prot_ind + 2] == "@":
            end_prot_ind += 2
        result["path"] = addr[end_prot_ind + 1:addr_len]
        return result

    username_ind = addr.find("@")
    if username_ind == -1:
        result["hostport"] = addr[end_prot_ind + 1:addr_len]
    else:
        if end_prot_ind > username_ind:
            return (result)  # error
        result["username"] = addr[end_prot_ind + 1:username_ind]
        result["hostport"] = addr[username_ind + 1:addr_len]

    end_host_ind = result["hostport"].find(":")
    if end_host_ind != -1:
        result["host"] = result["hostport"][0:end_host_ind]
        portpath = result["hostport"][end_host_ind+1:]

        end_port_ind = portpath.find("/")
        if end_port_ind != -1:
            result["port"] = portpath[0:end_port_ind]
            result["path"] = portpath[end_port_ind:]
        else:
            result["port"] = portpath
            result["path"] = ""
    else:
        end_host_ind = result["hostport"].find("/")
        if  end_host_ind != -1:
            result["host"] = result["hostport"][0:end_host_ind]
            result["port"] = ""
            result["path"] = result["hostport"][end_host_ind:]
        else:
            result["host"] = result["hostport"]
            result["port"] = ""
            result["path"] = ""

    return (result)


def get_default_port(schema: str) -> int:
    SCHEMA = schema.upper()
    if SCHEMA == "SIP":
        return 5060
    elif SCHEMA == "H323":
        return 1720
    elif SCHEMA == "RTSP":
        return 554
    else:
        return 0 # error


def is_addr_equivalent(addr1, addr2, strict_compare=False):
    """This function compare sip/h323 addresses addr1 and addr2.
    If addresses is equivalent then returns 1 else returns 0."""
    parsed_addr1 = parse_address(addr1)
    parsed_addr2 = parse_address(addr2)

    if strict_compare:
        if parsed_addr1["username"] != parsed_addr2["username"]:
            return (0)
    else:
        if parsed_addr1["username"] != "" and parsed_addr2["username"] != "" and \
            parsed_addr1["username"] != parsed_addr2["username"]:
            return (0)

    port1 = parsed_addr1["port"]
    if len(port1) == 0:
        port1 = str(get_default_port(parsed_addr1["protocol"]))
    port2 = parsed_addr2["port"]
    if len(port2) == 0:
        port2 = str(get_default_port(parsed_addr2["protocol"]))
    if port1 != port2:
        return (0)

    if parsed_addr1["protocol"] == "media" and parsed_addr2["protocol"] == "media":
        filename1 = os.path.basename( urllib.parse.unquote(parsed_addr1["path"]) )
        loop_ind1 = filename1.find("#loop")
        if loop_ind1 >= 0:
            filename1 = filename1[:loop_ind1]
        filename2 = os.path.basename( urllib.parse.unquote(parsed_addr2["path"]) )
        loop_ind2 = filename2.find("#loop")
        if loop_ind2 >= 0:
            filename2 = filename2[:loop_ind2]
        return filename1 == filename2

    if parsed_addr1["path"] != "" and parsed_addr2["path"] != "" and parsed_addr1["path"] != parsed_addr2["path"]:
        return (0)
    if parsed_addr1["protocol"] == parsed_addr2["protocol"] and parsed_addr1["host"] == parsed_addr2["host"]:
        return (1)
    else:
        return (0)


def is_addr_group_equivalent(uri: str, sip: str, h323: str, rtsp: str, file: str, strict_compare=False) -> bool:
    return is_addr_equivalent(uri, sip, strict_compare) or \
           is_addr_equivalent(uri, h323, strict_compare) or \
           is_addr_equivalent(uri, rtsp, strict_compare) or \
           is_addr_equivalent(uri, file, strict_compare)


def join_parsed_address(parsed_address: dict) -> str:
    """This function build address of abonent from dictionary getted by function parse_address()."""

    if ("protocol" not in parsed_address) or ("host" not in parsed_address):
        return ""

    if len(parsed_address["protocol"]) != 0:
        addr = parsed_address["protocol"] + ":"
        if parsed_address["protocol"] == "media":
            addr += "1@"
    else:
        addr = ""

    if ("username" in parsed_address) and len(parsed_address["username"]) != 0:
        addr += parsed_address["username"] + "@"

    addr += parsed_address["host"]

    if ("port" in parsed_address) and len(parsed_address["port"]) != 0:
        addr += ":" + parsed_address["port"]

    if ("path" in parsed_address) and len(parsed_address["path"]) != 0:
        addr += parsed_address["path"]

    if ("disp_name" in parsed_address) and len(parsed_address["disp_name"]) != 0:
        addr = '"%s" <%s>' % (parsed_address["disp_name"], addr)

    return addr


def quote_address(addr: str) ->str:
    if addr == None or addr == "":
        return ""
    parsed_address = parse_address(addr)

    if parsed_address["protocol"] == "media":
        if ("path" in parsed_address) and len(parsed_address["path"]) != 0:
            parsed_address["path"] = urllib.parse.quote(parsed_address["path"])
    else:
        if ("username" in parsed_address) and len(parsed_address["username"]) != 0:
            parsed_address["username"] = urllib.parse.quote(parsed_address["username"])

    quoted_addr = join_parsed_address(parsed_address)
    return quoted_addr


def unquote_address(addr: str) -> str:
    if addr == None or addr == "":
        return ""
    parsed_address = parse_address(addr)

    if parsed_address["protocol"] == "media":
        if ("path" in parsed_address) and len(parsed_address["path"]) != 0:
            parsed_address["path"] = urllib.parse.unquote(parsed_address["path"])
    else:
        if ("username" in parsed_address) and len(parsed_address["username"]) != 0:
            parsed_address["username"] = urllib.parse.unquote(parsed_address["username"])

    quoted_addr = join_parsed_address(parsed_address)
    return quoted_addr


def is_hex_digit(_symbol) -> bool:
    if len(_symbol) != 1:
        return False
    return (ord(_symbol) >= ord("0") and ord(_symbol) <= ord("9")) or \
        (ord(_symbol) >= ord("a") and ord(_symbol) <= ord("f")) or \
        (ord(_symbol) >= ord("A") and ord(_symbol) <= ord("F"))


def is_string_quoted(_string: str) -> bool:
    str_size = len(_string)
    for i in range(str_size):
        if _string[i] == "%" and i+2<str_size and is_hex_digit(_string[i+1]) and is_hex_digit(_string[i+2]):
            return True
    return False


def is_addr_quoted(addr: str) -> bool:
    if addr == None or addr == "":
        return False
    parsed_address = parse_address(addr)

    if parsed_address["protocol"] == "media":
        if ("path" in parsed_address) and len(parsed_address["path"]) != 0:
            return is_string_quoted(parsed_address["path"])
    else:
        if ("username" in parsed_address) and len(parsed_address["username"]) != 0:
            return is_string_quoted(parsed_address["username"])

    return False


def is_correct_sip_address(_sip_addr: str) -> bool:
    #sip_reg_exp = """^sip:([0-9a-zA-Z_\-+]*@)?([0-9a-zA-Z+]+(\.[0-9a-zA-Z]+)*)(:[0-9]{1,5})?$"""
    #sip_reg_exp = """^sip:([\w\-+ ]*@)?([0-9a-zA-Z+]+(\.[0-9a-zA-Z]+)*)(:[0-9]{1,5})?$"""
    username = """[\w\-+ \;\/\?\:\&\=\_\.\!\~\*\(\)\$\,%]+"""
    host = """[0-9a-zA-Z+\-]+"""
    port = """[0-9]{1,5}"""
    prm_char = """[A-Za-z0-9\[\]\/\:\&\+\$\-\_\.\!\~\*\(\)]"""
    prm_name = prm_char + "+"
    prm_val = prm_char + "*"
    params = """(;%s=%s)*""" % (prm_name, prm_name)
    tail = """(\?%s=%s(&%s=%s)*){0,1}""" % (prm_name, prm_val, prm_name, prm_val)
    sip_reg_exp = """^sip:(%s@)?(%s(\.%s)*)(:%s)?%s%s$""" % (username, host, host, port, params, tail)
    found_list = re.findall(sip_reg_exp, _sip_addr)
    if len(found_list) != 0:
        return True
    else:
        return False


def is_correct_h323_address(_h323_addr: str) -> bool:
    #h323_reg_exp = """^h323:([0-9a-zA-Z_\-+]*@)?([0-9a-zA-Z+]+(\.[0-9a-zA-Z]+)*)(:[0-9]{1,5})?$"""
    #h323_reg_exp = """^h323:([\w\-+ ]*@)?([0-9a-zA-Z+]+(\.[0-9a-zA-Z]+)*)(:[0-9]{1,5})?$"""
    username = """[\w\-+ \;\/\?\:\&\=\_\.\!\~\*\(\)\$\,\\\\%]+"""
    host = """[0-9a-zA-Z+\-]+"""
    port = """[0-9]{1,5}"""
    url_prm = """[\w\-+\/\?\:\&\=\_\.\!\~\*\(\)\$\,]+"""
    url_params = """(;%s)*""" % (url_prm)
    h323_reg_exp = """^h323:(%s@)?(%s(\.%s)*)(:%s)?%s$""" % (username, host, host, port, url_params)
    found_list = re.findall(h323_reg_exp, _h323_addr)
    if len(found_list) != 0:
        return True
    else:
        return False


def is_correct_rtsp_address(_rtsp_addr: str) -> bool:
    """NOTE: used rtsp format is: rtsp:[//][login[:passwd]@]host_word[.host_word]...[.host_word][:port][/path_word[/path_word]...][?prm_name1=prm_value1[&prm_name2=prm_value2]...]"""

    # OLD:rtsp_reg_exp = """^rtsp:(\/\/){0,1}([0-9a-zA-Z_\-]+(\.[0-9a-zA-Z_\-]+)*)(:[0-9]{1,5}){0,1}(\/[0-9a-zA-Z_\-\.]+)*(\/){0,1}$"""
    login = """[0-9a-zA-Z_\-\+\.\~\!\$\&\(\)\*\,\;\=\:%]+"""
    passwd = """[0-9a-zA-Z_\-\+]+"""
    host_word = """[0-9a-zA-Z_\-]+"""
    port = """[0-9]{1,5}"""
    path_word = """[0-9a-zA-Z_\-\.:]+"""
    prm_name = """[0-9a-zA-Z_\-\.\+]+"""
    prm_value = """[0-9a-zA-Z_\-\.\+]+"""
    tail = """(\?%s=%s(&%s=%s)*){0,1}""" % (prm_name, prm_value, prm_name, prm_value)
    rtsp_reg_exp = """^rtsp:(\/\/){0,1}(%s(:%s){0,1}@){0,1}(%s(\.%s)*)(:%s){0,1}(\/%s)*%s$""" % (
    login, passwd, host_word, host_word, port, path_word, tail)

    found_list = re.findall(rtsp_reg_exp, _rtsp_addr)
    if len(found_list) != 0:
        return True
    else:
        return False


def is_correct_file_uri(_file_uri: str) -> bool:
    file_reg_exp = """^media:(\\d@){0,1}.+$"""

    found_list = re.findall(file_reg_exp, _file_uri)
    if len(found_list) != 0:
        return True
    else:
        return False


def is_rtsp_prefix(abonent_uri: str) -> bool:
    if len(abonent_uri) > 4 and abonent_uri[:4] == "rtsp":
        return True
    else:
        return False


def add_prefix_if_not(_string: str, _prefix: str) -> str:
    pref_len = len(_prefix)
    if pref_len > len(_string):  # no prefix in string
        return _prefix + _string
    else:  # may be prefix in string
        if _string[:pref_len] == _prefix:  # prefix in string
            return _string
        else:
            return _prefix + _string


def delete_protocol_and_port(_addr: str) -> str:
    """This function return address without protocol and port"""
    if _addr.startswith(("sip:", "h323:")):
        parsed_addr = parse_address(_addr)
        parsed_addr["protocol"] = ""
        parsed_addr["port"] = ""
        new_addr = join_parsed_address(parsed_addr)
        return new_addr
    else:
        return _addr


def extract_conference(confstr, confid):
    """This function extracts conference 'confid' getted by http request 'http get /conferences.json'
    Input params:
    confstr - string with 'http get' responce (see function get_url_data())
    Return value:
    conference with id 'confid'
    at error returns empty dictionary"""
    conflist = []  # default conferences list
    if confstr == "":
        return ({})
    true = True  # define constants "true"/"false"  (they is present in input expression "connstr")
    false = False
    allconfdict = {}
    try:
        allconfdict = eval(confstr)  # convert string to python expression (json format)
    except:
        logging.error("extract_conference(): exception " + str(sys.exc_info()))
        return ({})
    if "conference_list" not in allconfdict:
        return ({})
    conflist = allconfdict["conference_list"]  # extract conferences list
    for conf in conflist:
        if conf["name"] == "conference" + str(confid):  # select conference with specified name/id
            return (conf)
    return ({})


def print_stack():
    stack_list = traceback.format_stack()
    for line in stack_list:
        print(line)


def log_stack():
    stack_list = traceback.format_stack()
    for line in stack_list:
        logging.error(line)


def get_frame_params(_confid):
    """This function returns frame params of conference '_confid'."""
    _confid = int(_confid)

    result = {"width": 1920, "height": 1080, "fps": 30}

    size_rows = dbtools.select(
        "SELECT width, height, fps FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            _confid))
    if len(size_rows) != 0:
        result["width"] = size_rows[0][0]
        result["height"] = size_rows[0][1]
        result["fps"] = size_rows[0][2]

    return (result)


def is_some_conf_launched() -> bool:
    conf_rows = dbtools.select("SELECT id FROM legacy.conferences WHERE state=%d OR state=%d OR state=%d" % (
        data.CONF_STATE_ACTIVE, data.CONF_STATE_START, data.CONF_STATE_CLOSING_VILE))

    return len(conf_rows) != 0


def is_param_match(_param, _filter):
    """This function find substring '_filter' in string '_param'. If found returns 1 else returns 0."""

    if _filter == "":
        return (1)

    PARAM = _param.upper()
    FILTER = _filter.upper()

    substr_ind = PARAM.find(FILTER)
    if substr_ind != -1:
        return (1)
    else:
        return (0)


def get_layout_name_by_id(_conference_id, _layout_id):
    _conference_id = int(_conference_id)
    _layout_id = int(_layout_id)
    return "layout%d_%d" % (_conference_id, _layout_id)


def get_layout_id_by_name(_layout_name):
    prefix = "layout"
    prefix_len = len(prefix)

    if len(_layout_name) <= prefix_len:
        return 0  # error
    if _layout_name[0:prefix_len] != prefix:
        return 0  # error

    ids_str = _layout_name[prefix_len:]

    separator_ind = ids_str.find("_")
    if separator_ind == -1:
        return 0  # error

    confid_str = ids_str[:separator_ind]
    if not confid_str.isdigit():
        return 0 # error

    layout_id_str = ids_str[separator_ind+1:]
    if not layout_id_str.isdigit():
        return 0 # error

    return int(layout_id_str)


CROWN_LAYOUT_PREFIX = "crown_layout_"


def is_crown_layout_name(lid: str) -> bool:
    if lid.find(CROWN_LAYOUT_PREFIX) == 0:
        return True
    else:
        return False


def confid_to_crown_layout_name(confid: int) -> str:
    """Build crown layout name by conference id"""
    return CROWN_LAYOUT_PREFIX + str(confid)


def crown_layout_name_to_confid(lid: str) -> int:
    """Extract conference id from crown layout name"""

    prefix_len = len(CROWN_LAYOUT_PREFIX)
    if len(lid) <= prefix_len:
        return 0  # error

    if lid[0:prefix_len] != CROWN_LAYOUT_PREFIX:
        return 0  # error

    confid_str = lid[prefix_len:]
    if confid_str.isdigit():
        return int(confid_str)
    else:
        return 0  # error


def abid_to_abook_name(abid: int) -> str:
    return "abonent%d" % (abid)


def conference_name_by_id(confid: int) -> str:
    return "conference%d" % (confid)


def get_engine_dir():
    if "VTSRV_DATA" in os.environ:
        return os.environ["VTSRV_DATA"]
    elif sys.platform == "win32" and ("APPDATA" in os.environ):
        return os.environ["APPDATA"] + SLASH + "vtsrv"
    elif "HOME" in os.environ:
        return os.environ["HOME"] + SLASH + ".vtsrv"
    else:
        return "." + SLASH + ".vtsrv"


def get_license_dir():
    return get_engine_dir() + SLASH + "0"


def get_default_license_fname():
    return get_license_dir() + SLASH + "license.xml"


def get_license_unique_fname():
    """This function returns unique file name to save license."""
    timestamp_str = get_unique_time_stamp_str()
    fname = get_license_dir() + SLASH + "license_" + timestamp_str + ".xml"
    return fname


def get_license_unique_fname2():
    default_license_fname = get_default_license_fname()
    if not os.path.exists(default_license_fname):
        dst_license_file = default_license_fname
    else:
        dst_license_file = get_license_unique_fname()

    return dst_license_file


def get_versions_fname():
    return get_license_dir() + SLASH + "version.json"


def id_to_db_num(_id):
    if _id == None:
        return 0
    else:
        return _id


def db_num_to_id(_db_num):
    if _db_num == 0:
        return None
    else:
        return _db_num


def str_to_int(_str: str) -> int:
    if _str.isdigit():
        return int(_str)
    else:
        return 0


def str_to_dbstr(_str: str) -> str:
    return _str.replace("'", "''")


def is_valid_ip(ip: str) -> bool:
    try:
        ip_obj = ipaddress.IPv4Address(ip)
    except:
        return False
    return True


def get_detail_error_item(reason: str, description: str, position: str, payload: dict) -> dict:
    """This function build one item of list 'details' in mcu error."""
    result = {"reason": reason, "description": description, "position": position}
    if payload != None and payload != {}:
        result["payload"] = payload
    return result


def crm_error_to_mcu_error(reason: str, description: str, crm_err_str: str) -> dict:
    """This fuction build one item of list 'details' in mcu error.
    Source error is license server error."""
    try:
        payload_json = json.loads(crm_err_str)
    except:
        payload_json = {}
        logging.error("crm_error_to_mcu_error(): cannot parse json: " + crm_err_str)

    position = ""
    if ("errors" in payload_json) and len(payload_json["errors"]) != 0 and ("location" in payload_json["errors"][0]):
        position = payload_json["errors"][0]["location"]

    mcu_err_dict = get_detail_error_item(reason,
                                         description,
                                         position,
                                         payload_json)
    return mcu_err_dict


def delete_file(fname: str):
    if sys.platform == "win32":
        cmd = 'del /Q "%s"' % (fname)
    else:
        cmd = 'rm -f "%s"' % (fname)
    os.system(cmd)


def save_text_to_file(_fname: str, _text: str) -> bool:
    try:
        f = open(_fname, "wb")
        data = _text.encode("UTF-8")
        f.write(data)
        f.close()
    except:
        logging.error("save_text_to_file() Exception: " + str(sys.exc_info()))
        return False

    return True


def id_list_to_db_list(id_list: list) -> str:
    """Convert list on int values to string '(id1, id2, ...)'."""
    id_count = len(id_list)
    if id_count == 0:
        return ""

    id_list_str = "("
    for i in range(id_count):
        id_list_str += "%d" % (id_list[i])
        if i != id_count - 1:
            id_list_str += ","
    id_list_str += ")"

    return id_list_str






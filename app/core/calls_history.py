#1/usr/bin/python3

import logging
import sys

import app.core.dbtools as dbtools
import app.core.tools as tools
import app.core.xmltools as xmltools
from app.core.requests import AddCallsHistoryEventRequest


def get_calls_history_fname():
    cfg = xmltools.get_config()
    return cfg["dirs"]["logsdir"] + tools.SLASH + "calls_history.log"


#TMP: simple version for Elbrus
#TODO: write calls history to database
def add_event(request: AddCallsHistoryEventRequest):
    """this function write to calls history one event"""

    conf_rows = dbtools.select("SELECT name FROM legacy.conferences WHERE id=%d AND deleted_at is null" %
                               (request.conference_id))
    if len(conf_rows) != 0:
        conf_name = conf_rows[0][0].strip()
    else:
        conf_name = "conference %d" % (request.conference_id)

    if request.outgoing:
        outgoing_str = "Outgoing"
    else:
        outgoing_str = "Incoming"

    if request.connected:
        conn_str = "added to conference"
    else:
        conn_str = "removed from confeence"

    dt = tools.get_time_stamp_str()
    message = "%s: %s call: participant '%s' %s '%s' \n" % \
              (dt, outgoing_str, request.participant_uri, conn_str, conf_name)

    fname = get_calls_history_fname()
    try:
        f = open(fname, "a")
        f.write(message)
        f.close()
    except:
        logging.error("add_event() error: " + str(sys.exc_info()))



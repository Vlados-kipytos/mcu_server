#!/usr/bin/python3


import logging

import app.core.dbtools as dbtools
from app.core.errors import SettingIsRestrictedByLicense


def set_features(_features_str):
    """This function update in DB supported features"""

    dbtools.make_sql_request("UPDATE legacy.commonconfig SET features='%s'" % (_features_str))


def get_features_list():
    """This function returns list of strings with feature names"""

    features_list = []
    rows = dbtools.select("SELECT features FROM legacy.commonconfig")
    if len(rows) != 0:
        features_str = rows[0][0]
        features_list = features_str.split()

    return (features_list)


def get_feature(_features_list, _feature_name):
    """This function find feature _feature_name in list _features_list. If found then return 1 else return 0."""

    if len(_features_list) <= 1:  # protect from bad features list
        return (1)

    for word in _features_list:
        if word == _feature_name:
            return (1)

    return (0)


# individual features for each conference
CONFERENCE_FEATURES_LIST = [
    {"name": "AAC", "dbfields": ["aac"], "apinames": ["aac"]},
    {"name": "G.711", "dbfields": ["g711"], "apinames": ["g711"]},
    {"name": "G.722", "dbfields": ["g722"], "apinames": ["g722"]},
    {"name": "G.722.1", "dbfields": ["g7221_24", "g7221_32"], "apinames": ["g722_1_24", "g722_1_32"]},
    {"name": "G.722.1C", "dbfields": ["g7221c_24", "g7221c_32", "g7221c_48"],
     "apinames": ["g722_1c_24", "g722_1c_32", "g722_1c_48"]},
    {"name": "G.723", "dbfields": ["g7231"], "apinames": ["g723_1"]},
    {"name": "G.726", "dbfields": ["g726"], "apinames": ["g726"]},
    {"name": "G.728", "dbfields": ["g728"], "apinames": ["g728"]},
    {"name": "G.729", "dbfields": ["g729"], "apinames": ["g729"]},
    {"name": "G.719", "dbfields": ["g719"], "apinames": ["g719"]},
    {"name": "H.261", "dbfields": ["h261"], "apinames": ["h261"]},
    {"name": "H.263", "dbfields": ["h263"], "apinames": ["h263"]},
    {"name": "H.264", "dbfields": ["h264"], "apinames": ["h264"]},
    {"name": "H.264-HIGH", "dbfields": ["h264high"], "apinames": ["h264_high"]},
    {"name": "H.265", "dbfields": ["h265"], "apinames": ["h265"]},
    {"name": "PRESENTATION", "dbfields": ["use_pres"], "apinames": ["use_pres"]}
]


# OLD: {"name": "ADAPTIVE_RATE_CONTROL", "dbfields": ["ratectrl", "adaptrate", "fec", ], "apinames": ["stream_control", "adapt_bitrate", "packet_recovery"] }


def update_conf_features(_features_list):
    """This function check all individual features of conferences and reset unsupported features in DB."""

    for f in CONFERENCE_FEATURES_LIST:
        supported = get_feature(_features_list, f["name"])
        logging.debug("update_conf_features(): feature %s, support = %d" % (f["name"], supported))
        if not supported:
            dbfields = f["dbfields"]
            for field in dbfields:
                sql = "UPDATE legacy.configurations SET %s=0" % (field)
                dbtools.make_sql_request(sql)
                logging.debug("update_conf_features(): sql = '%s'" % (sql))

                sql = "UPDATE legacy.abonents SET %s=0" % (field)
                dbtools.make_sql_request(sql)
                logging.debug("update_conf_features(): sql = '%s'" % (sql))

    # TMP:
    err = dbtools.get_last_error()
    if err != "":
        logging.error("update_conf_features(): DB error: " + err)


def is_conf_setting_supported(_features_list, _setting_name):
    """This function check conference setting '_setting_name'. If it allow by license, then return True."""
    for feature in CONFERENCE_FEATURES_LIST:
        for apiname in feature["apinames"]:
            if apiname == _setting_name:
                enabled = get_feature(_features_list, feature["name"])
                if enabled:
                    return True
    return False


def validate_conf_setting(_features_list, _setting_name):
    """This function check conference setting '_setting_name'. If it allow by license, then return True.
    If it restricted by license, then raise exception SettingIsRestrictedByLicense.
    If it is not exist, then return False."""

    for feature in CONFERENCE_FEATURES_LIST:
        setting_found = False
        for apiname in feature["apinames"]:
            if apiname == _setting_name:
                setting_found = True
                break

        if setting_found:
            enabled = get_feature(_features_list, feature["name"])
            if enabled:
                return True
            else:
                raise SettingIsRestrictedByLicense(_setting_name)

    # api setting is not found: unknown settings is not supported
    logging.error(
        "validate_conf_setting(): api setting '%s' is not found: unknown settings is not supported " % (_setting_name))
    return False


GLOBAL_FEATURES_LIST = [
    {"name": "FECC", "dbfields": ["fecc"]},
    {"name": "H323", "dbfields": ["useh323"]},
    {"name": "SIP", "dbfields": ["usesip"]},
    {"name": "ADAPTIVE_RATE_CONTROL", "dbfields": ["adaptrate", "ratectrl", "fec"]}
]


def update_global_features(_features_list):
    """This function check global features and reset unsupported features in DB."""

    for f in GLOBAL_FEATURES_LIST:
        supported = get_feature(_features_list, f["name"])
        if not supported:
            for field in f["dbfields"]:
                dbtools.make_sql_request("UPDATE legacy.commonconfig SET %s=0" % (field))

    # TMP:
    err = dbtools.get_last_error()
    if err != "":
        logging.error("update_global_features(): DB error: " + err)

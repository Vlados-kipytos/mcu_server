#!/usr/bin/python3

import os
import os.path
import sys

from aiohttp import streamer

import app.core.reservedir as reservedir
import app.core.tools as tools
import app.core.xmltools as xmltools
from .entities import FileEntity
from .errors import BadFileExtensionError
from .errors import DirectoryNotFoundError
from .errors import HandlingFileError
from .errors import MCUFileNotFoundError
from .requests import DeleteFileRequest
from .requests import GetFileRequest
from .requests import GetFilesListRequest
from .requests import UploadFileRequest
from .requests import UploadFileByChunksRequest
from .responses import GetFileResponse
from .responses import GetFilesListResponse
from .responses import HandlingFileResponse


SLASH = os.path.sep


def get_file_types_params():
    ctrl_params = xmltools.get_config()

    LOGS_DIR = ctrl_params["dirs"]["logsdir"]
    VIDEO_REC_DIR = ctrl_params["dirs"]["videodir"]
    ADDRBOOK_CSV_DIR = ctrl_params["dirs"]["addrbookcsv"]
    ADDRBOOK_XML_DIR = ctrl_params["dirs"]["addrbookxml"]
    DBCOPY_DIR = ctrl_params["dirs"]["dbcopy"]
    PRESENTATION_DIR = ctrl_params["dirs"]["presentation"]
    UTILS_DIR = ctrl_params["dirs"]["utils"]
    UPDATE_MCU_DIR = ctrl_params["dirs"]["updatemcu"]

    SHARED_FOLDER = ctrl_params["dirs"]["shareddir"]

    FILE_TYPES_LIST = [
        {"type": "videorecord", "dir": VIDEO_REC_DIR, "extensions": ["mkv"]},
        {"type": "log", "dir": LOGS_DIR, "extensions": ["*"]},
        {"type": "addrbookcsv", "dir": ADDRBOOK_CSV_DIR, "extensions": ["csv"]},
        {"type": "addrbookxml", "dir": ADDRBOOK_XML_DIR, "extensions": ["xml"]},
        {"type": "dbcopy", "dir": DBCOPY_DIR, "extensions": ["dump"]},
        {"type": "presentation", "dir": PRESENTATION_DIR,
         "extensions": ["bmp", "png", "jpg", "jpeg", "gif", "tif", "tiff"]},
        {"type": "data", "dir": SHARED_FOLDER, "extensions": []},
        {"type": "utildata", "dir": UTILS_DIR, "extensions": ["*"]},
        {"type": "updatemcu", "dir": UPDATE_MCU_DIR, "extensions": ["deb"]}]

    return FILE_TYPES_LIST


# NOTE: FOR INTERNAL USE ONLY
def get_file_params(_file_type: str):
    file_types_prm_list = get_file_types_params()

    for i in range(len(file_types_prm_list)):
        if file_types_prm_list[i]["type"] == _file_type:
            return file_types_prm_list[i]

    return file_types_prm_list[0]  # error


def get_file_types_list():
    file_types_prm_list = get_file_types_params()

    types_list = []
    for ft in file_types_prm_list:
        types_list.append(ft["type"])

    return types_list


def is_extension_correct(_file_type: str, _extension: str):
    """This function checks: is '_extension' correct for '_file_type'."""

    ftype_params = get_file_params(_file_type)

    ext_list = ftype_params["extensions"]

    EXTENSION = _extension.upper()

    ext_found = False
    for cur_ext in ext_list:
        if cur_ext == "*":
            ext_found = True
            break
        CUR_EXT = cur_ext.upper()
        if CUR_EXT == EXTENSION:
            ext_found = True
            break

    return ext_found


def get_directory(_file_type: str):
    file_types_prm_list = get_file_types_params()

    if _file_type == "dbcopy" and reservedir.is_use_net_folder():
        if not reservedir.is_network_dir_mounted():
            reservedir.mount_net_dir()
        return reservedir.get_net_reserve_folder()

    for i in range(len(file_types_prm_list)):
        if file_types_prm_list[i]["type"] == _file_type:
            return file_types_prm_list[i]["dir"]

    return file_types_prm_list[0]["dir"]  # error


def get_files_list(request: GetFilesListRequest) -> GetFilesListResponse:
    """This function returns list of files of specified type."""

    ftype_params = get_file_params(request.filetype)

    dir = get_directory(request.filetype)
    ext_list = ftype_params["extensions"]

    if not os.path.exists(dir):
        raise DirectoryNotFoundError(dir, "file_type")

    flist = tools.get_files_list(dir)

    out_files_list = []
    for file in flist:
        if file["dir"]:
            continue

        file_ext = tools.get_file_extension(file["shortname"])
        ext_correct = is_extension_correct(request.filetype, file_ext)
        if not ext_correct:
            continue

        out_files_list.append(FileEntity(file["shortname"], file["size"], file["create_time"]))

    return GetFilesListResponse(out_files_list)


def get_file(request: GetFileRequest) -> GetFileResponse:
    """This function returns binary content of specified file."""

    dir = get_directory(request.filetype)

    if not os.path.exists(dir):
        raise DirectoryNotFoundError(dir, "file_type")

    full_fname = dir + SLASH + request.filename

    if not os.path.exists(full_fname):
        raise MCUFileNotFoundError(full_fname, "file_name")

    data = b""
    try:
        f = open(full_fname, "rb")
        data = f.read()
        f.close()
    except:
        raise HandlingFileError(full_fname)

    return GetFileResponse(data)


@streamer
async def get_file_by_chunks(writer, request: GetFileRequest):
    """This function returns binary content of specified file."""

    dir = get_directory(request.filetype)

    if not os.path.exists(dir):
        raise DirectoryNotFoundError(dir, "file_type")

    full_fname = dir + SLASH + request.filename

    if not os.path.exists(full_fname):
        raise MCUFileNotFoundError(full_fname, "file_name")

    try:
        with open(full_fname, 'rb') as f:
            chunk = f.read(2 ** 16)
            while chunk:
                await writer.write(chunk)
                chunk = f.read(2 ** 16)
            f.close()
    except:
        raise HandlingFileError(full_fname)


def upload_file(request: UploadFileRequest) -> HandlingFileResponse:
    dir = get_directory(request.filetype)

    if not os.path.exists(dir):
        raise DirectoryNotFoundError(dir, "file_type")

    file_ext = tools.get_file_extension(request.filename)
    ext_correct = is_extension_correct(request.filetype, file_ext)
    if not ext_correct:
        raise BadFileExtensionError(file_ext, "file_type")

    full_fname = dir + SLASH + request.filename

    try:
        f = open(full_fname, "wb")
        f.write(request.data)
        f.close()
    except:
        raise HandlingFileError(full_fname)

    return HandlingFileResponse(full_fname)


async def upload_file_by_chunks(request: UploadFileByChunksRequest) -> HandlingFileResponse:
    dir = get_directory(request.filetype)

    if not os.path.exists(dir):
        raise DirectoryNotFoundError(dir, "file_type")

    file_ext = tools.get_file_extension(request.filename)
    ext_correct = is_extension_correct(request.filetype, file_ext)
    if not ext_correct:
        raise BadFileExtensionError(file_ext, "file_type")

    full_fname = dir + SLASH + request.filename

    try:
        f = open(full_fname, "wb")
        while True:
            chunk = await request.reader.read_chunk(65536)
            if not chunk:
                break
            f.write(chunk)
        f.close()
    except:
        raise HandlingFileError(full_fname)

    return HandlingFileResponse(full_fname)


def delete_file(request: DeleteFileRequest) -> HandlingFileResponse:
    """this function delete file with specified name and type."""

    dir = get_directory(request.filetype)

    if not os.path.exists(dir):
        raise DirectoryNotFoundError(dir, "file_type")

    full_fname = dir + SLASH + request.filename

    if not os.path.exists(full_fname):
        raise MCUFileNotFoundError(full_fname, "file_name")

    if sys.platform == "win32":
        out, err = tools.exec_cmd(["del", "/Q", "%s" % (full_fname)], True)
        # logging.error( "delete_file(): stdout: " + out )
        # logging.error( "delete_file(): stderr: " + err )
    else:
        out, err = tools.exec_cmd(["rm", "-f", "-v", full_fname], False)
        # logging.error( "delete_file(): stdout: " + out )
        # logging.error( "delete_file(): stderr: " + err )

    err = err.strip()
    if len(err) != 0:
        raise HandlingFileError(full_fname)

    return HandlingFileResponse(full_fname)

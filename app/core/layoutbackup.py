#!/usr/bin/python3

import logging

import app.core.data as data
import app.core.dbtools as dbtools
import app.core.layoutcfgtools as layoutcfgtools
import app.core.tools as tools
from app.core.entities import LayoutAbonentPositionEntity
from app.core.requests import ChangeOnlineLayoutRequest
from app.core.requests import CreateLayoutBackupRequest
from app.core.requests import CreateOnlineLayoutRequest
from app.core.responses import SheduledLayoutAspeakerResponse
from app.core.responses import SheduledLayoutGlobalResponse
from app.core.responses import SheduledLayoutIndividualResponse


def get_layout_id_by_type(confid: int, layout_type: int, template: bool) -> int:
    """Get single copy of layout of specified type"""
    confid = int(confid)
    layout_type = int(layout_type)
    template_num = tools.bool_to_int(template)
    layout_rows = dbtools.select("""SELECT id FROM legacy.layoutsprmbackup WHERE layout_type=%d AND template=%d AND 
                                 id IN (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d)""" %
                                 (layout_type, template_num, confid))
    if len(layout_rows) == 0:
        return (0)  # error: bad global layout id
    layout_id = layout_rows[0][0]
    return layout_id


#OLD:
#def get_global_layout_id(confid: int) -> int:
#    confid = int(confid)
#    layout_rows = dbtools.select("SELECT id FROM legacy.layoutconfbackup WHERE confid=%d and abid=0" % (confid))
#    if len(layout_rows) == 0:
#        return (0)  # error: bad global layout id
#    layout_id = layout_rows[0][0]
#    return layout_id

def get_global_layout_id(confid: int, template: bool) -> int:
    return get_layout_id_by_type(confid, data.LAYOUT_TYPE_GLOBAL, template)


def get_individual_layout_id(confid: int, abid: int, template: bool) -> int:
    confid = int(confid)
    abid = int(abid)
    template_num = tools.bool_to_int(template)
    layout_rows = dbtools.select("""SELECT id FROM legacy.layoutsprmbackup WHERE layout_type=%d AND template=%d AND 
                                 id IN (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d AND abid=%d)""" %
                                 (data.LAYOUT_TYPE_INDIVIDUAL, template_num, confid, abid))
    if len(layout_rows) == 0:
        return (0)  # error: bad individual layout id
    layout_id = layout_rows[0][0]
    return layout_id


def get_layout_type(layout_id: int) -> int:
    layout_rows = dbtools.select("""SELECT layout_type FROM legacy.layoutsprmbackup WHERE id=%d""" % (layout_id) )
    if len(layout_rows) != 0:
        layout_type = layout_rows[0][0]
        return layout_type
    else:
        return data.LAYOUT_TYPE_GLOBAL  # error


def delete_layout_backup(confid: int, layout_id: int, del_layout_conf: bool):
    result = dbtools.make_sql_request("DELETE FROM legacy.layoutsprmbackup WHERE id=%d" % (layout_id))
    if result == 0:
        logging.error("delete_layout_backup() prepare layoutsprmbackup, db error: " + dbtools.get_last_error())

    result = dbtools.make_sql_request(
        "DELETE FROM legacy.layoutbackup WHERE layout_id=%d" % (layout_id))
    if result == 0:
        logging.error("delete_layout_backup() prepare layoutbackup, db error: " + dbtools.get_last_error())

    if del_layout_conf:
        result = dbtools.make_sql_request("DELETE FROM legacy.layoutconfbackup WHERE id=%d" % (layout_id))
        if result == 0:
            logging.error("delete_layout_backup() prepare layoutconfbackup, db error: " + dbtools.get_last_error())


def delete_all_layout_backups(confid: int):
    result = dbtools.make_sql_request(
        "DELETE FROM legacy.layoutsprmbackup WHERE id IN (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d)" % (
            confid))
    if result == 0:
        logging.error("delete_all_layout_backups() prepare layoutsprmbackup, db error: " + dbtools.get_last_error())

    result = dbtools.make_sql_request("""DELETE FROM legacy.layoutbackup WHERE layout_id IN 
                                        (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d)""" % (confid))
    if result == 0:
        logging.error("delete_all_layout_backup() db error: " + dbtools.get_last_error())

    result = dbtools.make_sql_request("DELETE FROM legacy.layoutconfbackup WHERE confid=%d" % (confid))
    if result == 0:
        logging.error("delete_all_layout_backups() prepare layoutconfbackup, db error: " + dbtools.get_last_error())


def clean_all_layout_backups():
    result = dbtools.make_sql_request("DELETE FROM legacy.layoutsprmbackup")
    if result == 0:
        logging.error("clean_all_layout_backups() prepare layoutsprmbackup, db error: " + dbtools.get_last_error())

    result = dbtools.make_sql_request("DELETE FROM legacy.layoutbackup")
    if result == 0:
        logging.error("clean_all_layout_backups() db error: " + dbtools.get_last_error())

    result = dbtools.make_sql_request("DELETE FROM legacy.layoutconfbackup")
    if result == 0:
        logging.error("clean_all_layout_backups() prepare layoutconfbackup, db error: " + dbtools.get_last_error())


def create_layout_backup(request: CreateLayoutBackupRequest):
    # if request.cells_count == 0:
    #    logging.error("create_layout_backup() bad schema_type: " + str(request.schema_type))
    #    return  # error

    if request.confid <= 0:
        logging.error("create_layout_backup() bad conference id: " + str(request.confid))
        return  # error

    if request.layout_id <= 0:
        logging.error("create_layout_backup() bad layout id: " + str(request.layout_id))
        return  # error

    # add new backup
    auto_num = request.auto
    display_name = tools.str_to_dbstr(request.display_name)
    template_num = tools.int_to_bool(request.template)
    sql = """INSERT INTO legacy.layoutsprmbackup 
            (id, name, fix_type, fix_abonent, dictors_cnt, aspeaker, maxab, auto, layout_type, template) 
             VALUES (%d, '%s', 0, 0, %d, 0, %d, %d, %d, %d)""" % \
          (request.layout_id, display_name, request.cells_count, request.cells_count, auto_num, request.layout_type,
           template_num)
    result = dbtools.make_sql_request(sql)
    if result == 0:
        logging.error("create_layout_backup() prepare layoutsprmbackup; error add layout: " + dbtools.get_last_error())

    for participant in request.participant_positions:
        if participant.priority == None or participant.wndnum == None:
            continue
        abid = tools.id_to_db_num(participant.abid)
        if participant.role_id != layoutcfgtools.DEFAULT_FILLED_ROLE_ID: # set abid for FIXED PEER role only
            abid = 0
        render_mode_ind = data.get_render_participant_index(participant.render_mode)
        result = dbtools.make_sql_request("""INSERT INTO legacy.layoutbackup (confid, layout_id, num_ab, type, priority,
         wnd_num, abid, role, render_mode) VALUES (%d, %d, %d, %d, %d, %d, %d, %d, %d)""" %
         (0, request.layout_id, request.cells_count, request.schema_type, participant.priority,
             participant.wndnum, abid, participant.role_id, render_mode_ind))
        if result == 0:
            logging.error(
                "create_layout_backup() prepare layoutbackup; error of adding layout backup: " + dbtools.get_last_error())

    if request.owner_id != None:
        result = dbtools.make_sql_request(
            "INSERT INTO legacy.layoutconfbackup (id, confid, abid) VALUES (%d, %d, %d)" % (
                request.layout_id, request.confid, request.owner_id))
        if result == 0:
            logging.error(
                "create_layout_backup() prepare layoutconfbackup; error of adding layout backup: " + dbtools.get_last_error())


def update_layout_backup(request: CreateLayoutBackupRequest):
    # delete old backup
    delete_layout_backup(request.confid, request.layout_id, True)

    # create new backup
    create_layout_backup(request)


def create_layout_backup_from_db(confid: int, layout_id: int):
    # delete old backup
    delete_layout_backup(confid, layout_id, True)

    # build request "backup layout"
    auto = False
    cells_count = 0
    layout_type = data.LAYOUT_TYPE_GLOBAL
    display_name = ""
    template = False
    auto_rows = dbtools.select("SELECT auto, dictors_cnt, layout_type, name, template FROM legacy.layouts WHERE id=%d" % (layout_id))
    if len(auto_rows) != 0:
        auto = tools.int_to_bool(auto_rows[0][0])
        cells_count = auto_rows[0][1]
        layout_type = auto_rows[0][2]
        display_name = auto_rows[0][3]
        template = tools.int_to_bool(auto_rows[0][4])

    schema_type = 0
    participant_list = []
    partic_rows = dbtools.select("""SELECT num_ab, type, priority, wnd_num, abid, role, render_mode 
    FROM legacy.layoutconfigs WHERE layout_id=%d""" % (layout_id))
    for participant in partic_rows:
        # cells_count = participant[0]
        schema_type = participant[1]
        priority = participant[2]
        wnd_num = participant[3]
        abid = tools.db_num_to_id(participant[4])
        role = participant[5]
        render_mode = data.get_render_participant_name(participant[6])
        participant_list.append(LayoutAbonentPositionEntity(wnd_num, abid, priority, role, render_mode))

    owner_id = 0
    owner_rows = dbtools.select("SELECT abid FROM legacy.layoutconferences WHERE id=%d" % (layout_id))
    if len(owner_rows) != 0:
        owner_id = owner_rows[0][0]

    # make request
    request = CreateLayoutBackupRequest(confid, layout_id, cells_count, schema_type, participant_list, owner_id, auto,
                                        layout_type, display_name, template)
    create_layout_backup(request)


def create_all_layout_backups(confid: int):
    """This function create backup of all layouts of conference 'confid'."""
    lyt_id_list = layoutcfgtools.get_layouts_id_of_conf(confid)
    for layout_id in lyt_id_list:
        create_layout_backup_from_db(confid, layout_id)


def backup_of_created_online_layout(request: CreateOnlineLayoutRequest, layout_id: int):
    # delete old backup
    delete_layout_backup(request.confid, layout_id, True)

    # build request "backup layout"
    participant_list = []
    for participant in request.participant_positions:
        participant_list.append(
            LayoutAbonentPositionEntity(participant.position, participant.participant_id, data.PRIORITY_NORMAL,
                                        participant.role_id, participant.render_mode))

    owner_id = request.participant_id

    layout_type = data.get_layout_type_ind(request.layout_type)

    # make backup request
    backup_request = CreateLayoutBackupRequest(request.confid, layout_id, request.cells_count, request.schema_type,
                                               participant_list, owner_id, request.auto, layout_type,
                                               request.display_name, request.template)
    create_layout_backup(backup_request)


def backup_of_edited_online_layout(request: ChangeOnlineLayoutRequest):
    layout_id = tools.get_layout_id_by_name(request.layout_name)

    layout_type = get_layout_type(layout_id)

    # get old display name / template flags
    auto_rows = dbtools.select("SELECT name, template FROM legacy.layoutsprmbackup WHERE id=%d" % (layout_id))
    if len(auto_rows) != 0:
        display_name = auto_rows[0][0]
        template = tools.int_to_bool(auto_rows[0][1])
    else:
        display_name = ""
        template = False

    # delete old backup
    delete_layout_backup(request.confid, layout_id, False)

    # build request "backup layout"
    participant_list = []
    for participant in request.participant_positions:
        participant_list.append(
            LayoutAbonentPositionEntity(participant.position, participant.participant_id, data.PRIORITY_NORMAL,
                                        participant.role_id, participant.render_mode))

    # make backup request
    backup_request = CreateLayoutBackupRequest(request.confid, layout_id, request.cells_count, request.schema_type,
                                               participant_list, None, request.auto, layout_type, display_name,
                                               template)
    create_layout_backup(backup_request)


def get_layout_priority(layout_type_ind: int, template: int) -> int:
    """This function returns priority of searhing/using layouts."""
    priority_list = [
        {"type": data.LAYOUT_TYPE_INDIVIDUAL, "template": True, "priority": 4},
        {"type": data.LAYOUT_TYPE_INDIVIDUAL, "template": False, "priority": 3},
        {"type": data.LAYOUT_TYPE_GLOBAL, "template": True, "priority": 2},
        {"type": data.LAYOUT_TYPE_GLOBAL, "template": False, "priority": 1}
    ]

    for i in range(len(priority_list)):
        if priority_list[i]["type"] == layout_type_ind and priority_list[i]["template"] == template:
            return priority_list[i]["priority"]

    return -1 # error


def get_layout_priority_list(confid: int, owner: int):
    """This function returns list of layouts with its priorities for 'owner'."""
    layout_priority_list = []

    # get individual layout id (template)
    layout_id = get_individual_layout_id(confid, owner, True)
    priority = get_layout_priority(data.LAYOUT_TYPE_INDIVIDUAL, True)
    if layout_id != 0:
        layout_priority_list.append({"layout_id": layout_id, "priority": priority})

    # get individual layout id (common)
    layout_id = get_individual_layout_id(confid, owner, False)
    priority = get_layout_priority(data.LAYOUT_TYPE_INDIVIDUAL, False)
    if layout_id != 0:
        layout_priority_list.append({"layout_id": layout_id, "priority": priority})

    # get global layout id (template)
    layout_id = get_global_layout_id(confid, True)
    priority = get_layout_priority(data.LAYOUT_TYPE_GLOBAL, True)
    if layout_id != 0:
        layout_priority_list.append({"layout_id": layout_id, "priority": priority})

    # get global layout id (common)
    layout_id = get_global_layout_id(confid, False)
    priority = get_layout_priority(data.LAYOUT_TYPE_GLOBAL, False)
    if layout_id != 0:
        layout_priority_list.append({"layout_id": layout_id, "priority": priority})

    return layout_priority_list


def get_layout_id_by_owner_OLD(confid: int, owner: int):
    """This function find layout id by its owner id."""
    # get individual layout id (template)
    layout_id = get_individual_layout_id(confid, owner, True)
    if layout_id != 0:
        return layout_id

    # get individual layout id (common)
    layout_id = get_individual_layout_id(confid, owner, False)
    if layout_id != 0:
        return layout_id

    # get global layout id (template)
    layout_id = get_global_layout_id(confid, True)
    if layout_id != 0:
        return layout_id

    # get global layout id (common)
    layout_id = get_global_layout_id(confid, False)
    if layout_id != 0:
        return layout_id

    return 0 # error


def get_layout_id_by_owner2(confid: int, owner: int):
    """This function return layout and priority for owner 'owner'."""

    # get all layout-pretendents with its priorities
    layout_priority_list = get_layout_priority_list(confid, owner)
    if len(layout_priority_list) == 0:
        return 0, 0 # error

    # get layout-pretendent with max priority
    max_priority = -1000
    max_layout_id = 0
    for lyt in layout_priority_list:
        if lyt["priority"] > max_priority:
            max_priority = lyt["priority"]
            max_layout_id = lyt["layout_id"]

    return max_layout_id, max_priority


def get_layout_id_by_owner(confid: int, owner: int):
    """This function return layout for owner 'owner'."""
    layout_id, priority = get_layout_id_by_owner2(confid, owner)
    return layout_id


def get_layout_id_by_owner_ex_OLD(confid: int, owner: int, exclude_layout_id: int):
    """This function find layout id (exclude from search: exclude_layout_id) by its owner id."""
    # get individual layout id (template)
    layout_id = get_individual_layout_id(confid, owner, True)
    if layout_id != 0 and layout_id != exclude_layout_id:
        return layout_id

    # get individual layout id (common)
    layout_id = get_individual_layout_id(confid, owner, False)
    if layout_id != 0 and layout_id != exclude_layout_id:
        return layout_id

    # get global layout id (template)
    layout_id = get_global_layout_id(confid, True)
    if layout_id != 0 and layout_id != exclude_layout_id:
        return layout_id

    # get global layout id (common)
    layout_id = get_global_layout_id(confid, False)
    if layout_id != 0 and layout_id != exclude_layout_id:
        return layout_id

    return 0 # error


def get_layout_id_by_owner_ex(confid: int, owner: int, exclude_layout_id: int):
    """This function find layout id (exclude from search: exclude_layout_id) by its owner id."""

    # get all layout-pretendents with its priorities
    layout_priority_list = get_layout_priority_list(confid, owner)
    if len(layout_priority_list) == 0:
        return 0 # error

    # get layout-pretendent with max priority (exclude exclude_layout_id)
    max_priority = -1000
    max_layout_id = 0
    for lyt in layout_priority_list:
        if lyt["priority"] > max_priority and lyt["layout_id"] != exclude_layout_id:
            max_priority = lyt["priority"]
            max_layout_id = lyt["layout_id"]

    return max_layout_id


# TMP:
# def get_backup_schema_type(_layout_id):
#    _layout_id = int(_layout_id)
#
#    lyt_cfg_rows = dbtools.select("SELECT DISTINCT type FROM legacy.layoutbackup WHERE layout_id=%d" % (_layout_id))
#    if len(lyt_cfg_rows) != 0:
#        return lyt_cfg_rows[0][0]
#
#    return layoutcfgtools.DEFAULT_EQUAL_TYPE # error (no layout _layout_id in database)


def get_backup_of_layout(_confid: int, _layout_id: int):
    """This function returns backup of layout _layout_id in conference _confid."""

    auto = False
    cells_count = 0
    layout_type = data.LAYOUT_TYPE_GLOBAL
    display_name = ""
    template = False
    auto_rows = dbtools.select("SELECT auto, dictors_cnt, layout_type, name, template FROM legacy.layoutsprmbackup WHERE id=%d" %
                               (_layout_id))
    if len(auto_rows) != 0:
        auto = tools.int_to_bool(auto_rows[0][0])
        cells_count = auto_rows[0][1]
        layout_type = auto_rows[0][2]
        display_name = auto_rows[0][3]
        template = tools.int_to_bool(auto_rows[0][4])

    # owner of layout (0 - global layout)
    owner_id = 0
    owner_rows = dbtools.select("SELECT abid FROM legacy.layoutconfbackup WHERE id=%d" % (_layout_id))
    if len(owner_rows) != 0:
        owner_id = owner_rows[0][0]

    # build "backup of layout"
    # cells_count = 0
    schema_type = 0
    participant_list = []
    partic_rows = dbtools.select("""SELECT num_ab, type, priority, wnd_num, abid, role, render_mode 
    FROM legacy.layoutbackup WHERE layout_id=%d""" % (_layout_id))
    for participant in partic_rows:
        cells_count = participant[0]
        schema_type = participant[1]
        priority = participant[2]
        wnd_num = participant[3]
        abid = tools.db_num_to_id(participant[4])
        role = participant[5]
        render_mode = data.get_render_participant_name(participant[6])
        participant_list.append(LayoutAbonentPositionEntity(wnd_num, abid, priority, role, render_mode))

    if layout_type == data.LAYOUT_TYPE_GLOBAL:
        layout_backup = SheduledLayoutGlobalResponse(_layout_id, auto, cells_count, schema_type, participant_list,
                                                     display_name, template)
    elif layout_type == data.LAYOUT_TYPE_ASPEAKER:
        layout_backup = SheduledLayoutAspeakerResponse(_layout_id, auto, cells_count, schema_type, participant_list,
                                                       display_name, template)
    else:
        layout_backup = SheduledLayoutIndividualResponse(_layout_id, auto, cells_count, schema_type, participant_list,
                                                         owner_id, display_name, template)


    return layout_backup


def get_layouts_id_of_conference(confid: int):
    """This function returns id of all backuped layout of conference 'confid'."""
    layout_id_list = []
    layout_id_rows = dbtools.select("SELECT id FROM legacy.layoutconfbackup WHERE confid=%d" % (confid))
    for layout_id in layout_id_rows:
        layout_id_list.append(layout_id[0])

    return layout_id_list


def is_active_slot_in_conf(_confid: int) -> bool:
    """This function find slot 'ACTIVE' in all real-time layouts of conference '_confid'. If found then return True."""
    rows = dbtools.select(
        """SELECT role FROM legacy.layoutbackup 
        WHERE layout_id IN (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d) AND role=%d AND 
        layout_id IN (SELECT id FROM legacy.layoutsprmbackup WHERE auto=0)""" %(_confid, layoutcfgtools.ACTIVE_ROLE_ID))
    if len(rows) != 0:
        return True
    else:
        return False


def is_active_slot_exclude_layout(_confid: int, _layoutid: int) -> bool:
    """This function find slot 'ACTIVE' in all real-time layouts of conference '_confid' exclude '_layoutid'.
    If found then return True."""
    rows = dbtools.select(
        """SELECT role FROM legacy.layoutbackup 
        WHERE layout_id IN (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d) AND layout_id<>%d AND role=%d AND 
        layout_id IN (SELECT id FROM legacy.layoutsprmbackup WHERE auto=0)""" %
        (_confid, _layoutid, layoutcfgtools.ACTIVE_ROLE_ID))
    if len(rows) != 0:
        return True
    else:
        return False


def is_active_slot_in_layout(_confid: int, _layoutid: int) -> bool:
    """This function find slot 'ACTIVE' in real-time layout '_layoutid' of conference '_confid'. If found then return True."""
    rows = dbtools.select("SELECT role FROM legacy.layoutbackup WHERE layout_id=%d AND role=%d" %
                          (_layoutid, layoutcfgtools.ACTIVE_ROLE_ID))
    if len(rows) != 0:
        return True
    else:
        return False


def is_active_slot_in_slots_list(_participant_positions: list, _auto: bool) -> bool:
    """This function find slot 'ACTIVE' in slots list. If found, then return True.
    _participant_positions - list of OnlineLayoutAbonentEntity"""
    if _auto:
        return False
    for slot in _participant_positions:
        if slot.role_id == layoutcfgtools.ACTIVE_ROLE_ID:
            return True
    return False

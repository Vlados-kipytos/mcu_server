#!/usr/bin/python3


import logging
import os
import sys
import time

import app.core.xmltools as xmltools

g_last_error = ""


def get_last_error():
    global g_last_error
    return (g_last_error)


if sys.platform == "win32":  # --------------------------- WINDOWS VERSION ---------------------------------

    import win32pipe
    import win32file
    import win32api
    import winerror


    def get_pipe_name():
        return r'\\.\pipe\multiplexer'


    def open_server_pipe():
        """This function opens cnt server-pipes and returns list of its handles"""
        global g_last_error
        try:
            pipe_handle = win32pipe.CreateNamedPipe(get_pipe_name(),
                                                    win32pipe.PIPE_ACCESS_DUPLEX,
                                                    win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_WAIT,
                                                    win32pipe.PIPE_UNLIMITED_INSTANCES,
                                                    65536,
                                                    65536,
                                                    300,
                                                    None)
            # print "handle ok: ", pipe_handle.__nonzero__()
            res = win32pipe.ConnectNamedPipe(pipe_handle, None)
            logging.info("connect result: " + str(res))
            return (pipe_handle)
        except:
            g_last_error = "function open_server_pipe(): unknown exception:" + str(sys.exc_info())
            logging.error(g_last_error)
            return (None)


    def open_client_pipe():
        global g_last_error
        try:
            file_handle = win32file.CreateFile(get_pipe_name(),
                                               win32file.GENERIC_READ | win32file.GENERIC_WRITE,
                                               0,
                                               None,
                                               win32file.OPEN_EXISTING,
                                               0,
                                               None)
            # setting readmode to MESSAGE
            win32pipe.SetNamedPipeHandleState(file_handle,
                                              win32pipe.PIPE_READMODE_MESSAGE,
                                              None,
                                              None)
            return (file_handle)
        except:
            if (win32api.GetLastError() != winerror.ERROR_PIPE_BUSY):
                g_last_error = "function open_client_pipe(): unknown exception:" + str(sys.exc_info())
                logging.error(g_last_error)
                return (None)

            # All pipe instances are busy, so wait for 20 seconds.

            if win32pipe.WaitNamedPipe(get_pipe_name(), 20000) == 0:
                g_last_error = "function open_client_pipe(): WaitNamedPipe() timeout"
                logging.error(g_last_error)
                return (None)
            else:
                # print "wait branch..."
                return open_client_pipe()


    def write(pipe_handle, msg):
        global g_last_error
        try:
            if pipe_handle != None:
                win32file.WriteFile(pipe_handle, bytearray(msg + "\n", 'utf8'))
        except:
            g_last_error = "function write(): unknown exception:" + str(sys.exc_info())
            logging.error(g_last_error)


    def read(pipe_handle):
        global g_last_error
        try:
            if pipe_handle != None:
                (res, msg) = win32file.ReadFile(pipe_handle, 65536)
                if res != 0:
                    logging.error("function read(): error=" + str(res))
                return (msg.decode("UTF-8"))
            else:
                return ("")
        except:
            g_last_error = "function read(): unknown exception:" + str(sys.exc_info())
            logging.error(g_last_error)
            return ("")


    def close_pipe(pipe_handle, b_del_pipe):
        global g_last_error
        try:
            if pipe_handle != None:
                pipe_handle.close()
        except:
            g_last_error = "function close_pipe(): unknown exception:" + str(sys.exc_info())
            logging.error(g_last_error)


    def send_msg(_msg):
        """Send test message _msg to event processor."""
        # NOTE: in windows versio we cannot open/close client pipe many times
        pass
        pipe_h = open_client_pipe()
        write(pipe_h, _msg)
        close_pipe(pipe_h, 0)


else:  # ---------------------------- LINUX VERSION  -----------------------------------

    def get_pipe_name():
        cfg = xmltools.get_config()
        dir = cfg["dirs"]["shareddir"]
        return dir + "/multiplexer"


    def open_server_pipe():
        global g_last_error
        """This function opens cnt server-pipes and returns list of its handles"""
        try:
            pipe_name = get_pipe_name()
            if os.path.exists(pipe_name):
                os.unlink(pipe_name)
            os.umask(0o111)
            os.mkfifo(pipe_name, 0o666)
            pipe_file = open(pipe_name, "rb")
            return (pipe_file)
        except:
            g_last_error = "function open_server_pipe(): unknown exception:" + str(sys.exc_info())
            logging.error(g_last_error)
            return (None)


    def open_client_pipe():
        global g_last_error
        try:
            # use O_NONBLOCK to return error if other part of pipe is not opened (don't hang in this line)
            fd = os.open(get_pipe_name(), os.O_WRONLY|os.O_NONBLOCK)
            pipe_file = os.fdopen(fd, "wb", 0)
        except OSError as oserr:
            g_last_error = "function open_client_pipe() OSError: errno=%d , strerror=%s" % (oserr.errno, oserr.strerror)
            logging.error(g_last_error)
            pipe_file = None
        except:
            g_last_error = "function open_client_pipe(): unknown exception:" + str(sys.exc_info())
            logging.error(g_last_error)
            pipe_file = None
        if pipe_file == None:
            logging.error("open_client_pipe(): wait and try open client pipe again...")
            time.sleep(5)
            pipe_file = open_client_pipe()
        return pipe_file


    def write(pipe_handle, msg):
        global g_last_error
        try:
            if pipe_handle != None:
                pipe_handle.write(bytearray(msg + "\n", 'utf8'))
                pipe_handle.flush()
                # logging.debug( "function write(): pipe_file=" + str(pipe_handle) )
                # OLD: TMP: g_last_error = "name=" + str(pipe_handle.name) + " , closed=" + str(pipe_handle.closed) + " , mode=" + str(pipe_handle.mode)
        except:
            g_last_error = "function write(): unknown exception:" + str(sys.exc_info())
            logging.error(g_last_error)


    def read(pipe_handle):
        global g_last_error
        try:
            if pipe_handle != None:
                msg = pipe_handle.readline()
                # logging.debug( "function read(): pipe_file=" + str(pipe_handle) )
                return (msg.decode("UTF-8"))
            else:
                return ("")
        except:
            g_last_error = "function read(): unknown exception:" + str(sys.exc_info())
            logging.error(g_last_error)
            return ("")


    def close_pipe(pipe_handle, b_del_pipe):
        global g_last_error
        try:
            pipe_name = get_pipe_name()
            if pipe_handle != None and os.path.exists(pipe_name):
                pipe_handle.close()
                # logging.debug( "function close_pipe(): pipe_file=" + str(pipe_handle) )
                if b_del_pipe:
                    os.unlink(pipe_name)
        except:
            g_last_error = "function close_pipe(): unknown exception:" + str(sys.exc_info())
            logging.error(g_last_error)


    def send_msg(_msg):
        """Send test message _msg to event processor."""
        pipe_h = open_client_pipe()
        write(pipe_h, _msg)
        close_pipe(pipe_h, 0)

#!/usr/bin/python3


# state of abonent
AB_ST_INACTIVE = 0
AB_ST_IN_PROGRESS = 1
AB_ST_ESTABLISHED = 2

ABONENT_STATES_LIST = [
    {"en": "Inactive", "ru": "Отключен"},
    {"en": "In progress", "ru": "Идет подключение"},
    {"en": "Established", "ru": "Подключен"}]


RESOLUTIONS_LIST = [
    {"width": 3840, "height": 2160, "fps": 30, "hint": "3840x2160 30 FPS (16:9)", "disable": [], "feature": "4K"},
    {"width": 1920, "height": 1080, "fps": 60, "hint": "1920x1080 60 FPS (16:9)", "disable": [], "feature": "FHD"},
    {"width": 1920, "height": 1080, "fps": 30, "hint": "1920x1080 30 FPS (16:9)", "disable": [], "feature": "FHD"},
    {"width": 1280, "height": 720, "fps": 60, "hint": "1280x720 60 FPS (16:9)", "disable": ["1080P"], "feature": "HD"},
    {"width": 1280, "height": 720, "fps": 30, "hint": "1280x720 30 FPS (16:9)", "disable": ["1080P"], "feature": "HD"},
    {"width": 1280, "height": 960, "fps": 30, "hint": "1280x960 30 FPS (4:3)", "disable": ["1080P"], "feature": "HD"},
    {"width": 800, "height": 600, "fps": 30, "hint": "800x600 30 FPS (4:3)",
     "disable": ["720P", "1080P"], "feature": "SD"},
    {"width": 640, "height": 480, "fps": 30, "hint": "640x480 30 FPS (4:3)",
     "disable": ["720P", "1080P"], "feature": "SD"},
    {"width": 704, "height": 576, "fps": 30, "hint": "704x576 30 FPS (11:9, 4CIF)",
     "disable": ["720P", "1080P"], "feature": "SD"},
    {"width": 704, "height": 480, "fps": 30, "hint": "704x480 30 FPS (22:15, 4SIF)",
     "disable": ["720P", "1080P"], "feature": "SD"},
    {"width": 352, "height": 288, "fps": 30, "hint": "352x288 30 FPS (11:9, CIF)",
     "disable": ["SD", "720P", "1080P"], "feature": "CIF"},
    {"width": 352, "height": 240, "fps": 30, "hint": "352x240 30 FPS (22:15, SIF)",
     "disable": ["CIF", "SD", "720P", "1080P"], "feature": "CIF"},
    {"width": 176, "height": 144, "fps": 30, "hint": "176x144 30 FPS (11:9, QCIF)",
     "disable": ["CIF", "SD", "720P", "1080P"], "feature": "QCIF"},
    {"width": 176, "height": 120, "fps": 30, "hint": "176x120 30 FPS (22:15, QSIF)",
     "disable": ["CIF", "SD", "720P", "1080P"], "feature": "QCIF"}]


def get_frame_resolution(_res_index):
    _res_index = int(_res_index)
    if _res_index >= 0 and _res_index < len(RESOLUTIONS_LIST):
        return (RESOLUTIONS_LIST[_res_index])
    else:
        return (RESOLUTIONS_LIST[2])


def find_resolution_index(_width, _height, _fps):
    """This function returns index of resolution record in array RESOLUTIONS_LIST.
    At error returns 0 (first record)."""
    _width = int(_width)
    _height = int(_height)
    _fps = int(_fps)
    for i in range(len(RESOLUTIONS_LIST)):
        if RESOLUTIONS_LIST[i]["width"] == _width and RESOLUTIONS_LIST[i]["height"] == _height and RESOLUTIONS_LIST[i][
            "fps"] == _fps:
            return (i)
    return (0)


def get_disable_resol_str(_res_index):
    """This function build string with params for disabling resolutions. Example: '--disable SD --disable 720P'."""
    _res_index = int(_res_index)
    if _res_index >= 0 and _res_index < len(RESOLUTIONS_LIST):
        resolution = RESOLUTIONS_LIST[_res_index]
        str = ""
        cnt = len(resolution["disable"])
        for i in range(cnt):
            size_str = resolution["disable"][i]
            str += "--disable " + size_str
            if i != cnt - 1:
                str += " "
        return (str)
    else:
        return ("")


CONF_STATE_PRE_INACTIVE = 0
CONF_STATE_POST_INACTIVE = 1
CONF_STATE_START = 2
CONF_STATE_START2 = 3  # not used
CONF_STATE_CONFIGURING = 4  # not used
CONF_STATE_ACTIVE = 5
CONF_STATE_CLOSING_VILE = 6
CONF_STATE_CLOSING_VILE2 = 7  # not used
CONF_STATE_CLOSING_VM = 8  # not used
CONF_STATE_SEND_MAILS = 9  # not used

CONF_STATE_NAMES = [
    {"en": "inactive", "ru": "остановлена", "name": "inactive"},
    {"en": "inactive", "ru": "остановлена", "name": "inactive"},
    {"en": "starting conference", "ru": "запуск", "name": "start"},
    {"en": "starting conference", "ru": "запуск", "name": ""},
    {"en": "configuring", "ru": "конфигурирование", "name": ""},
    {"en": "active", "ru": "работает", "name": "active"},
    {"en": "closing conference", "ru": "завершение", "name": "closing"},
    {"en": "closing conference", "ru": "завершение", "name": ""},
    {"en": "closing conference", "ru": "завершение", "name": ""},
    {"en": "sending mails", "ru": "отправка приглашений", "name": ""}]


# show priority:
PRIORITY_LOW = 0
PRIORITY_NORMAL = 1
PRIORITY_HIGH = 2

PRIORITY_LIST = [
    {"en": "Low", "ru": "Низкий", "value": "TEMPORARY", "name": "low"},
    {"en": "Normal", "ru": "Нормальный", "value": "NORMAL", "name": "normal"},
    {"en": "High", "ru": "Высокий", "value": "HIGH", "name": "high"}]


def get_priority_value(priority):
    if priority >= 0 and priority < len(PRIORITY_LIST):
        value = PRIORITY_LIST[priority]["value"]
        return (value)
    else:
        return (u"ERROR: BAD PRIORITY VALUE")


def get_priority_by_name(priority_name):
    for prior_ind in range(len(PRIORITY_LIST)):
        if PRIORITY_LIST[prior_ind]["name"] == priority_name:
            return prior_ind
    return PRIORITY_NORMAL


def get_priority_name(priority_ind):
    if priority_ind >= 0 and priority_ind < len(PRIORITY_LIST):
        value = PRIORITY_LIST[priority_ind]["name"]
        return (value)
    else:
        return ("normal")


# state of license
LIC_STAT_UNDEFINED = 0
LIC_STAT_OFF = 1
LIC_STAT_INVALID = 2
LIC_STAT_VALID = 3

LICENSE_STATE_LIST = ["undefined", "off", "invalid", "valid"]


def get_license_state(_index):
    if (_index >= 0) and (_index < len(LICENSE_STATE_LIST)):
        res = LICENSE_STATE_LIST[_index]
        return res
    else:
        return "error"  # error


# types of licenses
LIC_TYPE_INSTANTANEOUS = 0
LIC_TYPE_DAILY = 1
LIC_TYPE_PERMANENT = 2

LICENSE_TYPE_LIST = ["INSTANTANEOUS", "DAILY", "PERMANENT"]


def get_license_type(_index):
    if (_index >= 0) and (_index < len(LICENSE_TYPE_LIST)):
        res = LICENSE_TYPE_LIST[_index]
        return res
    else:
        return "error"  # error


def license_type_str_to_index(_type_str: str) -> int:
    TYPE_STR = _type_str.upper()
    for i in range(len(LICENSE_TYPE_LIST)):
        CUR_TYPE_STR = LICENSE_TYPE_LIST[i].upper()
        if TYPE_STR == CUR_TYPE_STR:
            return i
    return -1


STREAM_QUALITY_DEFAULT = 0
STREAM_QUALITY_BALANCED = 1
STREAM_QUALITY_RESOLUTION = 2
STREAM_QUALITY_RATE = 3
STREAM_QUALITY_COMPLIANT = 4

STREAM_QUALITY_LIST = [
    {"name": "DEFAULT", "cmd": "SET QUALITY VIDEO DEFAULT", "cmd2": "SET QUALITY PRESENTATION DEFAULT"},
    {"name": "BALANCED", "cmd": "SET QUALITY VIDEO BALANCED", "cmd2": "SET QUALITY PRESENTATION BALANCED"},
    {"name": "RESOLUTION", "cmd": "SET QUALITY VIDEO RESOLUTION", "cmd2": "SET QUALITY PRESENTATION RESOLUTION"},
    {"name": "RATE", "cmd": "SET QUALITY VIDEO RATE", "cmd2": "SET QUALITY PRESENTATION RATE"},
    {"name": "COMPLIANT", "cmd": "SET QUALITY VIDEO COMPLIANT", "cmd2": "SET QUALITY PRESENTATION COMPLIANT"}]


def get_stream_quality(_quality_index):
    if (_quality_index >= 0) and (_quality_index < len(STREAM_QUALITY_LIST)):
        return STREAM_QUALITY_LIST[_quality_index]
    else:
        return STREAM_QUALITY_LIST[0]  # error


def get_stream_quality_index(_quality_name):
    for index in range(len(STREAM_QUALITY_LIST)):
        if STREAM_QUALITY_LIST[index]["name"] == _quality_name:
            return index
    return 0


SIP_H323 = 0
H323_SIP = 1

PROTOCOLS_ORDER_LIST = ["sip", "h323"]


def get_protocols_order(order):
    if (order >= 0) and (order < len(PROTOCOLS_ORDER_LIST)):
        str_ = PROTOCOLS_ORDER_LIST[order]
        return str_
    else:
        return "ERROR: BAD PROTOCOLS ORDER"


def get_protocols_order_index(order):
    if order in PROTOCOLS_ORDER_LIST:
        return PROTOCOLS_ORDER_LIST.index(order)
    else:
        return 0


CALL_SEQ_LOCAL_ABOOK_FIRST = 0
CALL_SEQ_SIP_SERVER_FIRST = 1

CALL_SEQ_LIST = [
    {"name": "addressbook", "cmd": "SET ALOOKUP ABOOK"},
    {"name": "gatekeeper", "cmd": "SET ALOOKUP SERVER"}]


def get_call_sequence(_index):
    if (_index >= 0) and (_index < len(CALL_SEQ_LIST)):
        return CALL_SEQ_LIST[_index]
    else:
        return CALL_SEQ_LIST[0]  # error


def get_call_sequence_name(_index):
    if (_index >= 0) and (_index < len(CALL_SEQ_LIST)):
        str_ = CALL_SEQ_LIST[_index]["name"]
        return str_
    else:
        return "error: bad call sequence index"  # error


def get_call_sequence_index(name):
    for i in range(len(CALL_SEQ_LIST)):
        if name == CALL_SEQ_LIST[i]["name"]:
            return i
    return 0  # error


SIP_TRANSP_AUTO = 0
SIP_TRANSP_UDP = 1
SIP_TRANSP_TCP = 2
SIP_TRANSP_TLS = 3

SIP_TRANSP_LIST = [
    {"name": "auto", "cmd": ""},
    {"name": "udp", "cmd": ";transport=udp;hide"},
    {"name": "tcp", "cmd": ";transport=tcp;hide"},
    {"name": "tls", "cmd": ";transport=tls;hide"}]


def get_sip_transport(_index):
    if (_index >= 0) and (_index < len(SIP_TRANSP_LIST)):
        return SIP_TRANSP_LIST[_index]
    else:
        return SIP_TRANSP_LIST[0]  # error


def get_sip_transport_name(_index):
    if (_index >= 0) and (_index < len(SIP_TRANSP_LIST)):
        str_ = SIP_TRANSP_LIST[_index]["name"]
        return str_
    else:
        return "error: bad sip transport index"  # error


def get_sip_transport_index(name):
    for i in range(len(SIP_TRANSP_LIST)):
        if name == SIP_TRANSP_LIST[i]["name"]:
            return i
    return 0  # error


BFCP_TRANSPORT_UDP = 0
BFCP_TRANSPORT_TCP = 1

BFCP_TRANSPORT_LIST = ["UDP", "TCP"]


def get_bfcp_transport_name(_index):
    if _index >= 0 and _index < len(BFCP_TRANSPORT_LIST):
        return BFCP_TRANSPORT_LIST[_index]
    else:
        return BFCP_TRANSPORT_LIST[0]  # error


def get_bfcp_transport_index(name):
    for i in range(len(BFCP_TRANSPORT_LIST)):
        if name == BFCP_TRANSPORT_LIST[i]:
            return i
    return 0  # error


BANDWIDTH_AUTO = 0
BANDWIDTH_20 = 1
BANDWIDTH_30 = 2
BANDWIDTH_40 = 3
BANDWIDTH_50 = 4
BANDWIDTH_60 = 5
BANDWIDTH_70 = 6

BANDWIDTH_LIST = [
    {"name": "auto", "bandwidth": 0},
    {"name": "20", "bandwidth": 20},
    {"name": "30", "bandwidth": 30},
    {"name": "40", "bandwidth": 40},
    {"name": "50", "bandwidth": 50},
    {"name": "60", "bandwidth": 60},
    {"name": "70", "bandwidth": 70}]


def get_bandwidth(_index):
    if (_index >= 0) and (_index < len(BANDWIDTH_LIST)):
        return BANDWIDTH_LIST[_index]
    else:
        return BANDWIDTH_LIST[0]  # error


def get_bandwidth_name(_index):
    if (_index >= 0) and (_index < len(BANDWIDTH_LIST)):
        str_ = BANDWIDTH_LIST[_index]["name"]
        return str_
    else:
        return "error: bad bandwidth index"  # error


def get_bandwidth_name_by_value(value):
    for i in range(len(BANDWIDTH_LIST)):
        if value == BANDWIDTH_LIST[i]["bandwidth"]:
            return BANDWIDTH_LIST[i]["name"]
    else:
        return "error: bad bandwidth value"  # error


def get_bandwidth_index(name):
    for i in range(len(BANDWIDTH_LIST)):
        if name == BANDWIDTH_LIST[i]["name"]:
            return i
    return 0  # error


BACKUP_PLACE_LOCAL = 0
BACKUP_PLACE_REMOTE = 1

BACKUP_PLACE_LIST = ["local", "remote"]


def get_backup_place(index):
    if (index >= 0) and (index < len(BACKUP_PLACE_LIST)):
        str_ = BACKUP_PLACE_LIST[index]
        return str_
    else:
        return "ERROR: BAD BACKUP PLACE"


def get_backup_place_index(place):
    if place in BACKUP_PLACE_LIST:
        return BACKUP_PLACE_LIST.index(place)
    else:
        return 0


WD_MONDAY = 0
WD_TUESDAY = 1
WD_WEDNESDAY = 2
WD_THURSDAY = 3
WD_FRIDAY = 4
WD_SATURDAY = 5
WD_SUNDAY = 6

WD_LIST = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]


def get_weekday(index):
    if (index >= 0) and (index < len(WD_LIST)):
        str_ = WD_LIST[index]
        return str_
    else:
        return "ERROR: BAD WEEKDAY"


def get_weekday_index(place):
    if place in WD_LIST:
        return WD_LIST.index(place)
    else:
        return 0


PREVIEW_COMMON_STREAM = 0
PREVIEW_PRESENTATION = 1
PREVIEW_CAMERA = 2

PREVIEW_STREAMS_LIST = [
    {"name": "first", "basecmd": "SET PREVIEW TX "},
    {"name": "second", "basecmd": "SET PREVIEW PRESENTATION "},
    {"name": "camera", "basecmd": "SET PREVIEW RX "}]


def get_preview_type(stream_name: str):
    for i in range(len(PREVIEW_STREAMS_LIST)):
        if PREVIEW_STREAMS_LIST[i]["name"] == stream_name:
            return i
    return 0


def get_preview_cmd(stream_name: str):
    for i in range(len(PREVIEW_STREAMS_LIST)):
        if PREVIEW_STREAMS_LIST[i]["name"] == stream_name:
            return PREVIEW_STREAMS_LIST[i]["basecmd"]
    return ""


FECC_CMD_LEFT = 0
FECC_CMD_RIGHT = 1
FECC_CMD_UP = 2
FECC_CMD_DOWN = 3
FECC_CMD_IN = 4
FECC_CMD_OUT = 5

FECC_CMD_LIST = [
    {"name": "left", "cmd": "FECC PAN LEFT PEER "},
    {"name": "right", "cmd": "FECC PAN RIGHT PEER "},
    {"name": "up", "cmd": "FECC TILT UP PEER "},
    {"name": "down", "cmd": "FECC TILT DOWN PEER "},
    {"name": "in", "cmd": "FECC ZOOM IN PEER "},
    {"name": "out", "cmd": "FECC ZOOM OUT PEER "},
    {"name": "stop", "cmd": "FECC STOP PEER "}]

def get_fecc_cmd(move_name: str, aburi: str):
    for i in range(len(FECC_CMD_LIST)):
        if FECC_CMD_LIST[i]["name"] == move_name:
            return FECC_CMD_LIST[i]["cmd"] + " " + aburi
    return ""


def get_fecc_commands_list():
    cmd_list = []
    for cmd in FECC_CMD_LIST:
        cmd_list.append(cmd["name"])
    return cmd_list


RENDER_PARTICIPANT_FILL = 0
RENDER_PARTICIPANT_FULL = 1
RENDER_PARTICIPANT_AUTO = 2

RENDER_PARTICIPANT_NAMES = ["FILL", "FULL", "AUTO"]


def get_render_participant_index(render_participant_name: str) -> int:
    for i in range(len(RENDER_PARTICIPANT_NAMES)):
        if RENDER_PARTICIPANT_NAMES[i] == render_participant_name:
            return i

    return 0


def get_render_participant_name(render_participant_index: int) -> str:
    if render_participant_index >= 0 and render_participant_index < len(RENDER_PARTICIPANT_NAMES):
        return RENDER_PARTICIPANT_NAMES[render_participant_index]

    return RENDER_PARTICIPANT_NAMES[0]


UTIL_PING = 0
UTIL_TRACEROUTE = 1
UTIL_TCPDUMP = 2

LIST_UTILS = ["ping", "traceroute", "tcpdump"]

# logging levels of vtsrv
LOG_LEVEL_ERROR = 0
LOG_LEVEL_WARNING = 1
LOG_LEVEL_INFO = 2
LOG_LEVEL_VERBOSE = 3
LOG_LEVEL_TRACE = 4

LOG_LEVELS_LIST = ["ERROR", "WARNING", "INFO", "VERBOSE", "TRACE"]


def get_log_level_str(log_level_ind: int) -> str:
    if log_level_ind >= 0 and log_level_ind < len(LOG_LEVELS_LIST):
        return LOG_LEVELS_LIST[log_level_ind]
    else:
        return "UNKNOWN"


BACKGROUND_OFF = 0
BACKGROUND_GREEN = 1
BACKGROUND_SILVER = 2
BACKGROUND_VIOLET = 3
BACKGROUND_WAVE = 4

BACKGROUND_LIST  = [
    {"name": "OFF", "cmd": "SET BACKGROUND OFF", "cmd2": "LAYOUT BACKGROUND ON "},
    {"name": "GREEN", "cmd": "SET BACKGROUND GREEN", "cmd2": "LAYOUT BACKGROUND GREEN "},
    {"name": "SILVER", "cmd": "SET BACKGROUND SILVER", "cmd2": "LAYOUT BACKGROUND SILVER "},
    {"name": "VIOLET", "cmd": "SET BACKGROUND VIOLET", "cmd2": "LAYOUT BACKGROUND VIOLET "},
    {"name": "WAVE", "cmd": "SET BACKGROUND WAVE", "cmd2": "LAYOUT BACKGROUND WAVE "}
]

def get_background_names():
    names_list = []
    for b in BACKGROUND_LIST:
        names_list.append(b["name"])
    return names_list

def get_background(background_ind: int) -> dict:
    if background_ind >= 0 and background_ind < len(BACKGROUND_LIST):
        return BACKGROUND_LIST[background_ind]
    else:
        return BACKGROUND_LIST[BACKGROUND_OFF]

def get_background_ind_by_name(background_name: str) -> int:
    for ind in range(len(BACKGROUND_LIST)):
        if BACKGROUND_LIST[ind]["name"] == background_name:
            return ind
    return BACKGROUND_OFF



LAYOUT_TYPE_GLOBAL = 0
LAYOUT_TYPE_INDIVIDUAL = 1
LAYOUT_TYPE_ASPEAKER = 2

LAYOUT_TYPES_LIST = ["global", "individual", "active_speaker"]

def get_layout_type_name(_layout_type_ind: int) -> str:
    if _layout_type_ind >= 0 and _layout_type_ind < len(LAYOUT_TYPES_LIST):
        return LAYOUT_TYPES_LIST[_layout_type_ind]
    else:
        return LAYOUT_TYPES_LIST[LAYOUT_TYPE_GLOBAL]

def get_layout_type_ind(_layout_type_name: str) -> int:
    for index in range(len(LAYOUT_TYPES_LIST)):
        if LAYOUT_TYPES_LIST[index] == _layout_type_name:
            return index
    return LAYOUT_TYPE_GLOBAL


# commands to power management
POWER_COMMANDS_LIST = ["restart_api", "restart_os", "power_off"]


# conference's auto start modes:
AUTO_START_MODE_OFF = 0
AUTO_START_MODE_DAILY = 1
AUTO_START_MODE_WEEKLY = 2
AUTO_START_MODE_MONTHLY = 3
AUTO_START_MODE_ONCE = 4

AUTO_START_MODES_LIST = ["off", "daily", "weekly", "monthly", "once"]

def get_auto_start_mode(_auto_start_mode_index: int) -> str:
    if _auto_start_mode_index >= 0 and _auto_start_mode_index < len(AUTO_START_MODES_LIST):
        return AUTO_START_MODES_LIST[_auto_start_mode_index]
    else:
        return AUTO_START_MODES_LIST[AUTO_START_MODE_OFF]

def get_auto_start_mode_index(_auto_start_mode_name: str) -> int:
    for ind in range(len(AUTO_START_MODES_LIST)):
        if AUTO_START_MODES_LIST[ind] == _auto_start_mode_name:
            return ind
    return AUTO_START_MODE_OFF


# conference's repeate modes:
REPEATE_MODE_OFF = 0
REPEATE_MODE_INFINITY = 1
REPEATE_MODE_COUNTER = 2
REPEATE_MODE_STOP_DATE = 3

REPEATE_MODES_LIST = ["off", "infinity", "counter", "date"]

def get_repeate_mode(_repeate_mode_index: int) -> str:
    if _repeate_mode_index >= 0 and _repeate_mode_index < len(REPEATE_MODES_LIST):
        return REPEATE_MODES_LIST[_repeate_mode_index]
    else:
        return REPEATE_MODES_LIST[REPEATE_MODE_OFF]

def get_repeate_mode_index(_repeate_mode_str: str) -> int:
    for ind in range(len(REPEATE_MODES_LIST)):
        if REPEATE_MODES_LIST[ind] == _repeate_mode_str:
            return ind
    return REPEATE_MODE_OFF


# stop time-table of conference:
STOP_TIME_TABLE_OFF = 0
STOP_TIME_TABLE_DAY = 1
STOP_TIME_TABLE_WEEK = 2
STOP_TIME_TABLE_MONTH = 3

STOP_TIME_TABLE_LIST = ["off", "day", "week", "month"]

def get_stop_time_table(_stop_time_table_index: int) -> str:
    if _stop_time_table_index >= 0 and _stop_time_table_index < len(STOP_TIME_TABLE_LIST):
        return STOP_TIME_TABLE_LIST[_stop_time_table_index]
    else:
        return STOP_TIME_TABLE_LIST[STOP_TIME_TABLE_OFF]

def get_stop_time_table_index(_stop_time_table: str) -> int:
    for ind in range(len(STOP_TIME_TABLE_LIST)):
        if STOP_TIME_TABLE_LIST[ind] == _stop_time_table:
            return ind
    return STOP_TIME_TABLE_OFF


# conference's auto stop modes:
AUTO_STOP_MODE_OFF = 0
AUTO_STOP_MODE_DURATION = 1
AUTO_STOP_MODE_STOPTIME = 2

AUTO_STOP_MODES_LIST = ["off", "duration", "stoptime"]

def get_auto_stop_mode(_auto_stop_mode_index: int) -> str:
    if _auto_stop_mode_index >= 0 and _auto_stop_mode_index < len(AUTO_STOP_MODES_LIST):
        return AUTO_STOP_MODES_LIST[_auto_stop_mode_index]
    else:
        return AUTO_STOP_MODES_LIST[AUTO_STOP_MODE_OFF]

def get_auto_stop_mode_index(_auto_stop_mode: str) -> int:
    for ind in range(len(AUTO_STOP_MODES_LIST)):
        if AUTO_STOP_MODES_LIST[ind] == _auto_stop_mode:
            return ind
    return AUTO_STOP_MODE_OFF


# installation process states:
INSTALL_STATE_STOPPED = 0
INSTALL_STATE_IN_PROGRESS = 1
INSTALL_STATE_UNKNOWN = 2

INSTALL_STATES_LIST = ["stopped", "in_progress", "unknown"]

def get_install_state(_install_state_index: int) -> str:
    if _install_state_index >= 0 and _install_state_index < len(INSTALL_STATES_LIST):
        return INSTALL_STATES_LIST[_install_state_index]
    else:
        return INSTALL_STATES_LIST[INSTALL_STATE_UNKNOWN]


AUDIO_CALL_HIDE = 0
AUDIO_CALL_SHOW = 1
AUDIO_CALL_REJECT = 2

AUDIO_CALLS_LIST = ["HIDE", "SHOW", "REJECT"]

def get_audio_call_name(_audio_call_ind: int) -> str:
    if _audio_call_ind >= 0 and _audio_call_ind < len(AUDIO_CALLS_LIST):
        return AUDIO_CALLS_LIST[_audio_call_ind]
    else:
        return AUDIO_CALLS_LIST[AUDIO_CALL_HIDE]  # default value

def get_audio_call_ind(_audio_call_name: str) -> int:
    for ind in range(len(AUDIO_CALLS_LIST)):
        if AUDIO_CALLS_LIST[ind] == _audio_call_name:
            return ind
    return AUDIO_CALL_HIDE  # default value

def get_audio_call_cmd(_audio_call_name: str):
    return "SET ACALL " + _audio_call_name




# call protocols:
CALL_PROTOCOL_AUTO = 0
CALL_PROTOCOL_SIP = 1
CALL_PROTOCOL_H323 = 2
CALL_PROTOCOL_RTSP = 3
CALL_PROTOCOL_FILE = 4

CALL_PROTOCOLS_LIST = ["auto", "sip", "h323", "rtsp", "media"]

def get_call_protocol_name(_call_protocol_ind: int) -> str:
    if _call_protocol_ind >= 0 and _call_protocol_ind < len(CALL_PROTOCOLS_LIST):
        return CALL_PROTOCOLS_LIST[_call_protocol_ind]
    return CALL_PROTOCOLS_LIST[CALL_PROTOCOL_AUTO]

def get_call_protocol_ind(_call_protocol_name: str) -> int:
    for i in range(len(CALL_PROTOCOLS_LIST)):
        if CALL_PROTOCOLS_LIST[i] == _call_protocol_name:
            return i
    return CALL_PROTOCOL_AUTO

def is_protocol_correspond_addr(_call_protocol_ind: int, _addr: str) -> bool:
    if (_addr is None) or len(_addr) == 0:
        return False

    if _call_protocol_ind == CALL_PROTOCOL_AUTO:
        return True

    addr_prot_sep_ind = _addr.find(":")
    if addr_prot_sep_ind < 0: # protocol separator is not found: return unmasked address by default
        return False

    call_protocol_name = get_call_protocol_name(_call_protocol_ind)
    return call_protocol_name == _addr[:addr_prot_sep_ind]

def get_masked_addr(_call_protocol_ind: int, _addr: str) -> str:
    """This function clear address '_addr' if it is not correspond to protocol '_call_protocol_ind'."""
    if is_protocol_correspond_addr(_call_protocol_ind, _addr):
        return _addr
    else:
        return ""

def is_protocol_correspond_any_addr(_call_protocol_ind: int, _sip: str, _h323: str, _rtsp: str, _file: str) -> bool:
    return is_protocol_correspond_addr(_call_protocol_ind, _sip) or \
           is_protocol_correspond_addr(_call_protocol_ind, _h323) or \
           is_protocol_correspond_addr(_call_protocol_ind, _rtsp) or \
           is_protocol_correspond_addr(_call_protocol_ind, _file)

NOISE_SUPPRESSOR_DISABLED = 0
NOISE_SUPPRESSOR_ENABLED = 1
NOISE_SUPPRESSOR_DEFAULT = 2

NOISE_SUPPRESSOR_NAMES = ["OFF", "ON", "DEFAULT"]

def get_noise_suppressor_name(_noise_suppressor_ind: int) -> str:
    if _noise_suppressor_ind >= 0 and _noise_suppressor_ind < len(NOISE_SUPPRESSOR_NAMES):
        return NOISE_SUPPRESSOR_NAMES[_noise_suppressor_ind]
    return NOISE_SUPPRESSOR_NAMES[NOISE_SUPPRESSOR_DISABLED]

def get_noise_suppressor_ind(_noise_suppressor_name: str) -> int:
    for ind in range(len(NOISE_SUPPRESSOR_NAMES)):
        if NOISE_SUPPRESSOR_NAMES[ind] == _noise_suppressor_name:
            return ind
    return NOISE_SUPPRESSOR_DISABLED

def get_noise_suppressor_global_cmd(enable_noise_suppressor: bool) -> str:
    if enable_noise_suppressor:
        return "SET DENOISE ON"
    else:
        return "SET DENOISE OFF"

def get_noise_suppressor_conf_cmd(noise_suppressor_ind: int, cid: str) -> str:
    return "CONFERENCE DENOISE %s %s" % (get_noise_suppressor_name(noise_suppressor_ind), cid)


def get_noise_suppressor_abonent_cmd(noise_suppressor_ind: int, conn_id: int) -> str:
    return "SET DENOISE %s #%d" % (get_noise_suppressor_name(noise_suppressor_ind), conn_id)


AUTO_GAIN_CTRL_DISABLED = 0
AUTO_GAIN_CTRL_ENABLED = 1
AUTO_GAIN_CTRL_DEFAULT = 2

AUTO_GAIN_CTRL_NAMES = ["OFF", "ON", "DEFAULT"]


def get_auto_gain_ctrl_name(_auto_gain_ctrl_ind: int) -> str:
    if _auto_gain_ctrl_ind >= 0 and _auto_gain_ctrl_ind < len(AUTO_GAIN_CTRL_NAMES):
        return AUTO_GAIN_CTRL_NAMES[_auto_gain_ctrl_ind]
    return AUTO_GAIN_CTRL_NAMES[AUTO_GAIN_CTRL_DISABLED]


def get_auto_gain_ctrl_ind(_auto_gain_ctrl_name: str) -> int:
    for ind in range(len(AUTO_GAIN_CTRL_NAMES)):
        if AUTO_GAIN_CTRL_NAMES[ind] == _auto_gain_ctrl_name:
            return ind
    return AUTO_GAIN_CTRL_DISABLED

def get_auto_gain_ctrl_global_cmd(enable_auto_gain_ctrl: bool) -> str:
    if enable_auto_gain_ctrl:
        return "SET AGC ON"
    else:
        return "SET AGC OFF"

def get_auto_gain_ctrl_conf_cmd(auto_gain_ctrl_ind: int, cid: str) -> str:
    return "CONFERENCE AGC %s %s" % (get_auto_gain_ctrl_name(auto_gain_ctrl_ind), cid)


def get_auto_gain_ctrl_abonent_cmd(auto_gain_ctrl_ind: int, conn_id: int) -> str:
    return "SET AGC %s #%d" % (get_auto_gain_ctrl_name(auto_gain_ctrl_ind), conn_id)



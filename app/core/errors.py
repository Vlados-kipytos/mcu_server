#!/usr/bin/python3

import app.core.tools as tools


class BadPageSizeError(Exception):
    def __init__(self, per_page: int, field: str):
        self._per_page = per_page
        self._field = field

    @property
    def per_page(self) -> int:
        return self._per_page

    @property
    def field(self) -> str:
        return self._field

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.page.size.field", "Bad page size", self._field, None) ]


class BadStartIndexError(Exception):
    def __init__(self, start_index: int, field: str):
        self._start_index = start_index
        self._field = field

    @property
    def start_index(self) -> int:
        return self._start_index

    @property
    def field(self) -> str:
        return self._field

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.start.index", "Bad start index", self._field, None) ]


class BadEndIndexError(Exception):
    def __init__(self, end_index: int, field: str):
        self._end_index = end_index
        self._field = field

    @property
    def end_index(self) -> int:
        return self._end_index

    @property
    def field(self) -> str:
        return self._field

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.end.index", "Bad end index", self._field, None) ]


class ContactNotFoundError(Exception):
    def __init__(self, contact_id: int, field: str):
        self._contact_id = contact_id
        self._field = field

    @property
    def contact_id(self) -> int:
        return self._contact_id

    @property
    def field(self) -> str:
        return self._field

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("contact.not.found", "Contact is not found", self._field, None) ]


class DuplicateSipAddressError(Exception):
    def __init__(self, address: str):
        self._address = address

    @property
    def address(self) -> str:
        return self._address

    def get_error_message(self):
        return "Duplicate sip address error"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("duplicate.sip.address.error", self.get_error_message(), "sip",
                                             None) ]


class DuplicateH323AddressError(Exception):
    def __init__(self, address: str):
        self._address = address

    @property
    def address(self) -> str:
        return self._address

    def get_error_message(self):
        return "Duplicate h323 address error"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("duplicate.h323.address.error", self.get_error_message(), "h323",
                                             None) ]


class DuplicateRTSPAddressError(Exception):
    def __init__(self, address: str):
        self._address = address

    @property
    def address(self) -> str:
        return self._address

    def get_error_message(self):
        return "Duplicate rtsp address error"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("duplicate.rtsp.address.error", self.get_error_message(), "rtsp",
                                             None) ]


class DuplicateFileUriError(Exception):
    def __init__(self, address: str):
        self._address = address

    @property
    def address(self) -> str:
        return self._address

    def get_error_message(self):
        return "Duplicate file URI error"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("duplicate.file.uri.error", self.get_error_message(), "file",
                                             None) ]


class InvalidAddressFormatError(Exception):
    def __init__(self, bad_address: str, format: str, field: str):
        self._bad_address = bad_address
        self._format = format
        self._field = field

    @property
    def bad_address(self) -> str:
        return self._bad_address

    @property
    def format(self) -> str:
        return self._format

    @property
    def field(self) -> str:
        return self._field

    def get_error_message(self):
        return "Invalid address format: %s. Use format: %s" % (self._bad_address, self._format)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("invalid.address.format", "Invalid format of address", self._field, None) ]


class GenerateContactIDError(Exception):
    # NOTE: cannot generate new id for abonent
    def __init__(self):
        pass

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("generate.contact.id.error", "Generate contact id error",
                                             "database", None) ]


class AddContactError(Exception):
    def __init__(self, contactname):
        self._contactname = contactname

    @property
    def contactname(self) -> str:
        return self._contactname

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("add.new.contacts.error", "Add new contact error", "database", None) ]


class DuplicateContactNameError(Exception):
    def __init__(self, name: str):
        self._name = name

    @property
    def name(self) -> str:
        return self._name

    def get_error_message(self):
        return "Duplicate contact name error"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("duplicate.contact.name.error", self.get_error_message(), "name",
                                             None) ]


class ChangeContactError(Exception):
    # NOTE: cannot update contact in database
    def __init__(self):
        pass

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("change.contacts.error", "Change contact error", "database", None) ]


class DeleteContactError(Exception):
    def __init__(self, message: str):
        self._message = message

    @property
    def message(self) -> str:
        return self._message

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.contacts.error", "Delete contact error", "database", None) ]


class ConferenceNotFoundError(Exception):
    def __init__(self, confid: int, field: str):
        self._confid = confid
        self._field = field

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def field(self) -> str:
        return self._field

    def get_error_message(self):
        return "Conference not found"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("conference.not.found", self.get_error_message(), self._field, None) ]


class AudioConfigurationNotFoundError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "audio configuration not found"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("audio.configuration.not.found.error", self.get_error_message(), "database",
                                            None)]


class VideoConfigurationNotFoundError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "video configuration not found"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("video.configuration.not.found.error", self.get_error_message(), "database",
                                            None)]


class UpdateAudioConfigError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "update audio configuration error"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("update.audio.configuration.error", self.get_error_message(), "database",
                                            None)]


class UpdateVideoConfigError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "update video configuration error"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("update.video.configuration.error", self.get_error_message(), "database",
                                            None)]


class CreateConferenceConfigurationError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "create conference configuration error"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("create.conference.configuration.error", self.get_error_message(),
                                            "database", None)]


class CreateConfereneceError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "create conference error"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("create.conference.error", self.get_error_message(),
                                            "database", None)]


class DeleteActiveConfereneceError(Exception):
    def __init__(self, field: str):
        self._field = field

    def get_error_message(self):
        return "delete active conference error"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.active.conference", "Delete active conference",
                                             self._field, None) ]


class DeleteConfereneceError(Exception):
    def __init__(self, message: str):
        self._message = message

    @property
    def message(self) -> str:
        return self._message

    def get_error_message(self):
        return "delete conference error: " % (self._message)

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("delete.conference.error", self.get_error_message(),
                                            "database", None)]


class ChangeConfereneceError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "change conference error"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("change.conference.error", self.get_error_message(), "database", None)]


class GetConferenceParticipantError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "error of getting participant of conference"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("get.conference.participant.error", self.get_error_message(), "database",
                                            None)]


class CreateConferenceParticipantError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "error of creating participant of conference"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("create.conference.participant.error", self.get_error_message(), "database",
                                            None)]


class ConferenceParticipantNotFoundError(Exception):
    def __init__(self, confid: int, abid: int, field: str):
        self._confid = confid
        self._abid = abid
        self._field = field

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def abid(self) -> int:
        return self._abid

    def get_error_message(self):
        return "participant %d of conference %d is not found" % (self._abid, self._confid)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("conference.participant.not.found", "Conference participant is not found",
                                             self._field, None) ]


class DeleteConferenceParticipantError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "error of deleting participant of conference"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.conference.participant.error", self.get_error_message(),
                                             "database", None) ]


class DeleteLayoutFromConferenceError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "error of deleting layout of participant of conference"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.layout.from.conference.error", self.get_error_message(),
                                             "database", None) ]


class ChangeConferenceParticipantError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "error of changing of conference's participant"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("change.conference.participant.error", self.get_error_message(),
                                             "database", None) ]


class SessionAbonentNotFoundError(Exception):
    def __init__(self, aburi: str, field: str):
        self._aburi = aburi
        self._field = field

    @property
    def aburi(self) -> str:
        return self._aburi

    @property
    def field(self) -> str:
        return self._field

    def get_error_message(self):
        return "abonent %s is not found in session" % (self._aburi)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("session.abonent.not.found", "Session abonent not found",
                                             self._field, None) ]


class BadConferenceStatusError(Exception):
    def __init__(self, message: str):
        self._message = message

    @property
    def message(self) -> str:
        return self._message

    def get_error_message(self):
        return "bad conference status: %s" % (self._message)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.conference.status.error", self.get_error_message(), "database",
                                             None) ]


class ConferenceLockedError(Exception):
    # NOTE: Cannot launch locked conference
    def __init__(self, field: str):
        self._field = field

    def get_error_message(self):
        return "conference is locked"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("conference.locked", "Conference locked", self._field, None) ]


class ConferenceAlreadyLaunchedError(Exception):
    # NOTE: Conference already launched
    def __init__(self, field: str):
        self._field = field

    def get_error_message(self):
        return "conference already launched"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("conference.already.launched", "Conference already launched",
                                             self._field, None) ]


class TooManyLayoutsError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "too many layouts"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("too.many.layouts.error", self.get_error_message(),
                                             "layout", None) ]


class SwitchConferenceStateError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "cannot switch conference state"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("switch.conference.state.error", self.get_error_message(), "state",
                                             None) ]


class ConferenceAlreadyStoppedError(Exception):
    # NOTE: Conference already stopped
    def __init__(self, field: str):
        self._field = field

    def get_error_message(self):
        return "conference already stopped"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("conference.already.stopped", "Conference already stopped",
                                             self._field, None) ]


class AlreadyInSessionError(Exception):
    # NOTE: you call to abonent who already connected to MCU
    def __init__(self, field: str):
        self._field = field

    def get_error_message(self):
        return "abonent already in session"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("already.in.session", "Address is used in session", self._field, None) ]


class SendCmdToEngineError(Exception):
    # NOTE: API/core cannot send some command(s) to vtsrv engine
    def __init__(self, cmd_list: list):
        self._cmd_list = cmd_list

    # cmd_list - list of strings (commands)
    @property
    def cmd_list(self) -> list:
        return self._cmd_list

    def get_error_message(self):
        err_str = "error of sending command(s) to engine:\n"
        for cmd in self.cmd_list:
            err_str += cmd + "\n"
        return err_str

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("send.engine.cmd.error", self.get_error_message(), "engine", None) ]


class GetOnlineStateError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "get online state error"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("get.online.state.error", self.get_error_message(),
                                            "engine", None)]


class BadLayoutNameError(Exception):
    def __init__(self, layout_name: str):
        self._layout_name = layout_name

    @property
    def layout_name(self) -> str:
        return self._layout_name

    def get_error_message(self):
        return "bad layout name"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("bad.layout.name.error", self.get_error_message(),
                                            "name", None)]


class NoLayoutOwnerError(Exception):
    def __init__(self, layout_name: str):
        self._layout_name = layout_name

    @property
    def layout_name(self) -> str:
        return self._layout_name

    def get_error_message(self):
        return "no owner for layout '%s'" % (self._layout_name)

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("no.layout.owner.error", self.get_error_message(), "participant_id", None)]


class ActiveConferenceNotFoundError(Exception):
    def __init__(self, confid: int, field: str):
        self._confid = confid
        self._field = field

    @property
    def confid(self) -> int:
        return self._confid

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("active.conference.not.found", "Active conference is not found",
                                             self._field, None) ]


class OnlineLayoutNotFoundError(Exception):
    def __init__(self, layout_name: str, field: str):
        self._layout_name = layout_name
        self._field = field

    @property
    def layout_name(self) -> str:
        return self._layout_name

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("online.layout.not.found", "Online layout is not found",
                                             self._field, None) ]


# you cannot delete global layout
class DeleteGlobalLayoutError(Exception):
    def __init__(self, layout_name: str):
        self._layout_name = layout_name

    @property
    def layout_name(self) -> str:
        return self._layout_name

    def get_error_message(self):
        return "you cannot delete global layout '%s'" % (self._layout_name)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.global.layout.error", self.get_error_message(),
                                             "layout", None) ]


# you cannot delete layout of active speaker
class DeleteAspeakerLayoutError(Exception):
    def __init__(self, layout_name: str):
        self._layout_name = layout_name

    @property
    def layout_name(self) -> str:
        return self._layout_name

    def get_error_message(self):
        return "you cannot delete layout of active speaker '%s'" % (self._layout_name)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.active.speaker.layout.error", self.get_error_message(),
                                             "layout", None) ]


# cannot find layout 'layout_id'
class LayoutNotFoundError(Exception):
    def __init__(self, layout_id: int, field: str):
        self._layout_id = layout_id
        self._field = field

    @property
    def layout_id(self) -> int:
        return self._layout_id

    def get_error_message(self):
        return "cannot find layout with id %d" % (self._layout_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("layout.not.found", "Layout is not found", self._field, None) ]


class TooManySlotRoles(Exception):
    def __init__(self, role_name: str):
        self._role_name = role_name

    @property
    def role_name(self) -> str:
        return self._role_name

    def get_error_message(self):
        return "layout contain too many slots with role %s" % (self._role_name)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("too.many.slot.roles", self.get_error_message(), "grid.slots", None) ]


class OldLayoutFormatError(Exception):

    def __init__(self, layout_id: int):
        self._layout_id = layout_id

    @property
    def layout_id(self) -> int:
        return self._layout_id

    def get_error_message(self):
        return "Old format of layout %d" % (self._layout_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("old.layout.format.error", "Old layout format", "database", None) ]


class BadSchemaTypeError(Exception):
    def __init__(self, schema_type: int, field: str):
        self._schema_type = schema_type
        self._field = field

    @property
    def schema_type(self) -> int:
        return self._schema_type

    def get_error_message(self):
        return "bad schema type: %d" % (self._schema_type)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.schema.type", "Bad schema type", self._field, None) ]


class EmptyLayoutSlotsListError(Exception):

    def __init__(self, layout_id: int):
        self._layout_id = layout_id

    @property
    def layout_id(self) -> int:
        return self._layout_id

    def get_error_message(self):
        return "no slots in layout %d" % (self._layout_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("no.slots.in.layout", self.get_error_message(), "grid.slots", None) ]


class EmptyLayoutOwnerError(Exception):

    def __init__(self):
        pass

    def get_error_message(self):
        return "individual layout must have owner"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("empty.layout.owner.error", self.get_error_message(), "participant_id",
                                             None) ]

class LayoutOfAbonentAlreadyExistError(Exception):

    def __init__(self, participant_id: int):
        self._participant_id = participant_id

    @property
    def participant_id(self) -> int:
        return self._participant_id

    def get_error_message(self):
        return "layout of abonent %d already exist" % (self._participant_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("layout.of.abonent.already.exist", self.get_error_message(),
                                             "participant_id", None) ]


class GenerateLayoutIDError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "cannot generate id for new layout"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("generate.layout.id.error", self.get_error_message(), "database", None) ]


class CreateLayoutError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "cannot create new layout"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("create.layout.error", self.get_error_message(), "database", None) ]


class AddLayoutToConferenceError(Exception):
    def __init__(self, layout_id: int, confid: int, abonentid: int):
        self._layout_id = layout_id
        self._confid = confid
        self._abonentid = abonentid

    @property
    def layout_id(self) -> int:
        return self._layout_id

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def abonentid(self) -> int:
        return self._abonentid

    def get_error_message(self):
        return "cannot add layout %d to conference %d" % (self._layout_id, self._confid)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("add.layout.to.conference.error", self.get_error_message(), "database",
                                             None) ]


class AddAbonentsToLayoutError(Exception):
    def __init__(self, abonentid: int, layout_id: int):
        self._abonentid = abonentid
        self._layout_id = layout_id

    @property
    def abonentid(self) -> int:
        return self._abonentid

    @property
    def layout_id(self) -> int:
        return self._layout_id

    def get_error_message(self):
        return "cannot add abonent %d to layout %d" % (self._abonentid, self._layout_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("add.abonents.to.layout.error", self.get_error_message(), "database",
                                             None) ]


class BadAbonentsNumberError(Exception):
    def __init__(self, numab: int):
        self._numab = numab

    @property
    def numab(self) -> int:
        return self._numab

    def get_error_message(self):
        return "bad abonents count: %d" % (self._numab)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.abonents.number.error", self.get_error_message(), "request", None) ]


# cannot delete one row of layout 'layoutid'
class DeleteLayoutAbonentsError(Exception):
    def __init__(self, layoutid: int):
        self._layoutid = layoutid

    @property
    def layoutid(self) -> int:
        return self._layoutid

    def get_error_message(self):
        return "cannot delete abonents of layout %d" % (self._layoutid)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.layout.abonents.error", self.get_error_message(), "database",
                                             None) ]

class UpdateLayoutError(Exception):
    def __init__(self, layoutid: int):
        self._layoutid = layoutid

    @property
    def layoutid(self) -> int:
        return self._layoutid

    def get_error_message(self):
        return "cannot update params of layout %d" % (self._layoutid)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("update.layout.error", self.get_error_message(), "database",
                                             None) ]


class DeleteScheduledLayoutFromConferenceError(Exception):
    def __init__(self, layoutid: int):
        self._layoutid = layoutid

    @property
    def layoutid(self) -> int:
        return self._layoutid

    def get_error_message(self):
        return "cannot delete layout %d from conference" % (self._layoutid)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.layout.from.conference.error", self.get_error_message(),
                                             "database", None) ]


class DeleteLayoutError(Exception):
    def __init__(self, layoutid: int):
        self._layoutid = layoutid

    @property
    def layoutid(self) -> int:
        return self._layoutid

    def get_error_message(self):
        return "cannot delete layout %d" % (self._layoutid)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.layout.error", self.get_error_message(), "database", None) ]


class SetLayoutModeError(Exception):
    def __init__(self, confid: int, aspeaker: bool):
        self._confid = confid
        self._aspeaker = aspeaker

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def aspeaker(self) -> bool:
        return self._aspeaker

    def get_error_message(self):
        if self._aspeaker:
            layout_mode = "active speaker"
        else:
            layout_mode = "common"
        return "cannot set mode '%s' of conference %d" % (layout_mode, self._confid)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("set.layout.mode.error", self.get_error_message(), "database", None) ]


class GroupNotFoundError(Exception):
    def __init__(self, group_id: int, field: str):
        self._group_id = group_id
        self._field = field

    @property
    def group_id(self) -> int:
        return self._group_id

    @property
    def field(self) -> str:
        return self._field

    def get_error_message(self):
        return "cannot find group %d" % (self._group_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("group.not.found.error", "Group not found", self._field, None) ]


class DuplicateGroupNameError(Exception):
    def __init__(self, name: str):
        self._name = name

    @property
    def name(self) -> str:
        return self._name

    def get_error_message(self):
        return "group with name '%s' already exist" % (self._name)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("duplicate.group.name.error", self.get_error_message(), "name", None) ]


class GenerateGroupIDError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "cannot generate new group id"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("generate.group.id.error", self.get_error_message(), "database", None) ]


class CreateNewGroupError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "cannot create new group"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("create.new.group.error", self.get_error_message(), "database", None) ]


class UpdateGroupError(Exception):
    def __init__(self, group_id: int):
        self._group_id = group_id

    @property
    def group_id(self) -> int:
        return self._group_id

    def get_error_message(self):
        return "cannot update group %d in database" % (self._group_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("update.group.error", self.get_error_message(), "database", None) ]


class DeleteGroupAbonentsError(Exception):
    def __init__(self, group_id: int):
        self._group_id = group_id

    @property
    def group_id(self) -> int:
        return self._group_id

    def get_error_message(self):
        return "cannot delete abonents of group %d" % (self._group_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.group.abonents.error", self.get_error_message(), "database",
                                             None) ]


class DeleteGroupError(Exception):
    def __init__(self, group_id: int):
        self._group_id = group_id

    @property
    def group_id(self) -> int:
        return self._group_id

    def get_error_message(self):
        return "cannot delete group %d" % (self._group_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.group.error", self.get_error_message(), "database", None) ]


class AlreadyInGroup(Exception):
    def __init__(self, group_id: int, contact_id: int, field: str):
        self._group_id = group_id
        self._contact_id = contact_id
        self._field = field

    @property
    def group_id(self) -> int:
        return self._group_id

    @property
    def contact_id(self) -> int:
        return self._contact_id

    @property
    def field(self) -> str:
        return self._field

    def get_error_message(self):
        return "abonent %d is already in group %d" % (self._contact_id, self._group_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("already.in.group.error", "Already in group", self._field, None) ]


class AddContactToGroupError(Exception):
    def __init__(self, group_id: int, contact_id: int):
        self._group_id = group_id
        self._contact_id = contact_id

    @property
    def group_id(self) -> int:
        return self._group_id

    @property
    def contact_id(self) -> int:
        return self._contact_id

    def get_error_message(self):
        return "cannot add abonent %d to group %d" % (self._contact_id, self._group_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("add.contact.to.group.error", self.get_error_message(), "database",
                                             None) ]

class ContactNotInGroup(Exception):
    def __init__(self, group_id: int, contact_id: int, field: str):
        self._group_id = group_id
        self._contact_id = contact_id
        self._field = field

    @property
    def group_id(self) -> int:
        return self._group_id

    @property
    def contact_id(self) -> int:
        return self._contact_id

    @property
    def field(self) -> str:
        return self._field

    def get_error_message(self):
        return "no abonent %d in group %d" % (self._contact_id, self._group_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("contact.not.in.group", "Contact is not in group", self._field, None) ]


class ErrorDelContactFromGroup(Exception):
    def __init__(self, group_id: int, contact_id: int):
        self._group_id = group_id
        self._contact_id = contact_id

    @property
    def group_id(self) -> int:
        return self._group_id

    @property
    def contact_id(self) -> int:
        return self._contact_id

    def get_error_message(self):
        return "cannot delete abonent %d from group %d" % (self._contact_id, self._group_id)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("delete.contact.from.group.error", self.get_error_message(), "database",
                                             None) ]

class SaveTemporaryLicenseError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "cannot save temporary license"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("save.tmp.license.error", self.get_error_message(), "filesystem", None) ]


class SplitGroupLicensesError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "cannot split licenses"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("split.group.licenses.error",self.get_error_message(),"filesystem",None) ]


class SaveZippedLicensesError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "cannot save zipped license"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("save.zipped.licenses.error",self.get_error_message(),"filesystem",None) ]


class OnlineRegistrationError(Exception):
    # NOTE: online registration engine return error code "err_code"
    def __init__(self, err_code: int, details: list, http_code: int):
        self._err_code = err_code
        self._details = details
        self._http_code = http_code

    @property
    def err_code(self) -> int:
        return self._err_code

    @property
    def details(self) -> list:
        return self._details

    @property
    def http_code(self) -> int:
        return self._http_code

    def get_error_message(self):
        return "online registration error N %d" % (self._err_code)

    def get_details_list(self) -> list:
        return self._details


class ChangeSettingsError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "Can't update settings"


class ParseLayoutCoordError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "error of parsing layout calculator result"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("parse.layout.coord.error", self.get_error_message(), "layout_grid.json",
                                             None) ]


class NoFieldInCalculatedLayoutError(Exception):
    def __init__(self, field_name: str):
        self._field_name = field_name

    @property
    def field_name(self) -> str:
        return self._field_name

    def get_error_message(self):
        return "no field %s in layout calculator result" % (self._field_name)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("no.field.in.calculated.layout.error", self.get_error_message(),
                                             self._field_name, None) ]


class NoFreeDiskSpaceError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "no free disk space"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("no.free.disk.space.error", self.get_error_message(), "filesystem", None) ]


class VideoStorageOverflowError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "video storage is overflow"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("video.storage.overflow.error", self.get_error_message(), "filesystem",
                                             None) ]


class DirectoryNotFoundError(Exception):
    def __init__(self, dirname: str, field: str):
        self._dirname = dirname
        self._field = field

    @property
    def dirname(self) -> str:
        return self._dirname

    def get_error_message(self):
        return "directory '%s' is not found" % (self._dirname)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("directory.not.found", "Directory is not found", self._field, None) ]


class MCUFileNotFoundError(Exception):
    def __init__(self, filename: str, field: str):
        self._filename = filename
        self._field = field

    @property
    def filename(self) -> str:
        return self._filename

    def get_error_message(self):
        return "file '%s' is not found" % (self._filename)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("mcu.file.not.found", "MCU file is not found", self._field, None) ]


class HandlingFileError(Exception):
    def __init__(self, filename: str):
        self._filename = filename

    @property
    def filename(self) -> str:
        return self._filename

    def get_error_message(self):
        return "error of handling file '%s'" % (self._filename)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("handling.file.error", self.get_error_message(), "filesystem", None) ]


class BadFileExtensionError(Exception):
    def __init__(self, extension: str, field: str):
        self._extension = extension
        self._field = field

    @property
    def extension(self) -> str:
        return self._extension

    def get_error_message(self):
        return "file extension '%s' is not supported" % (self._extension)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.file.extension", "Bad file extension", self._field, None) ]


class FileIsEmptyError(Exception):
    def __init__(self, filename: str):
        self._filename = filename

    @property
    def filename(self) -> str:
        return self._filename

    def get_error_message(self):
        return "file '%s' is empty" % (self._filename)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("file.is.empty.error", self.get_error_message(), "file", None) ]


class SettingIsRestrictedByLicense(Exception):
    def __init__(self, setting_name: str):
        self._setting_name = setting_name

    @property
    def setting_name(self) -> str:
        return self._setting_name

    def get_error_message(self):
        return "Setting '%s' is restricted by license" % (self._setting_name)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("setting.restricted.by.license", "Setting restricted by license",
                                             self._setting_name, None) ]


class MaxActiveParticipantsError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "max active participants error"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("max.active.participants.error", self.get_error_message(),
                                            "grid", None)]


class CreateExportFileError(Exception):
    def __init__(self, filename: str):
        self._filename = filename

    @property
    def filename(self) -> str:
        return self._filename

    def get_error_message(self):
        return "cannot create file '%s'" % (self._filename)

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("create.export.file.error", self.get_error_message(), "filesystem", None)]


class FileIOError(Exception):
    def __init__(self, errnum: int):
        self._errnum = errnum

    @property
    def errnum(self) -> int:
        return self._errnum

    def get_error_message(self):
        return "IO file operation error. errno=%d" % (self._errnum)

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("file.io.error", self.get_error_message(), "filesystem", None)]


class ExportAddressBookError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "cannot export address book"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("export.address.book.error", self.get_error_message(), "filesystem", None)]


class UnknownAddressBookTypeError(Exception):
    def __init__(self, abook_type: str):
        self._abook_type = abook_type

    @property
    def abook_type(self) -> str:
        return self._abook_type

    def get_error_message(self):
        return "Unknown address book type %s" % (self._abook_type)

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("unknown.address.book.type.error", self.get_error_message(), "abook_type",
                                            None)]


class ParseFileError(Exception):
    def __init__(self, filename: str):
        self._filename = filename

    @property
    def filename(self) -> str:
        return self._filename

    def get_error_message(self):
        return "cannot parse file '%s'" % (self._filename)

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("parse.file.error", self.get_error_message(), "file", None)]


class MountNetworkDirectoryError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "cannot mount network directory"

    def get_details_list(self) -> list:
        return [tools.get_detail_error_item("mount.network.directory.error", self.get_error_message(), "network", None)]


class PinCodeNotUniqueError(Exception):
    def __init__(self, pin: str, field: str):
        self._pin = pin
        self._field = field

    @property
    def pin(self) -> str:
        return self._pin

    def get_error_message(self):
        return "pin code is not unique: '%s'" % (self._pin)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("pin.code.not.unique", "Pin code is not unique", self._field, None) ]


class MaxConferenceParticipantsError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "Too many participants in conference."

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("max.conf.participants.error", self.get_error_message(), "conference",
                                             None) ]


class BadCallProtocolError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "Bad call protocol."

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.call.protocol.error", self.get_error_message(), "call_protocol",
                                             None) ]


class GetDatabaseDataError:
    def __init__(self):
        pass

    def get_error_message(self):
        return "Was some error at reading data from database."

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("read.database.error", self.get_error_message(), "database", None) ]


class UtiliteExecutionError(Exception):
    def __init__(self, message: str):
        self._message = message

    @property
    def message(self) -> str:
        return self._message

    def get_error_message(self):
        return "error at execute utilite: %s" % (self._message)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("utilite.execution.error", self.get_error_message(), "command", None) ]


class AbonentInLaunchedConference(Exception):
    def __init__(self, abid: int, field: str):
        self._abid = abid
        self._field = field

    @property
    def abid(self) -> int:
        return self._abid

    def get_error_message(self):
        return "abonent %d is in launched conference" % (self._abid)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("abonent.in.launched.conference",
                                             "Abonent is in launched conference", self._field, None) ]


class AddressIsUsedInLaunchedConference(Exception):
    def __init__(self, address: str, field: str):
        self._address = address
        self._field = field

    @property
    def address(self) -> str:
        return self._address

    @property
    def field(self) -> str:
        return self._field

    def get_error_message(self):
        return "address %s is used in launched conference" % (self._address)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("address.used.in.conference", "Address is used in conference",
                                             self._field, None) ]


class RecordingAlreadySwitchedOnError(Exception):
    def __init__(self, url_type: str):
        self._url_type = url_type

    @property
    def url_type(self) -> str:
        return self._url_type

    def get_error_message(self):
        return "recording with type %s is already switched on" % (self._url_type)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("recording.olready.on.error", self.get_error_message(),
                                             "session", None) ]


class RecordEmptyLayoutError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "you cannot make video record for empty layout"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("record.empty.layout.error", self.get_error_message(),
                                             "session", None) ]


class PresFileIsUsedInLaunchedConference(Exception):
    def __init__(self, filename: str):
        self._filename = filename

    @property
    def filename(self) -> str:
        return self._filename

    def get_error_message(self):
        return "presentation file %s is used in launched conference" % (self._filename)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("presentation.file.used.in.session.error", self.get_error_message(),
                                             "file", None) ]


class GetSettingsError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "Can't get settings"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("get.settings.error", self.get_error_message(), "database", None) ]


class NetworkInterfaceNotFoundError(Exception):
    def __init__(self, interface_name: str, field: str):
        self._interface_name = interface_name
        self._field = field

    @property
    def interface_name(self) -> str:
        return self._interface_name

    def get_error_message(self):
        return "Network interface '%s' is not found." % (self._interface_name)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("network.interface.not.found",
                                             "Network interface is not found", self._field, None) ]


class NetworkManagerError(Exception):
    def __init__(self, err_code: int):
        self._err_code = err_code

    @property
    def err_code(self) -> int:
        return self._err_code

    def get_error_message(self):
        return "nmcli return error code '%d'." % (self._err_code)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("network.manager.error", self.get_error_message(), "nmcli", None) ]


class NetworkManagerNotInstalledError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "NetworkManager is not installed"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("network.manager.not.installed.error", self.get_error_message(), "os",
                                             None) ]


class SendVideoToRTSPError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "You cannot send video to RTSP camera."

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("send.video.to.rtsp.error", self.get_error_message(),
                                             "", None) ]


class FeatureRestrictedByLicenseError(Exception):
    def __init__(self, feature: str):
        self._feature = feature

    def get_error_message(self):
        return "Feature '%s' restricted by license." % (self._feature)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("feature.restricted.by.license", "Feature restricted by license",
                                             self._feature, None) ]


class ExportDatabaseError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "error of export database to file"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("export.database.error", self.get_error_message(), "database", None) ]


class ImportDatabaseError(Exception):
    def __init__(self):
        pass

    def get_error_message(self):
        return "error of import database from file"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("import.database.error", self.get_error_message(), "database", None) ]


class BadLayoutTypeError(Exception):
    def __init__(self, layout_type: int, field: str):
        self._layout_type = layout_type
        self._field = field

    @property
    def layout_type(self) -> int:
        return self._layout_type

    def get_error_message(self):
        return "bad layout type %d" % (self._layout_type)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("bad.layout.type", "Bad layout type", self._field, None) ]


class ConcurrentLayoutsError(Exception):
    def __init__(self, layout_name1: str, layout_name2: str):
        self._layout_name1 = layout_name1
        self._layout_name2 = layout_name2

    @property
    def layout_name1(self) -> str:
        return self._layout_name1

    @property
    def layout_name2(self) -> str:
        return self._layout_name2

    def get_error_message(self):
        return "layouts '%s' and '%s' cannot work simultaneously" % (self._layout_name1, self._layout_name2)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("concurrent.layout.error", self.get_error_message(),
                                             "layout", None) ]

class NotCompatibleSettingsError(Exception):
    def __init__(self, message: str):
        self._message = message

    @property
    def message(self) -> str:
        return self._message

    def get_error_message(self):
        return "Not compatible settings. %s" % (self._message)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("not.compatible.settings.error", self.get_error_message(),
                                             "settings", None) ]


class CustomLayoutTypeNotFoundError(Exception):
    def __init__(self, layout_type: int, field: str):
        self._layout_type = layout_type
        self._field = field

    def get_error_message(self):
        return "Cannot find layout type %d" % (self._layout_type)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("custom.layout.type.not.found", self.get_error_message(),
                                             self._field, None) ]


class CustomLayoutTypeCreateError(Exception):
    def __init__(self, field: str):
        self._field = field

    def get_error_message(self):
        return "Cannot create layout type in database"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("custom.layout.type.create.error", self.get_error_message(),
                                             self._field, None) ]


class CustomLayoutTypeDeleteError(Exception):
    def __init__(self, field: str):
        self._field = field

    def get_error_message(self):
        return "Cannot delete layout type in database"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("custom.layout.type.delete.error", self.get_error_message(),
                                             self._field, None) ]


class CustomLayoutTypeChangeError(Exception):
    def __init__(self, field: str):
        self._field = field

    def get_error_message(self):
        return "Cannot change layout type in database"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("custom.layout.type.change.error", self.get_error_message(),
                                             self._field, None) ]


class CustomLayoutTypeSlotsIntersectError(Exception):
    def __init__(self, field: str):
        self._field = field

    def get_error_message(self):
        return "Custom layout type slots is intersected"

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("custom.layout.type.slots.intersect.error", self.get_error_message(),
                                             self._field, None) ]


class CustomLayoutTypeIsUsedError(Exception):
    def __init__(self, field: str, layout_type: int):
        self._field = field
        self._layout_type = layout_type

    def get_error_message(self):
        return "Custom layout type %d is used in some layouts." % (self.layout_type)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("custom.layout.type.is.used.error", self.get_error_message(),
                                             self._field, None) ]


# base class for errors, who contains list of common errors
class ComplexError(Exception):
    def __init__(self, http_code: int, details_list: list):
        self._http_code = http_code
        self._details_list = details_list

    # --- getters ---
    @property
    def http_code(self) -> int:
        return self._http_code

    @property
    def details_list(self) -> list:
        return self._details_list

    # --- setters ---
    @http_code.setter
    def http_code(self, value):
        self._http_code = value

    @details_list.setter
    def details_list(self, value):
        self._details_list = value

    # --- methods ---
    def get_error_message(self):
        return ""

    def get_details_list(self) -> list:
        return self._details_list


# container for list of errors in dialog creating/updating address book contact
class InvalidAbookContactParamsError(ComplexError):
    def __init__(self, details_list: list):
        ComplexError.__init__(self, 400, details_list)

    def get_error_message(self):
        return "Invalid address book contact params"


# container for list of errors in dialog creating/updating conference
class InvalidConferenceParamsError(ComplexError):
    def __init__(self, details_list: list):
        ComplexError.__init__(self, 400, details_list)

    def get_error_message(self):
        return "Invalid conference params"


# container for list of errors in dialog creating/updating conference's participant
class InvalidConferenceParticipantParamsError(ComplexError):
    def __init__(self, details_list: list):
        ComplexError.__init__(self, 400, details_list)

    def get_error_message(self):
        return "Invalid conference participant params"


# container for list of errors in dialog creating/updating scheduled layout
class InvalidScheduledLayoutParamsError(ComplexError):
    def __init__(self, details_list: list):
        ComplexError.__init__(self, 400, details_list)

    def get_error_message(self):
        return "Invalid scheduled layout params"


# container for list of errors in dialog creating/updating online layout
class InvalidOnlineLayoutParamsError(ComplexError):
    def __init__(self, details_list: list):
        ComplexError.__init__(self, 400, details_list)

    def get_error_message(self):
        return "Invalid online layout params"


class PowerManagementError(Exception):
    def __init__(self, message: str):
        self._message = message

    @property
    def message(self) -> str:
        return self._message

    def get_error_message(self):
        return "power management error: %s" % (self._message)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("power.management.error", self.get_error_message(), "command", None) ]


class InstallDebError(Exception):
    def __init__(self, err_code: int):
        self._err_code = err_code

    @property
    def err_code(self) -> int:
        return self._err_code

    def get_error_message(self):
        return "installer returns error code '%d'." % (self._err_code)

    def get_details_list(self) -> list:
        return [ tools.get_detail_error_item("install.deb.error", self.get_error_message(), "installer", None) ]


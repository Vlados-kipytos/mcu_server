import logging
import os
import os.path
import signal
import subprocess
import sys
import threading

import psutil

import app.core.tools as tools
import app.core.xmltools as xmltools
from app.core.errors import UtiliteExecutionError
from app.core.requests import GetUtilOutputRequest
from app.core.requests import StartUtilRequest
from app.core.requests import StopUtilRequest
from app.core.responses import StartUtilResponse
from app.core.responses import UtilResponse


def get_output_file_names(util_type: str):
    cfg = xmltools.get_config()
    dir = cfg["dirs"]["shareddir"]

    SLASH = os.path.sep

    out = dir + SLASH + util_type + "_out.log"
    err = dir + SLASH + util_type + "_err.log"

    return out, err


def stop_utilite(request: StopUtilRequest, _async: bool) -> StartUtilResponse:
    if sys.platform == "win32":
        if request.util_type == "ping":
            cmd = ["tskill.exe", "ping"]
        elif request.util_type == "traceroute":
            cmd = ["tskill.exe", "tracert"]
        elif request.util_type == "tcpdump":
            raise UtiliteExecutionError("No tcpdump utilite in OS Windows")
        else:
            raise UtiliteExecutionError("Unknown utilite type %s" % (request.util_type))
    else:
        if request.util_type == "ping":
            cmd = ["killall", "ping"]
        elif request.util_type == "traceroute":
            cmd = ["killall", "traceroute"]
        elif request.util_type == "tcpdump":
            cmd = ["killall", "tcpdump"]
        else:
            raise UtiliteExecutionError("Unknown utilite type %s" % (request.util_type))

    cmd_str = " ".join(cmd)
    logging.error(cmd_str)

    try:
        if _async:
            subprocess.Popen(cmd, shell=False)
        else:
            tools.exec_cmd(cmd, False)
    except:
        logging.error("stop_utilite() exception: " + str(sys.exc_info()))

    return StartUtilResponse(request.util_type)


def get_cmd_for_utilite(request: StartUtilRequest) -> list:
    if sys.platform == "win32":
        if request.util_type == "ping":
            # 3 pings of specified ip/host by IPv4
            cmd = ["ping.exe", "-n", "3", "-4", request.argument]
        elif request.util_type == "traceroute":
            # trace route to specified ip/host (no more 25 steps)
            cmd = ["tracert.exe", "-h", "25", request.argument]
        elif request.util_type == "tcpdump":
            raise UtiliteExecutionError("No tcpdump utilite in OS Windows")
        else:
            raise UtiliteExecutionError("Unknown utilite type %s" % (request.util_type))
    else:
        if request.util_type == "ping":
            # 3 pings of specified ip/host
            cmd = ["ping", "-c", "3", request.argument]
        elif request.util_type == "traceroute":
            # trace route to specified ip/host (no more 25 steps) by IPv4
            cmd = ["traceroute", "-4", "-m", "25", request.argument]
        elif request.util_type == "tcpdump":
            # capture network traffic to rotating files network_<datetime>.pcap (5 min for each rotatiing file)
            # max captured packets count: 250000
            cfg = xmltools.get_config()
            dir = cfg["dirs"]["utils"]
            filename = dir + "/network_dump_%Y_%m_%d_%H_%M_%S.pcap"
            cmd = ["tcpdump", "-c", "1000000", "-G", "300", "-Z", "root"]
            if len(request.argument) != 0:  # additional arguments/keys
                arg_list = request.argument.split()
                cmd += arg_list
            cmd += ["-w", filename]
        else:
            raise UtiliteExecutionError("Unknown utilite type %s" % (request.util_type))

    return cmd


def handle_sigchld(signum, frame):
    """Kill zombie"""
    os.wait()


def start_utilite_OLD(request: StartUtilRequest) -> StartUtilResponse:
    # stop utilite if it is running
    stop_request = StopUtilRequest(request.util_type)
    stop_utilite(stop_request, False)

    out_log, err_log = get_output_file_names(request.util_type)

    # remove output files if it is exists
    if os.path.exists(out_log):
        os.remove(out_log)

    if os.path.exists(err_log):
        os.remove(err_log)

    cmd = get_cmd_for_utilite(request)

    cmd_str = " ".join(cmd)
    logging.error(cmd_str)

    try:
        # open files without bufferization
        fout = open(out_log, "wb", 0)
        ferr = open(err_log, "wb", 0)

        if sys.platform != "win32":
            signal.signal(signal.SIGCHLD, handle_sigchld)

        subprocess.Popen(cmd, shell=False, stdout=fout, stderr=ferr)
    except:
        logging.error("start_utilite() exception: " + str(sys.exc_info()))

    return StartUtilResponse(request.util_type)


class cmd_launcher(threading.Thread):

    def __init__ (self, cmd):
        threading.Thread.__init__(self)
        self.cmd = cmd

    def run(self):
        logging.debug("cmd_launcher.run(): start util")
        logging.debug("cmd_launcher.run(): " + self.cmd)
        try:
            os.system(self.cmd)
        except:
            logging.error("cmd_launcher.run(): exception: " + str(sys.exc_info()))
        logging.debug("cmd_launcher.run(): stop util")


def start_utilite(request: StartUtilRequest) -> StartUtilResponse:
    # stop utilite if it is running
    stop_request = StopUtilRequest(request.util_type)
    stop_utilite(stop_request, False)

    out_log, err_log = get_output_file_names(request.util_type)

    # remove output files if it is exists
    #if os.path.exists(out_log):
    #    os.remove(out_log)

    #if os.path.exists(err_log):
    #    os.remove(err_log)

    cmd = get_cmd_for_utilite(request)

    cmd_str = " ".join(cmd)
    full_cmd_str = "%s 1>%s 2>%s" % (cmd_str, out_log, err_log)
    #logging.error(full_cmd_str)

    try:
        launcher_thread = cmd_launcher(full_cmd_str)
        launcher_thread.start()
    except:
        logging.error("start_utilite() exception: " + str(sys.exc_info()))

    return StartUtilResponse(request.util_type)


def is_program_running(_exe_fname: str):
    """If program with exe file name '_exe_fname' is running, then return True."""

    EXE_FNAME = _exe_fname.upper()

    for proc in psutil.process_iter(attrs=["name"]):
        CUR_EXE = proc.info["name"].upper()
        if CUR_EXE == EXE_FNAME:
            return True

    return False


def get_utilite_output(request: GetUtilOutputRequest) -> UtilResponse:
    # check running of program
    st_req = StartUtilRequest(request.util_type, "")
    cmd = get_cmd_for_utilite(st_req)
    if len(cmd) != 0:
        b_running = is_program_running(cmd[0])
    else:
        b_running = False

    out_log, err_log = get_output_file_names(request.util_type)

    # read output files
    output_str = ""
    error_str = ""
    try:
        fout = open(out_log, "rb")
        output_data = fout.read()
        fout.close()

        ferr = open(err_log, "rb")
        error_data = ferr.read()
        ferr.close()

        if sys.platform == "win32":
            output_str = output_data.decode("866")
            error_str = error_data.decode("866")
        else:
            output_str = output_data.decode("UTF-8")
            error_str = error_data.decode("UTF-8")
    except:
        logging.error("get_utilite_output() exception: " + str(sys.exc_info()))

    return UtilResponse(output_str, error_str, b_running)

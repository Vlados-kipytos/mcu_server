#!/usr/bin/python3

import copy
import logging
import time
import os
import os.path
import urllib

import app.core.data as data
import app.core.dbtools as dbtools
import app.core.features as features
import app.core.layoutcfgtools as layoutcfgtools
import app.core.tools as tools
import app.core.xmltools as xmltools
from app.core.entities import AudioCodecsEntity
from app.core.entities import ContactEntity
from app.core.entities import GroupEntity
from app.core.entities import VideoCodecsEntity
from app.core.entities import is_audio_codecs_equal
from app.core.entities import is_video_codecs_equal
from app.core.errors import AddContactError
from app.core.errors import AddContactToGroupError
from app.core.errors import AddressIsUsedInLaunchedConference
from app.core.errors import AlreadyInGroup
from app.core.errors import AlreadyInSessionError
from app.core.errors import BadEndIndexError
from app.core.errors import BadPageSizeError
from app.core.errors import BadStartIndexError
from app.core.errors import ChangeContactError
from app.core.errors import ConferenceAlreadyLaunchedError
from app.core.errors import ContactNotFoundError
from app.core.errors import ContactNotInGroup
from app.core.errors import CreateNewGroupError
from app.core.errors import DeleteGroupAbonentsError
from app.core.errors import DeleteGroupError
from app.core.errors import DuplicateContactNameError
from app.core.errors import DuplicateFileUriError
from app.core.errors import DuplicateGroupNameError
from app.core.errors import DuplicateH323AddressError
from app.core.errors import DuplicateRTSPAddressError
from app.core.errors import DuplicateSipAddressError
from app.core.errors import ErrorDelContactFromGroup
from app.core.errors import FeatureRestrictedByLicenseError
from app.core.errors import GenerateContactIDError
from app.core.errors import GenerateGroupIDError
from app.core.errors import GroupNotFoundError
from app.core.errors import InvalidAddressFormatError
from app.core.errors import UpdateGroupError
from app.core.requests import ChangeContactRequest
from app.core.requests import ChangeGroupRequest
from app.core.requests import CleanAddressBookRequest
from app.core.requests import CreateContactRequest
from app.core.requests import CreateGroupRequest
from app.core.requests import GetContactListRequest
from app.core.requests import GetContactRequest
from app.core.requests import GetGroupContactListRequest
from app.core.requests import GetGroupsListRequest
from app.core.responses import CleanAddressBookResponse
from app.core.responses import ContactListResponse
from app.core.responses import ContactResponse
from app.core.responses import DeleteContactFromGroupResponse
from .apptypes import ContactId


NO_GROUP_ID = 0


def get_full_videofile_name(file_uri: str, loop: bool) -> str:
    parsed_file_uri = tools.parse_address(file_uri)
    short_file_name = os.path.basename(parsed_file_uri["path"])
    if not tools.is_string_quoted(short_file_name):
        short_file_name = urllib.parse.quote(short_file_name)
    config = xmltools.get_config()
    parsed_file_uri["path"] = urllib.parse.quote(config["dirs"]["videodir"] + os.path.sep) + short_file_name
    if loop:
        parsed_file_uri["path"] += "#loop"
    new_file_uri = tools.join_parsed_address(parsed_file_uri)
    return new_file_uri


def get_abonents_connection_dict() -> dict:
    """This function returns dictionary of connected abonents dict[abid] = True"""

    result = {}
    ab_rows = dbtools.select("SELECT abonent_id FROM legacy.connections WHERE stop_at is NULL")
    for ab in ab_rows:
        abid = ab[0]
        if abid not in result:
            result[abid] = True

    return result


def get_all_connected_abonents_dict() -> dict:
    """This function returns dictionary of connected abonents dict[conn_id] = contact_id"""

    result = {}
    ab_rows = dbtools.select("SELECT abonent_id, connection_id FROM legacy.connections WHERE stop_at is NULL")
    for ab in ab_rows:
        abid = ab[0]
        conn_id = ab[1]
        if conn_id not in result:
            result[conn_id] = abid

    return result


def get_abonent_id(abonent_uri):
    """This function find 'abonent_uri' in database and returns id of found abonent.
    If abonent is not found, then return zero.
    NOTE: find abonent is NOT connected to conference."""
    if abonent_uri == None or abonent_uri == "":
        return (0)

    parsed_addr = tools.parse_address(abonent_uri)
    if len(parsed_addr["host"]) == 0 and parsed_addr["protocol"] != "media":
        return (0)  # error

    like_host = "'%" + parsed_addr["host"] + "%'"

    file_condition = ""
    if parsed_addr["protocol"] == "media":
        file_condition = " or (file is not null AND file<>'')"

    # strict compare
    ab_rows = dbtools.load_table("legacy.abonents", "id, sipaddr, h323addr, extaddr, file",
                                 "(sipaddr LIKE %s) or (h323addr LIKE %s) or (extaddr LIKE %s) %s" % (
                                     like_host, like_host, like_host, file_condition))
    for ab in ab_rows:
        id = ab[0]
        sipaddr = ab[1]
        h323addr = ab[2]
        extaddr = ab[3]
        file = ab[4]
        # logging.debug("abonent_uri=" + abonent_uri + " , sip=" + sipaddr + " , h323=" + h323addr)
        if tools.is_addr_group_equivalent(abonent_uri, sipaddr, h323addr, extaddr, file, True):
            return (id) # strict address found

    ab_conn_dict = get_abonents_connection_dict()

    # not strict compare
    for ab in ab_rows:
        id = ab[0]
        sipaddr = ab[1]
        h323addr = ab[2]
        extaddr = ab[3]
        file = ab[4]
        if tools.is_addr_group_equivalent(abonent_uri, sipaddr, h323addr, extaddr, file):
            if id in ab_conn_dict: # if found address already used in session
                continue
            else:
                return (id)

    return (0)


def get_ab_id_by_realaddr(abonent_uri):
    """This function find 'abonent_uri' in database and returns id of found abonent.
    If abonent is not found, then return zero."""
    if abonent_uri == None or abonent_uri == "":
        return (0)

    ab_rows = dbtools.select("SELECT abonent_id, real_uri FROM legacy.connections WHERE stop_at is NULL")
    for ab in ab_rows:
        id = ab[0]
        realaddr = ab[1]
        if realaddr == abonent_uri:
            return (id)

    return (0)


def get_realaddr_by_ab_id(abonent_id: int):
    """This function find 'abonent_id' in database and returns real address of found abonent."""
    if abonent_id == None or abonent_id == 0:
        return ("")

    ab_rows = dbtools.select("SELECT real_uri FROM legacy.connections WHERE abonent_id=%d AND stop_at is NULL" %
                             (abonent_id))
    if len(ab_rows) != 0:
        realaddr = ab_rows[0][0]
        return realaddr

    return ("")


def get_ab_id_by_conn_id(conn_id: int) -> int:
    """This function find abonent with specified connection id in database and returns id of found abonent.
    If abonent is not found, then return zero."""
    if conn_id == None:
        return 0

    ab_rows = dbtools.select("SELECT abonent_id FROM legacy.connections WHERE connection_id=%d AND stop_at is NULL" %
                             (conn_id))
    if len(ab_rows) != 0:
        return ab_rows[0][0]

    return 0


def get_conn_id_by_ab_id(abonent_id: int):
    """This function find 'abonent_id' in database and returns connection id of found abonent."""
    if abonent_id == None or abonent_id == 0:
        return -1

    ab_rows = dbtools.select("SELECT connection_id FROM legacy.connections WHERE abonent_id=%d AND stop_at is NULL" %
                             (abonent_id))
    if len(ab_rows) != 0:
        return ab_rows[0][0]

    return -1


def get_abid_by_hostport(aburi_without_protocol):
    """This function find 'aburi_without_protocol' in database and returns id of found abonent.
    If abonent is not found, then return zero."""
    if aburi_without_protocol == None or aburi_without_protocol == "":
        return (0)

    sip = tools.add_prefix_if_not(aburi_without_protocol, "sip:")
    h323 = tools.add_prefix_if_not(aburi_without_protocol, "h323:")
    rtsp = tools.add_prefix_if_not(aburi_without_protocol, "rtsp:")
    file = tools.add_prefix_if_not(aburi_without_protocol, "media:1@")

    sip = tools.quote_address(sip)
    h323 = tools.quote_address(h323)
    rtsp = tools.quote_address(rtsp)
    file = tools.quote_address(file)

    ab_rows = dbtools.load_table("legacy.abonents", "id, sipaddr, h323addr, extaddr, file", "")
    for ab in ab_rows:
        id = ab[0]
        sipaddr = ab[1]
        h323addr = ab[2]
        extaddr = ab[3]
        file_uri = ab[4]
        # logging.debug("abonent_uri=" + abonent_uri + " , sip=" + sipaddr + " , h323=" + h323addr)
        if tools.is_addr_equivalent(sip, sipaddr) or \
            tools.is_addr_equivalent(h323, h323addr) or \
            tools.is_addr_equivalent(rtsp, extaddr) or \
            tools.is_addr_equivalent(file, file_uri):
            return (id)

    return (0)


def is_abonent_connected(abid: int) -> bool:
    """This function check connection of abonent abid."""

    ab_rows = dbtools.select("SELECT connection_id FROM legacy.connections WHERE abonent_id=%d AND stop_at is NULL" %
                             (abid))
    if len(ab_rows) != 0:
        return True

    return False


def validate_sip_address(_sip_addr: str):
    """This function make correct sip address and returns it."""
    if _sip_addr == None or _sip_addr == "":
        return ""

    stripped_addr = _sip_addr.strip()
    addr_with_protocol = tools.add_prefix_if_not(stripped_addr, "sip:")

    if not tools.is_addr_quoted(addr_with_protocol):
       addr_with_protocol = tools.quote_address(addr_with_protocol)

    if tools.is_correct_sip_address(addr_with_protocol):
        return addr_with_protocol
    else:
        raise InvalidAddressFormatError(addr_with_protocol, "[sip:][username@]ip[:port]", "sip")


def validate_h323_address(_h323_addr: str):
    """This function make correct h323 address and returns it."""
    if _h323_addr == None or _h323_addr == "":
        return ""

    stripped_addr = _h323_addr.strip()
    addr_with_protocol = tools.add_prefix_if_not(stripped_addr, "h323:")

    if not tools.is_addr_quoted(addr_with_protocol):
       addr_with_protocol = tools.quote_address(addr_with_protocol)

    if tools.is_correct_h323_address(addr_with_protocol):
        return addr_with_protocol
    else:
        raise InvalidAddressFormatError(addr_with_protocol, "[h323:][username@]ip[:port]", "h323")


def validate_rtsp_address(_rtsp_addr: str):
    """This function make correct h323 address and returns it."""
    if _rtsp_addr == None or _rtsp_addr == "":
        return ""

    stripped_addr = _rtsp_addr.strip()
    addr_with_protocol = tools.add_prefix_if_not(stripped_addr, "rtsp:")

    if not tools.is_addr_quoted(addr_with_protocol):
       addr_with_protocol = tools.quote_address(addr_with_protocol)

    if tools.is_correct_rtsp_address(addr_with_protocol):
        return addr_with_protocol
    else:
        raise InvalidAddressFormatError(addr_with_protocol,
                                        "[rtsp:][//][login[:passwd]@]host[:port][/path][?prm1=value1[&prm2=value2]...]",
                                        "rtsp")


def validate_file_uri(_file_uri: str):
    """This function make correct file uri and returns it."""
    if _file_uri == None or _file_uri == "":
        return ""

    stripped_uri = _file_uri.strip()
    uri_with_protocol = tools.add_prefix_if_not(stripped_uri, "media:1@")

    if not tools.is_addr_quoted(uri_with_protocol):
       uri_with_protocol = tools.quote_address(uri_with_protocol)

    if tools.is_correct_file_uri(uri_with_protocol):
        return uri_with_protocol
    else:
        raise InvalidAddressFormatError(uri_with_protocol, "[media:1@]filename", "file")


def get_groups_of_abonent(_abid):
    """This function returns list of groups of abonent _abid."""
    _abid = int(_abid)

    gr_list = []
    gr_rows = dbtools.select(
        "SELECT id, name FROM legacy.groups WHERE id IN (SELECT groupid FROM legacy.groupabonents WHERE memberid=%d and is_subgroup=0)" % (
            _abid))
    for gr in gr_rows:
        id = gr[0]
        name = gr[1]
        gr_list.append({"id": id, "name": name})

    return (gr_list)


def get_groups_of_all_abonents():
    """This function return dictionary dict[abonent_id] = <list_of_groups>"""

    ab_dict = {}
    gr_ab_rows = dbtools.select(
        "SELECT groups.id, groups.name, groupabonents.memberid FROM legacy.groups, legacy.groupabonents WHERE groups.id=groupabonents.groupid and groupabonents.is_subgroup=0")
    for gr_ab in gr_ab_rows:
        grid = gr_ab[0]
        grname = gr_ab[1]

        abid = gr_ab[2]
        if abid not in ab_dict:
            ab_dict[abid] = []
        ab_dict[abid].append({"id": grid, "name": grname})

    return ab_dict


def get_no_group():
    return { "id": NO_GROUP_ID, "name": "Not in group" }


def get_no_group_size():
    cnt_rows = dbtools.select("""SELECT count(id) FROM legacy.abonents WHERE autoadded=0 AND id NOT IN 
    (SELECT memberid FROM legacy.groupabonents)""")
    if len(cnt_rows) != 0:
        return cnt_rows[0][0]
    else:
        return 0


def get_contact_by_id(request: GetContactRequest) -> ContactResponse:
    abid = request.id

    ab_row = dbtools.select("""SELECT id, name, dispname, sipaddr, h323addr, email, bitrate, 
    extaddr, realaddr, autoadded, createdat, conn_id,
    aac, g711, g726, g728, g722, g7221_24, g7221_32, g7221c_24, g7221c_32, g7221c_48, g729, g7231, g719,
    h261, h263, h264, h264high, h265,
    file, play_loop
    FROM legacy.abonents WHERE id=%d""" % (abid))
    if len(ab_row) == 1:
        id = ab_row[0][0]
        name = ab_row[0][1]
        dispname = ab_row[0][2]
        sipaddr = ab_row[0][3]
        h323addr = ab_row[0][4]
        email = ab_row[0][5]
        bitrate = ab_row[0][6]
        extaddr = ab_row[0][7]
        realaddr = ab_row[0][8]
        autoadded = tools.int_to_bool(ab_row[0][9])
        createdat_str = str(ab_row[0][10])
        createdat = tools.db_date_time_to_json(createdat_str)
        conn_id = ab_row[0][11]

        tmp_grlist = get_groups_of_abonent(id)
        if len(tmp_grlist) == 0:
            grlist = [ get_no_group() ]
        else:
            grlist = copy.deepcopy(tmp_grlist)

        audio_codecs = AudioCodecsEntity(True)
        audio_codecs.init_codecs(
            tools.int_to_bool(ab_row[0][12]), # aac
            tools.int_to_bool(ab_row[0][13]), # g711
            tools.int_to_bool(ab_row[0][14]), # g726
            tools.int_to_bool(ab_row[0][15]), # g728
            tools.int_to_bool(ab_row[0][16]), # g722
            tools.int_to_bool(ab_row[0][17]), # g7221_24
            tools.int_to_bool(ab_row[0][18]), # g7221_32
            tools.int_to_bool(ab_row[0][19]), # g722_1c_24
            tools.int_to_bool(ab_row[0][20]), # g722_1c_32
            tools.int_to_bool(ab_row[0][21]), # g722_1c_48
            tools.int_to_bool(ab_row[0][22]), # g729
            tools.int_to_bool(ab_row[0][23]), # g7231
            tools.int_to_bool(ab_row[0][24])  # g719
        )

        video_codecs = VideoCodecsEntity(True)
        video_codecs.init_codecs(
            tools.int_to_bool(ab_row[0][25]), # h261
            tools.int_to_bool(ab_row[0][26]), # h263
            tools.int_to_bool(ab_row[0][27]), # h264
            tools.int_to_bool(ab_row[0][28]), # h264high
            tools.int_to_bool(ab_row[0][29])  # h265
        )

        file = ab_row[0][30]
        loop = tools.int_to_bool(ab_row[0][31])

        contact = ContactEntity(id, name, dispname, sipaddr, h323addr, extaddr, email, bitrate, grlist, realaddr,
                                autoadded, createdat, conn_id, audio_codecs, video_codecs, file, loop)
        result = ContactResponse(contact)
    else:
        raise ContactNotFoundError(abid, "contact_id")

    return result


def get_contacts_list(request: GetContactListRequest) -> ContactListResponse:
    page = request.page
    per_page = request.per_page
    sort_by = request.sort_by
    order_by = request.order_by
    filter_name = request.name
    filter_email = request.email
    filter_disp_name = request.display_name
    filter_sip = request.sip
    filter_h323 = request.h323
    filter_rtsp = request.rtsp
    filter_auto = request.autoadded

    if per_page <= 0:
        raise BadPageSizeError(per_page, "per_page")

    ab_groups_dict = get_groups_of_all_abonents()

    data_json = []
    ab_list_rows = dbtools.select("""SELECT id, name, dispname, sipaddr, h323addr, email, bitrate, 
    extaddr, realaddr, autoadded, createdat, conn_id,
    aac, g711, g726, g728, g722, g7221_24, g7221_32, g7221c_24, g7221c_32, g7221c_48, g729, g7231, g719,
    h261, h263, h264, h264high, h265,
    file, play_loop
    FROM legacy.abonents ORDER BY name ASC""")
    for ab in ab_list_rows:
        id = ab[0]
        name = ab[1]
        dispname = ab[2]
        sipaddr = ab[3]
        h323addr = ab[4]
        email = ab[5]
        bitrate = ab[6]
        extaddr = ab[7]
        realaddr = ab[8]
        autoadded = tools.int_to_bool(ab[9])
        createdat_str = str(ab[10])
        createdat = tools.db_date_time_to_json(createdat_str)
        conn_id = ab[11]
        # tmp_grlist = get_groups_of_abonent(id)
        # grlist = copy.deepcopy(tmp_grlist)

        audio_codecs = AudioCodecsEntity(True)
        audio_codecs.init_codecs(
            tools.int_to_bool(ab[12]), # aac
            tools.int_to_bool(ab[13]), # g711
            tools.int_to_bool(ab[14]), # g726
            tools.int_to_bool(ab[15]), # g728
            tools.int_to_bool(ab[16]), # g722
            tools.int_to_bool(ab[17]), # g7221_24
            tools.int_to_bool(ab[18]), # g7221_32
            tools.int_to_bool(ab[19]), # g722_1c_24
            tools.int_to_bool(ab[20]), # g722_1c_32
            tools.int_to_bool(ab[21]), # g722_1c_48
            tools.int_to_bool(ab[22]), # g729
            tools.int_to_bool(ab[23]), # g7231
            tools.int_to_bool(ab[24])  # g719
        )

        video_codecs = VideoCodecsEntity(True)
        video_codecs.init_codecs(
            tools.int_to_bool(ab[25]), # h261
            tools.int_to_bool(ab[26]), # h263
            tools.int_to_bool(ab[27]), # h264
            tools.int_to_bool(ab[28]), # h264high
            tools.int_to_bool(ab[29])  # h265
        )

        file = ab[30]
        loop = tools.int_to_bool(ab[31])

        data_json.append(
            ContactEntity(ContactId(id), name, dispname, sipaddr, h323addr, extaddr, email, bitrate, [], realaddr,
                          autoadded, createdat, conn_id, audio_codecs, video_codecs, file, loop))

    # fill groups of abonents
    for abonent in data_json:
        if abonent.contact_id in ab_groups_dict:
            abonent.groups = copy.deepcopy(ab_groups_dict[abonent.contact_id])
        else:
            abonent.groups = [get_no_group()]

    # filter list of contacts
    filtered_ab_list = []
    for ab in data_json:
        find_name = filter_name != None and filter_name != ""
        if find_name:
            name_match = tools.is_param_match(ab.name, filter_name)
        else:
            name_match = False

        find_email = filter_email != None and filter_email != ""
        if find_email:
            email_match = tools.is_param_match(ab.email, filter_email)
        else:
            email_match = False

        find_disp_name = filter_disp_name != None and filter_disp_name != ""
        if find_disp_name:
            disp_name_match = tools.is_param_match(ab.display_name, filter_disp_name)
        else:
            disp_name_match = False

        find_sip = filter_sip != None and filter_sip != ""
        if find_sip:
            sip_match = tools.is_param_match(ab.sip, filter_sip)
        else:
            sip_match = False

        find_h323 = filter_h323 != None and filter_h323 != ""
        if find_h323:
            h323_match = tools.is_param_match(ab.h323, filter_h323)
        else:
            h323_match = False

        find_rtsp = filter_rtsp != None and filter_rtsp != ""
        if find_rtsp:
            rtsp_match = tools.is_param_match(ab.rtsp, filter_rtsp)
        else:
            rtsp_match = False

        find_auto = filter_auto != None
        if find_auto:
            auto_match = ab.autoadded == filter_auto
        else:
            auto_match = True

        if find_name or find_email or find_disp_name or find_sip or find_h323 or find_rtsp:
            any_param_match = (
                                          name_match or email_match or disp_name_match or sip_match or h323_match or rtsp_match) and auto_match
        else:
            any_param_match = auto_match
        if not any_param_match:
            continue

        filtered_ab_list.append(ab)

    ab_cnt = len(filtered_ab_list)
    if ab_cnt == 0:
        return (ContactListResponse([], 0))

    start_index = page * per_page
    if start_index < 0 or start_index >= ab_cnt:
        raise BadStartIndexError(start_index, "page")

    end_index = start_index + per_page - 1
    if end_index < 0:
        raise BadEndIndexError(end_index, "page")
    if end_index >= ab_cnt:
        end_index = ab_cnt - 1

    # get selected page of abonents
    out_ab_list = []
    index = start_index
    while index <= end_index:
        out_ab_list.append(filtered_ab_list[index])
        index += 1

    # sort contacts list
    cnt = len(out_ab_list)
    i = 0
    while i < cnt - 1:
        j = i + 1
        while j < cnt:
            if order_by == "asc":
                if out_ab_list[i].get_sort_field(sort_by) > out_ab_list[j].get_sort_field(sort_by):
                    out_ab_list[i], out_ab_list[j] = out_ab_list[j], out_ab_list[i]
            else:
                if out_ab_list[i].get_sort_field(sort_by) < out_ab_list[j].get_sort_field(sort_by):
                    out_ab_list[i], out_ab_list[j] = out_ab_list[j], out_ab_list[i]
            j += 1
        i += 1

    return ContactListResponse(out_ab_list, ab_cnt)


def create_new_contact_fast(request: CreateContactRequest) -> int:
    """NOTE: USE VALID INPUT PARAMS ONLY!!!"""
    ab_id = dbtools.generate_unique_fld("legacy.abonents", "id")
    if ab_id == 0:
        raise GenerateContactIDError()

    ab_created_at_str = tools.json_date_time_to_db(time.time())

    name_db = tools.str_to_dbstr(request.name)
    display_name_db = tools.str_to_dbstr(request.display_name)

    loop = tools.bool_to_int(request.loop)

    aac = tools.bool_to_int(request.audio_codecs.aac)
    g711 = tools.bool_to_int(request.audio_codecs.g711)
    g726 = tools.bool_to_int(request.audio_codecs.g726)
    g728 = tools.bool_to_int(request.audio_codecs.g728)
    g722 = tools.bool_to_int(request.audio_codecs.g722)
    g722_1_24 = tools.bool_to_int(request.audio_codecs.g722_1_24)
    g722_1_32 = tools.bool_to_int(request.audio_codecs.g722_1_32)
    g722_1c_24 = tools.bool_to_int(request.audio_codecs.g722_1c_24)
    g722_1c_32 = tools.bool_to_int(request.audio_codecs.g722_1c_32)
    g722_1c_48 = tools.bool_to_int(request.audio_codecs.g722_1c_48)
    g729 = tools.bool_to_int(request.audio_codecs.g729)
    g723_1 = tools.bool_to_int(request.audio_codecs.g723_1)
    g719 = tools.bool_to_int(request.audio_codecs.g719)

    h261 = tools.bool_to_int(request.video_codecs.h261)
    h263 = tools.bool_to_int(request.video_codecs.h263)
    h264 = tools.bool_to_int(request.video_codecs.h264)
    h264_high = tools.bool_to_int(request.video_codecs.h264_high)
    h265 = tools.bool_to_int(request.video_codecs.h265)

    sql = """INSERT INTO legacy.abonents (id, name, dispname, sipaddr, h323addr, email, bitrate, extaddr, realaddr, 
    autoadded, createdat, conn_id, 
    file, play_loop,
    aac, g711, g726, g728, g722, g7221_24, g7221_32, g7221c_24, g7221c_32, g7221c_48, g729, g7231, g719,
    h261, h263, h264, h264high, h265) 
    VALUES (%d,'%s', '%s', '%s','%s', '%s', %d, '%s', '%s', 
    %d, '%s', %d,
    '%s', %d,
    %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, 
    %d, %d, %d, %d, %d);""" % (
        ab_id, name_db, display_name_db, request.sip, request.h323, request.email, request.bitrate,
        request.rtsp, request.realaddr, tools.bool_to_int(request.autoadded), ab_created_at_str, request.conn_id,
        request.file_uri, loop,
        aac, g711, g726, g728, g722, g722_1_24, g722_1_32, g722_1c_24, g722_1c_32, g722_1c_48, g729, g723_1, g719,
        h261, h263, h264, h264_high, h265)
    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("create_new_contact_fast() database error: " + dbtools.get_last_error())
        raise AddContactError(request.name)

    return ab_id


def update_contact_fast(contact: ContactEntity):
    """NOTE: USE VALID INPUT PARAMS ONLY !!!"""

    name_db = tools.str_to_dbstr(contact.name)
    display_name_db = tools.str_to_dbstr(contact.display_name)

    loop = tools.bool_to_int(contact.loop)

    aac = tools.bool_to_int(contact.audio_codecs.aac)
    g711 = tools.bool_to_int(contact.audio_codecs.g711)
    g726 = tools.bool_to_int(contact.audio_codecs.g726)
    g728 = tools.bool_to_int(contact.audio_codecs.g728)
    g722 = tools.bool_to_int(contact.audio_codecs.g722)
    g722_1_24 = tools.bool_to_int(contact.audio_codecs.g722_1_24)
    g722_1_32 = tools.bool_to_int(contact.audio_codecs.g722_1_32)
    g722_1c_24 = tools.bool_to_int(contact.audio_codecs.g722_1c_24)
    g722_1c_32 = tools.bool_to_int(contact.audio_codecs.g722_1c_32)
    g722_1c_48 = tools.bool_to_int(contact.audio_codecs.g722_1c_48)
    g729 = tools.bool_to_int(contact.audio_codecs.g729)
    g723_1 = tools.bool_to_int(contact.audio_codecs.g723_1)
    g719 = tools.bool_to_int(contact.audio_codecs.g719)

    h261 = tools.bool_to_int(contact.video_codecs.h261)
    h263 = tools.bool_to_int(contact.video_codecs.h263)
    h264 = tools.bool_to_int(contact.video_codecs.h264)
    h264_high = tools.bool_to_int(contact.video_codecs.h264_high)
    h265 = tools.bool_to_int(contact.video_codecs.h265)

    sql = """UPDATE legacy.abonents set name='%s', dispname='%s', sipaddr='%s', h323addr='%s', email='%s', bitrate=%d, 
    extaddr='%s', realaddr='%s', autoadded=%d, conn_id=%d,
    file='%s', play_loop=%d,
    aac=%d, g711=%d, g726=%d, g728=%d, g722=%d, g7221_24=%d, g7221_32=%d, g7221c_24=%d, g7221c_32=%d, g7221c_48=%d, 
    g729=%d, g7231=%d, g719=%d,
    h261=%d, h263=%d, h264=%d, h264high=%d, h265=%d WHERE id=%d""" % (
        name_db, display_name_db, contact.sip, contact.h323, contact.email, contact.bitrate,
        contact.rtsp, contact.realaddr, tools.bool_to_int(contact.autoadded), contact.conn_id,contact.file,loop,
        aac, g711, g726, g728, g722, g722_1_24, g722_1_32, g722_1c_24, g722_1c_32, g722_1c_48,
        g729, g723_1, g719,
        h261, h263, h264, h264_high, h265, contact.contact_id)
    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("update_contact_fast(): database error: " + dbtools.get_last_error())
        raise ChangeContactError()


def update_realaddr(_abid: int, _realaddr: str, _conn_id: int, _confid: int, _outgoing: bool):
    """Optimized version of updating contact."""

    conn_rows = dbtools.select("SELECT id FROM legacy.connections WHERE connection_id=%d AND stop_at is NULL" %
                               (_conn_id))
    if len(conn_rows) != 0:
        id = conn_rows[0][0]
        res = dbtools.make_sql_request("""UPDATE legacy.connections SET abonent_id=%d, real_uri='%s', conference_id=%d 
                                       WHERE id=%d""" % (_abid, _realaddr, _confid, id))
        if res == 0:
            logging.error("update_realaddr(): database error: " + dbtools.get_last_error())
    else:
        start_at_str = tools.json_date_time_to_db(time.time())
        outgoing_num = tools.bool_to_int(_outgoing)
        id = dbtools.insert_with_serial("""INSERT INTO legacy.connections 
            (abonent_id, start_at, real_uri, connection_id, conference_id, outgoing, stop_at) 
            VALUES (%d, '%s', '%s', %d, %d, %d, NULL) RETURNING id""" %
            (_abid, start_at_str, _realaddr, _conn_id, _confid, outgoing_num))
        if id == 0:
            logging.error("update_realaddr(2): database error: " + dbtools.get_last_error())


def clear_realaddr(_abid: int, _realaddr: str, _conn_id: int, _confid: int, _outgoing: bool):
    """Clear 'realaddr' field for specified contact of address book."""

    stop_at_str = tools.json_date_time_to_db(time.time())
    outgoing_num = tools.bool_to_int(_outgoing)

    conn_rows = dbtools.select("SELECT id FROM legacy.connections WHERE connection_id=%d AND stop_at is NULL" %
                               (_conn_id))
    if len(conn_rows) != 0: # close opened connection
        sql = """UPDATE legacy.connections SET stop_at='%s' WHERE connection_id=%d AND stop_at is NULL""" % \
              (stop_at_str, _conn_id)
        res = dbtools.make_sql_request(sql)
        if res == 0:
            logging.error("clear_realaddr(): database error: " + dbtools.get_last_error())
    else: # create closed connection (for history)
        id = dbtools.insert_with_serial("""INSERT INTO legacy.connections 
            (abonent_id, start_at, real_uri, connection_id, conference_id, outgoing, stop_at) 
            VALUES (%d, '%s', '%s', %d, %d, %d, '%s') RETURNING id""" %
            (_abid, stop_at_str, _realaddr, _conn_id, _confid, outgoing_num, stop_at_str))
        if id == 0:
            logging.error("clear_realaddr(2): database error: " + dbtools.get_last_error())


def clear_all_realaddr():
    """Clear 'realaddr' field for all contacts of address book."""

    stop_at_str = tools.json_date_time_to_db(time.time())
    sql = """UPDATE legacy.connections SET stop_at='%s' WHERE stop_at is NULL""" % (stop_at_str)
    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("clear_all_realaddr(): database error: " + dbtools.get_last_error())


def rewrite_contact(request: CreateContactRequest, contact_id: int):
    """This function update contact 'contact_id' is equal (by some params) to created contact 'request'."""

    contact = ContactEntity(ContactId(contact_id), request.name, request.display_name, request.sip, request.h323,
                            request.rtsp, request.email, request.bitrate, [], request.realaddr, request.autoadded, 0,
                            request.conn_id, request.audio_codecs, request.video_codecs, request.file_uri, request.loop)
    update_contact_fast(contact)

    # get created contact
    req = GetContactRequest(contact_id)
    result = get_contact_by_id(req)
    return result


def get_unique_contact_name(_name: str) -> str:
    """This function build unique name for contact in address book."""

    ab_rows = dbtools.select("SELECT name FROM legacy.abonents")

    MAX_ITERATIONS_COUNT = 100
    iteration = 1
    try_name = _name
    while iteration < MAX_ITERATIONS_COUNT:
        found = False
        for ab in ab_rows:
            cur_name = ab[0]
            if cur_name == try_name:
                found = True
                break

        if found:
            iteration += 1
            try_name = "%s (%d)" % (_name, iteration)
        else:
            return try_name

    return try_name


def create_new_contact(request: CreateContactRequest) -> ContactResponse:
    request.sip = validate_sip_address(request.sip)
    request.h323 = validate_h323_address(request.h323)
    request.rtsp = validate_rtsp_address(request.rtsp)
    request.file_uri = validate_file_uri(request.file_uri)

    if request.rtsp != None and request.rtsp != "":
        flist = features.get_features_list()
        if not features.get_feature(flist, "RTSP"):
            raise FeatureRestrictedByLicenseError("RTSP")

    ab_conn_dict = get_abonents_connection_dict()

    # check: is new contact unique?
    abn_rows = dbtools.load_table("legacy.abonents", "id, name, sipaddr, h323addr, extaddr, realaddr, autoadded, file",
                                                     "")
    for ab in abn_rows:
        cur_id = ab[0]
        cur_name = ab[1]
        cur_sip = ab[2]
        cur_h323 = ab[3]
        cur_ext = ab[4]
        cur_realaddr = ab[5]
        cur_autoadded = tools.int_to_bool(ab[6])
        cur_file = ab[7]

        b_connected = cur_id in ab_conn_dict

        if request.name != "" and cur_name != "" and cur_name == request.name:
            if (not request.autoadded) and cur_autoadded:
                if b_connected:
                    raise AlreadyInSessionError("name")
                else:
                    return rewrite_contact(request, cur_id)
            else:
                raise DuplicateContactNameError(request.name)

        if request.sip != "" and cur_sip != "" and tools.is_addr_equivalent(cur_sip, request.sip, True):
            if (not request.autoadded) and cur_autoadded:
                if b_connected:
                    raise AlreadyInSessionError("sip")
                else:
                    return rewrite_contact(request, cur_id)
            else:
                raise DuplicateSipAddressError(request.sip)

        if request.h323 != "" and cur_h323 != "" and tools.is_addr_equivalent(cur_h323,request.h323,True):
            if (not request.autoadded) and cur_autoadded:
                if b_connected:
                    raise AlreadyInSessionError("h323")
                else:
                    return rewrite_contact(request, cur_id)
            else:
                raise DuplicateH323AddressError(request.h323)

        if request.rtsp != "" and cur_ext != "" and tools.is_addr_equivalent(cur_ext,request.rtsp,True):
            if (not request.autoadded) and cur_autoadded:
                if b_connected:
                    raise AlreadyInSessionError("rtsp")
                else:
                    return rewrite_contact(request, cur_id)
            else:
                raise DuplicateRTSPAddressError(request.rtsp)

        if request.file_uri != "" and cur_file != "" and tools.is_addr_equivalent(cur_file,request.file_uri,True):
            if (not request.autoadded) and cur_autoadded:
                if b_connected:
                    raise AlreadyInSessionError("file")
                else:
                    return rewrite_contact(request, cur_id)
            else:
                raise DuplicateFileUriError(request.file_uri)

    ab_id = create_new_contact_fast(request)

    # get created contact
    req = GetContactRequest(ab_id)
    result = get_contact_by_id(req)

    return (result)


def delete_abonent(ab_id):
    """This function deletes one abonent ab_id from address book.
    At success function returns empty string. At error function returns error message."""
    ab_id = str(ab_id)

    request = "DELETE from legacy.outgoingcalls WHERE abid=%s" % (ab_id)
    result = dbtools.make_sql_request(request)
    if result == 0:
        logging.error(
            "Error: cannot clean links of deleted abonent (in table outgoingcalls). " + dbtools.get_last_error())
        return ("Error: cannot clean links of deleted abonent (in table outgoingcalls). ")

    stop_at_str = tools.json_date_time_to_db(time.time())
    sql_request = "UPDATE legacy.connections SET stop_at='%s' WHERE abonent_id=%s AND stop_at is NULL" % \
                  (stop_at_str, ab_id)
    result = dbtools.make_sql_request(sql_request)
    if result == 0:
        logging.error("Error: cannot clean connections of deleted abonent. " + dbtools.get_last_error())
        return ("Error: cannot clean connections of deleted abonent. ")

    sql_request = "DELETE from legacy.groupabonents WHERE memberid=%s and is_subgroup=0" % (ab_id)
    result = dbtools.make_sql_request(sql_request)
    if result == 0:
        logging.error("Error: cannot clean links of deleted abonent. " + dbtools.get_last_error())
        return ("Error: cannot clean links of deleted abonent. ")

    request = "DELETE FROM legacy.confabonents WHERE abid=%s" % (ab_id)
    result = dbtools.make_sql_request(request)
    if result == 0:
        logging.error("Error: cannot clean deleted abonent in conferences. " + dbtools.get_last_error())
        return ("Error: cannot clean deleted abonent in conferences. ")

    # OLD:
    # request = "DELETE FROM legacy.layoutabonents WHERE show_abid=%s" % (ab_id)
    # result = dbtools.make_sql_request(request)
    # if result == 0:
    #    logging.error("Error: cannot clean deleted abonent in layout abonents. " + dbtools.get_last_error())
    #    return ("Error: cannot clean deleted abonent in layout abonents. ")

    # OLD: request = "DELETE FROM layoutconfigs WHERE abid=%s" % (ab_id)
    request = "UPDATE legacy.layoutconfigs SET abid=0, role=%d WHERE abid=%s" % (layoutcfgtools.AUTO_ROLE_ID, ab_id)
    result = dbtools.make_sql_request(request)
    if result == 0:
        logging.error("Error: cannot clean deleted abonent in layout configs. " + dbtools.get_last_error())
        return ("Error: cannot clean deleted abonent in layout configs. ")

    request = "DELETE FROM legacy.layoutconferences WHERE abid=%s" % (ab_id)
    result = dbtools.make_sql_request(request)
    if result == 0:
        logging.error("Error: cannot clean deleted abonent in table layoutconferences. " + dbtools.get_last_error())
        return ("Error: cannot clean deleted abonent in table layoutconferences. ")

    # request = "DELETE FROM layouts WHERE abid=%s" % (ab_id)
    # result = dbtools.make_sql_request(request)
    # if result == 0:
    #  return( "Error: cannot clean deleted abonent in layouts. " + dbtools.get_last_error() )

    request = "DELETE FROM legacy.movetoconf WHERE abid=%s" % (ab_id)
    result = dbtools.make_sql_request(request)
    if result == 0:
        logging.error("Error: cannot clean deleted abonent in 'move to conference' buffer. " + dbtools.get_last_error())
        return ("Error: cannot clean deleted abonent in 'move to conference' buffer. ")

    # reset recording participant if delete it
    result = dbtools.make_sql_request("UPDATE legacy.conferences SET rtmp_ab_id=0 WHERE rtmp_ab_id=%s" % (ab_id))
    if result == 0:
        logging.error("Error: cannot reset record participant. " + dbtools.get_last_error())
        return ("Error: cannot reset record participant. ")

    sql_request = """DELETE from legacy.abonents WHERE id=%s""" % (ab_id)
    result = dbtools.make_sql_request(sql_request)
    if result == 0:
        logging.error("Error: cannot delete abonent. " + dbtools.get_last_error())
        return ("Error: cannot delete abonent. ")

    return ("")


def update_contact(request: ChangeContactRequest) -> ContactResponse:
    ab_id = request.contact.contact_id

    # get old contact
    req = GetContactRequest(ab_id)
    old_con = get_contact_by_id(req)

    update_contact = request.contact

    if update_contact.rtsp != None and update_contact.rtsp != "":
        flist = features.get_features_list()
        if not features.get_feature(flist, "RTSP"):
            raise FeatureRestrictedByLicenseError("RTSP")

    if update_contact.name == None:
        update_contact.name = old_con.contact.name

    if update_contact.display_name == None:
        update_contact.display_name = old_con.contact.display_name

    if update_contact.sip == None:
        update_contact.sip = old_con.contact.sip
    else:
        update_contact.sip = validate_sip_address(update_contact.sip)

    if update_contact.h323 == None:
        update_contact.h323 = old_con.contact.h323
    else:
        update_contact.h323 = validate_h323_address(update_contact.h323)

    if update_contact.email == None:
        update_contact.email = old_con.contact.email

    if update_contact.bitrate == None:
        update_contact.bitrate = old_con.contact.bitrate

    if update_contact.rtsp == None:
        update_contact.rtsp = old_con.contact.rtsp
    else:
        update_contact.rtsp = validate_rtsp_address(update_contact.rtsp)

    if update_contact.file == None:
        update_contact.file = old_con.contact.file
    else:
        update_contact.file = validate_file_uri(update_contact.file)

    if update_contact.loop == None:
        update_contact.loop = old_con.contact.loop

    if update_contact.realaddr == None:
        update_contact.realaddr = old_con.contact.realaddr

    if update_contact.autoadded == None:
        update_contact.autoadded = old_con.contact.autoadded

    if update_contact.conn_id == None:
        update_contact.conn_id = old_con.contact.conn_id

    if update_contact.audio_codecs == None:
        update_contact.audio_codecs = old_con.contact.audio_codecs

    if update_contact.video_codecs == None:
        update_contact.video_codecs = old_con.contact.video_codecs

    ab_conn_dict = get_abonents_connection_dict()
    updated_ab_connected = ab_id in ab_conn_dict

    # if try update addresses of contact is used in conference
    change_important_field = update_contact.name != old_con.contact.name or update_contact.sip != old_con.contact.sip or \
                             update_contact.h323 != old_con.contact.h323 or update_contact.rtsp != old_con.contact.rtsp or \
                             update_contact.file != old_con.contact.file or \
                             update_contact.bitrate != old_con.contact.bitrate or \
                             (not is_audio_codecs_equal(update_contact.audio_codecs, old_con.contact.audio_codecs)) or \
                             (not is_video_codecs_equal(update_contact.video_codecs, old_con.contact.video_codecs))
    if updated_ab_connected and change_important_field:
        raise AlreadyInSessionError("contact_id")

    ab_rows = dbtools.load_table("legacy.abonents", "id", "name='%s' AND id<>%d" % (update_contact.name, ab_id))
    if len(ab_rows) != 0:
        raise DuplicateContactNameError(update_contact.name)

    abn_rows = dbtools.load_table("legacy.abonents", "id, name, sipaddr, h323addr, extaddr, realaddr, autoadded, file",
                                                     "")
    for ab in abn_rows:
        cur_id = ab[0]
        cur_name = ab[1]
        cur_sip = ab[2]
        cur_h323 = ab[3]
        cur_ext = ab[4]
        cur_realaddr = ab[5]
        cur_autoadded = tools.int_to_bool(ab[6])
        cur_file = ab[7]
        if int(cur_id) == int(ab_id):  # skip itself
            continue

        b_connected = cur_id in ab_conn_dict

        if update_contact.sip != "" and cur_sip != "" and \
            tools.is_addr_equivalent(cur_sip, update_contact.sip, True):
            if b_connected:
                raise AddressIsUsedInLaunchedConference(cur_sip, "sip")  # duplicate address is used in session
            elif cur_autoadded:
                delete_abonent(cur_id)  # duplicate address is 'hidden' address and not used now
            else:
                raise DuplicateSipAddressError(cur_sip)  # duplicate address is common contact in address book

        if update_contact.h323 != "" and cur_h323 != "" and \
            tools.is_addr_equivalent(cur_h323, update_contact.h323, True):
            if b_connected:
                raise AddressIsUsedInLaunchedConference(cur_h323, "h323")  # duplicate address is used in session
            elif cur_autoadded:
                delete_abonent(cur_id)  # duplicate address is 'hidden' address and not used now
            else:
                raise DuplicateH323AddressError(cur_h323)  # duplicate address is common contact in address book

        if update_contact.rtsp != "" and cur_ext != "" and \
            tools.is_addr_equivalent(cur_ext, update_contact.rtsp, True):
            if b_connected:
                raise AddressIsUsedInLaunchedConference(cur_ext, "rtsp")  # duplicate address is used in session
            elif cur_autoadded:
                delete_abonent(cur_id)  # duplicate address is 'hidden' address and not used now
            else:
                raise DuplicateRTSPAddressError(cur_ext)  # duplicate address is common contact in address book

        if update_contact.file != "" and cur_file != "" and \
            tools.is_addr_equivalent(cur_file, update_contact.file, True):
            if b_connected:
                raise AddressIsUsedInLaunchedConference(cur_file, "file")  # duplicate address is used in session
            elif cur_autoadded:
                delete_abonent(cur_id)  # duplicate address is 'hidden' address and not used now
            else:
                raise DuplicateFileUriError(cur_ext)  # duplicate address is common contact in address book

        # OLD:
        # tmp_json = get_contact_by_id(ab_id)
        # if tmp_json.has_key("error"):
        #    return( tools.get_error_json( 404, "ContactNotFound", "cannot find contact in database" ) )

    update_contact_fast(update_contact)

    # get changed contact
    req = GetContactRequest(ab_id)
    result = get_contact_by_id(req)

    return (result)


def get_group_by_id(_group_id: int):
    if _group_id == NO_GROUP_ID:
        gr_dict = get_no_group()
        size = get_no_group_size()
        return GroupEntity(_group_id, gr_dict["name"], size)

    gr_row = dbtools.select("SELECT name FROM legacy.groups WHERE id=%d" % (_group_id))
    if len(gr_row) == 0:
        raise GroupNotFoundError(_group_id, "group_id")

    count_rows = dbtools.select("SELECT count(*) FROM legacy.groupabonents WHERE groupid=%d" % (_group_id))
    if len(count_rows) != 0:
        ab_count = count_rows[0][0]
    else:
        ab_count = 0

    name = gr_row[0][0]
    return GroupEntity(_group_id, name, ab_count)


def get_groups_list(request: GetGroupsListRequest):
    if request.per_page <= 0 or request.per_page > 200:
        raise BadPageSizeError(request.per_page, "per_page")

    count_rows = dbtools.select("SELECT DISTINCT groupid, count(groupid) FROM legacy.groupabonents GROUP BY groupid")
    ab_cnt_dict = {}
    for row in count_rows:
        group_id = row[0]
        ab_cnt = row[1]
        ab_cnt_dict[group_id] = ab_cnt

    groups_list = []
    gr_rows = dbtools.select("SELECT id, name FROM legacy.groups ORDER BY name ASC")
    for gr in gr_rows:
        id = gr[0]
        name = gr[1]
        if request.filter_name != None and \
            request.filter_name != "" and \
            (not tools.is_param_match(name, request.filter_name)):
            continue
        ab_cnt = 0
        if id in ab_cnt_dict:
            ab_cnt = ab_cnt_dict[id]
        groups_list.append(GroupEntity(id, name, ab_cnt))

    groups_list.append(get_group_by_id(NO_GROUP_ID))

    gr_cnt = len(groups_list)
    if gr_cnt == 0:
        return []

    start_index = request.page * request.per_page
    if start_index < 0 or start_index >= gr_cnt:
        raise BadStartIndexError(start_index, "page")

    end_index = start_index + request.per_page - 1
    if end_index < 0:
        raise BadEndIndexError(end_index, "page")
    if end_index >= gr_cnt:
        end_index = gr_cnt - 1

    # get selected page of groups
    out_gr_list = []
    index = start_index
    while index <= end_index:
        out_gr_list.append(groups_list[index])
        index += 1

    return (out_gr_list)


def add_new_group_fast(request: CreateGroupRequest) -> GroupEntity:
    """This function adds new group in database. NOTE: USE CORRECT INPUT PARAMS ONLY!!!"""

    gr_id = dbtools.generate_unique_fld("legacy.groups", "id")
    if gr_id == 0:
        raise GenerateGroupIDError()

    db_name = tools.str_to_dbstr(request.name)

    sql = """INSERT INTO legacy.groups (id, name, collapse) VALUES (%d, '%s', 0);""" % (gr_id, db_name)
    res = dbtools.make_sql_request(sql)
    if res != 0:
        return GroupEntity(gr_id, request.name, 0)
    else:
        raise CreateNewGroupError()


def add_new_group(request: CreateGroupRequest):
    """This function adds new group in database."""

    gr_rows = dbtools.select("SELECT id FROM legacy.groups WHERE name='%s'" % (request.name))
    if len(gr_rows) != 0:
        raise DuplicateGroupNameError(request.name)

    result = add_new_group_fast(request)
    return result


def update_group(request: ChangeGroupRequest):
    """This function update group in database."""

    # check existing of group
    gr_row = dbtools.select("SELECT name FROM legacy.groups WHERE id=%d" % (request.group.group_id))
    if len(gr_row) == 0:
        raise GroupNotFoundError(request.group.group_id, "group_id")

    # find other group with same name
    gr_rows = dbtools.select(
        "SELECT id FROM legacy.groups WHERE name='%s' AND id<>%d" % (request.group.name, request.group.group_id))
    if len(gr_rows) != 0:
        raise DuplicateGroupNameError(request.group.name)

    # update group
    db_name = tools.str_to_dbstr(request.group.name)
    sql = """UPDATE legacy.groups SET name='%s' WHERE id=%d""" % (db_name, request.group.group_id)
    res = dbtools.make_sql_request(sql)
    if res != 0:
        group = get_group_by_id(request.group.group_id)
        return (group)
    else:
        raise UpdateGroupError(request.group.group_id)


def delete_group(_group_id: int):
    """This function deletes specified group."""

    # check existing of group
    gr_row = dbtools.select("SELECT name FROM legacy.groups WHERE id=%d" % (_group_id))
    if len(gr_row) == 0:
        raise GroupNotFoundError(_group_id, "group_id")

    sql = "DELETE from legacy.groupabonents WHERE groupid=%d" % (_group_id)
    res = dbtools.make_sql_request(sql)
    if res == 0:
        raise DeleteGroupAbonentsError(_group_id)

    sql = """DELETE from legacy.groups WHERE id=%d""" % (_group_id)
    res = dbtools.make_sql_request(sql)
    if res == 0:
        raise DeleteGroupError(_group_id)

    return (_group_id)


def get_contacts_of_group(request: GetGroupContactListRequest) -> ContactListResponse:
    """This function returns list of contacts of selected group"""

    if request.per_page <= 0 or request.per_page > 200:
        raise BadPageSizeError(request.per_page, "per_page")

    ab_groups_dict = get_groups_of_all_abonents()

    if request.group_id == NO_GROUP_ID:
        condition = "id NOT IN (SELECT memberid FROM legacy.groupabonents)"
    else:
        condition = "id IN (SELECT memberid FROM legacy.groupabonents WHERE groupid=%d and is_subgroup=0)" % \
                    (request.group_id)

    contacts_list = []
    ab_list_rows = dbtools.select("""SELECT id, name, dispname, sipaddr, h323addr, email, bitrate, 
    extaddr, realaddr, autoadded, createdat, conn_id,
    aac, g711, g726, g728, g722, g7221_24, g7221_32, g7221c_24, g7221c_32, g7221c_48, g729, g7231, g719,
    h261, h263, h264, h264high, h265,
    file, play_loop
    FROM legacy.abonents WHERE %s ORDER BY name""" % (condition))
    for ab in ab_list_rows:
        id = ab[0]
        name = ab[1]
        dispname = ab[2]
        sipaddr = ab[3]
        h323addr = ab[4]
        email = ab[5]
        bitrate = ab[6]
        extaddr = ab[7]
        realaddr = ab[8]
        autoadded = tools.int_to_bool(ab[9])
        createdat_str = str(ab[10])
        createdat = tools.db_date_time_to_json(createdat_str)
        conn_id = ab[11]
        # tmp_grlist = get_groups_of_abonent(id)
        # grlist = copy.deepcopy(tmp_grlist)

        audio_codecs = AudioCodecsEntity(True)
        audio_codecs.init_codecs(
            tools.int_to_bool(ab[12]), # aac
            tools.int_to_bool(ab[13]), # g711
            tools.int_to_bool(ab[14]), # g726
            tools.int_to_bool(ab[15]), # g728
            tools.int_to_bool(ab[16]), # g722
            tools.int_to_bool(ab[17]), # g7221_24
            tools.int_to_bool(ab[18]), # g7221_32
            tools.int_to_bool(ab[19]), # g722_1c_24
            tools.int_to_bool(ab[20]), # g722_1c_32
            tools.int_to_bool(ab[21]), # g722_1c_48
            tools.int_to_bool(ab[22]), # g729
            tools.int_to_bool(ab[23]), # g7231
            tools.int_to_bool(ab[24])  # g719
        )

        video_codecs = VideoCodecsEntity(True)
        video_codecs.init_codecs(
            tools.int_to_bool(ab[25]), # h261
            tools.int_to_bool(ab[26]), # h263
            tools.int_to_bool(ab[27]), # h264
            tools.int_to_bool(ab[28]), # h264high
            tools.int_to_bool(ab[29])  # h265
        )

        file = ab[30]
        loop = tools.int_to_bool(ab[31])

        contacts_list.append(
            ContactEntity(ContactId(id), name, dispname, sipaddr, h323addr, extaddr, email, bitrate, [], realaddr,
                          autoadded, createdat, conn_id, audio_codecs, video_codecs, file, loop))

    # fill groups of abonents
    for abonent in contacts_list:
        if abonent.contact_id in ab_groups_dict:
            abonent.groups = copy.deepcopy(ab_groups_dict[abonent.contact_id])
        else:
            abonent.groups = [ get_no_group() ]

    # filter list of contacts
    filtered_ab_list = []
    for ab in contacts_list:
        find_name = request.name != None and request.name != ""
        if find_name:
            name_match = tools.is_param_match(ab.name, request.name)
        else:
            name_match = False

        find_email = request.email != None and request.email != ""
        if find_email:
            email_match = tools.is_param_match(ab.email, request.email)
        else:
            email_match = False

        find_disp_name = request.display_name != None and request.display_name != ""
        if find_disp_name:
            disp_name_match = tools.is_param_match(ab.display_name, request.display_name)
        else:
            disp_name_match = False

        find_sip = request.sip != None and request.sip != ""
        if find_sip:
            sip_match = tools.is_param_match(ab.sip, request.sip)
        else:
            sip_match = False

        find_h323 = request.h323 != None and request.h323 != ""
        if find_h323:
            h323_match = tools.is_param_match(ab.h323, request.h323)
        else:
            h323_match = False

        find_rtsp = request.rtsp != None and request.rtsp != ""
        if find_rtsp:
            rtsp_match = tools.is_param_match(ab.rtsp, request.rtsp)
        else:
            rtsp_match = False

        find_auto = request.autoadded != None
        if find_auto:
            auto_match = ab.autoadded == request.autoadded
        else:
            auto_match = True

        if find_name or find_email or find_disp_name or find_sip or find_h323 or find_rtsp:
            any_param_match = (
                                          name_match or email_match or disp_name_match or sip_match or h323_match or rtsp_match) and auto_match
        else:
            any_param_match = auto_match
        if not any_param_match:
            continue

        filtered_ab_list.append(ab)

    ab_cnt = len(filtered_ab_list)
    if ab_cnt == 0:
        return (ContactListResponse([], 0))

    start_index = request.page * request.per_page
    if start_index < 0 or start_index >= ab_cnt:
        raise BadStartIndexError(start_index, "page")

    end_index = start_index + request.per_page - 1
    if end_index < 0:
        raise BadEndIndexError(end_index, "page")
    if end_index >= ab_cnt:
        end_index = ab_cnt - 1

    # get selected page of abonents
    out_ab_list = []
    index = start_index
    while index <= end_index:
        out_ab_list.append(filtered_ab_list[index])
        index += 1

    # sort contacts list
    cnt = len(out_ab_list)
    i = 0
    while i < cnt - 1:
        j = i + 1
        while j < cnt:
            if request.order_by == "asc":
                if out_ab_list[i].get_sort_field(request.sort_by) > out_ab_list[j].get_sort_field(request.sort_by):
                    out_ab_list[i], out_ab_list[j] = out_ab_list[j], out_ab_list[i]
            else:
                if out_ab_list[i].get_sort_field(request.sort_by) < out_ab_list[j].get_sort_field(request.sort_by):
                    out_ab_list[i], out_ab_list[j] = out_ab_list[j], out_ab_list[i]
            j += 1
        i += 1

    return ContactListResponse(out_ab_list, ab_cnt)


def add_contact_to_group_fast(_abid, _grid):
    """This function adds abonent to specified group. NOTE: USE CORRECT INPUT PARAMS ONLY!!!"""
    _abid = int(_abid)
    _grid = int(_grid)

    res = dbtools.make_sql_request(
        "insert into legacy.groupabonents (groupid, memberid, is_subgroup) values (%d, %d, 0);" % (_grid, _abid))
    if res == 0:
        logging.error("add_contact_to_group_fast(): db error: " + dbtools.get_last_error())
        raise AddContactToGroupError(_grid, _abid)


def add_contact_to_group(_abid, _grid):
    """This function adds abonent to specified group."""
    _abid = int(_abid)
    _grid = int(_grid)

    if _grid == NO_GROUP_ID:
        raise AddContactToGroupError(_grid, _abid)

    condition_str = "groupid=%d and memberid=%d and is_subgroup=0" % (_grid, _abid)
    rows = dbtools.load_table("legacy.groupabonents", "groupid, memberid, is_subgroup", condition_str)
    if len(rows) != 0:
        raise AlreadyInGroup(_grid, _abid, "contact_id")

    add_contact_to_group_fast(_abid, _grid)

    group = get_group_by_id(_grid)
    return group


def add_contacts_list_to_group(_abid_list: list, _grid: int):
    _abid_list = list(_abid_list)
    _grid = int(_grid)

    if len(_abid_list) == 0:
        return get_group_by_id(_grid)

    if _grid == NO_GROUP_ID:
        raise AddContactToGroupError(_grid, _abid_list[0])

    # check: is added abonents in group?
    abid_rows = dbtools.select("SELECT memberid FROM legacy.groupabonents WHERE groupid=%d" % (_grid))
    for row in abid_rows:
        cur_abid = row[0]
        if cur_abid in _abid_list:
            raise AlreadyInGroup(_grid, cur_abid, "contact_id_list")

    values_list_str = ""
    for i in range(len(_abid_list)):
        abid = _abid_list[i]
        values_list_str += "(%d, %d, 0)" % (_grid, abid)
        if i != len(_abid_list) - 1:
            values_list_str += ", "

    res = dbtools.make_sql_request(
        "insert into legacy.groupabonents (groupid, memberid, is_subgroup) values " + values_list_str)
    if res == 0:
        logging.error("add_contacts_list_to_group(): db error: " + dbtools.get_last_error())
        raise AddContactToGroupError(_grid, -1)

    return get_group_by_id(_grid)


def del_contact_from_group(_abid, _grid):
    """This function deletes abonent from specified group."""
    _abid = int(_abid)
    _grid = int(_grid)

    if _grid == NO_GROUP_ID:
        raise ErrorDelContactFromGroup(_grid, _abid)

    condition_str = "groupid=%d and memberid=%d and is_subgroup=0" % (_grid, _abid)
    rows = dbtools.load_table("legacy.groupabonents", "groupid, memberid, is_subgroup", condition_str)
    if len(rows) == 0:
        raise ContactNotInGroup(_grid, _abid, "contact_id")

    res = dbtools.make_sql_request("delete from legacy.groupabonents where " + condition_str)
    if res == 0:
        logging.error("del_contact_from_group(): " + dbtools.get_last_error())
        raise ErrorDelContactFromGroup(_grid, _abid)

    result = DeleteContactFromGroupResponse(_grid, _abid)
    return (result)


def add_abonent_to_groups(abid: int, gr_list: list):
    """This function adds abonent abid to all groups with names from gr_list."""

    for grname in gr_list:
        grid_rows = dbtools.select("SELECT id FROM legacy.groups WHERE name='%s'" % (grname))

        if len(grid_rows) != 0:
            grid = grid_rows[0][0]
        else:
            grid = dbtools.generate_unique_fld("legacy.groups", "id")
            db_name = tools.str_to_dbstr(grname)
            res = dbtools.make_sql_request(
                "INSERT INTO legacy.groups (id, name, collapse) VALUES (%d, '%s', 0)" % (grid, db_name))
            if res == 0:
                raise CreateNewGroupError()

        rows = dbtools.select(
            "SELECT groupid FROM legacy.groupabonents WHERE groupid=%d AND memberid=%d AND is_subgroup=0" % (
                grid, abid))
        if len(rows) == 0:
            res = dbtools.make_sql_request(
                "INSERT INTO legacy.groupabonents (groupid, memberid, is_subgroup) VALUES (%d, %d, 0)" % (grid, abid))
            if res == 0:
                raise AddContactToGroupError(grid, abid)


def get_all_groups_dict():
    """This function returns dictionary { "group_name": group_id }"""
    gr_dict = {}
    gr_rows = dbtools.select("SELECT id, name FROM legacy.groups")
    for gr in gr_rows:
        id = gr[0]
        name = gr[1]
        if name not in gr_dict:
            gr_dict[name] = id
    return gr_dict


def get_all_group_abonents():
    """This function returns dictionary { group_id1: { abonent_id1: True, anonent_id2: True } }"""
    gr_ab_dict = {}
    gr_ab_rows = dbtools.select("SELECT groupid, memberid FROM legacy.groupabonents WHERE is_subgroup=0")
    for gr_ab in gr_ab_rows:
        gr_id = gr_ab[0]
        ab_id = gr_ab[1]
        if gr_id not in gr_ab_dict:
            gr_ab_dict[gr_id] = {}
        if ab_id not in gr_ab_dict[gr_id]:
            gr_ab_dict[gr_id][ab_id] = True
    return gr_ab_dict


def get_all_abonent_names_dict() -> dict:
    """This function retuns dictionary: { abonent_id: { "name": <name>, "display_name": <display_name> }, ... }"""
    ab_dict = {}
    ab_list_rows = dbtools.select("SELECT id, name, dispname, sipaddr, h323addr, extaddr, file FROM legacy.abonents")
    for ab in ab_list_rows:
        id = ab[0]
        name = ab[1]
        dispname = ab[2]
        sipaddr = ab[3]
        h323addr = ab[4]
        extaddr = ab[5]
        file = ab[6]
        ab_dict[id] = {"name": name, "display_name": dispname, "sipaddr": sipaddr, "h323addr": h323addr,
                       "extaddr": extaddr, "file": file }
    return ab_dict


def get_conf_abonents(confid: int, b_callab_only: bool, b_filter_by_call_protocol: bool) -> list:
    """This function returns list of all scheduled abonents of conference confid."""

    callab_condition = ""
    if b_callab_only:
        callab_condition = " and callab!=0"

    ab_rows = dbtools.select("""SELECT DISTINCT abonents.id, abonents.name, abonents.dispname, abonents.sipaddr, 
        abonents.h323addr, abonents.extaddr, abonents.bitrate, abonents.email, confabonents.protocol,
        abonents.aac, abonents.g711, abonents.g726, abonents.g728, abonents.g722, 
        abonents.g7221_24, abonents.g7221_32, abonents.g7221c_24, abonents.g7221c_32, abonents.g7221c_48, 
        abonents.g729, abonents.g7231, abonents.g719,
        abonents.h261, abonents.h263, abonents.h264, abonents.h264high, abonents.h265,
        abonents.file, abonents.play_loop 
        FROM legacy.abonents, legacy.confabonents WHERE 
        abonents.id=confabonents.abid AND confabonents.confid=%d AND
        abonents.id IN (SELECT abid from legacy.confabonents WHERE confid=%d %s) """ % (confid,confid,callab_condition))

    ab_list = []
    for ab in ab_rows:
        id = ab[0]
        name = ab[1]
        dispname = ab[2]
        sipaddr = ab[3]
        h323addr = ab[4]
        extaddr = ab[5]
        bitrate = ab[6]
        email = ab[7]
        call_protocol_ind = ab[8]

        audio_codecs = AudioCodecsEntity(True)
        audio_codecs.init_codecs(
            tools.int_to_bool(ab[9]), # aac
            tools.int_to_bool(ab[10]), # g711
            tools.int_to_bool(ab[11]), # g726
            tools.int_to_bool(ab[12]), # g728
            tools.int_to_bool(ab[13]), # g722
            tools.int_to_bool(ab[14]), # g7221_24
            tools.int_to_bool(ab[15]), # g7221_32
            tools.int_to_bool(ab[16]), # g722_1c_24
            tools.int_to_bool(ab[17]), # g722_1c_32
            tools.int_to_bool(ab[18]), # g722_1c_48
            tools.int_to_bool(ab[19]), # g729
            tools.int_to_bool(ab[20]), # g7231
            tools.int_to_bool(ab[21])  # g719
        )

        video_codecs = VideoCodecsEntity(True)
        video_codecs.init_codecs(
            tools.int_to_bool(ab[22]), # h261
            tools.int_to_bool(ab[23]), # h263
            tools.int_to_bool(ab[24]), # h264
            tools.int_to_bool(ab[25]), # h264high
            tools.int_to_bool(ab[26])  # h265
        )

        file = ab[27]
        loop = tools.int_to_bool(ab[28])

        if b_filter_by_call_protocol:
            sipaddr = data.get_masked_addr(call_protocol_ind, sipaddr)
            h323addr = data.get_masked_addr(call_protocol_ind, h323addr)
            extaddr = data.get_masked_addr(call_protocol_ind, extaddr)

        ab_list.append( { "id": id, "name": name, "dispname": dispname, "sip": sipaddr, "h323": h323addr,
                          "rtsp": extaddr, "file": file, "loop": loop, "bitrate": bitrate, "email": email,
                          "audio_codecs": audio_codecs, "video_codecs": video_codecs } )

    return ab_list


def get_abonent_params(_abid: int) -> dict:
    """This function returns params of selected abonent (without gorups of abonent, no raise exceptions)."""

    result = { "success": False,
               "id": 0, "name": "", "dispname": "", "sip": "", "h323": "",
               "rtsp": "", "file": "", "loop": False, "bitrate": 0, "email": "" }

    ab_rows = dbtools.select("""SELECT id, name, dispname, sipaddr, h323addr, extaddr, bitrate, email,
        aac, g711, g726, g728, g722, g7221_24, g7221_32, g7221c_24, g7221c_32, g7221c_48, g729, g7231, g719,
        h261, h263, h264, h264high, h265,
        file, play_loop 
        FROM legacy.abonents WHERE id=%d""" % (_abid))
    if len(ab_rows) != 0:
        result["success"] = True
        result["id"] = ab_rows[0][0]
        result["name"] = ab_rows[0][1]
        result["dispname"] = ab_rows[0][2]
        result["sip"] = ab_rows[0][3]
        result["h323"] = ab_rows[0][4]
        result["rtsp"] = ab_rows[0][5]
        result["bitrate"] = ab_rows[0][6]
        result["email"] = ab_rows[0][7]

        audio_codecs = AudioCodecsEntity(True)
        audio_codecs.init_codecs(
            tools.int_to_bool(ab_rows[0][8]), # aac
            tools.int_to_bool(ab_rows[0][9]), # g711
            tools.int_to_bool(ab_rows[0][10]), # g726
            tools.int_to_bool(ab_rows[0][11]), # g728
            tools.int_to_bool(ab_rows[0][12]), # g722
            tools.int_to_bool(ab_rows[0][13]), # g7221_24
            tools.int_to_bool(ab_rows[0][14]), # g7221_32
            tools.int_to_bool(ab_rows[0][15]), # g722_1c_24
            tools.int_to_bool(ab_rows[0][16]), # g722_1c_32
            tools.int_to_bool(ab_rows[0][17]), # g722_1c_48
            tools.int_to_bool(ab_rows[0][18]), # g729
            tools.int_to_bool(ab_rows[0][19]), # g7231
            tools.int_to_bool(ab_rows[0][20])  # g719
        )
        result["audio_codecs"] = audio_codecs

        video_codecs = VideoCodecsEntity(True)
        video_codecs.init_codecs(
            tools.int_to_bool(ab_rows[0][21]), # h261
            tools.int_to_bool(ab_rows[0][22]), # h263
            tools.int_to_bool(ab_rows[0][23]), # h264
            tools.int_to_bool(ab_rows[0][24]), # h264high
            tools.int_to_bool(ab_rows[0][25])  # h265
        )
        result["video_codecs"] = video_codecs

        result["file"] = ab_rows[0][26]
        result["loop"] = tools.int_to_bool(ab_rows[0][27])

    return result


def clean_address_book(request: CleanAddressBookRequest) -> bool:

    # check launched conferences
    if tools.is_some_conf_launched():
        raise ConferenceAlreadyLaunchedError("some_conference")

    result = dbtools.make_sql_request("DELETE FROM legacy.outgoingcalls")
    if result == 0:
        err = dbtools.get_last_error()
        logging.error("Error: cannot clean links of abonents (in table outgoingcalls). " + err)
        return False

    stop_at_str = tools.json_date_time_to_db(time.time())
    sql_request = "UPDATE legacy.connections SET stop_at='%s' WHERE stop_at is NULL" % (stop_at_str)
    result = dbtools.make_sql_request(sql_request)
    if result == 0:
        logging.error("Error: cannot clean connections (in table connections). " + dbtools.get_last_error())
        return False

    result = dbtools.make_sql_request("DELETE FROM legacy.groupabonents")
    if result == 0:
        logging.error("Error: cannot clean links of abonents (in table groupabonents). " + dbtools.get_last_error())
        return False

    result = dbtools.make_sql_request("DELETE FROM legacy.groups")
    if result == 0:
        logging.error("Error: cannot clean groups. " + dbtools.get_last_error())
        return False

    # delete participants of conferences
    result = dbtools.make_sql_request("DELETE FROM legacy.confabonents")
    if result == 0:
        logging.error("Error: cannot clean conference's participants. " + dbtools.get_last_error())
        return False

    # get id of individual layouts
    lyt_id_str = ""
    layout_rows = dbtools.select("SELECT id FROM legacy.layoutconferences WHERE abid<>0")
    for i in range(len(layout_rows)):
        lyt_id = layout_rows[i][0]
        lyt_id_str += "%d" % (lyt_id)
        if i != len(layout_rows)-1:
            lyt_id_str += ","

    # delete individual layouts (if exists)
    if len(lyt_id_str) != 0:
        result = dbtools.make_sql_request("DELETE FROM legacy.layoutconfigs WHERE layout_id IN (%s)" % (lyt_id_str))
        if result == 0:
            logging.error("Error: cannot delete individual layouts (1). " + dbtools.get_last_error())
            return False
        result = dbtools.make_sql_request("DELETE FROM legacy.layoutconferences WHERE id IN (%s)" % (lyt_id_str))
        if result == 0:
            logging.error("Error: cannot delete individual layouts (2). " + dbtools.get_last_error())
            return False
        result = dbtools.make_sql_request("DELETE FROM legacy.layouts WHERE id IN (%s)" % (lyt_id_str))
        if result == 0:
            logging.error("Error: cannot delete individual layouts (3). " + dbtools.get_last_error())
            return False

    # change fixed slots to auto slots (in layouts)
    request = "UPDATE legacy.layoutconfigs SET abid=0, role=%d WHERE abid<>0" % (layoutcfgtools.AUTO_ROLE_ID)
    result = dbtools.make_sql_request(request)
    if result == 0:
        logging.error("Error: cannot clean abonents in layout configs. " + dbtools.get_last_error())
        return False

    result = dbtools.make_sql_request("DELETE FROM legacy.movetoconf")
    if result == 0:
        logging.error("Error: cannot clean abonents in 'move to conference' buffer. " + dbtools.get_last_error())
        return False

    # reset recording participant if delete it
    result = dbtools.make_sql_request("UPDATE legacy.conferences SET rtmp_ab_id=0")
    if result == 0:
        logging.error("Error: cannot reset record participants. " + dbtools.get_last_error())
        return False

    result = dbtools.make_sql_request("DELETE FROM legacy.abonents")
    if result == 0:
        logging.error("Error: cannot delete abonents. " + dbtools.get_last_error())
        return False

    return True

#!/usr/bin/python3

import logging
import time

import app.core.addrbook as addrbook
import app.core.callabtools as callabtools
import app.core.conftools as conftools
import app.core.constants as constants
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.nettools as nettools
import app.core.onlinetools as onlinetools
import app.core.tools as tools
import event_processor.ipc.on_event_tools as on_event_tools
from .entities import ContactEntity
from .errors import ConferenceAlreadyStoppedError
from .errors import SendVideoToRTSPError
from .errors import SessionAbonentNotFoundError
from .requests import BaseSwitchPreviewModeRequest
from .requests import CameraControlRequest
from .requests import ChangeContactRequest
# from .requests import TurnOnPreviewModeRequest
# from .requests import TurnOffPreviewModeRequest
from .requests import GetPreviewImageRequest
from .requests import SwitchAGCToGroupAbonentsRequest
from .requests import SwitchAudioToGroupAbonentsRequest
from .requests import SwitchVideoToGroupAbonentsRequest
from .requests import SwitchMicrophoneRequest
from .requests import SwitchNoiseSuppresorToGroupAbonentsRequest
from .requests import SwitchRenderModeToGroupAbonentsRequest
from .requests import SwitchVideoRequest
from .requests import TuneSpeakerVolumeRequest
from .responses import GetPreviewImageResponse
from .responses import SessionControlResponse
from .responses import SessionGroupOperationResponse
from .responses import SessionParticipantControlResponse

PREVIEW_TIMEOUT = 30

# param types:
PT_MICROPHONE = 0
PT_SPEAKER = 1
PT_AUDIO_GAIN_RX = 2
PT_AUDIO_GAIN_TX = 3
PT_VIDEO_TX = 4
PX_VIDEO_RX = 5

STEP_DURATION = 0.3
STEPS_COUNT = 20

#EXPERIMANTAL: change participant flags syncronous
def wait_for_participant_state(connection_id: int, param_type: int, param_value):
    """This function wait for changed abonent parameter."""
    for step in range(STEPS_COUNT):
        time.sleep(STEP_DURATION)

        connstr, confstr = onlinetools.get_online_state()
        conn_list = onlinetools.extract_conn_list(connstr)

        found_value = None
        for conn in conn_list:
            if conn["id"] == connection_id:
                if param_type == PT_MICROPHONE:
                    found_value = tools.int_to_bool(onlinetools.conn_to_enable_audio_status(conn))
                    break
                elif param_type == PT_SPEAKER:
                    found_value = tools.int_to_bool(onlinetools.conn_to_enable_audio_status_out(conn))
                    break
                elif param_type == PT_AUDIO_GAIN_RX:
                    found_value = conn["in"]["audio"]["gain_sB"]
                    break
                elif param_type == PT_AUDIO_GAIN_TX:
                    found_value = conn["out"]["audio"]["gain_sB"]
                    break
                elif param_type == PT_VIDEO_TX:
                    found_value = tools.int_to_bool(onlinetools.conn_to_enable_input_video(conn))
                    break

        if (found_value is not None) and found_value == param_value:
            logging.error("wait_for_participant_state() value found , OK")
            return True
        else:
            logging.error("wait_for_participant_state() current value = " + str(found_value))

    logging.error("wait_for_participant_state() value is not found , TIMEOUT")
    return False


def save_useaudio_rx_flag(b_use_audio: bool, conference_id: int, participant_id: int):
    """This funacion save in databese flag 'useaudio' (RX)"""

    useaudio_num = tools.bool_to_int(b_use_audio)

    dbtools.make_sql_request(
        "UPDATE legacy.confabonents SET useaudio=%d WHERE confid=%d and abid=%d" % (
        useaudio_num, conference_id, participant_id)
    )


def save_useaudio_tx_flag(b_use_audio: bool, conference_id: int, participant_id: int):
    """This funacion save in databese flag 'useaudio' (TX)"""

    useaudio_num = tools.bool_to_int(b_use_audio)

    dbtools.make_sql_request(
        "UPDATE legacy.confabonents SET useaudio_tx=%d WHERE confid=%d and abid=%d" % (
            useaudio_num, conference_id, participant_id)
    )


def switch_microphone(request: SwitchMicrophoneRequest) -> SessionParticipantControlResponse:
    """This function switch microphone of specified participant of conference."""

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    if request.use_conn_id:
        conn_id = request.participant_id
        aburi = onlinetools.get_realaddr_by_conn_id(conn_id, [])
        participant_id = addrbook.get_ab_id_by_conn_id(conn_id)
    else:
        aburi = addrbook.get_realaddr_by_ab_id(request.participant_id)
        conn_id = onlinetools.get_conn_id_by_realaddr(aburi, [])
        participant_id = request.participant_id
    if aburi == "":
        raise SessionAbonentNotFoundError(str(request.participant_id), "participant_id")

    logging.error("switch_microphone(): " + aburi)

    mute_num = tools.bool_to_int(not request.turn_on)
    on_off_str = tools.num_to_onoff(mute_num)

    conn_id_str = "#%d" % (conn_id)
    cmd = "SET MUTE " + on_off_str + " " + request.direction + " AUDIO " + conn_id_str
    nettools.exec_cmd_list_s([cmd])

    if request.direction == "RX":
        save_useaudio_rx_flag(request.turn_on, request.conference_id, participant_id)
        #wait_for_participant_state(conn_id, PT_MICROPHONE, request.turn_on)
    else:
        save_useaudio_tx_flag(request.turn_on, request.conference_id, participant_id)
        #wait_for_participant_state(conn_id, PT_SPEAKER, request.turn_on)

    result = SessionParticipantControlResponse(request.conference_id, request.participant_id)
    return result


def save_useaudio_flags_list(b_use_audio: bool, conference_id: int, participant_id_list: list, b_tx: bool):
    """This funacion save in databese flag 'useaudio' (TX)"""
    if len(participant_id_list) == 0:
        return

    useaudio_num = tools.bool_to_int(b_use_audio)

    if b_tx:
        db_flag_name = "useaudio_tx"
    else:
        db_flag_name = "useaudio"

    id_list_str = tools.id_list_to_db_list(participant_id_list)

    dbtools.make_sql_request(
        "UPDATE legacy.confabonents SET %s=%d WHERE confid=%d and abid IN %s" % (
            db_flag_name, useaudio_num, conference_id, id_list_str)
    )


def get_id_lists(_connection_id_list: list) -> (list, list):
    """This function convert _connection_id_list to 2 lists:
    1) list with connection id connected to MCU
    2) list of cantact id connected to MCU"""
    all_abonents_dict = addrbook.get_all_connected_abonents_dict()

    actual_connection_id_list = []
    actual_contact_id_list = []
    for conn_id in _connection_id_list:
        if conn_id in all_abonents_dict:
            actual_connection_id_list.append(conn_id)
            actual_contact_id_list.append(all_abonents_dict[conn_id])

    return actual_connection_id_list, actual_contact_id_list


def switch_group_audio(request: SwitchAudioToGroupAbonentsRequest) -> SessionGroupOperationResponse:
    """This function switch microphone/speaker of specified participants of conference."""
    if len(request.connection_id_list) == 0:
        return SessionGroupOperationResponse(request.conference_id, 0, 0)

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    actual_connection_id_list, actual_contact_id_list = get_id_lists(request.connection_id_list)
    processed_count = len(actual_connection_id_list)
    failed_count = len(request.connection_id_list) - processed_count

    logging.error("switch_group_audio() called")

    mute_num = tools.bool_to_int(not request.turn_on)
    on_off_str = tools.num_to_onoff(mute_num)

    cmd_list = []
    for conn_id in actual_connection_id_list:
        conn_id_str = "#%d" % (conn_id)
        cmd_list.append("SET MUTE " + on_off_str + " " + request.direction + " AUDIO " + conn_id_str)
    nettools.exec_cmd_list_s(cmd_list)

    if request.direction == "RX":
        save_useaudio_flags_list(request.turn_on, request.conference_id, actual_contact_id_list, False)
    else:
        save_useaudio_flags_list(request.turn_on, request.conference_id, actual_contact_id_list, True)

    result = SessionGroupOperationResponse(request.conference_id, processed_count, failed_count)
    return result




def set_speaker_volume(request: TuneSpeakerVolumeRequest) -> SessionParticipantControlResponse:
    """This function set sound volume of participant's speaker (in santibells)"""

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    if request.use_conn_id:
        conn_id = request.participant_id
        aburi = onlinetools.get_realaddr_by_conn_id(conn_id, [])
        participant_id = addrbook.get_ab_id_by_conn_id(conn_id)
    else:
        aburi = addrbook.get_realaddr_by_ab_id(request.participant_id)
        conn_id = onlinetools.get_conn_id_by_realaddr(aburi, [])
        participant_id = request.participant_id
    if aburi == "":
        raise SessionAbonentNotFoundError(str(request.participant_id), "participant_id")

    logging.error("set_speaker_volume(): " + aburi)

    MIN_VOLUME = -400
    MAX_VOLUME = 200

    volume = request.volume
    if volume < MIN_VOLUME:
        volume = MIN_VOLUME
    if volume > MAX_VOLUME:
        volume = MAX_VOLUME

    conn_id_str = "#%d" % (conn_id)
    cmd = "SET GAIN %s %d %s" % (conn_id_str, volume, request.direction)
    nettools.exec_cmd_list_s([cmd])

    if request.direction == "RX":
        dbtools.make_sql_request("UPDATE legacy.confabonents SET sndvol=%d WHERE confid=%d and abid=%d" % (
            volume, request.conference_id, participant_id))
        #wait_for_participant_state(conn_id, PT_AUDIO_GAIN_RX, volume)
    else:
        dbtools.make_sql_request("UPDATE legacy.confabonents SET sndvol_tx=%d WHERE confid=%d and abid=%d" % (
            volume, request.conference_id, participant_id))
        #wait_for_participant_state(conn_id, PT_AUDIO_GAIN_TX, volume)


    result = SessionParticipantControlResponse(request.conference_id, request.participant_id)
    return result


def save_usevideo_flag(b_usevideo: bool, conference_id: int, participant_id: int):
    """This function save in database flag 'usevideo' (RX)."""

    usevideo = tools.bool_to_int(b_usevideo)

    dbtools.make_sql_request(
        "UPDATE legacy.confabonents SET usevideo=%d WHERE confid=%d and abid=%d" % (
        usevideo, conference_id, participant_id)
    )


def switch_video(request: SwitchVideoRequest) -> SessionParticipantControlResponse:
    """This function add participants to layouts of conference or delete participant from layouts."""

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    if request.use_conn_id:
        conn_id = request.participant_id
        aburi = onlinetools.get_realaddr_by_conn_id(conn_id, [])
        participant_id = addrbook.get_ab_id_by_conn_id(conn_id)
    else:
        aburi = addrbook.get_realaddr_by_ab_id(request.participant_id)
        conn_id = onlinetools.get_conn_id_by_realaddr(aburi, [])
        participant_id = request.participant_id
    if aburi == "":
        raise SessionAbonentNotFoundError(str(request.participant_id), "participant_id")

    logging.error("switch_video(): " + aburi)

    usevideo = tools.bool_to_int(request.turn_on)

    conn_id_str = "#%d" % (conn_id)

    if request.direction == "TX":
        if request.turn_on and tools.is_rtsp_prefix(aburi):
            raise SendVideoToRTSPError()

        cmd = "SET MUTE %s TX VIDEO %s" % (tools.num_to_onoff(not usevideo), conn_id_str)
        nettools.exec_cmd_list_s([cmd])

        dbtools.make_sql_request("UPDATE legacy.confabonents SET usevideo_tx=%d WHERE confid=%d and abid=%d" % (
            usevideo, request.conference_id, participant_id))

        #wait_for_participant_state(conn_id, PT_VIDEO_TX, request.turn_on)
    else:
        cmd = "SET MUTE %s RX VIDEO %s" % (tools.num_to_onoff(not usevideo), conn_id_str)
        nettools.exec_cmd_list_s([cmd])
        save_usevideo_flag(request.turn_on, request.conference_id, participant_id)

    result = SessionParticipantControlResponse(request.conference_id, request.participant_id)
    return result


def save_usevideo_flags_list(b_usevideo: bool, conference_id: int, participant_id_list: list, b_tx: bool):
    """This function save in database flag 'usevideo'/'usevideo_tx' (RX/TX)"""
    if len(participant_id_list) == 0:
        return

    usevideo_num = tools.bool_to_int(b_usevideo)

    if b_tx:
        db_flag_name = "usevideo_tx"
    else:
        db_flag_name = "usevideo"

    id_list_str = tools.id_list_to_db_list(participant_id_list)

    dbtools.make_sql_request(
        "UPDATE legacy.confabonents SET %s=%d WHERE confid=%d and abid IN %s" % (
            db_flag_name, usevideo_num, conference_id, id_list_str)
    )


def switch_group_video(request: SwitchVideoToGroupAbonentsRequest) -> SessionGroupOperationResponse:
    """This function switch TX/RX video of specified participants of conference."""
    if len(request.connection_id_list) == 0:
        return SessionGroupOperationResponse(request.conference_id, 0, 0)

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    actual_connection_id_list, actual_contact_id_list = get_id_lists(request.connection_id_list)
    processed_count = len(actual_connection_id_list)
    failed_count = len(request.connection_id_list) - processed_count

    logging.error("switch_group_video() called")

    mute_num = tools.bool_to_int(not request.turn_on)
    on_off_str = tools.num_to_onoff(mute_num)

    cmd_list = []
    for conn_id in actual_connection_id_list:
        conn_id_str = "#%d" % (conn_id)
        cmd_list.append("SET MUTE " + on_off_str + " " + request.direction + " VIDEO " + conn_id_str)
    nettools.exec_cmd_list_s(cmd_list)

    save_usevideo_flags_list(request.turn_on, request.conference_id, actual_contact_id_list, request.direction == "TX")

    result = SessionGroupOperationResponse(request.conference_id, processed_count, failed_count)
    return result


def update_preview_time():
    """This function store in database last preview time."""

    cur_time_str = tools.json_date_time_to_db(time.time())

    preview_rows = dbtools.select("SELECT last_preview_time FROM legacy.preview")
    if len(preview_rows) == 0:
        dbtools.make_sql_request("INSERT INTO legacy.preview (last_preview_time) VALUES ('%s')" % (cur_time_str))
    else:
        dbtools.make_sql_request("UPDATE legacy.preview SET last_preview_time='%s'" % (cur_time_str))


def stop_preview():
    cmd_list = ["SET PREVIEW OFF"]
    nettools.exec_cmd_list_s(cmd_list)


def stop_preview_at_timeout():
    """This function stop preview mode and return True if operator turn on preview mode and don't update preview image long time."""

    preview_state = onlinetools.get_preview_state()
    if preview_state.state == "off":
        return False

    preview_rows = dbtools.select("SELECT last_preview_time FROM legacy.preview")
    if len(preview_rows) == 0:
        return False  # error

    last_preview_time_sec = tools.db_date_time_to_json(str(preview_rows[0][0]))
    cur_time = time.time()
    if cur_time > last_preview_time_sec + PREVIEW_TIMEOUT:
        stop_preview()
        logging.error("stop_preview_at_timeout(): stop preview")
        return True

    return False


def set_preview_mode(request: BaseSwitchPreviewModeRequest) -> SessionParticipantControlResponse:
    """This function switch preview mode for specified participant of session."""

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    if request.use_conn_id:
        conn_id = request.participant_id
        aburi = onlinetools.get_realaddr_by_conn_id(conn_id, [])
    else:
        aburi = addrbook.get_realaddr_by_ab_id(request.participant_id)
        conn_id = onlinetools.get_conn_id_by_realaddr(aburi, [])
    if aburi == "":
        raise SessionAbonentNotFoundError(str(request.participant_id), "participant_id")

    logging.error("set_preview_mode(): " + aburi)

    cid = tools.conference_name_by_id(request.conference_id)

    cmd_list = []
    if request.turn_on:
        preview_type = data.get_preview_type(request.stream)
        basecmd = data.get_preview_cmd(request.stream)

        if preview_type == data.PREVIEW_COMMON_STREAM:
            connstr, confstr = onlinetools.get_online_state()
            conf_json = onlinetools.extract_conference(confstr, request.conference_id)
            layout = onlinetools.extract_layout_by_conn_id(conf_json, conn_id)
            cmd_list.append(basecmd + cid + " " + layout["name"])

        elif preview_type == data.PREVIEW_PRESENTATION:
            cmd_list.append(basecmd + cid)

        elif preview_type == data.PREVIEW_CAMERA:
            cmd_list.append(basecmd + "#" + str(conn_id))

        if len(cmd_list) != 0:
            update_preview_time()

        nettools.exec_cmd_list_s(cmd_list)
    else:
        stop_preview()

    result = SessionParticipantControlResponse(request.conference_id, request.participant_id)
    return result


def get_preview_image(request: GetPreviewImageRequest) -> GetPreviewImageResponse:
    preview_image = nettools.get_url_data_s("mirror/preview.dat", False)
    update_preview_time()
    result = GetPreviewImageResponse(preview_image)
    return result


def send_cmd_to_camera(request: CameraControlRequest) -> SessionParticipantControlResponse:
    """This function send to session participant's camera command (to move/zoom camera)."""

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    if request.use_conn_id:
        conn_id = request.participant_id
        aburi = onlinetools.get_realaddr_by_conn_id(conn_id, [])
    else:
        aburi = addrbook.get_realaddr_by_ab_id(request.participant_id)
        conn_id = onlinetools.get_conn_id_by_realaddr(aburi, [])
    if aburi == "":
        raise SessionAbonentNotFoundError(str(request.participant_id), "participant_id")

    logging.error("send_cmd_to_camera(): " + aburi)

    engine_cmd = data.get_fecc_cmd(request.command, "#%d" % (conn_id))
    nettools.exec_cmd_list_s([engine_cmd])

    result = SessionParticipantControlResponse(request.conference_id, request.participant_id)
    return result


def rename_participant(conference_id: int, participant_id: int, dispname: str, use_conn_id: bool) -> SessionParticipantControlResponse:
    """This function set new display name for specified participant."""

    dispname = dispname.strip()

    status = conftools.get_conference_status(conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    if use_conn_id:
        conn_id = participant_id
        aburi = onlinetools.get_realaddr_by_conn_id(conn_id, [])
    else:
        aburi = addrbook.get_realaddr_by_ab_id(participant_id)
        conn_id = onlinetools.get_conn_id_by_realaddr(aburi, [])
    if aburi == "":
        raise SessionAbonentNotFoundError(str(participant_id), "participant_id")

    logging.error("rename_participant(): aburi=%s , conn_id=%d" % (aburi, conn_id))

    abonent = "#%d" % (conn_id)
    cmd_list = ["RENAME PEER " + abonent + " " + dispname]
    nettools.exec_cmd_list_s(cmd_list)

    # save new display name in database
    if use_conn_id:
        abid = addrbook.get_ab_id_by_conn_id(conn_id)
    else:
        abid = participant_id
    logging.error("rename_participant(): abid=%d" % (abid))
    contact = ContactEntity(abid, None, dispname, None, None, None, None, None, None, None, None, None, None, None,
                            None, None, None)
    request = ChangeContactRequest(contact)
    addrbook.update_contact(request)

    return SessionParticipantControlResponse(conference_id, participant_id)


def send_message_to_conference(confid: int, message: str) -> SessionControlResponse:
    """This function send message to all abonents of conference."""

    status = conftools.get_conference_status(confid)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    cid = tools.conference_name_by_id(confid)

    cmd_list = []
    cmd_list.append("CONFERENCE OPERATOR " + cid + " ->")
    cmd_list.append("CONFERENCE MESSAGE " + cid + " " + message)
    nettools.exec_cmd_list_s(cmd_list)

    return SessionControlResponse(confid)



def save_render_mode_flags_list(conference_id: int, participant_id_list: list, render_mode: str):
    """This funacion save in databese flag 'render_mode'."""
    if len(participant_id_list) == 0:
        return

    render_mode_ind = data.get_render_participant_index(render_mode)

    id_list_str = tools.id_list_to_db_list(participant_id_list)

    dbtools.make_sql_request(
        "UPDATE legacy.confabonents SET render_mode=%d WHERE confid=%d and abid IN %s" %
        (render_mode_ind, conference_id, id_list_str)
    )


def switch_group_render_mode(request: SwitchRenderModeToGroupAbonentsRequest) -> SessionGroupOperationResponse:
    """This function switch render mode of specified participants of conference."""
    if len(request.connection_id_list) == 0:
        return SessionGroupOperationResponse(request.conference_id, 0, 0)

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    actual_connection_id_list, actual_contact_id_list = get_id_lists(request.connection_id_list)
    processed_count = len(actual_connection_id_list)
    failed_count = len(request.connection_id_list) - processed_count

    logging.error("switch_group_render_mode() called")

    cmd_list = []
    for conn_id in actual_connection_id_list:
        conn_id_str = "#%d" % (conn_id)
        cmd_list.append("SET RENDER " + request.render_mode + " " + conn_id_str)

    nettools.exec_cmd_list_s(cmd_list)

    save_render_mode_flags_list(request.conference_id, actual_contact_id_list, request.render_mode)

    result = SessionGroupOperationResponse(request.conference_id, processed_count, failed_count)
    return result


def save_noise_suppressor_flags_list(conference_id: int, participant_id_list: list, noise_suppressor_ind: int):
    """This funacion save in databese flag 'noise_suppressor_ind'."""
    if len(participant_id_list) == 0:
        return

    id_list_str = tools.id_list_to_db_list(participant_id_list)

    dbtools.make_sql_request(
        "UPDATE legacy.confabonents SET noise_suppressor=%d WHERE confid=%d and abid IN %s" %
        (noise_suppressor_ind, conference_id, id_list_str)
    )


def switch_noise_suppressor(request: SwitchNoiseSuppresorToGroupAbonentsRequest) -> SessionGroupOperationResponse:
    """This function switch noise suppressor flag to group of participants"""
    if len(request.connection_id_list) == 0:
        return SessionGroupOperationResponse(request.conference_id, 0, 0)

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    actual_connection_id_list, actual_contact_id_list = get_id_lists(request.connection_id_list)
    processed_count = len(actual_connection_id_list)
    failed_count = len(request.connection_id_list) - processed_count

    logging.error("switch_noise_suppressor() called")

    noise_suppressor_ind = data.get_noise_suppressor_ind(request.noise_suppressor)

    cmd_list = []
    for conn_id in actual_connection_id_list:
        cmd_list.append(data.get_noise_suppressor_abonent_cmd(noise_suppressor_ind, conn_id))

    nettools.exec_cmd_list_s(cmd_list)

    save_noise_suppressor_flags_list(request.conference_id, actual_contact_id_list, noise_suppressor_ind)

    result = SessionGroupOperationResponse(request.conference_id, processed_count, failed_count)
    return result


def save_auto_gain_ctrl_flags_list(conference_id: int, participant_id_list: list, auto_gain_ctrl_ind: int):
    """This funacion save in databese flag 'auto_gain_ctrl_ind'."""
    if len(participant_id_list) == 0:
        return

    id_list_str = tools.id_list_to_db_list(participant_id_list)

    dbtools.make_sql_request(
        "UPDATE legacy.confabonents SET auto_gain_ctrl=%d WHERE confid=%d and abid IN %s" %
        (auto_gain_ctrl_ind, conference_id, id_list_str)
    )


def switch_auto_gain_ctrl(request: SwitchAGCToGroupAbonentsRequest) -> SessionGroupOperationResponse:
    """This function switch auto gain control flag to group of participants"""
    if len(request.connection_id_list) == 0:
        return SessionGroupOperationResponse(request.conference_id, 0, 0)

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    actual_connection_id_list, actual_contact_id_list = get_id_lists(request.connection_id_list)
    processed_count = len(actual_connection_id_list)
    failed_count = len(request.connection_id_list) - processed_count

    logging.error("switch_auto_gain_ctrl() called")

    auto_gain_ctrl_ind = data.get_auto_gain_ctrl_ind(request.auto_gain_ctrl)

    cmd_list = []
    for conn_id in actual_connection_id_list:
        cmd_list.append(data.get_auto_gain_ctrl_abonent_cmd(auto_gain_ctrl_ind, conn_id))

    nettools.exec_cmd_list_s(cmd_list)

    save_auto_gain_ctrl_flags_list(request.conference_id, actual_contact_id_list, auto_gain_ctrl_ind)

    result = SessionGroupOperationResponse(request.conference_id, processed_count, failed_count)
    return result
#!/usr/bin/python3

import errno
import logging
import sys

import psycopg2

# import app.core.constants as constants
import app.core.xmltools as xmltools

# NOTE: don't use this variable from other modules - call function get_last_err() instead
g_last_err = ""

g_connection = None
g_optimize_connection = False


def set_optimize_connection(optimize_conn: bool):
    """Set mode of getting connection to database."""
    global g_optimize_connection
    g_optimize_connection = optimize_conn


def create_connection():
    """For internal use."""

    # OLD: conn = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s' port=%d" % (constants.DB_NAME, constants.DB_USERNAME, constants.DB_HOST, constants.DB_PASSW, constants.DB_PORT), keepalives=1, keepalives_idle=30, keepalives_interval=10, keepalives_count=5)

    cfg = xmltools.get_config()
    dbname = cfg["db"]["dbname"]
    user = cfg["db"]["username"]
    host = cfg["db"]["host"]
    passwd = cfg["db"]["password"]
    port = cfg["db"]["port"]

    conn_str = "dbname='%s' user='%s' host='%s' password='%s' port=%d" % (dbname, user, host, passwd, port)

    # TMP:
    #logging.error("create_connection(): conn_str: " + conn_str)

    conn = psycopg2.connect(conn_str, keepalives=1, keepalives_idle=30, keepalives_interval=10, keepalives_count=5)
    return conn


def get_connection():
    """For internal use."""
    global g_connection
    global g_optimize_connection

    if g_optimize_connection:
        if g_connection == None:
            g_connection = create_connection()
        return g_connection
    else:
        return create_connection()


def close_connection(conn):
    """For internal use."""
    global g_optimize_connection

    if not g_optimize_connection:
        conn.close()


def reconnect():
    global g_connection
    global g_optimize_connection
    global g_last_err

    try:
        if g_optimize_connection and g_connection != None:
            g_connection.close()
    except:
        g_last_err = "reconnect(1): exception: " + str(sys.exc_info())
        logging.error(g_last_err)

    try:
        if g_optimize_connection:
            g_connection = create_connection()
    except:
        g_last_err = "reconnect(2): exception: " + str(sys.exc_info())
        logging.error(g_last_err)


def get_last_error():
    global g_last_err
    return (g_last_err)


def dbg_log(fname, msg):
    """This function writes message 'msg' to file 'fname'"""
    f = open(fname, "wb")
    f.write(msg + "\n")
    f.close()


def load_table(sql_tab_name, fields, sql_cond):
    """Loads list of table rows for table sql_tab_name with condition sql_cond. Returns list of rows."""
    global g_last_err
    rows = []
    if len(fields) == 0:
        return rows  # error
    sql_req = ""
    try:
        # print("try to connect to database server")
        conn = get_connection()
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)

        curs = conn.cursor()

        sql_req = "SELECT " + fields + " FROM " + sql_tab_name
        if sql_cond != "":
            sql_req = sql_req + " WHERE " + sql_cond
        # print('sql request: ' + sql_req)

        curs.execute(sql_req)
        # print("sql requerst executed")

        rows = curs.fetchall()
        # print('rows is getted: ' + str(rows))

        close_connection(conn)
    except IOError as e:
        if e.errno == errno.ENOSPC:
            g_last_err = "Disk is full."
        else:
            g_last_err = e.strerror
        logging.error("load_table() exception: " + g_last_err)
    except:
        g_last_err = "load_table() unknown exception: " + str(sys.exc_info()) + " , sql request: " + sql_req
        logging.error(g_last_err)
    return rows


def select(request, commit_connection=False):
    """Makes 'select' sql request. Returns list of rows.
    commit_connection - use this flag (True) if you want call procedure and change data in database"""
    global g_last_err
    rows = []
    try:
        conn = get_connection()
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)

        curs = conn.cursor()

        curs.execute(request)

        rows = curs.fetchall()

        if commit_connection:
            conn.commit()

        close_connection(conn)
    except IOError as e:
        if e.errno == errno.ENOSPC:
            g_last_err = "Disk is full."
        else:
            g_last_err = e.strerror
        logging.error("select() exception: " + g_last_err + " , request: " + request)
    except:
        g_last_err = "select() unknown exception:" + str(sys.exc_info()) + " , request: " + request
        logging.error(g_last_err)
    return rows


def make_sql_request(sql_request):
    """This function makes SQL-request sql_request (no 'select').
    At success returns 1 else 0."""
    global g_last_err

    try:
        conn = get_connection()
    except:
        g_last_err = "Database connect failed:" + str(sys.exc_info())
        return 0

    try:
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)

        curs = conn.cursor()
        curs.execute(sql_request)
        conn.commit()
        close_connection(conn)
        return 1
    except IOError as e:
        if e.errno == errno.ENOSPC:
            g_last_err = "Disk is full."
        else:
            g_last_err = e.strerror
        logging.error("make_sql_request() exception: " + g_last_err + " , request: " + sql_request)
    except:
        g_last_err = "make_sql_request() unknown exception:" + str(sys.exc_info()) + " , request: " + sql_request
        logging.error(g_last_err)
        close_connection(conn)

    return 0


def insert_with_serial(sql_request):
    """This function makes SQL-request sql_request ('INSERT ... RETURNING id') and return result of request (id of added row).
    At success returns id of added row. At error return 0."""
    global g_last_err

    try:
        conn = get_connection()
    except:
        g_last_err = "Database connect failed:" + str(sys.exc_info())
        return 0  # error

    result = 0  # default return value (no rows fetched)

    try:
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)

        curs = conn.cursor()
        curs.execute(sql_request)

        row = curs.fetchone()
        if len(row) != 0:
            result = row[0]

        conn.commit()
        close_connection(conn)
    except IOError as e:
        if e.errno == errno.ENOSPC:
            g_last_err = "Disk is full."
        else:
            g_last_err = e.strerror
        result = 0  # error
        logging.error("insert_with_serial() exception: " + g_last_err + " , request: " + sql_request)
    except:
        g_last_err = "insert_with_serial() unknown exception:" + str(sys.exc_info()) + " , request: " + sql_request
        logging.error(g_last_err)
        close_connection(conn)
        result = 0  # error

    return result


def get_max_field(tbl_name, fld_name):
    """This function get maximum value of field 'gld_name' of table 'tbl_name'.
    If table is empty, return 0."""
    cnt_rows = load_table(tbl_name, "count(*)", "")
    if len(cnt_rows) == 0:
        return (0)
    cnt = cnt_rows[0][0]
    if cnt == 0:
        return (0)
    max_val = load_table(tbl_name, "max(" + fld_name + ")", "")
    if len(max_val) == 0:
        return 0
    return max_val[0][0]


def generate_unique_fld(tbl_name, fld_name):
    """This function generates unique value of field 'fld_name' of table 'tbl_name'."""
    max_val = get_max_field(tbl_name, fld_name)
    return max_val + 1


def check_connection():
    """This function try to connect to database. If connection success, returns 1, else returns 0 and set variable g_last_err."""
    global g_last_err
    try:
        conn = create_connection()
        conn.close()
    except:
        g_last_err = "Database connection is failed."
        return (0)
    return (1)

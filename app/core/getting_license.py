#!/usr/bin/python3

import base64
import json
import logging
import shutil
import sys
import xml.etree.ElementTree as eltree
from time import sleep

import certifi
import requests
import urllib3

import app.core.cert_functions as cert_func
import app.core.tools as licensetools

"""
Parameters:
    scripts_params_file - file with URL's for requests
    cert_file           - file for save getting certificate
    private_key_file    - file with private key 
    license_file        - file for save license [OUT]
"""


def getting_licence(scripts_params_file, cert_file, private_key_file, license_file, ca_cert_fname):
    logging.debug("getting_licence(): try to get URL from scripts params file %s" % (scripts_params_file))

    # load data for this function
    try:
        tree = eltree.parse(scripts_params_file)
        root = tree.getroot()

        # url for POST-request
        url_for_req = root.find("url_get_licence").text
    except IOError:
        cert_func.log_msg("getting_licence() IOError: " + str(sys.exc_info()))
        return cert_func.build_error_dict(cert_func.NO_SCRIPT_PARAMS)
    except Exception:
        cert_func.log_msg("getting_licence() Exception: " + str(sys.exc_info()))
        return cert_func.build_error_dict(cert_func.ERROR_SCRIPT_PARAMS)

    logging.debug("getting_licence(): create tmp CA certificate file")

    if not licensetools.save_text_to_file(ca_cert_fname, cert_func.CA_CRT):
        cert_func.log_msg("getting_licence(): cannot save CA certificate to file")
        return cert_func.build_error_dict(cert_func.ERROR_CREATE_CA_CERT_FILE)

    logging.debug("getting_licence(): make request to URL=%s to get license" % (url_for_req))

    # GET-request to server
    try:
        http = urllib3.PoolManager(cert_file=cert_file,
                                   cert_reqs='CERT_REQUIRED',
                                   key_file=private_key_file,
                                   key_password=b"APM1JDVgT8WDGOWBgQv6EIhvxl4vDYvU",
                                   ca_certs=ca_cert_fname)
                                   #OLD:ca_certs=certifi.where())
        r = http.request('GET', url_for_req)

        if r.status != 200:
            cert_func.log_msg("getting_licence() bad httd ansver code: " + str(r.status))
            err_txt = r.data.decode("utf-8")
            cert_func.log_msg("getting_licence() ansver: " + str(err_txt))
            licensetools.delete_file(ca_cert_fname)  # delete tmp file
            return cert_func.build_error_dict_ex(cert_func.GET_LICENSE_HTTP_ERROR, err_txt)

    except Exception:
        cert_func.log_msg("getting_licence() exception at http get request: " + str(sys.exc_info()))
        licensetools.delete_file(ca_cert_fname)  # delete tmp file
        return cert_func.build_error_dict(cert_func.ERROR_CONNECTION)

    licensetools.delete_file(ca_cert_fname) # delete tmp file

    logging.debug("getting_licence(): save licenses to files")

    licenses_count = 0
    try:
        content = json.loads(r.data.decode("utf-8"))

        if len(content['data']) == 0:
            cert_func.log_msg("getting_licence() error: list of licenses is empty")
            return cert_func.build_error_dict(cert_func.LICENSES_LIST_EMPTY)

        ll = []
        for license in content['data']:

            license = license['content'].replace('\n', '').replace('\r', '').replace('\t', '').replace(' ', '').strip()
            if ll.count(license) == 0:
                ll.append(license)

                if (len(str(license)) % 4 == 0) and (len(str(license)) > 0):
                    license_file = licensetools.get_license_unique_fname2()
                    with open(license_file, "wb") as f:
                        f.write(base64.b64decode(license))
                        logging.debug("getting_licence(): SAVE LICENSE TO " + license_file)
                    sleep(0.001)
                    licenses_count += 1

                    # TMP: TO DEBUG
                    #shutil.copy(license_file, license_file + ".bak")
    except Exception:
        cert_func.log_msg("getting_licence() error get license: " + str(sys.exc_info()))
        return cert_func.build_error_dict(cert_func.ERROR_SAVE_LICENSE)

    if licenses_count == 0:
        cert_func.log_msg("getting_licence() error: No licenses saved")
        return cert_func.build_error_dict(cert_func.ERROR_SAVE_LICENSE)

    return cert_func.build_error_dict(cert_func.SUCCESS)


def send_event_to_crm(cert_file: str, private_key_file: str, json_data: dict, ca_cert_fname) -> dict:
    """Send event to license server."""

    logging.debug("send_event_to_crm(): create tmp CA certificate file")

    if not licensetools.save_text_to_file(ca_cert_fname, cert_func.CA_CRT):
        cert_func.log_msg("send_event_to_crm(): cannot save CA certificate to file")
        return cert_func.build_error_dict(cert_func.ERROR_CREATE_CA_CERT_FILE)

    logging.debug("send_event_to_crm(): start send event")

    url = "https://reg.trueconf.com/mcu/v1/events"
    encoded_data = json.dumps(json_data).encode('utf-8')

    # POST-request to server
    try:
        http = urllib3.PoolManager(cert_file=cert_file,
                                   cert_reqs='CERT_REQUIRED',
                                   key_file=private_key_file,
                                   key_password=b"APM1JDVgT8WDGOWBgQv6EIhvxl4vDYvU",
                                   ca_certs=ca_cert_fname)
                                   #OLD:ca_certs=certifi.where())

        r = http.request('POST', url, body=encoded_data, headers={'Content-Type': 'application/json'})
        #OLD:r = http.request('POST', url, fields={"body": encoded_data})

        if r.status != 200:
            cert_func.log_msg("send_event_to_crm() bad httd ansver code: " + str(r.status))
            err_txt = r.data.decode("utf-8")
            cert_func.log_msg("send_event_to_crm() ansver: " + str(err_txt))
            licensetools.delete_file(ca_cert_fname)  # delete tmp file
            return cert_func.build_error_dict_ex(cert_func.SEND_EVENT_HTTP_ERROR, err_txt)

    except Exception:
        cert_func.log_msg("send_event_to_crm() exception at http get request: " + str(sys.exc_info()))
        licensetools.delete_file(ca_cert_fname)  # delete tmp file
        return cert_func.build_error_dict(cert_func.ERROR_CONNECTION)

    licensetools.delete_file(ca_cert_fname)  # delete tmp file
    
    logging.debug("send_event_to_crm(): event is sent successfully")

    return cert_func.build_error_dict(cert_func.SUCCESS)


#!/usr/bin/python3


import csv
import errno

import app.core.addrbook as addrbook
import app.core.files as files
import app.core.tools as tools
from .errors import CreateExportFileError
from .errors import ExportAddressBookError
from .errors import FileIOError
from .errors import NoFreeDiskSpaceError
from .requests import GetContactListRequest
from .responses import HandlingFileResponse


def get_csv_export_file_name():
    """This function returns unique file name for export of address book to csv-file."""
    time_stamp_str = tools.get_unique_time_stamp_str()
    directory = files.get_directory("addrbookcsv")
    fname = directory + files.SLASH + time_stamp_str + ".csv"
    return (fname)


def get_abook_export_file_name():
    """This function returns unique file name for export of address book to csv-file."""
    time_stamp_str = tools.get_unique_time_stamp_str()
    directory = files.get_directory("addrbookxml")
    fname = directory + files.SLASH + time_stamp_str + ".xml"
    return (fname)


def pack_list_to_str(gr_list: list):
    """gr_list - list of { "id": <id>, "name": <name>}"""
    buffer_str = ""
    for i in range(len(gr_list)):
        elem = gr_list[i]["name"].replace('"', '')
        elem = elem.replace("'", "")
        buffer_str += '"' + elem + '"'
        if i != len(gr_list) - 1:
            buffer_str += " "
    return (buffer_str)


def export_addrbook_to_csv(filename: str) -> HandlingFileResponse:
    """This function exports address book to csv-file with name filename."""
    try:
        csvfile = open(filename, "w", encoding='utf-8')
        if csvfile == None:
            raise CreateExportFileError(filename)

        wrtr = csv.writer(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        request = GetContactListRequest(0, 1000000000, "name", "asc", "", "", "", "", "", "", None)
        response = addrbook.get_contacts_list(request)
        contacts_list = response.contacts

        # export contacts and its groups (7 columns in row)
        for contact in contacts_list:
            if contact.autoadded:  # skip auto added (hidden) contacts
                continue
            if len(contact.groups) == 1 and contact.groups[0]["id"] == addrbook.NO_GROUP_ID:
                gr_list_str = ""
            else:
                gr_list_str = pack_list_to_str(contact.groups)
            wrtr.writerow([contact.name, tools.unquote_address(contact.sip), tools.unquote_address(contact.h323),
                           contact.email, str(contact.bitrate), tools.unquote_address(contact.rtsp), gr_list_str])

        # export groups (1 column in row)
        all_groups_dict = addrbook.get_all_groups_dict()
        for group_name, group_id in all_groups_dict.items():
            wrtr.writerow([group_name])

        csvfile.flush()
        csvfile.close()
    except IOError as e:
        if e.errno == errno.ENOSPC:
            raise NoFreeDiskSpaceError()
        else:
            raise FileIOError(e.errno)
    except:
        raise ExportAddressBookError()

    short_fname = tools.get_short_file_name(filename)
    return HandlingFileResponse(short_fname)


def export_addrbook_to_xml(filename: str) -> HandlingFileResponse:
    """This function exports address book to xml-file with name filename (format compatible vith UZVKS-terminal)."""
    try:
        abookfile = open(filename, "wb")
        if abookfile == None:
            raise CreateExportFileError(filename)

        request = GetContactListRequest(0, 1000000000, "name", "asc", "", "", "", "", "", "", None)
        response = addrbook.get_contacts_list(request)
        contacts_list = response.contacts

        head = """<?xml version="1.0" encoding="UTF-8"?>\n<abook version="0">\n  <addresses>\n"""
        abookfile.write(head.encode("UTF-8"))

        for contact in contacts_list:
            if contact.autoadded:  # skip auto added (hidden) contacts
                continue
            if contact.sip == "" and contact.h323 == "":
                continue  # skip RTSP cameras
            line = """    <address name="%s" """ % (contact.name)
            if contact.email != "":
                line += """ e-mail="%s" """ % (contact.email)
            if contact.sip != "":
                line += """ sip-uri="%s" """ % (tools.unquote_address(contact.sip))
            if contact.h323 != "":
                line += """ h323-uri="%s" """ % (tools.unquote_address(contact.h323))
            if contact.bitrate != 0:
                line += """ speed="%d" """ % (contact.bitrate)
            line += "/>\n"
            abookfile.write(line.encode("UTF-8"))

        tail = """  </addresses>\n</abook>\n"""
        abookfile.write(tail.encode("UTF-8"))

        abookfile.flush()
        abookfile.close()
    except IOError as e:
        if e.errno == errno.ENOSPC:
            raise NoFreeDiskSpaceError()
        else:
            raise FileIOError(e.errno)
    except:
        raise ExportAddressBookError()

    short_fname = tools.get_short_file_name(filename)
    return HandlingFileResponse(short_fname)

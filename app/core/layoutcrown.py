#!/usr/bin/python3

import app.core.addrbook as addrbook
import app.core.constants as constants
import app.core.customlayouttypes as customlayouttypes
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.layoutbackup as layoutbackup
import app.core.layoutcfgtools as layoutcfgtools
import app.core.nettools as nettools
import app.core.onlinelayouttools as onlinelayouttools
import app.core.onlinetools as onlinetools
import app.core.sessionpartctrl as sessionpartctrl
import app.core.sysinfotools as sysinfotools
import app.core.tools as tools
from app.core.requests import DeleteCrownRequest
from app.core.requests import SetCrownRequest
from app.core.responses import DeleteCrownResponse
from app.core.responses import SetCrownResponse
from .errors import ActiveConferenceNotFoundError
from .errors import OnlineLayoutNotFoundError
from .errors import SessionAbonentNotFoundError

SCHEMA_TYPE_RIGHT = 1

MAX_CELLS_COUNT = 8


def switch_active_speaker_layout(confid: int, turn_on: bool, cmd_list: list):
    """this function switch layout special mode for active speaker layout (if it is exist)"""
    asp_lyt_id = layoutbackup.get_layout_id_by_type(confid, data.LAYOUT_TYPE_ASPEAKER, True)
    if asp_lyt_id == 0:
        asp_lyt_id = layoutbackup.get_layout_id_by_type(confid, data.LAYOUT_TYPE_ASPEAKER, False)
    if asp_lyt_id != 0:
        cid = tools.conference_name_by_id(confid)
        lid = tools.get_layout_name_by_id(confid, asp_lyt_id)
        layoutcfgtools.set_special_layout(cid, lid, turn_on, cmd_list)


def delete_crown_from_participant(request: DeleteCrownRequest, conf_json: dict) -> DeleteCrownResponse:
    if conf_json == {}:
        connstr, confstr = onlinetools.get_online_state()

        conf_json = onlinetools.extract_conference(confstr, request.confid)
        if conf_json == {}:
            raise ActiveConferenceNotFoundError(request.confid, "conference_id")

    cid = tools.conference_name_by_id(request.confid)

    # aburi = addrbook.get_realaddr_by_ab_id(abid)
    # if aburi == "":
    #    raise SessionAbonentNotFoundError(aburi, "participant_id")

    # extract crown layout (if exist)
    crown_layout_name = tools.confid_to_crown_layout_name(request.confid)
    crown_layout_json = onlinetools.extract_layout_by_name(conf_json, crown_layout_name)
    if crown_layout_json == {}:
        raise OnlineLayoutNotFoundError(crown_layout_name, "layout_name")

    # set for all participants it's planned/online layouts
    cmd_list = []
    for ab in crown_layout_json["tx"]["connections"]:
        aburi = ab["uri"]
        conn_id = ab["id"]
        cur_abid = addrbook.get_ab_id_by_conn_id(conn_id)
        if cur_abid == 0:
            continue  # error
        if tools.is_rtsp_prefix(aburi):  # don't show any layout to RTSP camera
            continue
        cur_layout_id = layoutbackup.get_layout_id_by_owner(request.confid, cur_abid)
        cur_lid = tools.get_layout_name_by_id(request.confid, cur_layout_id)
        cmd_list.append("LAYOUT SHOW %s %s #%d" % (cid, cur_lid, conn_id))

    # delete crown layout
    cmd_list.append("LAYOUT UNREGISTER " + cid + " " + crown_layout_name)

    # restore special layout for active speaker
    switch_active_speaker_layout(request.confid, True, cmd_list)

    nettools.exec_cmd_list_s(cmd_list)

    return DeleteCrownResponse(request.confid)


def set_crown_to_participant(request: SetCrownRequest) -> SetCrownResponse:
    connstr, confstr = onlinetools.get_online_state()

    conf_json = onlinetools.extract_conference(confstr, request.confid)
    if conf_json == {}:
        raise ActiveConferenceNotFoundError(request.confid, "conference_id")

    cid = tools.conference_name_by_id(request.confid)

    # check: is crown abonent in some session?
    if request.use_conn_id:
        crown_conn_id = request.contactid
        crown_aburi = onlinetools.get_realaddr_by_conn_id(crown_conn_id, [])
        contact_id = addrbook.get_ab_id_by_conn_id(crown_conn_id)
    else:
        crown_aburi = addrbook.get_realaddr_by_ab_id(request.contactid)
        crown_conn_id = onlinetools.get_conn_id_by_realaddr(crown_aburi, [])
        contact_id = request.contactid
    if crown_aburi == "":
        raise SessionAbonentNotFoundError(crown_aburi, "participant_id")

    conf_ab_list = onlinetools.extract_abonents_list(conf_json, 0, connstr)
    conf_ab_count = len(conf_ab_list)

    # check: is crown abonent in this session?
    found = False
    for abonent in conf_ab_list:
        if abonent["id"] == crown_conn_id:
            found = True
            break
    if not found:
        raise SessionAbonentNotFoundError(crown_aburi, "participant_id")

    crown_conn_id_str = "#%d" % (crown_conn_id)

    cmd_list = []

    # check existing crown layout
    crown_layout_name = tools.confid_to_crown_layout_name(request.confid)
    crown_layout_json = onlinetools.extract_layout_by_name(conf_json, crown_layout_name)
    if crown_layout_json != {}:
        # OLDEST:delete_crown_from_participant( DeleteCrownRequest(request.confid), conf_json ) # if cronw layout exist for some participant, then delete it
        # OLD:cmd_list.append("LAYOUT MAP INIT " + cid + " " + crown_layout_name)
        # change one crown abonent to other abonent
        cmd_list2 = []
        rolecmd = layoutcfgtools.role_cmd(layoutcfgtools.DEFAULT_FILLED_ROLE_ID, cid, crown_layout_name, 1,
                                          crown_conn_id_str)
        cmd_list2.append(rolecmd)
        cmd_list2.append("LAYOUT MAP APPLY " + cid + " " + crown_layout_name)
        cmd_list2.append("SET MUTE OFF RX AUDIO " + crown_conn_id_str)  # unmute crown abonent because he is main abon.
        sessionpartctrl.save_useaudio_rx_flag(True, request.confid, contact_id)
        nettools.exec_cmd_list_s(cmd_list2)
        return SetCrownResponse(request.confid, request.contactid)

    # --- check max active abonents restriction ---
    onlinelayouttools.validate_participants_added_to_layout2(request.confid, confstr, conf_ab_list, connstr)
    # ---------------------------------------------

    # turn off special layout for active speaker
    switch_active_speaker_layout(request.confid, False, cmd_list)

    # register crown layout
    layoutcfgtools.register_online_layout(request.confid, crown_layout_name, False, cmd_list)

    # set layout windows count limit
    max_visible = sysinfotools.get_abonents_restriction()["max_visible"]
    max_cells_count = min(MAX_CELLS_COUNT, max_visible)
    cells_count = min(max_cells_count, conf_ab_count)
    layoutcfgtools.set_layout_restrict(cid, crown_layout_name, cells_count, cmd_list)

    #custom_lyt_types = customlayouttypes.get_all_custom_layout_types()
    layout_mode_cmd = layoutcfgtools.get_layout_cmd(SCHEMA_TYPE_RIGHT, cid, crown_layout_name)

    # add "king" to crown layout
    cmd_list.append("LAYOUT ATTACH " + cid + " " + crown_layout_name + " " + crown_conn_id_str)
    cmd_list.append(
        layoutcfgtools.role_cmd(layoutcfgtools.DEFAULT_FILLED_ROLE_ID, cid, crown_layout_name, 1, crown_conn_id_str))

    cmd_list.append("SET MUTE OFF VIDEO " + crown_conn_id_str)
    sessionpartctrl.save_usevideo_flag(True, request.confid, contact_id)

    cmd_list.append("SET MUTE OFF RX AUDIO " + crown_conn_id_str) # unmute crown abonent because he is main abonent
    sessionpartctrl.save_useaudio_rx_flag(True, request.confid, contact_id)

    # add all connected abonents to crown layout (exclude "king")
    position = 2
    for ab in conf_ab_list:
        cur_conn_id = ab["id"]
        cur_conn_id_str = "#%d" % (cur_conn_id)
        if cur_conn_id == crown_conn_id:
            continue
        cmd_list.append("LAYOUT ATTACH " + cid + " " + crown_layout_name + " " + cur_conn_id_str)
        if position <= cells_count:
            cmd_list.append(layoutcfgtools.role_cmd(layoutcfgtools.ROLL_ROLE_ID, cid, crown_layout_name, position, ""))
        cmd_list.append("SET MUTE OFF VIDEO " + cur_conn_id_str)
        position += 1

        # save in database flag 'usevideo'
        cur_abid = addrbook.get_ab_id_by_conn_id(cur_conn_id)
        sessionpartctrl.save_usevideo_flag(True, request.confid, cur_abid)

    cmd_list.append(layout_mode_cmd)
    cmd_list.append("LAYOUT MAP APPLY " + cid + " " + crown_layout_name)

    # show crown layout to all connected abonents
    for ab in conf_ab_list:
        if tools.is_rtsp_prefix(ab["uri"]):  # don't show any layout to RTSP camera
            continue
        cur_conn_id = ab["id"]
        cur_conn_id_str = "#%d" % (cur_conn_id)
        cmd_list.append("LAYOUT SHOW %s %s %s" % (cid, crown_layout_name, cur_conn_id_str))

    nettools.exec_cmd_list_s(cmd_list)

    return SetCrownResponse(request.confid, request.contactid)


def update_crown_layout(confid: int, crown_layout_json: dict, aburi: str, conn_id: int, badd: bool) -> bool:
    """This function update 'crown layout' if abonent 'aburi' add/remove to/from conference 'confid'.
    If crown layout updated, then return True."""

    if crown_layout_json == {}:
        return False

    crown_layout_name = crown_layout_json["name"]

    cid = tools.conference_name_by_id(confid)

    conn_id_str = "#%d" % (conn_id)

    king_uri = ""
    king_conn_id = -1
    king_conn_id_str = ""
    if "map" in crown_layout_json:
        if len(crown_layout_json["map"]) != 0:
            first_slot = crown_layout_json["map"][0]
            if "uri" in first_slot:
                king_uri = first_slot["uri"]
            if "call_id" in first_slot:
                king_conn_id = first_slot["call_id"]
                king_conn_id_str = "#%d" % (king_conn_id)

    # detach abonents from crown layout
    cmd_list = []
    for ab in crown_layout_json["rx"]["connections"]:
        current_connection_id_str = "#%d" % (ab["id"])
        cmd_list.append("LAYOUT DETACH " + cid + " " + crown_layout_name + " " + current_connection_id_str)

    max_visible = sysinfotools.get_abonents_restriction()["max_visible"]

    #custom_lyt_types = customlayouttypes.get_all_custom_layout_types()

    if badd:
        # set new count of slots
        ab_list = []
        for ab in crown_layout_json["rx"]["connections"]:
            ab_list.append({"uri": ab["uri"], "id": ab["id"]})

        new_ab_count = len(ab_list) + 1
        max_cells_count = min(MAX_CELLS_COUNT, max_visible)
        new_cells_count = min(max_cells_count, new_ab_count)

        # if too many abonents, don't change map of layout
        if new_ab_count > MAX_CELLS_COUNT:
            cmd_list2 = ["LAYOUT ATTACH " + cid + " " + crown_layout_name + " " + conn_id_str]
            if not tools.is_rtsp_prefix(aburi):
                cmd_list2.append("LAYOUT SHOW %s %s %s" % (cid, crown_layout_name, conn_id_str))
            nettools.exec_cmd_list_s(cmd_list2)
            return True

        layoutcfgtools.set_layout_restrict(cid, crown_layout_name, new_cells_count, cmd_list)

        # set layout mode
        layout_mode_cmd = layoutcfgtools.get_layout_cmd(SCHEMA_TYPE_RIGHT, cid, crown_layout_name)
        cmd_list.append(layout_mode_cmd)

        # add "king" to crown layout
        cmd_list.append("LAYOUT ATTACH " + cid + " " + crown_layout_name + " " + king_conn_id_str)
        cmd_list.append(
            layoutcfgtools.role_cmd(layoutcfgtools.DEFAULT_FILLED_ROLE_ID, cid, crown_layout_name, 1, king_conn_id_str))

        # attach old abonents to crown layout (exclude "king")
        position = 2
        for abonent in ab_list:
            if abonent["id"] == king_conn_id:
                continue
            cur_conn_id_str = "#%d" % (abonent["id"])
            cmd_list.append("LAYOUT ATTACH " + cid + " " + crown_layout_name + " " + cur_conn_id_str)
            if position <= new_cells_count:
                cmd_list.append(
                    layoutcfgtools.role_cmd(layoutcfgtools.ROLL_ROLE_ID, cid, crown_layout_name, position, ""))
            position += 1

        # attach new added abonent to crown layout
        new_ab_pos = new_ab_count
        cmd_list.append("LAYOUT ATTACH " + cid + " " + crown_layout_name + " " + conn_id_str)
        if position <= new_cells_count:
            cmd_list.append(
                layoutcfgtools.role_cmd(layoutcfgtools.ROLL_ROLE_ID, cid, crown_layout_name, new_ab_pos,
                                        conn_id_str))

        cmd_list.append("LAYOUT MAP APPLY " + cid + " " + crown_layout_name)

        # show crown layout to all its abonents
        for abonent in ab_list:
            if tools.is_rtsp_prefix(abonent["uri"]):  # don't show any layout to RTSP camera
                continue
            cur_conn_id_str = "#%d" % (abonent["id"])
            cmd_list.append("LAYOUT SHOW %s %s %s" % (cid, crown_layout_name, cur_conn_id_str))
        if not tools.is_rtsp_prefix(aburi):
            cmd_list.append("LAYOUT SHOW %s %s %s" % (cid, crown_layout_name, conn_id_str))
    else:
        if king_conn_id_str == "" or king_conn_id == conn_id:
            return False  # "delete king" - delete crown layout in this case
        else:
            # set new count of slots
            new_ab_count = 0
            ab_list = []
            for ab in crown_layout_json["rx"]["connections"]:
                if ab["id"] == conn_id:
                    continue
                ab_list.append({"uri": ab["uri"], "id": ab["id"]})
                new_ab_count += 1

            # new_ab_count = len(ab_list)
            max_cells_count = min(MAX_CELLS_COUNT, max_visible)
            new_cells_count = min(max_cells_count, new_ab_count)

            # if too many abonents, don't change map of layout
            if new_ab_count > MAX_CELLS_COUNT:
                cmd_list2 = ["LAYOUT DETACH " + cid + " " + crown_layout_name + " " + conn_id_str]
                nettools.exec_cmd_list_s(cmd_list2)
                return True

            layoutcfgtools.set_layout_restrict(cid, crown_layout_name, new_cells_count, cmd_list)

            # set layout mode
            layout_mode_cmd = layoutcfgtools.get_layout_cmd(SCHEMA_TYPE_RIGHT, cid, crown_layout_name)
            cmd_list.append(layout_mode_cmd)

            # attach "king" to crown layout
            cmd_list.append("LAYOUT ATTACH " + cid + " " + crown_layout_name + " " + king_conn_id_str)
            cmd_list.append(layoutcfgtools.role_cmd(layoutcfgtools.DEFAULT_FILLED_ROLE_ID, cid, crown_layout_name, 1,
                                                    king_conn_id_str))

            # attach to crown layout other abonents (exclude king)
            position = 2
            for abonent in ab_list:
                if abonent["id"] == king_conn_id:
                    continue
                cur_conn_id_str = "#%d" % (abonent["id"])
                cmd_list.append("LAYOUT ATTACH " + cid + " " + crown_layout_name + " " + cur_conn_id_str)
                if position <= new_cells_count:
                    cmd_list.append(
                        layoutcfgtools.role_cmd(layoutcfgtools.ROLL_ROLE_ID, cid, crown_layout_name, position, ""))
                position += 1

            cmd_list.append("LAYOUT MAP APPLY " + cid + " " + crown_layout_name)

            # show crown layout to all its abonents
            for abonent in ab_list:
                if tools.is_rtsp_prefix(abonent["uri"]):  # don't show any layout to RTSP camera
                    continue
                cur_conn_id_str = "#%d" % (abonent["id"])
                cmd_list.append("LAYOUT SHOW %s %s %s" % (cid, crown_layout_name, cur_conn_id_str))

    nettools.exec_cmd_list_s(cmd_list)

    return True

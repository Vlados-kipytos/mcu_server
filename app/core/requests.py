from app.core.entities import *


class GetContactListRequest:

    def __init__(self, page: int = 0, per_page: int = 100, sort_by: str = "id", order_by: str = "asc", name: str = "",
                 email: str = "", display_name: str = "", sip: str = "", h323: str = "", rtsp: str = "",
                 autoadded: bool = False):
        self.__set_page(page)
        self.__set_per_page(per_page)
        self.__set_sort_by(sort_by)
        self.__set_order_by(order_by)
        self.__set_name(name)
        self.__set_email(email)
        self.__set_display_name(display_name)
        self.__set_sip(sip)
        self.__set_h323(h323)
        self.__set_rtsp(rtsp)
        self.__set_autoadded(autoadded)

    def __get_page(self) -> int:
        return self._page

    def __set_page(self, value):
        if value != None:
            self._page = value
        else:
            self._page = 0

    page = property(__get_page, __set_page)

    def __get_per_page(self) -> int:
        return self._per_page

    def __set_per_page(self, value):
        if value != None:
            self._per_page = value
        else:
            self._per_page = 60

    per_page = property(__get_per_page, __set_per_page)

    def __get_sort_by(self) -> str:
        return self._sort_by

    def __set_sort_by(self, value):
        if value != None:
            self._sort_by = value
        else:
            self._sort_by = "id"

    sort_by = property(__get_sort_by, __set_sort_by)

    def __get_order_by(self) -> str:
        return self._order_by

    def __set_order_by(self, value):
        if value != None:
            self._order_by = value
        else:
            self._order_by = "asc"

    order_by = property(__get_order_by, __set_order_by)

    def __get_name(self) -> str:
        return self._name

    def __set_name(self, value):
        if value != None:
            self._name = value
        else:
            self._name = ""

    name = property(__get_name, __set_name)

    def __get_email(self) -> str:
        return self._email

    def __set_email(self, value):
        if value != None:
            self._email = value
        else:
            self._email = ""

    email = property(__get_email, __set_email)

    def __get_display_name(self) -> str:
        return self._display_name

    def __set_display_name(self, value):
        if value != None:
            self._display_name = value
        else:
            self._display_name = ""

    display_name = property(__get_display_name, __set_display_name)

    def __get_sip(self) -> str:
        return self._sip

    def __set_sip(self, value):
        if value != None:
            self._sip = value
        else:
            self._sip = ""

    sip = property(__get_sip, __set_sip)

    def __get_h323(self) -> str:
        return self._h323

    def __set_h323(self, value):
        if value != None:
            self._h323 = value
        else:
            self._h323 = ""

    h323 = property(__get_h323, __set_h323)

    def __get_rtsp(self) -> str:
        return self._rtsp

    def __set_rtsp(self, value):
        if value != None:
            self._rtsp = value
        else:
            self._rtsp = ""

    rtsp = property(__get_rtsp, __set_rtsp)

    def __get_autoadded(self) -> bool:
        return self._autoadded

    def __set_autoadded(self, value):
        if value != None:
            self._autoadded = value
        else:
            self._autoadded = None

    autoadded = property(__get_autoadded, __set_autoadded)


class GetContactRequest:

    def __init__(self, id: int):
        self._id = id

    @property
    def id(self) -> int:
        return self._id


class CreateContactRequest:

    def __init__(self, name: str, display_name: str, sip: str, h323: str, rtsp: str, email: str, bitrate: int,
                 realaddr: str, autoadded: bool, conn_id: int, audio_codecs: AudioCodecsEntity,
                 video_codecs: VideoCodecsEntity, file: str, loop: bool):
        self.__set_name(name)
        self.__set_display_name(display_name)
        self.__set_sip(sip)
        self.__set_h323(h323)
        self.__set_rtsp(rtsp)
        self.__set_email(email)
        self.__set_bitrate(bitrate)
        self.__set_realaddr(realaddr)
        self.__set_autoadded(autoadded)
        self.__set_conn_id(conn_id)
        self.__set_audio_codecs(audio_codecs)
        self.__set_video_codecs(video_codecs)
        self.__set_file_uri(file)
        self.__set_loop(loop)

    def __get_name(self) -> str:
        return self._name

    def __set_name(self, value):
        if value != None:
            self._name = value
        else:
            self._name = ""

    name = property(__get_name, __set_name)

    def __get_display_name(self) -> str:
        return self._display_name

    def __set_display_name(self, value):
        if value != None:
            self._display_name = value
        else:
            self._display_name = ""

    display_name = property(__get_display_name, __set_display_name)

    def __get_sip(self) -> str:
        return self._sip

    def __set_sip(self, value):
        if value != None:
            self._sip = value
        else:
            self._sip = ""

    sip = property(__get_sip, __set_sip)

    def __get_h323(self) -> str:
        return self._h323

    def __set_h323(self, value):
        if value != None:
            self._h323 = value
        else:
            self._h323 = ""

    h323 = property(__get_h323, __set_h323)

    def __get_rtsp(self) -> str:
        return self._rtsp

    def __set_rtsp(self, value):
        if value != None:
            self._rtsp = value
        else:
            self._rtsp = ""

    rtsp = property(__get_rtsp, __set_rtsp)

    def __get_email(self) -> str:
        return self._email

    def __set_email(self, value):
        if value != None:
            self._email = value
        else:
            self._email = ""

    email = property(__get_email, __set_email)

    def __get_bitrate(self) -> int:
        return self._bitrate

    def __set_bitrate(self, value):
        if value != None:
            self._bitrate = value
        else:
            self._bitrate = 0

    bitrate = property(__get_bitrate, __set_bitrate)

    def __get_realaddr(self) -> str:
        return self._realaddr

    def __set_realaddr(self, value):
        if value != None:
            self._realaddr = value
        else:
            self._realaddr = ""

    realaddr = property(__get_realaddr, __set_realaddr)

    def __get_autoadded(self) -> bool:
        return self._autoadded

    def __set_autoadded(self, value):
        if value != None:
            self._autoadded = value
        else:
            self._autoadded = False

    autoadded = property(__get_autoadded, __set_autoadded)

    def __get_conn_id(self) -> int:
        return self._conn_id

    def __set_conn_id(self, value):
        if value != None:
            self._conn_id = value
        else:
            self._conn_id = -1

    conn_id = property(__get_conn_id, __set_conn_id)

    def __get_audio_codecs(self) -> AudioCodecsEntity:
        return self._audio_codecs

    def __set_audio_codecs(self, value):
        if value != None:
            self._audio_codecs = value
        else:
            self._audio_codecs = AudioCodecsEntity(True)

    audio_codecs = property(__get_audio_codecs, __set_audio_codecs)

    def __get_video_codecs(self) -> VideoCodecsEntity:
        return self._video_codecs

    def __set_video_codecs(self, value):
        if value != None:
            self._video_codecs = value
        else:
            self._video_codecs = VideoCodecsEntity(True)

    video_codecs = property(__get_video_codecs, __set_video_codecs)

    def __get_file_uri(self) -> str:
        return self._file

    def __set_file_uri(self, value):
        if value != None:
            self._file = value
        else:
            self._file = ""

    file_uri = property(__get_file_uri, __set_file_uri)

    def __get_loop(self) -> bool:
        return self._loop

    def __set_loop(self, value):
        if value != None:
            self._loop = value
        else:
            self._loop = False

    loop = property(__get_loop, __set_loop)


class ChangeContactRequest:
    def __init__(self, contact: ContactEntity):
        self._contact = contact

    @property
    def contact(self) -> ContactEntity:
        return self._contact


class DeleteContactRequest:
    def __init__(self, contactid: int):
        self._contactid = contactid

    @property
    def contactid(self) -> int:
        return self._contactid


class GetConferenceRequest:

    def __init__(self, confid: int, confstr: str = ""):
        self._confid = confid
        self._confstr = confstr

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def confstr(self) -> str:
        return self._confstr


class GetConferencesListRequest:

    def __init__(self, page: int = 0, per_page: int = 60, sort_by: str = "created_at", order_by: str = "asc",
                 name: str = "", state: str = "", recording: bool = False):
        self._page = page
        self._per_page = per_page
        self._sort_by = sort_by
        self._order_by = order_by
        self._name = name
        self._state = state
        self._recording = recording

    @property
    def page(self) -> int:
        return self._page

    @property
    def per_page(self) -> int:
        return self._per_page

    @property
    def sort_by(self) -> str:
        return self._sort_by

    @property
    def order_by(self) -> str:
        return self._order_by

    @property
    def name(self) -> str:
        return self._name

    @property
    def state(self) -> str:
        return self._state

    @property
    def recording(self) -> bool:
        return self._recording

    def get_db_sort_field(self):
        if self._sort_by == "id":
            return "id"
        elif self._sort_by == "name":
            return "name"
        elif self._sort_by == "start_time":
            return "starttime"
        elif self._sort_by == "end_time":
            return "endtime"
        elif self._sort_by == "created_at":
            return "id"
        else:
            return "id"


class CreateConferenceRequest:

    def __init__(self, name: str, usepin: bool, pin: str, infinity: bool,
                 auto_start: AutoStartConferenceModeEntity,
                 repeate: RepeateConferenceModeEntity,
                 stop_time_table: StopTimeTableEntity,
                 auto_stop: AutoStopConferenceEntity,
                 max_participants: int,
                 autocall: bool, rtmp: str, record_participant: int, audio: dict, video: dict):
        self.__set_name(name)
        self.__set_usepin(usepin)
        self.__set_pin(pin)
        self.__set_infinity(infinity)
        self.__set_auto_start(auto_start)
        self.__set_repeate(repeate)
        self.__set_stop_time_table(stop_time_table)
        self.__set_auto_stop(auto_stop)
        self.__set_max_participants(max_participants)
        self.__set_autocall(autocall)
        self.__set_rtmp(rtmp)
        self.__set_record_participant(record_participant)
        self.__set_audio(audio)
        self.__set_video(video)

    def __get_name(self) -> str:
        return self._name

    def __set_name(self, value):
        self._name = value

    name = property(__get_name, __set_name)

    def __get_usepin(self) -> bool:
        return self._usepin

    def __set_usepin(self, value):
        if value != None:
            self._usepin = value
        else:
            self._usepin = True

    usepin = property(__get_usepin, __set_usepin)

    def __get_pin(self) -> str:
        return self._pin

    def __set_pin(self, value):
        if value != None:
            self._pin = value.strip()
        else:
            self._pin = ""

    pin = property(__get_pin, __set_pin)

    def __get_infinity(self) -> bool:
        return self._infinity

    def __set_infinity(self, value):
        if value != None:
            self._infinity = value
        else:
            self._infinity = True

    infinity = property(__get_infinity, __set_infinity)

    def __get_auto_start(self) -> AutoStartConferenceModeEntity:
        return self._auto_start

    def __set_auto_start(self, value: AutoStartConferenceModeEntity):
        if value != None:
            self._auto_start = value
        if self._auto_start.time_range_list == None:
            self._auto_start.time_range_list = []
        if self._auto_start.auto_start_mode == None:
            self._auto_start.auto_start_mode = "off"
        if self._auto_start.auto_start_week_day == None:
            self._auto_start.auto_start_week_day = 0
        if self._auto_start.auto_start_month_day == None:
            self._auto_start.auto_start_month_day = 0
        if self._auto_start.first_week_day == None:
            self._auto_start.first_week_day = 0
        if self._auto_start.last_week_day == None:
            self._auto_start.last_week_day = 0

    auto_start = property(__get_auto_start, __set_auto_start)

    def __get_repeate(self) -> RepeateConferenceModeEntity:
        return self._repeate

    def __set_repeate(self, value: RepeateConferenceModeEntity):
        if value != None:
            self._repeate = value
        if self._repeate.repeate_mode == None:
            self._repeate.repeate_mode = "off"
        if self._repeate.repeate_count == None:
            self._repeate.repeate_count = 0
        if self._repeate.repeate_last_time == None:
            self._repeate.repeate_last_time = 0

    repeate = property(__get_repeate, __set_repeate)

    def __get_stop_time_table(self) -> StopTimeTableEntity:
        return self._stop_time_table

    def __set_stop_time_table(self, value: StopTimeTableEntity):
        if value != None:
            self._stop_time_table = value
        if self._stop_time_table.stop_time_table_mode == None:
            self._stop_time_table.stop_time_table_mode = "off"
        if self._stop_time_table.stop_time_table_begin == None:
            self._stop_time_table.stop_time_table_begin = 0
        if self._stop_time_table.stop_time_table_end == None:
            self._stop_time_table.stop_time_table_end = 0

    stop_time_table = property(__get_stop_time_table, __set_stop_time_table)

    def __get_auto_stop(self) -> AutoStopConferenceEntity:
        return self._auto_stop

    def __set_auto_stop(self, value: AutoStopConferenceEntity):
        if value != None:
            self._auto_stop = value
        if self._auto_stop.stop_mode == None:
            self._auto_stop.stop_mode = "off"
        if self._auto_stop.stop_duration == None:
            self._auto_stop.stop_duration = 0
        if self._auto_stop.stop_abs_time == None:
            self._auto_stop.stop_abs_time = 0

    auto_stop = property(__get_auto_stop, __set_auto_stop)

    def __get_max_participants(self) -> int:
        return self._max_participants

    def __set_max_participants(self, value):
        if value != None:
            self._max_participants = value
        else:
            self._max_participants = 16

    max_participants = property(__get_max_participants, __set_max_participants)

    def __get_autocall(self) -> bool:
        return self._autocall

    def __set_autocall(self, value):
        if value != None:
            self._autocall = value
        else:
            self._autocall = False

    autocall = property(__get_autocall, __set_autocall)

    def __get_rtmp(self) -> str:
        return self._rtmp

    def __set_rtmp(self, value):
        if value != None:
            self._rtmp = value
        else:
            self._rtmp = ""

    rtmp = property(__get_rtmp, __set_rtmp)

    def __get_record_participant(self) -> int:
        return self._record_participant

    def __set_record_participant(self, value):
        if value != None:
            self._record_participant = value
        else:
            self._record_participant = 0

    record_participant = property(__get_record_participant, __set_record_participant)

    def __get_audio(self) -> dict:
        return self._audio

    def __set_audio(self, value):
        self._audio = value

    audio = property(__get_audio, __set_audio)

    def __get_video(self) -> dict:
        return self._video

    def __set_video(self, value):
        self._video = value

    video = property(__get_video, __set_video)


class DeleteConferenceRequest:
    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class ChangeConferenceRequest:
    def __init__(self, conference: ConferenceEntity):
        self._conference = conference

    @property
    def conference(self) -> ConferenceEntity:
        return self._conference


class GetConferenceParticipantRequest:

    def __init__(self, confid: int, contactid: int):
        self._confid = confid
        self._contactid = contactid

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def contactid(self) -> int:
        return self._contactid


class GetConferenceParticipantsListRequest:

    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class CreateConferenceParticipantRequest:

    def __init__(self, confid: int, contactid: int, use_audio: bool, use_video: bool, call_ab: bool, volume: int,
                 use_audio_tx: bool, volume_tx: int, use_video_tx: bool, use_pres: bool, render_mode: str,
                 call_protocol: str, noise_suppressor: str, auto_gain_ctrl: str):
        self.__set_confid(confid)
        self.__set_contactid(contactid)
        self.__set_use_audio(use_audio)
        self.__set_use_video(use_video)
        self.__set_call_ab(call_ab)
        self.__set_volume(volume)
        self.__set_use_audio_tx(use_audio_tx)
        self.__set_volume_tx(volume_tx)
        self.__set_use_video_tx(use_video_tx)
        self.__set_use_pres(use_pres)
        self.__set_render_mode(render_mode)
        self.__set_call_protocol(call_protocol)
        self.__set_noise_suppressor(noise_suppressor)
        self.__set_auto_gain_ctrl(auto_gain_ctrl)

    def __get_confid(self) -> int:
        return self._confid

    def __set_confid(self, value):
        self._confid = value

    confid = property(__get_confid, __set_confid)

    def __get_contactid(self) -> int:
        return self._contactid

    def __set_contactid(self, value):
        self._contactid = value

    contactid = property(__get_contactid, __set_contactid)

    def __get_use_audio(self) -> bool:
        return self._use_audio

    def __set_use_audio(self, value):
        if value != None:
            self._use_audio = value
        else:
            self._use_audio = True

    use_audio = property(__get_use_audio, __set_use_audio)

    def __get_use_video(self) -> bool:
        return self._use_video

    def __set_use_video(self, value):
        if value != None:
            self._use_video = value
        else:
            self._use_video = True

    use_video = property(__get_use_video, __set_use_video)

    def __get_call_ab(self) -> bool:
        return self._call_ab

    def __set_call_ab(self, value):
        if value != None:
            self._call_ab = value
        else:
            self._call_ab = True

    call_ab = property(__get_call_ab, __set_call_ab)

    def __get_volume(self) -> int:
        return self._volume

    def __set_volume(self, value):
        self._volume = value

    volume = property(__get_volume, __set_volume)

    def __get_use_audio_tx(self) -> bool:
        return self._use_audio_tx

    def __set_use_audio_tx(self, value):
        self._use_audio_tx = value

    use_audio_tx = property(__get_use_audio_tx, __set_use_audio_tx)

    def __get_volume_tx(self) -> int:
        return self._volume_tx

    def __set_volume_tx(self, value):
        self._volume_tx = value

    volume_tx = property(__get_volume_tx, __set_volume_tx)

    def __get_use_video_tx(self) -> bool:
        return self._use_video_tx

    def __set_use_video_tx(self, value):
        self._use_video_tx = value

    use_video_tx = property(__get_use_video_tx, __set_use_video_tx)

    def __get_use_pres(self) -> bool:
        return self._use_pres

    def __set_use_pres(self, value):
        self._use_pres = value

    use_pres = property(__get_use_pres, __set_use_pres)

    def __get_render_mode(self) -> str:
        return self._render_mode

    def __set_render_mode(self, value):
        self._render_mode = value

    render_mode = property(__get_render_mode, __set_render_mode)

    def __get_call_protocol(self) -> str:
        return self._call_protocol

    def __set_call_protocol(self, value):
        self._call_protocol = value

    call_protocol = property(__get_call_protocol, __set_call_protocol)

    def __get_noise_suppressor(self) -> str:
        return self._noise_suppressor

    def __set_noise_suppressor(self, value):
        self._noise_suppressor = value

    noise_suppressor = property(__get_noise_suppressor, __set_noise_suppressor)

    def __get_auto_gain_ctrl(self) -> str:
        return self._auto_gain_ctrl

    def __set_auto_gain_ctrl(self, value):
        self._auto_gain_ctrl = value

    auto_gain_ctrl = property(__get_auto_gain_ctrl, __set_auto_gain_ctrl)


class DeleteConferenceParticipantRequest:

    def __init__(self, confid: int, contactid: int):
        self._confid = confid
        self._contactid = contactid

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def contactid(self) -> int:
        return self._contactid


class ChangeConferenceParticipantRequest:

    def __init__(self, confid: int, contactid: int, use_audio: bool, use_video: bool, call_ab: bool, volume: int,
                 use_audio_tx: bool, volume_tx: int, use_video_tx: bool, use_pres: bool, render_mode: str,
                 call_protocol: str, noise_suppressor: str, auto_gain_ctrl: str):
        self._confid = confid
        self._contactid = contactid
        self._use_audio = use_audio
        self._use_video = use_video
        self._call_ab = call_ab
        self._volume = volume
        self._use_audio_tx = use_audio_tx
        self._volume_tx = volume_tx
        self._use_video_tx = use_video_tx
        self._use_pres = use_pres
        self._render_mode = render_mode
        self._call_protocol = call_protocol
        self._noise_suppressor = noise_suppressor
        self._auto_gain_ctrl = auto_gain_ctrl

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def contactid(self) -> int:
        return self._contactid

    @property
    def use_audio(self) -> bool:
        return self._use_audio

    @property
    def use_video(self) -> bool:
        return self._use_video

    @property
    def call_ab(self) -> bool:
        return self._call_ab

    @property
    def volume(self) -> int:
        return self._volume

    @property
    def use_audio_tx(self) -> bool:
        return self._use_audio_tx

    @property
    def volume_tx(self) -> int:
        return self._volume_tx

    @property
    def use_video_tx(self) -> bool:
        return self._use_video_tx

    @property
    def use_pres(self) -> bool:
        return self._use_pres

    @property
    def render_mode(self) -> str:
        return self._render_mode

    @property
    def call_protocol(self) -> str:
        return self._call_protocol

    @property
    def noise_suppressor(self) -> str:
        return self._noise_suppressor

    @property
    def auto_gain_ctrl(self) -> str:
        return self._auto_gain_ctrl


class StartConferenceSessionRequest:

    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class StopConferenceSessionRequest:

    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class GetConferenceSessionParticipantRequest:

    def __init__(self, confid: int, participant_id: int, use_conn_id: bool):
        self._confid = confid
        self._participant_id = participant_id
        self._use_conn_id = use_conn_id

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def use_conn_id(self) -> bool:
        return self._use_conn_id


class GetConferenceSessionParticipantsListRequest:

    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class InviteSessionParticipantRequest:

    def __init__(self, session_addr: SessionParticipantAddressEntity):
        self._session_addr = session_addr

    @property
    def session_addr(self) -> SessionParticipantAddressEntity:
        return self._session_addr


class InviteConferenceParticipantRequest:

    def __init__(self, conference_addr: ConferenceParticipantAddressEntity):
        self._conference_addr = conference_addr

    @property
    def conference_addr(self) -> ConferenceParticipantAddressEntity:
        return self._conference_addr


class HangupSessionParticipantRequest:

    def __init__(self, session_addr: SessionParticipantAddressEntity):
        self._session_addr = session_addr

    @property
    def session_addr(self) -> SessionParticipantAddressEntity:
        return self._session_addr


class HangupConferenceParticipantRequest:

    def __init__(self, conference_addr: ConferenceParticipantAddressEntity, use_connection_id: bool):
        self._conference_addr = conference_addr
        self._use_connection_id = use_connection_id

    @property
    def conference_addr(self) -> ConferenceParticipantAddressEntity:
        return self._conference_addr

    @property
    def use_connection_id(self) -> bool:
        return self._use_connection_id


class GetOnlineLayoutRequest:

    def __init__(self, confid: int, layout_name: str):
        self._confid = confid
        self._layout_name = layout_name

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def layout_name(self) -> str:
        return self._layout_name


class GetOnlineLayoutsListRequest:

    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class CreateOnlineLayoutRequest:

    def __init__(self, confid: int, participant_id: int, cells_count: int, schema_type: int,
                 participant_positions: list, auto: bool, layout_type: str, connection_id: int,
                 display_name: str, template: bool):
        self._confid = confid
        self._participant_id = participant_id
        self._cells_count = cells_count
        self._schema_type = schema_type
        self._participant_positions = participant_positions
        self._auto = auto
        self._layout_type = layout_type
        self._connection_id = connection_id
        self._display_name = display_name
        self._template = template

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def cells_count(self) -> int:
        return self._cells_count

    @property
    def schema_type(self) -> int:
        return self._schema_type

    # list of OnlineLayoutAbonentEntity
    @property
    def participant_positions(self) -> list:
        return self._participant_positions

    @property
    def auto(self) -> bool:
        return self._auto

    @property
    def layout_type(self) -> str:
        return self._layout_type

    @property
    def connection_id(self) -> int:
        return self._connection_id

    @property
    def display_name(self) -> str:
        return self._display_name

    @property
    def template(self) -> bool:
        return self._template


class DeleteOnlineLayoutRequest:

    def __init__(self, confid: int, layout_name: str):
        self._confid = confid
        self._layout_name = layout_name

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def layout_name(self) -> str:
        return self._layout_name


class ChangeOnlineLayoutRequest:

    def __init__(self, confid: int, layout_name: str, cells_count: int, schema_type: int, participant_positions: list,
                 auto: bool):
        self._confid = confid
        self._layout_name = layout_name
        self._cells_count = cells_count
        self._schema_type = schema_type
        self._participant_positions = participant_positions
        self._auto = auto

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def layout_name(self) -> str:
        return self._layout_name

    @property
    def cells_count(self) -> int:
        return self._cells_count

    @property
    def schema_type(self) -> int:
        return self._schema_type

    # list of OnlineLayoutAbonentEntity
    @property
    def participant_positions(self) -> list:
        return self._participant_positions

    @property
    def auto(self) -> bool:
        return self._auto


class GetAllLayoutsTypesRequest:

    def __init__(self):
        pass


class CreateLayoutFromTemplateRequest:

    def __init__(self, confid: int, layout_template_id: int, layout_type: str, participant_id: int, connection_id: int):
        self._confid = confid
        self._layout_template_id = layout_template_id
        self._layout_type = layout_type
        self._participant_id = participant_id
        self._connection_id = connection_id

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def layout_template_id(self) -> int:
        return self._layout_template_id

    @property
    def layout_type(self) -> str:
        return self._layout_type

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def connection_id(self) -> int:
        return self._connection_id


class CreateTemplateFromOnlineLayoutRequest:

    def __init__(self, confid: int, layout_name: str, display_name: str):
        self._confid = confid
        self._layout_name = layout_name
        self._display_name = display_name

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def layout_name(self) -> str:
        return self._layout_name

    @property
    def display_name(self) -> str:
        return self._display_name


class GetSheduledLayoutRequest:

    def __init__(self, confid: int, layout_id: int):
        self._confid = confid
        self._layout_id = layout_id

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def layout_id(self) -> int:
        return self._layout_id


class GetSheduledLayoutsListRequest:

    def __init__(self, confid: int, template: bool):
        self._confid = confid
        self._template = template

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def template(self) -> bool:
        return self._template


class BaseCreateLayoutRequest:

    def __init__(self, confid: int, layout_type: str, auto: bool, cells_count: int, schema_type: int, wndlist: list,
                 display_name: str, template: bool):
        self._confid = confid
        self._layout_type = layout_type
        self._auto = auto
        self._cells_count = cells_count
        self._schema_type = schema_type
        self._wndlist = wndlist
        self._display_name = display_name
        self._template = template

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def layout_type(self) -> str:
        return self._layout_type

    @property
    def auto(self) -> bool:
        return self._auto

    @property
    def cells_count(self) -> int:
        return self._cells_count

    @property
    def schema_type(self) -> int:
        return self._schema_type

    @property
    def wndlist(self) -> list:
        return self._wndlist

    @property
    def display_name(self) -> str:
        return self._display_name

    @property
    def template(self) -> bool:
        return self._template


class CreateGlobalLayoutRequest(BaseCreateLayoutRequest):

    def __init__(self, confid: int, auto: bool, cells_count: int, schema_type: int, wndlist: list,
                 display_name: str, template: bool):
        BaseCreateLayoutRequest.__init__(self, confid, "global", auto, cells_count, schema_type, wndlist,
                                         display_name, template)


class CreateIndividualLayoutRequest(BaseCreateLayoutRequest):

    def __init__(self, confid: int, auto: bool, cells_count: int, schema_type: int, wndlist: list, participant_id: int,
                 display_name: str, template: bool):
        BaseCreateLayoutRequest.__init__(self, confid, "individual", auto, cells_count, schema_type, wndlist,
                                         display_name, template)
        self._participant_id = participant_id

    @property
    def participant_id(self) -> int:
        return self._participant_id


class CreateAspeakerLayoutRequest(BaseCreateLayoutRequest):

    def __init__(self, confid: int, auto: bool, cells_count: int, schema_type: int, wndlist: list,
                 display_name: str, template: bool):
        BaseCreateLayoutRequest.__init__(self, confid, "active_speaker", auto, cells_count, schema_type, wndlist,
                                         display_name, template)


class ChangeSheduledLayoutRequest:
    def __init__(self, layout_id: int, auto: bool, cells_count: int, schema_type: int, wndlist: list,
                 display_name: str):
        self._layout_id = layout_id
        self._auto = auto
        self._cells_count = cells_count
        self._schema_type = schema_type
        self._wndlist = wndlist
        self._display_name = display_name

    @property
    def layout_id(self) -> int:
        return self._layout_id

    @property
    def auto(self) -> bool:
        return self._auto

    @property
    def cells_count(self) -> int:
        return self._cells_count

    @property
    def schema_type(self) -> int:
        return self._schema_type

    @property
    def wndlist(self) -> list:
        return self._wndlist

    @property
    def display_name(self) -> str:
        return self._display_name


class DeleteSheduledLayoutRequest:

    def __init__(self, confid: int, layout_id: int):
        self._confid = confid
        self._layout_id = layout_id

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def layout_id(self) -> int:
        return self._layout_id


class GetGroupRequest:

    def __init__(self, id: int):
        self._id = id

    @property
    def id(self) -> int:
        return self._id


class GetGroupsListRequest:

    def __init__(self, page: int = 0, per_page: int = 60, filter_name: str = ""):
        self._page = page
        self._per_page = per_page
        self._filter_name = filter_name

    @property
    def page(self) -> int:
        return self._page

    @property
    def per_page(self) -> int:
        return self._per_page

    @property
    def filter_name(self) -> str:
        return self._filter_name


class CreateGroupRequest:

    def __init__(self, name: str):
        self._name = name

    @property
    def name(self) -> str:
        return self._name


class ChangeGroupRequest:

    def __init__(self, group: GroupEntity):
        self._group = group

    @property
    def group(self) -> GroupEntity:
        return self._group


class DeleteGroupRequest:

    def __init__(self, id: int):
        self._id = id

    @property
    def id(self) -> int:
        return self._id


class GetGroupContactListRequest(GetContactListRequest):

    def __init__(self, group_id: int, page: int = 0, per_page: int = 60, sort_by: str = "id", order_by: str = "asc",
                 name: str = "", email: str = "", display_name: str = "", sip: str = "", h323: str = "", rtsp: str = "",
                 autoadded: bool = False):
        GetContactListRequest.__init__(self, page, per_page, sort_by, order_by, name, email, display_name, sip, h323,
                                       rtsp, autoadded)
        self.__set_group_id(group_id)

    def __get_group_id(self) -> int:
        return self._group_id

    def __set_group_id(self, value):
        if value is not None:
            self._group_id = value
        else:
            self._group_id = 0

    group_id = property(__get_group_id, __set_group_id)


class AddContactToGroupRequest:
    def __init__(self, group_id: int, contact_id_list: list):
        self._group_id = group_id
        self._contact_id_list = contact_id_list

    @property
    def group_id(self) -> int:
        return self._group_id

    @property
    def contact_id_list(self) -> list:
        return self._contact_id_list


class GetContactOfGroupRequest:
    def __init__(self, group_id: int, contact_id: int):
        self._group_id = group_id
        self._contact_id = contact_id

    @property
    def group_id(self) -> int:
        return self._group_id

    @property
    def contact_id(self) -> int:
        return self._contact_id


class DeleteContactFromGroupRequest:
    def __init__(self, group_id: int, contact_id: int):
        self._group_id = group_id
        self._contact_id = contact_id

    @property
    def group_id(self) -> int:
        return self._group_id

    @property
    def contact_id(self) -> int:
        return self._contact_id


# build CSR-file (1st phase of offline registration)
class OfflineRegistrationRequest:

    def __init__(self, serial: str, server_name: str):
        self._serial = serial
        self._server_name = server_name

    @property
    def serial(self) -> str:
        return self._serial

    @property
    def server_name(self) -> str:
        return self._server_name


# online registration
class OnlineRegistrationRequest:

    def __init__(self, serial: str, server_name: str, request_as_file: bool):
        self._serial = serial
        self._server_name = server_name
        self._request_as_file = request_as_file

    @property
    def serial(self) -> str:
        return self._serial

    @property
    def server_name(self) -> str:
        return self._server_name

    @property
    def request_as_file(self) -> bool:
        return self._request_as_file


# upload license (2nd phase of offline registration)
class UploadLicenseRequest:

    def __init__(self, licenses_str: str):
        self._licenses_str = licenses_str

    @property
    def licenses_str(self) -> str:
        return self._licenses_str


class GetLicenseRequest:

    def __init__(self):
        pass


class EmptyRequest:

    def __init__(self):
        pass


class InstanceCallsRequest:

    def __init__(self, calls: CallsEntity):
        self._calls = calls

    @property
    def calls(self) -> CallsEntity:
        return self._calls


class InstanceSipRequest:

    def __init__(self, sip: SipEntity):
        self._sip = sip

    @property
    def sip(self) -> SipEntity:
        return self._sip


class SetH323SettingsRequest:

    def __init__(self, h323: H323SettingsEntity):
        self._h323 = h323

    @property
    def h323(self) -> H323SettingsEntity:
        return self._h323


class InstanceVideoRequest:

    def __init__(self, video: VideoSettingsEntity):
        self._video = video

    @property
    def video(self) -> VideoSettingsEntity:
        return self._video


class InstanceLDAPRequest:

    def __init__(self, ldap: LDAPSettingsEntity):
        self._ldap = ldap

    @property
    def ldap(self) -> LDAPSettingsEntity:
        return self._ldap


class InstanceEmailRequest:

    def __init__(self, email: EmailSettingsEntity):
        self._email = email

    @property
    def email(self) -> EmailSettingsEntity:
        return self._email


class InstanceDBRequest:

    def __init__(self, db: DBSettingsEntity):
        self._db = db

    @property
    def db(self) -> DBSettingsEntity:
        return self._db


class CreateLayoutBackupRequest:

    def __init__(self, confid: int, layout_id: int, cells_count: int, schema_type: int, participant_positions: list,
                 owner_id: int, auto: bool, layout_type: int, display_name: str, template: bool):
        self._confid = confid
        self._layout_id = layout_id
        self._cells_count = cells_count
        self._schema_type = schema_type
        self._participant_positions = participant_positions
        self._owner_id = owner_id
        self._auto = auto
        self._layout_type = layout_type
        self._display_name = display_name
        self._template = template

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def layout_id(self) -> int:
        return self._layout_id

    @property
    def cells_count(self) -> int:
        return self._cells_count

    @property
    def schema_type(self) -> int:
        return self._schema_type

    # list of LayoutAbonentPositionEntity(wndnum: int, abid: int, priority: int, role_id: int)
    @property
    def participant_positions(self) -> list:
        return self._participant_positions

    @property
    def owner_id(self) -> int:
        return self._owner_id

    @property
    def auto(self) -> bool:
        return self._auto

    @property
    def layout_type(self) -> int:
        return self._layout_type

    @property
    def display_name(self) -> str:
        return self._display_name

    @property
    def template(self) -> bool:
        return self._template


class GetAllResolutionsListRequest:

    def __init__(self):
        pass


class CalculateLayoutRequest:

    def __init__(self, cells_count: int, width: int, height: int, pip: bool, schema_type: int):
        self._cells_count = cells_count
        self._width = width
        self._height = height
        self._pip = pip
        self._schema_type = schema_type

    @property
    def cells_count(self) -> int:
        return self._cells_count

    @property
    def width(self) -> int:
        return self._width

    @property
    def height(self) -> int:
        return self._height

    @property
    def pip(self) -> bool:
        return self._pip

    # type of layout (index: 0,1,2,3)
    @property
    def schema_type(self) -> int:
        return self._schema_type


class SwitchMicrophoneRequest:
    def __init__(self, conference_id: int, participant_id: int, turn_on: bool, direction: str,
                 use_conn_id: bool):
        self._conference_id = conference_id
        self._participant_id = participant_id
        self._turn_on = turn_on
        self._direction = direction
        self._use_conn_id = use_conn_id

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def turn_on(self) -> bool:
        return self._turn_on

    @property
    def direction(self) -> str:
        return self._direction

    @property
    def use_conn_id(self) -> bool:
        return self._use_conn_id


class SwitchAudioToGroupAbonentsRequest:
    def __init__(self, conference_id: int, connection_id_list: list, turn_on: bool, direction: str):
        self._conference_id = conference_id
        self._connection_id_list = connection_id_list
        self._turn_on = turn_on
        self._direction = direction

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def connection_id_list(self) -> list:
        return self._connection_id_list

    @property
    def turn_on(self) -> bool:
        return self._turn_on

    @property
    def direction(self) -> str:
        return self._direction


class TuneSpeakerVolumeRequest:
    def __init__(self, conference_id: int, participant_id: int, volume: int, direction: str,
                 use_conn_id: bool):
        self._conference_id = conference_id
        self._participant_id = participant_id
        self._volume = volume
        self._direction = direction
        self._use_conn_id = use_conn_id

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def volume(self) -> int:
        return self._volume

    @property
    def direction(self) -> str:
        return self._direction

    @property
    def use_conn_id(self) -> bool:
        return self._use_conn_id


class SwitchVideoRequest:
    def __init__(self, conference_id: int, participant_id: int, turn_on: bool, direction: str,
                 use_conn_id: bool):
        self._conference_id = conference_id
        self._participant_id = participant_id
        self._turn_on = turn_on
        self._direction = direction
        self._use_conn_id = use_conn_id

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def turn_on(self) -> bool:
        return self._turn_on

    @property
    def direction(self) -> str:
        return self._direction

    @property
    def use_conn_id(self) -> bool:
        return self._use_conn_id


class BaseSwitchPreviewModeRequest:
    def __init__(self, conference_id: int, participant_id: int, turn_on: bool, use_conn_id: bool):
        self._conference_id = conference_id
        self._participant_id = participant_id
        self._turn_on = turn_on
        self._use_conn_id = use_conn_id

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def turn_on(self) -> bool:
        return self._turn_on

    @property
    def use_conn_id(self) -> bool:
        return self._use_conn_id


class TurnOnPreviewModeRequest(BaseSwitchPreviewModeRequest):
    def __init__(self, conference_id: int, participant_id: int, stream: str, use_conn_id: bool):
        BaseSwitchPreviewModeRequest.__init__(self, conference_id, participant_id, True, use_conn_id)
        self._stream = stream

    @property
    def stream(self) -> str:
        return self._stream


class TurnOffPreviewModeRequest(BaseSwitchPreviewModeRequest):
    def __init__(self, conference_id: int, participant_id: int, use_conn_id: bool):
        BaseSwitchPreviewModeRequest.__init__(self, conference_id, participant_id, False, use_conn_id)


class GetPreviewImageRequest:
    def __init__(self, conference_id: int, participant_id: int):
        self._conference_id = conference_id
        self._participant_id = participant_id

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def participant_id(self) -> int:
        return self._participant_id


class CameraControlRequest:
    def __init__(self, conference_id: int, participant_id: int, command: str, use_conn_id: bool):
        self._conference_id = conference_id
        self._participant_id = participant_id
        self._command = command
        self._use_conn_id = use_conn_id

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def command(self) -> str:
        return self._command

    @property
    def use_conn_id(self) -> bool:
        return self._use_conn_id


class RenameParticipantRequest:
    def __init__(self, conference_id: int, participant_id: int, dispname: str, use_conn_id: bool):
        self._conference_id = conference_id
        self._participant_id = participant_id
        self._dispname = dispname
        self._use_conn_id = use_conn_id

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def dispname(self) -> str:
        return self._dispname

    @property
    def use_conn_id(self) -> bool:
        return self._use_conn_id


class SendMessageToConfRequest:
    def __init__(self, conference_id: int, message: str):
        self._conference_id = conference_id
        self._message = message

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def message(self) -> str:
        return self._message


class MoveParticipantToOtherConfRequest:
    def __init__(self, conf_from: int, conf_to: int, abonent_uri: str, conn_id: int):
        self._conf_from = conf_from
        self._conf_to = conf_to
        self._abonent_uri = abonent_uri
        self._conn_id = conn_id

    @property
    def conf_from(self) -> int:
        return self._conf_from

    @property
    def conf_to(self) -> int:
        return self._conf_to

    @property
    def abonent_uri(self) -> str:
        return self._abonent_uri

    @property
    def conn_id(self) -> int:
        return self._conn_id


class GetWaitingAbonentsRequest:
    def __init__(self):
        pass


class SwitchVideoRecordRequest:
    def __init__(self, conference_id: int, turn_on: bool, url_type: str):
        self._conference_id = conference_id
        self._turn_on = turn_on
        self._url_type = url_type

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def turn_on(self) -> bool:
        return self._turn_on

    @property
    def url_type(self) -> str:
        return self._url_type


class GetRecordingStatusRequest:
    def __init__(self, conference_id: int):
        self._conference_id = conference_id

    @property
    def conference_id(self) -> int:
        return self._conference_id


class GetFilesListRequest:
    def __init__(self, filetype: str):
        self._filetype = filetype

    @property
    def filetype(self) -> str:
        return self._filetype


class GetFileRequest:
    def __init__(self, filetype: str, filename: str):
        self._filetype = filetype
        self._filename = filename

    @property
    def filetype(self) -> str:
        return self._filetype

    @property
    def filename(self) -> str:
        return self._filename


class UploadFileRequest:
    def __init__(self, filetype: str, filename: str, data: bytes):
        self._filetype = filetype
        self._filename = filename
        self._data = data

    @property
    def filetype(self) -> str:
        return self._filetype

    @property
    def filename(self) -> str:
        return self._filename

    @property
    def data(self) -> bytes:
        return self._data


class UploadFileByChunksRequest:
    def __init__(self, filetype: str, filename: str, reader):
        self._filetype = filetype
        self._filename = filename
        self._reader = reader

    @property
    def filetype(self) -> str:
        return self._filetype

    @property
    def filename(self) -> str:
        return self._filename

    @property
    def reader(self):
        return self._reader


class DeleteFileRequest:
    def __init__(self, filetype: str, filename: str):
        self._filetype = filetype
        self._filename = filename

    @property
    def filetype(self) -> str:
        return self._filetype

    @property
    def filename(self) -> str:
        return self._filename


class SwitchPresentationRequest:
    def __init__(self, conference_id: int, turn_on: bool, filename: str):
        self._conference_id = conference_id
        self._turn_on = turn_on
        self._filename = filename

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def turn_on(self) -> bool:
        return self._turn_on

    # short file name of image, stored in special directory
    @property
    def filename(self) -> str:
        return self._filename


class GetPresentationStatusRequest:
    def __init__(self, conference_id: int):
        self._conference_id = conference_id

    @property
    def conference_id(self) -> int:
        return self._conference_id


class StopIncomingPresentationRequest:
    def __init__(self, conference_id: int):
        self._conference_id = conference_id

    @property
    def conference_id(self) -> int:
        return self._conference_id


class ExportAbookToFileRequest:

    def __init__(self, abook_type: str):
        self._abook_type = abook_type

    # may be "filecsv" or "filexml"
    @property
    def abook_type(self) -> str:
        return self._abook_type


class ImportAbookFromFileRequest:

    def __init__(self, abook_type: str, short_filename: str):
        self._abook_type = abook_type
        self._short_filename = short_filename

    # may be "filecsv" or "filexml"
    @property
    def abook_type(self) -> str:
        return self._abook_type

    # name of imported file (without path)
    @property
    def short_filename(self) -> str:
        return self._short_filename


class SaveDatabaseToFile:

    def __init__(self):
        pass


class RestoreDatabaseFromFile:

    def __init__(self, short_filename: str):
        self._short_filename = short_filename

    # name of imported file (without path)
    @property
    def short_filename(self) -> str:
        return self._short_filename


class AllConfSessionParticipantsRequest:
    def __init__(self):
        pass


class GetServerTimeRequest:
    def __init__(self):
        pass


class GetMCUParamsRequest:
    def __init__(self):
        pass


class GetPreviewStateRequest:
    def __init__(self):
        pass


class StartUtilRequest:
    def __init__(self, util_type: str, argument: str):
        self._util_type = util_type
        self._argument = argument

    @property
    def util_type(self) -> str:
        return self._util_type

    @property
    def argument(self) -> str:
        return self._argument


class StopUtilRequest:
    def __init__(self, util_type: str):
        self._util_type = util_type

    @property
    def util_type(self) -> str:
        return self._util_type


class GetUtilOutputRequest:
    def __init__(self, util_type: str):
        self._util_type = util_type

    @property
    def util_type(self) -> str:
        return self._util_type


class SetCrownRequest:
    def __init__(self, confid: int, contactid: int, use_conn_id: bool):
        self._confid = confid
        self._contactid = contactid
        self._use_conn_id = use_conn_id

    @property
    def confid(self) -> int:
        return self._confid

    @property
    def contactid(self) -> int:
        return self._contactid

    @property
    def use_conn_id(self) -> bool:
        return self._use_conn_id


class DeleteCrownRequest:
    def __init__(self, confid: int):
        self._confid = confid

    @property
    def confid(self) -> int:
        return self._confid


class SetRTPSettingsRequest:

    def __init__(self, rtp: RTPEntity):
        self._rtp = rtp

    @property
    def rtp(self) -> RTPEntity:
        return self._rtp


class GetAllNetworkSettingsRequest:
    def __init__(self):
        pass


class GetNetworkSettingsRequest:
    def __init__(self, iface_name: str):
        self._iface_name = iface_name

    @property
    def iface_name(self) -> str:
        return self._iface_name


class ChangeNetworkSettingsRequest:
    def __init__(self, settings: NetworkSettingsEntity):
        self._settings = settings

    @property
    def settings(self) -> NetworkSettingsEntity:
        return self._settings


class CanChangeNetworkSettingsRequest:
    def __init__(self):
        pass


class GetVEngineLogRequest:
    def __init__(self, timestamp: int, id: int):
        self._timestamp = timestamp
        self._id = id

    @property
    def timestamp(self) -> int:
        return self._timestamp

    @property
    def id(self) -> int:
        return self._id


class ExecuteVEngineCommandRequest:
    def __init__(self, command: str):
        self._command = command

    @property
    def command(self) -> str:
        return self._command


class GetMCUReferencesRequest:
    def __init__(self, language: str):
        self._language = language

    @property
    def language(self) -> str:
        return self._language


class CleanAddressBookRequest:
    def __init__(self):
        pass


class AddCallsHistoryEventRequest:
    def __init__(self, outgoing: bool, participant_uri: str, conference_id: int, connected: bool):
        self._outgoing = outgoing
        self._participant_uri = participant_uri
        self._conference_id = conference_id
        self._connected = connected

    @property
    def outgoing(self) -> bool:
        return self._outgoing

    @property
    def participant_uri(self) -> str:
        return self._participant_uri

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def connected(self) -> bool:
        return self._connected


class CreateCustomLayoutTypeRequest:

    def __init__(self, width: int, height: int, centered_row: int, selected_slots: list):
        self._width = width
        self._height = height
        self._centered_row = centered_row
        self._selected_slots = selected_slots

    @property
    def width(self) -> int:
        return self._width

    @property
    def height(self) -> int:
        return self._height

    @property
    def centered_row(self) -> int:
        return self._centered_row

    # list of CustomLayoutTypeSlotEntity
    @property
    def selected_slots(self) -> list:
        return self._selected_slots


class ChangeCustomLayoutTypeRequest:
    def __init__(self, type: CustomLayoutTypeEntity):
        self._type = type

    @property
    def type(self) -> CustomLayoutTypeEntity:
        return self._type


class DeleteCustomLayoutTypeRequest:
    def __init__(self, layout_type: int):
        self._layout_type = layout_type

    @property
    def layout_type(self) -> int:
        return self._layout_type


class GetCustomLayoutTypeRequest:
    def __init__(self, layout_type: int):
        self._layout_type = layout_type

    @property
    def layout_type(self) -> int:
        return self._layout_type


class GetAllCustomLayoutTypesRequest:
    def __init__(self):
        pass


class ExecuteRestartCommandRequest:

    def __init__(self, command: str):
        self._command = command

    @property
    def command(self) -> str:
        return self._command


class InstallDebRequest:
    def __init__(self, filename: str):
        self._filename = filename

    @property
    def filename(self) -> str:
        return self._filename


class GetInstallDebStateRequest:
    def __init__(self):
        pass


class SwitchRenderModeToGroupAbonentsRequest:
    def __init__(self, conference_id: int, connection_id_list: list, render_mode: str):
        self._conference_id = conference_id
        self._connection_id_list = connection_id_list
        self._render_mode = render_mode

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def connection_id_list(self) -> list:
        return self._connection_id_list

    @property
    def render_mode(self) -> str:
        return self._render_mode


class HangupGroupAbonentsRequest:
    def __init__(self, conference_id: int, connection_id_list: list):
        self._conference_id = conference_id
        self._connection_id_list = connection_id_list

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def connection_id_list(self) -> list:
        return self._connection_id_list


class SwitchNoiseSuppresorToGroupAbonentsRequest:
    def __init__(self, conference_id: int, connection_id_list: list, noise_suppressor: str):
        self._conference_id = conference_id
        self._connection_id_list = connection_id_list
        self._noise_suppressor = noise_suppressor

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def connection_id_list(self) -> list:
        return self._connection_id_list

    @property
    def noise_suppressor(self) -> str:
        return self._noise_suppressor


class SwitchAGCToGroupAbonentsRequest:
    def __init__(self, conference_id: int, connection_id_list: list, auto_gain_ctrl: str):
        self._conference_id = conference_id
        self._connection_id_list = connection_id_list
        self._auto_gain_ctrl = auto_gain_ctrl

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def connection_id_list(self) -> list:
        return self._connection_id_list

    @property
    def auto_gain_ctrl(self) -> str:
        return self._auto_gain_ctrl


class SwitchVideoToGroupAbonentsRequest:
    def __init__(self, conference_id: int, connection_id_list: list, turn_on: bool, direction: str):
        self._conference_id = conference_id
        self._connection_id_list = connection_id_list
        self._turn_on = turn_on
        self._direction = direction

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def connection_id_list(self) -> list:
        return self._connection_id_list

    @property
    def turn_on(self) -> bool:
        return self._turn_on

    @property
    def direction(self) -> str:
        return self._direction

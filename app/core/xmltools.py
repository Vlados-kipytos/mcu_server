#!/usr/bin/python3
# -*- coding: cp1251 -*-


import os
import os.path
import sys
import xml.dom.minidom

g_config = None


def file_to_path(fname):
    """This function raturns path where 'fname' is located"""
    length = len(fname)
    if length == 0:
        return ("")
    i = length - 1
    while i >= 0:
        if fname[i] == '/' or fname[i] == '\\':
            return (fname[:i])
        i = i - 1
    return ("")


if sys.platform == "win32":
    XML_FILENAME = file_to_path(__file__) + "/ctrlmachine_win.xml"
else:
    # DP: To work under CYTHON.  Is it correct???
    XML_FILENAME = '/usr/lib/vcs-tools' + '/ctrlmachine_lin.xml'
    # XML_FILENAME = file_to_path(__file__) + "/ctrlmachine_lin.xml"


def get_deafult_config():
    if sys.platform == "win32":
        config = {"db": {"host": "localhost", "port": 5432, "dbname": "mcu", "username": "postgres", "password": "",
                         "charset": "utf8"},
                  "dirs": {"shareddir": "C:\\ProgramData\\vtsrv\\rec", "logsdir": "C:\\ProgramData\\vtsrv",
                           "videodir": "C:\\ProgramData\\vtsrv\\rec",
                           "addrbookcsv": "C:\\ProgramData\\vtsrv\\addrbook\\csv",
                           "addrbookxml": "C:\\ProgramData\\vtsrv\\addrbook\\xml",
                           "dbcopy": "C:\\ProgramData\\vtsrv\\dbcopy",
                           "presentation": "C:\\ProgramData\\vtsrv\\presentation",
                           "utils": "C:\\ProgramData\\vtsrv\\utildata",
                           "engine": "C:\\ProgramData\\vtsrv",
                           "updatemcu": "C:"},
                  "vile": {"ip": "127.0.0.1", "port": 5080, "login": "", "password": ""},
                  "osversion": {"se": 0, "demo": 0, "appdatavar": "APPDATA"},
                  "abonents": {"maxactiveab": 25, "maxab": 64, "maxlayouts": 15},
                  "about": {"manufacturer": "", "manref": "", "web": "", "email": "", "phone": "", "address": ""},
                  "webapi": {"port": 8081}}
    else:
        config = {
            "db": {"host": "localhost", "port": 5432, "dbname": "vcsdb", "username": "vcsuser", "password": "vcspwd123",
                   "charset": "utf8"},
            "dirs": {"shareddir": "/var/vtsrv/data", "logsdir": "/var/vtsrv", "videodir": "/var/vtsrv/rec",
                     "addrbookcsv": "/var/vtsrv/addrbook/csv", "addrbookxml": "/var/vtsrv/addrbook/xml",
                     "dbcopy": "/var/vtsrv/dbcopy", "presentation": "/var/vtsrv/presentation",
                     "utils": "/var/vtsrv/utildata",
                     "engine": "/var/vtsrv",
                     "updatemcu": "/tmp"},
            "vile": {"ip": "unix://%2Fvar%2Fvtsrv%2Fvtsrv.sock", "port": 5080, "login": "", "password": ""},
            "osversion": {"se": 0, "demo": 0, "appdatavar": ""},
            "abonents": {"maxactiveab": 25, "maxab": 64, "maxlayouts": 15},
            "about": {"manufacturer": "", "manref": "", "web": "", "email": "", "phone": "", "address": ""},
            "webapi": {"port": 8081}}

    return config


def ExtractHttpParams(xmlfile):
    """This function extracts from file config.xml params of HTTPD.
    Returns dictionary of params."""

    SLASH = os.path.sep

    result = get_deafult_config()

    if not os.path.exists(xmlfile):
        return (None)  # error

    xmldoc = xml.dom.minidom.parse(xmlfile)

    # db
    nodeList = xmldoc.getElementsByTagName("db")
    if len(nodeList) != 0:
        node = nodeList[0]
        attr = node.attributes
        for key, val in attr.items():
            # print "key=", key, " , value=", val
            if key == "host" or key == "dbname" or key == "username" or key == "password" or key == "charset":
                result["db"][key] = val
            if key == "port":
                result["db"][key] = int(val)

    # dirs
    nodeList = xmldoc.getElementsByTagName("dirs")
    if len(nodeList) != 0:
        node = nodeList[0]
        attr = node.attributes
        for key, val in attr.items():
            # print "key=", key, " , value=", val
            if key == "shareddir" or key == "logsdir" or key == "videodir" or key == "addrbookcsv" or \
                    key == "addrbookxml" or key == "dbcopy" or key == "presentation" or key == "utils" or key == "engine":
                result["dirs"][key] = val

    if "TCMCU_LOG_DIR" in os.environ:
        result["dirs"]["logsdir"] = os.environ["TCMCU_LOG_DIR"]
    elif "tcmcu_log_dir" in os.environ:
        result["dirs"]["logsdir"] = os.environ["tcmcu_log_dir"]
    if len(result["dirs"]["logsdir"]) != 0 and result["dirs"]["logsdir"][-1] == SLASH:
        result["dirs"]["logsdir"] = result["dirs"]["logsdir"][:-1]

    if "TCMCU_VIDEO_DIR" in os.environ:
        result["dirs"]["videodir"] = os.environ["TCMCU_VIDEO_DIR"]
    elif "tcmcu_video_dir" in os.environ:
        result["dirs"]["videodir"] = os.environ["tcmcu_video_dir"]
    if len(result["dirs"]["videodir"]) != 0 and result["dirs"]["videodir"][-1] == SLASH:
        result["dirs"]["videodir"] = result["dirs"]["videodir"][:-1]

    if sys.platform == "win32" and ("TEMP" in os.environ):
        result["dirs"]["updatemcu"] = os.environ["TEMP"]

    data_dir = ""
    if "TCMCU_WORK_DIR" in os.environ:
        data_dir = os.environ["TCMCU_WORK_DIR"]
    elif "tcmcu_work_dir" in os.environ:
        data_dir = os.environ["tcmcu_work_dir"]

    if data_dir != "":
        if data_dir[-1] == SLASH:
            data_dir = data_dir[:-1]
        result["dirs"]["shareddir"] = data_dir + SLASH + "data"
        result["dirs"]["addrbookcsv"] = data_dir + SLASH + "addrbook" + SLASH + "csv"
        result["dirs"]["addrbookxml"] = data_dir + SLASH + "addrbook" + SLASH + "xml"
        result["dirs"]["dbcopy"] = data_dir + SLASH + "dbcopy"
        result["dirs"]["presentation"] = data_dir + SLASH + "presentation"
        result["dirs"]["utils"] = data_dir + SLASH + "utildata"

    # vile
    nodeList = xmldoc.getElementsByTagName("vile")
    if len(nodeList) != 0:
        node = nodeList[0]
        attr = node.attributes
        for key, val in attr.items():
            # print "key=", key, " , value=", val
            if key == "ip" or key == "login" or key == "password":
                result["vile"][key] = val
            if key == "port":
                result["vile"][key] = int(val)

    # OS version
    nodeList = xmldoc.getElementsByTagName("osversion")
    if len(nodeList) != 0:
        node = nodeList[0]
        attr = node.attributes
        for key, val in attr.items():
            # print "key=", key, " , value=", val
            if key == "se" or key == "demo":
                result["osversion"][key] = int(val)
            if key == "appdatavar":
                result["osversion"][key] = val

    # abonents
    nodeList = xmldoc.getElementsByTagName("abonents")
    if len(nodeList) != 0:
        node = nodeList[0]
        attr = node.attributes
        for key, val in attr.items():
            # print "key=", key, " , value=", val
            if key == "maxactiveab":
                result["abonents"][key] = int(val)
            if key == "maxab":
                result["abonents"][key] = int(val)
            if key == "maxlayouts":
                result["abonents"][key] = int(val)

    # about
    nodeList = xmldoc.getElementsByTagName("about")
    if len(nodeList) != 0:
        node = nodeList[0]
        attr = node.attributes
        for key, val in attr.items():
            # print "key=", key, " , value=", val
            result["about"][key] = val

    # webapi
    nodeList = xmldoc.getElementsByTagName("webapi")
    if len(nodeList) != 0:
        node = nodeList[0]
        attr = node.attributes
        for key, val in attr.items():
            # print "key=", key, " , value=", val
            if key == "port":
                result["webapi"][key] = int(val)

    return (result)


def load_config(_fname):
    global g_config
    g_config = ExtractHttpParams(_fname)


def get_config():
    global g_config

    if g_config != None:
        return g_config

    g_config = get_deafult_config()

    return g_config

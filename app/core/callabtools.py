#!/usr/bin/python3

import os

import app.core.addrbook as addrbook
import app.core.conftools as conftools
import app.core.constants as constants
import app.core.dbtools as dbtools
import app.core.entities as entities
import app.core.features as features
import app.core.layoutcrown as layoutcrown
import app.core.nettools as nettools
import app.core.onlinetools as onlinetools
import app.core.tools as tools
import event_processor.ipc.on_event_tools as on_event_tools
from .errors import ActiveConferenceNotFoundError
from .errors import AlreadyInSessionError
from .errors import ConferenceAlreadyStoppedError
from .errors import FeatureRestrictedByLicenseError
from .errors import InvalidAddressFormatError
from .errors import SendCmdToEngineError
from .errors import SessionAbonentNotFoundError
from .requests import CreateContactRequest
from .requests import DeleteCrownRequest
from .requests import HangupGroupAbonentsRequest
from .responses import MoveToOtherConfResponse
from .responses import SessionGroupOperationResponse


def set_outgoing_call(confid, abid, sipaddr, h323addr, extaddr, file):
    """Set params of outgoing call to temporary buffer"""
    confid = int(confid)
    abid = int(abid)

    # logging.debug( "set_outgoing_call(): confid=%d , abid=%d , sip=%s , h323=%s , ext=%s" % (confid, abid, sipaddr, h323addr, extaddr)  )

    calls_rows = dbtools.select(
        "SELECT count(*) FROM legacy.outgoingcalls WHERE confid=%d and abid=%d" % (confid, abid))
    count = calls_rows[0][0]
    if count != 0:
        dbtools.make_sql_request("""UPDATE legacy.outgoingcalls SET sipaddr='%s', h323addr='%s', extaddr='%s', file='%s' 
        WHERE confid=%d and abid=%d""" % (sipaddr, h323addr, extaddr, file, confid, abid))
    else:
        dbtools.make_sql_request("""INSERT INTO legacy.outgoingcalls (confid, abid, sipaddr, h323addr, extaddr, file) 
        VALUES (%d, %d, '%s', '%s', '%s', '%s')""" % (confid, abid, sipaddr, h323addr, extaddr, file))

    # get default bitrate of confernce
    bitrate = conftools.get_conf_bitrate(confid)

    # get bitrate of abonent (if exist)
    ab_rows = dbtools.load_table("legacy.abonents", "bitrate", "id=%d" % (abid))
    if len(ab_rows) != 0:
        ab_bitrate = ab_rows[0][0]
        if ab_bitrate != 0:
            bitrate = ab_bitrate

    return (bitrate)


def make_outgoing_call(_confid, _abid):
    """This function make outgoing call to abonent _abid in conference _confid."""
    _confid = int(_confid)
    _abid = int(_abid)

    cid = tools.conference_name_by_id(_confid)

    conf_audio_codecs = conftools.get_conf_audio_codecs(_confid)
    conf_video_codecs = conftools.get_conf_video_codecs(_confid)

    conf_bitrate = conftools.get_conf_bitrate(_confid)

    # get abonent's address and other params
    aburi = ""
    ab = addrbook.get_abonent_params(_abid)
    if ab["success"]:
        abid = ab["id"]
        name = ab["name"]
        sip = ab["sip"]
        h323 = ab["h323"]
        extaddr = ab["rtsp"]
        file = ab["file"]
        loop = ab["loop"]
        email = ab["email"]
        ab_bitr = ab["bitrate"]
        ab_audio_codecs = ab["audio_codecs"]
        ab_video_codecs = ab["video_codecs"]

        abook_name = tools.abid_to_abook_name(abid)

        bitr_str = str(conf_bitrate)
        if ab_bitr != 0:
            bitr_str = str(ab_bitr)
        if bitr_str == "0":
            bitr_str = ""

        cmd_list = []

        cmd_list.append("ABOOK REGISTER "+abook_name+ "," + email + "," + "," + "," + bitr_str)
        if sip != "":
            cmd_list.append("ABOOK URI ADD %s,%s" % (abook_name, sip))
        if h323 != "":
            cmd_list.append("ABOOK URI ADD %s,%s" % (abook_name, h323))
        if extaddr != "":
            cmd_list.append("ABOOK URI ADD %s,%s" % (abook_name, extaddr))
        if file != "":
            full_fname = addrbook.get_full_videofile_name(file, loop)
            cmd_list.append("ABOOK URI ADD %s,%s" % (abook_name, full_fname))
        else:
            full_fname = ""

        masked_audio_codecs = entities.get_masked_audio_codecs(conf_audio_codecs, ab_audio_codecs)
        cmd_list += masked_audio_codecs.get_abonent_cmd_list(abook_name)

        masked_video_codecs = entities.get_masked_video_codecs(conf_video_codecs, ab_video_codecs)
        cmd_list += masked_video_codecs.get_abonent_cmd_list(abook_name)

        cmd_list.append("CONFERENCE DIAL %s %s" % (cid, abook_name))

        # store in buffer params of call
        set_outgoing_call(int(_confid), int(_abid), sip, h323, extaddr, full_fname)

        nettools.exec_cmd_list_s(cmd_list)


def is_co_addr(addr1, addr2):
    """This function compare sip/h323 addresses addr1 and addr2.
    If one address is sip-address of abonent and second address is h323-address of same abonent then return 1 else returns 0."""
    parsed_addr1 = tools.parse_address(addr1)
    parsed_addr2 = tools.parse_address(addr2)
    is_co_protcols = 0
    if (parsed_addr1["protocol"] == "sip" and parsed_addr2["protocol"] == "h323") or \
        (parsed_addr1["protocol"] == "h323" and parsed_addr2["protocol"] == "sip"):
        is_co_protcols = 1
    is_hosts_equ = 0
    if parsed_addr1["host"] == parsed_addr2["host"]:
        is_hosts_equ = 1
    is_username_equ = 1
    if parsed_addr1["username"] != "" and parsed_addr2["username"] != "" and \
        parsed_addr1["username"] != parsed_addr2["username"]:
        is_username_equ = 0
    if is_co_protcols and is_hosts_equ and is_username_equ:
        return (1)
    else:
        return (0)


def check_active_abonent_uri(aburi):
    """This function checks: is abonent 'aburi' connected to server? If connected then returns True, else returns False."""
    aburi = str(aburi)

    # get online state from buffer (in database)
    online_data_rows = dbtools.load_table("legacy.onlinedata", "connections", "")
    if len(online_data_rows) != 0:
        connstr = online_data_rows[0][0]
    else:
        connstr = '{ "connection_list": [] }'

    real_ab_list = onlinetools.extract_conn_list(connstr)

    # find abonent 'aburi' on VCS-server
    for j in range(len(real_ab_list)):
        real_ab_uri = real_ab_list[j]["contact_uri"]
        if tools.is_addr_equivalent(real_ab_uri, aburi, True) or is_co_addr(real_ab_uri, aburi):
            return (True)  # found

    return (False)


def check_active_abonent_conn_id(conn_id: int):
    """This function checks: is abonent 'aburi' connected to server? If connected then returns True, else returns False."""
    conn_id = int(conn_id)

    # get online state from buffer (in database)
    online_data_rows = dbtools.load_table("legacy.onlinedata", "connections", "")
    if len(online_data_rows) != 0:
        connstr = online_data_rows[0][0]
    else:
        connstr = '{ "connection_list": [] }'

    real_ab_list = onlinetools.extract_conn_list(connstr)

    # find abonent 'aburi' on VCS-server
    for j in range(len(real_ab_list)):
        if real_ab_list[j]["id"] == conn_id:
            return (True)  # found

    return (False)


def call_abonent_by_uri(confid, aburi):
    confid = int(confid)

    aburi = aburi.strip()

    ab_connected = check_active_abonent_uri(aburi)
    if ab_connected:
        raise AlreadyInSessionError("call_id")

    flist = features.get_features_list()

    # check call abonent by IP (without protocol sip/h323/rtsp)
    correct_protocol = aburi.startswith(("sip:", "h323:", "rtsp:"))
    if not correct_protocol:
        sip = tools.add_prefix_if_not(aburi, "sip:")
        h323 = tools.add_prefix_if_not(aburi, "h323:")
        rtsp = ""

        if check_active_abonent_uri(sip) or check_active_abonent_uri(h323):
            raise AlreadyInSessionError("call_id")

        abonent_name = aburi

        abid = addrbook.get_abid_by_hostport(aburi)  # find abonent in address book by IP
    else:
        if check_active_abonent_uri(aburi):
            raise AlreadyInSessionError("call_id")

        sip = ""
        if aburi.startswith("sip:"):
            sip = aburi
        h323 = ""
        if aburi.startswith("h323:"):
            h323 = aburi
        rtsp = ""
        if aburi.startswith("rtsp:"):
            rtsp = aburi

        if tools.is_correct_rtsp_address(aburi):
            if not features.get_feature(flist, "RTSP"):
                raise FeatureRestrictedByLicenseError("RTSP")

        abonent_name = tools.delete_protocol_and_port(aburi)

        aburi = tools.quote_address(aburi)

        abid = addrbook.get_abonent_id(aburi)  # find abonent in address book by uri

    # if contact is not found in address book, add it
    if abid == 0:
        abonent_name = addrbook.get_unique_contact_name(abonent_name)
        audio_codecs = entities.AudioCodecsEntity(True)
        audio_codecs.init_by_supported_fields(flist)
        video_codecs = entities.VideoCodecsEntity(True)
        video_codecs.init_by_supported_fields(flist)
        create_request = CreateContactRequest(abonent_name, "", sip, h323, rtsp, "", 0, "", True, -1,
                                              audio_codecs, video_codecs, "", False)
        response = addrbook.create_new_contact(create_request)
        abid = response.contact.contact_id

    make_outgoing_call(confid, abid)  # call abonent by it's contact in address book


def call_abonent_by_id(confid, abid):
    aburi = addrbook.get_realaddr_by_ab_id(abid)

    ab_connected = check_active_abonent_uri(aburi)
    if ab_connected:
        raise AlreadyInSessionError("participant_id")

    if tools.is_correct_rtsp_address(aburi):
        flist = features.get_features_list()
        if not features.get_feature(flist, "RTSP"):
            raise FeatureRestrictedByLicenseError("RTSP")

    make_outgoing_call(confid, abid)


def hangup_abonent(confid, aburi):
    confid = int(confid)

    ab_connected = check_active_abonent_uri(aburi)
    if not ab_connected:
        raise SessionAbonentNotFoundError(aburi, "participant_id")

    # reset autocall counter
    dbtools.make_sql_request("UPDATE legacy.conferences SET callcount=0 WHERE id=%d" % (confid))

    commands_list = ["HANGUP PEER " + aburi]
    res = nettools.exec_cmd_list_s(commands_list)
    if not res:
        raise SendCmdToEngineError(commands_list)


def hangup_abonent_by_id(confid, abid):
    confid = int(confid)

    ab_uri = addrbook.get_realaddr_by_ab_id(abid)

    ab_connected = check_active_abonent_uri(ab_uri)
    if not ab_connected:
        raise SessionAbonentNotFoundError(ab_uri, "participant_id")

    hangup_abonent(confid, ab_uri)


def hangup_abonent_by_conn_id(confid, conn_id):
    confid = int(confid)
    conn_id = int(conn_id)

    ab_connected = check_active_abonent_conn_id(conn_id)
    if not ab_connected:
        raise SessionAbonentNotFoundError("#%d" % (conn_id), "participant_id")

    # reset autocall counter
    dbtools.make_sql_request("UPDATE legacy.conferences SET callcount=0 WHERE id=%d" % (confid))

    commands_list = ["HANGUP PEER #%d" % (conn_id)]
    res = nettools.exec_cmd_list_s(commands_list)
    if not res:
        raise SendCmdToEngineError(commands_list)


def hangup_group_abonents(request: HangupGroupAbonentsRequest) -> SessionGroupOperationResponse:
    """This function hangup group of participants from conference."""

    status = conftools.get_conference_status(request.conference_id)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    connstr, confstr = onlinetools.get_online_state()
    conn_list = onlinetools.extract_conn_list(connstr)
    processed_ab_count = 0
    failed_ab_count = 0
    for conn_id in request.connection_id_list:
        uri = onlinetools.get_realaddr_by_conn_id(conn_id, conn_list)
        if len(uri) != 0:
            processed_ab_count += 1
        else:
            failed_ab_count += 1

    # reset autocall counter
    dbtools.make_sql_request("UPDATE legacy.conferences SET callcount=0 WHERE id=%d" % (request.conference_id))

    commands_list = []
    for conn_id in request.connection_id_list:
        commands_list.append( "HANGUP PEER #%d" % (conn_id) )

    res = nettools.exec_cmd_list_s(commands_list)
    if not res:
        raise SendCmdToEngineError(commands_list)

    return SessionGroupOperationResponse(request.conference_id, processed_ab_count, failed_ab_count)


def move_participant_to_other_conference(conf_from: int, conf_to: int, abonent_uri: str, conn_id: int) \
        -> MoveToOtherConfResponse:
    """This function move abonent abonent_uri from conference conf_from to conference conf_to. Conference conf_from may be ==0."""

    if conf_from == None:
        conf_from = 0

    status = conftools.get_conference_status(conf_to)
    if not status["launched"]:
        raise ConferenceAlreadyStoppedError("conference_id")

    if abonent_uri == None or abonent_uri == "":
        raise SessionAbonentNotFoundError(abonent_uri, "participant_id")

    ab_connected = check_active_abonent_conn_id(conn_id)
    if not ab_connected:
        raise SessionAbonentNotFoundError(abonent_uri, "participant_id")

    abid = addrbook.get_ab_id_by_conn_id(conn_id)
    if abid == 0:
        abid = addrbook.get_abonent_id(abonent_uri)

    # if source conference has crown layout and we move crown participant into other conference
    # then turn off crown layout
    if conf_from != 0:
        connstr, confstr = onlinetools.get_online_state()
        conf_json = onlinetools.extract_conference(confstr, conf_from)
        if conf_json == {}:
            raise ActiveConferenceNotFoundError(conf_from, "conference_id")
        crown_abid = onlinetools.get_crown_participant(conf_from, conf_json)
        if crown_abid == abid:
            request = DeleteCrownRequest(conf_from)
            layoutcrown.delete_crown_from_participant(request, conf_json)

    # move abonent 'abonent_uri' from conference conf_from to conference conf_to
    on_event_tools.set_move_to_conf_prm(conf_from, conf_to, abid, abonent_uri, conn_id)
    on_event_tools.configure_abonent1(conf_to, abonent_uri, abid, conn_id)

    return MoveToOtherConfResponse(conf_from, conf_to, abonent_uri)
#!/usr/bin/python3


import logging
import sys
import urllib

import app.core.constants as constants
import app.core.customlayouttypes as customlayouttypes
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.nettools as nettools
import app.core.sysinfotools as sysinfotools
import app.core.tools as tools
from .entities import LayoutAbonentPositionEntity
from .entities import LayoutWindowCoordEntity
from .errors import AddAbonentsToLayoutError
from .errors import AddLayoutToConferenceError
from .errors import BadLayoutTypeError
from .errors import BadSchemaTypeError
from .errors import ConferenceNotFoundError
from .errors import CreateLayoutError
from .errors import DeleteLayoutAbonentsError
from .errors import DeleteLayoutError
from .errors import DeleteScheduledLayoutFromConferenceError
from .errors import EmptyLayoutOwnerError
from .errors import LayoutNotFoundError
from .errors import LayoutOfAbonentAlreadyExistError
from .errors import NoFieldInCalculatedLayoutError
from .errors import OldLayoutFormatError
from .errors import ParseLayoutCoordError
from .errors import SetLayoutModeError
from .errors import TooManyLayoutsError
from .errors import UpdateLayoutError
from .requests import BaseCreateLayoutRequest
from .requests import CalculateLayoutRequest
from .requests import ChangeSheduledLayoutRequest
from .requests import CreateGlobalLayoutRequest
from .responses import CalculateLayoutResponse
from .responses import DeleteSheduledLayoutResponse
from .responses import GetAllLayoutsTypesResponse
from .responses import SheduledLayoutAspeakerResponse
from .responses import SheduledLayoutGlobalResponse
from .responses import SheduledLayoutIndividualResponse

LAYOUT_TYPES_LIST = [
    {"layout_type": "equal", "cmd": "LAYOUT MODE EQUAL "},
    {"layout_type": "selected (r)", "cmd": "LAYOUT MODE RIGHT "},
    {"layout_type": "selected (b)", "cmd": "LAYOUT MODE BOTTOM "},
    {"layout_type": "selected (t)", "cmd": "LAYOUT MODE TOP "},
    {"layout_type": "selected (l)", "cmd": "LAYOUT MODE LEFT "},
    {"layout_type": "selected (tr)", "cmd": "LAYOUT MODE TR "},
    {"layout_type": "selected (rb)", "cmd": "LAYOUT MODE RB "},
    {"layout_type": "selected (rlr)", "cmd": "LAYOUT MODE RLR "},
    {"layout_type": "selected (btb)", "cmd": "LAYOUT MODE BTB "},
    {"layout_type": "selected (c)", "cmd": "LAYOUT MODE CENTER "},
    {"layout_type": "double (btb)", "cmd": "LAYOUT MODE DOUBLE BTB "},
    {"layout_type": "double (c)", "cmd": "LAYOUT MODE DOUBLE CENTER "}]

# index in array LAYOUT_TYPES_LIST
DEFAULT_EQUAL_TYPE = 0

# index in array LAYOUT_TYPES_LIST
DEFAULT_SELECTED_TYPE = 1


def get_layout_type_num(_json_lyt_type):
    """This function returns layout type (number) by '_json_lyt_type' form json file."""

    for index in range(len(LAYOUT_TYPES_LIST)):
        if LAYOUT_TYPES_LIST[index]["layout_type"] == _json_lyt_type:
            return index

    # error
    return DEFAULT_EQUAL_TYPE


def get_layout_cmd(_layout_type, _cid, _lid):
    """This function build command to set layout mode.
    _layout_type - type of layout
    _cid - conference id
    _lid - layout id
    Return: command string (at error return empty string)"""

    cmd = ""

    if _layout_type >= 0 and _layout_type < len(LAYOUT_TYPES_LIST):
        cmd = LAYOUT_TYPES_LIST[_layout_type]["cmd"] + _cid + " " + _lid

    return cmd


def get_layout_cmd_ex(_layout_type, _cid, _lid, custom_layout_types_list: list):
    """This function build command to set layout mode.
    _layout_type - type of layout (may be predefined or custom layout type)
    _cid - conference id
    _lid - layout id
    Return: command string (at error return empty string)"""

    if customlayouttypes.is_custom_layout_type(_layout_type):
        cmd = customlayouttypes.get_custom_layout_type_cmd2(custom_layout_types_list, _layout_type, _cid, _lid)
    else:
        cmd = get_layout_cmd(_layout_type, _cid, _lid)

    return cmd


def get_layout_schema_type(_layout_id):
    _layout_id = int(_layout_id)

    lyt_cfg_rows = dbtools.select("SELECT type FROM legacy.layoutconfigs WHERE layout_id=%d" % (_layout_id))
    if len(lyt_cfg_rows) != 0:
        return lyt_cfg_rows[0][0]

    return DEFAULT_EQUAL_TYPE  # error (no layout _layout_id in database)


LAYOUT_WINDOW_ROLE = ["EMPTY", "PEER", "ACTIVE", "AUTO", "PRESENTATION", "ROLL"]

DEFAULT_EMPTY_ROLE_ID = 0
DEFAULT_EMPTY_ROLE_NAME = LAYOUT_WINDOW_ROLE[DEFAULT_EMPTY_ROLE_ID]

DEFAULT_FILLED_ROLE_ID = 1
DEFAULT_FILLED_ROLE_NAME = LAYOUT_WINDOW_ROLE[DEFAULT_FILLED_ROLE_ID]

ACTIVE_ROLE_ID = 2
ACTIVE_ROLE_NAME = LAYOUT_WINDOW_ROLE[ACTIVE_ROLE_ID]

AUTO_ROLE_ID = 3
AUTO_ROLE_NAME = LAYOUT_WINDOW_ROLE[AUTO_ROLE_ID]

PRESENTATION_ROLE_ID = 4
PRESENTATION_ROLE_NAME = LAYOUT_WINDOW_ROLE[PRESENTATION_ROLE_ID]

ROLL_ROLE_ID = 5
ROLL_ROLE_NAME = LAYOUT_WINDOW_ROLE[ROLL_ROLE_ID]


def role_name_to_id(_role_name: str) -> int:
    for index in range(len(LAYOUT_WINDOW_ROLE)):
        if LAYOUT_WINDOW_ROLE[index] == _role_name:
            return index
    return DEFAULT_EMPTY_ROLE_ID


def role_id_to_name(_role_id: int) -> str:
    if _role_id >= 0 and _role_id < len(LAYOUT_WINDOW_ROLE):
        return LAYOUT_WINDOW_ROLE[_role_id]
    else:
        return DEFAULT_EMPTY_ROLE_NAME


def role_cmd(_role_id: int, _conf_name: str, _layout_name: str, _position: int, _participant_uri: str):
    role_name = role_id_to_name(_role_id)

    if role_name == DEFAULT_FILLED_ROLE_NAME and (_participant_uri == None or _participant_uri == ""):
        logging.error("role_cmd() error: participant URI is not set for PEER role of slot")
        role_name = DEFAULT_EMPTY_ROLE_NAME

    if role_name == DEFAULT_FILLED_ROLE_NAME:
        cmd = "LAYOUT MAP %s %s %s %d %s" % (role_name, _conf_name, _layout_name, _position, _participant_uri)
    else:
        cmd = "LAYOUT MAP %s %s %s %d" % (role_name, _conf_name, _layout_name, _position)
    return cmd


def is_role_not_fixed_peer(_role_id: int) -> bool:
    if _role_id == ACTIVE_ROLE_ID or _role_id == AUTO_ROLE_ID or _role_id == ROLL_ROLE_ID:
        return True
    else:
        return False


def get_layout_types():
    """This function return list of all layout types."""

    # OLD: global g_layout_types

    result_list = []

    # for type_row in g_layout_types:
    #    cur_row = copy.deepcopy(type_row)
    #
    #    types_list = []
    #    for cur_type in cur_row["tlist"]:
    #        types_list.append( LayoutTypeEntity(cur_type["type"], cur_type["name"], cur_type["picture"]) )
    #
    #    result_list.append( LayoutRowEntity(cur_row["num_ab"], types_list) )

    return (GetAllLayoutsTypesResponse(result_list))


def get_aspeaker_flag(confid):
    confid = int(confid)
    aspeaker_rows = dbtools.select(
        "SELECT aspeaker FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            confid))
    if len(aspeaker_rows) != 0:
        aspeaker = aspeaker_rows[0][0]
    else:
        aspeaker = 0
    return aspeaker


def get_asp2lyt_flag(confid):
    confid = int(confid)
    asp2lyt_rows = dbtools.select(
        "SELECT asp2lyt FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            confid))
    if len(asp2lyt_rows) != 0:
        asp2lyt = asp2lyt_rows[0][0]
    else:
        asp2lyt = 0
    return (asp2lyt)


def get_aspeaker_2lyt_flags(confid):
    confid = int(confid)
    aspeaker_rows = dbtools.select(
        "SELECT aspeaker, asp2lyt FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            confid))
    if len(aspeaker_rows) != 0:
        aspeaker = aspeaker_rows[0][0]
        asp2lyt = aspeaker_rows[0][1]
    else:
        aspeaker = 0
        asp2lyt = 0
    return aspeaker, asp2lyt


def get_layout_id_by_type(confid: int, layout_type: int, template: bool) -> int:
    """Get single copy of layout of specified type"""
    confid = int(confid)
    layout_type = int(layout_type)
    template_num = tools.bool_to_int(template)
    layout_rows = dbtools.select("""SELECT id FROM legacy.layouts WHERE layout_type=%d AND template=%d AND id IN 
                                   (SELECT id FROM legacy.layoutconferences WHERE confid=%d)""" %
                                 (layout_type, template_num, confid))
    if len(layout_rows) == 0:
        return (0)  # error: bad global layout id
    layout_id = layout_rows[0][0]
    return layout_id


#OLD:
#def get_global_layout_id(confid: int) -> int:
#    confid = int(confid)
#    layout_rows = dbtools.select("SELECT id FROM legacy.layoutconferences WHERE confid=%d and abid=0" % (confid))
#    if len(layout_rows) == 0:
#        return (0)  # error: bad global layout id
#    layout_id = layout_rows[0][0]
#    return layout_id

def get_global_layout_id(confid: int, template: bool) -> int:
    return get_layout_id_by_type(confid, data.LAYOUT_TYPE_GLOBAL, template)


def get_individual_layout_id(confid: int, abid: int, template: bool) -> int:
    confid = int(confid)
    abid = int(abid)
    template_num = tools.bool_to_int(template)
    layout_rows = dbtools.select("""SELECT id FROM legacy.layouts WHERE layout_type=%d AND template=%d AND id IN 
                                   (SELECT id FROM legacy.layoutconferences WHERE confid=%d AND abid=%d)""" %
                                 (data.LAYOUT_TYPE_INDIVIDUAL, template_num, confid, abid))
    if len(layout_rows) == 0:
        return (0)  # error: bad individual layout id
    layout_id = layout_rows[0][0]
    return layout_id


def get_layouts_id_of_conf(confid: int) -> list:
    """This function returns id of all layouts of conference 'confid'."""
    lyt_id_list = []
    lyt_id_rows = dbtools.select("SELECT id FROM legacy.layoutconferences WHERE confid=%d" % (confid))
    for lyt in lyt_id_rows:
        layout_id = lyt[0]
        lyt_id_list.append(layout_id)
    return lyt_id_list


def get_template_layouts_id() -> list:
    """This function returns id of all layout templates."""
    lyt_id_list = []
    lyt_id_rows = dbtools.select("SELECT id FROM legacy.layouts WHERE template<>0")
    for lyt in lyt_id_rows:
        layout_id = lyt[0]
        lyt_id_list.append(layout_id)
    return lyt_id_list


def get_layout_by_id(layout_id):
    """This function build json with layout 'layout_id' of conference 'confid'."""
    layout_id = int(layout_id)

    lyt_rows = dbtools.select("SELECT auto, dictors_cnt, layout_type, name, template FROM legacy.layouts WHERE id=%d" % (layout_id))
    if len(lyt_rows) != 0:
        auto = tools.int_to_bool(lyt_rows[0][0])
        cells_count = lyt_rows[0][1]
        layout_type = lyt_rows[0][2]
        display_name = lyt_rows[0][3]
        template = tools.int_to_bool(lyt_rows[0][4])
    else:
        raise LayoutNotFoundError(layout_id, "layout_id")

    # get conference id / owner of layout
    if template:
        owner_id = 0 # tamplate is not in conference and has not owner
    else:
        conf_rows = dbtools.select("SELECT DISTINCT abid FROM legacy.layoutconferences WHERE id=%d" % (layout_id))
        if len(conf_rows) == 0:
            raise ConferenceNotFoundError(0, "conference")
        owner_id = conf_rows[0][0]

    # get active speaker flag
    # aspeaker = tools.int_to_bool( get_aspeaker_flag(confid) )

    num_ab = 0
    prev_num_ab = -1
    schema_type = 0
    slots_list = []
    lyt_cfg_rows = dbtools.select("""SELECT num_ab, type, priority, wnd_num, abid, role, render_mode 
    FROM legacy.layoutconfigs WHERE layout_id=%d ORDER BY num_ab, wnd_num ASC""" % (layout_id))
    for i in range(len(lyt_cfg_rows)):
        num_ab = lyt_cfg_rows[i][0]
        schema_type = lyt_cfg_rows[i][1]
        priority = lyt_cfg_rows[i][2]
        wnd_num = lyt_cfg_rows[i][3]
        cur_ab_id = tools.db_num_to_id(lyt_cfg_rows[i][4])
        role = lyt_cfg_rows[i][5]
        render_mode = data.get_render_participant_name(lyt_cfg_rows[i][6])

        if i != 0 and prev_num_ab != num_ab:
            raise OldLayoutFormatError(layout_id)

        slots_list.append(LayoutAbonentPositionEntity(wnd_num, cur_ab_id, priority, role, render_mode))
        prev_num_ab = num_ab

    if (not customlayouttypes.is_custom_layout_type(schema_type)) and \
        (schema_type < 0 or schema_type >= len(LAYOUT_TYPES_LIST)):
        raise BadSchemaTypeError(schema_type, "grid.type")

    # if len(slots_list) == 0:
    #    raise EmptyLayoutSlotsListError(layout_id)

    if layout_type == data.LAYOUT_TYPE_GLOBAL:
        result = SheduledLayoutGlobalResponse(layout_id, auto, cells_count, schema_type, slots_list, display_name,
                                              template)
    elif layout_type == data.LAYOUT_TYPE_INDIVIDUAL:
        result = SheduledLayoutIndividualResponse(layout_id, auto, cells_count, schema_type, slots_list, owner_id,
                                                  display_name, template)
    elif layout_type == data.LAYOUT_TYPE_ASPEAKER:
        result = SheduledLayoutAspeakerResponse(layout_id, auto, cells_count, schema_type, slots_list, display_name,
                                                template)
    else:
        raise BadLayoutTypeError(layout_type, "type")

    return (result)


def get_layouts_list(confid: int, template: bool):
    confid = int(confid)

    if template:
        lyt_id_list = get_template_layouts_id() # get id of all layout templates
    else:
        lyt_id_list = get_layouts_id_of_conf(confid) # get id of all scheduled layouts of conference 'confid'
    if len(lyt_id_list) == 0:
        return []

    layouts_list = []
    for layout_id in lyt_id_list:
        layouts_list.append(get_layout_by_id(layout_id))

    return layouts_list


def get_layouts_id_list_str(confid):
    confid = int(confid)

    # get id of all scheduled layouts of conference 'confid'
    lyt_id_list = get_layouts_id_of_conf(confid)
    if len(lyt_id_list) == 0:
        return ""

    lyt_id_list_str = "("
    for i in range(len(lyt_id_list)):
        lyt_id = lyt_id_list[i]
        lyt_id_list_str += "%d" % (lyt_id)
        if i != len(lyt_id_list) - 1:
            lyt_id_list_str += ","

    lyt_id_list_str += ")"
    return lyt_id_list_str


def create_layout(request: BaseCreateLayoutRequest):
    """This function create layout for 'abonent' and fill it by participants of conference 'confid'."""
    confid = int(request.confid)

    # check conference (layout template is not in conference)
    if not request.template:
        conf_rows = dbtools.select("SELECT id FROM legacy.conferences WHERE id=%d AND deleted_at is null" % (confid))
        if len(conf_rows) == 0:
            raise ConferenceNotFoundError(confid, "conference_id")

    # aspeaker = get_aspeaker_flag(confid)
    layout_type_ind = data.get_layout_type_ind(request.layout_type)

    if request.template:
        layout_type_ind = layout_type_ind
        #TODO?:layout_type_ind = data.LAYOUT_TYPE_GLOBAL
    else:
        # check layout owner and count of created layouts
        if layout_type_ind == data.LAYOUT_TYPE_INDIVIDUAL:
            if request.participant_id == None or request.participant_id == 0:
                raise EmptyLayoutOwnerError()

            lyt_id = get_individual_layout_id(confid, request.participant_id, False)
            if lyt_id != 0:
                raise LayoutOfAbonentAlreadyExistError(request.participant_id)

            lyt_cnt_rows = dbtools.select(
                "SELECT count(*) FROM legacy.layoutconferences WHERE confid=%d and abid<>0" % (confid))
            lyt_cnt = lyt_cnt_rows[0][0]
            lax_layouts_cnt = sysinfotools.get_abonents_restriction()["max_layouts"]
            if lyt_cnt >= lax_layouts_cnt - 1:
                logging.error("create_layout(): error: too many layouts")
                raise TooManyLayoutsError()
        elif layout_type_ind == data.LAYOUT_TYPE_GLOBAL:
            # check existing global layout
            glb_lyt_id = get_global_layout_id(confid, False)
            if glb_lyt_id != 0:
                raise LayoutOfAbonentAlreadyExistError(0)
        elif layout_type_ind == data.LAYOUT_TYPE_ASPEAKER:
            # check existing aspeaker layout
            lyt_id = get_layout_id_by_type(confid, layout_type_ind, False)
            if lyt_id != 0:
                raise LayoutOfAbonentAlreadyExistError(-1)
        else:
            raise BadLayoutTypeError(layout_type_ind, "type")

    # check abonents of layout
    # if request.wndlist == None or len(request.wndlist) == 0:
    #    raise EmptyLayoutSlotsListError(0)

    # OLD:
    # get new layout id
    # layout_id = dbtools.generate_unique_fld("layouts", "id")
    # if layout_id == 0:
    #    raise GenerateLayoutIDError()

    auto_num = tools.bool_to_int(request.auto)

    display_name = tools.str_to_dbstr(request.display_name)
    template_num = tools.bool_to_int(request.template)

    ab_cnt = request.cells_count

    # create layout
    sql = """INSERT INTO legacy.layouts 
            (name, fix_type, fix_abonent, dictors_cnt, aspeaker, maxab, auto, layout_type, template) 
            VALUES ('%s', 0, 0, %d, 0, %d, %d, %d, %d) RETURNING id""" % \
          (display_name, ab_cnt, ab_cnt, auto_num, layout_type_ind, template_num)
    layout_id = dbtools.insert_with_serial(sql)
    if layout_id == 0:
        logging.error("create_layout() create layout failed: " + dbtools.get_last_error())
        raise CreateLayoutError()

    # add layout to conference 'confid' (layout template is not in conference)
    if not request.template:
        if layout_type_ind == data.LAYOUT_TYPE_INDIVIDUAL:
            owner_id = request.participant_id
        else:
            owner_id = 0
        sql = """INSERT INTO legacy.layoutconferences (id, confid, abid) VALUES (%d, %d, %d)""" % (
            layout_id, confid, owner_id)
        result = dbtools.make_sql_request(sql)
        if result == 0:
            logging.error("create_layout() add layout to conference failed: " + dbtools.get_last_error())
            raise AddLayoutToConferenceError(layout_id, confid, owner_id)

    # check schema type
    if (not customlayouttypes.is_custom_layout_type(request.schema_type)) and \
        (request.schema_type < 0 or request.schema_type >= len(LAYOUT_TYPES_LIST)):
        raise BadSchemaTypeError(request.schema_type, "grid.type")

    # add abonents to layout
    for i in range(len(request.wndlist)):
        wnd_num = request.wndlist[i].wndnum
        cur_ab_id = tools.id_to_db_num(request.wndlist[i].abid)
        priority = request.wndlist[i].priority
        role = request.wndlist[i].role_id
        render_mode_ind = data.get_render_participant_index(request.wndlist[i].render_mode)
        sql = """INSERT INTO legacy.layoutconfigs (layout_id, num_ab, type, priority, wnd_num, abid, role, render_mode) 
        VALUES (%d, %d, %d, %d, %d, %d, %d, %d)""" % \
        (layout_id, ab_cnt, request.schema_type, priority, wnd_num, cur_ab_id, role, render_mode_ind)
        result = dbtools.make_sql_request(sql)
        if result == 0:
            logging.error("create_layout() add abonents to layout failed (2): " + dbtools.get_last_error())
            raise AddAbonentsToLayoutError(cur_ab_id, layout_id)

    # return created layout
    result = get_layout_by_id(layout_id)
    return (result)


def update_layout(request: ChangeSheduledLayoutRequest):
    """This function update one 'row' of layout by json data '_lyt_obj'."""

    layout_id = int(request.layout_id)

    # check layout
    lyt_rows = dbtools.select("SELECT id FROM legacy.layouts WHERE id=%d" % (layout_id))
    if len(lyt_rows) == 0:
        raise LayoutNotFoundError(layout_id, "layout_id")

    # get old layout
    old_layout = get_layout_by_id(layout_id)

    if request.auto != None:
        auto_num = tools.bool_to_int(request.auto)
    else:
        auto_num = tools.bool_to_int(old_layout.auto)

    if request.display_name != None:
        display_name = tools.str_to_dbstr(request.display_name)
    else:
        display_name = tools.str_to_dbstr(old_layout.display_name)

    if request.cells_count != None:
        cells_count = request.cells_count
    else:
        cells_count = old_layout.cells_count

    if request.schema_type != None:
        schema_type = request.schema_type
    else:
        schema_type = old_layout.schema_type

    # check schema type
    if (not customlayouttypes.is_custom_layout_type(request.schema_type)) and \
        (schema_type < 0 or schema_type >= len(LAYOUT_TYPES_LIST)):
        raise BadSchemaTypeError(schema_type, "grid.type")

    # update flags of layout
    res = dbtools.make_sql_request("""UPDATE legacy.layouts SET dictors_cnt=%d, auto=%d, name='%s' WHERE id=%d""" %
                                   (cells_count, auto_num, display_name, layout_id))
    if res == 0:
        logging.error("update_layout_by_json() cannot update layout: " + dbtools.get_last_error())
        raise UpdateLayoutError(layout_id)

    # update abonents of layout
    if request.wndlist != None:  # OLD: and len(request.wndlist) != 0:
        # delete old rows of layout
        res = dbtools.make_sql_request("DELETE FROM legacy.layoutconfigs WHERE layout_id=%d" % (layout_id))
        if res == 0:
            logging.error("update_layout_by_json() cannot delete abonents of layout:" + dbtools.get_last_error())
            logging.error("layoutid=%d" % (layout_id))
            raise DeleteLayoutAbonentsError(layout_id)

        # add new rows
        for wnd in request.wndlist:
            abid = tools.id_to_db_num(wnd.abid)
            render_mode_ind = data.get_render_participant_index(wnd.render_mode)
            res = dbtools.make_sql_request("""INSERT INTO legacy.layoutconfigs (layout_id, num_ab, type, priority, 
            wnd_num, abid, role, render_mode) VALUES(%d, %d, %d, %d, %d, %d, %d, %d)""" %
            (layout_id, cells_count, schema_type, wnd.priority, wnd.wndnum, abid, wnd.role_id, render_mode_ind))
            if res == 0:
                logging.error("update_layout_by_json() cannot update layout (2): " + dbtools.get_last_error())
                raise UpdateLayoutError(layout_id)

    # return updated layout
    result = get_layout_by_id(layout_id)
    return (result)


def delete_layout(_layout_id):
    _layout_id = int(_layout_id)

    template = False
    lyt_rows = dbtools.select("SELECT template FROM legacy.layouts WHERE id=%d" % (_layout_id))
    if len(lyt_rows) == 0:
        raise LayoutNotFoundError(_layout_id, "layout_id")
    else:
        template = tools.int_to_bool(lyt_rows[0][0])

    # get conference id
    if not template:
        conf_rows = dbtools.select("SELECT DISTINCT confid FROM legacy.layoutconferences WHERE id=%d" % (_layout_id))
        if len(conf_rows) == 0:
            logging.error("delete_layout(): conference is not found: " + dbtools.get_last_error())
            raise ConferenceNotFoundError(0, "conference")

    # OLD:
    # res = dbtools.make_sql_request( "DELETE FROM layoutabonents WHERE layout_id=%d" % (_layout_id) )
    # if res == 0:
    #    logging.error("delete_layout(): deleting of abonents failed: " + dbtools.get_last_error())
    #    raise DeleteLayoutAbonentsError(_layout_id)

    res = dbtools.make_sql_request("DELETE FROM legacy.layoutconfigs WHERE layout_id=%d" % (_layout_id))
    if res == 0:
        logging.error("delete_layout(): deleting of abonents failed(2): " + dbtools.get_last_error())
        raise DeleteLayoutAbonentsError(_layout_id)

    res = dbtools.make_sql_request("DELETE FROM legacy.layoutconferences WHERE id=%d" % (_layout_id))
    if res == 0:
        logging.error("delete_layout(): deleting layout form conference failed: " + dbtools.get_last_error())
        raise DeleteScheduledLayoutFromConferenceError(_layout_id)

    res = dbtools.make_sql_request("DELETE FROM legacy.layouts WHERE id=%d" % (_layout_id))
    if res == 0:
        logging.error("delete_layout(): deleting layout failed: " + dbtools.get_last_error())
        raise DeleteLayoutError(_layout_id)

    return (DeleteSheduledLayoutResponse(_layout_id))


def layout_mode_to_aspeaker(_layout_mode: str):
    if _layout_mode == "aspeaker":
        return True
    else:
        return False


def aspeaker_to_layout_mode(_aspeaker: bool):
    if _aspeaker:
        return "aspeaker"
    else:
        return "common"


def delete_all_layouts_of_conference(_confid):
    _confid = int(_confid)
    lyt_id_list = get_layouts_id_of_conf(_confid)
    for lyt_id in lyt_id_list:
        delete_layout(lyt_id)


def create_default_global_layout(_confid: int, _aspeaker: bool, _auto: bool):
    # check existing of global layout
    glb_lyt_id = get_global_layout_id(_confid, False)
    if glb_lyt_id != 0:
        raise LayoutOfAbonentAlreadyExistError(0)  # error: global layout is already exist

    # get list of all abonents of conference _confid
    #conf_ab_rows = dbtools.select("SELECT abid FROM legacy.confabonents WHERE confid=%d" % (_confid))
    #conf_ab_cnt = len(conf_ab_rows)
    conf_ab_cnt = 0

    # get schema_type of layout
    if _aspeaker:
        schema_type = DEFAULT_SELECTED_TYPE
    else:
        schema_type = DEFAULT_EQUAL_TYPE

    # build list of abonents of layout
    wndlist = []
    #for i in range(conf_ab_cnt):
    #    position = i + 1
    #    cur_ab_id = conf_ab_rows[i][0]
    #    wndlist.append(LayoutAbonentPositionEntity(position, cur_ab_id, data.PRIORITY_NORMAL, DEFAULT_FILLED_ROLE_ID))

    # create global layout
    #b_auto_layout = _auto
    #if conf_ab_cnt == 0:
    #    b_auto_layout = True
    b_auto_layout = True
    request = CreateGlobalLayoutRequest(_confid, b_auto_layout, conf_ab_cnt, schema_type, wndlist, "", False)
    layout = create_layout(request)

    # set aspeaker flag
    aspeaker_num = tools.bool_to_int(_aspeaker)
    res = dbtools.make_sql_request(
        "UPDATE legacy.configurations set aspeaker=%d WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            aspeaker_num, _confid))
    if res == 0:
        raise SetLayoutModeError(_confid, _aspeaker)

    return layout


def switch_layout_mode(_confid: int, _layout_mode: str):  # _aspeaker: bool
    if _layout_mode == None:
        return

    old_aspeaker_flag = tools.int_to_bool(get_aspeaker_flag(_confid))
    new_aspeaker_flag = layout_mode_to_aspeaker(_layout_mode)

    if old_aspeaker_flag != new_aspeaker_flag:
        # check count of abonents of conference _confid
        # conf_ab_rows = dbtools.select("SELECT abid FROM legacy.confabonents WHERE confid=%d" % (_confid))
        # if len(conf_ab_rows) == 0:
        #    raise EmptyLayoutSlotsListError(0)  # error: no abonents in conference (layout cannot be empty)

        delete_all_layouts_of_conference(_confid)
        auto = new_aspeaker_flag
        create_default_global_layout(_confid, new_aspeaker_flag, auto)


def get_layout_wnd_coord(request: CalculateLayoutRequest) -> CalculateLayoutResponse:
    """ This function calculate layout slots coordinates. """

    if request.schema_type >= 0 and request.schema_type < len(LAYOUT_TYPES_LIST):
        type_str = LAYOUT_TYPES_LIST[request.schema_type]["layout_type"]
    else:
        type_str = LAYOUT_TYPES_LIST[0]["layout_type"]

    type_quoted = urllib.parse.quote(type_str)

    pip_num = tools.bool_to_int(request.pip)

    request_str = 'layout_grid.json?size=%d&width=%d&height=%d&pip=%d&type=%s' % (
    request.cells_count, request.width, request.height, pip_num, type_quoted)

    layout_str = nettools.get_url_data_s(request_str, True)

    layout_json = {}
    try:
        true = True  # define constants "true"/"false"
        false = False
        layout_json = eval(layout_str)  # convert string to python expression (json format)
    except:
        logging.error("get_layout_wnd_coord() error: " + str(sys.exc_info()))
        logging.error("get_layout_wnd_coord() layout_grid.json: " + layout_str)
        raise ParseLayoutCoordError()

    if "layout_grid" not in layout_json:
        raise NoFieldInCalculatedLayoutError("layout_grid")
    if "grid" not in layout_json["layout_grid"]:
        raise NoFieldInCalculatedLayoutError("grid")
    wnd_list_json = layout_json["layout_grid"]["grid"]

    result_wnd_list = []
    for wnd in wnd_list_json:
        result_wnd_list.append(
            LayoutWindowCoordEntity(wnd["pos"], wnd["x"], wnd["y"], wnd["width"], wnd["height"], wnd["z_order"]))

    return CalculateLayoutResponse(result_wnd_list)


def aspeaker_cmd(_confid, _aspeaker_on):
    """This function returns command to turn on/off sapeaker mode."""
    if _aspeaker_on:
        asp_str = "ON"
    else:
        asp_str = "OFF"

    cid = tools.conference_name_by_id(_confid)

    return "SET ASPEAKER %s %s" % (asp_str, cid)


def is_show_audio_power(_confid: int) -> bool:
    conf_rows = dbtools.select(
        "SELECT show_aud_pwr FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            _confid))
    if len(conf_rows) != 0:
        return tools.int_to_bool(conf_rows[0][0])
    else:
        return False


def is_show_names(_confid: int) -> bool:
    conf_rows = dbtools.select(
        "SELECT show_names FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            _confid))
    if len(conf_rows) != 0:
        return tools.int_to_bool(conf_rows[0][0])
    else:
        return False


def get_conf_background_ind(_confid) -> int:
    conf_rows = dbtools.select(
        "SELECT background FROM legacy.configurations WHERE id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (
            _confid))
    if len(conf_rows) != 0:
        return conf_rows[0][0]
    else:
        return data.BACKGROUND_OFF


def register_online_layout(confid: int, layout_name: str, auto: bool, cmd_list: list):
    """This function update commands list 'cmd_list' to register layout 'layout_name' in conference 'confid'."""

    frm = tools.get_frame_params(confid)

    show_audio_power = is_show_audio_power(confid)
    show_names = is_show_names(confid)
    background_ind = get_conf_background_ind(confid)
    background_base_cmd = data.get_background(background_ind)["cmd2"]

    cid = tools.conference_name_by_id(confid)

    cmd_list.append("LAYOUT REGISTER " + cid + " " + layout_name)
    cmd_list.append("LAYOUT FRAME SIZE " + cid + " " + layout_name + " " + str(frm["width"]) + " " + str(frm["height"]))
    cmd_list.append("LAYOUT FRAME RATE " + cid + " " + layout_name + " " + str(frm["fps"]))
    cmd_list.append(background_base_cmd + " " + cid + " " + layout_name)
    cmd_list.append("LAYOUT LOUDNESS %s %s %s" % (tools.num_to_onoff(show_audio_power), cid, layout_name))
    cmd_list.append("LAYOUT DN %s %s %s" % (tools.num_to_onoff(show_names), cid, layout_name))
    cmd_list.append("LAYOUT PIP OFF %s %s" % (cid, layout_name))
    if auto:
        cmd_list.append("LAYOUT KEEP ON %s %s" % (cid, layout_name))


def set_layout_restrict(cid: str, layout_name: str, cells_count: int, cmd_list: list):
    """This function set restriction 'cells_count' for layout 'layout_name'."""

    cmd_list.append("LAYOUT RESTRICT " + cid + " " + layout_name + " " + str(cells_count))
    cmd_list.append("LAYOUT MAP INIT " + cid + " " + layout_name)
    cmd_list.append("LAYOUT MAP APPLY " + cid + " " + layout_name)


def set_special_layout(cid: str, layout_name: str, special: bool, cmd_list: list):
    """Add to 'cmd_list' commands to make layout special (for active speaker).
    Make it after 'LAYOUT RESTRICT' only!"""
    spec_on_off = tools.num_to_onoff(special)
    cmd_list.append("LAYOUT SPECIAL "  + spec_on_off + " " + cid + " " + layout_name)


def turn_off_vad_in_all_layouts():
    dbtools.make_sql_request("UPDATE legacy.layoutconfigs SET role=%d WHERE role=%d" %
                             (DEFAULT_EMPTY_ROLE_ID, ACTIVE_ROLE_ID))


def get_layout_render_mode_cmd(cid: str, lid: str, position: int, render_mode_index: int) -> str:
    if render_mode_index == data.RENDER_PARTICIPANT_FILL:
        mode = "FILL"
    elif render_mode_index == data.RENDER_PARTICIPANT_FULL:
        mode = "FULL"
    else:
        mode = "DEFAULT"
    return "LAYOUT MAP OPTION %s %s %s %d" % (mode, cid, lid, position)



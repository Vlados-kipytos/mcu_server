#!/usr/bin/python3

import logging
from datetime import datetime
from datetime import timedelta

from OpenSSL import crypto

import app.core.cert_functions as cert_func


def YYYYMMDDhhmmssZ_to_datetime(str_data):
    return datetime(int(str_data[:4]), int(str_data[4:6]), int(str_data[6:8]), int(str_data[8:10]),
                    int(str_data[10:12]), int(str_data[12:14]))


"""
Parameters:
    cert_file           - file-certificate (.crt)
"""


def check_cert(cert_file):
    try:
        with open(cert_file, 'rb') as der_file:
            x509 = crypto.load_certificate(crypto.FILETYPE_PEM, der_file.read())
    except Exception:
        return cert_func.NO_CERT_FILE

    start_cert = x509.get_notBefore()
    end_cert = x509.get_notAfter()

    cert_func.log_msg("start_cert: %s" % start_cert)
    cert_func.log_msg("end_cert: %s" % end_cert)

    now = datetime.now()
    end = YYYYMMDDhhmmssZ_to_datetime(end_cert)
    start = YYYYMMDDhhmmssZ_to_datetime(start_cert)

    DAYS_TO_EXPIRATION = 30.0

    if (now <= end) and (now >= start):
        logging.debug("Certificate is OK")
        if now.year == end.year and now.month == end.month and now.day == end.day:
            logging.debug("But today is a last day of work")
            return cert_func.LAST_DAY_OF_CERT
        elif end - now < timedelta(days=DAYS_TO_EXPIRATION):
            logging.debug("But it will expired in %f days" % (DAYS_TO_EXPIRATION))
            return cert_func.CERT_WILL_EXPIRE_SOON
        else:
            logging.debug("Correct cert")
            return cert_func.CORRECT_CERT
    else:
        logging.debug("Certificate expired!")
        return cert_func.BAD_CERT

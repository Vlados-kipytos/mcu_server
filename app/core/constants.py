#!/usr/bin/python3

import sys

import app.core.xmltools as xmltools

# read script's params from ctrlmahine.xml
ctrl_params = xmltools.get_config()



MAX_ACTIVE_ABONENTS = ctrl_params["abonents"]["maxactiveab"]
# MAX_LOADING  = 1280 * 720 * 30 * MAX_ACTIVE_ABONENTS # width*height*FPS*num_abonents
MAX_ABONENTS = ctrl_params["abonents"]["maxab"]
MAX_LAYOUTS = ctrl_params["abonents"]["maxlayouts"]
USE_PIN = False

MAX_DEMO_CONFERENCES = 4

MAX_AUTOCALLS = 100

DIGEST_REALM = "Digest"
HTDIGEST = "/var/www/.htdusers"

# if this parameter != 0 , then use Astra Linux Special Edition (else Common Edition)
SPECIAL_EDITION = ctrl_params["osversion"]["se"]
DEMO_VERSION = ctrl_params["osversion"]["demo"]

USE_CMD_LOGS = 1

# count of records in one page
PAGE_SIZE = 25



HW_INFO_XML_FNAME = "sysdata.xml"

#!/usr/bin/python3

import logging

import ldap3

import app.core.constants as constants
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.nettools as nettools
import app.core.tools as tools
from app.core.entities import DBSettingsEntity
from app.core.entities import EmailSettingsEntity
from app.core.entities import LDAPSettingsEntity
from app.core.entities import RTPEntity
from app.core.entities import VideoSettingsEntity
from app.core.errors import ChangeSettingsError
from app.core.errors import GetSettingsError


def get_video_settings():
    """This function get profile calls's parameters"""

    settings = dbtools.select("""SELECT maxvideosz, quality1, quality2, bandwidth, 
    adaptrate, ratectrl, fec, background, rolltimeout FROM legacy.commonconfig""")

    video_storage_size = settings[0][0]
    stream1_priority = data.get_stream_quality(settings[0][1])["name"].lower()
    stream2_priority = data.get_stream_quality(settings[0][2])["name"].lower()
    content_wide_band = data.get_bandwidth_name_by_value(settings[0][3])
    adapt_bitrate = tools.int_to_bool(settings[0][4])
    stream_control = tools.int_to_bool(settings[0][5])
    packet_recovery = tools.int_to_bool(settings[0][6])
    background = data.get_background(settings[0][7])["name"]
    rolltimeout = settings[0][8]

    return VideoSettingsEntity(video_storage_size, stream1_priority, stream2_priority, content_wide_band,
                               adapt_bitrate, stream_control, packet_recovery, background, rolltimeout)


def get_video_settings_commands(_cmd_list: list):
    commonconfigs = dbtools.select("""SELECT quality1, quality2, bandwidth, adaptrate, ratectrl, fec, background,
                                   rolltimeout FROM legacy.commonconfig""")
    if len(commonconfigs) != 0:
        quality1 = commonconfigs[0][0]
        quality2 = commonconfigs[0][1]
        bandwidth = commonconfigs[0][2]
        _cmd_list.append(data.get_stream_quality(quality1)["cmd"])
        _cmd_list.append(data.get_stream_quality(quality2)["cmd2"])
        _cmd_list.append("PRESENTATION BANDWIDTH %d" % (bandwidth))

        _cmd_list.append("SET ADAPTRATE " + tools.num_to_onoff(commonconfigs[0][3]))
        _cmd_list.append("SET RATECTRL " + tools.num_to_onoff(commonconfigs[0][4]))
        _cmd_list.append("SET FEC " + tools.num_to_onoff(commonconfigs[0][5]))

        bk_cmd = data.get_background(commonconfigs[0][6])["cmd"]
        _cmd_list.append(bk_cmd)

        _cmd_list.append("SET ROTATE TIMEOUT %d" % (commonconfigs[0][7]))


def set_video_settings(new_video: VideoSettingsEntity):
    updating_params = []
    if new_video.video_storage_size is not None:
        updating_params.append("maxvideosz=%d" % new_video.video_storage_size)

    if new_video.stream1_priority is not None:
        updating_params.append("quality1=%d" % data.get_stream_quality_index(new_video.stream1_priority.upper()))

    if new_video.stream2_priority is not None:
        updating_params.append("quality2=%d" % data.get_stream_quality_index(new_video.stream2_priority.upper()))

    if new_video.content_wide_band is not None:
        updating_params.append(
            "bandwidth=%d" % data.get_bandwidth(data.get_bandwidth_index(new_video.content_wide_band))["bandwidth"])

    if new_video.adapt_bitrate is not None:
        updating_params.append("adaptrate=%d" % tools.bool_to_int(new_video.adapt_bitrate))

    if new_video.stream_control is not None:
        updating_params.append("ratectrl=%d" % tools.bool_to_int(new_video.stream_control))

    if new_video.packet_recovery is not None:
        updating_params.append("fec=%d" % tools.bool_to_int(new_video.packet_recovery))

    if new_video.background is not None:
        updating_params.append("background=%d" % data.get_background_ind_by_name(new_video.background))

    if new_video.rolltimeout is not None:
        updating_params.append("rolltimeout=%d" % (new_video.rolltimeout))

    if len(updating_params) == 0:
        return

    sql = """UPDATE legacy.commonconfig SET """
    for i in range(len(updating_params) - 1):
        sql += updating_params[i] + ", "
    sql += updating_params[-1]

    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("cannot update settings: %s " % (dbtools.get_last_error()))
        raise ChangeSettingsError()

    cmd_list = []
    get_video_settings_commands(cmd_list)
    nettools.exec_cmd_list_s(cmd_list)


def get_video_storage_size():
    video_rows = dbtools.select("SELECT maxvideosz FROM legacy.commonconfig")
    if len(video_rows) != 0:
        maxvideosz = video_rows[0][0]
        return (maxvideosz)
    else:
        return (0)


def is_ldap_server_online():
    """This function check network state of LDAP server (online/offline)."""

    # get LDAP configuration
    cfg_rows = dbtools.select("SELECT ldapip, ldapport, dn FROM legacy.ldapconfig")
    if len(cfg_rows) == 0:
        return 0

    ldapip = cfg_rows[0][0]
    ldapport = cfg_rows[0][1]
    dn = cfg_rows[0][2]

    # just save LDAP params in returned object
    server = ldap3.Server(host=ldapip, port=ldapport, get_info='ALL')
    conn = ldap3.Connection(server, dn, "")

    try:
        if not conn.bind():
            logging.error("Cannot bind to ldap server: %s ", conn.last_error)
            return 0
    except Exception:
        return 0

    conn.unbind()
    return 1


def get_ldap_settings():
    """This function get profile LDAP's parameters"""

    settings = dbtools.select(
        "SELECT ldapip, ldapport, dn, ldapfilter FROM legacy.ldapconfig")

    server = settings[0][0]
    port = settings[0][1]
    distinct_name = settings[0][2]
    filter = settings[0][3]

    return LDAPSettingsEntity(server, port, distinct_name, filter)


def set_ldap_settings(new_ldap: LDAPSettingsEntity):
    updating_params = []
    if new_ldap.server is not None:
        updating_params.append("ldapip='%s'" % new_ldap.server)

    if new_ldap.port is not None:
        updating_params.append("ldapport=%d" % new_ldap.port)

    if new_ldap.distinct_name is not None:
        updating_params.append("dn='%s'" % new_ldap.distinct_name)

    if new_ldap.filter is not None:
        updating_params.append("ldapfilter='%s'" % new_ldap.filter)

    if len(updating_params) == 0:
        return

    sql = """UPDATE legacy.ldapconfig SET """
    for i in range(len(updating_params) - 1):
        sql += updating_params[i] + ", "
    sql += updating_params[-1]

    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("cannot update settings: %s " % (dbtools.get_last_error()))
        raise ChangeSettingsError()


def get_email_settings():
    """This function get profile Email's parameters"""

    settings = dbtools.select(
        "SELECT smtp_host, smtp_login, smtp_pwd FROM legacy.commonconfig")

    smtp_server = settings[0][0]
    username = settings[0][1]
    password = settings[0][2]

    return EmailSettingsEntity(smtp_server, username, password)


def set_email_settings(new_email: EmailSettingsEntity):
    updating_params = []
    if new_email.smtp_server is not None:
        updating_params.append("smtp_host='%s'" % new_email.smtp_server)

    if new_email.username is not None:
        updating_params.append("smtp_login='%s'" % new_email.username)

    if new_email.password is not None:
        updating_params.append("smtp_pwd='%s'" % new_email.password)

    if len(updating_params) == 0:
        return

    sql = """UPDATE legacy.commonconfig SET """
    for i in range(len(updating_params) - 1):
        sql += updating_params[i] + ", "
    sql += updating_params[-1]

    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("cannot update settings: %s " % (dbtools.get_last_error()))
        raise ChangeSettingsError()


def get_db_settings():
    """This function get profile Email's parameters"""

    settings = dbtools.select(
        "SELECT use_net, url, username, password, week_day, hour, minute FROM legacy.reserve_config")

    backup_place = data.get_backup_place(settings[0][0])
    if backup_place == "remote":
        shared_dir_url = settings[0][1]
        username = settings[0][2]
        password = settings[0][3]
    else:
        shared_dir_url = None
        username = None
        password = None

    if settings[0][4] == -1:
        backup_type = "manual"
        weekday = None
        hour = None
        minute = None
    else:
        backup_type = "timer"
        weekday = data.get_weekday(settings[0][4])
        hour = settings[0][5]
        minute = settings[0][6]

    return DBSettingsEntity(backup_place, shared_dir_url, username, password, backup_type, weekday, hour, minute)


def set_db_settings(new_db: DBSettingsEntity):
    updating_params = []

    updating_params.append("use_net=%d" % data.get_backup_place_index(new_db.backup_place))

    if new_db.backup_place == "remote":
        updating_params.append("url='%s'" % new_db.shared_dir_url)
        updating_params.append("username='%s'" % new_db.username)
        updating_params.append("password='%s'" % new_db.password)

    if new_db.backup_type == "timer":
        updating_params.append("week_day=%d" % data.get_weekday_index(new_db.weekday))
        updating_params.append("hour=%d" % new_db.hour)
        updating_params.append("minute=%d" % new_db.minute)
    else:
        updating_params.append("week_day=-1")

    if len(updating_params) == 0:
        return

    sql = """UPDATE legacy.reserve_config SET """
    for i in range(len(updating_params) - 1):
        sql += updating_params[i] + ", "
    sql += updating_params[-1]

    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("cannot update settings: %s " % (dbtools.get_last_error()))
        raise ChangeSettingsError()


def dscp_to_ip_presedence(_dscp: int) -> int:
    return _dscp >> 3


def ip_presedence_to_dscp(_ip_presedence: int) -> int:
    return _ip_presedence << 3


def get_rtp_settings() -> RTPEntity:
    rtp_rows = dbtools.select(
        "SELECT rtpportmin, rtpportmax, mtu, dscp_audio, dscp_video, dscp_other FROM legacy.commonconfig")
    if len(rtp_rows) != 0:
        rtpportmin = rtp_rows[0][0]
        rtpportmax = rtp_rows[0][1]
        mtu = rtp_rows[0][2]
        dscp_audio = rtp_rows[0][3]
        dscp_video = rtp_rows[0][4]
        dscp_other = rtp_rows[0][5]
        return RTPEntity(rtpportmin, rtpportmax, mtu, dscp_audio, dscp_video, dscp_other)
    else:
        raise GetSettingsError()


def get_rtp_commands(_cmd_list: list):
    rtp_set = get_rtp_settings()

    _cmd_list.append("SET PROTOCOL RTP PORT %d-%d" % (rtp_set.rtpportmin, rtp_set.rtpportmax))
    _cmd_list.append("SET PROTOCOL RTP MTU %d" % (rtp_set.mtu))
    _cmd_list.append("SET PROTOCOL RTP DSCP AUDIO " + str(rtp_set.dscp_audio))
    _cmd_list.append("SET PROTOCOL RTP DSCP VIDEO " + str(rtp_set.dscp_video))
    _cmd_list.append("SET PROTOCOL RTP DSCP OTHER " + str(rtp_set.dscp_other))


def set_rtp_settings(rtp: RTPEntity):
    old_rtp_settings = get_rtp_settings()

    if rtp.rtpportmin != None:
        rtpportmin = rtp.rtpportmin
    else:
        rtpportmin = old_rtp_settings.rtpportmin

    if rtp.rtpportmax != None:
        rtpportmax = rtp.rtpportmax
    else:
        rtpportmax = old_rtp_settings.rtpportmax

    if rtp.mtu != None:
        mtu = rtp.mtu
    else:
        mtu = old_rtp_settings.mtu

    if rtp.dscp_audio != None:
        dscp_audio = rtp.dscp_audio
    else:
        dscp_audio = old_rtp_settings.dscp_audio

    if rtp.dscp_video != None:
        dscp_video = rtp.dscp_video
    else:
        dscp_video = old_rtp_settings.dscp_video

    if rtp.dscp_other != None:
        dscp_other = rtp.dscp_other
    else:
        dscp_other = old_rtp_settings.dscp_other

    sql = "UPDATE legacy.commonconfig SET rtpportmin=%d, rtpportmax=%d, mtu=%d, dscp_audio=%d, dscp_video=%d, dscp_other=%d" % (
        rtpportmin, rtpportmax, mtu, dscp_audio, dscp_video, dscp_other)
    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("cannot update settings: %s " % (dbtools.get_last_error()))
        raise ChangeSettingsError()

    cmd_list = []
    get_rtp_commands(cmd_list)
    nettools.exec_cmd_list_s(cmd_list)

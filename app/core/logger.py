import logging
import logging.handlers
import os
import os.path
import sys

import app.core.xmltools as xmltools


SLASH = os.path.sep


def init_logger(_fname):
    lg = logging.getLogger('')
    lg.setLevel(logging.DEBUG)

    lg = logging.getLogger('')
    lg.setLevel(logging.DEBUG)

    cfg = xmltools.get_config()
    LOG_DIR = cfg["dirs"]["logsdir"]

    try:
        os.makedirs(LOG_DIR)
    except OSError:
        pass

    fh = logging.handlers.RotatingFileHandler(LOG_DIR + SLASH + _fname, maxBytes=1024 * 1024, backupCount=4)
    ch = logging.StreamHandler()

    fh.setLevel(logging.DEBUG)
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(process)s - %(module)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    lg.addHandler(fh)
    lg.addHandler(ch)

    return lg

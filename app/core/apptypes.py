from typing import NewType

ContactId = NewType('ContactId', int)

#!/usr/bin/python3

import json
import logging
import platform
import sys

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509.oid import NameOID

import app.core.tools as tools

if sys.platform == "win32":
    pass


CA_CRT = """-----BEGIN CERTIFICATE-----
MIIGDzCCA/egAwIBAgIUQrYaewJAmiqk/fNxsCD17WbJefkwDQYJKoZIhvcNAQEM
BQAwgYIxCzAJBgNVBAYTAlJVMQ8wDQYDVQQHDAZNb3Njb3cxFTATBgNVBAoMDFRy
dWVDb25mIExMQzEpMCcGA1UEAwwgVHJ1ZUNvbmYgQ2VydGlmaWNhdGlvbiBBdXRo
b3JpdHkxIDAeBgkqhkiG9w0BCQEWEWluZm9AdHJ1ZWNvbmYuY29tMCAXDTIwMDYy
MjE2MzUxNVoYDzIwNzAwNjEwMTYzNTE1WjCBgjELMAkGA1UEBhMCUlUxDzANBgNV
BAcMBk1vc2NvdzEVMBMGA1UECgwMVHJ1ZUNvbmYgTExDMSkwJwYDVQQDDCBUcnVl
Q29uZiBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEgMB4GCSqGSIb3DQEJARYRaW5m
b0B0cnVlY29uZi5jb20wggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC2
PEllH5kLY0GoX+8oXdXBdNEUSxYx4qw8SLgimOwNWfJqJ7eP1XK/M75dHSaKFNbe
SmxwO4DGGrjev7YisYIEy+BrHC1AFrPtM8o4SF69DIcFIAy8IsiFHx/XRRXDhRNu
yIcXeBkrkAgWt7u9pNYb2qAwZRt2RZdndFOO0wG0KVO0oVqyy7CQj2kboyPxTj8d
OLP6JZVTQK3GdrtHt2bKb+mgQMjVY64CpQeOy3VagMyRqX0qpy4b4E4kEqSsG2E/
USGi/UqpCmJ/pnV+tAqrxn/32YFYPnHFcuutJkaaJycbLDkSZ9gezApTh4UnSHzp
z2aRKWZEECfuddmJINp2w9unuV/Ih3PIS8wxGjKWkio1mFUsjrmZlXWTyTBWh4Dd
8101Qhp3XaoSYwyoaOVZASCE5bcfA/V7l6NNK+zbiDAVkHYQNTQbt6CRiQulB/U+
flvaUQ1MUOskDW8eXA4bwgnkAdAydVfv60N+XmMzPj/ovZYZPxSxT9WnfobhofCT
v9R8UhsRO+1teUkqmRwJbp1sffcqs8vIjR1HE3pyrOHTJjIVb8evItdchaRJizkk
SbXszZjuhoz7iKHnT5s/lwWT6qGYeTUdctnG/S4L8Cyo+VIO0UIaO4eKAE0AXzMA
j/G5hQcdHNKbmy2pubZX2e3ke/xa1DzL2RP18wCi3wIDAQABo3kwdzAdBgNVHQ4E
FgQU54DC2v0+bna1S/llWSnNAoww+iUwHwYDVR0jBBgwFoAU54DC2v0+bna1S/ll
WSnNAoww+iUwDwYDVR0TAQH/BAUwAwEB/zALBgNVHQ8EBAMCAQYwFwYDVR0RBBAw
DoIMdHJ1ZWNvbmYuY29tMA0GCSqGSIb3DQEBDAUAA4ICAQCPpU5KYm2li+SKQNNX
mIFnywwSM6QGQXifZ6FXD1uP/dWhx5UNiiLk03K1JXjjNxm9OMUqxWOrz+Y1jZMT
6j40N87Ke2DmOV8b2mT3L7TOkDXxYOSlo94TUnTxh8v+ftxpbbsX3qP+3M9zfKPd
6FUgf8ft9qK7fW7A3tTNous7UtC/pU5757tZ5AMsYTYujNGy6SyB7aIKup9E6RDx
J5+15/1A8T1dulg0BSYPLoT9BGsjYpL+kZtAPYl40N/5gaWqK4JrY49JgMUuezzW
0Di/WQo7JlWjP1X71ZyArWR1WQCqjbajIDVFLA8j0cA9HPRgkeGB3jXP6hyqiONL
1yzOhuYrIJnyTndJ12y9nzPlb9LmNW7tEF/IIzsW13c558FMz4+rLwmwAmWMdskM
9ilSPg6WiMzXGbQmy/OEQA28Vh5fzFauZmailNl/tOKAy5f4XIl5gS+xmizqod37
FL3bvnp8+5+Wod076ySD0fIfIfLbGFCtMYWBKrJmZiBVy3D+c/Z3sD+ONNT7S4CH
dZxUWqympEpbDPV6P2hB741fFqsI2mxO+dj2DLIbW0dY/Qu10/ClKV2S34Vl06JJ
7LIJEY8t12afJkGiFdPH9oxl0JdjG8tMyZVTQMU1BVxFN99gJ4iDfTehwUblJCV4
kku1VTB0zp4NptEdjuMcF2TLcw==
-----END CERTIFICATE-----
"""


SUCCESS = 0
NO_SCRIPT_PARAMS = 1
ERROR_SCRIPT_PARAMS = 2
NO_CLIENTDATA = 3
ERROR_CLIENTDATA = 4
NO_SYSDATA = 5
ERROR_SYSDATA = 6
ERROR_CONNECTION = 7
ERROR_SAVE_CERT = 8
CORRECT_CERT = 9
BAD_CERT = 10
LAST_DAY_OF_CERT = 11
NO_CERT_FILE = 12
GET_LICENSE_HTTP_ERROR = 13
ERROR_SAVE_LICENSE = 14
ERROR_CSR = 15
CERT_WILL_EXPIRE_SOON = 16
LICENSES_LIST_EMPTY = 17
GET_CERT_HTTP_ERROR = 18
UPD_CERT_HTTP_ERROR = 19
SEND_EVENT_HTTP_ERROR = 20
ERROR_CREATE_CA_CERT_FILE = 21

LICENSE_ERRORS_LIST = [
    { "reason": "success", "description": "success", "position": "", "http_code": 200 },
    { "reason": "no.script.params", "description": "no script params", "position": "file", "http_code": 500 },
    { "reason": "error.script.params", "description": "error script params", "position": "url", "http_code": 500 },
    { "reason": "no.client.data", "description": "no client data", "position": "file", "http_code": 500 },
    { "reason": "error.client.data", "description": "error client data", "position": "serial number / domain name",
      "http_code": 500},
    { "reason": "no.system.data", "description": "no system data", "position": "file", "http_code": 500 },
    { "reason": "error.system.data", "description": "error system data", "position": "mac / bios id", "http_code": 500},
    { "reason": "error.of.connection", "description": "error of connection", "position": "network", "http_code": 400 },
    { "reason": "error.save.certificate", "description": "error save certificate to file", "position": "file",
      "http_code": 500 },
    { "reason": "correct.certificate", "description": "correct certificate", "position": "",  "http_code": 200 },
    { "reason": "bad.certificate", "description": "certificate is expired", "position": "certificate",
      "http_code": 500 },
    { "reason": "last.day.of.certificate", "description": "last day of certificate", "position": "certificate",
      "http_code": 500 },
    { "reason": "no.certificate.file", "description": "no certificate file", "position": "file", "http_code": 500 },
    { "reason": "get.license.http.error", "description": "error at http request to license server", "position":"http",
      "http_code": 400 },
    { "reason": "error.save.license", "description": "error of saving license to file", "position": "file",
      "http_code": 500 },
    { "reason": "error.csr.request", "description": "error of building csr request", "position": "csr",
      "http_code": 400 },
    { "reason":"certificate.will.expire.soon", "description":"certificate will expire soon", "position":"certificate",
      "http_code": 500 },
    { "reason": "licenses.list.empty", "description": "licenses list is empty", "position": "license",
      "http_code": 500 },
    {"reason": "get.cert.http.error", "description": "Get certificate error (in http request)", "position": "http",
     "http_code": 400 },
    {"reason": "upd.cert.http.error", "description": "Update certificate error (in http request)", "position": "http",
     "http_code": 400 },
    {"reason": "send.event.http.error", "description": "Send event error (in http request)", "position": "http",
     "http_code": 400 },
    {"reason": "create.ca.cert.file.error", "description": "Create CA certificate file error", "position":"certificate",
     "http_code": 500}
]

def get_license_error(err_code: int) -> dict:
    if err_code >= 0 and err_code < len(LICENSE_ERRORS_LIST):
        return LICENSE_ERRORS_LIST[err_code]
    else:
        return LICENSE_ERRORS_LIST[0]


CRM_ERRORS_TRANSLATION = [
    {
        "crm": {"reason": "wrongCSR", "message": "Wrong CSR format", "locationType": "parameter", "location": "csr"},
        "mcu": tools.get_detail_error_item("wrong.csr.format", "Wrong CSR format", "csr", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "extensionNotFound", "message": "Extension in csr file not found",
                "locationType": "parameter", "location": "serial"},
        "mcu": tools.get_detail_error_item("no.param.in.csr", "param is not found in csr", "serial", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "extensionNotFound", "message": "Extension in csr file not found",
                "locationType": "parameter", "location": "server_id"},
        "mcu": tools.get_detail_error_item("no.param.in.csr", "param is not found in csr", "server_id", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "extensionNotFound", "message": "Extension in csr file not found",
                "locationType": "parameter", "location": "mac_address"},
        "mcu": tools.get_detail_error_item("no.param.in.csr", "param is not found in csr", "mac_address", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "extensionNotFound", "message": "Extension in csr file not found",
                "locationType": "parameter", "location": "bios_uuid"},
        "mcu": tools.get_detail_error_item("no.param.in.csr", "param is not found in csr", "bios_uuid", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "extensionNotFound", "message": "Extension in csr file not found",
                "locationType": "parameter", "location": "version"},
        "mcu": tools.get_detail_error_item("no.param.in.csr", "param is not found in csr", "version", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "extensionNotFound", "message": "Extension in csr file not found",
                "locationType": "parameter", "location": "os"},
        "mcu": tools.get_detail_error_item("no.param.in.csr", "param is not found in csr", "os", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "extensionNotFound", "message": "Extension in csr file not found",
                "locationType": "parameter", "location": "cpu"},
        "mcu": tools.get_detail_error_item("no.param.in.csr", "param is not found in csr", "cpu", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "objectNotFound", "message": "Object not found", "locationType": "database",
                "location": "serial"},
        "mcu": tools.get_detail_error_item("not.found", "Param is not found in database", "serial", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "objectNotFound", "message": "Object not found", "locationType": "database",
                "location": "server"},
        "mcu": tools.get_detail_error_item("not.found", "Param is not found in database", "serial", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "objectNotFound", "message": "Object not found", "locationType": "database",
                "location": "mac_address"},
        "mcu": tools.get_detail_error_item("invalid.mac.address", "Param is not found in database", "serial",None),
        "http_code": 400
    },
    {
        "crm": {"reason": "objectNotFound", "message": "Object not found", "locationType": "database",
                "location": "bios_uuid"},
        "mcu": tools.get_detail_error_item("invalid.bios.uuid", "Param is not found in database", "serial", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "serverWithCurrentDomainNotFound", "message": "Server with current domain not found",
                "locationType": "database", "location": "server"},
        "mcu": tools.get_detail_error_item("invalid.domain", "Domain is not found in database", "serial", None),
        "http_code": 400
    },
    {
        "crm": {"reason": "serverHasNoActiveLicenses", "message": "The server has no active licenses",
                "locationType": "database", "location": "server"},
        "mcu": tools.get_detail_error_item("licenses.not.found", "No active licenses in database", "serial", None),
        "http_code": 400
    }
]


def translate_crm_error(err_json: dict) -> list:
    if "errors" not in err_json:
        return [tools.get_detail_error_item("bad.crm.error.format", "Bad CRM error format", "error", None)]

    details_list = []
    for item in err_json["errors"]:
        if "reason" not in item:
            return [tools.get_detail_error_item("bad.crm.error.format", "Bad CRM error format", "reason", None)]
        if "locationType" not in item:
            return [tools.get_detail_error_item("bad.crm.error.format", "Bad CRM error format", "locationType", None)]
        if "location" not in item:
            return [tools.get_detail_error_item("bad.crm.error.format", "Bad CRM error format", "location", None)]

        for err in CRM_ERRORS_TRANSLATION:
            if err["crm"]["reason"] == item["reason"] and \
                err["crm"]["locationType"] == item["locationType"] and \
                err["crm"]["location"] == item["location"]:
                details_list.append(tools.get_detail_error_item(err["mcu"]["reason"], err["mcu"]["description"],
                                                                err["mcu"]["position"], None))
                break

    return details_list


def build_error_dict(error_code: int) -> dict:
    b_ok = error_code == SUCCESS
    err = get_license_error(error_code)
    details = [ tools.get_detail_error_item(err["reason"], err["description"], err["position"], None) ]
    result = {"ok": b_ok, "code": error_code, "details": details, "http_code": err["http_code"]}
    return result


def build_error_dict_ex(error_code: int, crm_err_str: str) -> dict:
    b_ok = error_code == SUCCESS

    # convert crm error from stirng to json
    try:
        data_json = json.loads(crm_err_str)
    except:
        details = [ tools.get_detail_error_item("unknown.crm.error", crm_err_str, "crm", None) ]
        res = {"ok": b_ok, "code": error_code, "details": details, "http_code": 500}
        return res

    # translate crm error to mcu error
    details = translate_crm_error(data_json)
    http_code = 400

    # if unknown crm error, get mcu error from error code
    if details == []:
        err = get_license_error(error_code)
        details = [ tools.get_detail_error_item(err["reason"], err["description"], err["position"], data_json) ]
        http_code = err["http_code"]

    result = {"ok": b_ok, "code": error_code, "details": details, "http_code": http_code }
    return result


# CSR generation
def generate_csr(domain, key, serial, mac_address, bios_id, version, os_info, cpu_info):
    serial_oid = x509.ObjectIdentifier(str("1.2.643.2.4310.1.1"))
    version_oid = x509.ObjectIdentifier(str("1.2.643.2.4310.1.2"))
    os_oid = x509.ObjectIdentifier(str("1.2.643.2.4310.1.3"))
    cpu_oid = x509.ObjectIdentifier(str("1.2.643.2.4310.1.4"))
    mac_oid = x509.ObjectIdentifier(str("1.2.643.2.4310.1.7"))
    bios_oid = x509.ObjectIdentifier(str("1.2.643.2.4310.1.8"))

    builder = x509.CertificateSigningRequestBuilder()
    builder = builder.subject_name(x509.Name([
        # Provide various details about who we are.
        x509.NameAttribute(NameOID.COMMON_NAME, str(domain)),
    ]))
    builder = builder.add_extension(
        x509.UnrecognizedExtension(serial_oid, str(serial).encode()),
        critical=False,
    )
    builder = builder.add_extension(
        x509.UnrecognizedExtension(version_oid, str(version).encode()),
        critical=False,
    )
    builder = builder.add_extension(
        x509.UnrecognizedExtension(os_oid, str(os_info).encode()),
        critical=False,
    )
    builder = builder.add_extension(
        x509.UnrecognizedExtension(cpu_oid, str(cpu_info).encode()),
        critical=False,
    )
    builder = builder.add_extension(
        x509.UnrecognizedExtension(mac_oid, str(mac_address).encode()),
        critical=False,
    )
    builder = builder.add_extension(
        x509.UnrecognizedExtension(bios_oid, str(bios_id).encode()),
        critical=False,
    )

    req = builder.sign(key, hashes.SHA256(), default_backend())

    return req


# csr to string (byte)
def csr_to_str(csr):
    return csr.public_bytes(serialization.Encoding.PEM)


# write CSR to file
def write_csr(file, csr):
    with open(file, "wb") as f:
        f.write(csr_to_str(csr))

    # private_key generation


def generate_key():
    key = private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )
    return key


# private_key to string (byte)
def key_to_str(key):
    return key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.BestAvailableEncryption(b"APM1JDVgT8WDGOWBgQv6EIhvxl4vDYvU"),
    )


# write private_key to file
def write_key(file, key):
    str_key = key_to_str(key)
    with open(file, "wb") as f:
        f.write(str_key)


def read_key(file):
    with open(file, "rb") as f:
        text = f.read()
    return text


def log_msg(_msg):
    logging.error(_msg)


def get_python_os_info():
    """
    Get Operating System type/distribution and major version
    using python platform module

    :returns: (os_name, os_version)
    :rtype: `tuple` of `str`
    """
    info = platform.system_alias(
        platform.system(),
        platform.release(),
        platform.version()
    )
    os_type, os_ver, _ = info
    os_type = os_type.lower()
    if os_type.startswith('linux'):
        info = platform.linux_distribution()
        # On arch, platform.linux_distribution() is reportedly ('','',''),
        # so handle it defensively
        if info[0]:
            os_type = info[0]
        if info[1]:
            os_ver = info[1]
        if info[2]:
            os_ver = info[2]
        if info[1] and info[2]:
            os_ver = str(info[1]) + ' ' + str(info[2])
    elif platform.win32_ver()[1]:
        os_ver = str(platform.win32_ver()[0]) + ' ' + str(platform.win32_ver()[1]) + ' ' + str(
            platform.win32_ver()[2]) + ' ' + str(platform.win32_ver()[3])
    else:
        # Cases known to fall here: Cygwin python
        os_ver = ''
    return str(os_type) + ' ' + str(os_ver)

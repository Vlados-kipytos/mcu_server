#!/usr/bin/python3

import logging

import app.core.constants as constants
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.features as features
import app.core.nettools as nettools
import app.core.onlinetools as onlinetools
import app.core.tools as tools
from app.core.entities import CallsEntity
from app.core.entities import H323SettingsEntity
from app.core.entities import SipEntity
from app.core.errors import ChangeSettingsError
from app.core.errors import GetDatabaseDataError
from app.core.errors import NotCompatibleSettingsError
from app.core.errors import SettingIsRestrictedByLicense


def get_calls_settings():
    """This function get profile calls's parameters"""

    settings = dbtools.select("""SELECT fecc, protpref, usesip, useh323, server_name, 
                                 call_seq, external_ip, audio_call, noise_suppressor, auto_gain_ctrl
                                 FROM legacy.commonconfig""")
    if len(settings) != 0:
        fecc = tools.int_to_bool(settings[0][0])
        protpref = data.get_protocols_order(settings[0][1])
        usesip = tools.int_to_bool(settings[0][2])
        useh323 = tools.int_to_bool(settings[0][3])
        disp_name = settings[0][4]
        call_sequence = data.get_call_sequence_name(settings[0][5])
        external_ip = settings[0][6]
        audio_call = data.get_audio_call_name(settings[0][7])
        noise_suppressor = tools.int_to_bool(settings[0][8])
        auto_gain_ctrl = tools.int_to_bool(settings[0][9])
        return CallsEntity(fecc, usesip, useh323, protpref, disp_name, call_sequence, external_ip, audio_call,
                           noise_suppressor, auto_gain_ctrl)
    else:
        return CallsEntity(True, True, True, data.get_protocols_order(0), "MCU", data.get_call_sequence_name(0), "",
                           "HIDE", False, False)


def get_calls_commands(_cmd_list):
    calls_set = get_calls_settings()

    _cmd_list.append("SET CALL FECC " + tools.num_to_onoff(calls_set.remote_camera_control))

    prot_pref_index = data.get_protocols_order_index(calls_set.preferred_protocol)
    _cmd_list.append("SET PROTOCOL PREF " + tools.get_prot_pref_str(prot_pref_index))

    _cmd_list.append("SET PROTOCOL SIP " + tools.num_to_onoff(calls_set.enable_sip))
    _cmd_list.append("SET PROTOCOL H323 " + tools.num_to_onoff(calls_set.enable_h323))

    calls_seq_ind = data.get_call_sequence_index(calls_set.call_sequence)
    calls_seq_cmd = data.get_call_sequence(calls_seq_ind)["cmd"]
    _cmd_list.append(calls_seq_cmd)

    _cmd_list.append("SET PROTOCOL DISPLAYNAME " + calls_set.mcu_display_name)

    if len(calls_set.external_ip) != 0:
        _cmd_list.append("SET PROTOCOL MYIP " + calls_set.external_ip)

    _cmd_list.append( data.get_audio_call_cmd(calls_set.audio_call) )

    _cmd_list.append( data.get_noise_suppressor_global_cmd(calls_set.enable_noise_suppressor) )
    _cmd_list.append( data.get_auto_gain_ctrl_global_cmd(calls_set.enable_auto_gain_ctrl) )


def set_calls_settings(new_calls: CallsEntity):
    features_list = features.get_features_list()

    updating_params = []
    if new_calls.remote_camera_control is not None:
        if new_calls.remote_camera_control and (not features.get_feature(features_list, "FECC")):
            raise SettingIsRestrictedByLicense("fecc")
        updating_params.append("fecc=%d" % tools.bool_to_int(new_calls.remote_camera_control))

    if new_calls.enable_sip is not None:
        if new_calls.enable_sip and (not features.get_feature(features_list, "SIP")):
            raise SettingIsRestrictedByLicense("sip")
        updating_params.append("usesip=%d" % tools.bool_to_int(new_calls.enable_sip))

    if new_calls.enable_h323 is not None:
        if new_calls.enable_h323 and (not features.get_feature(features_list, "H323")):
            raise SettingIsRestrictedByLicense("h323")
        updating_params.append("useh323=%d" % tools.bool_to_int(new_calls.enable_h323))

    if new_calls.preferred_protocol is not None:
        updating_params.append("protpref=%d" % data.get_protocols_order_index(new_calls.preferred_protocol))

    if new_calls.mcu_display_name is not None:
        updating_params.append("server_name='%s'" % new_calls.mcu_display_name)

    if new_calls.call_sequence is not None:
        updating_params.append("call_seq=%d" % data.get_call_sequence_index(new_calls.call_sequence))

    if new_calls.external_ip is not None:
        updating_params.append("external_ip='%s'" % (new_calls.external_ip))

    if new_calls.audio_call is not None:
        audio_call_ind = data.get_audio_call_ind(new_calls.audio_call)
        updating_params.append("audio_call=%d" % (audio_call_ind))

    if new_calls.enable_noise_suppressor is not None:
        updating_params.append("noise_suppressor=%d" % tools.bool_to_int(new_calls.enable_noise_suppressor))

    if new_calls.enable_auto_gain_ctrl is not None:
        updating_params.append("auto_gain_ctrl=%d" % tools.bool_to_int(new_calls.enable_auto_gain_ctrl))

    if len(updating_params) == 0:
        return

    sql = """UPDATE legacy.commonconfig SET """
    for i in range(len(updating_params) - 1):
        sql += updating_params[i] + ", "
    sql += updating_params[-1]

    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("cannot update settings: %s " % (dbtools.get_last_error()))
        raise ChangeSettingsError()

    cmd_list = []
    get_calls_commands(cmd_list)
    nettools.exec_cmd_list_s(cmd_list)


def parse_turn_address(_address: str) -> dict:
    """This function parse address of TURN server to components 'username:password@hostport'."""
    result = {"username": "", "password": "", "hostport": ""}

    if _address is None or len(_address) == 0:
        return result

    dog_index = _address.find("@")
    if dog_index < 0:
        result["hostport"] = _address
    else:
        result["hostport"] = _address[dog_index+1:]
        userpass = _address[:dog_index]
        dots_index = userpass.find(":")
        if dots_index < 0:
            result["username"] = userpass
        else:
            result["username"] = userpass[:dots_index]
            result["password"] = userpass[dots_index+1:]

    return result


def join_turn_address(_username: str, _password: str, _hostport: str) -> str:
    """This function join components of TURN server address to string 'username:password@hostport'."""

    user = _username
    if user is None or len(user) == 0:
        user = ""

    passwd = _password
    if passwd is None or len(passwd) == 0:
        passwd = ""

    host = _hostport
    if host is None or len(host) == 0:
        host = ""

    userpass = user
    if len(passwd) != 0:
        userpass += ":" + passwd

    address = host
    if len(userpass) != 0:
        address = userpass + "@" + host

    return address


def get_sip_settings():
    """This function get profile calls's parameters"""

    settings = dbtools.select("""SELECT sip_reg, sip_server, sip_login, sip_pwd, sip_transp, sip_bfcp,
    use_ice, turn_addr, sip_port, bfcp_port_min, bfcp_port_max FROM legacy.commonconfig""")

    register = tools.int_to_bool(settings[0][0])
    server = settings[0][1]
    username = settings[0][2]
    password = settings[0][3]
    transport_protocol = data.get_sip_transport_name(settings[0][4])
    sip_bfcp = data.get_bfcp_transport_name(settings[0][5])
    use_ice = tools.int_to_bool(settings[0][6])
    turn_addr = settings[0][7]
    sip_port = settings[0][8]
    bfcp_port_min = settings[0][9]
    bfcp_port_max = settings[0][10]

    registration = False
    config = onlinetools.get_config_from_engine()
    if config != {}:
        registration = config["config"]["sip"]["status"]["registration"]

    return SipEntity(register, server, username, password, transport_protocol, registration, sip_bfcp,
                     use_ice, turn_addr, sip_port, bfcp_port_min, bfcp_port_max)


def get_sip_commands(_cmd_list):
    """This functio add to _cmd_list sip registration commands"""

    calls_set = get_calls_settings()
    # get_calls_commands(_cmd_list)

    if calls_set.enable_sip:
        sip_set = get_sip_settings()
        _cmd_list.append("SET PROTOCOL SIP PORT %d" % (sip_set.sip_port))
        _cmd_list.append("SET PROTOCOL SIP BFCP PORT %d-%d" % (sip_set.bfcp_port_min, sip_set.bfcp_port_max))

        if sip_set.server != "" and sip_set.username != "":
            transp_prot_ind = data.get_sip_transport_index(sip_set.transport_protocol)
            transp_prot_cmd = data.get_sip_transport(transp_prot_ind)["cmd"]

            if sip_set.password != "":
                uri = "%s:%s@%s%s" % (sip_set.username, sip_set.password, sip_set.server, transp_prot_cmd)
            else:
                uri = "%s@%s%s" % (sip_set.username, sip_set.server, transp_prot_cmd)

            _cmd_list.append("SET PROTOCOL SIP CRED " + uri)
            _cmd_list.append("SET PROTOCOL SIP REGISTRATION " + tools.num_to_onoff(sip_set.register))
        else:
            _cmd_list.append("SET PROTOCOL SIP CRED @")
            # _cmd_list.append( "SET PROTOCOL SIP REGISTRATION OFF" )

        _cmd_list.append("SET PROTOCOL SIP BFCP " + sip_set.sip_bfcp)

        if sip_set.use_ice:
            if len(sip_set.turn_addr) != 0:
                _cmd_list.append("SET PROTOCOL SIP TURN CRED " + sip_set.turn_addr)
                _cmd_list.append("SET PROTOCOL SIP TURN ON")
            else:
                _cmd_list.append("SET PROTOCOL SIP TURN OFF")
            _cmd_list.append("SET PROTOCOL SIP ICE ON")
        else:
            _cmd_list.append("SET PROTOCOL SIP TURN OFF")
            _cmd_list.append("SET PROTOCOL SIP ICE OFF")


def set_sip_settings(new_sip: SipEntity):
    updating_params = []
    if new_sip.register is not None:
        updating_params.append("sip_reg=%d" % tools.bool_to_int(new_sip.register))

    if new_sip.server is not None:
        updating_params.append("sip_server='%s'" % new_sip.server)

    if new_sip.username is not None:
        updating_params.append("sip_login='%s'" % new_sip.username)

    if new_sip.password is not None:
        updating_params.append("sip_pwd='%s'" % new_sip.password)

    if new_sip.transport_protocol is not None:
        updating_params.append("sip_transp=%d" % data.get_sip_transport_index(new_sip.transport_protocol))

    if new_sip.sip_bfcp is not None:
        updating_params.append("sip_bfcp=%d" % data.get_bfcp_transport_index(new_sip.sip_bfcp))

    if new_sip.use_ice is not None:
        updating_params.append("use_ice=%d" % tools.bool_to_int(new_sip.use_ice))

    if new_sip.turn_addr is not None:
        updating_params.append("turn_addr='%s'" % (new_sip.turn_addr))

    if new_sip.sip_port is not None:
        updating_params.append("sip_port=%d" % (new_sip.sip_port))

    if new_sip.bfcp_port_min is not None:
        updating_params.append("bfcp_port_min=%d" % (new_sip.bfcp_port_min))

    if new_sip.bfcp_port_max is not None:
        updating_params.append("bfcp_port_max=%d" % (new_sip.bfcp_port_max))

    if len(updating_params) == 0:
        return

    sql = """UPDATE legacy.commonconfig SET """
    for i in range(len(updating_params) - 1):
        sql += updating_params[i] + ", "
    sql += updating_params[-1]

    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("cannot update settings: %s " % (dbtools.get_last_error()))
        raise ChangeSettingsError()

    cmd_list = []
    get_sip_commands(cmd_list)
    nettools.exec_cmd_list_s(cmd_list)


def get_h323_settings() -> H323SettingsEntity:
    """This function returns settings of gatekeeper"""

    settings_rows = dbtools.select("""SELECT use_gk, gk_id_num, gk_e164, gk_server, gk_zone, 
    h46017, h46018, h46019, h323_port, h245_port_min, h245_port_max FROM legacy.commonconfig""")
    if len(settings_rows) == 0:
        raise GetDatabaseDataError()

    use_gatekeeper = tools.int_to_bool(settings_rows[0][0])
    id_num = settings_rows[0][1]
    e164_extension = settings_rows[0][2]
    server_address = settings_rows[0][3]
    zone = settings_rows[0][4]
    h46017 = tools.int_to_bool(settings_rows[0][5])
    h46018 = tools.int_to_bool(settings_rows[0][6])
    h46019 = tools.int_to_bool(settings_rows[0][7])
    h323_port = settings_rows[0][8]
    h245_port_min = settings_rows[0][9]
    h245_port_max = settings_rows[0][10]

    registration = False
    config = onlinetools.get_config_from_engine()
    if config != {}:
        registration = config["config"]["h323"]["status"]["registration"]

    settings = H323SettingsEntity(use_gatekeeper, id_num, e164_extension, server_address, zone,
                                  registration, h46017, h46018, h46019,
                                  h323_port, h245_port_min, h245_port_max)

    return settings


def get_h323_commands(_cmd_list):
    """This function add to _cmd_list h323 (gatekeeper) registration commands"""

    calls_set = get_calls_settings()
    # get_calls_commands(_cmd_list)

    if calls_set.enable_h323:
        h323_settings = get_h323_settings()

        #TODO: wait for implementation command:
        # _cmd_list.append("SET PROTOCOL H323 PORT %d" % (h323_settings.h323_port))
        _cmd_list.append("SET PROTOCOL H323 H245 PORT %d-%d" % (h323_settings.h245_port_min, h323_settings.h245_port_max))

        if h323_settings.use_gatekeeper:
            if h323_settings.id_num != "":
                _cmd_list.append("SET PROTOCOL H323 ID " + h323_settings.id_num)

            if h323_settings.e164_extension != "":
                _cmd_list.append("SET PROTOCOL H323 EXTENSION " + h323_settings.e164_extension)

            if h323_settings.server != "":
                if h323_settings.server.find("@") != -1:
                    server_str = h323_settings.server
                else:
                    server_str = "@" + h323_settings.server
                _cmd_list.append("SET PROTOCOL H323 CRED " + server_str)

            if h323_settings.zone != "":
                _cmd_list.append("SET PROTOCOL H323 ZONE " + h323_settings.zone)

            if h323_settings.server != "":
                _cmd_list.append("SET PROTOCOL H323 GATEKEEPER ON")
            else:
                _cmd_list.append("SET PROTOCOL H323 GATEKEEPER DISCOVER")
        else:
            # clear h323 params
            _cmd_list.append("SET PROTOCOL H323 EXTENSION ")
            _cmd_list.append("SET PROTOCOL H323 ID ")
            _cmd_list.append("SET PROTOCOL H323 CRED @")
            _cmd_list.append("SET PROTOCOL H323 ZONE ")

            # turn off registration
            _cmd_list.append("SET PROTOCOL H323 GATEKEEPER OFF")

        if h323_settings.h46017 or h323_settings.h46018 or h323_settings.h46019:
            _cmd_list.append("SET PROTOCOL H323 H46017 " + tools.bool_to_auto_off(h323_settings.h46017))
            _cmd_list.append("SET PROTOCOL H323 H46018 " + tools.bool_to_auto_off(h323_settings.h46018))
            _cmd_list.append("SET PROTOCOL H323 H46019 " + tools.bool_to_auto_off(h323_settings.h46019))
            _cmd_list.append("SET PROTOCOL H323 NAT AUTO")
        else:
            _cmd_list.append("SET PROTOCOL H323 NAT OFF")


def set_h323_settings(settings: H323SettingsEntity):
    """this function update gatekeeper settings"""

    old_settings = get_h323_settings()

    if settings.use_gatekeeper != None:
        use_gk_num = tools.bool_to_int(settings.use_gatekeeper)
    else:
        use_gk_num = tools.bool_to_int(old_settings.use_gatekeeper)

    if settings.id_num != None:
        id_num_db_str = tools.str_to_dbstr(settings.id_num)
    else:
        id_num_db_str = tools.str_to_dbstr(old_settings.id_num)

    if settings.e164_extension != None:
        e164_ext = tools.str_to_dbstr(settings.e164_extension)
    else:
        e164_ext = tools.str_to_dbstr(old_settings.e164_extension)

    if settings.server != None:
        server_db_str = tools.str_to_dbstr(settings.server)
    else:
        server_db_str = tools.str_to_dbstr(old_settings.server)

    if settings.zone != None:
        zone_db_str = tools.str_to_dbstr(settings.zone)
    else:
        zone_db_str = tools.str_to_dbstr(old_settings.zone)

    if settings.h46017 != None:
        h46017 = tools.bool_to_int(settings.h46017)
    else:
        h46017 = tools.bool_to_int(old_settings.h46017)

    if settings.h46018 != None:
        h46018 = tools.bool_to_int(settings.h46018)
    else:
        h46018 = tools.bool_to_int(old_settings.h46018)

    if settings.h46019 != None:
        h46019 = tools.bool_to_int(settings.h46019)
    else:
        h46019 = tools.bool_to_int(old_settings.h46019)

    if h46017 and (not use_gk_num):
        raise NotCompatibleSettingsError("Setting h46017 need gatekeeper.")

    if h46017 and h46018:
        raise NotCompatibleSettingsError("Settings h46017 and h46018 cannot be used simultaneous.")

    if (not h46017) and (not h46018) and h46019:
        raise NotCompatibleSettingsError("Setting h46019 may be used if one of settings h46017 or h46018 enabled.")

    if settings.h323_port != None:
        h323_port = settings.h323_port
    else:
        h323_port = old_settings.h323_port

    if settings.h245_port_min != None:
        h245_port_min = settings.h245_port_min
    else:
        h245_port_min = old_settings.h245_port_min

    if settings.h245_port_max != None:
        h245_port_max = settings.h245_port_max
    else:
        h245_port_max = old_settings.h245_port_max

    sql = """UPDATE legacy.commonconfig SET use_gk=%d, gk_id_num='%s', gk_e164='%s', gk_server='%s', 
        gk_zone='%s', h46017=%d, h46018=%d, h46019=%d, h323_port=%d, h245_port_min=%d,
        h245_port_max=%d""" % (use_gk_num, id_num_db_str, e164_ext, server_db_str, zone_db_str,
                               h46017, h46018, h46019, h323_port, h245_port_min, h245_port_max)

    res = dbtools.make_sql_request(sql)
    if res == 0:
        logging.error("cannot update settings: %s " % (dbtools.get_last_error()))
        raise ChangeSettingsError()

    cmd_list = []
    get_h323_commands(cmd_list)
    nettools.exec_cmd_list_s(cmd_list)

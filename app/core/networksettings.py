#!/usr/bin/python3

import copy
import ipaddress
import logging
import os
import os.path
import random
import sys
import time

import app.core.xmltools as xmltools
from app.core.entities import IPAddressMaskEntity
from app.core.entities import NetworkSettingsEntity
from app.core.errors import NetworkInterfaceNotFoundError
from app.core.errors import NetworkManagerError
from app.core.errors import NetworkManagerNotInstalledError
from app.core.requests import ChangeNetworkSettingsRequest
from app.core.requests import GetNetworkSettingsRequest


def exec_cmd_ex(cmd):
    """This function execute command 'cmd' and returns exit code/stdout/stderr."""
    SLASH = os.path.sep
    cfg = xmltools.get_config()
    OUT_FILE = cfg["dirs"]["shareddir"] + SLASH + "nmcli_stdout.log"
    ERR_FILE = cfg["dirs"]["shareddir"] + SLASH + "nmcli_stderr.log"

    code = -1
    try:
        full_cmd = cmd + (" 1>%s 2>%s" % (OUT_FILE, ERR_FILE))
        logging.error("exec_cmd_ex(): execute:" + full_cmd)
        code = os.system(full_cmd)
        code >>= 8  # get major byte
    except:
        out = ""
        err = "exec_cmd_ex(): os.system() exception: " + str(sys.exc_info())
        logging.error(err)
        return out, err, code

    out_str = ""
    err_str = ""
    try:
        fout = open(OUT_FILE, "rb")
        out_data = fout.read()
        fout.close()

        ferr = open(ERR_FILE, "rb")
        err_data = ferr.read()
        ferr.close()

        out_str = out_data.decode("UTF-8")
        err_str = err_data.decode("UTF-8")
    except:
        logging.error("exec_cmd_ex(): exception: " + str(sys.exc_info()))

    if len(err_str) != 0:
        logging.error("exec_cmd_ex(): error at execute command:")
        logging.error(full_cmd)
        logging.error("exec_cmd_ex(): stderr:")
        logging.error(err_str)

    return out_str, err_str, code


def is_valid_ip(ip: str) -> bool:
    try:
        ip_obj = ipaddress.IPv4Address(ip)
    except:
        return False
    return True


def parse_ip_with_prefix(ip_with_prefix: str):
    """This function parse string 'ip/prefix' and returns pair ip-prefix."""

    slash_ind = ip_with_prefix.find("/")
    if slash_ind == -1:
        return ip_with_prefix, 0
    else:
        ip_str = ip_with_prefix[:slash_ind]
        prefix_str = ip_with_prefix[slash_ind + 1:]
        if prefix_str.isdigit():
            prefix = int(prefix_str)
        else:
            prefix = 0
        return ip_str, prefix


def prefix_to_netmask(ipaddr: str, prefix: int) -> str:
    netmask = str(ipaddress.IPv4Network((ipaddr, prefix), False).netmask)
    return netmask


def netmask_to_prefix(ipaddr: str, netmask: str) -> int:
    ip_network = ipaddress.IPv4Network((ipaddr, netmask), False).compressed
    prefix = int(ip_network.split("/")[1])
    return prefix


def ipaddress_to_int(ipaddr: str) -> int:
    ipaddrobj = ipaddress.IPv4Address(ipaddr)
    return ipaddrobj.packed


IP_MASK_SUCCESS = 0
BAD_IP_ADDRESS = -1
BAD_NETWORK_MASK = -2
OTHER_ERROR = -3


def check_ip_mask(ipaddr: str, netmask: str):
    try:
        ip_mask_obj = ipaddress.IPv4Network((ipaddr, netmask), False)
    except ipaddress.AddressValueError as exc:
        logging.error("check_ip_mask(): exception: bad ip address %s" % (ipaddr))
        return BAD_IP_ADDRESS
    except ipaddress.NetmaskValueError as exc:
        logging.error("check_ip_mask(): exception: bad network mask %s" % (netmask))
        return BAD_NETWORK_MASK
    except:
        logging.error("check_ip_mask(): exception: " + str(sys.exc_info()))
        return OTHER_ERROR

    return IP_MASK_SUCCESS


if sys.platform == "win32":  # --------------------------- WINDOWS VERSION ---------------------------------

    def get_all_network_settings() -> list:
        """This function returns list of NetworkSettingsEntity."""
        # DUMMY:
        ipmask_list = [IPAddressMaskEntity("192.168.1.1", "255.255.255.0"),
                       IPAddressMaskEntity("192.168.1.2", "255.255.255.0")]
        dns_list = ["192.168.0.1", "192.168.0.2"]
        return [NetworkSettingsEntity("eth0", "00:00:00:00:00:00", True, ipmask_list, "0.0.0.0", dns_list, False, "",
                                      "ethernet")]


    def get_network_settings(request: GetNetworkSettingsRequest) -> NetworkSettingsEntity:
        """This function returns NetworkSettingsEntity."""
        # DUMMY:
        return NetworkSettingsEntity(request.iface_name, "00:00:00:00:00:00", True, [], "0.0.0.0", [], False, "",
                                     "ethernet")


    def set_network_settings(request: ChangeNetworkSettingsRequest) -> NetworkSettingsEntity:
        """This function set new network settings and returns NetworkSettingsEntity."""
        # DUMMY:
        return NetworkSettingsEntity(request.settings.ifname, request.settings.mac, request.settings.use_dhcp,
                                     request.settings.ipaddr_list, request.settings.gateway,
                                     request.settings.nameservers, False, "", "ethernet")


    def can_edit_network_settings() -> bool:
        # DUMMY:
        return True

else:  # --------------------------- LINUX VERSION ---------------------------------

    def is_network_manager_installed() -> bool:
        """This function check: is NetworkManeger installed?"""

        out, err, code = exec_cmd_ex("LC_ALL=C nmcli --version")
        #if code != 0:
        #    raise NetworkManagerError(code)

        if out.find("version") != -1:
            return True
        else:
            return False


    def parse_params_list(prm_list_str: str) -> list:
        """This function converts text to list of pairs name-value."""

        prm_list = []
        lines_list = prm_list_str.splitlines()
        for line in lines_list:
            line = line.strip()

            space_ind = line.find(" ")
            if space_ind != -1:
                name = line[:space_ind]
                value = line[space_ind:]
            else:
                name = line
                value = ""

            name = name.strip()
            if len(name) >= 1 and name[-1] == ":":
                name = name[:-1]

            value = value.strip()

            prm_list.append({"name": name, "value": value})

        return prm_list


    def params_list_to_sublists(prm_list: list) -> list:
        """This function build list of sublists, separated in 'prm_list' by empty line."""

        out_list = []
        cur_sublist = []
        for item in prm_list:
            if len(item["name"]) == 0 and len(item["value"]) == 0:
                sublist_copy = copy.deepcopy(cur_sublist)
                out_list.append(sublist_copy)
                cur_sublist = []
            else:
                cur_sublist.append(item)

        if len(cur_sublist) != 0:
            sublist_copy = copy.deepcopy(cur_sublist)
            out_list.append(sublist_copy)
            cur_sublist = []

        return out_list


    def extract_params_sublist(prm_list: list, name: str, value: str) -> list:
        """This function find sublist with specified parameter name/value and return it."""

        found = False
        sub_list = []
        for item in prm_list:
            if item["name"] == name and item["value"] == value:
                found = True

            if len(item["name"]) == 0 and len(item["value"]) == 0:
                if found:
                    return sub_list
                else:
                    sub_list = []
            else:
                sub_list.append(item)

        if found:
            return sub_list
        else:
            return []


    def param_list_to_network_settings(prm_list: list) -> NetworkSettingsEntity:

        ip_mask_list = []
        dns_list = []

        settings = NetworkSettingsEntity("", "", True, [], "", [], False, "--", "")
        for item in prm_list:
            name = item["name"]
            value = item["value"]

            if name == "GENERAL.DEVICE":
                settings.ifname = value
            elif name == "GENERAL.TYPE":
                settings.type = value
            elif name == "GENERAL.HWADDR":
                settings.mac = value
            elif name.find("IP4.ADDRESS") != -1:
                ip, prefix = parse_ip_with_prefix(value)
                netmask = prefix_to_netmask(ip, prefix)
                ip_mask_list.append(IPAddressMaskEntity(ip, netmask))
            elif name == "IP4.GATEWAY":
                settings.gateway = value
            elif name.find("IP4.DNS") != -1:
                dns_list.append(value)
            elif name == "GENERAL.STATE":
                if value.find("100") != -1:
                    settings.ready = True
                else:
                    settings.ready = False
            elif name == "GENERAL.CON-UUID":
                settings.conn_uuid = value

        settings.ipaddr_list = []
        for ip_mask in ip_mask_list:
            settings.ipaddr_list.append(ip_mask)

        settings.nameservers = []
        for dns in dns_list:
            settings.nameservers.append(dns)

        return settings


    def update_network_settings_by_conn_params(conn_prm_list: list,
                                               settings: NetworkSettingsEntity) -> NetworkSettingsEntity:

        for item in conn_prm_list:
            name = item["name"]
            value = item["value"]

            if name == "ipv4.method":
                if value == "auto":
                    settings.use_dhcp = True
                elif value == "manual":
                    settings.use_dhcp = False
                else:
                    logging.error("error: unknown ipv4.method")

        return settings


    def get_all_network_settings() -> list:
        """This function returns list of NetworkSettingsEntity."""

        if not is_network_manager_installed():
            raise NetworkManagerNotInstalledError()

        out, err, code = exec_cmd_ex("LC_ALL=C nmcli --fields all device show")
        if code != 0:
            raise NetworkManagerError(code)
        # f = open("F:\\DichevGit\\trueconf-mcu\\web-dev\\stdout.log", "rt")
        # out = f.read()
        # f.close()

        prm_list = parse_params_list(out)
        sublists = params_list_to_sublists(prm_list)

        settings_list = []
        for sub_list in sublists:
            settings = param_list_to_network_settings(sub_list)

            if settings.ifname == "lo":
                continue
            if settings.type != "ethernet":
                continue

            if len(settings.conn_uuid) != 0 and settings.conn_uuid != "--":
                out2, err2, code2 = exec_cmd_ex("LC_ALL=C nmcli connection show %s" % (settings.conn_uuid))
                if code2 != 0:
                    raise NetworkManagerError(code2)
                conn_prm_list = parse_params_list(out2)
                # conn_prm_list = [ {"name": "ipv4.method", "value": "manual"} ]
                settings = update_network_settings_by_conn_params(conn_prm_list, settings)

            settings_list.append(settings)

        return settings_list


    def get_network_settings(request: GetNetworkSettingsRequest) -> NetworkSettingsEntity:
        """This function returns NetworkSettingsEntity."""

        settings_list = get_all_network_settings()
        for settings in settings_list:
            if settings.ifname == request.iface_name:
                return settings

        raise NetworkInterfaceNotFoundError(request.iface_name, "iface")


    def delete_connection(conn_uuid: str):
        out, err, code = exec_cmd_ex("LC_ALL=C nmcli connection delete %s" % (conn_uuid))
        if code != 0:
            raise NetworkManagerError(code)


    def ipaddr_list_to_cmd_option(ipaddr_list: list) -> str:
        """This function convevrt list of IPAddressMaskEntity to string 'ip/prefix, ...'."""

        result_str = ""
        for i in range(len(ipaddr_list)):
            ipaddr = ipaddr_list[i].ipaddr
            netmask = ipaddr_list[i].netmask
            prefix = netmask_to_prefix(ipaddr, netmask)
            ip_prefix_str = "%s/%d" % (ipaddr, prefix)
            result_str += ip_prefix_str
            if i != len(ipaddr_list) - 1:
                result_str += ","

        return result_str


    def nameservers_to_cmd_option(nameservers: list) -> str:
        """This function convert list of dns (strings) to string 'dns1,dns2, ...'."""
        result_str = ""
        for i in range(len(nameservers)):
            result_str += nameservers[i]
            if i != len(nameservers) - 1:
                result_str += ","

        return result_str


    def generate_connection_name():
        ind = int(random.random() * 0x7fffffff)
        conn_name = "connection%d" % (ind)
        return conn_name


    def add_connection(settings: NetworkSettingsEntity):

        conn_name = generate_connection_name()

        if settings.use_dhcp:
            out, err, code = exec_cmd_ex(
                """LC_ALL=C nmcli connection add save yes con-name "%s" ifname %s autoconnect yes type ethernet ipv4.method auto""" % (
                conn_name, settings.ifname))
        else:
            ip_list_str = ipaddr_list_to_cmd_option(settings.ipaddr_list)
            nameservers = nameservers_to_cmd_option(settings.nameservers)
            dns_str = """ ipv4.dns "%s" """ % (nameservers)
            out, err, code = exec_cmd_ex(
                """LC_ALL=C nmcli connection add save yes con-name "%s" ifname %s autoconnect yes type ethernet ipv4.method manual ipv4.addr "%s" ipv4.gateway %s %s """ % (
                conn_name, settings.ifname, ip_list_str, settings.gateway, dns_str))

        if code != 0:
            raise NetworkManagerError(code)


    def edit_connection(conn_uuid: str, settings: NetworkSettingsEntity):

        if settings.use_dhcp:
            out, err, code = exec_cmd_ex(
                """LC_ALL=C nmcli connection modify uuid %s ifname %s autoconnect yes ipv4.method auto""" % (
                conn_uuid, settings.ifname))
        else:
            ip_list_str = ipaddr_list_to_cmd_option(settings.ipaddr_list)
            nameservers = nameservers_to_cmd_option(settings.nameservers)
            dns_str = """ ipv4.dns "%s" """ % (nameservers)
            out, err, code = exec_cmd_ex(
                """LC_ALL=C nmcli connection modify uuid %s ifname %s autoconnect yes ipv4.method manual ipv4.addresses "%s" ipv4.gateway %s %s """ % (
                conn_uuid, settings.ifname, ip_list_str, settings.gateway, dns_str))

        if code != 0:
            raise NetworkManagerError(code)


    def switch_connection_on(conn_uuid: str):

        out, err, code = exec_cmd_ex("LC_ALL=C nmcli connection up uuid %s" % (conn_uuid))
        if code != 0:
            raise NetworkManagerError(code)


    def set_network_settings(request: ChangeNetworkSettingsRequest) -> NetworkSettingsEntity:
        """This function set new network settings and returns NetworkSettingsEntity."""
        ifname = request.settings.ifname

        # gew actual network settings
        get_request = GetNetworkSettingsRequest(ifname)
        actual_settings = get_network_settings(get_request)

        if len(actual_settings.conn_uuid) != 0 and actual_settings.conn_uuid != "--":
            edit_connection(actual_settings.conn_uuid, request.settings)
            switch_connection_on(actual_settings.conn_uuid)
        else:
            add_connection(request.settings)

        # wait for connection will be active
        CHECK_COUNT = 3
        WAIT_TIME = 3
        get_request2 = GetNetworkSettingsRequest(ifname)
        for i in range(CHECK_COUNT):
            new_settings = get_network_settings(get_request2)
            if not new_settings.ready:
                time.sleep(WAIT_TIME)
            else:
                return new_settings

        return new_settings


    def can_edit_network_settings() -> bool:
        return is_network_manager_installed()

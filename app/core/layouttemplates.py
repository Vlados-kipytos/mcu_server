#!/usr/bin/python3

import app.core.data as data
import app.core.layoutcfgtools as layoutcfgtools
import app.core.onlinelayouttools as onlinelayouttools

from app.core.entities import LayoutAbonentPositionEntity
from app.core.entities import OnlineLayoutAbonentEntity

from app.core.requests import CreateLayoutFromTemplateRequest
from app.core.requests import CreateGlobalLayoutRequest
from app.core.requests import CreateOnlineLayoutRequest
from app.core.requests import CreateTemplateFromOnlineLayoutRequest

from app.core.responses import IndividualOnlineLayoutResponse


def create_layout_from_template(request: CreateLayoutFromTemplateRequest) -> IndividualOnlineLayoutResponse:
    layout_template = layoutcfgtools.get_layout_by_id(request.layout_template_id)

    participant_pos_list = []
    for slot in layout_template.wndlist:
        participant_pos_list.append(OnlineLayoutAbonentEntity(slot.wndnum, slot.abid, "", True, slot.role_id, -1,
                                                              slot.render_mode))

    req = CreateOnlineLayoutRequest(request.confid, request.participant_id, layout_template.cells_count,
                                    layout_template.schema_type, participant_pos_list, layout_template.auto,
                                    request.layout_type, request.connection_id, layout_template.display_name, True)
    result = onlinelayouttools.create_online_layout_ex(req)
    return result


def create_template_from_online_layout(request: CreateTemplateFromOnlineLayoutRequest):
    onlinelayout = onlinelayouttools.get_online_layout_by_name(request.confid, request.layout_name, "", "")

    display_name = onlinelayout.display_name.strip()
    if len(display_name) == 0 and len(request.display_name) != 0:
        display_name = request.display_name

    participants_list = []
    for slot in onlinelayout.participant_positions:
        participants_list.append(
            LayoutAbonentPositionEntity(slot.position, slot.participant_id, data.PRIORITY_NORMAL, slot.role_id,
                                        slot.render_mode))

    new_request = CreateGlobalLayoutRequest(request.confid, onlinelayout.auto, onlinelayout.cells_count,
                                            onlinelayout.schema_type, participants_list, display_name,
                                            True)

    new_layout = layoutcfgtools.create_layout(new_request)
    return new_layout




#!/usr/bin/python3

import logging

import app.core.dbtools as dbtools

from app.core.entities import CustomLayoutTypeEntity
from app.core.entities import CustomLayoutTypeSlotEntity
from app.core.entities import LayoutWindowCoordEntity

from app.core.errors import CustomLayoutTypeChangeError
from app.core.errors import CustomLayoutTypeCreateError
from app.core.errors import CustomLayoutTypeDeleteError
from app.core.errors import CustomLayoutTypeIsUsedError
from app.core.errors import CustomLayoutTypeNotFoundError

from app.core.requests import ChangeCustomLayoutTypeRequest
from app.core.requests import CreateCustomLayoutTypeRequest

from app.core.responses import CalculateLayoutResponse
from app.core.responses import CustomLayoutTypeResponse




CUSTOM_LAYOUT_TYPE_SHIFT = 1000000


def id_to_layout_type(id: int) -> int:
    return id + CUSTOM_LAYOUT_TYPE_SHIFT


def layout_type_to_id(layout_type: int) -> int:
    return layout_type - CUSTOM_LAYOUT_TYPE_SHIFT


def is_custom_layout_type(layout_type: int) -> bool:
    return layout_type >= CUSTOM_LAYOUT_TYPE_SHIFT


def num_to_db_num(_num: int) -> str:
    if _num is not None:
        return str(_num)
    else:
        return "null"


def get_custom_layout_type(layout_type: int) -> CustomLayoutTypeResponse:
    """This function returns description of custom layout type."""

    id = layout_type_to_id(layout_type)

    lyt_type_rows = dbtools.select("SELECT id, width, height, centered_row FROM legacy.layouttypes WHERE id=%d" % (id))
    if len(lyt_type_rows) == 0:
        raise CustomLayoutTypeNotFoundError(layout_type, "layout_type")

    full_width = lyt_type_rows[0][1]
    full_height = lyt_type_rows[0][2]
    centered_row = lyt_type_rows[0][3]

    slots_list = []
    slots_rows = dbtools.select("SELECT x,y,width,height FROM legacy.layouttypeslots WHERE layout_type_id=%d" % (id))
    for slot in slots_rows:
        x = slot[0]
        y = slot[1]
        width = slot[2]
        height = slot[3]
        slots_list.append( CustomLayoutTypeSlotEntity(x, y, width, height) )

    custom_layout = CustomLayoutTypeEntity(layout_type, full_width, full_height, centered_row, slots_list)
    return CustomLayoutTypeResponse(custom_layout)


def get_all_custom_layout_types() -> list:
    """This function returns descriptions of all custom layout types."""

    # layout types
    lyt_types_rows = dbtools.select("SELECT id, width, height, centered_row FROM legacy.layouttypes")

    # layout types slots: build dict[layout_type_id] = [ CustomLayoutTypeSlotEntity(), ... ]
    slots_dict = {}
    slots_rows = dbtools.select("SELECT layout_type_id,x,y,width,height FROM legacy.layouttypeslots")
    for slot in slots_rows:
        id = slot[0]
        x = slot[1]
        y = slot[2]
        width = slot[3]
        height = slot[4]
        if id not in slots_dict:
            slots_dict[id] = []
        slots_dict[id].append( CustomLayoutTypeSlotEntity(x, y, width, height) )

    layout_types_list = []
    for layout_type_item in lyt_types_rows:
        id = layout_type_item[0]
        full_width = layout_type_item[1]
        full_height = layout_type_item[2]
        centered_row = layout_type_item[3]
        slots_list = []
        if id in slots_dict:
            slots_list = slots_dict[id]
        layout_type = id_to_layout_type(id)
        layout_types_list.append(CustomLayoutTypeEntity(layout_type, full_width, full_height, centered_row, slots_list))

    return layout_types_list


def is_custom_layout_type_used(layout_type: int) -> bool:
    """This function returns True if specified layout type is used in some layouts.'"""

    types_rows = dbtools.select("SELECT type FROM legacy.layoutconfigs WHERE type=%d" % (layout_type))
    if len(types_rows) != 0:
        return True

    types_rows2 = dbtools.select("SELECT type FROM legacy.layoutbackup WHERE type=%d" % (layout_type))
    if len(types_rows2) != 0:
        return True

    return False


def create_new_custom_layout_type(request: CreateCustomLayoutTypeRequest) -> CustomLayoutTypeResponse:
    """This function create new custom layout type."""

    sql = """INSERT INTO legacy.layouttypes (width, height, centered_row) 
          VALUES (%d, %d, %s) RETURNING id""" % (request.width, request.height, num_to_db_num(request.centered_row))
    id = dbtools.insert_with_serial(sql)
    if id == 0:
        raise CustomLayoutTypeCreateError("legacy.layouttypes")

    for slot in request.selected_slots:
        sql = """INSERT INTO legacy.layouttypeslots (layout_type_id, x, y, width, height)
        VALUES (%d, %d, %d, %d, %d) RETURNING id""" % (id, slot.x, slot.y, slot.width, slot.height)
        slot_id = dbtools.insert_with_serial(sql)
        if slot_id == 0:
            raise CustomLayoutTypeCreateError("legacy.layouttypeslots")

    layout_type = id_to_layout_type(id)
    result = get_custom_layout_type(layout_type)
    return result


def delete_custom_layout_type(layout_type: int):
    """This function delete custom layout type."""

    # check: is layout type exist?
    tmp_layout_type = get_custom_layout_type(layout_type)

    if is_custom_layout_type_used(layout_type):
        raise CustomLayoutTypeIsUsedError("schema_type", layout_type)

    id = layout_type_to_id(layout_type)

    res = dbtools.make_sql_request("DELETE FROM legacy.layouttypeslots WHERE layout_type_id=%d" % (id))
    if res == 0:
        raise CustomLayoutTypeDeleteError("legacy.layouttypeslots")

    res = dbtools.make_sql_request("DELETE FROM legacy.layouttypes WHERE id=%d" % (id))
    if res == 0:
        raise CustomLayoutTypeDeleteError("legacy.layouttypes")


def change_custom_layout_type(request: ChangeCustomLayoutTypeRequest) -> CustomLayoutTypeResponse:
    """This function change specified custom layout type."""

    if is_custom_layout_type_used(request.type.layout_type):
        raise CustomLayoutTypeIsUsedError("schema_type", request.type.layout_type)

    centered_row_db = num_to_db_num(request.type.centered_row)
    id = layout_type_to_id(request.type.layout_type)

    sql = """UPDATE legacy.layouttypes SET width=%d, height=%d, centered_row=%s WHERE id=%d""" % \
          (request.type.width, request.type.height, centered_row_db, id)
    res = dbtools.make_sql_request(sql)
    if res == 0:
        raise CustomLayoutTypeChangeError("legacy.layouttypes")

    res = dbtools.make_sql_request("DELETE FROM legacy.layouttypeslots WHERE layout_type_id=%d" % (id))
    if res == 0:
        raise CustomLayoutTypeDeleteError("legacy.layouttypeslots")

    for slot in request.type.selected_slots:
        sql = """INSERT INTO legacy.layouttypeslots (layout_type_id, x, y, width, height)
        VALUES (%d, %d, %d, %d, %d) RETURNING id""" % (id, slot.x, slot.y, slot.width, slot.height)
        slot_id = dbtools.insert_with_serial(sql)
        if slot_id == 0:
            raise CustomLayoutTypeCreateError("legacy.layouttypeslots")

    result = get_custom_layout_type(request.type.layout_type)
    return result


def is_point_in_rect(px, py, rx, ry, rw, rh):
    """This function returns True if point (px, py) is in rectangle (rx, ry, rw, rh)"""
    x_in_rect = px >= rx and px <= rx + rw - 1
    y_in_rect = py >= ry and py <= ry + rh - 1
    return x_in_rect and y_in_rect


def slot_coord_to_screen_coord(screen_width: int, screen_height: int, col_count: int, row_count: int,
                               slot: CustomLayoutTypeSlotEntity) -> CustomLayoutTypeSlotEntity:
    """This function convert indexes of row/column to coondinates in screen (pixels)."""

    if row_count == 0 or col_count == 0:
        logging.error("slot_coord_to_screen_coord() error: row_count=%d , col_count = %d" % (row_count, col_count))
        return CustomLayoutTypeSlotEntity(0,0,0,0)

    wnd_x1 = int(slot.x * screen_width / col_count)
    wnd_y1 = int(slot.y * screen_height / row_count)
    wnd_x2 = int((slot.x + slot.width) * screen_width / col_count)
    wnd_y2 = int((slot.y + slot.height) * screen_height / row_count)
    wnd_w = wnd_x2 - wnd_x1
    wnd_h = wnd_y2 - wnd_y1

    return(CustomLayoutTypeSlotEntity(wnd_x1, wnd_y1, wnd_w, wnd_h))


def get_custom_layout_max_slots_count(type: CustomLayoutTypeEntity) -> int:
    """this function returns max cells count for specified custom layout type."""
    count = 0

    # calculate small slots count
    for y in range(type.height):
        for x in range(type.width):
            found = False
            for slot in type.selected_slots:
                if is_point_in_rect(x, y, slot.x, slot.y, slot.width, slot.height):
                    found = True
                    break
            if not found:
                count += 1

    count += len(type.selected_slots)
    return count


def is_rectangles_intersected(x1, y1, w1, h1, x2, y2, w2, h2) -> bool:
    x_intersect = not (x2+w2-1<x1 or x1+w1-1<x2)
    y_intersect = not (y2+h2-1<y1 or y1+h1-1<y2)
    return x_intersect and y_intersect


def calc_custom_layout_type_coord(cells_count: int, screen_width: int, screen_height: int,
                                  type: CustomLayoutTypeEntity) -> CalculateLayoutResponse:
    """This function calculate coordinates of all slots of custom layout type.
    If cells_count == None, then return coordinates of all slots."""

    result_wnd_list = []

    slot_num = 1

    # add selected slots to layout wnd list
    for y in range(type.height):
        for x in range(type.width):
            for slot in type.selected_slots:
                if x == slot.x and y == slot.y:
                    wnd_slot = slot_coord_to_screen_coord(screen_width, screen_height, type.width, type.height, slot)
                    result_wnd_list.append( LayoutWindowCoordEntity(slot_num, wnd_slot.x, wnd_slot.y, wnd_slot.width,
                                                                    wnd_slot.height, 0) )
                    slot_num += 1
                    if (cells_count is None) or ((cells_count is not None) and slot_num > cells_count):
                        #return CalculateLayoutResponse(result_wnd_list)
                        break

    # add common slots to layout wnd list
    centered_wnd_list = []
    for y in range(type.height):
        for x in range(type.width):
            small_slot = CustomLayoutTypeSlotEntity(x, y, 1, 1)
            intersect = False
            for slot in type.selected_slots:
                if is_point_in_rect(x, y, slot.x, slot.y, slot.width, slot.height):
                    intersect = True
                    break
            if not intersect:
                wnd_slot = slot_coord_to_screen_coord(screen_width, screen_height, type.width, type.height, small_slot)
                result_wnd_list.append( LayoutWindowCoordEntity(slot_num, wnd_slot.x, wnd_slot.y, wnd_slot.width,
                                                                wnd_slot.height, 0) )
                if (type.centered_row is not None) and type.centered_row == y:
                    centered_wnd_list.append(slot_num) # save nums of centered windows (small slots in centered row)
                slot_num += 1
                if (cells_count is None) or ((cells_count is not None) and slot_num > cells_count):
                    #return CalculateLayoutResponse(result_wnd_list)
                    break

    if (type.centered_row is not None) and type.width > 0:
        centered_row_shift = int((screen_width - len(centered_wnd_list) * screen_width / type.width) / 2)
        for i in range(len(result_wnd_list)):
            if result_wnd_list[i].position in centered_wnd_list:
                result_wnd_list[i].x = result_wnd_list[i].x + centered_row_shift

    return CalculateLayoutResponse(result_wnd_list)


def calc_custom_layout_type_coord2(cells_count: int, screen_width: int, screen_height: int, layout_type: int,
                                   all_custom_layout_types: list) -> CalculateLayoutResponse:
    """This function calculate coordinates of all slots of custom layout type.
    If cells_count == None, then return coordinates of all slots."""

    for custom_layout_type in all_custom_layout_types:
        if custom_layout_type.layout_type == layout_type:
            return calc_custom_layout_type_coord(cells_count, screen_width, screen_height, custom_layout_type)

    # layout type is not found
    logging.error("calc_custom_layout_type_coord2() error: custom layout type is not found)")
    logging.error("calc_custom_layout_type_coord2() layout_type=%d" % (layout_type))

    raise CustomLayoutTypeNotFoundError(layout_type, "schema_type")
    #return CalculateLayoutResponse([])


def get_custom_layout_type_cmd(type: CustomLayoutTypeEntity, cid: str, lid: str) -> str:
    """this function returns command to set custom layout type for layout 'lid' in conference 'cid'."""

    cmd = "LAYOUT MODE CUSTOM %s %s %d %d" % (cid, lid, type.width, type.height)

    slots_count = len(type.selected_slots)
    if slots_count > 0:
        cmd += " S%d" % (slots_count)
        for slot in type.selected_slots:
            cmd += " %d %d %d %d" % (slot.x, slot.y, slot.width, slot.height)

    if type.centered_row is not None:
        cmd += " C%d" % (type.centered_row)

    return cmd


def get_custom_layout_type_cmd2(all_custom_layout_types: list, layout_type: int, cid: str, lid: str) -> str:
    """this function returns command to set custom layout type for layout 'lid' in conference 'cid'."""

    for custom_layout_type in all_custom_layout_types:
        if custom_layout_type.layout_type == layout_type:
            return get_custom_layout_type_cmd(custom_layout_type, cid, lid)

    # layout type is not found
    logging.error("get_custom_layout_type_cmd2() error: custom layout type is not found)")
    logging.error("get_custom_layout_type_cmd2() layout_type=%d" % (layout_type))

    return ""


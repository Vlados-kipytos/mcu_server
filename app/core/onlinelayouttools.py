#!/usr/bin/python3

import logging
import sys

import app.core.addrbook as addrbook
import app.core.constants as constants
import app.core.customlayouttypes as customlayouttypes
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.layoutbackup as layoutbackup
import app.core.layoutcfgtools as layoutcfgtools
import app.core.nettools as nettools
import app.core.onlinetools as onlinetools
import app.core.sysinfotools as sysinfotools
import app.core.tools as tools
from .entities import OnlineLayoutAbonentEntity
from .errors import ActiveConferenceNotFoundError
from .errors import BadLayoutNameError
from .errors import ConcurrentLayoutsError
from .errors import DeleteAspeakerLayoutError
from .errors import DeleteGlobalLayoutError
from .errors import GetOnlineStateError
from .errors import LayoutOfAbonentAlreadyExistError
from .errors import MaxActiveParticipantsError
from .errors import OnlineLayoutNotFoundError
from .errors import SendVideoToRTSPError
from .errors import TooManyLayoutsError
from .requests import ChangeOnlineLayoutRequest
from .requests import CreateOnlineLayoutRequest
from .requests import GetContactRequest
from .responses import DeleteOnlineLayoutResponse
from .responses import GlobalOnlineLayoutResponse
from .responses import IndividualOnlineLayoutResponse




def get_online_layouts_count(_confstr):
    """This fiunction returns count of layouts in all launched conferences"""
    true = True  # define constants "true"/"false"  (they is present in input expression "connstr")
    false = False
    allconfdict = {}
    try:
        allconfdict = eval(_confstr)  # convert string to python expression (json format)
    except:
        logging.error("get_online_layouts_count() exceprion: " + str(sys.exc_info()))
        return (0)
    if "conference_list" not in allconfdict:
        return (0)
    conflist = allconfdict["conference_list"]  # extract conferences list

    lyt_cnt = 0
    for conf in conflist:
        if "layout_list" not in conf:
            continue
        for lyt in conf["layout_list"]:
            if lyt["name"] == "DEFAULT" or lyt["name"] == "PRESENTATION":
                continue
            lyt_cnt += 1

    return (lyt_cnt)


def get_online_layouts_count2():
    """This fiunction returns count of layouts in all launched conferences"""
    # get online state from buffer (in database)

    # get online state from buffer (in database)
    connstr, confstr = onlinetools.get_online_state()

    if confstr == "":
        confstr = '{ "conference_list": [] }'

    lyt_cnt = get_online_layouts_count(confstr)
    return (lyt_cnt)


def get_layout_owner_conn_id(layout_json: dict) -> int:
    if layout_json != {}:
        if len(layout_json["tx"]["connections"]) != 0:
            ab = layout_json["tx"]["connections"][0]
            return ab["id"]
    return -1


def change_layout_owners(_conf_json: dict, _from_layout_name: str, _to_layout_name: str, _cmd_list: list):
    """This function make all owners of layout _from_layout_name owners of _to_layout_name.
    (Build commands list for engine.)"""
    if "name" not in _conf_json:
        raise GetOnlineStateError()

    cid = _conf_json["name"]

    from_layout_json = onlinetools.extract_layout_by_name(_conf_json, _from_layout_name)
    if from_layout_json == {}:
        raise OnlineLayoutNotFoundError(_from_layout_name, "layout_name")

    for ab in from_layout_json["tx"]["connections"]:
        conn_id = ab["id"]
        conn_id_str = "#%d" % (conn_id)
        _cmd_list.append("LAYOUT SHOW %s %s %s" % (cid, _to_layout_name, conn_id_str))


def get_owner_disp_name(owner_contact_id: int) -> str:
    """This function returns name/display_name of specified contact in address book."""

    request = GetContactRequest(owner_contact_id)
    response = addrbook.get_contact_by_id(request)

    if response.contact.display_name != "":
        return response.contact.display_name

    return response.contact.name


def get_online_layout_by_name(_confid, _layout_name, _connstr, _confstr):
    """This function get from engine vtsrv layout _layout_name in conference _confid."""
    _confid = int(_confid)

    # check layout name
    layout_id = tools.get_layout_id_by_name(_layout_name)
    if layout_id == 0:
        raise BadLayoutNameError(_layout_name)

    # get online state from buffer (in database)
    if _connstr == "" or _confstr == "":
        _connstr, _confstr = onlinetools.get_online_state()
    if _connstr == "" or _confstr == "":
        raise GetOnlineStateError()

    conf_json = onlinetools.extract_conference(_confstr, _confid)
    if conf_json == {}:
        raise ActiveConferenceNotFoundError(_confid, "conference_id")

    layout_json = onlinetools.extract_layout_by_name(conf_json, _layout_name)
    # if layout_json == {}:
    #    raise OnlineLayoutNotFoundError(_layout_name, "layout_name")

    conn_list = onlinetools.extract_conn_list(_connstr)

    ab_names_dict = addrbook.get_all_abonent_names_dict()

    # get planned backup of layout
    planned_layout = layoutbackup.get_backup_of_layout(_confid, layout_id)

    # get abonents list in layout
    ab_list = []
    # num_ab = layout_json["restriction"]
    num_ab = planned_layout.cells_count

    if planned_layout.auto:
        if layout_json != {}:
            for ab in layout_json["rx"]["connections"]:
                ab_uri = ab["uri"]
                conn_id = ab["id"]
                ab_name = ab["display_name"]
                ab_id = addrbook.get_ab_id_by_conn_id(conn_id)
                #OLD:ab_id = addrbook.get_ab_id_by_realaddr(ab_uri)

                b_outgoing = onlinetools.is_conn_id_outgoing(conn_list, conn_id)
                if (b_outgoing or len(ab_name) == 0) and (ab_id in ab_names_dict):
                    ab_name = ab_names_dict[ab_id]["name"]

                position = 0
                if "position" in ab["rendering"]:
                    position = ab["rendering"]["position"] + 1
                ab_list.append(OnlineLayoutAbonentEntity(position, ab_id, ab_name, True, layoutcfgtools.AUTO_ROLE_ID,
                                                         conn_id, "AUTO"))
        num_ab = len(ab_list)
    else:
        for index in range(num_ab):
            wnd_num = index + 1

            # get real participant of layout (if exist)
            ab_uri = ""
            ab_name = ""
            conn_id = -1
            if layout_json != {}:
                for ab in layout_json["rx"]["connections"]:
                    if "position" not in ab["rendering"]:
                        continue
                    if index == ab["rendering"]["position"]:
                        ab_uri = ab["uri"]
                        ab_name = ab["display_name"]
                        conn_id = ab["id"]
                        break

            # get planned participant of layout
            ab_id = 0
            role_id = layoutcfgtools.DEFAULT_EMPTY_ROLE_ID
            render_mode = "AUTO"
            for ab_pos in planned_layout.wndlist:
                if ab_pos.wndnum == wnd_num:
                    ab_id = ab_pos.abid
                    role_id = ab_pos.role_id
                    render_mode = ab_pos.render_mode # get planned value
                    break
            if role_id != layoutcfgtools.DEFAULT_FILLED_ROLE_ID: # for auto slots calculate actual participant_id
                ab_id = addrbook.get_ab_id_by_conn_id(conn_id)

            # get real render mode from map of layout
            render_mode_ind = onlinetools.get_render_mode_ind_from_map_pos(layout_json,wnd_num)
            if render_mode_ind >= 0:
                render_mode = data.get_render_participant_name(render_mode_ind)

            b_outgoing = onlinetools.is_conn_id_outgoing(conn_list, conn_id)
            if (b_outgoing or len(ab_name) == 0) and (ab_id in ab_names_dict):
                ab_name = ab_names_dict[ab_id]["name"]

            if ab_uri != "":
                ab_list.append(OnlineLayoutAbonentEntity(wnd_num, ab_id, ab_name, True, role_id, conn_id, render_mode))
            else:
                ab_list.append(OnlineLayoutAbonentEntity(wnd_num, ab_id, "", False, role_id, conn_id, render_mode))

    # -----------------------------------
    # get type number of layout from json
    # -----------------------------------
    # OLD: eff_lyt_type_num = layoutcfgtools.DEFAULT_EQUAL_TYPE

    # olways show PLANNED schema type
    eff_lyt_type_num = planned_layout.schema_type

    # if "layout_type" in layout_json:
    #    eff_json_type = layout_json["layout_type"]
    #    eff_lyt_type_num = layoutcfgtools.get_layout_type_num(eff_json_type)
    # -----------------------------------

    layout_type = planned_layout.layout_type

    if layout_type == "global":
        return GlobalOnlineLayoutResponse(_layout_name, layout_type, num_ab, eff_lyt_type_num, ab_list,
                                          planned_layout.auto, planned_layout.display_name, planned_layout.template)
    elif layout_type == "active_speaker":
        return GlobalOnlineLayoutResponse(_layout_name, layout_type, num_ab, eff_lyt_type_num, ab_list,
                                          planned_layout.auto, planned_layout.display_name, planned_layout.template)
    else:
        participant_id = 0
        owner_disp_name = ""
        connection_id = -1
        if layout_type == "individual":
            participant_id = planned_layout.participant_id
            owner_disp_name = get_owner_disp_name(participant_id)
            connection_id = get_layout_owner_conn_id(layout_json)
        return IndividualOnlineLayoutResponse(_layout_name, layout_type, num_ab, eff_lyt_type_num, ab_list,
                                              participant_id, owner_disp_name, planned_layout.auto,
                                              connection_id, planned_layout.display_name, planned_layout.template)


def get_online_layouts_list(_confid):
    """This function returns list of layouts of conference _confid (in active session)"""

    connstr, confstr = onlinetools.get_online_state()
    if connstr == "" or confstr == "":
        raise GetOnlineStateError()

    conf_json = onlinetools.extract_conference(confstr, _confid)
    if conf_json == {}:
        raise ActiveConferenceNotFoundError(_confid, "conference_id")

    layout_id_list = layoutbackup.get_layouts_id_of_conference(_confid)

    layout_list = []
    for layout_id in layout_id_list:
        layout_name = tools.get_layout_name_by_id(_confid, layout_id)
        layout_list.append(get_online_layout_by_name(_confid, layout_name, connstr, confstr))

    return layout_list


def is_ab_not_in_other_layouts(_conf_json, _lid, _ab_uri):
    """This function returns True if abonent _ab_uri not present in all layouts exclude _lid
    (abonent may be or may not be in _lid). Else returns False."""
    if "layout_list" not in _conf_json:
        return False  # error

    for lyt in _conf_json["layout_list"]:
        cur_lid = lyt["name"]
        for ab in lyt["rx"]["connections"]:
            cur_ab_uri = ab["uri"]
            if cur_lid != _lid and cur_ab_uri == _ab_uri:
                return False

    return True


def is_conn_id_not_in_other_layouts(_conf_json: dict, _lid: str, _conn_id: int):
    """This function returns True if abonent _conn_id not present in all layouts exclude _lid
    (abonent may be or may not be in _lid). Else returns False."""
    if "layout_list" not in _conf_json:
        return False  # error

    for lyt in _conf_json["layout_list"]:
        cur_lid = lyt["name"]
        for ab in lyt["rx"]["connections"]:
            cur_conn_id = ab["id"]
            if cur_lid != _lid and cur_conn_id == _conn_id:
                return False

    return True


def get_not_fixed_hidden_abonents(_confid: int, _layout_name: str, _participant_positions: list,
                                  _conf_ab_list: list, _conf_json: dict) -> list:
    """This function returns list of abonets URI, who can come to 'not fixed' slots of layout _layout_name
    and not showed in other layouts (so called 'hidden')."""

    # get participants who is added to layout as concrete (fixed) peers
    fixed_peers_list = []
    for position in _participant_positions:
        if position.role_id == layoutcfgtools.DEFAULT_FILLED_ROLE_ID and position.participant_id != None and position.participant_id != 0:
            participant_uri = addrbook.get_realaddr_by_ab_id(position.participant_id)
            fixed_peers_list.append({ "aburi": participant_uri, "conn_id": position.connection_id })

    # get not fixed peers list
    not_fixed_peers_list = []
    for ab in _conf_ab_list:
        aburi = ab["uri"]
        conn_id = ab["id"]
        found = False
        for fixed_peer in fixed_peers_list:
            if fixed_peer["conn_id"] > 0:
                if fixed_peer["conn_id"] == conn_id:
                    found = True
                    break
            elif tools.is_addr_equivalent(fixed_peer["aburi"], aburi):
                found = True
                break
        if (not found) and is_conn_id_not_in_other_layouts(_conf_json, _layout_name, conn_id):
            not_fixed_peers_list.append(aburi)

    return not_fixed_peers_list


def validate_participants_added_to_layout(_confid: int, _new_layout_name: str, _confstr: str, _conf_ab_list: list,
                                          _ab_pos_list: list, _psevdo_del_list: list, _other_deleted_count: int,
                                          _connstr: str):
    """Check max active abonents restriction. If max, raise MaxActiveParticipantsError.
    _confstr - string with all conferences list (conferences.json)
    _conf_ab_list - list of conference abonents (include presentation)
    _ab_pos_list - list of OnlineLayoutAbonentEntity
    _psevdo_del_list - list of URI of concrete participants marked as invisible
    _other_deleted_count - count of unknown participants marked as invisible."""

    max_visible_ab = sysinfotools.get_abonents_restriction()["max_visible"]

    conf_list = onlinetools.extract_conf_list(_confstr)
    visible_ab_list = onlinetools.extract_all_active_abonents(conf_list)

    conf_json = onlinetools.extract_conference(_confstr, _confid)

    # find presentation
    presentation_uri = onlinetools.get_presentation_uri(_conf_ab_list, _connstr)

    logging.error("validate_participants_added_to_layout():")

    # calculate count of not fixed 'hidden' abonents, who can come to not fixed slots
    not_fixed_hidden_ab_list = get_not_fixed_hidden_abonents(_confid, _new_layout_name, _ab_pos_list, _conf_ab_list,
                                                             conf_json)
    not_fixed_ab_cnt = len(not_fixed_hidden_ab_list)
    logging.error("not_fixed_ab_cnt=%d" % (not_fixed_ab_cnt))

    # calculate count of active (visible) participants
    added_ab = 0
    not_fixed_ab = 0
    for ab_pos in _ab_pos_list:
        role_name = layoutcfgtools.role_id_to_name(ab_pos.role_id)
        logging.error("position=%d , role_id=%d , role_name=%s" % (ab_pos.position, ab_pos.role_id, role_name))

        cur_role = ab_pos.role_id
        if cur_role == layoutcfgtools.DEFAULT_EMPTY_ROLE_ID:
            continue  # skip slots without participants

        if cur_role == layoutcfgtools.DEFAULT_FILLED_ROLE_ID:  # concrete peer
            #visible_participant = False
            participant_uri = ""
            conn_id = ab_pos.connection_id
            if ab_pos.participant_id != None and ab_pos.participant_id != 0:  # is participant active (visible)?
                participant_uri = addrbook.get_realaddr_by_ab_id(ab_pos.participant_id)
                #for ab in visible_ab_list:
                #    if participant_uri != "" and tools.is_addr_equivalent(ab["uri"], participant_uri):
                #        visible_participant = True
                #        logging.error("visible_participant = True")
                #        break

            # deleted = False
            # for ab_uri in _psevdo_del_list:
            #    if participant_uri != "" and tools.is_addr_equivalent(ab_uri, participant_uri):
            #        deleted = True
            #        logging.error("deleted = True")
            #        break

            if (conn_id is not None) and conn_id >= 0:
                ab_add_to_visible = is_conn_id_not_in_other_layouts(conf_json, _new_layout_name, conn_id)
            else:
                ab_add_to_visible = is_ab_not_in_other_layouts(conf_json, _new_layout_name, participant_uri)
            if participant_uri != "" and ab_add_to_visible:  # participant is not active: try make it active (check)
                added_ab += 1
                logging.error("participant_uri=%s (added_ab+1)" % (participant_uri))
        elif cur_role == layoutcfgtools.PRESENTATION_ROLE_ID:
            if presentation_uri != "":
                added_ab += 1
                logging.error("presentation_uri=%s (added_ab+1)" % (presentation_uri))
        else:  # some (unknown) abonent
            if not_fixed_ab < not_fixed_ab_cnt:
                logging.error(
                    "other slot (added_ab+1): not_fixed_ab=%d , not_fixed_ab_cnt=%d" % (not_fixed_ab, not_fixed_ab_cnt))
                not_fixed_ab += 1
                added_ab += 1
            else:
                logging.error("other slot (no more 'hidden' abonents for this slot)")

        logging.error(
            "visible_ab_count=%d , psevdo_del_count=%d , added_ab=%d , _other_deleted_count=%d , max_visible_ab=%d" %
            (len(visible_ab_list), len(_psevdo_del_list), added_ab, _other_deleted_count, max_visible_ab))

        if len(visible_ab_list) - len(_psevdo_del_list) + added_ab - _other_deleted_count > max_visible_ab:
            raise MaxActiveParticipantsError()


def validate_participants_added_to_layout2(_confid: int, _confstr: str, _conf_ab_list: list, _connstr: str):
    """Check max active abonents restriction. If max, raise MaxActiveParticipantsError.
    _confstr - string with all conferences list (conferences.json)
    _conf_ab_list - list of conference abonents (include presentation).
    (check creating layout with all abonents of conference)"""

    max_visible_ab = sysinfotools.get_abonents_restriction()["max_visible"]

    conf_list = onlinetools.extract_conf_list(_confstr)
    visible_ab_list = onlinetools.extract_all_active_abonents(conf_list)

    conf_json = onlinetools.extract_conference(_confstr, _confid)

    # find presentation
    presentation_uri = onlinetools.get_presentation_uri(_conf_ab_list, _connstr)

    logging.error("validate_participants_added_to_layout2():")

    # calculate count of active (visible) participants
    added_ab = 0
    for ab in _conf_ab_list:
        participant_uri = ab["uri"]
        conn_id = ab["id"]

        ab_add_to_visible = is_conn_id_not_in_other_layouts(conf_json, "", conn_id)
        if participant_uri != "" and ab_add_to_visible:  # participant is not active: try make it active (check)
            added_ab += 1
            logging.error("participant_uri=%s (added_ab+1)" % (participant_uri))

        logging.error(
            "visible_ab_count=%d , added_ab=%d , max_visible_ab=%d" % (len(visible_ab_list), added_ab, max_visible_ab))

        if len(visible_ab_list) + added_ab > max_visible_ab:
            raise MaxActiveParticipantsError()


def attach_not_fixed_conf_abonents(_confid: int, _layout_name: str, _participant_positions: list,
                                   _conf_ab_list: list) -> list:
    """This function build commands list to attach to layout '_layout_name' of conference '_confid'
    all participants of conference if layout '_layout_name' has 'not fixed peer' slots.
    _confid - id of conference,
    _layout_name - name of layout,
    _participant_positions - list of OnlineLayoutAbonentEntity (not attached participants),
    _conf_ab_list - list of all abonents connected to conference '_confid',
    _lyt_ab_dict - dictionary of audio flags: _lyt_ab_dict[ab_uri] = use_audio."""

    cid = tools.conference_name_by_id(_confid)

    # get participants who is added to layout as concrete (fixed) peers
    exclude_peers_list = []
    add_peers = False
    for position in _participant_positions:
        if position.role_id == layoutcfgtools.DEFAULT_FILLED_ROLE_ID and position.participant_id != None and position.participant_id != 0:
            participant_uri = addrbook.get_realaddr_by_ab_id(position.participant_id)
            exclude_peers_list.append({"aburi": participant_uri, "conn_id": position.connection_id})
        if layoutcfgtools.is_role_not_fixed_peer(position.role_id):
            add_peers = True

    # if "not fixed peer" slot(s) found, then build commands to attach all others (not fixed) peers to layout
    cmd_list = []
    if add_peers:
        for ab in _conf_ab_list:
            added_ab_uri = ab["uri"]
            added_conn_id = ab["id"]
            found = False
            for exclude_peer in exclude_peers_list:
                if exclude_peer["conn_id"] > 0:
                    if exclude_peer["conn_id"] == added_conn_id:
                        found = True
                        break
                elif tools.is_addr_equivalent(exclude_peer["aburi"], added_ab_uri):
                    found = True
                    break
            if not found:
                logging.debug("attach not fixed abonent (see command down): " + added_ab_uri)
                conn_id_str = "#%d" % (added_conn_id)
                cmd_list.append("LAYOUT ATTACH " + cid + " " + _layout_name + " " + conn_id_str)
                cmd_list.append("SET MUTE OFF VIDEO " + conn_id_str)
                # TMP: FIX BUG 4685
                # if _lyt_ab_dict != {} and (added_ab_uri in _lyt_ab_dict):
                #    useaudio = _lyt_ab_dict[added_ab_uri]["useaudio"]
                # else:
                #    useaudio = 1
                # if useaudio:
                #    cmd_list.append("SET MUTE OFF AUDIO " + added_ab_uri)

    return cmd_list


def generate_unique_layout_id(confid: int, confstr: str) -> int:
    """This function returns unique id of new layout."""

    conf_list = onlinetools.extract_conf_list(confstr)
    new_num = 1000000000
    while 1:
        duplicate_found = 0
        for conf in conf_list:
            for lyt in conf["layout_list"]:
                if tools.get_layout_name_by_id(confid, new_num) == lyt["name"]:
                    duplicate_found = 1
                    break
            if duplicate_found:
                break
        if duplicate_found:
            new_num += 1
        else:
            break

    return new_num


def create_online_layout(request: CreateOnlineLayoutRequest):
    """Create online layout with 'auto' == False."""

    # get conference's online state
    connstr, confstr = onlinetools.get_online_state()
    conf_json = onlinetools.extract_conference(confstr, request.confid)
    if conf_json == {}:
        raise ActiveConferenceNotFoundError(request.confid, "conference_id")

    if "layout_list" not in conf_json:
        raise GetOnlineStateError()

    online_lyt_cnt = get_online_layouts_count2()
    max_layouts_cnt = sysinfotools.get_abonents_restriction()["max_layouts"]
    if online_lyt_cnt >= max_layouts_cnt:
        raise TooManyLayoutsError()

    conf_ab_list = onlinetools.extract_abonents_list(conf_json, 1, connstr)

    # --- check max active abonents restriction ---
    validate_participants_added_to_layout(request.confid, "", confstr, conf_ab_list, request.participant_positions, [],
                                          0, connstr)
    # ---------------------------------------------

    # don't use active speker layout if crown layout exist
    if request.layout_type == "active_speaker":
        crown_layout = onlinetools.get_crown_layout(request.confid, conf_json)
        if crown_layout != {}:
            raise ConcurrentLayoutsError("active speaker", "crown")

    # check owner of layout
    owner_found = False
    owner_uri = addrbook.get_realaddr_by_ab_id(request.participant_id)
    owner_conn_id = request.connection_id
    for ab in conf_ab_list:
        if (owner_conn_id is not None) and owner_conn_id >= 0:
            if owner_conn_id == ab["id"]:
                owner_found = True
                break
        elif owner_uri != "" and tools.is_addr_equivalent(ab["uri"], owner_uri):
            owner_found = True
            break
    # if not owner_found: # owner of layout is not connected to conference
    #    raise SessionAbonentNotFoundError(request.participant_id, "participant_id")

    # check participants in layout
    # for lyt_ab in request.participant_positions:
    #    if lyt_ab.participant_id == None or lyt_ab.participant_id == "":
    #        continue
    #    lyt_participant_found = False
    #    for ab in conf_ab_list:
    #        if tools.is_addr_equivalent(ab["uri"], lyt_ab.participant_id):
    #            lyt_participant_found = True
    #            break
    #    if not lyt_participant_found: # participant declared in layout but not connected to conference
    #        raise SessionAbonentNotFoundError(lyt_ab.participant_id, "participant_id")

    # don't create individual layout for RTSP camera (don't send video to RTSP camera)
    if owner_found and owner_uri != "":
        if tools.is_rtsp_prefix(owner_uri):
            raise SendVideoToRTSPError()

    # generate unique number/name of layout
    new_num = generate_unique_layout_id(request.confid, confstr)
    new_layout_name = tools.get_layout_name_by_id(request.confid, new_num)

    cmd_list = []

    # register layout
    cid = tools.conference_name_by_id(request.confid)
    layoutcfgtools.register_online_layout(request.confid, new_layout_name, request.auto, cmd_list)

    # set layout windows count limit
    layoutcfgtools.set_layout_restrict(cid, new_layout_name, request.cells_count, cmd_list)

    if request.layout_type == "active_speaker":
        other_aspeaker_layout_id = layoutbackup.get_layout_id_by_type(request.confid, data.LAYOUT_TYPE_ASPEAKER,
                                                                      not request.template)
        if other_aspeaker_layout_id != 0:
            other_aspeaker_layout_name = tools.get_layout_name_by_id(request.confid, other_aspeaker_layout_id)
            layoutcfgtools.set_special_layout(cid, other_aspeaker_layout_name, False, cmd_list)
        layoutcfgtools.set_special_layout(cid, new_layout_name, True, cmd_list)

    # show layout for participant_id
    layout_type_ind = data.get_layout_type_ind(request.layout_type)
    if request.layout_type == "individual":
        cur_layout_id, cur_priority = layoutbackup.get_layout_id_by_owner2(request.confid, request.participant_id)
        new_priority = layoutbackup.get_layout_priority(layout_type_ind, request.template)
        if owner_found and new_priority > cur_priority:
            if (owner_conn_id is not None) and owner_conn_id >= 0:
                owner_addr = "#%d" % (owner_conn_id)
            else:
                owner_addr = owner_uri
            cmd_list.append("LAYOUT SHOW " + cid + " " + new_layout_name + " " + owner_addr)
    elif request.layout_type == "global" and request.template:
        # make all owners of common global layout owners of new 'global template' layout
        glb_lyt_id = layoutbackup.get_global_layout_id(request.confid, False)
        glb_lyt_name = tools.get_layout_name_by_id(request.confid, glb_lyt_id)
        change_layout_owners(conf_json, glb_lyt_name, new_layout_name, cmd_list)

    # OLD: cmd_list.append("LAYOUT FIXED EQUAL " + cid + " " + new_layout_name)

    # set aspeaker mode (if need)
    active_slot_in_new_layout = layoutbackup.is_active_slot_in_slots_list(request.participant_positions, request.auto)
    active_slot_in_other_layouts = layoutbackup.is_active_slot_exclude_layout(request.confid, new_num)
    if not active_slot_in_other_layouts:
        cmd_list.append(layoutcfgtools.aspeaker_cmd(request.confid, active_slot_in_new_layout))

    # get command "layout mode" for specified layout type
    custom_lyt_types = customlayouttypes.get_all_custom_layout_types()
    layout_mode_cmd = layoutcfgtools.get_layout_cmd_ex(request.schema_type, cid, new_layout_name, custom_lyt_types)

    # attach/pin abonents to created layout
    for ab_pos in request.participant_positions:
        lyt_participant_found = False
        participant_addr = ""
        if ab_pos.role_id == layoutcfgtools.DEFAULT_FILLED_ROLE_ID and ab_pos.connection_id >= 0:
            for ab in conf_ab_list:
                if ab_pos.connection_id == ab["id"]:
                    lyt_participant_found = True
                    participant_addr = "#%d" % (ab_pos.connection_id)
                    break
        elif ab_pos.role_id == layoutcfgtools.DEFAULT_FILLED_ROLE_ID and ab_pos.participant_id != None and \
            ab_pos.participant_id != 0:
            participant_addr = addrbook.get_realaddr_by_ab_id(ab_pos.participant_id)
            for ab in conf_ab_list:
                if participant_addr != "" and tools.is_addr_equivalent(ab["uri"], participant_addr):
                    lyt_participant_found = True
                    break
        if lyt_participant_found:
            cmd_list.append("LAYOUT ATTACH " + cid + " " + new_layout_name + " " + participant_addr)
        cmd_list.append(layoutcfgtools.role_cmd(ab_pos.role_id, cid, new_layout_name, ab_pos.position, participant_addr))
        render_mode_ind = data.get_render_participant_index(ab_pos.render_mode)
        cmd_list.append(layoutcfgtools.get_layout_render_mode_cmd(cid, new_layout_name, ab_pos.position, render_mode_ind))

    cmd_list.append(layout_mode_cmd)

    cmd_list.append("LAYOUT MAP APPLY " + cid + " " + new_layout_name)

    # add commands to attach "not fixed" peers to layout
    cmd_list += attach_not_fixed_conf_abonents(request.confid, new_layout_name, request.participant_positions,
                                               conf_ab_list)

    nettools.exec_cmd_list_s(cmd_list)

    # make copy of layout in database
    layoutbackup.backup_of_created_online_layout(request, new_num)

    # TMP: FAKE
    # NOTE: REAL RESULT WILL BUILDED IN ENGINE AT UNKNOWN MOMENT
    if request.layout_type == "active_speaker" or request.layout_type == "global":
        return GlobalOnlineLayoutResponse(new_layout_name, request.layout_type, request.cells_count,
                                          request.schema_type, request.participant_positions, request.auto,
                                          request.display_name, request.template)
    else:
        owner_disp_name = get_owner_disp_name(request.participant_id)
        return IndividualOnlineLayoutResponse(new_layout_name, "individual", request.cells_count, request.schema_type,
                                              request.participant_positions, request.participant_id, owner_disp_name,
                                              request.auto, owner_conn_id, request.display_name, request.template)


def create_online_layout_auto(request: CreateOnlineLayoutRequest):
    """Create online layout with 'auto' == True."""

    # get conference's online state
    connstr, confstr = onlinetools.get_online_state()
    conf_json = onlinetools.extract_conference(confstr, request.confid)
    if conf_json == {}:
        raise ActiveConferenceNotFoundError(request.confid, "conference_id")

    if "layout_list" not in conf_json:
        raise GetOnlineStateError()

    online_lyt_cnt = get_online_layouts_count2()
    max_layouts_cnt = sysinfotools.get_abonents_restriction()["max_layouts"]
    if online_lyt_cnt >= max_layouts_cnt:
        raise TooManyLayoutsError()

    conf_ab_list = onlinetools.extract_abonents_list(conf_json, 1, connstr)

    conn_list = onlinetools.extract_conn_list(connstr)

    ab_names_dict = addrbook.get_all_abonent_names_dict()

    # --- check max active abonents restriction ---
    validate_participants_added_to_layout2(request.confid, confstr, conf_ab_list, connstr)
    # ---------------------------------------------

    # check owner of layout
    owner_found = False
    owner_uri = addrbook.get_realaddr_by_ab_id(request.participant_id)
    owner_conn_id = request.connection_id
    for ab in conf_ab_list:
        if (owner_conn_id is not None) and owner_conn_id >= 0:
            if owner_conn_id == ab["id"]:
                owner_found = True
                break
        elif owner_uri != "" and tools.is_addr_equivalent(ab["uri"], owner_uri):
            owner_conn_id = addrbook.get_conn_id_by_ab_id(request.participant_id)
            owner_found = True
            break

    # don't create individual layout for RTSP camera (don't send video to RTSP camera)
    if owner_found and owner_uri != "":
        if tools.is_rtsp_prefix(owner_uri):
            raise SendVideoToRTSPError()

    # don't use active speker layout if crown layout exist
    if request.layout_type == "active_speaker":
        crown_layout = onlinetools.get_crown_layout(request.confid, conf_json)
        if crown_layout != {}:
            raise ConcurrentLayoutsError("active speaker", "crown")

    # generate unique number/name of layout
    new_num = generate_unique_layout_id(request.confid, confstr)
    new_layout_name = tools.get_layout_name_by_id(request.confid, new_num)

    cmd_list = []

    # register layout
    cid = tools.conference_name_by_id(request.confid)
    layoutcfgtools.register_online_layout(request.confid, new_layout_name, request.auto, cmd_list)

    # set layout windows count limit
    max_visible_ab = sysinfotools.get_abonents_restriction()["max_visible"]
    cmd_list.append("LAYOUT RESTRICT " + cid + " " + new_layout_name + " " + str(max_visible_ab))

    if request.layout_type == "active_speaker":
        other_aspeaker_layout_id = layoutbackup.get_layout_id_by_type(request.confid, data.LAYOUT_TYPE_ASPEAKER,
                                                                      not request.template)
        if other_aspeaker_layout_id != 0:
            other_aspeaker_layout_name = tools.get_layout_name_by_id(request.confid, other_aspeaker_layout_id)
            layoutcfgtools.set_special_layout(cid, other_aspeaker_layout_name, False, cmd_list)
        layoutcfgtools.set_special_layout(cid, new_layout_name, True, cmd_list)

    # show layout for participant_id
    layout_type_ind = data.get_layout_type_ind(request.layout_type)
    if request.layout_type == "individual":
        cur_layout_id, cur_priority = layoutbackup.get_layout_id_by_owner2(request.confid, request.participant_id)
        new_priority = layoutbackup.get_layout_priority(layout_type_ind, request.template)
        if owner_found and new_priority > cur_priority:
            if (owner_conn_id is not None) and owner_conn_id >= 0:
                owner_addr = "#%d" % (owner_conn_id)
            else:
                owner_addr = owner_uri
            cmd_list.append("LAYOUT SHOW " + cid + " " + new_layout_name + " " + owner_addr)
    elif request.layout_type == "global" and request.template:
        # make all owners of common global layout owners of new 'global template' layout
        glb_lyt_id = layoutbackup.get_global_layout_id(request.confid, False)
        glb_lyt_name = tools.get_layout_name_by_id(request.confid, glb_lyt_id)
        change_layout_owners(conf_json, glb_lyt_name, new_layout_name, cmd_list)

    # get command "layout mode" for specified layout type
    custom_lyt_types = customlayouttypes.get_all_custom_layout_types()
    layout_mode_cmd = layoutcfgtools.get_layout_cmd_ex(layoutcfgtools.DEFAULT_EQUAL_TYPE, cid, new_layout_name,
                                                       custom_lyt_types)

    # attach abonents to created layout
    participant_pos_list = []
    for ab in conf_ab_list:
        conn_id = ab["id"]
        conn_id_str = "#%d" % (conn_id)
        if owner_found and (owner_conn_id is not None) and owner_conn_id >= 0 and owner_conn_id == conn_id:
            continue # don't attach owner to it's layout
        cmd_list.append("LAYOUT ATTACH " + cid + " " + new_layout_name + " " + conn_id_str)
        participant_id = addrbook.get_ab_id_by_conn_id(conn_id)
        #OLD:participant_id = addrbook.get_ab_id_by_realaddr(ab["uri"])

        ab_name = ab["display_name"]
        b_outgoing = onlinetools.is_conn_id_outgoing(conn_list, conn_id)
        if b_outgoing and (participant_id in ab_names_dict):
            ab_name = ab_names_dict[participant_id]["name"]

        participant_pos_list.append(OnlineLayoutAbonentEntity(0, participant_id, ab_name, True,
                                                              layoutcfgtools.AUTO_ROLE_ID, conn_id, "AUTO"))

    cmd_list.append(layout_mode_cmd)

    nettools.exec_cmd_list_s(cmd_list)

    # make copy of layout in database
    layoutbackup.backup_of_created_online_layout(request, new_num)

    # TMP: FAKE
    # NOTE: REAL RESULT WILL BUILDED IN ENGINE AT UNKNOWN MOMENT
    if request.layout_type == "active_speaker" or request.layout_type == "global":
        return GlobalOnlineLayoutResponse(new_layout_name, request.layout_type, len(conf_ab_list),
                                          request.schema_type, participant_pos_list, request.auto,
                                          request.display_name, request.template)
    else:
        owner_disp_name = get_owner_disp_name(request.participant_id)
        return IndividualOnlineLayoutResponse(new_layout_name, "individual", len(conf_ab_list), request.schema_type,
                                              participant_pos_list, request.participant_id, owner_disp_name,
                                              request.auto, owner_conn_id, request.display_name, request.template)


def create_online_layout_ex(request: CreateOnlineLayoutRequest) -> IndividualOnlineLayoutResponse:
    layout_type_ind = data.get_layout_type_ind(request.layout_type)

    if request.layout_type == "global" and (not request.template): # cannot create gloabal layout
        logging.error("Cannot create global online-layout manually (not template)")
        raise LayoutOfAbonentAlreadyExistError(-1)

    if request.layout_type == "active_speaker" or request.layout_type == "global":
        layout_id = layoutbackup.get_layout_id_by_type(request.confid, layout_type_ind, request.template)
        if layout_id != 0:
            logging.error("Cannot create global/active_peaker online-layout. Layout this type already exist.")
            logging.error("template=%d" % (tools.bool_to_int(request.template)))
            raise LayoutOfAbonentAlreadyExistError(-1)

    if request.layout_type == "individual" and (request.participant_id > 0 or request.connection_id >= 0):
        layout_id = layoutbackup.get_individual_layout_id(request.confid, request.participant_id, request.template)
        if layout_id != 0:
            logging.error("Cannot create individual online-layout. Layout this type already exist.")
            logging.error("participant_id=%d , template=%d" % (request.participant_id, tools.bool_to_int(request.template)))
            raise LayoutOfAbonentAlreadyExistError(-1)

    if request.auto:
        response = create_online_layout_auto(request)
    else:
        response = create_online_layout(request)
    return response


def is_ab_in_one_layout(_confid, _lid, _ab_uri):
    """This function returns 1 if abonent _ab_uri present in layout _lid only. Else returns 0"""
    _confid = int(_confid)

    # get online state from buffer (in database)
    connstr, confstr = onlinetools.get_online_state()

    conf_json = onlinetools.extract_conference(confstr, _confid)
    if "layout_list" not in conf_json:
        return (0)  # error

    in_one_lid = 0
    in_other_lid = 0
    for lyt in conf_json["layout_list"]:
        cur_lid = lyt["name"]
        for ab in lyt["rx"]["connections"]:
            cur_ab_uri = ab["uri"]
            if cur_lid == _lid and cur_ab_uri == _ab_uri:
                in_one_lid = 1
            if cur_lid != _lid and cur_ab_uri == _ab_uri:
                in_other_lid = 1

    if in_one_lid == 1 and in_other_lid == 0:
        return (1)
    else:
        return (0)


def is_conn_id_in_one_layout(_confid, _lid, _conn_id):
    """This function returns 1 if abonent _conn_id present in layout _lid only. Else returns 0"""
    _confid = int(_confid)

    # get online state from buffer (in database)
    connstr, confstr = onlinetools.get_online_state()

    conf_json = onlinetools.extract_conference(confstr, _confid)
    if "layout_list" not in conf_json:
        return (0)  # error

    in_one_lid = 0
    in_other_lid = 0
    for lyt in conf_json["layout_list"]:
        cur_lid = lyt["name"]
        for ab in lyt["rx"]["connections"]:
            cur_conn_id = ab["id"]
            if cur_lid == _lid and cur_conn_id == _conn_id:
                in_one_lid = 1
            if cur_lid != _lid and cur_conn_id == _conn_id:
                in_other_lid = 1

    if in_one_lid == 1 and in_other_lid == 0:
        return (1)
    else:
        return (0)


def reset_to_global_layout_OLD(_confid, _layout_name):
    _confid = int(_confid)

    cid = tools.conference_name_by_id(_confid)
    glb_lyt_id = layoutbackup.get_global_layout_id(_confid, False)
    lid = tools.get_layout_name_by_id(_confid, glb_lyt_id)

    # get conference's online state
    connstr, confstr = onlinetools.get_online_state()
    conf_json = onlinetools.extract_conference(confstr, _confid)
    if conf_json == {}:
        raise ActiveConferenceNotFoundError(_confid, "conference_id")

    if "layout_list" not in conf_json:
        raise GetOnlineStateError()

    layout_json = onlinetools.extract_layout_by_name(conf_json, _layout_name)
    if layout_json == {}:
        raise OnlineLayoutNotFoundError(_layout_name, "layout_name")

    # get deleted layout params
    layout = get_online_layout_by_name(_confid, _layout_name, connstr, confstr)

    # you cannot delete global layout
    if layout.layout_type == "global" or _layout_name == lid:
        raise DeleteGlobalLayoutError(_layout_name)

    #if layout.layout_type == "active_speaker":
    #    raise DeleteAspeakerLayoutError(_layout_name)

    conf_ab_list = onlinetools.extract_abonents_list(conf_json, 0, connstr)

    # check owner of layout
    owner_found = False
    owner_uri = ""
    owner_conn_id = -1
    if layout.layout_type == "individual":
        owner_uri = addrbook.get_realaddr_by_ab_id(layout.participant_id)
        owner_conn_id = layout.connection_id
        for ab in conf_ab_list:
            if (owner_conn_id is not None) and owner_conn_id >= 0:
                if owner_conn_id == ab["id"]:
                    owner_found = True
                    break
            elif owner_uri != "" and tools.is_addr_equivalent(ab["uri"], owner_uri):
                owner_found = True
                break

    cmd_list = []

    # give global layout to owner of deleted layout
    if owner_found:
        if (owner_conn_id is not None) and owner_conn_id >= 0:
            owner_addr = "#%d" % (owner_conn_id)
        else:
            owner_addr = owner_uri
        cmd_list.append("LAYOUT SHOW %s %s %s" % (cid, lid, owner_addr))

    cmd_list.append("LAYOUT UNREGISTER " + cid + " " + _layout_name)

    # reset aspeaker mode (if need)
    old_layout_id = tools.get_layout_id_by_name(_layout_name)
    active_slot_in_old_layout = layoutbackup.is_active_slot_in_slots_list(layout.participant_positions, layout.auto)
    active_slot_in_other_layouts = layoutbackup.is_active_slot_exclude_layout(_confid, old_layout_id)
    if (not active_slot_in_other_layouts) and active_slot_in_old_layout:
        cmd_list.append(layoutcfgtools.aspeaker_cmd(_confid, False))

    # delete copy of layout in database
    layout_id = tools.get_layout_id_by_name(_layout_name)
    layoutbackup.delete_layout_backup(_confid, layout_id, True)

    nettools.exec_cmd_list_s(cmd_list)

    nettools.update_online_state()

    # unregister old individual layout (if it is unused)
    # OLD: unregister_unused_tmp_layouts(_confid)

    return DeleteOnlineLayoutResponse(_confid, _layout_name)


def reset_to_global_layout(_confid, _layout_name):
    _confid = int(_confid)

    cid = tools.conference_name_by_id(_confid)

    # get conference's online state
    connstr, confstr = onlinetools.get_online_state()
    conf_json = onlinetools.extract_conference(confstr, _confid)
    if conf_json == {}:
        raise ActiveConferenceNotFoundError(_confid, "conference_id")

    if "layout_list" not in conf_json:
        raise GetOnlineStateError()

    reset_layout_json = onlinetools.extract_layout_by_name(conf_json, _layout_name)
    if reset_layout_json == {}:
        raise OnlineLayoutNotFoundError(_layout_name, "layout_name")

    # get deleted layout params
    reset_layout_id = tools.get_layout_id_by_name(_layout_name)
    reset_layout = get_online_layout_by_name(_confid, _layout_name, connstr, confstr)

    # you cannot delete global layout
    glb_lyt_id = layoutbackup.get_global_layout_id(_confid, False)
    glb_lid = tools.get_layout_name_by_id(_confid, glb_lyt_id)
    if (reset_layout.layout_type == "global" and (not reset_layout.template)) or _layout_name == glb_lid:
        raise DeleteGlobalLayoutError(_layout_name)

    # get new owner of reseted layout _layout_name
    owner_id = -1
    if reset_layout.layout_type == "individual":
        owner_id = reset_layout.participant_id

    # get changed layout (new owner of participants of reseted layout)
    change_layout_id = layoutbackup.get_layout_id_by_owner_ex(_confid, owner_id, reset_layout_id)
    change_layout_name = tools.get_layout_name_by_id(_confid, change_layout_id)
    if change_layout_id == 0 or change_layout_id == reset_layout_id:
        raise OnlineLayoutNotFoundError(change_layout_name, "new_owner_layout")

    cmd_list = []

    # make layout 'change_layout_name' is owner of old owners of layout '_layout_name'
    change_layout_owners(conf_json, _layout_name, change_layout_name, cmd_list)

    cmd_list.append("LAYOUT UNREGISTER " + cid + " " + _layout_name)

    # reset aspeaker mode (if need)
    active_slot_in_old_layout = layoutbackup.is_active_slot_in_slots_list(reset_layout.participant_positions, reset_layout.auto)
    active_slot_in_other_layouts = layoutbackup.is_active_slot_exclude_layout(_confid, reset_layout_id)
    if (not active_slot_in_other_layouts) and active_slot_in_old_layout:
        cmd_list.append(layoutcfgtools.aspeaker_cmd(_confid, False))

    # delete copy of layout in database
    layoutbackup.delete_layout_backup(_confid, reset_layout_id, True)

    nettools.exec_cmd_list_s(cmd_list)

    nettools.update_online_state()

    return DeleteOnlineLayoutResponse(_confid, _layout_name)


def edit_online_layout(request: ChangeOnlineLayoutRequest):
    cid = tools.conference_name_by_id(request.confid)

    # get conference's online state
    connstr, confstr = onlinetools.get_online_state()
    conf_json = onlinetools.extract_conference(confstr, request.confid)
    if conf_json == {}:
        raise ActiveConferenceNotFoundError(request.confid, "conference_id")

    if "layout_list" not in conf_json:
        raise GetOnlineStateError()

    conf_ab_list = onlinetools.extract_abonents_list(conf_json, 1, connstr)

    # find presentation
    presentation_uri = onlinetools.get_presentation_uri(conf_ab_list, connstr)

    conn_list = onlinetools.extract_conn_list(connstr)

    ab_names_dict = addrbook.get_all_abonent_names_dict()

    # get current state of edited layout
    old_layout = get_online_layout_by_name(request.confid, request.layout_name, connstr, confstr)

    cmd_list = []

    # set aspeaker mode (if need)
    layout_id = tools.get_layout_id_by_name(request.layout_name)
    active_slot_in_old_layout = layoutbackup.is_active_slot_in_slots_list(old_layout.participant_positions,
                                                                          old_layout.auto)
    active_slot_in_new_layout = layoutbackup.is_active_slot_in_slots_list(request.participant_positions, request.auto)
    active_slot_in_other_layouts = layoutbackup.is_active_slot_exclude_layout(request.confid, layout_id)
    if not active_slot_in_other_layouts:
        if (not active_slot_in_old_layout) and active_slot_in_new_layout:
            cmd_list.append(layoutcfgtools.aspeaker_cmd(request.confid, True))
        elif active_slot_in_old_layout and (not active_slot_in_new_layout):
            cmd_list.append(layoutcfgtools.aspeaker_cmd(request.confid, False))

    # unpin/detach current abonents from layout
    old_layout_json = onlinetools.extract_layout_by_name(conf_json, old_layout.layout_name)
    psevdo_del_list = []
    other_deleted_count = 0
    if old_layout.auto:
        for ab in old_layout_json["rx"]["connections"]:
            conn_id = ab["id"]
            conn_id_str = "#%d" % (conn_id)
            cmd_list.append("LAYOUT DETACH " + cid + " " + old_layout.layout_name + " " + conn_id_str)
            in_this_lyt_only = is_conn_id_in_one_layout(request.confid, old_layout.layout_name, conn_id)
            if in_this_lyt_only:
                psevdo_del_list.append(ab["uri"])

        new_participant_list = []
        pos = 1
        for ab in conf_ab_list:
            conn_id = ab["id"]
            participant_id = addrbook.get_ab_id_by_conn_id(conn_id)
            #OLD:participant_id = addrbook.get_ab_id_by_realaddr(ab["uri"])
            new_participant_list.append(OnlineLayoutAbonentEntity(pos, participant_id, "", True,
                                                                  layoutcfgtools.DEFAULT_FILLED_ROLE_ID,
                                                                  conn_id, "AUTO"))
            pos += 1
    else:
        for participant in old_layout.participant_positions:
            cmd_list.append(layoutcfgtools.role_cmd(layoutcfgtools.DEFAULT_EMPTY_ROLE_ID, cid, old_layout.layout_name,
                                                    participant.position, ""))

            cur_role = participant.role_id
            if cur_role == layoutcfgtools.DEFAULT_EMPTY_ROLE_ID:
                continue  # skip slots without participants

            if cur_role == layoutcfgtools.DEFAULT_FILLED_ROLE_ID and participant.participant_id != None and participant.participant_id != 0:
                participant_uri = addrbook.get_realaddr_by_ab_id(participant.participant_id)
                conn_id = participant.connection_id
                conn_id_str = "#%d" % (conn_id)
                if (conn_id is not None) and conn_id >= 0:
                    address = conn_id_str
                    in_this_lyt_only = is_conn_id_in_one_layout(request.confid, old_layout.layout_name, conn_id)
                else:
                    address = participant_uri
                    in_this_lyt_only = is_ab_in_one_layout(request.confid, old_layout.layout_name, participant_uri)
                cmd_list.append("LAYOUT DETACH " + cid + " " + old_layout.layout_name + " " + address)

                # if abonent is present only in one layout "lid", then "mute video on"
                if in_this_lyt_only:
                    cmd_list.append("SET MUTE ON VIDEO " + address)
                    # TMP: FIX BUG 4685
                    # cmd_list.append("SET MUTE ON AUDIO " + participant_uri)
                    psevdo_del_list.append(participant_uri)  # "delete" participant from active (formal make invisible)
            elif cur_role == layoutcfgtools.PRESENTATION_ROLE_ID:
                if presentation_uri != "":
                    other_deleted_count += 1
            else:
                conn_id = onlinetools.get_conn_id_from_layout_position(old_layout_json, participant.position)
                if (conn_id is not None) and conn_id >= 0:
                    conn_id_str = "#%d" % (conn_id)
                    in_this_lyt_only = is_conn_id_in_one_layout(request.confid, old_layout.layout_name, conn_id)
                    cmd_list.append("LAYOUT DETACH " + cid + " " + old_layout.layout_name + " " + conn_id_str)
                    if in_this_lyt_only:
                        cmd_list.append("SET MUTE ON VIDEO " + conn_id_str)
                    other_deleted_count += 1  # make some (unknown) participant invisible

        new_participant_list = request.participant_positions

    # --- check max active abonents restriction ---
    validate_participants_added_to_layout(request.confid, request.layout_name, confstr, conf_ab_list,
                                          new_participant_list, psevdo_del_list, other_deleted_count, connstr)
    # ---------------------------------------------

    if not old_layout.auto:
        cmd_list.append("LAYOUT MAP APPLY " + cid + " " + old_layout.layout_name)

    # restrict:
    max_visible_ab = sysinfotools.get_abonents_restriction()["max_visible"]
    new_cells_count = request.cells_count
    if old_layout.auto:
        if request.auto:
            new_cells_count = len(conf_ab_list)
            cmd_list.append("LAYOUT RESTRICT " + cid + " " + request.layout_name + " " + str(max_visible_ab))
        else:
            layoutcfgtools.set_layout_restrict(cid, request.layout_name, new_cells_count, cmd_list)
    else:
        if request.auto:
            new_cells_count = len(conf_ab_list)
            cmd_list.append("LAYOUT MAP DELETE " + cid + " " + request.layout_name)
            cmd_list.append("LAYOUT RESTRICT " + cid + " " + request.layout_name + " " + str(max_visible_ab))
        else:
            # if count of windows changed
            if old_layout.cells_count != new_cells_count:
                layoutcfgtools.set_layout_restrict(cid, request.layout_name, new_cells_count, cmd_list)

    # enable automatic control for layouts with map (for active speaker slots)
    # disable for auto layouts (no active speaker slots)
    if old_layout.auto != request.auto:
        cmd_list.append("LAYOUT KEEP %s %s %s" % (tools.num_to_onoff(request.auto), cid, request.layout_name))

    custom_lyt_types = customlayouttypes.get_all_custom_layout_types()

    # attach abonents to new edited layout
    if request.auto:
        participant_pos_list = []
        for ab in conf_ab_list:
            conn_id = ab["id"]
            conn_id_str = "#%d" % (conn_id)
            participant_id = addrbook.get_ab_id_by_conn_id(conn_id)
            cmd_list.append("LAYOUT ATTACH " + cid + " " + request.layout_name + " " + conn_id_str)
            cmd_list.append("SET MUTE OFF VIDEO " + conn_id_str)
            #OLD:participant_id = addrbook.get_ab_id_by_realaddr(ab["uri"])

            ab_name = ab["display_name"]
            b_outgoing = onlinetools.is_conn_id_outgoing(conn_list, conn_id)
            if b_outgoing and (participant_id in ab_names_dict):
                ab_name = ab_names_dict[participant_id]["name"]

            participant_pos_list.append(
                OnlineLayoutAbonentEntity(0, participant_id, ab_name, True, layoutcfgtools.AUTO_ROLE_ID,
                                          conn_id, "AUTO"))

        # get command "layout mode" for specified layout type
        layout_mode_cmd = layoutcfgtools.get_layout_cmd_ex(layoutcfgtools.DEFAULT_EQUAL_TYPE, cid, request.layout_name,
                                                           custom_lyt_types)
        cmd_list.append(layout_mode_cmd)
    else:
        # get dictionary with audio settings of all abonents of conference
        #OLD:lyt_ab_dict = get_layout_ab_dict(conf_ab_list, request.confid)  # get from database audio settings of abonents

        # attach/pin abonents to new edited layout
        for ab_pos in request.participant_positions:
            lyt_participant_found = False
            conn_id = ab_pos.connection_id
            address = ""
            if (conn_id is not None) and conn_id >= 0 and ab_pos.role_id == layoutcfgtools.DEFAULT_FILLED_ROLE_ID:
                for ab in conf_ab_list:
                    if ab["id"] == conn_id:
                        lyt_participant_found = True
                        address = "#%d" % (conn_id)
                        break
            elif ab_pos.participant_id != None and ab_pos.participant_id != 0 and \
                ab_pos.role_id == layoutcfgtools.DEFAULT_FILLED_ROLE_ID:
                # check: participant is online?
                participant_uri = addrbook.get_realaddr_by_ab_id(ab_pos.participant_id)
                for ab in conf_ab_list:
                    if participant_uri != "" and tools.is_addr_equivalent(ab["uri"], participant_uri):
                        lyt_participant_found = True
                        address = participant_uri
                        break

            if lyt_participant_found:
                cmd_list.append("LAYOUT ATTACH " + cid + " " + request.layout_name + " " + address)

            cmd_list.append(
                layoutcfgtools.role_cmd(ab_pos.role_id, cid, request.layout_name, ab_pos.position, address))

            if lyt_participant_found:
                cmd_list.append("SET MUTE OFF VIDEO " + address)
                # TMP: FIX BUG 4685
                # if participant_uri in lyt_ab_dict:
                #    useaudio = lyt_ab_dict[participant_uri]["useaudio"]
                # else:
                #    useaudio = 1
                # if useaudio:
                #    cmd_list.append("SET MUTE OFF AUDIO " + participant_uri)

            render_mode_ind = data.get_render_participant_index(ab_pos.render_mode)
            cmd_list.append(layoutcfgtools.get_layout_render_mode_cmd(cid, request.layout_name, ab_pos.position,
                                                                      render_mode_ind))

        # get command "layout mode" for specified layout type
        layout_mode_cmd = layoutcfgtools.get_layout_cmd_ex(request.schema_type, cid, request.layout_name,
                                                           custom_lyt_types)
        cmd_list.append(layout_mode_cmd)

        cmd_list.append("LAYOUT MAP APPLY " + cid + " " + request.layout_name)

        # add commands to attach "not fixed" peers to layout
        cmd_list += attach_not_fixed_conf_abonents(request.confid, request.layout_name, request.participant_positions,
                                                   conf_ab_list)

        participant_pos_list = request.participant_positions

    nettools.exec_cmd_list_s(cmd_list)

    # update copy of layout in database
    layoutbackup.backup_of_edited_online_layout(request)

    # TMP: FAKE
    # NOTE: REAL RESULT WILL BUILDED IN ENGINE AT UNKNOWN MOMENT
    if old_layout.layout_type == "global":
        return GlobalOnlineLayoutResponse(request.layout_name, old_layout.layout_type, new_cells_count,
                                          request.schema_type, participant_pos_list, request.auto,
                                          old_layout.display_name, old_layout.template)
    elif old_layout.layout_type == "active_speaker":
        return GlobalOnlineLayoutResponse(request.layout_name, old_layout.layout_type, new_cells_count,
                                          request.schema_type, participant_pos_list, request.auto,
                                          old_layout.display_name, old_layout.template)
    else:
        owner_disp_name = get_owner_disp_name(old_layout.participant_id)
        return IndividualOnlineLayoutResponse(request.layout_name, old_layout.layout_type, new_cells_count,
                                              request.schema_type, participant_pos_list,
                                              old_layout.participant_id, owner_disp_name, request.auto,
                                              old_layout.connection_id, old_layout.display_name, old_layout.template)

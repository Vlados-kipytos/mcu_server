#!/usr/bin/python3

import sys


def parse_command_line():
    """This function parse command line 'program.exe --key1 value1 --key2 value2 ...' and returns dictionary of parsed values { '--key1': 'value1', '--key2': 'value2', ... }."""

    result = {"success": True, "error": "", "params": {}}

    i = 1
    while i < len(sys.argv):
        key_index = i
        key_str = sys.argv[key_index]
        if len(key_str) < 3 or key_str[:2] != "--":
            result["success"] = False
            result["error"] = "key '%s' must begin from '--'" % (key_str)
            return result

        value_index = i + 1
        if value_index < len(sys.argv):
            value_str = sys.argv[value_index]
        else:
            result["success"] = False
            result["error"] = "no value for key '%s'" % (key_str)
            return result

        result["params"][key_str] = value_str

        i += 2

    return result

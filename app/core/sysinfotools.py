#!/usr/bin/python

import json
import logging
import os
import sys

import app.core.constants as constants
import app.core.dbtools as dbtools
import app.core.nettools as nettools
import app.core.tools as tools
from .errors import GetDatabaseDataError
from .errors import GetOnlineStateError
from .requests import GetMCUReferencesRequest
from .responses import MCUReferencesResponse

if sys.platform == "win32":
    import winreg

# NOTE: don't use empty string to build correct csr-file
DEFAULT_VERSION = "unknown version"


def get_sysinfo_json():
    """this function get sysinfo.json file form server and return it as dictionnary."""
    sysinfo_json = {}

    sysinfo_str = nettools.get_url_data_s("sysinfo.json", True)
    if sysinfo_str != "":
        true = True  # define constants "true"/"false"
        false = False
        try:
            sysinfo_json = eval(sysinfo_str)  # convert string to python expression (json format)
        except:
            return ({})

    return (sysinfo_json)


def get_abonents_restriction():
    """This function returns max count of abonents restricted by license."""

    result = {"max_calls": constants.MAX_ABONENTS, "max_visible": constants.MAX_ACTIVE_ABONENTS,
              "max_layouts": constants.MAX_LAYOUTS}

    sysinfo_json = get_sysinfo_json()
    if sysinfo_json != {}:
        logging.error("get_abonents_restriction(): max_calls=%d , max_visible=%d" % (
        sysinfo_json["max_calls"], sysinfo_json["max_visible"]))
        result["max_calls"] = sysinfo_json["max_calls"]
        result["max_visible"] = sysinfo_json["max_visible"]
        #TODO: get max layouts count from license

    return result


def get_program_version_win():
    """This function returns version of program (in OS Windows)"""

    if sys.platform != "win32":
        return (DEFAULT_VERSION)

    try:
        aReg = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)
        aKey = winreg.OpenKey(aReg,
                              "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{ABB42E9E-0A10-402A-AB70-43AA48736DD3}_is1")

        version_tuple = winreg.QueryValueEx(aKey, "DisplayVersion")
        version_str = version_tuple[0]

        return (version_str)
    except:
        logging.error("get_program_version_win(): exception text: " + str(sys.exc_info()))
        return (DEFAULT_VERSION)


def get_deb_description():
    """This function get information about deb-pack, parse it and return field 'Descriprion'."""

    result = {"version": DEFAULT_VERSION, "maintainer": "", "description": "", "errstr": ""}

    if sys.platform == "win32":
        result["version"] = get_program_version_win()
        result["maintainer"] = "www.trueconf.ru"
        result["description"] = "TrueConf MCU"
        return (result)

    out, err = tools.exec_cmd(['dpkg', '--status', 'trueconf-mcu'], False)

    # logging.error("dpkg stdout:")
    # logging.error(out)
    # logging.error("dpkg stderr:")
    # logging.error(err)

    if len(err) != 0:
        result["errstr"] = err.decode("UTF-8")
        return (result)  # show error

    deb_info_str = out.decode("UTF-8")
    deb_info_lines = deb_info_str.splitlines()
    if len(deb_info_lines) == 0:
        return (result)

    cur_field_name = ""
    cur_field_value = ""
    for line in deb_info_lines:
        if len(line) == 0:
            continue

        if line[0] == " " or line[0] == "\t":
            continue_field = True
        else:
            continue_field = False

        if continue_field:
            cur_field_value += line
        else:
            separator_index = line.find(":")
            if separator_index != -1:
                cur_field_name = line[0:separator_index]
                cur_field_value = line[separator_index + 1:]

        if cur_field_name == "Description":
            result["description"] = cur_field_value
        if cur_field_name == "Version":
            result["version"] = cur_field_value
        if cur_field_name == "Maintainer":
            result["maintainer"] = cur_field_value

    return result


def get_all_versions():
    """This function returns all versions of all components of MCU."""

    versions_json = {"server": DEFAULT_VERSION, "installer": DEFAULT_VERSION, "ui": DEFAULT_VERSION,
                     "api": DEFAULT_VERSION}

    versions_fname = tools.get_versions_fname()

    if not os.path.exists(versions_fname):
        logging.error("get_all_versions(): file %s is not found" % (versions_fname))
    else:
        try:
            f = open(versions_fname, "rb")
            versions_data = f.read()
            f.close()
            versions_str = versions_data.decode("UTF-8")
            versions_json = json.loads(versions_str)
        except:
            logging.error("get_all_versions(): cannot read version from file %s" % (versions_fname))
            logging.error("get_all_versions(): exception %s" % (sys.exc_info()))
            versions_json = {"server": DEFAULT_VERSION, "installer": DEFAULT_VERSION, "ui": DEFAULT_VERSION,
                             "api": DEFAULT_VERSION}

    # prepare [] (if exist)
    if type(versions_json) == list:
        versions_dict = versions_json[0]
    else:
        versions_dict = versions_json

    # get veresions of vtsrv
    vtsrv_versions_json = get_sysinfo_json()

    if "veng_version" in vtsrv_versions_json:
        versions_dict["veng"] = vtsrv_versions_json["veng_version"]
    else:
        versions_dict["veng"] = DEFAULT_VERSION

    if "cv_version" in vtsrv_versions_json:
        versions_dict["crystalvu"] = vtsrv_versions_json["cv_version"]
    else:
        versions_dict["crystalvu"] = DEFAULT_VERSION

    if "version" in vtsrv_versions_json:
        versions_dict["vile"] = vtsrv_versions_json["version"]
    else:
        versions_dict["vile"] = DEFAULT_VERSION

    return versions_dict


def get_diagnostics_info() -> dict:
    """This function get from engine diagnostic information."""
    sysinfo_json = get_sysinfo_json()
    if "diagnostics" in sysinfo_json:
        return sysinfo_json["diagnostics"]
    else:
        return {}


def is_db_connected():
    """This function checks connection to database."""
    rows = dbtools.select("SELECT count(*) FROM legacy.commonconfig")
    return len(rows) != 0


def check_connection_to_backend_components():
    """This function check connection to vengine and to database.
    If no connection, then raise appropriate exception"""

    # check connection to video engine (vtsrv)
    diagnostics = get_diagnostics_info()
    if diagnostics == {}:
        raise GetOnlineStateError()

    # check connection to database
    if not is_db_connected():
        raise GetDatabaseDataError()


def get_mcu_references(request: GetMCUReferencesRequest) -> MCUReferencesResponse:
    versions_dict = get_all_versions()
    deb_version = versions_dict["installer"]

    suffix = "?lang=%s&version=%s" % (request.language, deb_version)

    result = {
        "mcu_key": "https://trueconf.com/mcu/key/%s" % (suffix),
        "mcu_index": "https://trueconf.com/mcu/index/%s" % (suffix),
        "mcu_free_license_restrictions": "https://trueconf.com/mcu/free-license-restrictions/%s" % (suffix),
        "mcu_buy": "https://trueconf.com/mcu/buy%s" % (suffix),
        "mcu_license_renewal": "https://trueconf.com/mcu/license-renewal/%s" % (suffix),
        "mcu_trial_full_version": "https://trueconf.com/mcu/trial/full-version/%s" % (suffix),
        "mcu_contacts": "https://trueconf.com/mcu/contacts/%s" % (suffix)
    }

    return MCUReferencesResponse(result)

from .apptypes import ContactId
import app.core.features as features
import app.core.tools as tools


class AudioCodecsEntity:

    def set_all_fields(self, init_value: bool):
        self._aac = init_value
        self._g711 = init_value
        self._g726 = init_value
        self._g728 = init_value
        self._g722 = init_value
        self._g722_1_24 = init_value
        self._g722_1_32 = init_value
        self._g722_1c_24 = init_value
        self._g722_1c_32 = init_value
        self._g722_1c_48 = init_value
        self._g729 = init_value
        self._g723_1 = init_value
        self._g719 = init_value

    def __init__(self, init_value: bool):
        self.set_all_fields(init_value)

    def init_codecs(self, aac: bool, g711: bool, g726: bool, g728: bool, g722: bool,
                    g722_1_24: bool, g722_1_32: bool, g722_1c_24: bool, g722_1c_32: bool, g722_1c_48: bool,
                    g729: bool, g723_1: bool, g719: bool):
        self._aac = aac
        self._g711 = g711
        self._g726 = g726
        self._g728 = g728
        self._g722 = g722
        self._g722_1_24 = g722_1_24
        self._g722_1_32 = g722_1_32
        self._g722_1c_24 = g722_1c_24
        self._g722_1c_32 = g722_1c_32
        self._g722_1c_48 = g722_1c_48
        self._g729 = g729
        self._g723_1 = g723_1
        self._g719 = g719

    def init_by_list(self, codecs_list: list):
        features_list = features.get_features_list()
        for codec_name in codecs_list:
            features.validate_conf_setting(features_list, codec_name)

        self._aac = "aac" in codecs_list
        self._g711 = "g711" in codecs_list
        self._g726 = "g726" in codecs_list
        self._g728 = "g728" in codecs_list
        self._g722 = "g722" in codecs_list
        self._g722_1_24 = "g722_1_24" in codecs_list
        self._g722_1_32 = "g722_1_32" in codecs_list
        self._g722_1c_24 = "g722_1c_24" in codecs_list
        self._g722_1c_32 = "g722_1c_32" in codecs_list
        self._g722_1c_48 = "g722_1c_48" in codecs_list
        self._g729 = "g729" in codecs_list
        self._g723_1 = "g723_1" in codecs_list
        self._g719 = "g719" in codecs_list

    def get_codecs_list(self) -> list:
        codecs_list = []
        if self._aac:
            codecs_list.append("aac")
        if self._g711:
            codecs_list.append("g711")
        if self._g726:
            codecs_list.append("g726")
        if self._g728:
            codecs_list.append("g728")
        if self._g722:
            codecs_list.append("g722")
        if self._g722_1_24:
            codecs_list.append("g722_1_24")
        if self._g722_1_32:
            codecs_list.append("g722_1_32")
        if self._g722_1c_24:
            codecs_list.append("g722_1c_24")
        if self._g722_1c_32:
            codecs_list.append("g722_1c_32")
        if self._g722_1c_48:
            codecs_list.append("g722_1c_48")
        if self._g729:
            codecs_list.append("g729")
        if self._g723_1:
            codecs_list.append("g723_1")
        if self._g719:
            codecs_list.append("g719")
        return codecs_list

    def init_by_supported_fields(self, _features_list):
        self.set_all_fields(True)
        full_list = self.get_codecs_list()
        supported_list = []
        for codec in full_list:
            if features.is_conf_setting_supported(_features_list, codec):
                supported_list.append(codec)
        self.init_by_list(supported_list)

    def get_engine_codecs(self) -> list:
        """Tis function returns list of names of engine codecs and appropriate settings in this container."""
        codecs_list = [{"engine_name": "AAC", "state": self._aac},
                       {"engine_name": "G711", "state": self._g711},
                       {"engine_name": "G726", "state": self._g726},
                       {"engine_name": "G728", "state": self._g728},
                       {"engine_name": "G722", "state": self._g722},
                       {"engine_name": "G7221 24", "state": self._g722_1_24},
                       {"engine_name": "G7221 32", "state": self._g722_1_32},
                       {"engine_name": "G7221C 24", "state": self._g722_1c_24},
                       {"engine_name": "G7221C 32", "state": self._g722_1c_32},
                       {"engine_name": "G7221C 48", "state": self._g722_1c_48},
                       {"engine_name": "G729", "state": self._g729},
                       {"engine_name": "G723", "state": self._g723_1},
                       {"engine_name": "G719", "state": self._g719}]
        return codecs_list

    def get_conference_cmd_list(self, cid: str) -> list:
        """Build list of commands for engine to configure conference with engine name 'cid'."""
        engine_codecs_list = self.get_engine_codecs()
        cmd_list = []

        # turn on all audiocodecs (global settings)
        for i in range(len(engine_codecs_list)):
            engine_name = engine_codecs_list[i]["engine_name"]
            cmd_list.append("SET ACODEC %s ON" % (engine_name))

        # turn audiocodecs on (this conference settings)
        for i in range(len(engine_codecs_list)):
            if engine_codecs_list[i]["state"]:
                engine_name = engine_codecs_list[i]["engine_name"]
                state = engine_codecs_list[i]["state"]
                cmd_list.append("SET ACODEC %s %s %s" % (engine_name, tools.num_to_defaultoff(state), cid))

        # turn audiocodecs off (this conference settings)
        for i in range(len(engine_codecs_list)):
            if not engine_codecs_list[i]["state"]:
                engine_name = engine_codecs_list[i]["engine_name"]
                state = engine_codecs_list[i]["state"]
                cmd_list.append("SET ACODEC %s %s %s" % (engine_name, tools.num_to_defaultoff(state), cid))

        return cmd_list

    def get_abonent_cmd_list(self, abonent_name: str) -> list:
        """Build list of commands for engine to configure abonent with engine abook name 'abonent_name'."""
        engine_codecs_list = self.get_engine_codecs()
        cmd_list = []

        # turn audiocodecs on
        for i in range(len(engine_codecs_list)):
            if engine_codecs_list[i]["state"]:
                engine_name = engine_codecs_list[i]["engine_name"]
                state = engine_codecs_list[i]["state"]
                cmd_list.append("ABOOK ACODEC %s %s %s" % (engine_name, tools.num_to_defaultoff(state), abonent_name))

        # turn audiocodecs off
        for i in range(len(engine_codecs_list)):
            if not engine_codecs_list[i]["state"]:
                engine_name = engine_codecs_list[i]["engine_name"]
                state = engine_codecs_list[i]["state"]
                cmd_list.append("ABOOK ACODEC %s %s %s" % (engine_name, tools.num_to_defaultoff(state), abonent_name))

        return cmd_list

    # ----- getters -----
    @property
    def aac(self) -> bool:
        return self._aac

    @property
    def g711(self) -> bool:
        return self._g711

    @property
    def g726(self) -> bool:
        return self._g726

    @property
    def g728(self) -> bool:
        return self._g728

    @property
    def g722(self) -> bool:
        return self._g722

    @property
    def g722_1_24(self) -> bool:
        return self._g722_1_24

    @property
    def g722_1_32(self) -> bool:
        return self._g722_1_32

    @property
    def g722_1c_24(self) -> bool:
        return self._g722_1c_24

    @property
    def g722_1c_32(self) -> bool:
        return self._g722_1c_32

    @property
    def g722_1c_48(self) -> bool:
        return self._g722_1c_48

    @property
    def g729(self) -> bool:
        return self._g729

    @property
    def g723_1(self) -> bool:
        return self._g723_1

    @property
    def g719(self) -> bool:
        return self._g719


def is_audio_codecs_equal(audio_codecs1: AudioCodecsEntity, audio_codecs2: AudioCodecsEntity) -> bool:
    return audio_codecs1.aac == audio_codecs2.aac and audio_codecs1.g711 == audio_codecs2.g711 and \
           audio_codecs1.g726 == audio_codecs2.g726 and audio_codecs1.g728 == audio_codecs2.g728 and \
           audio_codecs1.g722 == audio_codecs2.g722 and audio_codecs1.g722_1_24 == audio_codecs2.g722_1_24 and \
           audio_codecs1.g722_1_32 == audio_codecs2.g722_1_32 and \
           audio_codecs1.g722_1c_24 == audio_codecs2.g722_1c_24 and \
           audio_codecs1.g722_1c_32 == audio_codecs2.g722_1c_32 and \
           audio_codecs1.g722_1c_48 == audio_codecs2.g722_1c_48 and \
           audio_codecs1.g729 == audio_codecs2.g729 and audio_codecs1.g723_1 == audio_codecs2.g723_1 and \
           audio_codecs1.g719 == audio_codecs2.g719


def get_masked_audio_codecs(audio_codecs: AudioCodecsEntity, mask: AudioCodecsEntity) -> AudioCodecsEntity:
    """This function returns logical 'AND' for all audio codecs of 'audio_codecs' and 'mask'."""
    audio_codecs_list = audio_codecs.get_codecs_list()
    mask_list = mask.get_codecs_list()

    masked_list = []
    for acodec in audio_codecs_list:
        if acodec in mask_list:
            masked_list.append(acodec)

    masked_audio_codecs = AudioCodecsEntity(False)
    masked_audio_codecs.init_by_list(masked_list)
    return masked_audio_codecs


class VideoCodecsEntity:
    def set_all_fields(self, init_value: bool):
        self._h261 = init_value
        self._h263 = init_value
        self._h264 = init_value
        self._h264_high = init_value
        self._h265 = init_value

    def __init__(self, init_value: bool):
        self.set_all_fields(init_value)

    def init_codecs(self, h261: bool, h263: bool, h264: bool, h264_high: bool, h265: bool):
        self._h261 = h261
        self._h263 = h263
        self._h264 = h264
        self._h264_high = h264_high
        self._h265 = h265

    def init_by_list(self, codecs_list: list):
        features_list = features.get_features_list()
        for codec_name in codecs_list:
            features.validate_conf_setting(features_list, codec_name)

        self._h261 = "h261" in codecs_list
        self._h263 = "h263" in codecs_list
        self._h264 = "h264" in codecs_list
        self._h264_high = "h264_high" in codecs_list
        self._h265 = "h265" in codecs_list

    def get_codecs_list(self) -> list:
        codecs_list = []
        if self._h261:
            codecs_list.append("h261")
        if self._h263:
            codecs_list.append("h263")
        if self._h264:
            codecs_list.append("h264")
        if self._h264_high:
            codecs_list.append("h264_high")
        if self._h265:
            codecs_list.append("h265")
        return codecs_list

    def init_by_supported_fields(self, _features_list):
        self.set_all_fields(True)
        full_list = self.get_codecs_list()
        supported_list = []
        for codec in full_list:
            if features.is_conf_setting_supported(_features_list, codec):
                supported_list.append(codec)
        self.init_by_list(supported_list)

    def get_engine_codecs(self) -> list:
        """Tis function returns list of names of engine codecs and appropriate settings in this container."""
        codecs_list = [{"engine_name": "H261", "state": self._h261},
                       {"engine_name": "H263", "state": self._h263},
                       {"engine_name": "H264", "state": self._h264},
                       {"engine_name": "H264-HIGH", "state": self._h264_high},
                       {"engine_name": "H265", "state": self._h265}]
        return codecs_list

    def get_conference_cmd_list(self, cid: str) -> list:
        """Build list of commands for engine to configure conference with engine name 'cid'."""
        engine_codecs_list = self.get_engine_codecs()
        cmd_list = []

        # turn on all videocodecs (global settings)
        for i in range(len(engine_codecs_list)):
            engine_name = engine_codecs_list[i]["engine_name"]
            cmd_list.append("SET VCODEC %s ON" % (engine_name))

        # turn videocodecs on (this conference settings)
        for i in range(len(engine_codecs_list)):
            if engine_codecs_list[i]["state"]:
                engine_name = engine_codecs_list[i]["engine_name"]
                state = engine_codecs_list[i]["state"]
                cmd_list.append("SET VCODEC %s %s %s" % (engine_name, tools.num_to_defaultoff(state), cid))

        # turn videocodecs off (this conference settings)
        for i in range(len(engine_codecs_list)):
            if not engine_codecs_list[i]["state"]:
                engine_name = engine_codecs_list[i]["engine_name"]
                state = engine_codecs_list[i]["state"]
                cmd_list.append("SET VCODEC %s %s %s" % (engine_name, tools.num_to_defaultoff(state), cid))

        return cmd_list

    def get_abonent_cmd_list(self, abonent_name: str) -> list:
        """Build list of commands for engine to configure abonent with engine abook name 'abonent_name'."""
        engine_codecs_list = self.get_engine_codecs()
        cmd_list = []

        # turn videocodecs on
        for i in range(len(engine_codecs_list)):
            if engine_codecs_list[i]["state"]:
                engine_name = engine_codecs_list[i]["engine_name"]
                state = engine_codecs_list[i]["state"]
                cmd_list.append("ABOOK VCODEC %s %s %s" % (engine_name, tools.num_to_defaultoff(state), abonent_name))

        # turn videocodecs off
        for i in range(len(engine_codecs_list)):
            if not engine_codecs_list[i]["state"]:
                engine_name = engine_codecs_list[i]["engine_name"]
                state = engine_codecs_list[i]["state"]
                cmd_list.append("ABOOK VCODEC %s %s %s" % (engine_name, tools.num_to_defaultoff(state), abonent_name))

        return cmd_list

    # ----- getters -----
    @property
    def h261(self) -> bool:
        return self._h261

    @property
    def h263(self) -> bool:
        return self._h263

    @property
    def h264(self) -> bool:
        return self._h264

    @property
    def h264_high(self) -> bool:
        return self._h264_high

    @property
    def h265(self) -> bool:
        return self._h265


def is_video_codecs_equal(video_codecs1: VideoCodecsEntity, video_codecs2: VideoCodecsEntity) -> bool:
    return video_codecs1.h261 == video_codecs2.h261 and video_codecs1.h263 == video_codecs2.h263 and \
           video_codecs1.h264 == video_codecs2.h264 and video_codecs1.h264_high == video_codecs2.h264_high and \
           video_codecs1.h265 == video_codecs2.h265


def get_masked_video_codecs(video_codecs: VideoCodecsEntity, mask: VideoCodecsEntity) -> VideoCodecsEntity:
    """This function returns logical 'AND' for all video codecs of 'video_codecs' and 'mask'."""
    video_codecs_list = video_codecs.get_codecs_list()
    mask_list = mask.get_codecs_list()

    masked_list = []
    for vcodec in video_codecs_list:
        if vcodec in mask_list:
            masked_list.append(vcodec)

    masked_video_codecs = VideoCodecsEntity(False)
    masked_video_codecs.init_by_list(masked_list)
    return masked_video_codecs


class ContactEntity:

    def __init__(self, contact_id: ContactId, name: str, display_name: str, sip: str, h323: str, rtsp: str, email: str,
                 bitrate: int, groups: list, realaddr: str, autoadded: bool, created_at: int, conn_id: int,
                 audio_codecs: AudioCodecsEntity, video_codecs: VideoCodecsEntity, file: str, loop: bool):
        self._contact_id = contact_id
        self._name = name
        self._display_name = display_name
        self._sip = sip
        self._h323 = h323
        self._rtsp = rtsp
        self._email = email
        self._bitrate = bitrate
        self._groups = groups
        self._realaddr = realaddr
        self._autoadded = autoadded
        self._created_at = created_at
        self._conn_id = conn_id
        self._audio_codecs = audio_codecs
        self._video_codecs = video_codecs
        self._file = file
        self._loop = loop

    # ----- getters -----
    @property
    def contact_id(self) -> ContactId:
        return self._contact_id

    @property
    def name(self) -> str:
        return self._name

    @property
    def display_name(self) -> str:
        return self._display_name

    @property
    def sip(self) -> str:
        return self._sip

    @property
    def h323(self) -> str:
        return self._h323

    @property
    def rtsp(self) -> str:
        return self._rtsp

    @property
    def email(self) -> str:
        return self._email

    @property
    def bitrate(self) -> int:
        return self._bitrate

    @property
    def groups(self) -> list:
        return self._groups

    @property
    def realaddr(self) -> str:
        return self._realaddr

    @property
    def autoadded(self) -> bool:
        return self._autoadded

    @property
    def created_at(self) -> int:
        return self._created_at

    @property
    def conn_id(self) -> int:
        return self._conn_id

    @property
    def audio_codecs(self) -> AudioCodecsEntity:
        return self._audio_codecs

    @property
    def video_codecs(self) -> VideoCodecsEntity:
        return self._video_codecs

    @property
    def file(self) -> str:
        return self._file

    @property
    def loop(self) -> bool:
        return self._loop

    # ----- setters -----
    @contact_id.setter
    def contact_id(self, value):
        self._contact_id = value

    @name.setter
    def name(self, value):
        self._name = value

    @display_name.setter
    def display_name(self, value):
        self._display_name = value

    @sip.setter
    def sip(self, value):
        self._sip = value

    @h323.setter
    def h323(self, value):
        self._h323 = value

    @rtsp.setter
    def rtsp(self, value):
        self._rtsp = value

    @email.setter
    def email(self, value):
        self._email = value

    @bitrate.setter
    def bitrate(self, value):
        self._bitrate = value

    @groups.setter
    def groups(self, value):
        self._groups = value

    @realaddr.setter
    def realaddr(self, value):
        self._realaddr = value

    @autoadded.setter
    def autoadded(self, value):
        self._autoadded = value

    @created_at.setter
    def created_at(self, value):
        self._created_at = value

    @conn_id.setter
    def conn_id(self, value):
        self._conn_id = value

    @audio_codecs.setter
    def audio_codecs(self, value):
        self._audio_codecs = value

    @video_codecs.setter
    def video_codecs(self, value):
        self._video_codecs = value

    @file.setter
    def file(self, value):
        self._file = value

    @loop.setter
    def loop(self, value):
        self._loop = value

    def get_sort_field(self, sortfield):
        if sortfield == "id":
            return self._contact_id
        elif sortfield == "name":
            return self._name
        elif sortfield == "sip":
            return self._sip
        elif sortfield == "h323":
            return self._h323
        elif sortfield == "rtsp":
            return self._rtsp
        if sortfield == "email":
            return self._email
        elif sortfield == "bitrate":
            return self._bitrate
        else:
            return self._name


class GroupEntity:
    def __init__(self, group_id: int, name: str, ab_cnt):
        self._group_id = group_id
        self._name = name
        self._ab_cnt = ab_cnt

    @property
    def group_id(self) -> int:
        return self._group_id

    @property
    def name(self) -> str:
        return self._name

    @property
    def ab_cnt(self) -> int:
        return self._ab_cnt


class TimeRangeEntity:
    def __init__(self, start_time: int, end_time: int):
        self._start_time = start_time
        self._end_time = end_time

    # ----- getters -----
    @property
    def start_time(self) -> int:
        return self._start_time

    @property
    def end_time(self) -> int:
        return self._end_time


class AutoStartConferenceModeEntity:
    def __init__(self, time_range_list: list,
                 auto_start_mode: str, auto_start_week_day: int,
                 auto_start_month_day: int, first_week_day: int, last_week_day: int):
        self._time_range_list = time_range_list
        self._auto_start_mode = auto_start_mode
        self._auto_start_week_day = auto_start_week_day
        self._auto_start_month_day = auto_start_month_day
        self._first_week_day = first_week_day
        self._last_week_day = last_week_day

    # ----- getters -----
    # list of TimeRangeEntity
    @property
    def time_range_list(self) -> list:
        return self._time_range_list

    @property
    def auto_start_mode(self) -> str:
        return self._auto_start_mode

    @property
    def auto_start_week_day(self) -> int:
        return self._auto_start_week_day

    @property
    def auto_start_month_day(self) -> int:
        return self._auto_start_month_day

    @property
    def first_week_day(self) -> int:
        return self._first_week_day

    @property
    def last_week_day(self) -> int:
        return self._last_week_day

    # ----- setters -----
    @time_range_list.setter
    def time_range_list(self, value):
        self._time_range_list = value

    @auto_start_mode.setter
    def auto_start_mode(self, value):
        self._auto_start_mode = value

    @auto_start_week_day.setter
    def auto_start_week_day(self, value):
        self._auto_start_week_day = value

    @auto_start_month_day.setter
    def auto_start_month_day(self, value):
        self._auto_start_month_day = value

    @first_week_day.setter
    def first_week_day(self, value):
        self._first_week_day = value

    @last_week_day.setter
    def last_week_day(self, value):
        self._last_week_day = value


class RepeateConferenceModeEntity:
    def __init__(self, repeate_mode: str, repeate_count: int, repeate_last_time: int):
        self._repeate_mode = repeate_mode
        self._repeate_count = repeate_count
        self._repeate_last_time = repeate_last_time

    # ----- getters -----
    @property
    def repeate_mode(self) -> str:
        return self._repeate_mode

    @property
    def repeate_count(self) -> int:
        return self._repeate_count

    @property
    def repeate_last_time(self) -> int:
        return self._repeate_last_time

    # ----- setters -----
    @repeate_mode.setter
    def repeate_mode(self, value):
        self._repeate_mode = value

    @repeate_count.setter
    def repeate_count(self, value):
        self._repeate_count = value

    @repeate_last_time.setter
    def repeate_last_time(self, value):
        self._repeate_last_time = value


class StopTimeTableEntity:
    def __init__(self, stop_time_table_mode: str, stop_time_table_begin: int, stop_time_table_end: int):
        self._stop_time_table_mode = stop_time_table_mode
        self._stop_time_table_begin = stop_time_table_begin
        self._stop_time_table_end = stop_time_table_end

    # ----- getters -----
    @property
    def stop_time_table_mode(self) -> str:
        return self._stop_time_table_mode

    @property
    def stop_time_table_begin(self) -> int:
        return self._stop_time_table_begin

    @property
    def stop_time_table_end(self) -> int:
        return self._stop_time_table_end

    # ----- setters -----
    @stop_time_table_mode.setter
    def stop_time_table_mode(self, value):
        self._stop_time_table_mode = value

    @stop_time_table_begin.setter
    def stop_time_table_begin(self, value):
        self._stop_time_table_begin = value

    @stop_time_table_end.setter
    def stop_time_table_end(self, value):
        self._stop_time_table_end = value


class AutoStopConferenceEntity:
    def __init__(self, stop_mode: str, stop_duration: int, stop_abs_time: int):
        self._stop_mode = stop_mode
        self._stop_duration = stop_duration
        self._stop_abs_time = stop_abs_time

    # ----- getters -----
    @property
    def stop_mode(self) -> str:
        return self._stop_mode

    @property
    def stop_duration(self) -> int:
        return self._stop_duration

    @property
    def stop_abs_time(self) -> int:
        return self._stop_abs_time

    # ----- setters -----
    @stop_mode.setter
    def stop_mode(self, value):
        self._stop_mode = value

    @stop_duration.setter
    def stop_duration(self, value):
        self._stop_duration = value

    @stop_abs_time.setter
    def stop_abs_time(self, value):
        self._stop_abs_time = value


class ConferenceEntity:

    def __init__(self, confid: int, name: str, infinity: bool, last_start_time: int,
                 auto_start: AutoStartConferenceModeEntity,
                 repeate: RepeateConferenceModeEntity,
                 stop_time_table: StopTimeTableEntity,
                 auto_stop: AutoStopConferenceEntity,
                 duration: int, created_at: str, max_participants: int,
                 usepin: bool, pin: str, autocall: bool, rtmp: str, record_participant: int, state: str,
                 is_recording: bool, layout_mode: str, participants_cnt: int,
                 audio: dict, video: dict):
        self._confid = confid
        self._name = name
        self._infinity = infinity
        self._last_start_time = last_start_time
        self._auto_start = auto_start
        self._repeate = repeate
        self._stop_time_table = stop_time_table
        self._auto_stop = auto_stop
        self._duration = duration
        self._created_at = created_at
        self._max_participants = max_participants
        self._usepin = usepin
        self._pin = pin
        self._autocall = autocall
        self._rtmp = rtmp
        self._record_participant = record_participant
        self._state = state
        self._is_recording = is_recording
        self._layout_mode = layout_mode
        self._participants_cnt = participants_cnt
        self._audio = audio
        self._video = video

    # ----- getters -----
    @property
    def confid(self) -> int:
        return self._confid

    @property
    def name(self) -> str:
        return self._name

    @property
    def infinity(self) -> bool:
        return self._infinity

    @property
    def last_start_time(self) -> int:
        return self._last_start_time

    @property
    def auto_start(self) -> AutoStartConferenceModeEntity:
        return self._auto_start

    @property
    def repeate(self) -> RepeateConferenceModeEntity:
        return self._repeate

    @property
    def stop_time_table(self) -> StopTimeTableEntity:
        return self._stop_time_table

    @property
    def auto_stop(self) -> AutoStopConferenceEntity:
        return self._auto_stop

    @property
    def duration(self) -> int:
        return self._duration

    @property
    def created_at(self) -> str:
        return self._created_at

    @property
    def max_participants(self) -> int:
        return self._max_participants

    @property
    def usepin(self) -> bool:
        return self._usepin

    @property
    def pin(self) -> str:
        return self._pin

    @property
    def autocall(self) -> bool:
        return self._autocall

    @property
    def rtmp(self) -> str:
        return self._rtmp

    @property
    def record_participant(self) -> int:
        return self._record_participant

    @property
    def state(self) -> str:
        return self._state

    @property
    def is_recording(self) -> bool:
        return self._is_recording

    @property
    def layout_mode(self) -> str:
        return self._layout_mode

    @property
    def participants_cnt(self) -> int:
        return self._participants_cnt

    @property
    def audio(self) -> dict:
        return self._audio

    @property
    def video(self) -> dict:
        return self._video

    # ----- setters -----
    @participants_cnt.setter
    def participants_cnt(self, value):
        self._participants_cnt = value


class ParticipantEntity:

    def __init__(self, contactid: int, useaudio: bool, usevideo: bool, callab: bool, volume: int, use_audio_tx: bool,
                 volume_tx: int, use_video_tx: bool, use_pres: bool, contact: ContactEntity, render_mode: str,
                 call_protocol: str, noise_suppressor: str, auto_gain_ctrl: str):
        self._contactid = contactid
        self._useaudio = useaudio
        self._usevideo = usevideo
        self._callab = callab
        self._volume = volume
        self._use_audio_tx = use_audio_tx
        self._volume_tx = volume_tx
        self._use_video_tx = use_video_tx
        self._use_pres = use_pres
        self._contact = contact
        self._render_mode = render_mode
        self._call_protocol = call_protocol
        self._noise_suppressor = noise_suppressor
        self._auto_gain_ctrl = auto_gain_ctrl

    @property
    def contactid(self) -> int:
        return self._contactid

    @property
    def useaudio(self) -> bool:
        return self._useaudio

    @property
    def usevideo(self) -> bool:
        return self._usevideo

    @property
    def callab(self) -> bool:
        return self._callab

    @property
    def volume(self) -> int:
        return self._volume

    @property
    def use_audio_tx(self) -> bool:
        return self._use_audio_tx

    @property
    def volume_tx(self) -> int:
        return self._volume_tx

    @property
    def use_video_tx(self) -> bool:
        return self._use_video_tx

    @property
    def use_pres(self) -> bool:
        return self._use_pres

    @property
    def contact(self) -> ContactEntity:
        return self._contact

    @property
    def render_mode(self) -> str:
        return self._render_mode

    @property
    def call_protocol(self) -> str:
        return self._call_protocol

    @property
    def noise_suppressor(self) -> str:
        return self._noise_suppressor

    @property
    def auto_gain_ctrl(self) -> str:
        return self._auto_gain_ctrl


class SessionParticipantEntity:
    def __init__(self, id: str, contact_id: int, name: str, dispname: str, sipaddr: str, h323addr: str, extaddr: str,
                 online: int, video_en: bool, audio_en: bool, speaker: bool, sound_vol: int, audio_call: bool,
                 online_other_conf: bool, global_layout: bool, lyt_reg: bool, status: dict, detail_status: dict,
                 fill: bool, sortname: str, video_en_tx: bool, audio_en_tx: bool, sound_vol_tx: int, audio_power: int,
                 has_crown: bool, send_presentation: bool, is_outgoing: bool, full_bitrate: int, user_agent: str,
                 connection_id: int, render_mode: str, noise_suppressor: bool, auto_gain_ctrl: bool):
        self._id = id
        self._contact_id = contact_id
        self._name = name
        self._dispname = dispname
        self._sipaddr = sipaddr
        self._h323addr = h323addr
        self._extaddr = extaddr
        self._online = online
        self._video_en = video_en
        self._audio_en = audio_en
        self._speaker = speaker
        self._sound_vol = sound_vol
        self._audio_call = audio_call
        self._online_other_conf = online_other_conf
        self._global_layout = global_layout
        self._lyt_reg = lyt_reg
        self._status = status
        self._detail_status = detail_status
        self._fill = fill
        self._sortname = sortname
        self._video_en_tx = video_en_tx
        self._audio_en_tx = audio_en_tx
        self._sound_vol_tx = sound_vol_tx
        self._audio_power = audio_power
        self._has_crown = has_crown
        self._send_presentation = send_presentation
        self._is_outgoing = is_outgoing
        self._full_bitrate = full_bitrate
        self._user_agent = user_agent
        self._connection_id = connection_id
        self._render_mode = render_mode
        self._noise_suppressor = noise_suppressor
        self._auto_gain_ctrl = auto_gain_ctrl

    # --------- getters ---------
    @property
    def id(self) -> str:
        return self._id

    @property
    def contact_id(self) -> int:
        return self._contact_id

    @property
    def name(self) -> str:
        return self._name

    @property
    def dispname(self) -> str:
        return self._dispname

    @property
    def sipaddr(self) -> str:
        return self._sipaddr

    @property
    def h323addr(self) -> str:
        return self._h323addr

    @property
    def extaddr(self) -> str:
        return self._extaddr

    @property
    def online(self) -> int:
        return self._online

    @property
    def video_en(self) -> bool:
        return self._video_en

    @property
    def audio_en(self) -> bool:
        return self._audio_en

    @property
    def speaker(self) -> bool:
        return self._speaker

    @property
    def sound_vol(self) -> int:
        return self._sound_vol

    @property
    def audio_call(self) -> bool:
        return self._audio_call

    @property
    def online_other_conf(self) -> bool:
        return self._online_other_conf

    @property
    def global_layout(self) -> bool:
        return self._global_layout

    @property
    def lyt_reg(self) -> bool:
        return self._lyt_reg

    @property
    def status(self) -> dict:
        return self._status

    @property
    def detail_status(self) -> dict:
        return self._detail_status

    @property
    def fill(self) -> bool:
        return self._fill

    @property
    def sortname(self) -> str:
        return self._sortname

    @property
    def video_en_tx(self) -> bool:
        return self._video_en_tx

    @property
    def audio_en_tx(self) -> bool:
        return self._audio_en_tx

    @property
    def sound_vol_tx(self) -> int:
        return self._sound_vol_tx

    @property
    def audio_power(self) -> int:
        return self._audio_power

    @property
    def has_crown(self) -> bool:
        return self._has_crown

    @property
    def send_presentation(self) -> bool:
        return self._send_presentation

    @property
    def is_outgoing(self) -> bool:
        return self._is_outgoing

    @property
    def full_bitrate(self) -> int:
        return self._full_bitrate

    @property
    def user_agent(self) -> str:
        return self._user_agent

    @property
    def connection_id(self) -> int:
        return self._connection_id

    @property
    def render_mode(self) -> str:
        return self._render_mode

    @property
    def noise_suppressor(self) -> bool:
        return self._noise_suppressor

    @property
    def auto_gain_ctrl(self) -> bool:
        return self._auto_gain_ctrl

    # --------- setters ---------
    @id.setter
    def id(self, value):
        self._id = value

    @contact_id.setter
    def contact_id(self, value):
        self._contact_id = value

    @name.setter
    def name(self, value):
        self._name = value

    @dispname.setter
    def dispname(self, value):
        self._dispname = value

    @sipaddr.setter
    def sipaddr(self, value):
        self._sipaddr = value

    @h323addr.setter
    def h323addr(self, value):
        self._h323addr = value

    @extaddr.setter
    def extaddr(self, value):
        self._extaddr = value

    @online.setter
    def online(self, value):
        self._online = value

    @video_en.setter
    def video_en(self, value):
        self._video_en = value

    @audio_en.setter
    def audio_en(self, value):
        self._audio_en = value

    @speaker.setter
    def speaker(self, value):
        self._speaker = value

    @sound_vol.setter
    def sound_vol(self, value):
        self._sound_vol = value

    @audio_call.setter
    def audio_call(self, value):
        self._audio_call = value

    @online_other_conf.setter
    def online_other_conf(self, value):
        self._online_other_conf = value

    @global_layout.setter
    def global_layout(self, value):
        self._global_layout = value

    @lyt_reg.setter
    def lyt_reg(self, value):
        self._lyt_reg = value

    @status.setter
    def status(self, value):
        self._status = value

    @detail_status.setter
    def detail_status(self, value):
        self._detail_status = value

    @fill.setter
    def fill(self, value):
        self._fill = value

    @sortname.setter
    def sortname(self, value):
        self._sortname = value

    @video_en_tx.setter
    def video_en_tx(self, value):
        self._video_en_tx = value

    @audio_en_tx.setter
    def audio_en_tx(self, value):
        self._audio_en_tx = value

    @sound_vol_tx.setter
    def sound_vol_tx(self, value):
        self._sound_vol_tx = value

    @audio_power.setter
    def audio_power(self, value):
        self._audio_power = value

    @has_crown.setter
    def has_crown(self, value):
        self._has_crown = value

    @send_presentation.setter
    def send_presentation(self, value):
        self._send_presentation = value

    @is_outgoing.setter
    def is_outgoing(self, value):
        self._is_outgoing = value

    @full_bitrate.setter
    def full_bitrate(self, value):
        self._full_bitrate = value

    @user_agent.setter
    def user_agent(self, value):
        self._user_agent = value

    @connection_id.setter
    def connection_id(self, value):
        self._connection_id = value

    @render_mode.setter
    def render_mode(self, value):
        self._render_mode = value

    @noise_suppressor.setter
    def noise_suppressor(self, value):
        self._noise_suppressor = value

    @auto_gain_ctrl.setter
    def auto_gain_ctrl(self, value):
        self._auto_gain_ctrl = value


class SessionParticipantAddressEntity:

    def __init__(self, conference_id: int, participant_uri: str):
        self._conference_id = conference_id
        self._participant_uri = participant_uri

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def participant_uri(self) -> str:
        return self._participant_uri


class ConferenceParticipantAddressEntity:

    def __init__(self, conference_id: int, participant_id: int):
        self._conference_id = conference_id
        self._participant_id = participant_id

    @property
    def conference_id(self) -> int:
        return self._conference_id

    @property
    def participant_id(self) -> int:
        return self._participant_id


class DisconnectedSessionParticipantEntity:
    def __init__(self, contact_id: int, name: str, dispname: str, realuri: str, autoadded: bool):
        self._contact_id = contact_id
        self._name = name
        self._dispname = dispname
        self._realuri = realuri
        self._autoadded = autoadded

    # --------- getters ---------
    @property
    def contact_id(self) -> int:
        return self._contact_id

    @property
    def name(self) -> str:
        return self._name

    @property
    def dispname(self) -> str:
        return self._dispname

    @property
    def realuri(self) -> str:
        return self._realuri

    @property
    def autoadded(self) -> bool:
        return self._autoadded


class LayoutTypeEntity:

    def __init__(self, lyt_type: int, name: str, picture: str):
        self._lyt_type = lyt_type
        self._name = name
        self._picture = picture

    @property
    def lyt_type(self) -> int:
        return self._lyt_type

    @property
    def name(self) -> str:
        return self._name

    @property
    def picture(self) -> str:
        return self._picture


class LayoutRowEntity:

    def __init__(self, ab_num: int, types_list: list):
        self._ab_num = ab_num
        self._types_list = types_list

    @property
    def ab_num(self) -> int:
        return self._ab_num

    @property
    def types_list(self) -> list:
        return self._types_list


class OnlineLayoutAbonentEntity:

    def __init__(self, position: int, participant_id: int, name: str, in_layout: bool, role_id: int,
                 connection_id: int, render_mode: str):
        self._position = position
        self._participant_id = participant_id
        self._name = name
        self._in_layout = in_layout
        self._role_id = role_id
        self._connection_id = connection_id
        self._render_mode = render_mode

    @property
    def position(self) -> int:
        return self._position

    @property
    def participant_id(self) -> int:
        return self._participant_id

    @property
    def name(self) -> str:
        return self._name

    @property
    def in_layout(self) -> bool:
        return self._in_layout

    @property
    def role_id(self) -> int:
        return self._role_id

    @property
    def connection_id(self) -> int:
        return self._connection_id

    @property
    def render_mode(self) -> str:
        return self._render_mode


# one abonent in window of layout
class LayoutAbonentPositionEntity:

    def __init__(self, wndnum: int, abid: int, priority: int, role_id: int, render_mode: str):
        self._wndnum = wndnum
        self._abid = abid
        self._priority = priority
        self._role_id = role_id
        self._render_mode = render_mode

    @property
    def wndnum(self) -> int:
        return self._wndnum

    @property
    def abid(self) -> int:
        return self._abid

    @property
    def priority(self) -> int:
        return self._priority

    @property
    def role_id(self) -> int:
        return self._role_id

    @property
    def render_mode(self) -> str:
        return self._render_mode


class SheduledLayoutEntity:

    def __init__(self, layoutid: int, layout_type: str, auto: bool, cells_count: int, schema_type: int, wndlist: list,
                 display_name: str, template: bool):
        self._layoutid = layoutid
        self._layout_type = layout_type
        self._auto = auto
        self._cells_count = cells_count
        self._schema_type = schema_type
        self._wndlist = wndlist
        self._display_name = display_name
        self._template = template

    @property
    def layoutid(self) -> int:
        return self._layoutid

    @property
    def layout_type(self) -> str:
        return self._layout_type

    @property
    def auto(self) -> bool:
        return self._auto

    @property
    def cells_count(self) -> int:
        return self._cells_count

    @property
    def schema_type(self) -> int:
        return self._schema_type

    @property
    def wndlist(self) -> list:
        return self._wndlist

    @property
    def display_name(self) -> str:
        return self._display_name

    @property
    def template(self) -> bool:
        return self._template

    # --- setters ---
    @layoutid.setter
    def layoutid(self, value):
        self._layoutid = value

    @layout_type.setter
    def layout_type(self, value):
        self._layout_type = value

    @auto.setter
    def auto(self, value):
        self._auto = value

    @cells_count.setter
    def cells_count(self, value):
        self._cells_count = value

    @schema_type.setter
    def schema_type(self, value):
        self._schema_type = value

    @wndlist.setter
    def wndlist(self, value):
        self._wndlist = value

    @display_name.setter
    def display_name(self, value):
        self._display_name = value

    @template.setter
    def template(self, value):
        self._template = value


class RegistrationResultEntity:

    def __init__(self, registered: bool, err_code: int, err_msg: str):
        self._registered = registered
        self._err_code = err_code
        self._err_msg = err_msg

    @property
    def registered(self) -> bool:
        return self._registered

    @property
    def err_code(self) -> int:
        return self._err_code

    @property
    def err_msg(self) -> str:
        return self._err_msg


class LicenseEntity:

    def __init__(self, state: str, type: str, exp_first: int, exp_last: int, count: int, serial: str, features: list,
                 max_participants: int, max_active_participants: int, max_layouts: int, demo: bool, last_upd_fail: bool,
                 domain: str, license_params: dict):
        self._state = state
        self._type = type
        self._exp_first = exp_first
        self._exp_last = exp_last
        self._count = count
        self._serial = serial
        self._features = features
        self._max_participants = max_participants
        self._max_active_participants = max_active_participants
        self._max_layouts = max_layouts
        self._demo = demo
        self._last_upd_fail = last_upd_fail
        self. _domain = domain
        self._license_params = license_params

    @property
    def state(self) -> str:
        return self._state

    @property
    def type(self) -> str:
        return self._type

    @property
    def exp_first(self) -> int:
        return self._exp_first

    @property
    def exp_last(self) -> int:
        return self._exp_last

    @property
    def count(self) -> int:
        return self._count

    @property
    def serial(self) -> str:
        return self._serial

    @property
    def features(self) -> list:
        return self._features

    @property
    def max_participants(self) -> int:
        return self._max_participants

    @property
    def max_active_participants(self) -> int:
        return self._max_active_participants

    @property
    def max_layouts(self) -> int:
        return self._max_layouts

    @property
    def demo(self) -> bool:
        return self._demo

    @property
    def last_upd_fail(self) -> bool:
        return self._last_upd_fail

    @property
    def domain(self) -> str:
        return self._domain

    @property
    def license_params(self) -> dict:
        return self._license_params


class CallsEntity:

    def __init__(self, remote_camera_control: bool, enable_sip: bool, enable_h323: bool, preferred_protocol: str,
                 mcu_display_name: str, call_sequence: str, external_ip: str, audio_call: str,
                 enable_noise_suppressor: bool, enable_auto_gain_ctrl: bool):
        self._remote_camera_control = remote_camera_control
        self._enable_sip = enable_sip
        self._enable_h323 = enable_h323
        self._preferred_protocol = preferred_protocol
        self._mcu_display_name = mcu_display_name
        self._call_sequence = call_sequence
        self._external_ip = external_ip
        self._audio_call = audio_call
        self._enable_noise_suppressor = enable_noise_suppressor
        self._enable_auto_gain_ctrl = enable_auto_gain_ctrl

    @property
    def remote_camera_control(self) -> bool:
        return self._remote_camera_control

    @property
    def enable_sip(self) -> bool:
        return self._enable_sip

    @property
    def enable_h323(self) -> bool:
        return self._enable_h323

    @property
    def preferred_protocol(self) -> str:
        return self._preferred_protocol

    @property
    def mcu_display_name(self) -> str:
        return self._mcu_display_name

    @property
    def call_sequence(self) -> str:
        return self._call_sequence

    @property
    def external_ip(self) -> str:
        return self._external_ip

    @property
    def audio_call(self) -> str:
        return self._audio_call

    @property
    def enable_noise_suppressor(self) -> bool:
        return self._enable_noise_suppressor

    @property
    def enable_auto_gain_ctrl(self) -> bool:
        return self._enable_auto_gain_ctrl


class SipEntity:

    def __init__(self, register: bool, server: str, username: str, password: str,
                 transport_protocol: str, registration: bool, sip_bfcp: str, use_ice: bool,
                 turn_addr: str, sip_port: int, bfcp_port_min: int, bfcp_port_max: int):
        self._register = register
        self._server = server
        self._username = username
        self._password = password
        self._transport_protocol = transport_protocol
        self._registration = registration
        self._sip_bfcp = sip_bfcp
        self._use_ice = use_ice
        self._turn_addr = turn_addr
        self._sip_port = sip_port
        self._bfcp_port_min = bfcp_port_min
        self._bfcp_port_max = bfcp_port_max

    @property
    def register(self) -> bool:
        return self._register

    @property
    def server(self) -> str:
        return self._server

    @property
    def username(self) -> str:
        return self._username

    @property
    def password(self) -> str:
        return self._password

    @property
    def transport_protocol(self) -> str:
        return self._transport_protocol

    # read only flag
    @property
    def registration(self) -> bool:
        return self._registration

    @property
    def sip_bfcp(self) -> str:
        return self._sip_bfcp

    @property
    def use_ice(self) -> bool:
        return self._use_ice

    @property
    def turn_addr(self) -> str:
        return self._turn_addr

    @property
    def sip_port(self) -> int:
        return self._sip_port

    @property
    def bfcp_port_min(self) -> int:
        return self._bfcp_port_min

    @property
    def bfcp_port_max(self) -> int:
        return self._bfcp_port_max


class H323SettingsEntity:

    def __init__(self, use_gatekeeper: bool, id_num: str, e164_extension: str, server: str, zone: str,
                 registration: bool, h46017: bool, h46018: bool, h46019: bool,
                 h323_port: int, h245_port_min: int, h245_port_max: int):
        self._use_gatekeeper = use_gatekeeper
        self._id_num = id_num
        self._e164_extension = e164_extension
        self._server = server
        self._zone = zone
        self._registration = registration
        self._h46017 = h46017
        self._h46018 = h46018
        self._h46019 = h46019
        self._h323_port = h323_port
        self._h245_port_min = h245_port_min
        self._h245_port_max = h245_port_max

    @property
    def use_gatekeeper(self) -> bool:
        return self._use_gatekeeper

    @property
    def id_num(self) -> str:
        return self._id_num

    @property
    def e164_extension(self) -> str:
        return self._e164_extension

    @property
    def server(self) -> str:
        return self._server

    @property
    def zone(self) -> str:
        return self._zone

    @property
    def registration(self) -> bool:
        return self._registration

    @property
    def h46017(self) -> bool:
        return self._h46017

    @property
    def h46018(self) -> bool:
        return self._h46018

    @property
    def h46019(self) -> bool:
        return self._h46019

    @property
    def h323_port(self) -> int:
        return self._h323_port

    @property
    def h245_port_min(self) -> int:
        return self._h245_port_min

    @property
    def h245_port_max(self) -> int:
        return self._h245_port_max


class VideoSettingsEntity:

    def __init__(self, video_storage_size: int, stream1_priority: str, stream2_priority: str, content_wide_band: str,
                 adapt_bitrate: bool, stream_control: bool, packet_recovery: bool, background: str, rolltimeout: int):
        self._video_storage_size = video_storage_size
        self._stream1_priority = stream1_priority
        self._stream2_priority = stream2_priority
        self._content_wide_band = content_wide_band
        self._adapt_bitrate = adapt_bitrate
        self._stream_control = stream_control
        self._packet_recovery = packet_recovery
        self._background = background
        self._rolltimeout = rolltimeout

    @property
    def video_storage_size(self) -> int:
        return self._video_storage_size

    @property
    def stream1_priority(self) -> str:
        return self._stream1_priority

    @property
    def stream2_priority(self) -> str:
        return self._stream2_priority

    @property
    def content_wide_band(self) -> str:
        return self._content_wide_band

    @property
    def adapt_bitrate(self) -> bool:
        return self._adapt_bitrate

    @property
    def stream_control(self) -> bool:
        return self._stream_control

    @property
    def packet_recovery(self) -> bool:
        return self._packet_recovery

    @property
    def background(self) -> str:
        return self._background

    @property
    def rolltimeout(self) -> int:
        return self._rolltimeout


class LDAPSettingsEntity:

    def __init__(self, server: str, port: int, distinct_name: str, filter: str):
        self._server = server
        self._port = port
        self._distinct_name = distinct_name
        self._filter = filter

    @property
    def server(self) -> str:
        return self._server

    @property
    def port(self) -> int:
        return self._port

    @property
    def distinct_name(self) -> str:
        return self._distinct_name

    @property
    def filter(self) -> str:
        return self._filter


class EmailSettingsEntity:

    def __init__(self, smtp_server: str, username: str, password: str):
        self._smtp_server = smtp_server
        self._username = username
        self._password = password

    @property
    def smtp_server(self) -> str:
        return self._smtp_server

    @property
    def username(self) -> str:
        return self._username

    @property
    def password(self) -> str:
        return self._password


class DBSettingsEntity:

    def __init__(self, backup_place: str, shared_dir_url: str, username: str, password: str, backup_type: str,
                 weekday: str, hour: int, minute: int):
        self._backup_place = backup_place
        self._shared_dir_url = shared_dir_url
        self._username = username
        self._password = password
        self._backup_type = backup_type
        self._weekday = weekday
        self._hour = hour
        self._minute = minute

    @property
    def shared_dir_url(self) -> str:
        return self._shared_dir_url

    @property
    def backup_place(self) -> str:
        return self._backup_place

    @property
    def username(self) -> str:
        return self._username

    @property
    def password(self) -> str:
        return self._password

    @property
    def backup_type(self) -> str:
        return self._backup_type

    @property
    def weekday(self) -> str:
        return self._weekday

    @property
    def hour(self) -> int:
        return self._hour

    @property
    def minute(self) -> int:
        return self._minute


class MemoryDashboardEntity:

    def __init__(self, all: int, using: int, free: int):
        self._all = all
        self._using = using
        self._free = free

    @property
    def all(self) -> int:
        return self._all

    @property
    def using(self) -> int:
        return self._using

    @property
    def free(self) -> int:
        return self._free


class CoreEntity:

    def __init__(self, user: float, system: float, idle: float):
        self._user = user
        self._system = system
        self._idle = idle

    @property
    def user(self) -> float:
        return self._user

    @property
    def system(self) -> float:
        return self._system

    @property
    def idle(self) -> float:
        return self._idle


class NetInterfaceEntity:

    def __init__(self, name: str, rx: int, tx: int):
        self._name = name
        self._rx = rx
        self._tx = tx

    @property
    def name(self) -> str:
        return self._name

    @property
    def rx(self) -> int:
        return self._rx

    @property
    def tx(self) -> int:
        return self._tx


class FreeSpaceEntity:

    def __init__(self, name: str, free: int, total: int):
        self._name = name
        self._free = free
        self._total = total

    @property
    def name(self) -> str:
        return self._name

    @property
    def free(self) -> int:
        return self._free

    @property
    def total(self) -> int:
        return self._total


class StringRecordEntity:
    def __init__(self, id: int, name: str):
        self._id = id
        self._name = name

    @property
    def id(self) -> int:
        return self._id

    @property
    def name(self) -> str:
        return self._name


class LayoutWindowCoordEntity:
    def __init__(self, position: int, x: int, y: int, width: int, height: int, z_order: int):
        self._position = position
        self._x = x
        self._y = y
        self._width = width
        self._height = height
        self._z_order = z_order

    # ----- getters -----
    @property
    def position(self) -> int:
        return self._position

    @property
    def x(self) -> int:
        return self._x

    @property
    def y(self) -> int:
        return self._y

    @property
    def width(self) -> int:
        return self._width

    @property
    def height(self) -> int:
        return self._height

    @property
    def z_order(self) -> int:
        return self._z_order

    # ----- setters -----
    @position.setter
    def position(self, value):
        self._position = value

    @x.setter
    def x(self, value):
        self._x = value

    @y.setter
    def y(self, value):
        self._y = value

    @width.setter
    def width(self, value):
        self._width = value

    @height.setter
    def height(self, value):
        self._height = value

    @z_order.setter
    def z_order(self, value):
        self._z_order = value



class WaitAbonentEntity:
    def __init__(self, address: str, status: dict, conn_id: int, name: str):
        self._address = address
        self._status = status
        self._conn_id = conn_id
        self._name = name

    @property
    def address(self) -> str:
        return self._address

    @property
    def status(self) -> dict:
        return self._status

    @property
    def conn_id(self) -> int:
        return self._conn_id

    @property
    def name(self) -> str:
        return self._name


class RecordingURLEntity:
    def __init__(self, type: str, url: str):
        self._type = type
        self._url = url

    @property
    def type(self) -> str:
        return self._type

    @property
    def url(self) -> str:
        return self._url


class FileEntity:
    def __init__(self, filename: str, size: int, create_time_sec: int):
        self._filename = filename
        self._size = size
        self._create_time_sec = create_time_sec

    @property
    def filename(self) -> str:
        return self._filename

    @property
    def size(self) -> int:
        return self._size

    @property
    def create_time_sec(self) -> int:
        return self._create_time_sec


class ConfSessionParticipantEntity:
    def __init__(self, conf_id: int, contact_id: int, address: str, name: str, conn_id: int):
        self._conf_id = conf_id
        self._contact_id = contact_id
        self._address = address
        self._name = name
        self._conn_id = conn_id

    # --------- getters ---------
    @property
    def conf_id(self) -> int:
        return self._conf_id

    @property
    def contact_id(self) -> int:
        return self._contact_id

    @property
    def address(self) -> str:
        return self._address

    @property
    def name(self) -> str:
        return self._name

    @property
    def conn_id(self) -> int:
        return self._conn_id


class RTPEntity:
    def __init__(self, rtpportmin: int, rtpportmax: int, mtu: int, dscp_audio: int, dscp_video: int, dscp_other: int):
        self._rtpportmin = rtpportmin
        self._rtpportmax = rtpportmax
        self._mtu = mtu
        self._dscp_audio = dscp_audio
        self._dscp_video = dscp_video
        self._dscp_other = dscp_other

    @property
    def rtpportmin(self) -> int:
        return self._rtpportmin

    @property
    def rtpportmax(self) -> int:
        return self._rtpportmax

    @property
    def mtu(self) -> int:
        return self._mtu

    @property
    def dscp_audio(self) -> int:
        return self._dscp_audio

    @property
    def dscp_video(self) -> int:
        return self._dscp_video

    @property
    def dscp_other(self) -> int:
        return self._dscp_other


class IPAddressMaskEntity:
    def __init__(self, ipaddr: str, netmask: str):
        self._ipaddr = ipaddr
        self._netmask = netmask

    @property
    def ipaddr(self) -> str:
        return self._ipaddr

    @property
    def netmask(self) -> str:
        return self._netmask


class NetworkSettingsEntity:
    def __init__(self, ifname: str, mac: str, use_dhcp: bool, ipaddr_list: list, gateway: str, nameservers: list,
                 ready: bool, conn_uuid: str, type: str):
        self._ifname = ifname
        self._mac = mac
        self._use_dhcp = use_dhcp
        self._ipaddr_list = ipaddr_list
        self._gateway = gateway
        self._nameservers = nameservers
        self._ready = ready
        self._conn_uuid = conn_uuid
        self._type = type

    # ----- getters -----
    @property
    def ifname(self) -> str:
        return self._ifname

    @property
    def mac(self) -> str:
        return self._mac

    @property
    def use_dhcp(self) -> bool:
        return self._use_dhcp

    # list of IPAddressMaskEntity
    @property
    def ipaddr_list(self) -> list:
        return self._ipaddr_list

    @property
    def gateway(self) -> str:
        return self._gateway

    # list of strings
    @property
    def nameservers(self) -> list:
        return self._nameservers

    @property
    def ready(self) -> bool:
        return self._ready

    @property
    def conn_uuid(self) -> str:
        return self._conn_uuid

    @property
    def type(self) -> str:
        return self._type

    # ----- setters -----
    @ifname.setter
    def ifname(self, value):
        self._ifname = value

    @mac.setter
    def mac(self, value):
        self._mac = value

    @use_dhcp.setter
    def use_dhcp(self, value):
        self._use_dhcp = value

    # list of IPAddressMaskEntity
    @ipaddr_list.setter
    def ipaddr_list(self, value):
        self._ipaddr_list = value

    @gateway.setter
    def gateway(self, value):
        self._gateway = value

    # list of strings
    @nameservers.setter
    def nameservers(self, value):
        self._nameservers = value

    @ready.setter
    def ready(self, value):
        self._ready = value

    @conn_uuid.setter
    def conn_uuid(self, value):
        self._conn_uuid = value

    @type.setter
    def type(self, value):
        self._type = value


class CustomLayoutTypeSlotEntity:
    def __init__(self, x: int, y: int, width: int, height: int):
        self._x = x
        self._y = y
        self._width = width
        self._height = height

    @property
    def x(self) -> int:
        return self._x

    @property
    def y(self) -> int:
        return self._y

    @property
    def width(self) -> int:
        return self._width

    @property
    def height(self) -> int:
        return self._height


class CustomLayoutTypeEntity:
    def __init__(self, layout_type: int, width: int, height: int, centered_row: int, selected_slots: list):
        self._layout_type = layout_type
        self._width = width
        self._height = height
        self._centered_row = centered_row
        self._selected_slots = selected_slots

    @property
    def layout_type(self) -> int:
        return self._layout_type

    @property
    def width(self) -> int:
        return self._width

    @property
    def height(self) -> int:
        return self._height

    @property
    def centered_row(self) -> int:
        return self._centered_row

    # list of CustomLayoutTypeSlotEntity
    @property
    def selected_slots(self) -> list:
        return self._selected_slots


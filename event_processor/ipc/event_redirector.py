#!/usr/bin/python3

import sys

import event_processor.ipc.redirector_tools as redirector_tools

if __name__ != "__main__":
    sys.exit(1)

redirector_tools.launch_redirector()

#!/usr/bin/python3

import logging
import logging.handlers
import os
import os.path
import signal
import sys
import time

import app.core.logger as logger
import app.core.pipetools as pipetools

PID_FILE = '/var/run/event_redirector.pid'


def term_handler(signum, frame):
    # TODO
    # Remove pid file
    if sys.platform != "win32":
        if os.path.exists(PID_FILE):
            sys.stderr.write("term_handler(): pid file found, delete it\n")
            os.unlink(PID_FILE)
        else:
            sys.stderr.write("term_handler(): cannot find pid file\n")
    sys.exit(1)


def create_pid_file():
    # Create PID file
    if sys.platform != "win32":
        own_pid = os.getpid()
        own_pid_str = str(own_pid)
        own_pid_bytes = own_pid_str.encode("UTF-8")

        try:  # Try create PID file
            fd = os.open(PID_FILE, os.O_WRONLY | os.O_CREAT | os.O_EXCL)
            os.write(fd, own_pid_bytes)
            os.close(fd)
        except:
            try:
                fd = os.open(PID_FILE, os.O_RDONLY)
                try:
                    b = os.read(fd, 10)
                    s = b.decode("UTF-8")
                    if len(s) != 0:
                        pid = int(s)
                        sys.stderr.write(
                            "Another instance already running, try kill another instance with pid=" + str(pid) + "\n")
                        os.kill(pid, signal.SIGKILL)
                except:
                    sys.stderr.write("redirector: read pid file / kill pid: exception: " +  str(sys.exc_info()))
                os.close(fd)
                os.unlink(PID_FILE)
            except:
                sys.stderr.write("redirector: open pid file / delete pid file: exception: " + str(sys.exc_info()))
            # Retry create PID file
            try:
                fd = os.open(PID_FILE, os.O_WRONLY | os.O_CREAT | os.O_EXCL)
                os.write(fd, own_pid_bytes)
                os.close(fd)
            except:
                sys.stderr.write("Failed to create PID file\n")
                sys.exit(1)


def redirect_messages():
    client_p = pipetools.open_client_pipe()

    if client_p != None:
        logging.debug("pipe successfully opened")
        is_pipe_open = 1

        # process server's messages
        while 1:
            # read message
            try:
                message = input("")
            except:
                break
            if not message:
                continue
            logging.debug("redirect message: " + message)

            if sys.platform == "win32" and is_pipe_open == 0:  # ----- WINDOWS VERSION -----
                client_p = pipetools.open_client_pipe()

            pipetools.write(client_p, message)  # redirect message to event processor's pipe

            if sys.platform == "win32":  # ----- WINDOWS VERSION -----
                pipetools.close_pipe(client_p, 0)
                is_pipe_open = 0

        if sys.platform != "win32" or is_pipe_open == 1:
            pipetools.close_pipe(client_p, 0)
    else:
        logging.error("Failed to open client socket")


def launch_redirector():
    signal.signal(signal.SIGTERM, term_handler)

    create_pid_file()

    # Initialize logging
    logger.init_logger("event_redirector.log")

    # wait for server pipe is opened
    time.sleep(3)

    logging.debug("Try to open pipe")

    redirect_messages()

    # Remove pid file
    if sys.platform != "win32":
        if os.path.exists(PID_FILE):
            sys.stderr.write("launch_redirector(): pid file found, delete it\n")
            os.unlink(PID_FILE)
        else:
            sys.stderr.write("launch_redirector(): cannot find pid file\n")

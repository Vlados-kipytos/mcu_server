#!/usr/bin/python3

import logging
import sys
# TMP: TO DEBUG
import time

import app.core.addrbook as addrbook
import app.core.calls_history as calls_history
import app.core.conftools as conftools
import app.core.constants as constants
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.features as features
import app.core.layoutbackup as layoutbackup
import app.core.layoutcfgtools as layoutcfgtools
import app.core.licenses as licenses
import app.core.nettools as nettools
import app.core.onlinetools as onlinetools
import app.core.sysinfotools as sysinfotools
import app.core.tools as tools
import event_processor.ipc.on_event_tools as on_event_tools
import event_processor.ipc.state_controller as state_controller
from app.core.requests import AddCallsHistoryEventRequest

def parse_vcs_message(msg):
    """This function parse message 'msg' from VCS-server (one line).
    Returns parsed params of message."""
    result = {"type": "", "params": []}

    msg_len = len(msg)
    if msg_len == 0:
        return (result)

    # parse type of message
    i = 0
    type_str = ""
    while i < msg_len:
        if msg[i] != " ":
            type_str = type_str + msg[i]
        else:
            break
        i = i + 1

    result["type"] = type_str

    # set pointer to begin of params
    i = i + 1
    if i >= msg_len:
        return (result)

    # parse params list ( name1=value1 name2=value2 ... )
    param_name = ""
    param_value = ""
    bParseName = 1
    while i < msg_len:
        if bParseName == 1:
            if msg[i] == " ":
                i = i + 1
                continue
            if msg[i] != "=":
                param_name = param_name + msg[i]
            else:
                bParseName = 0
        else:
            if msg[i] != " ":
                param_value = param_value + msg[i]
            else:
                result["params"].append({"name": param_name, "value": param_value})
                param_name = ""
                param_value = ""
                bParseName = 1
        i = i + 1

    # process tail
    if param_name != "" and param_value != "":
        result["params"].append({"name": param_name, "value": param_value})

    return (result)


def print_message(parsed_msg):
    """This function print to stdout parsed message 'parsed_msg'."""
    if "type" in parsed_msg:
        print("type:" + parsed_msg["type"])
    if "params" in parsed_msg:
        for prm in parsed_msg["params"]:
            print(prm["name"] + "=" + prm["value"])


def find_value_by_name(parsed_msg, name):
    """This function finds 'name' in 'parsed_msg' and returns appropriate value.
    If value is not found, then returns empty JavaScript"""
    if "params" not in parsed_msg:
        return ("")
    for item in parsed_msg["params"]:
        if item["name"] == name:
            return (item["value"])
    return ("")


def get_confid_by_abonent(abid):
    """This function find active conference by its abonent 'abid'.
    Return value: conference id (at error returns 0)."""
    abid = int(abid)
    conf_rows = dbtools.load_table("legacy.conferences", "id, usepin",
                                   "(state=%d or state=%d) and id in (SELECT confid FROM legacy.confabonents WHERE abid=%d)" % (
                                       data.CONF_STATE_ACTIVE, data.CONF_STATE_START, abid))
    if len(conf_rows) == 0:
        return (0)  # error
    confid = conf_rows[0][0]
    return (confid)


def get_confid_list_by_abonent(abid):
    """This function find active conferences by its abonent 'abid'.
    Return value: conferences id list (at error returns empty list)."""
    abid = int(abid)
    conf_rows = dbtools.load_table("legacy.conferences", "id",
                                   "(state=%d or state=%d) and id in (SELECT confid FROM legacy.confabonents WHERE abid=%d)" % (
                                       data.CONF_STATE_ACTIVE, data.CONF_STATE_START, abid))
    if len(conf_rows) == 0:
        return []  # error
    id_list = []
    for row in conf_rows:
        id_list.append(row[0])
    return id_list


def exist_active_conf_with_pin():
    conf_rows = dbtools.load_table("legacy.conferences", "id", "(state=%d or state=%d) and usepin<>0" % (
        data.CONF_STATE_ACTIVE, data.CONF_STATE_START))
    if len(conf_rows) == 0:
        return (0)
    else:
        return (1)


def get_confid_by_pin(pin):
    """This function find active conference with used pin code 'pin'."""
    pin = str(pin)
    conf_rows = dbtools.load_table("legacy.conferences", "id", "(state=%d or state=%d) and usepin<>0 and pin='%s'" % (
        data.CONF_STATE_ACTIVE, data.CONF_STATE_START, pin))
    if len(conf_rows) == 0:
        return (0)
    confid = conf_rows[0][0]
    return (confid)


def get_confid_without_pin(abid):
    """This function find active conference (without pin) by its abonent 'abid'.
    Return value: conference id (at error returns 0)."""
    abid = int(abid)
    conf_rows = dbtools.load_table("legacy.conferences", "id",
                                   "(state=%d or state=%d) and usepin=0 and id in (SELECT confid FROM legacy.confabonents WHERE abid=%d)" % (
                                       data.CONF_STATE_ACTIVE, data.CONF_STATE_START, abid))
    if len(conf_rows) == 0:
        return (0)  # error
    confid = conf_rows[0][0]
    return (confid)


def hangup_call(aburi):
    nettools.exec_cmd_list_s(["HANGUP PEER " + aburi])


def is_hangup_all(confid):
    """This function returnn 1 if specified conference is in closing state (all abonents hangups). Else returns 0."""
    confid = int(confid)
    conf_rows = dbtools.load_table("legacy.conferences", "state", "id=%d AND deleted_at is null" % (confid))
    if len(conf_rows) != 0:
        state = conf_rows[0][0]
        if state == data.CONF_STATE_PRE_INACTIVE or state == data.CONF_STATE_POST_INACTIVE or state == data.CONF_STATE_CLOSING_VILE:
            return (1)
    return (0)


def get_secret_level():
    """This function returns secret level"""
    config_rows = dbtools.load_table("legacy.commonconfig", "secret", "")
    if len(config_rows) != 0:
        return (config_rows[0][0])
    else:
        return (0)


# dictionary of conferences { 'confid_in': 'confname' }
g_conferences_id = {}

# list of conferences [ { 'confid': ... , 'abid': ... , 'aburi': ... }, ... ]
g_confabonents = []

# limit of abonents conected to MCU (buffered value)
g_max_abonents = 10

# limit of abonents showed in laouts
g_max_visible = 10

# id of unregistered in DB abonents / conferences
OUTSIDER_SHIFT = 1000000000
g_outside_abid = OUTSIDER_SHIFT
g_outside_confid = OUTSIDER_SHIFT


def update_confabonents_list(confid, abid, aburi, conn_id):
    global g_confabonents

    confid = int(confid)
    abid = int(abid)

    cnt = len(g_confabonents)

    i = 0
    while i < cnt:
        if g_confabonents[i]["conn_id"] == conn_id:
            break
        i = i + 1

    if i >= cnt:
        g_confabonents.append({"confid": confid, "abid": abid, "aburi": aburi, "conn_id": conn_id})
        return (1)
    else:
        if confid != 0:
            g_confabonents[i]["confid"] = confid
        if abid != 0:
            g_confabonents[i]["abid"] = abid
        if aburi != "":
            g_confabonents[i]["aburi"] = aburi
        if conn_id != -1:
            g_confabonents[i]["conn_id"] = conn_id
        return (1)


def get_confab_params(aburi, conn_id):
    global g_confabonents
    global g_outside_abid

    result = {"confid": 0, "abid": 0, "aburi": aburi, "conn_id": conn_id}

    cnt = len(g_confabonents)

    i = 0
    while i < cnt:
        if g_confabonents[i]["conn_id"] == conn_id:
            break
        i = i + 1

    prm = on_event_tools.get_move_to_conf_prm(aburi, conn_id)

    if i < cnt:
        if prm["conn_id"] == conn_id and prm["conf_from"] == g_confabonents[i]["confid"]:
            g_confabonents[i]["confid"] = prm["conf_to"]
            on_event_tools.del_move_to_conf_prm(aburi, conn_id)

        result["confid"] = g_confabonents[i]["confid"]
        result["abid"] = g_confabonents[i]["abid"]
    elif prm["conn_id"] == conn_id:
        on_event_tools.del_move_to_conf_prm(aburi, conn_id)
        new_confid = prm["conf_to"]
        new_abid = prm["abid"]

        if new_abid == 0:
            new_abid = g_outside_abid
            g_outside_abid += 1

        g_confabonents.append({"confid": new_confid, "abid": new_abid, "aburi": aburi, "conn_id": conn_id})
        result["confid"] = new_confid
        result["abid"] = new_abid

    return (result)


def just_get_confab_params(aburi, conn_id):
    global g_confabonents

    result = {"confid": 0, "abid": 0, "aburi": aburi, "conn_id": conn_id}

    cnt = len(g_confabonents)

    i = 0
    while i < cnt:
        if g_confabonents[i]["conn_id"] == conn_id:
            break
        i = i + 1

    if i < cnt:
        result["confid"] = g_confabonents[i]["confid"]
        result["abid"] = g_confabonents[i]["abid"]

    return (result)


def get_connected_to_conf_abonents(confid):
    """This function returns count of abonents connected to conference 'confid'."""
    global g_confabonents

    confid = int(confid)

    count = 0
    for confab in g_confabonents:
        if confab["confid"] == confid:
            count += 1

    return count


def is_max_abonents_connected(_conn_id : int) -> bool:
    """Check count of abonents connected to MCU"""
    global g_confabonents
    global g_max_abonents

    ab_in_list = False
    for confab in g_confabonents:
        if confab["conn_id"] == _conn_id:
            ab_in_list = True
            break

    if ab_in_list: # 2-nd phase of configure abonent (it is not new abonent)
        return False

    return len(g_confabonents) >= g_max_abonents


def get_abonents_list(confid):
    global g_confabonents

    confid = int(confid)

    cnt = len(g_confabonents)

    ablist = []

    i = 0
    while i < cnt:
        if g_confabonents[i]["confid"] == confid:
            ablist.append([g_confabonents[i]["aburi"], g_confabonents[i]["abid"]])
        i = i + 1

    return (ablist)


def del_confabonent_from_list(aburi, conn_id):
    global g_confabonents

    result = {"deleted": 0, "uri": aburi, "abid": 0, "conn_id": conn_id}

    cnt = len(g_confabonents)

    i = 0
    while i < cnt:
        if g_confabonents[i]["conn_id"] == conn_id:
            break
        i = i + 1

    if i < cnt:
        result["deleted"] = 1
        result["abid"] = g_confabonents[i]["abid"]
        result["confid"] = g_confabonents[i]["confid"]
        del g_confabonents[i]
        return (result)
    else:
        return (result)


def get_confid(confid_in):
    global g_conferences_id
    global g_confabonents

    if not g_conferences_id.has_key(confid_in):
        return (0)

    confname = g_conferences_id[confid_in]

    confid_str = confname[len("conference"):]

    confid = int(confid_str)
    return (confid)


def get_abonents_count(confid_in):
    global g_confabonents

    confid = get_confid(confid_in)

    cnt = len(g_confabonents)
    n = 0
    abcnt = 0
    while n < cnt:
        if g_confabonents[n]["confid"] == confid:
            abcnt = abcnt + 1
        n = n + 1

    return (abcnt)


def clean_event_processor():
    global g_conferences_id
    global g_confabonents

    # clean dictionary of conferences
    g_conferences_id = {}

    # clean list of conferences
    g_confabonents = []

    # if server is restarted, stop all conferences
    dbtools.make_sql_request("UPDATE legacy.conferences SET state=%d WHERE deleted_at is null" %
                             (data.CONF_STATE_POST_INACTIVE))
    dbtools.make_sql_request("DELETE FROM legacy.movetoconf")
    layoutbackup.clean_all_layout_backups()
    addrbook.clear_all_realaddr()


def get_outsider_prm(uri, conf_id, ab_id):
    """This function find in internal list outsider 'uri'.
    If uri found, then returns its params.
    Else generate new params and returns its."""
    global g_outside_abid
    global g_outside_confid

    conf_id = int(conf_id)
    ab_id = int(ab_id)

    if ab_id != 0:
        abid = ab_id
    else:
        abid = g_outside_abid
        g_outside_abid = g_outside_abid + 1

    if conf_id != 0:
        confid = conf_id
    else:
        confid = g_outside_confid
        g_outside_confid = g_outside_confid + 1

    return ({"uri": uri, "abid": abid, "confid": confid, "added": 1})


def is_outsider_id(id):
    """This function checks abonent id or conference id.
    If id is outsider's id, then return 1, else return 0."""
    if id >= OUTSIDER_SHIFT:
        return (1)
    else:
        return (0)


def process_server_message(message):
    """This function process server message 'message'."""
    global g_conferences_id
    global g_confabonents
    global g_max_abonents
    global g_max_visible

    # parse message
    parsed_message = parse_vcs_message(message)

    # TMP: ECHO
    logging.debug(message)

    if "type" not in parsed_message:
        logging.warning("message '" + message + "' has not 'type' field")
        # logging.debug("message '" + message + "' has not 'type' field")
        return (0)

    if parsed_message["type"] == "REGISTRATION":
        lic_status = find_value_by_name(parsed_message, "STATUS")
        lic_serial = find_value_by_name(parsed_message, "SERIAL")
        license = find_value_by_name(parsed_message, "LICENSE")
        licenses_cnt = find_value_by_name(parsed_message, "LICENSES")
        if licenses_cnt == "":
            licenses_cnt = "0"
        lic_type = find_value_by_name(parsed_message, "TYPE")
        exp_first = find_value_by_name(parsed_message, "EXP_FIRST")
        exp_last = find_value_by_name(parsed_message, "EXP_LAST")
        if lic_status == "OFF":
            logging.debug("license off")
            licenses.update_license_state_in_db(data.LIC_STAT_OFF, lic_type, exp_first, exp_last, licenses_cnt,
                                                lic_serial)
            on_event_tools.stop_all_conferences()
            #OLD:licenses.smart_upd_lic_from_srv(0)
            #OLD:sys.exit(1)  # restart all by vcsd
        elif lic_status == "ON" and license == "INVALID":
            logging.debug("license invalid")
            licenses.update_license_state_in_db(data.LIC_STAT_INVALID, lic_type, exp_first, exp_last, licenses_cnt,
                                                lic_serial)
            on_event_tools.stop_all_conferences()
            licenses.smart_upd_lic_from_srv(0)
            sys.exit(1)  # restart all vcsd
        elif lic_status == "ON" and license == "VALID":
            logging.debug("license valid")
            licenses.update_license_state_in_db(data.LIC_STAT_VALID, lic_type, exp_first, exp_last, licenses_cnt,
                                                lic_serial)
            limits = sysinfotools.get_abonents_restriction()
            g_max_abonents = limits["max_calls"]
            g_max_visible = limits["max_visible"]
            logging.debug("max abonents = %d , max visible abonents = %d" % (g_max_abonents, g_max_visible))
            b_demo = licenses.get_demo_flag()
            licenses.send_event_to_crm(lic_type, exp_first, exp_last, licenses_cnt, g_max_abonents, g_max_visible, b_demo)

    # check license state and block processing if no license
    lic_stat_rows = dbtools.select("SELECT lic_stat FROM legacy.commonconfig")
    if len(lic_stat_rows) != 0:
        cur_lic_stat = lic_stat_rows[0][0]
        if cur_lic_stat == data.LIC_STAT_OFF or cur_lic_stat == data.LIC_STAT_INVALID:
            logging.debug("cur_lic_stat=%d" % (cur_lic_stat))
            logging.debug("stop processing by bad license")
            return (0)

    if parsed_message["type"] == "START":
        logging.info("vtsrv started")
        clean_event_processor()
        state_controller.init_state_controller()

    if parsed_message["type"] == "FEATURES":
        features.set_features(message)
        features_list = message.split()
        features.update_conf_features(features_list)
        features.update_global_features(features_list)
        if not features.get_feature(features_list, "VAD"):
            layoutcfgtools.turn_off_vad_in_all_layouts()

    if parsed_message["type"] == "CONFERENCE":
        internal_id = find_value_by_name(parsed_message, "ID")
        name = find_value_by_name(parsed_message, "NAME")
        status = find_value_by_name(parsed_message, "STATUS")
        if status == "REGISTERED":
            g_conferences_id[internal_id] = name  # conferenece name
            #cmd_list = ["LAYOUT BACKGROUND ON " + name + " PRESENTATION"]
            cmd_list = [ "LAYOUT DN OFF " + name + "  PRESENTATION" ]
            nettools.exec_cmd_list_s(cmd_list)
            limits = sysinfotools.get_abonents_restriction()
            g_max_abonents = limits["max_calls"]
            g_max_visible = limits["max_visible"]
            logging.debug("max abonents = %d , max visible abonents = %d" % (g_max_abonents, g_max_visible))
        elif status == "UNREGISTERED":
            del g_conferences_id[internal_id]

    if parsed_message["type"] == "CALL":
        direction = find_value_by_name(parsed_message, "DIR")
        status = find_value_by_name(parsed_message, "STATUS")
        abonent_uri = find_value_by_name(parsed_message, "URI")
        layout = find_value_by_name(parsed_message, "LAYOUT")
        confid_in = find_value_by_name(parsed_message, "CONFERENCE")
        pin = find_value_by_name(parsed_message, "PIN")
        conn_id = tools.str_to_int(find_value_by_name(parsed_message, "ID"))
        #secret = find_value_by_name(parsed_message, "CLASSIFIED")

        if status == "INACTIVE":
            res = on_event_tools.get_outgoing_call(abonent_uri) # if outgoing call failed
            if res["abid"] != 0:
                on_event_tools.unregister_abonent_in_abook(res["abid"]) # clear engine abook
            result = del_confabonent_from_list(abonent_uri, conn_id)
            if result["deleted"]: # abonent was connected and after this disconnected
                confid = result["confid"]
                request = AddCallsHistoryEventRequest(direction == "OUTGOING", abonent_uri, confid, False)
                calls_history.add_event(request)
                if confid != 0:
                    on_event_tools.clear_abonent_address_in_addrbook(abonent_uri,conn_id,confid,direction == "OUTGOING")
                    # OLD: onlinelayouttools.unregister_unused_tmp_layouts(confid)
                    if not is_hangup_all(confid):  # if hangup all, dont reconfigure layouts for hangupped abonents
                        on_event_tools.reconfigure_all_layouts2(confid, 0, abonent_uri, result["abid"], conn_id)
                        # on_event_tools.fill_empty_layouts(confid) # if layout is empty, fill it by its subscribers
            else: # abonent was not in established connection
                if res["confid"] != 0: # not connected outgoing call
                    on_event_tools.update_abonent_address_in_addrbook(abonent_uri, conn_id, res["confid"],
                                                                      direction == "OUTGOING")
                    on_event_tools.clear_abonent_address_in_addrbook(abonent_uri, conn_id, res["confid"],
                                                                     direction == "OUTGOING")

        if is_max_abonents_connected(conn_id):
            logging.info("too many abonents: hangup new abonent " + abonent_uri)
            hangup_call(abonent_uri)
            # historytools.add_event_to_history(0, abonent_uri, outgoing, strings.HIST_EVENT_HANGUP_BY_COUNT)
            return (1)

        if status != "ESTABLISHED":
            return (1)

        if layout != "0" and layout != "1":  # process only first message "call" with layout==0 and message with default layout=1
            return (1)

        # TMP: TO DEBUG
        t1 = time.time()

        abid = 0
        confid = 0
        if direction == "INCOMING":
            if layout == "0" and confid_in == "0":
                if pin == "":
                    abid = addrbook.get_abonent_id(abonent_uri)
                    confid_list = get_confid_list_by_abonent(abid)

                    if abid != 0 and len(confid_list) == 1 and confid_list[0] != 0:
                        confid = confid_list[0]
                        update_confabonents_list(confid, abid, abonent_uri, conn_id)
                        logging.info("abonents list updated")
                    elif len(confid_list) > 1:
                        nettools.exec_cmd_list_s(["PIN REQUEST " + abonent_uri])
                        logging.info("%d conferences found (> 1): send PIN REQUEST" % (len(confid_list)))
                        return (1)
                    else:
                        if exist_active_conf_with_pin():
                            nettools.exec_cmd_list_s(["PIN REQUEST " + abonent_uri])
                            # historytools.add_event_to_history(0, abonent_uri, 0, strings.HIST_EVENT_CONN_TO_PIN)
                            logging.info("send PIN REQUEST")
                            return (1)
                        else:
                            logging.info("hangup " + abonent_uri + " (no active conference for abonent)")
                            hangup_call(abonent_uri)
                            return (1)
                else:
                    logging.info("entered PIN: " + pin)

                    pin_num = tools.str_to_int(pin)
                    abid = addrbook.get_abonent_id(abonent_uri)
                    confid_list = get_confid_list_by_abonent(abid)  # find conference with id=pin
                    if pin_num in confid_list:
                        confid = pin_num
                        logging.info("interpret pin as conference id, confid=%d" % (confid))

                    if confid == 0:  # find conference with pin "pin"
                        confid = get_confid_by_pin(pin)
                        if confid != 0:
                            logging.info("conference %d with pin '%s' found" % (confid, pin))

                    if confid != 0:
                        abid = addrbook.get_abonent_id(abonent_uri)
                        update_confabonents_list(confid, abid, abonent_uri, conn_id)
                        # historytools.add_event_to_history(confid, abonent_uri, 0, strings.HIST_EVENT_CORRECT_PIN)
                    else:
                        # hangup_call(abonent_uri)
                        nettools.exec_cmd_list_s(["PIN REQUEST " + abonent_uri])
                        # historytools.add_event_to_history(0, abonent_uri, 0, strings.HIST_EVENT_INCORRECT_PIN)
                        logging.info("send PIN REQUEST again")
                        return (1)
            else:
                prm = get_confab_params(abonent_uri, conn_id)
                abid = prm["abid"]
                confid = prm["confid"]
        else:
            prm = get_confab_params(abonent_uri, conn_id)
            abid = prm["abid"]
            confid = prm["confid"]
            if prm["confid"] == 0:
                res = on_event_tools.get_outgoing_call(abonent_uri)
                if res["confid"] != 0:
                    confid = res["confid"]
                    abid = res["abid"]
                    update_confabonents_list(confid, abid, abonent_uri, conn_id)
                    on_event_tools.del_outgoing_call(confid, abid)

        if confid != 0:
            conn_count = get_connected_to_conf_abonents(confid)
            max_count = conftools.get_max_conf_participants(confid)
            if conn_count > max_count:
                abid = 0
                confid = 0
                logging.info("connected to conf participants > max conf participants, hangup " + abonent_uri)
                hangup_call(abonent_uri)
                on_event_tools.unregister_abonent_in_abook(abid)
                return (1)

        # if abonent is in conference, update its real address
        if confid != 0:
            on_event_tools.update_abonent_address_in_addrbook(abonent_uri, conn_id, confid, direction=="OUTGOING")

        if confid == 0:
            logging.error("cannot find confid for " + abonent_uri)
            return (1)

        # TMP: TO DEBUG
        dt1 = time.time() - t1
        logging.error("GET ABID, CONFID TIME: " + str(dt1))

        confid_in = find_value_by_name(parsed_message, "CONFERENCE")
        if confid_in == "0":

            # TMP: TO DEBUG
            t2 = time.time()

            logging.info("configure abonent (phase 1): confid=" + str(confid) + " , abid=" + str(abid) +
                         " , abonent_uri=" + abonent_uri + " , conn_id=" + str(conn_id))
            on_event_tools.configure_abonent1(confid, abonent_uri, abid, conn_id)
            on_event_tools.unregister_abonent_in_abook(abid)

            request = AddCallsHistoryEventRequest(direction=="OUTGOING", abonent_uri, confid, True)
            calls_history.add_event(request)

            # TMP: TO DEBUG
            dt2 = time.time() - t2
            logging.error("CONFIGURE ABONENT " + abonent_uri + " TIME: " + str(dt2))

        else:
            # TMP: TO DEBUG
            t3 = time.time()

            confstr = nettools.get_url_data_s("conferences.json", True)
            conf_list = onlinetools.extract_conf_list(confstr)

            # check count of connected abonents
            max_loading = onlinetools.is_maximum_loading_ex(g_max_visible, conf_list)
            in_some_lyt = on_event_tools.is_abonent_in_some_layout(confid, abonent_uri, conn_id)
            max_loading = max_loading or (not in_some_lyt)
            if max_loading:
                logging.info("too many abonents: mute " + abonent_uri)

            logging.info("configure abonent (phase 2) and layouts: confid=" + str(confid) + " , abid=" + str(abid) +
                         " , abonent_uri=" + abonent_uri + " , conn_id=" + str(conn_id))

            display_name = onlinetools.get_abonent_display_name(conf_list, conn_id)
            res = on_event_tools.configure_abonent2(confid, abid, abonent_uri, display_name, conn_id, max_loading,
                                                    direction!="INCOMING")
            if res != 1:
                logging.info("ERROR: on_event_tools.configure_abonent2() returns " + str(res))

            if max_loading == 0:
                on_event_tools.reconfigure_all_layouts2(confid, 1, abonent_uri, abid, conn_id)

            # TMP: TO DEBUG
            dt3 = time.time() - t3
            logging.error("CONFIGURE LAYOUT FOR ABONENT " + abonent_uri + " TIME: " + str(dt3))

            # historytools.add_event_to_history(confid, abonent_uri, outgoing, strings.HIST_EVENT_CONN_TO_SERVER)
    return (1)

#!/usr/bin/python3

import logging
import time

import app.core.addrbook as addrbook
import app.core.callabtools as callabtools
import app.core.calls_settings as calls_settings
import app.core.conftools as conftools
import app.core.constants as constants
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.entities as entities
import app.core.layoutbackup as layoutbackup
import app.core.nettools as nettools
# import historytools
import app.core.onlinetools as onlinetools
import app.core.profile_settings as profile_settings
import app.core.tools as tools
import app.core.videorecord as videorecord
import event_processor.ipc.on_event_tools as on_event_tools


def register_addr_book(conf_id):
    conf_audio_codecs = conftools.get_conf_audio_codecs(conf_id)
    conf_video_codecs = conftools.get_conf_video_codecs(conf_id)

    conf_bitrate = conftools.get_conf_bitrate(conf_id)

    ab_list = addrbook.get_conf_abonents(conf_id, True, True)
    cmd_list = []
    for ab in ab_list:
        abid = ab["id"]
        #name = ab["name"]
        sipaddr = ab["sip"]
        h323addr = ab["h323"]
        rtspaddr = ab["rtsp"]
        file = ab["file"]
        loop = ab["loop"]
        email = ab["email"]
        bitrate = ab["bitrate"]
        ab_audio_codecs = ab["audio_codecs"]
        ab_video_codecs = ab["video_codecs"]

        abook_name = tools.abid_to_abook_name(abid)

        bitr_str = str(conf_bitrate)
        if bitrate != 0:
            bitr_str = str(bitrate)
        if bitr_str == "0":
            bitr_str = ""

        cmd_list.append("ABOOK REGISTER "+abook_name+ "," + email + "," + "," + "," + bitr_str)
        if sipaddr != "":
            cmd_list.append("ABOOK URI ADD %s,%s" % (abook_name, sipaddr))
        if h323addr != "":
            cmd_list.append("ABOOK URI ADD %s,%s" % (abook_name, h323addr))
        if rtspaddr != "":
            cmd_list.append("ABOOK URI ADD %s,%s" % (abook_name, rtspaddr))
        if file != "":
            full_fname = addrbook.get_full_videofile_name(file, loop)
            cmd_list.append("ABOOK URI ADD %s,%s" % (abook_name, full_fname))

        masked_audio_codecs = entities.get_masked_audio_codecs(conf_audio_codecs, ab_audio_codecs)
        cmd_list += masked_audio_codecs.get_abonent_cmd_list(abook_name)

        masked_video_codecs = entities.get_masked_video_codecs(conf_video_codecs, ab_video_codecs)
        cmd_list += masked_video_codecs.get_abonent_cmd_list(abook_name)

    nettools.exec_cmd_list_s(cmd_list)


def configure_vm(confid):
    """This function configure specified vile. Note: launch this function one time at starting of system.
    vile_ip   - ip address of vile server
    vile_port - port of httpd of vile server
    vile_lgn  - login of httpd in vile
    vile_pwd  - password of httpd in vile
    abonents  - list of abonent's url
    Rerurn value: 1 at success, 0 at error."""
    confid = int(confid)

    cid = tools.conference_name_by_id(confid)

    config_commands = []

    # configure audio codecs
    conftools.get_audio_codecs_cmd_list(confid, config_commands)

    # configure video codecs
    conftools.get_video_codecs_cmd_list(confid, config_commands)

    configs = dbtools.load_table(
        "legacy.configurations",
        """bitrate, width, height, use_pres, noise_suppressor, auto_gain_ctrl""",
        "id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (confid)
    )
    if len(configs) != 0:
        cid = tools.conference_name_by_id(confid)

        bitrate = configs[0][0]
        width = configs[0][1]
        height = configs[0][2]
        use_pres = tools.int_to_bool(configs[0][3])
        noise_suppressor = configs[0][4]
        auto_gain_ctrl = configs[0][5]

        config_commands.append("SET BITRATE 8192")

        #OLD: config_commands.append("SET VSIZE " + str(configs[0][20]) + " " + str(configs[0][21]))

        conftools.get_presentation_cmd(confid, use_pres, config_commands)

        config_commands.append( data.get_noise_suppressor_conf_cmd(noise_suppressor, cid) )
        config_commands.append( data.get_auto_gain_ctrl_conf_cmd(auto_gain_ctrl, cid) )

        #config_commands.append("SET ACALL REJECT")  # enable audio calls only
        #config_commands.append("SET ACALL HIDE")
        # config_commands.append( "SET ACALL SHOW" )

    config_commands.append("SET CALL AUTOACCEPT " + "ON")  # tools.num_to_onoff(commonconfigs[0][19]) )
    config_commands.append("SET TEXT DN PERMANENT")

    calls_settings.get_calls_commands(config_commands)
    profile_settings.get_video_settings_commands(config_commands)

    nettools.exec_cmd_list_s(config_commands)

    return 1


def call_abonents(conf_id):
    """This function make calls from vile server to its abonents.
    conf_id   - id of conference
    vile_ip   - ip address of vile
    vile_port - port of vile
    vile_lgn  - login for vile web-interface
    vile_pwd  - password for vile web-interface
    Return value: 1 at success, 0 at error"""
    cid = tools.conference_name_by_id(conf_id)

    connstr, confstr = onlinetools.get_online_state()
    real_ab_list = onlinetools.extract_conn_list(connstr)

    cmd = ""
    cmd_list = []
    ab_list = addrbook.get_conf_abonents(conf_id, True, True)
    for ab in ab_list:
        abid = ab["id"]
        abname = ab["name"]
        sipaddr = ab["sip"]
        h323addr = ab["h323"]
        extaddr = ab["rtsp"]
        if len(ab["file"]) != 0:
            full_fname = addrbook.get_full_videofile_name(ab["file"], ab["loop"])
        else:
            full_fname = ""
        ab_bitrate = ab["bitrate"]

        abook_name = tools.abid_to_abook_name(abid)

        # if abonent already online, don't call it
        online_ab_found = False
        for conn in real_ab_list:
            if tools.is_addr_group_equivalent(conn["contact_uri"], sipaddr, h323addr, extaddr, full_fname, True):
                online_ab_found = True
                logging.error("abonent " + conn["contact_uri"] + " is already connected: don't call it")
                break
        if online_ab_found:
            continue

        cmd = "CONFERENCE DIAL %s %s" % (cid, abook_name)
        cmd_list.append(cmd)

        # store calling abonent in buffer
        callabtools.set_outgoing_call(conf_id, abid, sipaddr, h323addr, extaddr, full_fname)

    nettools.exec_cmd_list_s(cmd_list)


def init_state_controller():
    # turn on sending of text messages inband
    cmd_list = []
    cmd_list.append("SET TEXT TIMEOUT 20")
    cmd_list.append("SET BACKGROUND ON")

    calls_settings.get_calls_commands(cmd_list)
    calls_settings.get_sip_commands(cmd_list)
    calls_settings.get_h323_commands(cmd_list)
    profile_settings.get_rtp_commands(cmd_list)

    nettools.exec_cmd_list_s(cmd_list)


def autocall_abonents():
    """This function check all abonents of all active conferences with counter of calls 'autocall'>0.
    If abonent is disconnected, then call abonent.
    connstr - string with connections list from connections.json"""

    # get online state from buffer (in database)
    online_data_rows = dbtools.load_table("legacy.onlinedata", "connections", "")
    if len(online_data_rows) != 0:
        connstr = online_data_rows[0][0]
    else:
        connstr = '{ "connection_list": [] }'

    conn_json = onlinetools.extract_conn_list(connstr)

    # build dictionary of abonents
    ab_dict = addrbook.get_all_abonent_names_dict()

    processed_ab = {}

    # get conferences with activated autocall option
    conf_rows = dbtools.select(
        """SELECT id, autocall, callcount FROM legacy.conferences WHERE state=%d and autocall<>0 and callcount>0 
        AND deleted_at is null""" % (data.CONF_STATE_ACTIVE))
    for conf in conf_rows:
        confid = conf[0]
        autocall = conf[1]
        callcount = conf[2]

        conf_processed = 0

        # get list of abonents from active conference
        ab_rows = dbtools.select("SELECT DISTINCT abid FROM legacy.confabonents WHERE confid=%d" % (confid))
        for ab in ab_rows:
            abid = ab[0]
            if abid not in ab_dict:
                continue

            # find abonent in connections.json
            ab_rec = ab_dict[abid]
            sipaddr = ab_rec["sipaddr"]
            h323addr = ab_rec["h323addr"]
            extaddr = ab_rec["extaddr"]
            file = ab_rec["file"]
            found = 0
            for conn in conn_json:
                conn_uri = conn["contact_uri"]
                if tools.is_addr_group_equivalent(conn_uri, sipaddr, h323addr, extaddr, file, True):
                    found = 1
                    break

            # find in processed list
            if abid in processed_ab:
                found = 1

            # call abonent
            if found == 0:
                callabtools.make_outgoing_call(confid, abid)
                processed_ab[abid] = 1
                conf_processed = 1
                # logging.info("autocall_abonents(): autocall abonent " + ab_rec["name"])

        # decrement counter of calls
        if conf_processed != 0:
            dbtools.make_sql_request("UPDATE legacy.conferences SET callcount=%d WHERE id=%d" % (callcount - 1, confid))


def update_online_state():
    """This function update in database state of conferences and its abonents."""
    nettools.update_online_state()


def update_start_time_params(confid, infinity, repeate_count, b_auto_start):
    """This function update in database start time of conference 'confid' if 'infinity' is nonzero.
    confid   - id of conference
    infinity - flag (1 - conference start manually, 0 - conference start by timer)"""
    confid = int(confid)
    infinity = int(infinity)
    repeate_count = int(repeate_count)

    cur_time_str = tools.json_date_time_to_db(time.time())

    if b_auto_start:
        new_repeate_count = repeate_count - 1
    else:
        new_repeate_count = repeate_count

    if new_repeate_count < 0:
        new_repeate_count = 0

    dbtools.make_sql_request("UPDATE legacy.conferences SET last_start_time='%s', repeate_count=%d WHERE id=%d" %
                             (cur_time_str, new_repeate_count, confid))


def update_state():
    """This function update state of each conference."""
    time_range_dict = conftools.get_all_time_ranges()

    rows_list = dbtools.select("""SELECT id, name, starttime, endtime, state, infinity, autocall, last_start_time,        
        start_mode, start_week_day, start_month_day, start_wk_first, start_wk_last,
        repeate_mode, repeate_count, repeate_last_time,
        stop_time_table_mode, stop_time_table_begin, stop_time_table_end,
        stop_mode, stop_duration, stop_abs_time 
        FROM legacy.conferences WHERE deleted_at is null""")
    for row in rows_list:
        id = row[0]
        name = row[1]
        startsec = tools.db_date_time_to_json(str(row[2]))
        endsec = tools.db_date_time_to_json(str(row[3]))
        state = row[4]
        infinity = row[5]
        autocall = row[6]
        last_start_time = tools.db_date_time_to_json(str(row[7]))

        auto_start_mode = row[8]
        auto_start_week_day = row[9]
        auto_start_month_day = row[10]
        start_week_first = row[11]
        start_week_last = row[12]

        repeate_mode = row[13]
        repeate_count = row[14]
        repeate_last_time = tools.db_date_time_to_json(str(row[15]))

        stop_time_table_mode = row[16]
        stop_time_table_begin = tools.db_date_time_to_json(str(row[17]))
        stop_time_table_end = tools.db_date_time_to_json(str(row[18]))

        stop_mode = row[19]
        stop_duration = row[20]
        stop_abs_time = tools.db_date_time_to_json(str(row[21]))

        time_range_list = []
        if id in time_range_dict:
            time_range_list = time_range_dict[id]

        curtime = int(time.time())

        # auto start params:
        b_common_auto_start = conftools.is_auto_start_time_range(curtime, time_range_list, auto_start_mode)
        b_auto_start_periodically = conftools.is_auto_start_period(curtime, auto_start_mode, auto_start_week_day,
                                                                   auto_start_month_day, start_week_first,
                                                                   start_week_last)
        #b_hour_minute = conftools.is_auto_start_hour_minute(curtime, time_range_list)
        b_repeate_auto_start = conftools.is_repeate_auto_start(curtime, repeate_mode, repeate_count, repeate_last_time)
        b_time_table_stopped = conftools.is_time_table_stopped(curtime, stop_time_table_mode, stop_time_table_begin,
                                                               stop_time_table_end)

        b_auto_start = b_common_auto_start and \
            (auto_start_mode == data.AUTO_START_MODE_ONCE or
            (b_auto_start_periodically and b_repeate_auto_start)) and \
            (not b_time_table_stopped)

        # auto stop params
        b_stop_hour_minute = auto_start_mode != data.AUTO_START_MODE_OFF and \
                             auto_start_mode != data.AUTO_START_MODE_ONCE and \
                             conftools.is_auto_stop_hour_minute(curtime, time_range_list)

        b_auto_stop = conftools.is_auto_stop(curtime, last_start_time, stop_mode, stop_duration, stop_abs_time) or \
                      (auto_start_mode == data.AUTO_START_MODE_ONCE and
                       conftools.is_end_of_time_range(curtime, time_range_list)) or \
                      b_time_table_stopped or b_stop_hour_minute

        b_idle_state = state == data.CONF_STATE_PRE_INACTIVE or state == data.CONF_STATE_POST_INACTIVE
        if state == data.CONF_STATE_START or (b_idle_state and b_auto_start):
            logging.info("infinity=" + str(infinity))
            logging.info("b_common_auto_start=" + str(b_common_auto_start))
            logging.info("auto_start_mode=%d" % (auto_start_mode))
            logging.info("b_auto_start_periodically=" + str(b_auto_start_periodically))
            #logging.info("b_hour_minute=" + str(b_hour_minute))
            logging.info("b_repeate_auto_start=" + str(b_repeate_auto_start))
            logging.info("b_time_table_stopped=" + str(b_time_table_stopped))

            # TMP: TO DEBUG
            t1 = time.time()

            logging.error("update_state(): START CONFERENCE")
            if autocall != 0:
                dbtools.make_sql_request(
                    "UPDATE legacy.conferences SET callcount=%d WHERE id=%d" % (constants.MAX_AUTOCALLS, id))

            on_event_tools.register_conference(id)
            res = on_event_tools.register_layouts(id, 1)
            if res != 1:
                logging.error("ERROR: on_event_tools.register_layouts() return " + str(res))

            layoutbackup.delete_all_layout_backups(id)
            layoutbackup.create_all_layout_backups(id)
            on_event_tools.configure_aspeaker(id)
            on_event_tools.set_layouts_restrict(id)

            update_start_time_params(id, infinity, repeate_count, b_auto_start)
            configure_vm(id)
            register_addr_book(id)
            call_abonents(id)
            # NOTE: auto start record then participant is coming to recording layout
            # if videorecord.is_record_at_start_conf(id):
            #    videorecord.switch_video_record2(id, True)
            dbtools.make_sql_request(
                "UPDATE legacy.conferences SET state=" + str(data.CONF_STATE_ACTIVE) + " WHERE id=" + str(id))

            # TMP: TO DEBUG
            dt = time.time() - t1
            logging.error("START CONF COMMANDS DT: " + str(dt))

            # TODO: historytools.add_event_to_history(id, "", 1, data.HIST_EVENT_START_CONF)

        if state == data.CONF_STATE_CLOSING_VILE or (state == data.CONF_STATE_ACTIVE and b_auto_stop):
            logging.info("infinity=" + str(infinity))
            logging.info("curtime > endsec=" + str(curtime > endsec))
            logging.info("b_time_table_stopped=" + str(b_time_table_stopped))
            logging.info("b_stop_hour_minute=" + str(b_stop_hour_minute))

            logging.error("update_state(): STOP CONFERENCE")
            videorecord.switch_video_record2(id, False)
            on_event_tools.deconfigure_all_abonents(id, 1)
            on_event_tools.unregister_layouts(id)
            layoutbackup.delete_all_layout_backups(id)
            on_event_tools.deconfigure_conference(id)
            # on_event_tools.del_all_disconnected_abonents(id)
            dbtools.make_sql_request(
                "UPDATE legacy.conferences SET state=%d, fst_ab='' WHERE id=%d" % (data.CONF_STATE_POST_INACTIVE, id))

            # TODO: historytools.add_event_to_history(id, "", 1, data.HIST_EVENT_STOP_CONF)

            # if all conferences are stopped, clear abook/history in vtsrv engine
            started_rows = dbtools.select("SELECT count(*) FROM legacy.conferences WHERE state<>%d AND state<>%d" % (
                data.CONF_STATE_PRE_INACTIVE, data.CONF_STATE_POST_INACTIVE))
            if len(started_rows) != 0:
                started_conf_cnt = started_rows[0][0]
                if started_conf_cnt == 0:
                    cmd_list = []
                    cmd_list.append("ABOOK CLEAR")
                    cmd_list.append("HISTORY FORGET")
                    nettools.exec_cmd_list_s(cmd_list)

    update_online_state()

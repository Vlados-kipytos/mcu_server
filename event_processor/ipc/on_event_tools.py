#!/usr/bin/python3

import logging
import sys

import app.core.addrbook as addrbook
import app.core.constants as constants
import app.core.customlayouttypes as customlayouttypes
import app.core.data as data
import app.core.dbtools as dbtools
import app.core.layoutbackup as layoutbackup
import app.core.layoutcfgtools as layoutcfgtools
import app.core.layoutcrown as layoutcrown
import app.core.nettools as nettools
import app.core.onlinelayouttools as onlinelayouttools
import app.core.onlinetools as onlinetools
import app.core.sysinfotools as sysinfotools
import app.core.tools as tools
from app.core.requests import CreateContactRequest
from app.core.requests import DeleteCrownRequest
from app.core.entities import AudioCodecsEntity
from app.core.entities import VideoCodecsEntity


def configure_abonent1(confid, abonent_uri, abid, conn_id):
    """This function configure abonent 'abid' in conference 'confid' (in video engine)."""
    confid = int(confid)
    abid = int(abid)
    conn_id = int(conn_id)

    cid = tools.conference_name_by_id(confid)

    conn_id_str = "#%d" % (conn_id)

    cmd_list = []
    cmd = "CONFERENCE ADD " + cid + " " + conn_id_str
    cmd_list.append(cmd)

    nettools.exec_cmd_list_s(cmd_list)
    return (1)


def configure_abonent2(confid, abid, abonent_uri, abonent_name, conn_id, force_mute, b_outgoing):
    """This function configure abonent 'abid' in conference 'confid' (in video engine)."""
    confid = int(confid)
    abid = int(abid)
    conn_id = int(conn_id)
    force_mute = int(force_mute)

    # don't configure presentation (it has not abonents flags)
    if tools.is_presentation_file(abonent_uri):
        return (1)

    cid = tools.conference_name_by_id(confid)

    conn_id_str = "#%d" % (conn_id)

    cmd_list = []

    # get flags: "render participants fully", mute audio/video to all participants
    render_fully_conf = data.get_render_participant_name(data.RENDER_PARTICIPANT_AUTO)
    muteaudio = False
    mutevideo = False
    cfg_rows = dbtools.load_table("legacy.configurations", "render_fully, muteaudio, mutevideo",
                                  "id IN (SELECT configid FROM legacy.conferences WHERE id=%d)" % (confid))
    if len(cfg_rows) != 0:
        render_fully_conf = data.get_render_participant_name(cfg_rows[0][0])
        muteaudio = tools.int_to_bool(cfg_rows[0][1])
        mutevideo = tools.int_to_bool(cfg_rows[0][2])

    # set display name (if exist)
    abonent_display_name = ""
    if b_outgoing:
        name_rows = dbtools.select("SELECT name, dispname FROM legacy.abonents WHERE id=%d" % (abid))
        if len(name_rows) != 0:
            name = name_rows[0][0]
            dispname = name_rows[0][1]
            if dispname != "" and dispname != None:
                abonent_display_name = dispname.strip()
            elif name != "" and name != None:
                abonent_display_name = name.strip()
    else:
        abonent_display_name = tools.delete_protocol_and_port(abonent_name)
        abonent_display_name = abonent_display_name.strip()
    if len(abonent_display_name) != 0:
        cmd_list.append("RENAME PEER " + conn_id_str + " " + abonent_display_name)

    # get layout id
    layout_id = layoutbackup.get_layout_id_by_owner(confid, abid)

    b_rtsp = tools.is_rtsp_prefix(abonent_uri)

    # get show priority, useaudio, usevideo flags for abonent 'abonent_uri'
    flags_rows = dbtools.select("""SELECT useaudio, usevideo, sndvol, useaudio_tx, sndvol_tx, usevideo_tx, render_mode, 
        noise_suppressor, auto_gain_ctrl FROM legacy.confabonents WHERE confid=%d and abid=%d""" % (confid, abid))
    if len(flags_rows) == 0:
        useaudio = 1
        usevideo = 1
        sndvol = 0
        useaudio_tx = 1
        sndvol_tx = 0
        usevideo_tx = 1
        render_fully_ab = data.get_render_participant_name(data.RENDER_PARTICIPANT_AUTO)
        noise_suppressor = data.NOISE_SUPPRESSOR_DEFAULT
        auto_gain_ctrl = data.AUTO_GAIN_CTRL_DEFAULT
        logging.info("configure_abonent2(): use default flags")
    else:
        useaudio = flags_rows[0][0]
        usevideo = flags_rows[0][1]
        sndvol = flags_rows[0][2]
        useaudio_tx = flags_rows[0][3]
        sndvol_tx = flags_rows[0][4]
        usevideo_tx = flags_rows[0][5]
        render_fully_ab = data.get_render_participant_name(flags_rows[0][6])
        noise_suppressor = flags_rows[0][7]
        auto_gain_ctrl = flags_rows[0][8]

    if render_fully_conf == "AUTO":
        cmd_list.append("SET RENDER " + render_fully_ab + " " + conn_id_str)
    else:
        cmd_list.append("SET RENDER " + render_fully_conf + " " + conn_id_str)

    # don't send video to rtsp-camera
    if b_rtsp:
        usevideo_tx = 0

    if layout_id == 0:
        layout_name = "DEFAULT"
    else:
        layout_name = tools.get_layout_name_by_id(confid, layout_id)

    # show layout to all abonents exclude rtsp-camera
    if not b_rtsp:
        cmd = "LAYOUT SHOW " + cid + " " + layout_name + " " + conn_id_str
        cmd_list.append(cmd)

    cmd1 = "SET MUTE %s RX AUDIO %s" % (tools.num_to_onoff((not useaudio) or muteaudio), conn_id_str)
    cmd2 = "SET MUTE %s TX AUDIO %s" % (tools.num_to_onoff(not useaudio_tx), conn_id_str)
    # if force_mute:
    #    cmd1 = "SET MUTE ON RX AUDIO " + abonent_uri
    #    cmd2 = "SET MUTE ON TX AUDIO " + abonent_uri
    cmd_list.append(cmd1)
    cmd_list.append(cmd2)

    cmd_list.append("SET MUTE %s TX VIDEO %s" % (tools.num_to_onoff(not usevideo_tx), conn_id_str))
    if force_mute or mutevideo or (not usevideo):
        cmd_list.append("SET MUTE ON RX VIDEO " + conn_id_str)

    cmd_list.append("SET GAIN %s %d RX" % (conn_id_str, sndvol))
    cmd_list.append("SET GAIN %s %d TX" % (conn_id_str, sndvol_tx))

    cmd_list.append( data.get_noise_suppressor_abonent_cmd(noise_suppressor, conn_id) )
    cmd_list.append( data.get_auto_gain_ctrl_abonent_cmd(auto_gain_ctrl, conn_id) )

    nettools.exec_cmd_list_s(cmd_list)
    return (1)


def stop_all_conferences():
    """This function hangups all connected abonents, unregister all active conferences and set state of conferences to inactive.
    If some abonents hangupped or some conferences stopped, return 1 else return 0."""

    connstr = nettools.get_url_data_s("connections.json", True)

    conn_json = onlinetools.extract_conn_list(connstr)

    cmd_list = []

    # stop all calls
    for conn in conn_json:
        uri = conn["contact_uri"]
        cmd_list.append("HANGUP PEER " + uri)

    # unregister all conferences
    conf_rows = dbtools.select("SELECT id FROM legacy.conferences WHERE state=%d AND deleted_at is null" %
                               (data.CONF_STATE_ACTIVE))
    for conf in conf_rows:
        confid = conf[0]
        cid = tools.conference_name_by_id(confid)
        cmd_list.append("REC STOP " + cid)
        cmd_list.append("CONFERENCE UNREGISTER " + cid)

    nettools.exec_cmd_list_s(cmd_list)

    # update conferences state
    dbtools.make_sql_request("UPDATE legacy.conferences SET state=%d WHERE state=%d" % (
        data.CONF_STATE_POST_INACTIVE, data.CONF_STATE_ACTIVE))

    if len(conn_json) != 0 or len(conf_rows) != 0:
        return (1)
    else:
        return (0)


def deconfigure_all_abonents(confid, fast):
    """This function hangup all abonents of conference 'confid'."""
    confid = int(confid)

    connstr = nettools.get_url_data_s("connections.json", True)
    confstr = nettools.get_url_data_s("conferences.json", True)

    conf_json = onlinetools.extract_conference(confstr, confid)

    conn_list = onlinetools.extract_abonents_list(conf_json, 1, connstr)

    # hangup abonents in conference confid
    cmd_list = []
    used_conn_id_list = []
    for cur_ab in conn_list:
        cur_uri = cur_ab["uri"]
        cur_conn_id = cur_ab["id"]
        cur_conn_id_str = "#%d" % (cur_conn_id)
        cmd_list.append("HANGUP PEER " + cur_conn_id_str)
        used_conn_id_list.append(cur_conn_id)

    # get abonents not in conference confid (they try to connect to conference confid)
    real_ab_list = onlinetools.extract_conn_list(connstr)

    # get list of "static" abonents of conference from DB
    ab_rows = addrbook.get_conf_abonents(confid, False, False)
    for ab in ab_rows:
        real_ab_uri = ""
        conn_id_str = ""
        for real_ab in real_ab_list:
            cur_uri = real_ab["contact_uri"]
            cur_conn_id = real_ab["id"]
            if cur_conn_id in used_conn_id_list:
                continue
            cur_conn_id_str = "#%d" % (cur_conn_id)
            if tools.is_addr_group_equivalent(cur_uri, ab["sip"], ab["h323"], ab["rtsp"], ab["file"]):
                real_ab_uri = cur_uri
                conn_id_str = cur_conn_id_str
                break
        if conn_id_str != "":
            cmd_list.append("HANGUP PEER " + conn_id_str)

    nettools.exec_cmd_list_s(cmd_list)

    # clean buffer of abonent of stopped conference
    dbtools.make_sql_request("DELETE FROM legacy.outgoingcalls WHERE confid=%d" % (confid))


def unregister_layouts(confid):
    """ This function unregister all layouts of conference 'confid'. """

    connstr, confstr = onlinetools.get_online_state()

    conf_json = onlinetools.extract_conference(confstr, confid)
    if conf_json == {}:
        return  # error
    if "layout_list" not in conf_json:
        return  # error

    cid = tools.conference_name_by_id(confid)

    cmd_list = []
    for layout in conf_json["layout_list"]:
        cmd_list.append("LAYOUT UNREGISTER " + cid + " " + layout["name"])

    nettools.exec_cmd_list_s(cmd_list)


def register_conference(confid):
    """This function registers conference 'confid' in video engne."""
    cid = tools.conference_name_by_id(confid)
    cmd = "CONFERENCE REGISTER " + cid
    nettools.exec_cmd_list_s([cmd])


def configure_aspeaker(confid):
    """This function set parameter 'active speaker' for conference 'confid'."""
    confid = int(confid)

    aspeaker = layoutcfgtools.get_aspeaker_flag(confid)
    active_slot = layoutbackup.is_active_slot_in_conf(confid)
    cmd = layoutcfgtools.aspeaker_cmd(confid, aspeaker or active_slot)

    cmd_list = [cmd]
    nettools.exec_cmd_list_s(cmd_list)


def deconfigure_conference(confid):
    """This function unregisters conference 'confid' in video engne (delete all layouts, hangup all calls)."""
    cid = tools.conference_name_by_id(confid)
    cmd = "CONFERENCE UNREGISTER " + cid
    nettools.exec_cmd_list_s([cmd])


def set_layouts_restrict(confid):
    """This function set limit to count of showed abonents (for active speaker mode) or disable limit (for common mode)."""
    confid = int(confid)

    cid = tools.conference_name_by_id(confid)

    # read "active speaker" flag
    # aspeaker = layoutcfgtools.get_aspeaker_flag(confid)
    aspeaker, asp2lyt = layoutcfgtools.get_aspeaker_2lyt_flags(confid)

    cmd_list = []

    if aspeaker:
        # asp2lyt = layoutcfgtools.get_asp2lyt_flag(confid)
        glb_lyt_id = layoutcfgtools.get_global_layout_id(confid, False)
        dictors_rows = dbtools.select("SELECT id, dictors_cnt FROM legacy.layoutsprmbackup WHERE id=%d" % (glb_lyt_id))
        if len(dictors_rows) != 0:
            layout_id = dictors_rows[0][0]
            dictors_cnt = dictors_rows[0][1]
            lid = tools.get_layout_name_by_id(confid, layout_id)
            if asp2lyt:  # check active speaker special mode: "2 layouts: all see speaker, speaker see all"
                cmd_list.append("LAYOUT RESTRICT " + cid + " " + lid + " 1")
                cmd_list.append("LAYOUT RESTRICT " + cid + " SPEAKER 64")
                cmd_list.append("LAYOUT SPECIAL ON " + cid + " SPEAKER")
            else:
                cmd_list.append("LAYOUT RESTRICT " + cid + " " + lid + " " + str(dictors_cnt))
                cmd_list.append("LAYOUT MAP INIT " + cid + " " + lid)
                cmd_list.append("LAYOUT MAP ACTIVE " + cid + " " + lid + " 1")
                cmd_list.append("LAYOUT MAP APPLY " + cid + " " + lid)
    else:
        lyt_id_list = layoutbackup.get_layouts_id_of_conference(confid)
        for layout_id in lyt_id_list:
            lid = tools.get_layout_name_by_id(confid, layout_id)

            planned_layout = layoutbackup.get_backup_of_layout(confid, layout_id)

            # check "auto" flag
            auto = 0
            layout_type = data.LAYOUT_TYPE_GLOBAL
            auto_rows = dbtools.select("SELECT auto,layout_type FROM legacy.layoutsprmbackup WHERE id=%d" % (layout_id))
            if len(auto_rows) != 0:
                auto = auto_rows[0][0]
                layout_type = auto_rows[0][1]

            if auto:
                cells_restrict = sysinfotools.get_abonents_restriction()["max_visible"]
            else:
                cells_restrict = planned_layout.cells_count
            cmd_list.append("LAYOUT RESTRICT " + cid + " " + lid + " " + str(cells_restrict))

            if not auto:
                cmd_list.append("LAYOUT MAP INIT " + cid + " " + lid)
                for ab_pos in planned_layout.wndlist:  # set roles of layout
                    if ab_pos.role_id != layoutcfgtools.DEFAULT_FILLED_ROLE_ID:
                        cmd_list.append(layoutcfgtools.role_cmd(ab_pos.role_id, cid, lid, ab_pos.wndnum, ""))
                cmd_list.append("LAYOUT MAP APPLY " + cid + " " + lid)

            if layout_type == data.LAYOUT_TYPE_ASPEAKER:
                layoutcfgtools.set_special_layout(cid, lid, True, cmd_list)

    nettools.exec_cmd_list_s(cmd_list)


def register_layouts(confid, fast):
    """This function registers all layouts of conference 'confid'."""
    confid = int(confid)

    glb_lyt_id = layoutcfgtools.get_global_layout_id(confid, False)

    aspeaker, asp2lyt = layoutcfgtools.get_aspeaker_2lyt_flags(confid)

    # if global layout is not exist, create it in database
    if glb_lyt_id == 0:
        # aspeaker = tools.int_to_bool(layoutcfgtools.get_aspeaker_flag(confid))
        b_aspeaker = tools.int_to_bool(aspeaker)
        glb_lyt = layoutcfgtools.create_default_global_layout(confid, b_aspeaker, False)
        glb_lyt_id = glb_lyt.layoutid
        if glb_lyt_id == 0:
            return (0)  # error

    # all layouts of conference 'confid'
    cmd_list = []
    layout_prm_rows = dbtools.load_table("legacy.layouts", "id, auto",
                                         "id in (SELECT id FROM legacy.layoutconferences WHERE confid=%d)" % confid)
    for layout_prm in layout_prm_rows:
        layout_id = layout_prm[0]
        auto = tools.int_to_bool(layout_prm[1])
        lid = tools.get_layout_name_by_id(confid, layout_id)
        layoutcfgtools.register_online_layout(confid, lid, auto, cmd_list)

    # check active speaker special mode: "2 layouts: all see speaker, speaker see all"
    # aspeaker = layoutcfgtools.get_aspeaker_flag(confid)
    # asp2lyt = layoutcfgtools.get_asp2lyt_flag(confid)
    if aspeaker and asp2lyt:
        layoutcfgtools.register_online_layout(confid, "SPEAKER", False, cmd_list)

    if len(cmd_list) == 0:
        return (0)  # error

    # register layouts
    nettools.exec_cmd_list_s(cmd_list)
    return (1)


def get_glb_lyt_priority(layout_id, abid):
    """This function get priority of abonent 'abid' in gloabal layout 'layout_id'."""

    layout_id = int(layout_id)
    abid = int(abid)

    prior_rows = dbtools.select(
        "SELECT priority FROM legacy.layoutconfigs WHERE layout_id=%d AND abid=%d AND abid<>0" % (layout_id, abid))
    if len(prior_rows) != 0:
        priority = prior_rows[0][0]
    else:
        priority = data.PRIORITY_NORMAL  # error: get default priority

    return (priority)


def reconfigure_all_layouts2(confid, badd, last_ab_uri, last_ab_id, conn_id):
    """This function reconfigure all layouts at add/delete abonent to/from conference.
    confid      - id of conference
    badd        - flag (1 - last abonent last_ab_uri is added, 0 - last abonent last_ab_uri is deleted)
    last_ab_uri - last abonent added/deleted to/from conference
    last_ab_id  - id of last abonent"""
    confid = int(confid)
    cid = tools.conference_name_by_id(confid)

    last_ab_id = int(last_ab_id)
    conn_id = int(conn_id)

    if conn_id  != -1:
        conn_id_str = "#%d" % (conn_id)
    else:
        conn_id_str = last_ab_uri #TMP

    # logging.debug("reconfigure_all_layouts2(): badd= " + str(badd) + " , last_ab_uri=" + last_ab_uri + " , last_ab_id=" + str(last_ab_id))

    cmd_list = []

    # aspeaker = layoutcfgtools.get_aspeaker_flag(confid)
    # asp2lyt = layoutcfgtools.get_asp2lyt_flag(confid)
    aspeaker, asp2lyt = layoutcfgtools.get_aspeaker_2lyt_flags(confid)

    # update crown layout (if it is used)
    try:
        crown_layout = onlinetools.get_crown_layout(confid, {})
        if crown_layout != {}:
            confstr = nettools.get_url_data_s("conferences.json", True)
            conf_json = onlinetools.extract_conference(confstr, confid)

            actual_crown_layout = onlinetools.get_crown_layout(confid, conf_json)
            crown_ab_id = onlinetools.get_crown_participant_from_layout(actual_crown_layout)
            if (crown_ab_id == 0 or crown_ab_id == last_ab_id) and (not badd):
                request = DeleteCrownRequest(confid)
                layoutcrown.delete_crown_from_participant(request, conf_json)
            else:
                layoutcrown.update_crown_layout(confid, actual_crown_layout, last_ab_uri, conn_id, badd)
    except:
        logging.error("reconfigure_all_layouts2(): exception " + str(sys.exc_info()))

    custom_lyt_types = customlayouttypes.get_all_custom_layout_types()

    owner_layout_id = layoutbackup.get_individual_layout_id(confid, last_ab_id, True)
    owner_layout_id2 = layoutbackup.get_individual_layout_id(confid, last_ab_id, False)

    # cofigure "auto" layouts
    auto_layouts_rows = dbtools.select("""SELECT layoutconfbackup.id, layoutconfbackup.abid 
                                           FROM legacy.layoutconfbackup, legacy.layoutsprmbackup 
                                           WHERE layoutconfbackup.id=layoutsprmbackup.id AND layoutsprmbackup.auto<>0 and layoutconfbackup.confid=%d""" % (
        confid))
    for lyt in auto_layouts_rows:
        layout_id = lyt[0]
        lyt_abid = lyt[1]
        lid = tools.get_layout_name_by_id(confid, layout_id)

        # don't attach owner to it's layout in auto layouts
        if last_ab_id != 0 and (layout_id == owner_layout_id or layout_id == owner_layout_id2):
            continue

        if badd:
            cmd_list.append("LAYOUT ATTACH " + cid + " " + lid + " " + conn_id_str)
            if aspeaker:
                priorval_str = data.get_priority_value(data.PRIORITY_NORMAL)  # get default priority
                cmd_list.append("SET MUTE PRIORITY " + priorval_str + " " + conn_id_str)
            if aspeaker and asp2lyt:
                cmd_list.append("LAYOUT ATTACH " + cid + " SPEAKER " + conn_id_str)
        else:
            cmd_list.append("LAYOUT DETACH " + cid + " " + lid + " " + conn_id_str)

    # configure layouts with concrete abonents
    layouts_with_fixed_peer = {}

    # get slots with concrete peers (not auto layouts)
    ab_num_rows = dbtools.select("""SELECT layoutbackup.num_ab, layoutbackup.type, layoutbackup.priority,
                                    layoutbackup.wnd_num, layoutbackup.role, layoutbackup.layout_id, 
                                    layoutbackup.render_mode 
                                    FROM legacy.layoutbackup, legacy.layoutsprmbackup 
                                    WHERE layoutbackup.layout_id=layoutsprmbackup.id AND layoutsprmbackup.auto=0 AND 
                                    layoutbackup.layout_id IN (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d)
                                    AND layoutbackup.abid=%d AND layoutbackup.abid<>0""" % (confid, last_ab_id))
    for ab in ab_num_rows:
        num_ab = ab[0]
        ltype = ab[1]
        priority = ab[2]
        wnd_num = ab[3]
        role = ab[4]
        layout_id = ab[5]
        render_mode_ind = ab[6]

        layouts_with_fixed_peer[layout_id] = True

        lid = tools.get_layout_name_by_id(confid, layout_id)

        layout_mode_cmd = layoutcfgtools.get_layout_cmd_ex(ltype, cid, lid, custom_lyt_types)

        if badd:
            cmd_list.append("LAYOUT ATTACH " + cid + " " + lid + " " + conn_id_str)
            if aspeaker:
                pval_str = data.get_priority_value(priority)
                cmd_list.append("SET MUTE PRIORITY " + pval_str + " " + conn_id_str)
                if asp2lyt:
                    cmd_list.append("LAYOUT ATTACH " + cid + " SPEAKER " + conn_id_str)
                if layout_mode_cmd.find("EQUAL") == -1:
                    cmd_list.append(layout_mode_cmd)
            else:
                cmd_list.append(layoutcfgtools.role_cmd(role, cid, lid, wnd_num, conn_id_str))
                cmd_list.append(layout_mode_cmd)
                cmd_list.append(layoutcfgtools.get_layout_render_mode_cmd(cid, lid, wnd_num, render_mode_ind))
                cmd_list.append("LAYOUT MAP APPLY " + cid + " " + lid)
        else:
            cmd_list.append(
                layoutcfgtools.role_cmd(layoutcfgtools.DEFAULT_EMPTY_ROLE_ID, cid, lid, wnd_num, conn_id_str))
            cmd_list.append("LAYOUT MAP APPLY " + cid + " " + lid)
            cmd_list.append("LAYOUT DETACH " + cid + " " + lid + " " + conn_id_str)
            if aspeaker and asp2lyt:
                cmd_list.append("LAYOUT DETACH " + cid + " SPEAKER " + conn_id_str)

    # get "not fixed" slotsin all layouts of conference confid
    layout_not_fixed_peer = []
    not_fixed_peer_rows = dbtools.select("""SELECT DISTINCT layoutbackup.layout_id, layoutbackup.type, layoutbackup.role,
                                         layoutbackup.render_mode, layoutbackup.wnd_num
                                         FROM legacy.layoutbackup, legacy.layoutsprmbackup 
                                         WHERE layoutbackup.layout_id=layoutsprmbackup.id AND layoutsprmbackup.auto=0 
                                         AND layoutbackup.layout_id IN 
                                         (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d)""" % (confid))
    for lyt in not_fixed_peer_rows:
        layout_id = lyt[0]
        type = lyt[1]
        roleid = lyt[2]
        render_mode_ind = lyt[3]
        wnd_num = lyt[4]

        # skip layout with fixed peers
        if layout_id in layouts_with_fixed_peer:
            continue

        if layoutcfgtools.is_role_not_fixed_peer(roleid):
            found = False
            for layout in layout_not_fixed_peer:  # find layout in result list
                if layout["layout_id"] == layout_id:
                    found = True
                    break

            # add layout to result list (one time!)
            if not found:
                layout_not_fixed_peer.append({"layout_id": layout_id, "type": type, "roleid": roleid,
                                              "render_mode": render_mode_ind, "wnd_num": wnd_num})

    # prepare "not fixed" slots in layouts
    for layout in layout_not_fixed_peer:
        layout_id = layout["layout_id"]
        layout_type = layout["type"]
        lid = tools.get_layout_name_by_id(confid, layout_id)
        render_mode_ind = layout["render_mode"]
        wnd_num = layout["wnd_num"]
        layout_mode_cmd = layoutcfgtools.get_layout_cmd_ex(layout_type, cid, lid, custom_lyt_types)
        if badd:
            cmd_list.append("LAYOUT ATTACH " + cid + " " + lid + " " + conn_id_str)
            cmd_list.append(layout_mode_cmd)
            cmd_list.append(layoutcfgtools.get_layout_render_mode_cmd(cid, lid, wnd_num, render_mode_ind))
            cmd_list.append("LAYOUT MAP APPLY " + cid + " " + lid)
        else:
            cmd_list.append("LAYOUT DETACH " + cid + " " + lid + " " + conn_id_str)

    nettools.exec_cmd_list_s(cmd_list)
    return (1)


def get_outgoing_call(address):
    """Get params of outgoung call"""
    result = {"confid": 0, "abid": 0}

    # logging.debug( "get_outgoing_call(%s)" % (address) )

    # strict compare addresses
    calls_rows = dbtools.select("SELECT confid, abid, sipaddr, h323addr, extaddr, file FROM legacy.outgoingcalls")
    for call in calls_rows:
        confid = call[0]
        abid = call[1]
        sipaddr = call[2]
        h323addr = call[3]
        extaddr = call[4]
        file = call[5]
        # logging.debug( "get_outgoing_call() check address: sip=%s , h323=%s , ext=%s" % (sipaddr, h323addr, extaddr) )
        if tools.is_addr_group_equivalent(address, sipaddr, h323addr, extaddr, file, True):
            result["confid"] = confid
            result["abid"] = abid
            # logging.debug( "get_outgoing_call() found: confid=%d , abid=%d" % (confid, abid) )
            return result

    # not striect compare addresses
    for call in calls_rows:
        confid = call[0]
        abid = call[1]
        sipaddr = call[2]
        h323addr = call[3]
        extaddr = call[4]
        file = call[5]
        # logging.debug( "get_outgoing_call() check address: sip=%s , h323=%s , ext=%s" % (sipaddr, h323addr, extaddr) )
        if tools.is_addr_group_equivalent(address, sipaddr, h323addr, extaddr, file):
            result["confid"] = confid
            result["abid"] = abid
            # logging.debug( "get_outgoing_call() found: confid=%d , abid=%d" % (confid, abid) )
            return result

    return result


def del_outgoing_call(confid, abid):
    """delete outgoing call from buffer"""
    confid = int(confid)
    abid = int(abid)
    dbtools.make_sql_request("DELETE FROM legacy.outgoingcalls WHERE confid=%d and abid=%d" % (confid, abid))
    # logging.debug( "del_outgoing_call(): last err: " + dbtools.get_last_error() )


def start_sheduled_conference():
    """If exist sheduled coference, start it (in moment of starting of vcs-server)."""

    confid_rows = dbtools.select("SELECT confid FROM legacy.sheduledconference")
    if len(confid_rows) == 0:
        return  # no sheduled conference

    confid = confid_rows[0][0]
    if confid != 0:
        dbtools.make_sql_request("UPDATE legacy.sheduledconference SET confid=0")
        dbtools.make_sql_request("UPDATE legacy.conferences SET state=%d WHERE id=%d" % (data.CONF_STATE_START, confid))


def set_move_to_conf_prm(conf_from, conf_to, abid, aburi, conn_id):
    conf_from = int(conf_from)
    conf_to = int(conf_to)
    abid = int(abid)
    conn_id = int(conn_id)

    #TODO: find moved abonent by conn_id
    prm_rows = dbtools.select("SELECT aburi, conn_id FROM legacy.movetoconf WHERE conn_id=%d" % (conn_id))
    if len(prm_rows) != 0:
        dbtools.make_sql_request("""UPDATE legacy.movetoconf SET confid_from=%d, confid_to=%d, abid=%d WHERE conn_id=%d"""
                                 % (conf_from, conf_to, abid, conn_id))
        return

    dbtools.make_sql_request("""INSERT INTO legacy.movetoconf (confid_from, confid_to, abid, aburi, conn_id) 
    VALUES (%d, %d, %d, '%s', %d)""" % (conf_from, conf_to, abid, aburi, conn_id))


def get_move_to_conf_prm(aburi, conn_id):
    prm = {"conf_from": 0, "conf_to": 0, "abid": 0, "aburi": "", "conn_id": -1}

    prm_rows = dbtools.select("""SELECT confid_from, confid_to, abid, aburi, conn_id FROM legacy.movetoconf 
    WHERE conn_id=%d""" % (conn_id))
    if len(prm_rows) != 0:
        prm["conf_from"] = prm_rows[0][0]
        prm["conf_to"] = prm_rows[0][1]
        prm["abid"] = prm_rows[0][2]
        prm["aburi"] = prm_rows[0][3]
        prm["conn_id"] = prm_rows[0][4]
        return (prm)

    return (prm)


def del_move_to_conf_prm(aburi, conn_id):
    dbtools.make_sql_request("DELETE FROM legacy.movetoconf WHERE conn_id=%d" % (conn_id))


def is_abonent_in_some_layout(_confid, _ab_uri, _conn_id):
    """If abonent _ab_uri is exist in some layouts in conference _confid (in database), then return 1, else return 0."""
    _confid = int(_confid)

    # presentation already in layout (don't mute) presentation
    if tools.is_presentation_file(_ab_uri):
        return (1)

    # check automatic addition of abonents to layout
    lyt_rows = dbtools.select(
        "SELECT auto FROM legacy.layoutsprmbackup WHERE id IN (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d)" % (
            _confid))
    if len(lyt_rows) != 0:
        auto = lyt_rows[0][0]
        if auto:
            return (1)  # auto add: abonent is in layout

    # is some layout of conference has slot with role "ACTIVE"/"AUTO"/"ROLL"
    not_fixed_peer_rows = dbtools.select("""SELECT role FROM legacy.layoutbackup WHERE layout_id in 
                                        (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d)""" % (_confid))
    for row in not_fixed_peer_rows:
        roleid = row[0]
        if layoutcfgtools.is_role_not_fixed_peer(roleid):
            return (1)

    # if crown layout is on, then all participants is attached to it
    crown_layout = onlinetools.get_crown_layout(_confid, {})
    if crown_layout != {}:
        return (1)

    # get ID of abonent
    abid = addrbook.get_ab_id_by_conn_id(_conn_id)
    if abid == 0:
        return (0)  # no specified abonent in DB (no layout)

    # find abonent _ab_uri in layouts of conference _confid
    ab_rows = dbtools.select("""SELECT abid FROM legacy.layoutbackup WHERE abid=%d AND layout_id IN 
                               (SELECT id FROM legacy.layoutconfbackup WHERE confid=%d)""" % (abid, _confid))
    if len(ab_rows) != 0:
        return (1)
    else:
        return (0)


def update_abonent_address_in_addrbook(_abonent_uri: str, _conn_id: int, _confid: int, _outgoing: bool):
    """ This function add to address book new abonent with real address _abonent_uri (if it is not exist in address book).
     Or update 'realaddr' field (if contact is exist in address book)."""

    if tools.is_presentation_file(_abonent_uri): # skip presentation
        return (1)

    # save all addresses in database in quote encoding
    if not tools.is_addr_quoted(_abonent_uri):
        _abonent_uri = tools.quote_address(_abonent_uri)

    try:
        abid = addrbook.get_abonent_id(_abonent_uri)
        if abid != 0:
            # update_contact = ContactEntity(abid, None, None, None, None, None, None, None, None, _abonent_uri, None, None)
            # update_request = ChangeContactRequest(update_contact)
            # addrbook.update_contact(update_request)
            addrbook.update_realaddr(abid, _abonent_uri, _conn_id, _confid, _outgoing)
        else:
            name = tools.unquote_address(_abonent_uri)
            name = addrbook.get_unique_contact_name(name)

            sip = ""
            if len(_abonent_uri) > 3 and _abonent_uri[:3] == "sip":
                sip = _abonent_uri

            h323 = ""
            if len(_abonent_uri) > 4 and _abonent_uri[:4] == "h323":
                h323 = _abonent_uri

            rtsp = ""
            if len(_abonent_uri) > 4 and _abonent_uri[:4] == "rtsp":
                rtsp = _abonent_uri

            audio_codecs = AudioCodecsEntity(True)
            video_codecs = VideoCodecsEntity(True)
            create_request = CreateContactRequest(name, "", sip, h323, rtsp, "", 0, _abonent_uri, True, _conn_id,
                                                  audio_codecs, video_codecs, "", False)
            new_contact = addrbook.create_new_contact(create_request)
            addrbook.update_realaddr(new_contact.contact.contact_id, _abonent_uri, new_contact.contact.conn_id, _confid,
                                     _outgoing)
    except:
        logging.error("update_abonent_address_in_addrbook() error: " + str(sys.exc_info()))


def clear_abonent_address_in_addrbook(_abonent_uri: str, _conn_id: int, _confid: int, _outgoing: bool):
    """ This function find in address book abonent with real address _abonent_uri and clear real address. """

    if tools.is_presentation_file(_abonent_uri): # skip presentation
        return (1)

    try:
        abid = addrbook.get_ab_id_by_conn_id(_conn_id)
        if abid != 0: # abonent was connected
            addrbook.clear_realaddr(abid, _abonent_uri, _conn_id, _confid, _outgoing)
    except:
        logging.error("clear_abonent_address_in_addrbook() error: " + str(sys.exc_info()))


def unregister_abonent_in_abook(abid: int):
    """This function unregister abonent 'abid' in address book of vtsrv engine."""
    abook_name = tools.abid_to_abook_name(abid)
    cmd = "ABOOK UNREGISTER %s" % (abook_name)
    nettools.exec_cmd_list_s([cmd])

#!/usr/bin/python3


import _thread
import copy
import logging
import logging.handlers
import os
import queue
import sys
import threading
import time

import app.core.data as data
import app.core.dbtools as dbtools
import app.core.licenses as licenses
import app.core.logger as logger
import app.core.pipetools as pipetools
import app.core.sessionpartctrl as sessionpartctrl
import app.core.videorecord as videorecord
import app.core.tools as tools
import app.core.xmltools as xmltools
import event_processor.ipc.on_event_tools as on_event_tools
import event_processor.ipc.on_vcs_msg as on_vcs_msg
import event_processor.ipc.state_controller as state_controller

if sys.platform == "win32":  # --------------------------- WINDOWS VERSION ---------------------------------
    import win32pipe


def get_msg_type(msg):
    """This function parse message 'msg' from VCS-server (one line).
    Returns type of message (first word)."""
    msg_len = len(msg)
    if msg_len == 0:
        return ("")

    # parse type of message
    i = 0
    type_str = ""
    while i < msg_len:
        if msg[i] != " ":
            type_str = type_str + msg[i]
        else:
            break
        i = i + 1

    return (type_str)


def multiplex_to_queue(_queue, _event):
    """This function reads messages from named pipe(s) and multiplex its to one queue."""
    logging.info("multiplex_to_queue() is launched")
    srv_pipe = pipetools.open_server_pipe()
    while 1:
        msg = pipetools.read(srv_pipe)
        # logging.debug("readed from pipe: " + msg)
        msg = msg.strip()
        if msg == "":
            continue
        logging.debug("put to queue: " + msg)
        _queue.put(msg)
        _event.set()
        if msg == "quit":
            logging.info("multiplex_to_queue(): quit received...")
            break

        if sys.platform == "win32":  # --------------------------- WINDOWS VERSION ---------------------------------
            win32pipe.DisconnectNamedPipe(srv_pipe)
            win32pipe.ConnectNamedPipe(srv_pipe, None)

    pipetools.close_pipe(srv_pipe, 1)


def process_queue(_queue, _event, _timeout):
    """This function reads messages from queue andprocess it. + make periodic actions."""
    start_time = time.time()
    last_upd_time = start_time
    last_upd_time2 = start_time
    last_upd_time3 = start_time
    last_upd_time4 = start_time

    last_upd_time5 = start_time
    last_recording_state = {}

    started = 0

    # TMP: reserve_updated = 0

    while 1:
        _event.wait(_timeout)
        bempty = 0
        while not bempty:
            try:
                rd_msg = _queue.get_nowait()
            except:
                rd_msg = ""
                bempty = 1
                _event.clear()

            rd_msg = rd_msg.strip()
            if rd_msg != "":
                msg_list = rd_msg.splitlines()

                for msg in msg_list:
                    if msg == "quit":
                        logging.info("get message 'quit'")
                        return

                    # process message
                    msg_type = get_msg_type(msg)
                    if msg_type == "START" or msg_type == "CALL" or msg_type == "CONFERENCE" or msg_type == "LAYOUT" or msg_type == "REGISTRATION" or msg_type == "FEATURES":
                        on_vcs_msg.process_server_message(msg)

                    if msg_type == "START":
                        started = 1

                    # TODO:
                    # if msg_type == "DELLICENSES":
                    #  licensetools.del_license_files()

                    if msg_type == "RESTART":
                        logging.info("try to restart server")
                        is_conf_stopped = on_event_tools.stop_all_conferences()
                        if is_conf_stopped != 0:
                            time.sleep(2)
                        # if len(msg) > len("RESTART "):
                        #    params = msg[len("RESTART "):]
                        # else:
                        #    params = ""
                        sys.exit(1)

                    if msg_type == "RECONNECTDB":
                        logging.info("try to reconnect to database")
                        dbtools.reconnect()

                    # NOTE: update state for long series of messages (don't freeze GUI)
                    cur_time = time.time()
                    if cur_time > last_upd_time + 1.0:
                        last_upd_time = cur_time
                        state_controller.update_state()
                        # logging.debug("update internal state (2)")

        # NOTE: wait while come message "START PORT=5080"
        if started == 0:
            continue

        cur_time = time.time()

        if cur_time > last_upd_time + _timeout:
            #dt = cur_time - last_upd_time
            #logging.error("UPDATE STATE: dt=%f" % (dt))
            last_upd_time = cur_time
            state_controller.update_state()

        if cur_time > last_upd_time2 + sessionpartctrl.PREVIEW_TIMEOUT:
            last_upd_time2 = cur_time
            sessionpartctrl.stop_preview_at_timeout()

        if cur_time > last_upd_time3 + 5:
            last_upd_time3 = cur_time
            state_controller.autocall_abonents()

        if cur_time > last_upd_time4 + 600:
            last_upd_time4 = cur_time
            if not videorecord.can_write_video():
                videorecord.stop_all_recording()

        if cur_time > last_upd_time5 + 5:
            last_upd_time5 = cur_time
            new_recording_state = videorecord.get_all_conf_record_state()
            for confid, state in new_recording_state.items():
                layout_was_empty = True
                if confid in last_recording_state:
                    layout_was_empty = last_recording_state[confid]["record_layout_empty"]

                if (not state["recording"]) and state["autostart"] and layout_was_empty and (
                not state["record_layout_empty"]):
                    logging.error("AUTO START VIDEO RECORD")
                    videorecord.switch_video_record2(confid, True)
                elif state["recording"] and state["autostop"] and (not layout_was_empty) and state[
                    "record_layout_empty"]:
                    logging.error("AUTO STOP VIDEO RECORD")
                    videorecord.switch_video_record2(confid, False)

            last_recording_state = copy.deepcopy(new_recording_state)

        lic_upd_time = licenses.get_license_upd_time()
        if cur_time < lic_upd_time:
            lic_upd_time = cur_time
            licenses.set_license_upd_time(int(cur_time))

        first_lic_upd = licenses.is_first_upd_license()
        start_timeout = cur_time > start_time + 60
        update_period_complete = cur_time > lic_upd_time + 24*3600

        if update_period_complete or (first_lic_upd and start_timeout):
            if not tools.is_some_conf_launched():
                lic_state = licenses.get_license_state()
                lic_type = licenses.get_license_type()
                if lic_state == data.LIC_STAT_VALID and lic_type != data.LIC_TYPE_PERMANENT:
                    lic_upd_cnt = licenses.get_license_upd_cnt()
                    licenses.set_license_upd_time( int(time.time()) )

                    if first_lic_upd:
                        logging.debug("CHECK LICENSES: AT START")
                    else:
                        logging.debug("CHECK LICENSES: CHECK COUNT=%d" % (lic_upd_cnt))
                    result = licenses.smart_upd_lic_from_srv(0)

                    drop_licenses_now = first_lic_upd
                    first_lic_upd = False
                    licenses.set_first_license_upd(first_lic_upd)

                    if result["ok"]:
                        lic_upd_cnt = 0
                        licenses.set_license_upd_cnt(lic_upd_cnt)
                        sys.exit(1)
                    else:
                        if update_period_complete:
                            lic_upd_cnt += 1
                            licenses.set_license_upd_cnt(lic_upd_cnt)
                        # new license does not loaded, analyse params of old license
                        if lic_upd_cnt >= licenses.MAX_LIC_UPD_FAILS or drop_licenses_now:
                            if lic_upd_cnt >= licenses.MAX_LIC_UPD_FAILS:
                                logging.debug("CANNOT GET LICENSE FROM LICENSE SERVER %d TIMES" % (lic_upd_cnt))
                            if drop_licenses_now:
                                logging.debug("CANNOT GET LICENSE AT START")
                            logging.debug("TURN ENGINE INTO 'PUMPKIN'")
                            licenses.del_all_licenses()
                            sys.exit(1)


# if __name__ != "__main__":
#    quit()

def start_event_processor():
    # Initialize logging
    logger.init_logger("event_processor.log")
    logging.info("start event processor")

    if sys.platform != "win32":
        cfg = xmltools.get_config()
        cmd = "cd %s ; /usr/bin/hwid_test ;" % (cfg["dirs"]["shareddir"])
        os.system(cmd)

    dbtools.set_optimize_connection(True)

    on_vcs_msg.clean_event_processor()

    # create multiplexor queue
    g_queue = queue.Queue(1000)

    g_event = threading.Event()
    g_event.clear()

    # start reading server pipes to move data to one multiplexor queue
    _thread.start_new_thread(multiplex_to_queue, (g_queue, g_event,))

    # get messages from queue and process it
    process_queue(g_queue, g_event, 0.2)

    exit()


#EXPERIMENT:
def run_event_processor_in_web_api():
    logging.info("start event processor")

    if sys.platform != "win32":
        cfg = xmltools.get_config()
        cmd = "cd %s ; /usr/bin/hwid_test ;" % (cfg["dirs"]["shareddir"])
        os.system(cmd)

    #dbtools.set_optimize_connection(True)

    on_vcs_msg.clean_event_processor()

    # create multiplexor queue
    g_queue = queue.Queue(1000)

    g_event = threading.Event()
    g_event.clear()

    # start reading server pipes to move data to one multiplexor queue
    _thread.start_new_thread(multiplex_to_queue, (g_queue, g_event,))

    # get messages from queue and process it
    _thread.start_new_thread(process_queue, (g_queue, g_event, 0.2,))


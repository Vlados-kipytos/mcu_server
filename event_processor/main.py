from app.core import cmdline
from app.core import xmltools
from event_processor.ipc.event_processor import start_event_processor


def main() -> None:
    cmd_line_params = cmdline.parse_command_line()
    if not cmd_line_params["success"]:
        print(cmd_line_params["error"])
        return

    if "--config" in cmd_line_params["params"]:
        xmltools.load_config(cmd_line_params["params"]["--config"])

    start_event_processor()

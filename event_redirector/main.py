import app.core.cmdline as cmdline
import app.core.xmltools as xmltools
import event_processor.ipc.redirector_tools as redirector_tools


def main() -> None:
    cmd_line_params = cmdline.parse_command_line()
    if not cmd_line_params["success"]:
        print(cmd_line_params["error"])
        return

    if "--config" in cmd_line_params["params"]:
        xmltools.load_config(cmd_line_params["params"]["--config"])

    redirector_tools.launch_redirector()

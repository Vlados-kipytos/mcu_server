import datetime
from typing import List, Optional, Tuple

from aiohttp.web import Application

import app.core.dbtools as dbtools
from mcu import BaseHandler
from mcu.postgresql.client import Manager as PostgreSqlClientManager

NAME = 'mcu.postgresql.schemas.oauth.get_access_token'


class Params:
    def __init__(self, access_token_id: str):
        self.access_token_id = access_token_id


class Result:
    def __init__(self,
                 access_token_id: str,
                 created_at: datetime.datetime,
                 expire_at: datetime.datetime,
                 user_id: int,
                 client_id: str,
                 scopes: List[str]):
        self.access_token_id: str = access_token_id
        self.created_at: datetime.datetime = created_at
        self.expire_at: datetime.datetime = expire_at
        self.user_id: int = user_id
        self.client_id: str = client_id
        self.scopes: List[str] = scopes


class Select(BaseHandler):

    @staticmethod
    def get_class_name() -> str:
        return ".".join([Select.__module__, Select.__name__])

    @staticmethod
    def setup(app: Application) -> None:
        app[Select.get_class_name()] = Select(app)

    async def handle(self, params: Params) -> Optional[Result]:
        #OLD:
        #client: PostgreSqlClientManager = self._app[PostgreSqlClientManager.get_class_name()]
        #record: Tuple = await client.fetch_row(
        #    "select * from oauth.get_access_token( %(access_token_id)s )", {
        #        "access_token_id": params.access_token_id
        #    })

        # fix "Proxy Error 502":
        rows = dbtools.select("select * from oauth.get_access_token('%s')" % (params.access_token_id))
        if len(rows) != 1:
            return None
        record = rows[0]

        if record is None:
            return None

        if record[0] is None:
            return None

        return Result(
            access_token_id=params.access_token_id,
            created_at=record[0],
            expire_at=record[1],
            user_id=record[2],
            client_id=record[3],
            scopes=record[4]
        )

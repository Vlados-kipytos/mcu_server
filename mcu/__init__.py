from abc import abstractmethod

from aiohttp.web import Application


class BaseHandler:
    _app: Application

    def __init__(self, app: Application):
        self._app = app

    @staticmethod
    @abstractmethod
    def get_class_name() -> str:
        pass

    @staticmethod
    @abstractmethod
    def setup(app: Application) -> None:
        pass

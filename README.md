# Project

## Prerequisites

1. python 3.5.3
1. pipenv

## Framework
В качестве фреймворка используется [aiohttp](https://docs.aiohttp.org/en/stable/index.html)

## Build virtual environment
1) зайти в каталог web-dev\api: ```cd api```
2) создать виртуальное окружение включая девелоперские пакеты (pyinstaller): ```pipenv install --dev```

## Build exe in Windows
1) войти в каталог web-dev\api: ```cd api```
2) запустить оболочку настроенную на виртуальное окружение: ```pipenv.exe shell```
3) создать каталог dist_web_api с исполняемым файлом web_api, библиотеками и файлами данных: ```pyinstaller.exe --name web_api --workpath .\build_web_api --distpath .\dist_web_api .\app\__main__.py``` 
4) создать каталог dist_evt_proc с исполняемым файлом evt_proc (event_processor): ```pyinstaller.exe --name evt_proc --workpath .\build_evt_proc --distpath .\dist_evt_proc .\event_processor\__main__.py```
5) создать event redirector: ```pyinstaller.exe --name redirector --workpath .\build_redirector --distpath .\dist_redirector .\event_redirector\__main__.py```
6) выйти из оболочки: exit

## Build exe in Linux
1) войти в каталог web-dev\api: ```cd api```
2) запустить команду: ```pipenv run pyinstaller --clean --name web_api --workpath ./build_web_api --distpath ./dist_web_api ./app/__main__.py```
  (исполняемый файл web_api появится в каталоге "dist_web_api" вместе с библиотеками и файлами данных)
3) собрать event_processor: ```pipenv run pyinstaller --clean --name evt_proc --workpath ./build_evt_proc --distpath ./dist_evt_proc ./event_processor/__main__.py```
4) собрать event redirector: ```pipenv run pyinstaller --clean --name redirector --workpath ./build_redirector --distpath ./dist_redirector ./event_redirector/__main__.py```


## Tests
Запуск тестов из PyCharm:
1) Нажать на текущую конфигурацию (в верхнем меню справа, рядом со значком запуска), нажать "Edit configuration"
2) Установить:
    1) Script path: ```path_to_project/api/tests/__main__.py```
    2) Environment variables: добавить ```PYTHONPATH=path_to_project/api/tests/api/v1```
    3) Working Directory: ```path_to_project/api/tests```
    4) Поставить две галочки Add ... roots to PYTHONPATH
3) По клику на зелёный треугольник ('Run') запускаются тесты

Из консоли:
1) ```pipenv shell``` из папки с проектом
2) ```cd tests```
3) ```SET PYTHONPATH=path_to_project/api;path_to_project/api/tests/api/v1```
4) ```pipenv run python __main__.py``` - запуск тестов
   
О структуре тестов: в ```api/tests/api/v1``` каждая директория представляет из себя набор тестов для одного из endpoint. 
Структура одного теста (порядок запросов, их url, http-методы и т. п.) описываются в файлах ```test_*.tavern.yaml``` 
(ВАЖНО!!! - по документации модуля Tavern название файла должно начинаться со слова ```test```)
Все эти файлы для формирования запросов используют файл ```api/tests/config.yaml```, содержащий имя протокола, хост, 
порт и версию API, а так же вызывают python-функции для формирования данных запросов и обработки ответов (например, 
тест ```test_one_abonent.tavern.yaml``` вызывает функции из ```api/tests/api/v1/test_abook_functions```). Python-файлы, 
функции которых будут вызываться из ```.tavern.yaml```, должны находиться в директории, которая содержится в переменной
среды PYTHONPATH (ВАЖНО!!!). Тестовые данные лежат в этой же директории и каждый их набор содержит префикс, 
использующийся при отправке данных/обработке ответа для сопоставления стадии теста.

О проекте тестов: в функции ```main()``` из файла ```api\tests\main.py``` вызываются функции из каждой директории в 
```api\tests\api\v1```. Эти функции ищут в своих директориях файлы ```test_<name_group_test>.tavern.yaml```, для 
каждого такого найденного файла ищут в этой же директории файлы с данными ```<name_group_test>_data_test*.yaml``` и для каждого
такого файла запускают тест. Результаты тестов пишутся в log-файл, лежащий в корневой папке проекта (При каждом запуске 
```main``` создаётся новый файл).

Директория ```api\tests\api\v1\address_book``` содержит универсальный файл ```test.py```, с помощью которого можно
тестировать все запросы, связанные с простыми сущностями (создание, получение, редактирование и удаление сущности (
например, контакта в адресной книге, конференции, группы и т. п.)). 